# Quasi 1 Dimensional Compressible Flow Dynamics: q1dcfd #

1D compressible flow, real gas computational fluid dynamics for simulation of power cycles and channel flow

## Summary: ##

* Version: 0.1

q1dcfd is designed for 1d dynamic simulation of axially dominant flow, such as flow through channels, pipes, turbomachinery and certain types of heat exchangers. The underlying physics relies upon the quasi-one-dimensional, finite volume formulation of the viscid, compressible euler equations. Viscous effects, higher-order effects and external heat transfer are modelled using bulk or thin-film correlations. Heat transfer across solid elements such as pipe walls, turbomachinery housing and heat exchanger walls is modelled using the finite volume form of Fourier's heat transfer equation. Turbomachinery is modelled as a combination of expanding contracting channel areas, with momentum energy jump discontinuities, using performance maps.

## Install ##

### Computational Environment ###

Currently only compatible with \*nix environments.

### Dependencies ###

* Python3.6+ with the following packages are required to run q1dcfd:

    * numpy
    * matplotlib
    * python3-tk
    * prompt-toolkit (v2.0+)
    * CoolProp (python wrapper) (v6.0.0 or above)

* A recent D compiler, either dmd or ldc. If using dmd, you must have version 2.088 or newer. Install links are available on https://dlang.org/download.html. You must also have the dub package manager, this should come packaged with D by default.

If you are using Linux and have curl installed, you can install the chosen compiler with the following commands:

dmd: 
```shell 
curl -fsS https://dlang.org/install.sh | bash -s dmd
```

ldc: 
```shell
curl -fsS https://dlang.org/install.sh | bash -s ldc
```

As of early 2018, dmd has faster compilation speeds and is best for running development or debug builds. ldc produces a more optimized executable.

* CoolProp Static Library (installation instructions below)

#### Obtaining the CoolProp Static Library ####

Navigate using the terminal to the location where you would like to save the CoolProp repository.

Check out the sources for CoolProp

```shell
git clone https://github.com/CoolProp/CoolProp --recursive
```

Move into the repository you just created

```shell
cd CoolProp
```

Add the following lines to CMakeLists.txt below the existing options

```cmake
option(NO_ERROR_CATCHING
    "Do not catch errors, let them be thrown"
    OFF)
IF (NO_ERROR_CATCHING)
    add_definitions(-DNO_ERROR_CATCHING) 
ENDIF()
```

Make a build folder

```shell
mkdir -p build && cd build
```

Build the makefile using CMake

```shell
cmake .. -DCOOLPROP_STATIC_LIBRARY=ON -DNO_ERROR_CATCHING=ON
cmake --build .
```

#### Obtaining the PyPi CoolProp Wrapper ####

Note: Even if you have the CoolProp source code, you will require the python wrapper version for table generation and post processing. Download with:

```shell
python3.6 -m pip install -vvv --pre --trusted-host www.coolprop.dreamhosters.com --find-links http://www.coolprop.dreamhosters.com/binaries/Python/ -U --force-reinstall CoolProp
```

Replacing python3.6 with whatever version of python 3 you are using (3.6+).

### Installation ###

* From the directory in which you want q1dcfd to be installed. Clone this repository:

```shell
git clone https://gitlab.com/treddell/q1dcfd.git --recursive
```

* Add to PATH environment variable. On Linux from the root directory, edit .bashrc with your text editor of choice, enter the following line:

```shell
export PATH=${PATH}:/path/to/cloned/repository/q1dcfd/source/cli
```

where ```/path/to/cloned/repository/``` is replaced by whatever location (from root directory) you cloned the q1dcfd repository to in step 1.

Now run:

```shell
source .bashrc
```

You may need to reset the terminal by closing and reopening it.

* Run installation compilation. Make sure the chosen compiler is activated in the current environment with the following command, replace{COMPILER-VERSION} with whichever environment you want to activate:

```shell
source ~/dlang/{COMPILER-VERSION}/activate
```

Then navigate to the q1dcfd repository and run:

```shell
sh install.sh /home/user/CoolProp/install/directory
```

Where /home/user/CoolProp/install/directory is replaced with whatever location you installed the CoolProp repository at.

q1dcfd should now be ready to use.

* (Optional) run the test suite

Open the q1dcfd command line application:

```shell
q1dcfd
```

In the q1dcfd shell, enter:

```shell
> tests
```

Once the tests have completed, reconfigure q1dcfd to use the main application with:

```shell
> build
```

### Upgrading ###
Pull the latest version from the repository and rerun the install.sh script

### Getting Started ###

To run any simulations, you will require a fluids dataset. The packaged definition scripts expect a CO2 dataset named 'sCO2'. To generate this, open the q1dcfd application, and in the shell, enter:

```shell
> tablegen sCO2 CO2 4.0E6:25.0E6 270:900
```

This will generate a density-internal energy and pressure-enthalpy table with all the necessary inputs, between pressures of 4-25MPa and temperatures of 270-900 K. By default, a linear grid of 200*200 points will be used. This mesh can be refined by passing in --N1=... --N2=... flags.

The bundled definition scripts should now be able to be simulated. From within the q1dcfd shell:

```shell
> run simName.qdef
```

Where simName.qdef is the name of whichever script you wish to run. A list should appear after typing run in the shell. Results will be saved in q1dcfd/results, and can be loaded with the ```load``` command. The ```help``` command will display a list of topics.

### Reporting Bugs/Suggestions ###

File an issue

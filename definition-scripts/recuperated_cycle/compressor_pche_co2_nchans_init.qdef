// run recuperated_cycle/compressor_pche_co2_nchans_init.qdef
// Simulates the system
//                    outflow <-- |    | <-- inflow
//                                |pche|
//      inflow --> compressor --> |    | --> outflow
//
// Uses map-based turbomachinery models.
// Designed to initialize open cycle and for compressor model testing.
// Runs purely off 'constant' (not stored) initial conditions, so is easily
// portable.
// Run simulation then export results and use to initialize a simulation with a
// turbine.
//
// See 'recuperated_cycle.py' in definition-scripts/init_scripts/ for details on
// how geometry values are calculed.


// PLOTTING CMDS:
// qseries T inflow inflow_pipe[0] compressor_inflow compressor_outflow connector_pipe[0] recuperator[1] recuperator_turbine_pipe[0] outflow -times=4
// qseries T recuperator_inflow recuperator[0] recuperator_outflow -times=4

// NOTES:
// - When initializing the real cycle, need to carefully consider geometric
// parameters, correlations, etc (haven't done so here).
//
// - Stagnation outflow is required to maintain stability of initialization
// procedure. The outlet stagnation pressure will set the compressor high-side
// pressure, ensuring that the compressor doesn't surge when the high-side
// temperature increases. (During the initial 'shock' the compressor may surge
// briefly, though this should correct quickly.)
//
// - PCHE wall setting induces some interesting behaviours. Bigger walls seem to
// somehow stop compressor surging (not sure of mechanism).
// 
// - With a standard max Courant number, the initial 'shock' crashes the simulation.
// Using max_CFL = 0.3 gives good results.


Simulation: comp_recup_nchans
{
    end_time      : 4.0;
    max_CFL       : 0.3; // Low CFL to handle initial 'shock'
    steady_tol    : 2E5;
    save_interval : 1.0E-2;
}

Define:
{
    //------------------------------------------------------------------------------
    // GEOMETRY
    // Currently must use area-matched geometry (i.e. pipe and PCHE flow areas
    // are the same).
    pche_flow_area    :  7.853981633974482e-07;
    pche_perimeter    :  0.0031415926535897933;
    pche_dh           :  0.001;
    pche_n_chans      :  40000;
    pche_length       :  0.6;
    n_rows            :  200;
    n_cols            :  200;
    pche_wall_t1      :  0.0013;
    pche_wall_t2      :  0.0006;
    pipe_flow_area    :  0.031415926535897934;
    pipe_perimeter    :  0.6283185307179586;
    pipe_dh           :  0.2;

    // // Pipes, not area matched
    // pipe_flow_area    : 0.001257;
    // pipe_dh           : 0.04;
    // pipe_perimeter    : 3.3322E-3; // Correct this

    //------------------------------------------------------------------------------
    // THERMODYNAMIC CONDITIONS
    // Compressor conditions
    comp_t_stag     : 320.0;
    comp_p_stag     : 8.65e+06;
    comp_t_in       : 320;
    comp_p_in       : 8.5e+06;
    comp_v_in       : 29.74;
    comp_t_in_wall  : 320;
    comp_t_out      : 350;
    comp_p_out      : 12.73e+06;
    comp_v_out      : 23.02;
    comp_t_out_wall : 350;
    comp_mdot       : 10;

    // Turbine conditions, scaled to match compressor/hx outlet
    turb_t_in       : 507.6;
    turb_p_in       : 1.27e+07;
    turb_v_in       : 55.6373;
    turb_t_in_wall  : 507.6;
    turb_t_out      : 470.261;
    turb_p_out      : 8.53591e+06;
    turb_v_out      : 76.5605;
    turb_t_out_wall : 470.261;
    turb_mdot       : 10;
    turb_er_design  : 1.5;
    turb_mfp_design : 1.785;
    turb_T0_design  : 500.0;
    turb_P0_design  : 12.53E6;

    // Recuperator inlet conditions
    recup_p_stag : 9.1E6;
    recup_t_low  : 350;
    recup_v_low  : 40.87; // Corresponds to t_low and p_in
    recup_t_in   : 576.85;
    recup_p_in   : 9E6;
    recup_v_in   : 93.96;

    // Molten salt inlet conditions
    t_in_salt  : 700;
    p_in_salt  : 8E6;
    v_in_salt  : 4.0;
}

// Pipe-sized cross section, used for both turbomachine plenums and all pipes
ChannelCrossSection: turbo_cross_section
{
    cross_area         : pipe_flow_area;
    heat_circumference : pipe_perimeter;
    hydraulic_diameter : pipe_dh;
}

// Cross-section of a PCHE flow channel
ChannelCrossSection: pche_cross_section
{
    cross_area         : pche_flow_area;
    heat_circumference : pche_perimeter;
    hydraulic_diameter : pche_dh;
    // roughness          : pche_roughness;
}

// Cross-section of a PCHE wall
WallCrossSection: pche_wall_cross_section
{
    // Reduced values
    width         : pche_wall_t1;
    thickness     : pche_wall_t2;

    // properties of stainless steel
    density       : 7700.0; // kg/m3
    conductivity  : 16.3; // W/m.K
    specific_heat : 510.7896; // J/kg.K
}

// For pipe walls
WallCrossSection: wall_cross_section
{
    width     : 0.1257;
    thickness : 0.01;

    // Properties of stainless steel
    conductivity  : 16.3; // W/m.K
    density       : 7700.0; // kg/m3
    specific_heat : 510.7896; // J/kg.K
}

Inflow: inflow
{
    node_model         : InflowFromStagnation;
    a_in               : pipe_flow_area;
    relax_factor       : 0.005;
    coolprop_fluid     : CO2;
    coolprop_backend   : BICUBIC&HEOS;

    state_transient    : comp_t_stag;
    massflow_transient : comp_mdot;

    initial_stagnation_pressure    : comp_p_stag;
    initial_stagnation_temperature : comp_t_stag;

    initial_temperature        : comp_t_in;
    initial_pressure           : comp_p_in;
    initial_velocity           : comp_v_in;

    save: e, p, T, v, mdot, rho, p_stag, t_stag;
}

Pipe: inflow_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : comp_t_in;
    channel_initial_pressure    : comp_p_in;
    channel_initial_velocity    : comp_v_in;
    wall_initial_temperature    : comp_t_in_wall;

    save_channel: e, p, T, v, mdot, rho;
}

MapCompressor: compressor
{
    cross_section : turbo_cross_section;
    plenum_length : 0.04;
    map           : compressor-data;
    stream        : flow_path; // Needs to match stream below
    rotor_inertia : 7.0E-4;
    initial_speed : 1.00;
    motor_torque  : Polynomial(110.4);

    initial_upstream_temperature   : comp_t_in;
    initial_upstream_pressure      : comp_p_in;
    initial_upstream_velocity      : comp_v_in;
    initial_downstream_temperature : comp_t_out;
    initial_downstream_pressure    : comp_p_out;
    initial_downstream_velocity    : comp_v_out;
    initial_mass_flow_rate         : comp_mdot;

    save_turbomachine              : speed, mdot, delta_h, PR, w_sh, eta, p_in, p_out, T_in, T_out, T_load, T_motor;
}

Pipe: connector_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : comp_t_out;
    channel_initial_pressure    : comp_p_out;
    channel_initial_velocity    : comp_v_out;
    wall_initial_temperature    : comp_t_out_wall;

    save_channel: e, p, T, v, mdot, rho;
}

// Recuperator definition
Define:
{
    recuperator_n_nodes          : 40;
    recuperator_incomp_period    : 0.004;
    recuperator_hot_coolprop_backend : INCOMP;
    recuperator_hot_coolprop_fluid   : NaK;
}
ParallelHE: recuperator
{
    orientation              : counterflow;
    channel[0]_cross_section : pche_cross_section;
    channel[1]_cross_section : pche_cross_section;
    wall_cross_section       : pche_wall_cross_section;
    nodes                    : recuperator_n_nodes;
    length                   : pche_length;
    n_channels               : pche_n_chans;
    name_convection[0]       : pche_convection_hot;
    name_convection[1]       : pche_convection_cold;

    channel[0]_initial_temperature : recup_t_low;
    channel[0]_initial_pressure    : recup_p_in;
    channel[0]_initial_velocity    : recup_v_low;

    channel[1]_initial_temperature : comp_t_out;
    channel[1]_initial_pressure    : comp_p_out;
    channel[1]_initial_velocity    : comp_v_out;

    wall_initial_temperature : comp_t_out_wall;

    save_channel[0] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_channel[1] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_wall       : T, heat_flux;
}

// Will flow to turbine in later jobfile
Pipe: recuperator_turbine_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : comp_t_out;
    channel_initial_pressure    : comp_p_out;
    channel_initial_velocity    : comp_v_out;
    wall_initial_temperature    : comp_t_out_wall;

    save_channel: e, p, T, v, mdot, rho;
}

Outflow: outflow
{
    node_model          : OutflowToStagnation;
    coolprop_fluid      : CO2;
    coolprop_backend    : BICUBIC&HEOS;

    initial_temperature : comp_t_out;
    initial_pressure    : comp_p_out;
    initial_velocity    : comp_v_out;

    save                : e, p, T, v, mdot, rho, p_stag;
}


//------------------------------------------------------------------------------
// Recuperator hot side

Inflow: recuperator_inflow
{
    node_model         : InflowFromStagnation;
    a_in               : pipe_flow_area;
    relax_factor       : 0.005;
    coolprop_fluid     : CO2;
    coolprop_backend   : BICUBIC&HEOS;

    state_transient    : CubicRamp(0.4, 350, 1.0, 550);
    massflow_transient : comp_mdot;

    initial_stagnation_pressure    : recup_p_stag;
    initial_stagnation_temperature : recup_t_low;

    initial_temperature        : recup_t_low;
    initial_pressure           : recup_p_in;
    initial_velocity           : recup_v_low;

    save: e, p, T, v, mdot, rho, p_stag, t_stag;
}

Outflow: recuperator_outflow
{
    node_model          : OutflowToStagnation;
    coolprop_fluid      : CO2;
    coolprop_backend    : BICUBIC&HEOS;

    initial_temperature : recup_t_low;
    initial_pressure    : recup_p_in;
    initial_velocity    : recup_v_low;

    save: e, p, T, v, mdot;
}

Stream: flow_path
{
    data : sCO2;

    // LP side
    inflow
    -> inflow_pipe[0]
    -> compressor_inflow;

    // HP side
    compressor_outflow
    -> connector_pipe[0]
    -> recuperator[1]
    -> recuperator_turbine_pipe[0]
    -> outflow;

    // Recuperator hot side
    recuperator_inflow
    -> recuperator[0]
    -> recuperator_outflow;
}


// Simulates the system
//                    outflow <-- |    | <-- inflow
//                                |pche|
//      inflow --> compressor --> |    | --> outflow
//
// Uses map-based turbomachinery models.
// Used to test 'FromData' initialization.


// PLOTTING CMDS:
// qseries T inflow inflow_pipe[0] compressor_inflow compressor_outflow connector_pipe[0] recuperator[1] recuperator_turbine_pipe[0] outflow -times=4
// qseries T connector_pipe[0] recuperator[1] recuperator_turbine_pipe[0] -times=4
// qseries T recuperator_inflow recuperator[0] recuperator_outflow -times=4
// qseries re recuperator[1] -times=4

// These simulations don't obtain the velocities that I am trying to hit in the
// input script (must have a bug somewhere in this script).
// I think the very high velocities are the cause of the low heat transfer.
// Should run a sim for 10 sec and use this as new initialization data.


Simulation: comp_recup_nchans_from_data
{
    end_time      : 10.0;
    max_CFL       : 0.6;
    steady_tol    : 2E5;
    save_interval : 1.0E-2;
}

Define:
{
    //--------------------------------------------------------------------------
    // Geometry

    // This geo obtains a reasonable approach temperature
    pche_flow_area    :  5.026548245743669e-07 ;
    pche_perimeter    :  0.0025132741228718345 ;
    pche_dh           :  0.0008 ;
    pche_n_chans      :  202500 ;
    pche_length       :  3.0 ;
    n_rows            :  450 ;
    n_cols            :  450 ;
    pche_wall_t1      :  0.0013 ;
    pche_wall_t2      :  0.0006 ;

    pipe_flow_area    :  0.10178760197630929 ;
    pipe_perimeter    :  1.1309733552923256 ;
    pipe_dh           :  0.36 ;

    // //--------------------------------------------------------------------------
    // // This geometry achieves T_ap = ~70 K with a maximum Re of ~1350
    // // Velocity isn't quite what I was aiming for in the job file, need to debug
    // pche_flow_area    :  7.853981633974483e-07 ;
    // pche_perimeter    :  0.0031415926535897933 ;
    // pche_dh           :  0.001 ;
    // pche_n_chans      :  445493 ;
    // pche_wall_t1      :  0.013461410037997614 ;
    // pche_wall_t2      :  0.013461410037997614 ;

    // pipe_flow_area    :  0.34988938400641945 ;
    // pipe_perimeter    :  2.096864247741017 ;
    // pipe_dh           :  0.667452620041303 ;

    //--------------------------------------------------------------------------
    // Thermodynamic conditions

    // Compressor conditions
    comp_t_stag     : 320.0;
    comp_p_stag     : 8.65e+06;
    comp_t_in       : 320;
    comp_p_in       : 8.5e+06;
    comp_v_in       : 29.74;
    comp_t_in_wall  : 320;
    comp_t_out      : 350;
    comp_p_out      : 12.73e+06;
    comp_v_out      : 23.02;
    comp_t_out_wall : 350;
    comp_mdot       : 10;

    // Turbine conditions, scaled to match compressor/hx outlet
    turb_t_in       : 507.6;
    turb_p_in       : 1.27e+07;
    turb_v_in       : 55.6373;
    turb_t_in_wall  : 507.6;
    turb_t_out      : 470.261;
    turb_p_out      : 8.53591e+06;
    turb_v_out      : 76.5605;
    turb_t_out_wall : 470.261;
    turb_mdot       : 10;
    turb_er_design  : 1.5;
    turb_mfp_design : 1.785;
    turb_T0_design  : 500.0;
    turb_P0_design  : 12.53E6;

    // Recuperator inlet conditions
    recup_p_stag : 9.1E6;
    recup_t_low  : 350;
    recup_v_low  : 40.87; // Corresponds to t_low and p_in
    recup_t_in   : 576.85;
    recup_p_in   : 9E6;
    recup_v_in   : 93.96;

    // Molten salt inlet conditions
    t_in_salt  : 700;
    p_in_salt  : 8E6;
    v_in_salt  : 4.0;
}

// Pipe-sized cross section, used for both turbomachine plenums and all pipes
ChannelCrossSection: turbo_cross_section
{
    cross_area         : pipe_flow_area;
    heat_circumference : pipe_perimeter;
    hydraulic_diameter : pipe_dh;
}

// Cross-section of a PCHE flow channel
ChannelCrossSection: pche_cross_section
{
    cross_area         : pche_flow_area;
    heat_circumference : pche_perimeter;
    hydraulic_diameter : pche_dh;
    // roughness          : pche_roughness;
}

// Cross-section of a PCHE wall
WallCrossSection: pche_wall_cross_section
{
    // Reduced values
    width         : pche_wall_t1;
    thickness     : pche_wall_t2;

    // properties of stainless steel
    density       : 7700.0; // kg/m3
    conductivity  : 16.3; // W/m.K
    specific_heat : 510.7896; // J/kg.K
}

// For pipe walls
WallCrossSection: wall_cross_section
{
    width     : 0.1257;
    thickness : 0.01;

    // Properties of stainless steel
    conductivity  : 16.3; // W/m.K
    density       : 7700.0; // kg/m3
    specific_heat : 510.7896; // J/kg.K
}

Inflow: inflow
{
    node_model         : InflowFromStagnation;
    a_in               : pipe_flow_area;
    relax_factor       : 0.05;
    coolprop_fluid     : CO2;
    coolprop_backend   : BICUBIC&HEOS;

    state_transient    : comp_t_stag;
    massflow_transient : comp_mdot;

    initial_stagnation_pressure    : FromData(compressor_recuperator_nchans_steady/inflow/p_stag);
    initial_stagnation_temperature : FromData(compressor_recuperator_nchans_steady/inflow/t_stag);

    initial_temperature        : FromData(compressor_recuperator_nchans_steady/inflow/T);
    initial_pressure           : FromData(compressor_recuperator_nchans_steady/inflow/p);
    initial_velocity           : FromData(compressor_recuperator_nchans_steady/inflow/v);

    save: e, p, T, v, mdot, rho, p_stag, t_stag;
}

Pipe: inflow_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(compressor_recuperator_nchans_steady/inflow_pipe[0]/T);
    channel_initial_pressure    : FromData(compressor_recuperator_nchans_steady/inflow_pipe[0]/p);
    channel_initial_velocity    : FromData(compressor_recuperator_nchans_steady/inflow_pipe[0]/v);
    wall_initial_temperature    : FromData(compressor_recuperator_nchans_steady/inflow_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

MapCompressor: compressor
{
    cross_section : turbo_cross_section;
    plenum_length : 0.04;
    map           : compressor-data;
    speed         : Polynomial(1.0);
    stream        : flow_path; // Needs to match stream below

    initial_upstream_temperature   : FromData(compressor_recuperator_nchans_steady/compressor_inflow/T); 
    initial_upstream_pressure      : FromData(compressor_recuperator_nchans_steady/compressor_inflow/p); 
    initial_upstream_velocity      : FromData(compressor_recuperator_nchans_steady/compressor_inflow/v); 
    initial_downstream_temperature : FromData(compressor_recuperator_nchans_steady/compressor_outflow/T); 
    initial_downstream_pressure    : FromData(compressor_recuperator_nchans_steady/compressor_outflow/p); 
    initial_downstream_velocity    : FromData(compressor_recuperator_nchans_steady/compressor_outflow/v); 
    initial_mass_flow_rate         : comp_mdot;
}

Pipe: connector_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(compressor_recuperator_nchans_steady/connector_pipe[0]/T);
    channel_initial_pressure    : FromData(compressor_recuperator_nchans_steady/connector_pipe[0]/p);
    channel_initial_velocity    : FromData(compressor_recuperator_nchans_steady/connector_pipe[0]/v);
    wall_initial_temperature    : FromData(compressor_recuperator_nchans_steady/connector_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

// Recuperator definition
Define:
{
    recuperator_n_nodes          : 40;
    recuperator_length           : 1.0;
    // recuperator_n_chans          : 938;
    recuperator_incomp_period    : 0.004;
    recuperator_hot_coolprop_backend : INCOMP;
    recuperator_hot_coolprop_fluid   : NaK;
}
ParallelHE: recuperator
{
    orientation              : counterflow;
    channel[0]_cross_section : pche_cross_section;
    channel[1]_cross_section : pche_cross_section;
    wall_cross_section       : pche_wall_cross_section;
    nodes                    : recuperator_n_nodes;
    length                   : recuperator_length;
    n_channels               : pche_n_chans;
    name_convection[0]       : pche_convection_hot;
    name_convection[1]       : pche_convection_cold;

    channel[0]_initial_temperature : FromData(compressor_recuperator_nchans_steady/recuperator[0]/T);
    channel[0]_initial_pressure    : FromData(compressor_recuperator_nchans_steady/recuperator[0]/p);
    channel[0]_initial_velocity    : FromData(compressor_recuperator_nchans_steady/recuperator[0]/v);

    channel[1]_initial_temperature : FromData(compressor_recuperator_nchans_steady/recuperator[1]/T);
    channel[1]_initial_pressure    : FromData(compressor_recuperator_nchans_steady/recuperator[1]/p);
    channel[1]_initial_velocity    : FromData(compressor_recuperator_nchans_steady/recuperator[1]/v);

    wall_initial_temperature : FromData(compressor_recuperator_nchans_steady/recuperator[2]/T);

    save_channel[0] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_channel[1] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_wall       : T, heat_flux;
}

// Will flow to turbine in later jobfile
Pipe: recuperator_turbine_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(compressor_recuperator_nchans_steady/recuperator_turbine_pipe[0]/T);
    channel_initial_pressure    : FromData(compressor_recuperator_nchans_steady/recuperator_turbine_pipe[0]/p);
    channel_initial_velocity    : FromData(compressor_recuperator_nchans_steady/recuperator_turbine_pipe[0]/v);
    wall_initial_temperature    : FromData(compressor_recuperator_nchans_steady/recuperator_turbine_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

Outflow: outflow
{
    node_model          : OutflowToStagnation;
    coolprop_fluid      : CO2;
    coolprop_backend    : BICUBIC&HEOS;

    initial_temperature : FromData(compressor_recuperator_nchans_steady/outflow/T);
    initial_pressure    : FromData(compressor_recuperator_nchans_steady/outflow/p);
    initial_velocity    : FromData(compressor_recuperator_nchans_steady/outflow/v);

    save                : e, p, T, v, mdot, rho, p_stag;
}


//------------------------------------------------------------------------------
// Recuperator hot side

Inflow: recuperator_inflow
{
    node_model         : InflowFromStagnation;
    a_in               : pipe_flow_area;
    relax_factor       : 0.05;
    coolprop_fluid     : CO2;
    coolprop_backend   : BICUBIC&HEOS;

    state_transient    : 550.0;
    massflow_transient : comp_mdot;

    initial_stagnation_pressure    : FromData(compressor_recuperator_nchans_steady/recuperator_inflow/p_stag);
    initial_stagnation_temperature : FromData(compressor_recuperator_nchans_steady/recuperator_inflow/t_stag);

    initial_temperature        : FromData(compressor_recuperator_nchans_steady/recuperator_inflow/T);
    initial_pressure           : FromData(compressor_recuperator_nchans_steady/recuperator_inflow/p);
    initial_velocity           : FromData(compressor_recuperator_nchans_steady/recuperator_inflow/v);

    save: e, p, T, v, mdot, rho, p_stag, t_stag;
}

Outflow: recuperator_outflow
{
    // target_function : recup_p_in;
    node_model          : OutflowToStagnation;
    coolprop_fluid      : CO2;
    coolprop_backend    : BICUBIC&HEOS;

    initial_temperature : FromData(compressor_recuperator_nchans_steady/recuperator_outflow/T);
    initial_pressure    : FromData(compressor_recuperator_nchans_steady/recuperator_outflow/p);
    initial_velocity    : FromData(compressor_recuperator_nchans_steady/recuperator_outflow/v);

    save: e, p, T, v, mdot;
}

Stream: flow_path
{
    data : sCO2;

    // LP side
    inflow
    -> inflow_pipe[0]
    -> compressor_inflow;

    // HP side
    compressor_outflow
    -> connector_pipe[0]
    -> recuperator[1]
    -> recuperator_turbine_pipe[0]
    -> outflow;

    // Recuperator hot side
    recuperator_inflow
    -> recuperator[0]
    -> recuperator_outflow;
}


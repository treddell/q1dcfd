// run open_cycle/compressor_heater_turbine_mpc_testing.qdef
// Simulates the system
//                     outflow <-- |    | <-- inflow (salt)
//                                 |pche|
// (CO2) inflow --> compressor --> |    | --> turbine --> outflow
//
// Uses map-based turbomachinery models.

// PLOTTING CMDS:
// qseries T inflow inflow_pipe[0] compressor_inflow compressor_outflow compressor_heater_pipe[0] heater[1] heater_turbine_pipe[0] turbine_inflow turbine_outflow outflow_pipe[0] outflow -times=4
// qseries T incomp_inflow_boundary heater[0] incomp_outflow_boundary -times=4


Simulation: compressor_heater_turbine_mpc_test
{
    // end_time      : 0.2;
    end_time      : 12.0;
    max_CFL       : 1.2;
    steady_tol    : 2E5;
    save_interval : 1.0E-2;
}

Define:
{
    //------------------------------------------------------------------------------
    // GEOMETRY
    // Currently must use area-matched geometry (i.e. pipe and PCHE flow areas
    // are the same).
    pche_flow_area    :  7.853981633974482e-07;
    pche_perimeter    :  0.0031415926535897933;
    pche_dh           :  0.001;
    pche_n_chans      :  40000;
    pche_length       :  0.3;
    n_rows            :  200;
    n_cols            :  200;
    pche_wall_t1      :  0.0013;
    pche_wall_t2      :  0.0006;
    pipe_flow_area    :  0.031415926535897934;
    pipe_perimeter    :  0.6283185307179586;
    pipe_dh           :  0.2;

    //------------------------------------------------------------------------------
    // THERMODYNAMIC CONDITIONS
    // Compressor conditions
    comp_t_stag     : 320;
    comp_p_stag     : 8.65e+06;
    comp_t_in       : 320;
    comp_p_in       : 8.5e+06;
    comp_v_in       : 1.18988;
    comp_t_in_wall  : 320;
    comp_t_out      : 347.041;
    comp_p_out      : 1.19561e+07;
    comp_v_out      : 0.989689;
    comp_t_out_wall : 347.041;
    comp_mdot       : 10;
    comp_torque     : 83.118;
    comp_speed      : 1;

    // Turbine conditions, scaled to match compressor/hx outlet
    turb_t_in       : 550;
    turb_p_in       : 1.195e+07;
    turb_v_in       : 2.65185;
    turb_t_in_wall  : 550;
    turb_t_out      : 526.359;
    turb_p_out      : 9.23567e+06;
    turb_v_out      : 3.27227;
    turb_t_out_wall : 526.359;
    turb_mdot       : 10;
    // Turbine scaling
    turb_er_design  : 1.5;
    turb_mfp_design : 3.2;
    turb_T0_design  : 700.0;
    turb_P0_design  : 12.53E6;

    // Molten salt inlet conditions
    t_in_salt    : 700;
    p_in_salt    : 8E6;
    v_in_salt    : 0.40;
    mdot_in_salt : 20.0;
    t_wall_salt  : 620.0;
    // Pump parameters
    mdot_in_salt_max  : 50.0;
    dmdotdt_in_salt_max : 5.0;
}

// Pipe-sized cross section, used for both turbomachine plenums and all pipes
ChannelCrossSection: turbo_cross_section
{
    cross_area         : pipe_flow_area;
    heat_circumference : pipe_perimeter;
    hydraulic_diameter : pipe_dh;
}

// Cross-section of a PCHE flow channel
ChannelCrossSection: pche_cross_section
{
    cross_area         : pche_flow_area;
    heat_circumference : pche_perimeter;
    hydraulic_diameter : pche_dh;
    // roughness          : pche_roughness;
}

// Cross-section of a PCHE wall
WallCrossSection: pche_wall_cross_section
{
    // Reduced values
    width         : pche_wall_t1;
    thickness     : pche_wall_t2;

    // properties of stainless steel
    density       : 7700.0; // kg/m3
    conductivity  : 16.3; // W/m.K
    specific_heat : 510.7896; // J/kg.K
}

// For pipe walls
WallCrossSection: wall_cross_section
{
    width     : 0.1257;
    thickness : 0.01;

    // Properties of stainless steel
    conductivity  : 16.3; // W/m.K
    density       : 7700.0; // kg/m3
    specific_heat : 510.7896; // J/kg.K
}

Inflow: inflow
{
    node_model         : InflowFromStagnation;
    a_in               : pipe_flow_area;
    relax_factor       : 0.0;
    coolprop_fluid     : CO2;
    coolprop_backend   : BICUBIC&HEOS;

    state_transient    : comp_t_stag;
    massflow_transient : comp_mdot;

    initial_stagnation_pressure    : FromData(comp_heater_turb_open_cycle_steady/inflow/p_stag);
    initial_stagnation_temperature : FromData(comp_heater_turb_open_cycle_steady/inflow/t_stag);

    initial_temperature        : FromData(comp_heater_turb_open_cycle_steady/inflow/T);
    initial_pressure           : FromData(comp_heater_turb_open_cycle_steady/inflow/p);
    initial_velocity           : FromData(comp_heater_turb_open_cycle_steady/inflow/v);

    save: e, p, T, v, mdot, rho, p_stag, t_stag;
}

Pipe: inflow_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/inflow_pipe[0]/T);
    channel_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/inflow_pipe[0]/p);
    channel_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/inflow_pipe[0]/v);
    wall_initial_temperature    : FromData(comp_heater_turb_open_cycle_steady/inflow_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

MapCompressor: compressor
{
    cross_section : turbo_cross_section;
    plenum_length : 0.04;
    map           : compressor-data;
    stream        : flow_path; // Needs to match stream below
    rotor_inertia : 7.0E-4;
    // motor_torque  : comp_torque;

    initial_speed                  : FromData(comp_heater_turb_open_cycle_steady/compressor_node/speed);
    initial_motor_torque           : FromData(comp_heater_turb_open_cycle_steady/compressor_node/T_motor);
    initial_upstream_temperature   : FromData(comp_heater_turb_open_cycle_steady/compressor_inflow/T);
    initial_upstream_pressure      : FromData(comp_heater_turb_open_cycle_steady/compressor_inflow/p);
    initial_upstream_velocity      : FromData(comp_heater_turb_open_cycle_steady/compressor_inflow/v);
    initial_downstream_temperature : FromData(comp_heater_turb_open_cycle_steady/compressor_outflow/T);
    initial_downstream_pressure    : FromData(comp_heater_turb_open_cycle_steady/compressor_outflow/p);
    initial_downstream_velocity    : FromData(comp_heater_turb_open_cycle_steady/compressor_outflow/v);
    initial_mass_flow_rate         : FromData(comp_heater_turb_open_cycle_steady/compressor_node/mdot);

    save_plenum: e, p, T, v, mdot;
    save_turbomachine              : speed, mdot, delta_h, PR, w_sh, eta, p_in, p_out, T_in, T_out, T_load, T_motor;
}

Pipe: compressor_heater_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/compressor_heater_pipe[0]/T);
    channel_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/compressor_heater_pipe[0]/p);
    channel_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/compressor_heater_pipe[0]/v);
    wall_initial_temperature    : FromData(comp_heater_turb_open_cycle_steady/compressor_heater_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

// Heater definition
Define:
{
    heater_n_nodes          : 40;
    heater_incomp_period    : 0.004;
    heater_hot_coolprop_backend : INCOMP;
    heater_hot_coolprop_fluid   : NaK;
}
MixedHE: heater
{
    orientation              : counterflow;
    channel[0]_cross_section : pche_cross_section;
    channel[1]_cross_section : pche_cross_section;
    wall_cross_section       : pche_wall_cross_section;
    nodes                    : heater_n_nodes;
    length                   : pche_length;
    n_channels               : pche_n_chans;
    // name_convection[0]       : pche_convection_hot;
    name_convection[1]       : pche_convection_cold;

    channel[0]_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/heater[0]/T);
    channel[0]_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/heater[0]/p);
    channel[0]_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/heater[0]/v);

    channel[1]_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/heater[1]/T);
    channel[1]_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/heater[1]/p);
    channel[1]_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/heater[1]/v);

    wall_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/heater[2]/T);

    save_channel[0] : p, T, v, rho, e, mdot; // TODO: add Re for incomp chans
    // save_channel[0] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_channel[1] : p, T, v, rho, e, mdot, qdot_extern, re;
    save_wall       : T, heat_flux;
}

// Will flow to turbine in later jobfile
Pipe: heater_turbine_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 10;
    length : 0.2;

    channel_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/heater_turbine_pipe[0]/T);
    channel_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/heater_turbine_pipe[0]/p);
    channel_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/heater_turbine_pipe[0]/v);
    wall_initial_temperature    : FromData(comp_heater_turb_open_cycle_steady/heater_turbine_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

MapTurbine: turbine
{
    cross_section  : turbo_cross_section;
    plenum_length  : 0.04;
    map            : turbine-data;
    coolprop_fluid : CO2;
    stream         : flow_path; // Ideally, this automatically populate

    initial_upstream_temperature   : FromData(comp_heater_turb_open_cycle_steady/turbine_inflow/T);
    initial_upstream_pressure      : FromData(comp_heater_turb_open_cycle_steady/turbine_inflow/p);
    initial_upstream_velocity      : FromData(comp_heater_turb_open_cycle_steady/turbine_inflow/v);
    initial_downstream_temperature : FromData(comp_heater_turb_open_cycle_steady/turbine_outflow/T);
    initial_downstream_pressure    : FromData(comp_heater_turb_open_cycle_steady/turbine_outflow/p);
    initial_downstream_velocity    : FromData(comp_heater_turb_open_cycle_steady/turbine_outflow/v);
    initial_speed                  : FromData(comp_heater_turb_open_cycle_steady/turbine_node/speed);

    er_design  : turb_er_design;
    mfp_design : turb_mfp_design;
    T0_design  : turb_T0_design;
    P0_design  : turb_P0_design;

    save_plenum: e, p, T, v, mdot;
    save_turbomachine : speed, mdot, delta_h, PR, w_sh, eta, p_in, p_out, T_in, T_out, T_load;
}

Pipe: outflow_pipe
{
    channel_cross_section : turbo_cross_section;
    wall_cross_section    : wall_cross_section;
    nodes  : 20;
    length : 0.2;

    channel_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/outflow_pipe[0]/T);
    channel_initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/outflow_pipe[0]/p);
    channel_initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/outflow_pipe[0]/v);
    wall_initial_temperature    : FromData(comp_heater_turb_open_cycle_steady/outflow_pipe[1]/T);

    save_channel: e, p, T, v, mdot, rho;
}

Outflow: outflow
{
    node_model          : OutflowToStagnation;
    coolprop_fluid      : CO2;
    coolprop_backend    : BICUBIC&HEOS;

    initial_temperature : FromData(comp_heater_turb_open_cycle_steady/outflow/T);
    initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/outflow/p);
    initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/outflow/v);

    save                : e, p, T, v, mdot, rho, p_stag;
}


//------------------------------------------------------------------------------
// Salt stream

Inflow: incomp_inflow_boundary
{
    node_model          : IncompressiblePump;
    piso_period         : 0.005;
    coolprop_backend    : INCOMP; // CoolProp fluid backend
    coolprop_fluid      : NaK;    // CoolProp fluid name
    a_in                : pipe_flow_area;

    mdot_max            : mdot_in_salt_max;
    mdot_dot_max        : dmdotdt_in_salt_max;

    state_transient     : t_in_salt; // Temperature, K
    // massflow_transient  : mdot_in_salt;

    initial_temperature : FromData(comp_heater_turb_open_cycle_steady/incomp_inflow_boundary/T);
    initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/incomp_inflow_boundary/p);
    initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/incomp_inflow_boundary/v);

    save                : p, e, T, v, rho, mdot;
}

Outflow: incomp_outflow_boundary
{
    node_model          : IncompressibleOutflow;

    initial_temperature : FromData(comp_heater_turb_open_cycle_steady/incomp_outflow_boundary/T);
    initial_pressure    : FromData(comp_heater_turb_open_cycle_steady/incomp_outflow_boundary/p);
    initial_velocity    : FromData(comp_heater_turb_open_cycle_steady/incomp_outflow_boundary/v);

    save                : p, e, T, v, rho, mdot;
}

PisoSolver: salt_solver
{
    cross_section : pche_cross_section;
    nodes         : heater_n_nodes;
    n_channels    : pche_n_chans;
    length        : pche_length;
    piso_period   : 0.005;
    coolprop_backend : INCOMP; // CoolProp fluid backend
    coolprop_name    : NaK;    // CoolProp fluid name

    initial_temperature      : FromData(comp_heater_turb_open_cycle_steady/heater[0]/T);
    initial_pressure         : FromData(comp_heater_turb_open_cycle_steady/heater[0]/p);
    initial_velocity         : FromData(comp_heater_turb_open_cycle_steady/heater[0]/v);
    wall_initial_temperature : FromData(comp_heater_turb_open_cycle_steady/heater[2]/T);
}

Stream: flow_path
{
    data : sCO2;

    // Compressor LP side
    inflow
    -> inflow_pipe[0]
    -> compressor_inflow;

    // HP side
    compressor_outflow
    -> compressor_heater_pipe[0]
    -> heater[1]
    -> heater_turbine_pipe[0]
    -> turbine_inflow;

    // Turbine LP side
    turbine_outflow
    -> outflow_pipe[0]
    -> outflow;

    // Heater is incompressible (handled below)
}

// Molten salt stream
connect-piso-solver:
{
    salt_solver -> heater[0];
}
connect-incompressible-boundaries:
{
    incomp_inflow_boundary -> salt_solver -> incomp_outflow_boundary;
}


//------------------------------------------------------------------------------
// MPC

Controller: controller
{
    control_model      : StateSpaceController;
    controller_jobfile : mpc_comp_heater_turb_salt_test;
    n_controls         : 2;  // matches number of actuators
    n_measurements     : 46; // matches number of sensors
    // controller_dt      : 0.2;
    controller_dt      : 0.1;

    // // Testdrive the surge constraint (should stop it before 7)
    // ref_traj_0         : Polynomial(7.0); // mdot process stream
    // ref_traj_4         : Polynomial(630000); // turbine inlet e

    // // Dynamic case
    // ref_traj_0         : Step(10.0, 12.0, 4.0); // mdot process stream
    // ref_traj_4         : Step(630000, 600000, 4.0); // turbine inlet e

    // Constant setpoint
    ref_traj_0         : Polynomial(10.0); // mdot process stream
    ref_traj_4         : Polynomial(630000); // turbine inlet e

    // Untracked outputs
    ref_traj_1         : Polynomial(0.0); // compressor speed
    ref_traj_2         : Polynomial(0.0); // compressor torque
    ref_traj_3         : Polynomial(0.0); // salt outlet e

    u_0                : 0.0;   // input @ port 0 (compressor motor torque DERIVATIVE)
    u_1                : 28.0;  // input @ port 1 (mdot_target salt)
}

// Control inputs
control-motor-torque: {
    port: 0; // compressor
    controller -> compressor_node;
}
control-flow-in: {
    port: 1; // salt pump
    controller -> incomp_inflow_boundary;
}

//--------------------------------------
// CO2 Stream sensors

// Compressor
controller-input: {
    port: 0;
    input_variable: p_in;
    compressor_node -> controller;
}
controller-input: {
    port: 1;
    input_variable: T_in;
    compressor_node -> controller;
}
controller-input: {
    port: 2;
    input_variable: p_out;
    compressor_node -> controller;
}
controller-input: {
    port: 3;
    input_variable: T_out;
    compressor_node -> controller;
}
controller-input: {
    port: 4;
    input_variable: mdot;
    compressor_node -> controller;
}
controller-input: {
    port: 5;
    input_variable: speed;
    compressor_node -> controller;
}
controller-input: {
    port: 6;
    input_variable: T_load;
    compressor_node -> controller;
}
controller-input: {
    port: 42;
    input_variable: T_motor;
    compressor_node -> controller;
}
controller-input: {
    port: 43;
    input_variable: dT_motor_dt;
    compressor_node -> controller;
}
controller-input: {
    port: 44;
    input_variable: eta;
    compressor_node -> controller;
}

// Turbine
controller-input: {
    port: 7;
    input_variable: p_in;
    turbine_node -> controller;
}
controller-input: {
    port: 8;
    input_variable: T_in;
    turbine_node -> controller;
}
controller-input: {
    port: 9;
    input_variable: p_out;
    turbine_node -> controller;
}
controller-input: {
    port: 10;
    input_variable: T_out;
    turbine_node -> controller;
}
controller-input: {
    port: 11;
    input_variable: mdot;
    turbine_node -> controller;
}
controller-input: {
    port: 12;
    input_variable: speed;
    turbine_node -> controller;
}
controller-input: {
    port: 13;
    input_variable: T_load;
    turbine_node -> controller;
}
controller-input: {
    port: 45;
    input_variable: eta;
    turbine_node -> controller;
}

// NOTE: We treat compressor outlet plenum and the connected pipe as a single entity
// Compressor to hx pipe (compressor_outflow then compressor_heater_pipe)
controller-input: {
    port: 14;
    input_variable: p;
    node_id: 0;
    compressor_outflow -> controller;
}
controller-input: {
    port: 15;
    input_variable: h;
    node_id: 0;
    compressor_outflow -> controller;
}
controller-input:
{
    port: 16;
    input_variable: T;
    node_id: 0;
    compressor_outflow -> controller;
}
controller-input:
{
    port: 17;
    input_variable: mdot;
    compressor_node -> controller;
    // // mdot is lazily evaluated in FluidNode cells so must currently use value
    // // from compressor node
    // node_id: 0;
    // compressor_outflow -> controller;
}
controller-input: {
    port: 18;
    input_variable: e;
    node_id: -1;
    compressor_heater_pipe[0] -> controller;
}
controller-input:
{
    port: 19;
    input_variable: T;
    node_id: -1;
    compressor_heater_pipe[0] -> controller;
}

// Heater cold CO2 side
controller-input: {
    port: 20;
    input_variable: p;
    node_id: 0;
    heater[1] -> controller;
}
controller-input: {
    port: 21;
    input_variable: h;
    node_id: 0;
    heater[1] -> controller;
}
controller-input: {
    port: 22;
    input_variable: T;
    node_id: 0;
    heater[1] -> controller;
}
controller-input: {
    port: 23;
    input_variable: mdot;
    compressor_node -> controller;
    // // mdot is lazily evaluated in FluidNode cells so must currently use value
    // // from compressor node
    // node_id: 0;
    // heater[1] -> controller;
}
controller-input: {
    port: 24;
    input_variable: e;
    node_id: -1;
    heater[1] -> controller;
}
controller-input:
{
    port: 25;
    input_variable: T;
    node_id: -1;
    heater[1] -> controller;
}

// Heater to turbine pipe (turbine_inflow)
// Inlet
controller-input: {
    port: 26;
    input_variable: p;
    node_id: 0;
    heater_turbine_pipe[0] -> controller;
}
controller-input: {
    port: 27;
    input_variable: h;
    node_id: 0;
    heater_turbine_pipe[0] -> controller;
}
controller-input: {
    port: 28;
    input_variable: T;
    node_id: 0;
    heater_turbine_pipe[0] -> controller;
}
controller-input: {
    port: 29;
    input_variable: mdot;
    compressor_node -> controller;
    // // mdot is lazily evaluated in FluidNode cells so must currently use value
    // // from compressor node
    // node_id: 0;
    // heater_turbine_pipe[0] -> controller;
}
// Outlet
controller-input: {
    port: 30;
    input_variable: e;
    node_id: -1;
    turbine_inflow -> controller;
}
controller-input: {
    port: 31;
    input_variable: T;
    node_id: -1;
    turbine_inflow -> controller;
}

//--------------------------------------
// Hot CO2 stream and attached pump sensors 
// Stream
controller-input: { // also used for hot pump
    port: 32;
    input_variable: p;
    incomp_inflow_boundary -> controller;
}
controller-input: {
    port: 33;
    input_variable: h;
    incomp_inflow_boundary -> controller;
}
controller-input: { // also used for hot pump
    port: 34;
    input_variable: T;
    incomp_inflow_boundary -> controller;
}
controller-input: { // also used for hot pump
    port: 35;
    input_variable: mdot;
    incomp_inflow_boundary -> controller;
}
controller-input: {
    port: 36;
    input_variable: e;
    incomp_outflow_boundary -> controller;
}
controller-input: {
    port: 37;
    input_variable: T;
    incomp_outflow_boundary -> controller;
}

// Salt pump (some sensors duplicated, but much easier to handle this way)
controller-input: {
    port: 38;
    input_variable: e;
    incomp_inflow_boundary -> controller;
}
controller-input: {
    port: 39;
    input_variable: mdot_pump;
    incomp_inflow_boundary -> controller;
}
controller-input: {
    port: 40;
    input_variable: mdot_dot_pump;
    incomp_inflow_boundary -> controller;
}
controller-input: {
    port: 41;
    input_variable: mdot_dot_dot_pump;
    incomp_inflow_boundary -> controller;
}


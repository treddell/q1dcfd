module q1dcfd.control_volumes.outflow_to_stagnation;

import q1dcfd.simulation: Simulation;
import q1dcfd.control_volumes.fluid_node: FluidNode;
import q1dcfd.tables.extract_table_data: TableData;
import q1dcfd.config: Real, Transient, Address;
import q1dcfd.utils.math: square;
import q1dcfd.types.geometry: NodeGeometry;
import q1dcfd.utils.metaprogramming: generate_save_method, PopulateStateFields,
    PopulateStateMap;
import q1dcfd.stream: Stream;
import q1dcfd.eos: generate_eos, backend, EOSBase ;

import std.format;
import std.math: fabs, fmin, fmax;
import std.conv: to;
import std.stdio;

import coolprop;

/*
 * Computes outflow boundary conditions as if the flow was flowing from the
 * exit plane into some reservoir at specified stagnation conditions.
 * Sets fluid state in ghost cell just after the outlet (rather than setting
 * flux
 * values).
 * Based on PJ's 'from_stagnation' subsonic inflow boundary condition for
 * Eilmer.
 *
 * Max Gains, 2019
 */
final class OutflowToStagnation : FluidNode
{
    // Generate the property fields
    enum STAG_FIELDS = ["p_stag"];
    enum STATE_FIELDS = FluidNode.STATE_FIELDS ~ STAG_FIELDS;
    mixin(PopulateStateFields!(STAG_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    AbstractState EosCp;

    // Number of independent ODEs, 0 for this BC
    static uint order() { return 0; }

    Real v1, rho1, h1; // Properties in first cell
    Real v_out, rho_out, h_out; // Outlet props

    this(const Address address, Stream stream, backend backend_model,
         Real initial_p_stag, Real initial_t_stag, Transient initial_v,
         const string coolprop_backend, const string coolprop_fluid)
    {
        super(address, stream, NodeGeometry(), "NoFriction", "NoHeatTransfer",
              false, // thin-film correlations n/a for outflow
              1,     // n_channels n/a for outflow
              backend_model);

        this.p_stag = initial_p_stag;

        if(coolprop_fluid == "none") {
            throw new Exception("Need to specify CoolProp fluid for 'OutflowToStagnation' "
                                ~ "boundary.");
        }
        this.EosCp = new AbstractState(coolprop_backend, coolprop_fluid);

        // Set other conditions from specified (p,T)
        EosCp.update(PT_INPUTS, p_stag, initial_t_stag);

        // For initialisation, assume props in adjacent cell are same as in this cell
        EosCp.update(PT_INPUTS, p_stag, initial_t_stag);
        this.rho_out = EosCp.rhomass;
        this.h_out   = EosCp.hmass;
        this.v_out   = initial_v(0);
    }

    override @property string type() { return "OutflowToStagnation"; }

    override void update(const ref Real[] state, const Real t)
    {
        Real actual_mass_flux = rho_out * v_out;

        // Assume zero velocity gradient at the inlet
        v = v_out;
        // Block any inflow with stagnation condition
        if (actual_mass_flux < 0.0) { v = 0.0; }

        // Assume 0% isentropic process with ghost-cell enthalpy set by flow enthalpy
        h = h_out;
        EosCp.update(HmassP_INPUTS, h, p_stag);
        s = EosCp.smass;

        // Use the EOS to set rho and e
        rho = EosCp.rhomass;
        e   = EosCp.umass;
        ke  = 0.5*v.square;
        E   = e + ke;

        // Update tabular eos and use to set other props
        eos.update(rho, e);
        p   = eos.p;
        T   = eos.T;
        cp  = eos.cp;
        dv  = eos.dv;
        k   = eos.k;
        a   = eos.a;

        cons_state[0] = rho;
        cons_state[1] = rho*v;
        cons_state[2] = rho*E;
        dm = cons_state[1];

        M = fabs(v)/a;
        H = h + ke;
    }

    override void update_balance(ref Real[] balance)
    {
        rho_out = rho1;
        h_out = h1;
        v_out = v1;
    }

    override void check_well_posedness()
    {
        super.check_well_posedness;
        assert(v < a, format!("Supersonic Flow in (%s). This node model "
            ~"is only well defined for subsonic flow")(identify));
    }
}




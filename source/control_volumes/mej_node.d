module q1dcfd.control_volumes.mej_node ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.control_volumes.node: Node ;
import q1dcfd.control_volumes.outflow_node: OutflowNode ;
import q1dcfd.control_volumes.inflowDV: InflowDV ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.utils.metaprogramming: PopulateStateFields, PopulateStateMap ;
import q1dcfd.utils.math: square ;
import q1dcfd.config: Real, Address, Transient ;
import q1dcfd.correlations.maps: EfficiencyMap ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: backend, generate_eos, EOSBase ;

import std.format ;

/**
 * MomentumEnergyJumpNode
 * 
 *  A momemtum and energy discontinuity linking two boundary nodes
 *
 *   ---> g(t)
 *   ---------               --------- rho(e + 1/2 v^2)
 *   |       | ---> F(X)---> |       | <----
 *   |       | --->     ---> |       |
 *   ---------               ---------
 *   upstream                downstream
 *
 *   Defines the relationship between upstream and downstream states F(X)
 *
 *   F(X) =  { h_d = eta*(hds - h_u) + h_u
 *   { (rho v)_d = (rho v)_u
 *
 *   Where hds = h(p_d, s_u)
 *
 *   The known variables are h_u, s_u, (rhoE)_d it is further assumed that eta is 
 *   calculated semi implicitly at the current time state (fully implicitly with
 *   respect to the upstream state, and semi implicitly with respect to the down-
 *   stream state).
 *
 *   Hence, the following equations are solved implicitly for rho_d, e_d
 *
 *   (rhoE)_d = rho_d*(e_d + 1/2 (rho v)_u^2 /rho_d)
 *   h_d(rho_d, e_d) = eta(X_u, X_d) * (h(p(rho_d, e_d), s_u ) - h_u) + h_u
 *
 *   Then v_d, d(rho_d)/dt, d(v_d)/dt are calculated from these known properties
 */
class MomentumEnergyJumpNode: Node
{
    // generate property fields
    enum STATE_FIELDS = [
        "eta",     // effective 'efficiency'
        "delta_h",  // delta enthalpy (J/kg)
        "delta_H", // delta total enthalpy (J/kg)
        "mdot", // mass flow rate (kg/s)
        "power" // total power out (W)
    ] ;

    mixin(PopulateStateFields!(["eta"])) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    // 'efficiency' map, determines the entropy generation across the discontinuity
    EfficiencyMap calculate_efficiency ;

    // the shaft speed boundary condition
    Transient shaft_speed ;

    // (rho,e) based equation of state calculator
    EOSBase conservative_eos ;

    // (p,s) based equation of state calculator
    EOSBase isentropic_eos ;

    // density, velocity and rates extrapolated over the discontinuity
    Real rho_extrapolated ;
    Real v_extrapolated ;
    Real drhodt_extrapolated ;
    Real dvdt_extrapolated ;

    Real[2] cons_vars ;

    // type tag
    override @property string type() {return "MomentumEnergyJumpNode" ;}

    // upstream and downstream sub-states
    OutflowNode upstream ; 
    InflowDV downstream ;

    // Has no independent ODE's
    static uint order() {return 0 ;}

    @property
    {
        // static specific enthalpy difference across the discontinuity (J/kg)
        Real delta_h() {return upstream.h - downstream.h ;}

        // total specific enthalpy difference across the discontinuity (J/kg)
        Real delta_H() {return upstream.H - downstream.H ;}

        // conserved mass flow rate across the discontinuity (kg/s)
        Real mdot() {return upstream.mdot ;}

        // net power change across the discontinuity (W)
        Real power() {return delta_H*mdot ;}
    }

    this(const Address address,
         Stream stream,
         EfficiencyMap calculate_efficiency,
         Transient shaft_speed,
         backend backend_model)
    {
        super(address, NodeGeometry()) ;

        this.conservative_eos = generate_eos(stream.conservative_data,
                                             backend_model,
                                             identify) ;

        this.isentropic_eos = generate_eos(stream.isentropic_data,
                                           backend_model,
                                           identify) ;

        this.calculate_efficiency = calculate_efficiency ;
        this.shaft_speed = shaft_speed ;
    }

    override void update(const ref Real[] state, const Real t)
    out 
    {
        check_well_posedness ;
    }
    do
    {
        import q1dcfd.utils.convert_fluid_props: cons_from_pressure_step ;

        eta = calculate_efficiency(downstream, upstream, shaft_speed(t)) ;

        Real rhoE = state[downstream.index_l] ;

        cons_from_pressure_step(conservative_eos, isentropic_eos, cons_vars,
                                upstream.dm, upstream.h, upstream.s, rhoE, eta) ;

        rho_extrapolated = cons_vars[0] ;
        v_extrapolated = upstream.rho*upstream.v/cons_vars[0] ;

        drhodt_extrapolated = (rho_extrapolated - downstream.rho)/Simulation.dt ;

        dvdt_extrapolated = (v_extrapolated - downstream.v)/Simulation.dt ;
    }

    override void update_balance(ref Real[] balance) {}
}

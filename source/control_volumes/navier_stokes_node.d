module q1dcfd.control_volumes.navier_stokes_node ;

import std.math: fabs ;
import std.format ;
import std.conv: to ;
import std.stdio ;

import q1dcfd.control_volumes.fluid_node: FluidNode;
import q1dcfd.simulation: Simulation ;
import q1dcfd.domains.domain: Domain ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.utils.math: square, sign ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.utils.metaprogramming: PopulateStateFields,PopulateStateMap ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: backend ;

/**
 * InternalNode
 *
 * Base class for internal fluid node models
 * 
 * Single phase, quasi 1 dimensional nodel model. Fluid properties are 
 * calculated dynamically from conservation of mass, momentum and energy.
 * Viscous losses are accounted for via bulk correlations.
 */
abstract class InternalNode: FluidNode
{
    // generate state property getter/setters
    enum STATE_FIELDS = FluidNode.STATE_FIELDS ;
    enum FIELDS = STATE_FIELDS ;

    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    static uint order() {return 3 ;} // number of independent odes

    // Lazily evaluated properties, do not appear in update equations, but may
    // be referenced by other nodes or write out state
    override @property
    {
        Real dt_density() {return balance(0) ;}
        Real dt_momentum() {return balance(1) ;}
        Real dt_energy() {return balance(2) ;}
        Real dvdt() {return (dt_momentum - v*dt_density)/rho ;}
        Real dedt() {return (dt_energy - dt_density*E)/rho -v*dvdt ;}
        Real x() { return eos.x ;}
        Real s() {return eos.s ;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt ;}
        Real dpdt() {return dt_density*(h - e) - rho*dedt ;}
        Real F0l() { return flux.left[0]*n_channels ;}
        Real F0r() { return flux.right[0]*n_channels ;}
        Real F1l() { return flux.left[1]*n_channels ;}
        Real F1r() { return flux.right[1]*n_channels ;}
        Real F2l() { return flux.left[2]*n_channels ;}
        Real F2r() { return flux.right[2]*n_channels ;}
        Real mass() {return rho*geometry.volume*n_channels ;}
        Real mdot() {return 0.5*(flux.left[0] + flux.right[0])*n_channels ;}
        Real M() {return v/a ;}
        Real shear() {return internal_source[1]*n_channels ;}
        Real p_loss() {return internal_source[2]*geometry.volume*n_channels ;}
        Real nu() {return htc*geometry.length/k ;}
        Real qdot_extern() {return flux.extern_heat*n_channels ;}
    }

    this(const Address address,
         Stream stream, 
         const NodeGeometry geometry, 
         const string f_corr_name, 
         const string ht_corr_name, 
         const bool thin_film_heat_transfer, 
         const uint n_channels,
         backend backend_model)
    {
        super(address, stream, geometry, f_corr_name, ht_corr_name, 
              thin_film_heat_transfer, n_channels, backend_model);

        // No external mass flow
        this.internal_source[0] = 0.0 ;
    }

    /**
     * updates the equations of state calculator from conservative state 
     * variables (rho, rho*v, rho*E) and updates all internal and external
     * source terms
     */
    override void update(const ref Real[] state, const Real t)
    out
    {
        check_well_posedness ;
    }
    do
    {
        // zero fluxes
        flux.reset ;

        cons_state[] = state[index_l..index_r] ;

        rho = cons_state[0] ;
        vol = 1.0/rho ;
        v = cons_state[1]*vol ;
        ke = 0.5*v.square ;
        e = cons_state[2]*vol - ke ;
        dm = cons_state[1] ;

        eos.update(rho, e) ;
      
        h = eos.h ;
        T = eos.T ;
        cp = eos.cp ;
        dv = eos.dv ;
        k = eos.k ;
        a = eos.a ;

        p = (h - e)*rho ;
        E = e + ke ;
        H = h + ke ;

        pr = dv*cp/k ;
        re = rho*fabs(v)*geometry.hydr_d/dv ;
        f = f_corr.evaluate() ;

        internal_source[1] = -sign(v)*f*rho*ke/geometry.hydr_d ;
        internal_source[2] = internal_source[1]*fabs(v) ;

        critical_dt = geometry.length/(fabs(v) + a) ;
    }

    /**
     * Updates the nodal balance equations as the vector dU/dt
     * representing conservation of mass momentum and energy
     */
    override void update_balance(ref Real[] balance) 
    {
        balance[index_l..index_r] =
            - (flux.right[] - flux.left[])*geometry.specific_density 
            + internal_source[] ;

        // Add in external heat transfer
        balance[index_l + 2] += flux.extern_heat*geometry.specific_density ;
    }

    override void check_well_posedness()
    {
        import std.math: isNaN ;
        super.check_well_posedness ;
        assert(!(x > 0 && x < 1),format!("Two Phase Fluid in (%s), "
            ~"NavierStokesNode is only valid for single phase flow")(identify));
        assert(cp > 0 && !cp.isNaN, format!("Invalid specific heat coefficient "
            ~"%f in %s at rho=%f, e=%f")(cp, identify, rho, e)) ;
        assert(dv > 0 && !dv.isNaN, format!("Invalid dynamic viscosity "
            ~"%f in %s at rho=%f, e=%f")(dv, identify, rho, e)) ;
        assert(k > 0 && !k.isNaN, format!("Invalid thermal conductivity "
            ~"%f in %s at rho=%f, e=%f")(k, identify, rho, e)) ;
        assert(f >= 0 && !f.isNaN, format!("Invalid friction factor "
            ~"%f in %s at rho=%f, e=%f")(f, identify, rho, e)) ;
        assert(re >= 0 && !re.isNaN, format!("Invalid Reynolds number "
            ~"%f in %s at rho=%f, e=%f")(re, identify, rho, e)) ;
    }

    override string describe()
    {
        return (identify ~ " channels: %u, left_area: %f, right_area: %f volume: %f,"
                ~" hydraulic_diameter: %f, heat_area: %f, surface_roughness: %f, length: %f")
            .format(n_channels, geometry.lA, geometry.rA, geometry.volume, geometry.hydr_d,
                    geometry.htA, geometry.delta, geometry.length) ;
    }
}

/**
 * NavierStokesNode
 * 
 * Run of the mill internal viscous node model. Simply implements the
 * InternalNode base class
 */
final class NavierStokesNode: InternalNode 
{
    override @property string type() {return "NavierStokesNode" ;}

    this(const Address address,
         Stream stream,
         const NodeGeometry geometry, 
         const string f_corr_name, 
         const string ht_corr_name, 
         const bool thin_film_heat_transfer, 
         const uint n_channels,
         backend backend_model)
    {
        super(address, stream, geometry, f_corr_name, ht_corr_name, 
              thin_film_heat_transfer, n_channels, backend_model) ;
    }
}

/** 
 * An internal control volume, which, receives mass flow from an external 
 * source
 */
final class ExternalValveNode: InternalNode
{
    // generate property fields
    enum UNIQUE_FIELDS = ["mdot_external"] ;
    enum STATE_FIELDS = UNIQUE_FIELDS ~ NavierStokesNode.STATE_FIELDS ;
    mixin(PopulateStateFields!(UNIQUE_FIELDS)) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    override @property string type() {return "ExternalValveNode" ;}

    // external mass flow rate (kg/s) entering the volume, as a function of time
    Transient mass_flow_rate ;

    this(const Address address,
         Stream stream,
         const NodeGeometry geometry, 
         const string f_corr_name, 
         const string ht_corr_name, 
         const bool thin_film_heat_transfer, 
         const uint n_channels,
         backend backend_model)
    {
        super(address, stream, geometry, f_corr_name, ht_corr_name, 
              thin_film_heat_transfer, n_channels, backend_model) ;
    }

    // runs the standard InternalNode update, but also sets an external
    // mass flow rate
    override void update(const ref Real[] state, const Real t)
    {
        super.update(state, t);
        mdot_external = mass_flow_rate(t) ;
    }

    // adds the external mass flow rate to the d(rho)/dt term
    override void update_balance(ref Real[] balance)
    {
        super.update_balance(balance) ;
        balance[index_l] += mdot_external*geometry.specific_density ;
    }
}

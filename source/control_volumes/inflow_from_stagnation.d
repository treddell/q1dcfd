module q1dcfd.control_volumes.inflow_from_stagnation;

import q1dcfd.simulation: Simulation;
import q1dcfd.control_volumes.fluid_node: FluidNode;
import q1dcfd.tables.extract_table_data: TableData;
import q1dcfd.config: Real, Transient, Address;
import q1dcfd.utils.math: square;
import q1dcfd.types.geometry: NodeGeometry;
import q1dcfd.utils.metaprogramming: generate_save_method, PopulateStateFields,
    PopulateStateMap;
import q1dcfd.stream: Stream;
import q1dcfd.eos: generate_eos, backend, EOSBase ;

import std.format;
import std.math: fabs, fmin, fmax;
import std.conv: to;
import std.stdio;

import coolprop;

/*
 * Computes inflow boundary conditions as if they were flowing from a reservoir
 * at stagnation conditions into the computational domain.
 * Sets fluid state in ghost cell just before inlet (rather than setting flux
 * values).
 * Based on PJ's 'from_stagnation' subsonic boundary condition for Eilmer.
 *
 * Max Gains, 2019
 */
class InflowFromStagnation : FluidNode
{
    // Generate the property fields
    enum STAG_FIELDS = ["p_stag", "t_stag", "h_stag", "s_stag"];
    enum STATE_FIELDS = FluidNode.STATE_FIELDS ~ STAG_FIELDS;
    mixin(PopulateStateFields!(STAG_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    AbstractState EosCp;

    // User-defined inlet transients
    Transient temperature_transient;
    Transient massflow_transient;

    // TODO: Better to use mass flux transient (scaling)?

    // Number of independent ODEs, 0 for this BC
    static uint order() { return 0; }

    Real relax_factor;
    Real p_stag_min, p_stag_max; // Limits when adjusting stagnation conditions
    Real p1, v1, rho1; // Properties in first cell
    Real a_in; // Inlet flow area, needed since we use a massflow transient

    this(
        const Address address,
        Stream stream,
        backend backend_model,
        Real initial_p_stag,
        Real initial_t_stag,
        Transient initial_v,
        const string coolprop_backend,
        const string coolprop_fluid,
        const Real a_in,
        const Real relax_factor=0.05)
    {
        super(address, stream, NodeGeometry(), "NoFriction", "NoHeatTransfer",
              false, 1, backend_model);

        this.a_in = a_in;

        this.p_stag = initial_p_stag;
        this.t_stag = initial_t_stag;
        this.p_stag_min = 0.1 * p_stag;
        this.p_stag_max = 10.0 * p_stag;
        this.relax_factor = relax_factor;
        if(coolprop_fluid == "none") {
            throw new Exception("Need to specify CoolProp fluid for "
                    ~ "'InflowFromStagnation' boundary.");
        }
        this.EosCp = new AbstractState(coolprop_backend, coolprop_fluid);

        // For initialisation, assume props in adjacent cell are same as in this
        // cell
        EosCp.update(PT_INPUTS, p_stag, t_stag);
        this.p1   = p_stag;
        this.rho1 = EosCp.rhomass;
        this.v1   = initial_v(0);

        // Initialise other parameters
        v    = v1;
        mdot = a_in * rho1 * v1;
        update_fluid_node_properties(EosCp); // EosCp must be set to node state
    }

    override @property string type() { return "InflowFromStagnation"; }

    override void update(const ref Real[] state, const Real t)
    {
        t_stag = temperature_transient(t);
        EosCp.update(PT_INPUTS, p_stag, temperature_transient(t));

        // Massflow transient
        Real target_mdot = calculate_target_massflow(t);
        Real target_mass_flux = target_mdot / a_in;
        Real target_rho = EosCp.rhomass;
        Real actual_mass_flux = rho1 * v1;
        mdot = actual_mass_flux * a_in;

        if (target_mass_flux > 0.0)
        {
            // Adjust stagnation pressure to move toward the target mass flux
            Real dp_over_p = relax_factor * 0.5 / rho1 * 
                (target_mass_flux.square - actual_mass_flux*fabs(actual_mass_flux)) / p;
            Real new_p_stag = (1.0 + dp_over_p) * p_stag;
            new_p_stag = fmin(fmax(new_p_stag, p_stag_min), p_stag_max);
            p_stag = new_p_stag;
            EosCp.update(PT_INPUTS, p_stag, temperature_transient(t));
            h_stag = EosCp.hmass;
            s_stag = EosCp.smass;
        }
        // Assume zero velocity gradient at the inlet
        v = v1;

        // Block any outflow with stagnation condition
        if (actual_mass_flux < 0.0) { v = 0.0; }

        // Assume an isentropic process from a known total enthalpy to set ghost
        // cell enthalpy
        h = h_stag - 0.5 * v.square;
        // Set EOS using (p, h) inputs so that TTSE backend can be used
        set_coolprop_eos_from_h_s(EosCp, h, s_stag, p);
        update_fluid_node_properties(EosCp); // EosCp must be set to node state
    }

    override void update_balance(ref Real[] balance) { }

    override void check_well_posedness()
    {
        super.check_well_posedness;
        assert(v < a, format!("Supersonic Flow in (%s). This node model "
            ~"is only well defined for subsonic flow")(identify));
    }

    /* EosCp must be set to node state before calling this function */
    void update_fluid_node_properties(AbstractState EosCp)
    {
        rho  = EosCp.rhomass;
        e    = EosCp.umass;
        ke   = 0.5*v.square;
        E    = e + ke;

        eos.update(rho, e);
        p   = eos.p;
        T   = eos.T;
        cp  = eos.cp;
        dv  = eos.dv;
        k   = eos.k;
        a   = eos.a;

        cons_state[0] = rho;
        cons_state[1] = rho*v;
        cons_state[2] = rho*E;
        dm = cons_state[1];

        M = fabs(v)/a;
        H = h + ke;
    }

    Real calculate_target_massflow(const Real t)
    {
        return massflow_transient(t);
    }
}

/*
 * Pump model for compressible streams.
 * Adds pump dynamics to mass flow rate transient for an InflowFromStagnation
 * boundary.
 * Here, massflow_transient gives massflow setpoint for the pump model (rather
 * than ~actual massflow, as for InflowFromStagnation).
 * This is not the neatest implementation, but will suffice for the moment.
 */
class PumpFromStagnation : InflowFromStagnation
{
    // Generate the property fields
    enum PUMP_FIELDS = ["mdot_pump", "mdot_dot_pump", "mdot_dot_dot_pump",
                     "mdot_ref"];
    enum STATE_FIELDS = InflowFromStagnation.STATE_FIELDS ~ PUMP_FIELDS;
    mixin(PopulateStateFields!(PUMP_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    Real t_prev = 0.0;
    Real omega_min, omega_max, omega_dot_max, mdot_max, mdot_dot_max, k_pump,
           omega_n, zeta, i_rotor, c_rotor, k_rotor, mdot_min; // Constants
    Real omega, omega_dot; // State vars

    this(
        Real mdot_max,
        Real mdot_dot_max,
        const Address address,
        Stream stream,
        backend backend_model,
        Real initial_p_stag,
        Real initial_t_stag,
        Transient initial_v,
        const string coolprop_backend,
        const string coolprop_fluid,
        const Real a_in,
        const Real relax_factor=0.05)
    {
        super(address, stream, backend_model, initial_p_stag, initial_t_stag,
                initial_v, coolprop_backend, coolprop_fluid, a_in,
                relax_factor);

        // Currently assuming that all pumps have the same natural frequency and
        // damping ratio, but could make this user inputs if desired.
        // Set design parameters
        import std.math: PI;
        this.mdot_max = mdot_max;           // mass flow rate (kg/s)
        i_rotor       = 1;                  // Rotational inertia (kg m^2)
        omega_n       = 2.0;                // natural frequency (Hz)
        zeta          = 1.3;                // damping ratio

        // Performance limits
        omega_max    = 1000 * 2 * PI / 60;
        omega_min    = 10 * 2 * PI / 60;  // don't allow zero flow for stability
        this.mdot_dot_max = mdot_dot_max; // rate of change of mdot (kg/s^2)

        // Calculated parameters (to give desired omega, zeta)
        import std.math: pow;
        k_rotor = i_rotor * pow(omega_n, 2);
        c_rotor = 2 * i_rotor * zeta * omega_n;
        k_pump  = mdot_max / omega_max;
        omega_dot_max = mdot_dot_max / k_pump;
        mdot_min = k_pump * omega_min;

        // Initialize pump dynamic model
        EosCp.update(PT_INPUTS, p_stag, t_stag);
        mdot_pump = EosCp.rhomass * initial_v(0) * a_in;
        omega     = mdot_pump / k_pump;
        omega_dot = 0;
    }

    override @property string type() { return "PumpFromStagnation"; }

    override Real calculate_target_massflow(const Real t)
    {
        auto delta_t = t - t_prev;
        update_pump_dynamic_model(massflow_transient(t), delta_t); // updates mdot_pump
        t_prev = t;

        return mdot_pump;
    }

    void update_pump_dynamic_model(Real mdot_ref, Real delta_t)
    {
        this.mdot_ref = mdot_ref;
        // Convert reference mass flow rate to speed
        auto omega_ref = mdot_ref / k_pump;
        // Compute derivatives of state variables
        auto omega_dot_dot = (k_rotor * (omega_ref - omega)
                              - c_rotor * omega_dot) / i_rotor;

        // Check saturation limits and calculate updated values
        if (omega_dot > omega_dot_max)
            omega += omega_dot_max * delta_t;
        else if (omega_dot < -omega_dot_max)
            omega -= omega_dot_max * delta_t;
        else
            omega += omega_dot * delta_t;
        omega_dot += omega_dot_dot * delta_t;
        mdot_pump = k_pump * omega;
        // If we go below the min flow rate, just apply min value and set
        // derivatives to zero
        if (mdot_pump <= mdot_min)
        {
            mdot_pump = mdot_min;
            mdot_dot_pump = 0.0;
            mdot_dot_dot_pump = 0.0;
        }
        else if (mdot_pump >= mdot_max)
        {
            mdot_pump = mdot_max;
            mdot_dot_pump = 0.0;
            mdot_dot_dot_pump = 0.0;
        }
        else
        {
            mdot_dot_pump = k_pump * omega_dot;
            mdot_dot_dot_pump = k_pump * omega_dot_dot; // req'd for LTI model
        }
    }
}

/*
 * Newton-based method to set (p, s)-based EOS using (h, s) inputs.
 *
 * Solves for root of f(p) = h(p, s0) - h0 for known state (h0, s0).
 *
 * Solves p[k+1] = p[k] - f(p) / f'(p)
 * until |delta| = |p[k+1] - p[k]| < tol
 *
 * f' is evaluated using central differences.
 */
void set_eos_from_h_s(
    EOSBase eos,
    Real h0, // Known enthalpy
    Real s0, // Known entropy
    Real p,  // Guess of pressure
    Real tol = 1.00E-4,
    uint max_iter = 1_000_000)
{
    import std.math: fabs;
    import q1dcfd.utils.errors: NonConvergenceError;

    Real f_dash, delta;
    Real H = 1E-8;
    Real h, h_minus, h_plus;

    // Iterate until convergence
    // Assumes 'eos' has been updated at (rho, e) from previous time step
    foreach(i; 0..max_iter)
    {
        // Value of h for current estimate p
        eos.update(p, s0);
        h = eos.h;

        // Compute dh/dp|s
        eos.update((1+H)*p, s0);
        h_plus = eos.h;
        eos.update((1-H)*p, s0);
        h_minus = eos.h;
        f_dash = (h_plus - h_minus) / (2*H*p);
        if (f_dash == 0.0)
        {
            throw new NonConvergenceError("Derivative of function is zero");
        }

        delta = (h0 - h) / f_dash;
        p += delta;
        if (fabs(delta) < tol)
        {
            // Might as well take value from last iteration
            eos.update(p, s0);
            return;
        }
    }

    import std.stdio;
    writeln("Maximum number of iterations, ", to!string(max_iter), ", exceeded.");
    writeln("delta (change in pressure between last two guesses) = ", delta);
    throw new NonConvergenceError(
        "Newton's method failed to converge when computing (p, s) from (h, s)."
        );
}

/*
 * Newton-based method to set (p, h)-based CoolProp EOS using (h, s) inputs.
 *
 * Solves for root of f(p) = s(p, h0) - s0 for known state (h0, s0).
 *
 * Solves s[k+1] = s[k] - f(p) / f'(p)
 * until |delta| = |s[k+1] - s[k]| < tol
 *
 * f' is evaluated using central differences.
 */
void set_coolprop_eos_from_h_s(
    AbstractState eos,
    Real h0, // Known enthalpy
    Real s0, // Known entropy
    Real p,  // Guess of pressure
    Real tol = 1.00E-4,
    uint max_iter = 1_000_000)
{
    import std.math: fabs;
    import q1dcfd.utils.errors: NonConvergenceError;

    Real f_dash, delta;
    Real H = 1E-8;
    Real s, s_minus, s_plus;

    // Iterate until convergence
    // Assumes 'eos' has been updated at (rho, e) from previous time step
    foreach(i; 0..max_iter)
    {
        // Value of h for current estimate p
        eos.update(HmassP_INPUTS, h0, p);
        s = eos.smass;

        // Compute dh/dp|s
        eos.update(HmassP_INPUTS, h0, (1+H)*p);
        s_plus = eos.smass;
        eos.update(HmassP_INPUTS, h0, (1-H)*p);
        s_minus = eos.smass;
        f_dash = (s_plus - s_minus) / (2*H*p);
        if (f_dash == 0.0)
        {
            throw new NonConvergenceError("Derivative of function is zero");
        }

        delta = (s0 - s) / f_dash;
        p += delta;
        if (fabs(delta) < tol)
        {
            // Might as well take value from last iteration
            eos.update(HmassP_INPUTS, h0, p);
            return;
        }
    }

    import std.stdio;
    writeln("Maximum number of iterations, ", to!string(max_iter), ", exceeded.");
    writeln("delta (change in pressure between last two guesses) = ", delta);
    throw new NonConvergenceError(
        "Newton's method failed to converge when computing (p, s) from (h, s)."
        );
}



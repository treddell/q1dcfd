module q1dcfd.control_volumes.inflowDV ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.utils.math: square ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.metaprogramming: generate_save_method, PopulateStateFields,
    PopulateStateMap ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: backend ;

import std.format ;
import std.math: fabs ;
import std.conv: to ;
import std.stdio ;

/**
 * InflowDV
 *
 * An inflow boundary condition with user defined density and velocity inflow
 * transients (and their time derivatives). Uses the Navier Stokes Consistent 
 * Boundary Condition Method to extrapolate characteristics leaving the 
 * computational domain, and constructs the balance equations from these.
 * As both density and velocity are assigned, Only the energy equation is 
 * unconstrained
 */
final class InflowDV: FluidNode 
{
    // generate the property fields
    enum STATE_FIELDS = FluidNode.STATE_FIELDS ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    // user defined density(t)
    Transient density_transient ;

    // user defined d(rho)/dt (t)
    Transient density_rate_transient ; 

    // user defined velocity(t)
    Transient velocity_transient ; 

    // user defined d(v)/dt (t)
    Transient velocity_rate_transient ;

    // number of independent odes, (rho E) is the only free variable
    static uint order() {return 1 ;}

    // type tag
    override @property string type() {return "InflowDV" ;}

    // Lazily evaluated properties, do not appear in update equations, but may
    // be referenced by other nodes or write out state
    override @property
    {
        Real dt_momentum() {return rho*dvdt + v*dt_density ;}
        Real dt_energy() {return balance(0) ;}
        Real dedt() {return (dt_energy - dt_density*E)/rho -v*dvdt ;}
        Real x() {return eos.x ;}
        Real s() {return eos.s ;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt ;}
        Real dpdt() {return dt_density*(h - e) - rho*dedt ;}
        Real F0l() { assert(false, format!"%s has no left interface"(identify)) ;}
        Real F0r() { return flux.right[0] ;}
        Real F1l() { assert(false, format!"%s has no left interface"(identify)) ;}
        Real F1r() { return flux.right[1] ;}
        Real F2l() { assert(false, format!"%s has no left interface"(identify)) ;}
        Real F2r() { return flux.right[2] ;}
        Real mass() {return 0.0 ;}
        Real mdot() {return F0r ;}
    }

    this(const Address address,
         Stream stream,
         backend backend_model) 
    {
        super(address, stream, NodeGeometry(),"NoFriction", "NoHeatTransfer", 
              false, 1, backend_model) ;
    }

    /**
     * Update method retrieves density, velocity and time derivatives
     * from boundary conditions, and rho*E from the state space vector
     * then solves for all other states from (rho, e, v)
     */
    override void update(const ref Real[] state, const Real t)
    out
    {
        check_well_posedness ;
    }
    do
    {
        rho = density_transient(t) ;
        dt_density = density_rate_transient(t) ;
        v = velocity_transient(t) ;
        dvdt = velocity_rate_transient(t) ;
        
        vol = 1.0/rho ;
        ke = 0.5*v.square ;
        E = state[index_l]*vol ;
        e = E - ke ;
        cons_state[0] = rho ;
        cons_state[1] = rho*v ;
        cons_state[2] = rho*E ;
        dm = cons_state[1] ;

        eos.update(rho, e) ;

        h = eos.h ;
        T = eos.T ;
        cp = eos.cp ;
        dv = eos.dv ;
        k = eos.k ;
        a = eos.a ;

        M = fabs(v)/a ;
        p = (h - e)*rho ;
        H = h + ke ;
    }

    /**
     * balance update uses the NSCBC method to construct the
     * waves entering the domain. Uses backward extrapolated estimates
     * of the spatial derivatives
     */
    override void update_balance(ref Real[] balance) 
    {
        Real L1, L2, L5, d1, d2, d5 ;

        // upstream travelling accoustic wave
        L1 = (v - a)*(dpdx - rho*a*dvdx) ;

        // downstream travelling accoustic wave
        L5 = L1 - 2.0*rho*a*dvdt ;

        // downstream travelling convective wave
        L2 = - a.square*dt_density - 0.5*(L5 + L1) ;

        d1 = (L2 + 0.5*(L5 + L1))/a.square ;
        d5 = 0.5*(L5 + L1) ;
        d2 = (L5 - L1)/(2.0*rho*a) ;

        balance[index_l] = -H*d1 - cons_state[1]*d2 - 
            (rho*e + p)/(rho*a.square)*d5 ;
    }

    override void check_well_posedness()
    {
        super.check_well_posedness ;
        assert(v < a, format!("Supersonic Flow in (%s). This node model "
            ~"only well defined for subsonic flow")(identify));
    }
}

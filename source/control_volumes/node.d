module q1dcfd.control_volumes.node ;

import q1dcfd.config: Real, Address ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.types.geometry: NodeGeometry ;

import std.conv: to ;
import std.format ;
import std.string: replace ;

/**
 * Node
 * 
 * Base class for a node/element/control volume/cell/algebraic constraint.
 * Encodes the physics describing the state and balance at any point in time
 */
abstract class Node 
{
    /**
     * Globally stored location in the global state space vector,
     * saved properties vector, and a reference to the owning domain
     */
    Address address ;
    @property 
    {
        uint index_l() {return address.index_l ;}
        uint index_r() {return address.index_r ;}
        uint id() {return address.id ;}
        uint w_index_l() {return address.w_index_l ;}
        string owner() {return address.owner ;}
    }

    // if true, node will always be added to the ordered node queue
    bool priority_queue = false ; 

    /**
     * time taken for dominant eigenvector to cross node. Used to construct
     * a maximum stable time step.
     *
     * If cell has no impact on stability then infinity can be used
     */
    Real critical_dt = Real.infinity ; 

    // write saved property vector system saved properties vector
    void delegate(ref Real[] save_state) write_to ;

    // geometry attributes
    const NodeGeometry geometry ; 

    /// node type identifier
    abstract @property string type() ;

    /// Get the i'th component of the nodal ode's
    Real balance(const uint i)
    in
    {
        assert(i < index_r - index_l, 
            format!("Attempt to reference out of bounds balance "
            ~"component of Node %s. Local index given was %u but node has "
            ~"configuration space dimension %u")(identify, i, index_r - index_l)) ;
    } 
    do
    {
        return Simulation.get_balance(index_l + i) ;
    }

    this(const Address address, const NodeGeometry geometry) 
    {
        this.geometry = geometry ;
        this.address = address ;
    }

    /// Return a nodal identifier string, for debugging purposes
    string identify()
    {
        return "%s.%s, id:%u, address:%u,%u".format(owner, type, id, index_l, index_r) ;
    }

    /// A verbose string describing the node, for logging purposes.
    /// Contains equal or more information compared to 'identify'
    string describe() {return identify ;}

    /// Update any transients
    abstract void update(const ref Real[] state, const Real t) ;

    /// Update state space derivative
    abstract void update_balance(ref Real[] balance) ;

    /// Run some contract checks to make sure the cell is in a consistent state
    void check_well_posedness() {} ;

    /// Returns the value of the field 'prop', autogenerated by Simulation.initialise
    Real map(string prop) {assert(false) ;}

    /// TEMPORARY: Emulates behaviour of upcoming code refactor
    import q1dcfd.types: AssertionResult ;
    AssertionResult check_initialisation_conditions() {return typeof(return)(true, "") ;}
}

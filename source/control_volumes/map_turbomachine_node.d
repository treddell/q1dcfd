module q1dcfd.control_volumes.map_turbomachine_node;

import q1dcfd.lconfig: ROOT_DIRECTORY;
import q1dcfd.config: Real, Transient, Address;
import q1dcfd.utils.math: square;
import q1dcfd.control_volumes.node: Node;
import q1dcfd.maps: Turbine, Compressor;
import q1dcfd.stream: Stream;
import q1dcfd.types.geometry: NodeGeometry;
import q1dcfd.utils.metaprogramming: PopulateStateFields, PopulateStateMap;
import q1dcfd.simulation: Simulation;

version(high_prec) { import std.math: fabs; }
else               { import core.stdc.math: fabs; }

version(LDC) { import ldc.attributes: fastmath; }
else         { enum fastmath; }

/**
 * Special type of node that captures the discontinuity in momentum and
 * energy that occurs over a turbomachine.
 * This component is modelled as a node rather than an interface so that it can
 * be connected to controllers and sensors.
 *
 * No internal behaviour is actually modelled in this node, it is purely used to
 * calculate the upstream and downstream interface fluxes, where the difference
 * between these fluxes is the momentum and energy change over the turbomachine.
 * See domains/map_turbomachine_node.d for more details on the procedure by
 * which the momentum and energy discontinuities are calculated.
 *
 * Flux calculation procedure is as follows:
 * - Set T_in = T_left and p_in = P_left
 * - Set p_out = P_right
 * - Calculate mass_flux = f(p_in/p_out)
 * - Calculate W_sh = g(T_in, p_in/p_out)
 * - Set e_flux_in = mass_flux * e_in, set e_flux_out = e_flux_in - W_sh
 * - Calculate v_in = mass_flux/rho_in, v_out = mass_flux/rho_out
 * - Calculate momentum fluxes as v_flux_in = mass_flux * v_in + p_in,
 *   v_flux_out = mass_flux * v_out + p_out
 * - The left and right interfaces then read the relevant flux values.
 */
abstract class MapTurbomachineNode : Node
{
    // Generate property fields
    enum STATE_FIELDS =
    [
        "T_in",
        "p_in",
        "v_in",
        "h_in",
        "rho_in",
        "T_out",
        "p_out",
        "v_out",
        "h_out",
        "rho_out",
        "speed",       // Normalized rotational speed (N_actual / N_nominal)
        "mdot",
        "eta",         // Isentropic efficiency
        "delta_h",     // Enthalpy change
        "PR",          // Pressure ratio
        "w_sh",        // Shaft work
        "T_load",      // Load torque
        "T_motor",     // Supplied torque
        "dT_motor_dt", // Torque derivative
        "mass_flux",
        "v_flux_in",   // Momentum fluxes
        "v_flux_out",
        "e_flux_in",   // Energy Fluxes
        "e_flux_out",
    ];
    mixin(PopulateStateFields!(STATE_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    // These are the values that are used to communicate to the interfaces
    Real[3] flux_integral_upstream, flux_integral_downstream;

    Real area; // mean flow area

    this(const Address address, const NodeGeometry geometry)
    {
        super(address, geometry);

        // With the way this currently works, need to ensure that we have equal
        // flow area either side of the interface (eventually would be nice to
        // capture flow area changing)
        assert(geometry.rA == geometry.lA);
        area = geometry.lA;
    }

    @fastmath override void update(const ref Real[] state, const Real t)
    {
        compute_turbomachine_fluxes();
    }

    /* Solve the turbomachinery map model */
    @fastmath void compute_turbomachine_fluxes()
    {
        import std.typecons;
        import q1dcfd.utils.math: square;

        Real[3] flux_upstream, flux_downstream;

        double[string] result = solve_turbomachine(speed, T_in, p_in, p_out);
        mdot    = result["mdot"];
        eta     = result["eta"];
        delta_h = result["delta_h"];
        rho_out = result["rho_out"];
        T_out   = result["T_out"];
        h_out   = result["h_out"];
        PR      = p_out / p_in;

        // Calculate mass and momentum fluxes
        mass_flux = mdot / area;
        v_in = mass_flux / rho_in;
        v_out = mass_flux / rho_out;
        v_flux_in = mass_flux * v_in + p_in;
        v_flux_out = mass_flux * v_out + p_out;

        // TODO: Compressor model should know its inlet/outlet areas and find
        // w_sh internally

        // Calculate shaft work
        w_sh = (delta_h + 0.5*v_out.square - 0.5*v_in.square) * mdot;

        // Calculate energy flux in and out
        e_flux_in  = mass_flux * h_in + 0.5 * v_in.square;
        e_flux_out = e_flux_in + w_sh / area;

        // Assemble fluxes, then multiply by area
        flux_upstream   = [mass_flux, v_flux_in, e_flux_in];
        flux_downstream = [mass_flux, v_flux_out, e_flux_out];

        flux_integral_upstream[]   = flux_upstream[] * area;
        flux_integral_downstream[] = flux_downstream[] * area;
    }

    double[string] solve_turbomachine(
        double speed, double T_in, double p_in, double p_out);
}

final class MapTurbineNode : MapTurbomachineNode
{
    override @property string type() { return "MapTurbineNode"; }

    // Assume fixed-speed turbine, so dynamic model order is zero
    static uint order() { return 0; }

    Turbine Turb;

    this(
        const Address address,
        const NodeGeometry geometry,
        const string coolprop_fluid,
        const string map_name,
        immutable Real er_design,
        immutable Real mfp_design,
        immutable Real T0_design,
        immutable Real P0_design)
    {
        string map_dir = "/source/maps/" ~ map_name;
        Turb = new Turbine(ROOT_DIRECTORY ~ map_dir, coolprop_fluid, er_design,
                mfp_design, T0_design, P0_design);

        super(address, geometry);
    }

    override double[string] solve_turbomachine(
        double speed, double T_in, double p_in, double p_out)
    {
        return Turb.calc_from_er(speed, T_in, p_in, p_out);
    }

    override void update_balance(ref Real[] balance)
    {
        // Not really the best place for this...
        T_load = w_sh / (speed * Turb.speed_nominal);
    }
}

final class MapCompressorNode : MapTurbomachineNode
{
    override @property string type() { return "MapCompressorNode"; }
    static uint order() { return 2; }

    immutable bool fixed_speed; // force fixed speed (for initialization sims)
    immutable Real rotor_inertia;
    Transient motor_torque_transient;
    Transient motor_torque_derivative;
    void delegate(const ref Real[] state, const Real) update_motor_torque_state_var;

    Compressor Comp;

    this(
        const Address address,
        const NodeGeometry geometry,
        const string stream_name,
        const bool fixed_speed,
        Real initial_p_out,
        string map_name,
        immutable Real k_dh,
        immutable Real k_eta,
        immutable Real rotor_inertia)
    {
        super(address, geometry);
        this.fixed_speed = fixed_speed;

        // Rotordynamics parameters, calculate damping constant such that
        // damping ratio is 0.05 (effective stiffness is 1 since torque
        // difference is 'stiffness' term)
        import std.math: sqrt;
        immutable zeta = 0.05; // Currently not using damping
        this.rotor_inertia = rotor_inertia;

        Stream stream = cast(Stream) Simulation.get_stream(stream_name);
        string map_dir = "/source/maps/" ~ map_name;
        Comp = new Compressor(ROOT_DIRECTORY ~ map_dir, stream, initial_p_out,
                k_dh, k_eta);

        dT_motor_dt = 0.0; // assume no torque derivative at T=0
    }

    override double[string] solve_turbomachine(
        double speed, double T_in, double p_in, double p_out)
    {
        return Comp.calc_from_er(speed, T_in, p_in, p_out, mdot);
    }

    /*
     * transient_is_specified == true
     *      motor_torque_transient is specified explicitly in jobfile.
     *      Only need to evaluate speed ODE. 
     *
     * transient_is_specified == false
     *      controller is connected, gives motor_torque_derivative (i.e.
     *      motor_torque_transient is parametrized as first-order hold)
     *      Solve ODEs for T_motor and speed.
     */
    void configure_motor_torque_transients(const bool transient_is_specified)
    {
        if(transient_is_specified == true)
        {
            Real zero_func(const Real t){ return 0.0; }
            motor_torque_derivative = &zero_func;

            // Make a function that sets T_motor = motor_torque_transient(t)
            update_motor_torque_state_var = &set_motor_torque_from_transient;
        }
        else // transient_is_specified == false
        {
            update_motor_torque_state_var = &set_motor_torque_with_controller;
        }
    }

    void set_motor_torque_from_transient(const ref Real[] state, const Real t)
    {
        T_motor = motor_torque_transient(t);
    }

    void set_motor_torque_with_controller(const ref Real[] state, const Real t)
    {
        T_motor = state[index_l];
    }

    /*
     * Assume compressor is driven by electric motor with speed independent of
     * turbine speed.
     * Here, solve rotor dynamic equation
     *
     *   J dw/dt = (T_motor - T_load) - C * w
     *
     * to find evolution of compressor speed.
     * Assumes infinite shaft stiffness and, if C=0, bearings are frictionless.
     * We work in normalized speed so the result is scaled by the inverse of
     * compressor nominal speed.
     * Compressor motor torque is parametrized using first-order hold, so also
     * solve ODE
     *
     *   d(T_motor)/dt = <input>
     *
     * to get motor torque.
     * The value of <input> is given by a transient or a controller.
     */
    override void update_balance(ref Real[] balance)
    {
        balance[index_l] = dT_motor_dt;

        // Should this initialized in constructor then go in 'update'?
        T_load = w_sh / (speed * Comp.speed_nominal);
        balance[index_l + 1] =
            (T_motor - T_load) / (rotor_inertia * Comp.speed_nominal);
    }

    @fastmath override void update(const ref Real[] state, const Real t)
    {
        if(fixed_speed) T_motor = T_load;
        else update_motor_torque_state_var(state, t);
        dT_motor_dt = motor_torque_derivative(t);
        compute_turbomachine_fluxes();
        speed = state[index_l + 1];
    }
}


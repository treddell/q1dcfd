module q1dcfd.control_volumes.atmospheric_node ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.control_volumes.node: Node ;
import q1dcfd.config: Real, Address ;
import q1dcfd.utils.metaprogramming: PopulateStateFields ;
import q1dcfd.types.geometry: NodeGeometry ;

abstract class Atmosphere: Node 
{
    /** STUB for future implementation **/
    enum STATE_FIELDS = [] ;
    mixin(PopulateStateFields!(STATE_FIELDS)) ;

    Real pr ;
    Real ra ;
    Real k ;
    Real char_length ;

    this(const Address address, const NodeGeometry geometry)
    {
        super(address, geometry);
    }
}

module q1dcfd.control_volumes.outflow_node ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.utils.math: square ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.metaprogramming: PopulateStateFields, PopulateStateMap ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: backend ;

import std.format ;
import std.math: fabs ;
import std.conv: to ;
import std.stdio ;
import std.algorithm.comparison: max, min ;

/**
 * Base class for an outflow boundary condition 
 */
abstract class OutflowNode: FluidNode 
{
    // generate the property fields
    private enum UNIQUE_FIELDS = ["p_inf" /* far field pressure (Pa)*/] ;
    enum STATE_FIELDS = FluidNode.STATE_FIELDS ~ UNIQUE_FIELDS ;
    mixin(PopulateStateFields!(UNIQUE_FIELDS)) ;

    // scaling coefficient on the far field pressure reflection
    // L1 = a*refl_coeff*delta_p
    const Real refl_coeff ;

    // boundary condition function
    Transient target_function ;

    // Lazily evaluated properties, do not appear in update equations, but may
    // be referenced by other nodes or write out state
    override @property
    {
        Real dt_density() {return balance(0) ;}
        Real dt_momentum() {return balance(1) ;}
        Real dt_energy() {return balance(2) ;}
        Real dvdt() {return (dt_momentum - v*dt_density)/rho ;}
        Real dedt() {return (dt_energy - dt_density*E)/rho -v*dvdt ;}
        Real x() { return eos.x ;}
        Real s() {return eos.s ;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt ;}
        Real dpdt() {return dt_density*(h - e) - rho*dedt ;}
        Real F0l() { return flux.left[0] ;}
        Real F0r() {assert(false, format!"%s has no right interface"(identify));}
        Real F1l() { return flux.left[1] ;}
        Real F1r() {assert(false, format!"%s has no right interface"(identify));}
        Real F2l() { return flux.left[2] ;}
        Real F2r() {assert(false, format!"%s has no right interface"(identify));}
        Real mass() {return 0.0 ;}
        Real mdot() {return F0l ;}
    }

    this(const Address address,
         Stream stream,
         const Real refl_coeff,
         backend backend_model)
    {
        super(address, stream, NodeGeometry(), "NoFriction", "NoHeatTransfer", 
              false, 1, backend_model) ;
        this.refl_coeff = refl_coeff ;
    }

    override void update(const ref Real[] state, const Real t) 
    in
    {
        check_well_posedness ;
    }
    do
    {
        cons_state[] = state[index_l..index_l+3] ;

        rho = cons_state[0] ;
        vol = 1.0/rho ;
        v = cons_state[1]*vol ;
        ke = 0.5*v.square ;
        e = cons_state[2]*vol - ke ;
        dm = cons_state[1] ;

        eos.update(rho, e) ;

        h = eos.h;
        T = eos.T ;
        cp = eos.cp ;
        dv = eos.dv ;
        k = eos.k ;
        a = eos.a ;

        M = fabs(v)/a ;
        p = (h - e)*rho ;
        E = e + ke ;
        H = h + ke ;
    }

    override void update_balance(ref Real[] balance) 
    {
        Real L1, L2, L5, d1, d2, d5 ; // temporary variables

        // upstream travelling accoustic wave amplitude
        L1 = a*refl_coeff*(p - p_inf) ;

        //downstream travelling accoustic wave amplitude
        L5 = (v + a)*(dpdx + rho*a*dvdx) ;

        //downstream travelling convective wave
        L2 = v*(a.square * drhodx - dpdx) ;

        d1 = (L2 + 0.5*(L5 + L1))/a.square ;
        d5 = 0.5*(L5 + L1);
        d2 = (L5 - L1)/(2*rho*a) ;

        balance[index_l] = -d1 ;                // d(rho)/dt
        balance[index_l + 1] = -v*d1 - rho*d2 ; // d(rho v)/dt
        balance[index_l + 2] = -H*d1 - cons_state[1]*d2 
            - (rho*e + p)/(rho*a.square)*d5 ;              // d(rho E)/dt
    }

    override void check_well_posedness()
    {
        super.check_well_posedness ;

        assert(v < a, format!("Supersonic Flow M=%f in (%s). This node model "
            ~"only well defined for subsonic flow")(v/a, identify));
    }
}

/**
 * FarFieldNode
 * 
 * An outflow boundary condition with a user defined far field pressure
 * Uses the Navier Stokes Consistent Boundary Condition Method to extrapolate
 * outward moving characteristics across the boundary and constructs the 
 * balance equations from these
 */
final class FarFieldNode: OutflowNode
{
    mixin(PopulateStateMap!(OutflowNode.STATE_FIELDS)) ;

    // number of independent odes (rho, rho v, rho E)
    static uint order() {return 3 ;}

    override @property string type() {return "FarFieldNode" ;}

    this(const Address address,
         Stream stream,
         const Real refl_coeff,
         backend backend_model)
    {
        super(address, stream, refl_coeff, backend_model) ;
    }

    override void update(const ref Real[] state, const Real t)
    {
        // far field pressure assigned from boundary condition or controller
        p_inf = target_function(t) ;

        super.update(state, t) ;
    }
}

/**
 * TargetMassNode
 * 
 * An outflow boundary condition which adjusts a far field pressure
 * in order to achieve a target mass flow rate
 *
 * Has a configuration space dimension of 4, state is [rho, rho v, rho E, p_inf]
 */
final class TargetMassNode: OutflowNode
{
    // generate the property fields
    private enum UNIQUE_FIELDS = [
        "mdot_target" /* target mass flow rate at the current moment in time (Pa) */] ;
    enum STATE_FIELDS = OutflowNode.STATE_FIELDS ~ UNIQUE_FIELDS ;
    mixin(PopulateStateFields!(UNIQUE_FIELDS)) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;
    
    private const
    {
        // maximum rate of change of pressure
        Real max_dpdt ;

        // scaling factor on the rate of change of pressure
        Real mass_coeff ;
    }

    // number of independent odes (rho, rho v, rho E, p_inf)
    static uint order() {return 4 ;}

    // type tag
    override @property string type() {return "MassFlowTarget" ;}

    this(const Address address,
         Stream stream,
         const Real refl_coeff,
         const Real max_dpdt,
         const Real mass_coeff,
         backend backend_model)
    {
        super(address, stream, refl_coeff, backend_model) ;
        this.max_dpdt = max_dpdt ;
        this.mass_coeff = mass_coeff ;
    }

    /**
     * update method uses the generic conservative variable update
     * scheme, but also pulls p_inf from the state space vector
     * and updates the target mass flow rate
     */
    override void update(const ref Real[] state, const Real t)
    {
        super.update(state, t) ;
        p_inf = state[index_r - 1] ;
        mdot_target = target_function(t) ;
    }

    /**
     * update balance method uses the navier stokes boundary condition
     * method, and sets the rate of change of far field pressure as
     * proportionally to the deviation from target mass flow rate
     * or as an upper/lower magnitude if this rate is too high
     */
    override void update_balance(ref Real[] balance)
    {
        super.update_balance(balance) ;

        // calculate an objective pressure rate of change and truncate
        // to within acceptable limits if necessary
        auto dpdt_uncorrected =  mass_coeff*(mdot - mdot_target) ;
        if(dpdt_uncorrected > 0)
        {
            balance[index_l + 3] = min(max_dpdt, dpdt_uncorrected) ;
        }
        else
        {
            balance[index_l + 3] = max(-max_dpdt, dpdt_uncorrected) ;
        }
    }
}

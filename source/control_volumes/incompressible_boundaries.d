module q1dcfd.control_volumes.incompressible_boundaries;

import q1dcfd.control_volumes.node: Node;
import q1dcfd.types.geometry: NodeGeometry;
import q1dcfd.utils.metaprogramming: from, PopulateStateFields, PopulateStateMap;

import coolprop;

/* Base class for incompressible boundary conditions. */
abstract class IncompressibleBoundary: Node
{
    import q1dcfd.config: Real, Address, Transient;

    // Generate property fields, only have a limited number of props here that
    // are required for controller integration
    // All properties are per channel (affects qdot and mdot)
    enum STATE_FIELDS =
    [
        "rho",  // density (kg/m3)
        "e",    // specific internal energy (J/kg)
        "p",    // static pressure (Pa)
        "T",    // total temperature (K)
        "v",    // inertial flow velocity (m/s)
        "mu",   // viscosity
        "k",    // conductivity
        "h",    // enthalpy
        "mdot", // cell average mass flow rate per channel (kg/s)
        "htc",  // heat transfer coefficient (W/mK)
        "qdot", // heat transfer rate per channel (W)
    ];
    enum FIELDS = STATE_FIELDS ;

    mixin(PopulateStateFields!(STATE_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS)); // This makes the getters and setters

    this(const Address address)
    {
        import q1dcfd.types.geometry: NodeGeometry;
        super(address, NodeGeometry());
    }

    override @property string type() { return "IncompressibleBoundary"; }
}

/*
 * Inflow boundary for incompressible streams.
 * The incompressible solver is stable for prescribed boundary conditions with
 * no special treatment, so this class just updates inlet velocity and
 * temperature values with values from a controller or user-defined transient.
 */
class IncompressibleInflow: IncompressibleBoundary
{
    import q1dcfd.config: Real, Address, Transient;

    // We currently need a Coolprop EOS to evaluate derived properties for
    // incompressible fluids
    AbstractState EosCp;

    // User-defined transients
    Transient temperature_transient;
    Transient massflow_transient;

    // Transient velocity_transient; FIX
    // Transient velocity_rate_transient;

    // TEMPORARY
    Real piso_period;
    Real last_update_time = 0.0;
    Real a_in; // Inlet flow area, needed since we use a massflow transient

    this(const Address address, string coolprop_backend, string coolprop_fluid,
         Real piso_period, const Real a_in)
    {
        super(address);
        this.a_in = a_in;
        EosCp = new AbstractState(coolprop_backend, coolprop_fluid);
        this.piso_period = piso_period;
    }

    override @property string type() { return "IncompressibleInflow"; }

    // Since we just pull results from a solver, there are no actual ODEs to
    // solve here
    static uint order() { return 0; }

    override void update(const ref Real[] state, const Real t)
    {
        // We only run this update every incompressible update since it uses
        // coolprop (and during initialisation)
        double piso_delta_t = t - last_update_time;
        if(piso_delta_t >= piso_period || t == 0.0)
        {
            mdot = calculate_massflow(t);
            T = temperature_transient(t);
            update_derived_properties();
            v = mdot / (rho * a_in);
        }
    }

    // Again, no ODEs/state vars for this node type so update_balance is not req'd
    override void update_balance(const ref Real[] state) {}

    /* Calculate values of other thermodynamic properties from (p,T) using CoolProp EOS */
    void update_derived_properties()
    {
            EosCp.update(PT_INPUTS, p, T);
            rho = EosCp.rhomass;
            mu  = EosCp.viscosity;
            e   = EosCp.umass;
            k   = EosCp.conductivity;
            h   = EosCp.hmass;
    }

    Real calculate_massflow(const Real t)
    {
        return massflow_transient(t);
    }
}

/*
 * Outflow boundary for incompressible streams.
 * The incompressible solver is stable for fully developed (constant gradient) outflow boundary
 * conditions with no special treatment, so this class just records the outlet
 * values from the solver.
 */
final class IncompressibleOutflow: IncompressibleBoundary
{
    import q1dcfd.config: Real, Address, Transient;

    // boundary condition function
    Transient target_function ;

    this(const Address address)
    {
        super(address);
    }

    override @property string type() { return "IncompressibleOutflow"; }

    // Since we just pull results from a solver, there are no actual ODEs to
    // solve here
    static uint order() { return 0; }

    // Just records values (done in interface), so no update method required
    override void update(const ref Real[] state, const Real t) {}

    // Again, no ODEs/state vars for this node type so update_balance is not req'd
    override void update_balance(const ref Real[] state) {}
}

class IncompressiblePump : IncompressibleInflow
{
    // Generate the property fields
    enum PUMP_FIELDS = ["mdot_pump", "mdot_dot_pump", "mdot_dot_dot_pump", "mdot_ref"];
    enum STATE_FIELDS = IncompressibleInflow.STATE_FIELDS ~ PUMP_FIELDS;
    mixin(PopulateStateFields!(PUMP_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    Real t_prev = 0.0;
    Real omega_min, omega_max, omega_dot_max, mdot_max, mdot_dot_max, k_pump,
           omega_n, zeta, i_rotor, c_rotor, k_rotor, mdot_min; // Constants
    Real omega, omega_dot; // State vars

    this(const Address address, string coolprop_backend, string coolprop_fluid,
         Real piso_period, const Real a_in, Transient initial_p, Transient
         initial_T, Transient initial_v, Real mdot_max, Real mdot_dot_max)
    {
        super(address, coolprop_backend, coolprop_fluid, piso_period, a_in);

        if(mdot_max == 0.0 || mdot_dot_max == 0.0)
        {
            throw new Exception(
                "ERROR: 'mdot_max' or 'mdot_dot_max' has not been specified "
                ~ "for 'IncompressiblePump'. These must be specified.");
        }

        // Currently assuming that all pumps have the same natural frequency and
        // damping ratio, but could make this user inputs if desired.
        // Set design parameters
        import std.math: PI;
        this.mdot_max = mdot_max;           // mass flow rate (kg/s)
        i_rotor       = 1;                  // Rotational inertia (kg m^2)
        omega_n       = 2.0;                // natural frequency (Hz)
        zeta          = 1.3;                // damping ratio

        // Performance limits
        omega_max    = 1000 * 2 * PI / 60;
        omega_min    = 10 * 2 * PI / 60;  // don't allow zero flow for stability
        this.mdot_dot_max = mdot_dot_max; // rate of change of mdot (kg/s^2)

        // Calculated parameters (to give desired omega, zeta)
        import std.math: pow;
        k_rotor = i_rotor * pow(omega_n, 2);
        c_rotor = 2 * i_rotor * zeta * omega_n;
        k_pump  = mdot_max / omega_max;
        omega_dot_max = mdot_dot_max / k_pump;
        mdot_min = k_pump * omega_min;

        // Initialize pump dynamic model
        EosCp.update(PT_INPUTS, initial_p(0), initial_T(0));
        mdot_pump = EosCp.rhomass * initial_v(0) * a_in;
        omega     = mdot_pump / k_pump;
        omega_dot = 0;
    }

    override @property string type() { return "IncompressiblePump"; }

    override Real calculate_massflow(const Real t)
    {
        auto delta_t = t - t_prev;
        update_pump_dynamic_model(massflow_transient(t), delta_t); // updates mdot_pump
        t_prev = t;
        return mdot_pump;
    }

    void update_pump_dynamic_model(Real mdot_ref, Real delta_t)
    {
        this.mdot_ref = mdot_ref;
        // Convert reference mass flow rate to speed
        auto omega_ref = mdot_ref / k_pump;
        // Compute derivatives of state variables
        auto omega_dot_dot = (k_rotor * (omega_ref - omega)
                              - c_rotor * omega_dot) / i_rotor;

        // Check saturation limits and calculate updated values
        if (omega_dot > omega_dot_max)
            omega += omega_dot_max * delta_t;
        else if (omega_dot < -omega_dot_max)
            omega -= omega_dot_max * delta_t;
        else
            omega += omega_dot * delta_t;
        omega_dot += omega_dot_dot * delta_t;
        mdot_pump = k_pump * omega;
        // If we go below the min flow rate, just apply min value and set
        // derivatives to zero
        if (mdot_pump <= mdot_min)
        {
            mdot_pump = mdot_min;
            mdot_dot_pump = 0.0;
            mdot_dot_dot_pump = 0.0;
        }
        else if (mdot_pump >= mdot_max)
        {
            mdot_pump = mdot_max;
            mdot_dot_pump = 0.0;
            mdot_dot_dot_pump = 0.0;
        }
        else
        {
            mdot_dot_pump = k_pump * omega_dot;
            mdot_dot_dot_pump = k_pump * omega_dot_dot; // req'd for LTI model
        }
    }
}


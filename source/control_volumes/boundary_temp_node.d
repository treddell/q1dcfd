module q1dcfd.control_volumes.boundary_temp_node ;

import q1dcfd.control_volumes.thermal_node: ThermalNode ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.math: MathFunction ;

import std.conv: to ;
import std.format ;

/**
 * TemperatureNode
 *
 * Lumped mass held at a user defined temperature. Used as a boundary condition
 * for simple boiler/cooler models etc
 *
 * Has no independent ODE's, simply used as an algebraic constraint
 */
final class TemperatureNode: ThermalNode
{
    // generate property fields
    enum STATE_FIELDS = ThermalNode.STATE_FIELDS ;

    // Function(t), user defined temperature
    Transient temperature_transient ;

    // no independent ODE's
    static uint order() {return 0 ;}

    // type tag
    override @property string type() {return "TemperatureNode" ;}

    this(const Address address, const NodeGeometry geometry)
    {
        super(address, geometry) ;
    }

    // Simply retrieves the temperature from the boundary condition
    override void update(const ref Real[] state, const Real t) 
    out
    {
       check_well_posedness ;
    }
    do
    {
        // zero fluxes
        heat_flux = 0 ;

        // assign the new value of temperature from the given boundary condition
        T = temperature_transient(t) ;
    }

    // No ODEs to update
    override void update_balance(ref Real[] balance) {}
}

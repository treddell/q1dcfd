module q1dcfd.control_volumes.fluid_node ;

import q1dcfd.control_volumes.node: Node ; 
import q1dcfd.config: Real, NUMERICAL_TEMPERATURE_LIMIT, Address ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.correlations.standard_corr: 
    HeatTransferCorrelation, FrictionFactorCorrelation, friction_factor_map,
    heat_transfer_map ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.metaprogramming: PopulateStateFields ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: generate_eos, backend, EOSBase ;

import std.format ;

/*
 * A container for external fluxes
 */
private struct Flux
{
    // mass/momentum/energy flux, left interface
    Real[3] left ;  

    // mass/momentum/energy flux, right interface
    Real[3] right ; 

    // external heat addition
    Real extern_heat = 0 ;

    // zero all fluxes
    @nogc pure nothrow void reset()
    {
        left[] = 0 ;
        right[] = 0 ;
        extern_heat = 0 ;
    }
}

/**
 * FluidNode
 * 
 * A control volume describing the flow of a fluid, base class for
 * all internal and boundary fluidlike elements
 */
abstract class FluidNode: Node 
{
    // generate property fields
    enum STATE_FIELDS = 
    [
        "rho", // density (kg/m3)
        "vol", // specific volume (m3/kg)
        "e", // specific internal energy (J/kg)
        "E", // specific total energy (J/kg)
        "ke", // specific kinetic energy (J/kg)
        "p", // static pressure (Pa)
        "h", // static enthalpy (J/kg)
        "H", // total enthalpy (J/kg)
        "T", // total temperature (K)
        "cp", // specific heat capacity (J/kg.K)
        "dv", // dynamic viscosity (kg/s.m)
        "k", // thermal conductivity (W/m.K)
        "v", // inertial flow velocity (m/s)
        "a", // speed of sound (m/s)
        "s", // entropy (J/kg.K)
        "M", // Mach number (-)
        "x", // vapour fraction (-)
        "pr", // Prandtl number  (-)
        "re", // Reynolds number (-)
        "f", // friction factor (-)
        "dm", // mass flux (kg/m2.s)
        "dt_density", // density rate of change (kg/m3.s)
        "dt_momentum", // momentum density rate of change (kg /m2.s)
        "dt_energy", // total energy density rate of change (J/m3.s)
        "dvdt", // fluid acceleration (m/s2)
        "dedt", // specific internal energy rate of change (J/kg.s)
        "dTdt", // temperature rate of change (K/s)
        "dpdt", // rate of change of pressure (Pa/s)
        "dpdx", // extrapolated pressure gradient (Pa/m)
        "dvdx", // extrapolated velocity gradient (1/s)
        "drhodx", // extrapolated density gradient (kg/m4),
        "F0l", // left cell face mass flow rate (kg/s)
        "F0r", // right cell face mass flow rate (kg/s)
        "F1l", // left cell face force (kg.m/s2)
        "F1r", // right cell face force (kg.m/s2)
        "F2l", // left cell face energy flow (kg m2/s3)
        "F2r", // right cell face energy flow (kg m2/s3)
        "mass", // cell fluid mass (kg)
        "mdot", // cell average mass flow rate (kg/s)
        "shear", // cell viscous shear (kg/m^2 s^2)
        "p_loss", // power loss due to shear (W)
        "htc", // heat transfer coefficient (W/mK)
        "nu", // nusselt number
        "qdot_extern", // external heat transfer into volume (W)
    ] ;
    
    mixin(PopulateStateFields!(STATE_FIELDS)) ;

    // equation of state calculator
    EOSBase eos ;

    // container for flux contributions
    Flux flux ; 

    // friction factor correlation
    FrictionFactorCorrelation f_corr ; 

    // fluid element conserved property vector, (rho, rho v, rho E)
    Real[3] cons_state ;

    // internal source term contributions
    Real[3] internal_source ; 

    const
    {
        // correlation keys
        string f_corr_name ;
        string ht_corr_name ;

        // heat transfer correlations evaluated at thin film conditions (true) 
        // or bulk conditions (false)
        bool thin_film_heat_transfer ;

        // number of parallel channels
        uint n_channels ;
    }

    this(
        const Address address,
        Stream stream ,
        const NodeGeometry geometry, 
        const string f_corr_name, 
        const string ht_corr_name, 
        const bool thin_film_heat_transfer,
        const uint n_channels,
        backend backend_model)
    {
        super(address, geometry);
        this.eos = generate_eos(stream.conservative_data,
                                backend_model,
                                identify) ;

        this.f_corr_name = f_corr_name ;
        this.f_corr = friction_factor_map(this, f_corr_name) ;

        this.ht_corr_name = ht_corr_name ;
        this.thin_film_heat_transfer = thin_film_heat_transfer ;

        stream.add_node(this) ;

        this.n_channels = n_channels ;
    }

    override void check_well_posedness()
    {
        import std.math: isNaN ;
        assert(rho > 0 && !rho.isNaN, format!("Invalid density %f in (%s)")
            (rho, identify)) ;
        assert(e > 0 && !e.isNaN, format!("Invalid internal energy %f in (%s)")
            (e, identify)) ;
        assert(p > 0 && !p.isNaN, format!("Invalid pressure %f in (%s)")
            (p, identify)) ;
        assert(T > 0 && T < NUMERICAL_TEMPERATURE_LIMIT, 
            format!("Temperature %f outside of expected bounds in (%s)")
            (T, identify)) ;
        assert(a > 0 && !a.isNaN, format!("Invalid speed of sound %f in (%s)")
            (a, identify)) ;
    }
}

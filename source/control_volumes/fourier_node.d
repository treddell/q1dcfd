module q1dcfd.control_volumes.fourier_node ;

import q1dcfd.control_volumes.thermal_node: ThermalNode ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.config: Real, Address ;
import q1dcfd.utils.math: square ;
import q1dcfd.types.geometry: NodeGeometry ;

import std.conv: to ;
import std.format ;

/**
 * FourierNode
 * 
 * Control volume for a homogeneous, constant density, heat capacity and 
 * conductivity lumped thermal mass. Based off conservation of 
 * energy from Fouriers heat equation. Equivilant to a finite volume
 * formulation
 */
final class FourierNode: ThermalNode 
{
    // generate property fields
    enum STATE_FIELDS = ThermalNode.STATE_FIELDS ;

    // some additional constant properties
    const
    {
        // specific heat capacity (J/kg.K)
        Real c ; 

        // density (kg/m3)
        Real rho ; 

        // net heat capacitance (J/K)
        Real cap ; 

        // inverse of capacitance (K/J)
        Real elastance ; 

        // thermal diffusitivity (m2/s)  
        Real alpha ; 
    }

    // number of independent odes, only T
    static uint order() { return 1 ;}

    // type tag
    override @property string type() {return "FourierNode" ;}

    this(
        const Address address,
        const NodeGeometry geometry, 
        const Real conductivity,
        const Real density, 
        const Real specific_heat)     
    {
        super(address, geometry) ;

        this.k = conductivity ;
        this.rho = density ;
        this.c = specific_heat ;
        this.cap = this.rho*this.c*this.geometry.volume ;
        this.elastance = 1.0/this.cap ;
        this.alpha = this.k/(this.rho*this.c) ;

        this.critical_dt = this.geometry.length.square/this.alpha ;
        this.heat_flux = 0.0 ;
    }

    override void update(const ref Real[] state, const Real t) 
    out
    {
        check_well_posedness ;
    }
    do
    {
        // zero fluxes
        heat_flux = 0 ;
        
        // get the nodal temperature from the system configuration space
        T = state[index_l] ;
    }

    override void update_balance(ref Real[] balance) 
    out 
    {
        import std.math: isNaN ;
        assert(!balance[index_l].isNaN, format!("dT/dt overflow in %s "
            ~"heat flux is %f, previous temperature is %f")
            (identify, heat_flux, T)) ;
    }
    do
    {
        // Update the nodal balance equation (dT/dt)
        balance[index_l] = heat_flux*elastance ;
    }

    override string describe()
    {
        return (identify ~ " thermal_conductivity: %f, specific_heat: %f, density: %f"
                ~" length: %f, volume: %f, critical_time_step: %f")
            .format(k, c, rho, geometry.volume, geometry.length, critical_dt) ;
    }
}

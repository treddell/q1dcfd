module q1dcfd.control_volumes.incompressible_node;

import q1dcfd.control_volumes.node: Node;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.metaprogramming: from, PopulateStateFields, PopulateStateMap;

final class IncompressibleNode: Node
{
    import q1dcfd.config: Real, Address;

    override @property string type() { return "IncompressibleNode"; }

    const uint n_channels; // Req'd to calculate total heat transfer area

    // Generate property fields, only have a limited number of props here that
    // are required for controller testing
    // All properties are per channel (affects qdot and mdot)
    enum STATE_FIELDS =
    [
        "rho",  // density (kg/m3)
        "e",    // specific internal energy (J/kg)
        "h",    // specific enthalpy (J/kg)
        "p",    // static pressure (Pa)
        "T",    // total temperature (K)
        "v",    // inertial flow velocity (m/s)
        "mu",   // viscosity (Pa s)
        "k",    // thermal conductivity (W / m K)
        "mdot", // cell average mass flow rate per channel (kg/s)
        "htc",  // heat transfer coefficient
        "qdot_extern", // heat transfer rate per channel (W)
    ];
    enum FIELDS = STATE_FIELDS ;

    mixin(PopulateStateFields!(STATE_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS)); // This makes the getters and setters

    this(const Address address,
         const NodeGeometry geometry,
         const uint n_channels)
    {
        super(address, geometry);
        this.n_channels = n_channels;

        this.htc = 0.0; // TODO: How to initialise this to the correct value?
                        // (Does it even matter? Overwritten on first timestep.)
    }

    // Since we just pull results from a solver, there are no actual ODEs to
    // solve here
    static uint order() { return 0; }

    override void update(const ref Real[] state, const Real t) { }

    // Again, no ODEs/state vars for this node type so update_balance is not req'd
    override void update_balance(const ref Real[] state) { }
}



module q1dcfd.control_volumes.piso_solver;

import q1dcfd.control_volumes.node: Node;
import q1dcfd.simulation: Simulation;
import q1dcfd.config: Real, Transient, Address;
import q1dcfd.utils.math: square;
import q1dcfd.utils.metaprogramming: from ;

import coolprop;

import mir.ndslice;
import lubeck;

alias odarr = Slice!(double*, 1);
alias tdarr = Slice!(double*, 2);

/*
 * Assigns, sizes, and zeros dynamic arrays with names in 'vars' to the current scope.
 * Also creates time derivative and substepping arrays.
 */
template InitArrays(string[] names, string length="")
{
    import std.conv: to;
    import std.format;
    string InitArrays()
    {
        string str_out = "";
        foreach(name; names)
        {
            static if (length.length)
            {
                // Normal arrays
                str_out ~= format!("%s.length = %s; ")(name, length);
                str_out ~= format!("%s[] = 0.0; ")(name);
                // Derivative arrays
                str_out ~= format!("d%sdt.length = %s; ")(name, length);
                str_out ~= format!("d%sdt[] = 0.0; ")(name);
            }
            else {
                str_out ~= format!("double[] %s; ")(name);
                str_out ~= format!("double[] d%sdt; ")(name);
            }
        }
        return str_out;
    }
}

/*
 * Assigns and sizes previous-timestep property arrays.
 * E.g., for prop rho, 1) assigns rho0, 2) sets rho0,
 * and 3) initialises rho0 to rho. Does this for all props.
 */
template InitPreviousTimestepAndSubstepArrays(string[] props, string length="")
{
    import std.conv: to;
    import std.format;
    string InitPreviousTimestepAndSubstepArrays()
    {
        string str_out = "";
        foreach(prop; props)
        {
            static if (length.length) {
                // Prev timestep arrays
                str_out ~= format!("%s0.length = %s; ")(prop, length);
                str_out ~= format!("%s0[] = %s[]; ")(prop, prop);
                // Substep arrays
                str_out ~= format!("%s_sub.length = %s; ")(prop, length);
                str_out ~= format!("%s_sub[] = %s[]; ")(prop, prop);
            }
            else {
                str_out ~= format!("double[] %s0; ")(prop);
                str_out ~= format!("double[] %s_sub; ")(prop);
            }
        }
        return str_out;
    }
}

/*
 * Dynamic model of heat exchanger stream that is integrated using PISO scheme.
 * Required for incompressible fluids since density-based methods become
 * ill-conditioned.
 *
 * Assumptions:
 *     - flow direction is positive (left to right)
 *     - grid spacing is uniform.
 */
final class PisoSolver: Node
{
    import std.math: PI, pow, log, fabs, sqrt;
    import std.conv: to;

    uint nCELLS;      // Number of axial discretisations
    Real PISO_PERIOD; // Interval between PISO updates
    Real last_update_time = 0.0;
    Real last_substep_time = 0.0;

    final struct Parameters
    {
        string FLUID_CODE;     // CoolProp incompressible fluid code, "INCOMP:<fluid>"
        string FLUX_METHOD;    // Currently only supports upwinding
        string NU_CORRELATION; // Nusselt number correlation
        string FR_CORRELATION; // Friction factor correlation
        bool VISCOUS_FX_FLAG;  // Enable momentum diffusivity and axial thermal conduction

        this(string[string] stream_defs)
        {
            FLUID_CODE = stream_defs["FLUID_CODE"];
            FLUX_METHOD = stream_defs["FLUX_METHOD"];
            NU_CORRELATION = stream_defs["NU_CORRELATION"];
            FR_CORRELATION = stream_defs["FR_CORRELATION"];
            VISCOUS_FX_FLAG = to!bool(stream_defs["VISCOUS_FX_FLAG"]);
        }
    }

    /* We use doubles for all numeric values since the sole purpose of these
       values are as parameters in floating-point calculations */
    final struct Geometry
    {
        double L;         // Length
        double D_TUBE;    // Channel diameter
        double L_C;       // Characteristic length
        double D_H;       // Hydraulic diameter
        double nCHANNELS; // Number of channels
        double EPSILON;   // Surface roughness
        double A_FLOW;    // Flow area
        double P;         // Channel perimeter
        double P_HEAT;    // Q1D uses a hypothetical "heat circumference" that
                          // can be tuned to adjust heat transfer area
                          // independently of flow area
        double V_CELL;    // Volume per cell
        double DELTA_X;   // Axial distance per cell

        this(uint nCELLS, string[string] stream_defs, bool q1d_geo_flag=false)
        {
            // Geometry general to each version (regular and q1d)
            nCHANNELS = to!double(stream_defs["nCHANNELS"]);
            L = to!double(stream_defs["LENGTH"]);
            EPSILON = to!double(stream_defs["EPSILON"]);
            DELTA_X = L / to!double(nCELLS);

            // Version specific geometry settings
            if (!q1d_geo_flag)
            {
                // Regular geometry initialisation based on channel shape
                D_TUBE = to!double(stream_defs["D_TUBE"]);
                if (stream_defs["CHANNEL_GEO"] == "circular")
                {
                    L_C = D_TUBE;
                    D_H = D_TUBE;
                    P = PI * D_TUBE;
                    P_HEAT = P; // Normal geo model, P_HEAT and P are the same
                }
                else
                {
                    import std.format;
                    throw new Exception(format!(
                        "Channel geometry %s not implemented. " ~
                        "Valid options are 'circular'.")(stream_defs["CHANNEL_GEO"]));
                }
                A_FLOW = nCHANNELS * PI * 0.25 * D_TUBE.pow(2);
                V_CELL = DELTA_X * A_FLOW;
            }
            else
            {
                // q1d initialisation procedure, allows specification of flow area
                // and hydraulic diameter independently
                // PISO solver uses total flow area rather than per channel
                A_FLOW = to!double(stream_defs["CROSS_AREA"]) * nCHANNELS;
                D_H = to!double(stream_defs["HYDRAULIC_DIAMETER"]);
                L_C = D_H;
                P = PI * D_H;
                P_HEAT = to!double(stream_defs["HEAT_CIRCUMFERENCE"]);
                V_CELL = DELTA_X * A_FLOW;
            }
        }
    }

    Parameters Params;
    Geometry Geo;
    AbstractState Eos;

    // Velocity arrays are treated separately due to staggered grid (they have one less node)
    // Current time-step properties
    // T_wall now not used (q_dot taken directly from main code)
    immutable string[] PROPS  = ["p", "T", "rho", "mu", "e", "k", "v_scalar",
              "mdot", "re", "q_dot", "htc"];
    mixin(InitArrays!(PROPS));
    mixin(InitArrays!(["v"])); // has one less node
    // Previous time-step properties (used to cal
    mixin(InitPreviousTimestepAndSubstepArrays!(PROPS));
    mixin(InitPreviousTimestepAndSubstepArrays!(["v"]));
    // Pressure correction fields
    mixin(InitArrays!(["p_dash", "p_dash_dash"]));
    // Corrected pressures and velocities
    mixin(InitArrays!(["p_star", "p_star_star", "p_star_star_star"]));
    mixin(InitArrays!(["u_star", "u_star_star", "u_star_star_star"]));

    // Linear system matrices / vectors
    tdarr A, b, C, d, f, G, l;

    this(const Address address,
         uint nCELLS,
         Real piso_period,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         string[string] stream_defs,
         const bool q1d_geo_flag)
    {
        import std.array: split;

        // This is a 'dimensionless' node that runs the PISO solver, so we pass
        // in a blank NodeGeometry
        super(address, from!"q1dcfd.types.geometry".NodeGeometry());

        this.nCELLS = nCELLS;
        this.PISO_PERIOD = piso_period;
        // Main q1d simulation loop is not allowed to integrate more than
        // piso_period per step
        this.critical_dt = piso_period;

        Params = Parameters(stream_defs);
        Geo = Geometry(nCELLS, stream_defs, q1d_geo_flag);
        Eos = new AbstractState(Params.FLUID_CODE.split(":")[0],
                                Params.FLUID_CODE.split(":")[1]);

        // Property arrays have two extra nodes to hold boundary values
        // We have one less velocity node due to the staggered grid
        mixin(InitArrays!(PROPS, "nCELLS + 2"));
        mixin(InitArrays!(["v"], "nCELLS + 1"));
        mixin(InitArrays!(["p_dash", "p_dash_dash"], "nCELLS + 2"));
        mixin(InitArrays!(["p_star", "p_star_star", "p_star_star_star"],
                                     "nCELLS + 2"));
        mixin(InitArrays!(["u_star", "u_star_star", "u_star_star_star"],
                                     "nCELLS + 1"));

        initialise_state_variables(initial_temperature_distribution,
                                   initial_pressure_distribution,
                                   initial_velocity_distribution);
        update_derived_properties();
        update_velocity_and_mass_flow_at_scalar_nodes();

        initialise_linear_sys_matrices();

        // Need HTCs for the main code initialisation process
        htc[1..nCELLS+1] = compute_heat_transfer_coefficient();

        // Initialise previous-timestep values to current values
        mixin(InitPreviousTimestepAndSubstepArrays!(PROPS, "nCELLS + 2"));
        mixin(InitPreviousTimestepAndSubstepArrays!(["v"], "nCELLS + 1"));
    }

    // ------------------- NODE REQUIRED METHODS ---------------------

    // PisoSolver does not have any ODEs that need to be updated using
    // 'update_balance' in the q1d framework. These ODEs are updated in the
    // 'PisoNode's.
    static uint order() { return 0; }

    override @property string type() { return "PisoSolver"; }

    override void update(const ref Real[] state, const Real t)
    {
        // PISO has a high cost per step but is stable for high timesteps, so we
        // only update the piso solver every 'PISO_PERIOD' (approx)
        double piso_delta_t = t - last_update_time;
        if(piso_delta_t >= PISO_PERIOD)
        {
            integrate(piso_delta_t);
            last_update_time = t;
        }

        // Substep the values that are copied into the incompressible channel
        // (this ensures stability for parallel update procedure)
        auto sub_delta_t = t - last_substep_time;
        update_substep_arrays(sub_delta_t);
        last_substep_time = t;
    }

    // PisoSolver is implicit and so does not use the standard q1d explicit
    // solution procedure. update_balance is not required.
    override void update_balance(ref Real[] balance) {}

    // Theoretically the PISO scheme is unconditionally stable, however large
    // timesteps may cause temporary overshoots of physical T and P values.
    // In future, could look at implementing a check for this.
    override void check_well_posedness() {}


    // ------------------- PISO SPECIFIC METHODS ---------------------

    void initialise_linear_sys_matrices()
    {
        A = slice!double(nCELLS-1, nCELLS-1);
        b = slice!double(nCELLS-1, 1);
        C = slice!double(nCELLS, nCELLS);
        d = slice!double(nCELLS, 1);
        f = slice!double(nCELLS, 1);
        G = slice!double(nCELLS, nCELLS);
        l = slice!double(nCELLS, 1);
        A[] = 0.0;
        C[] = 0.0;
        d[] = 0.0;
        f[] = 0.0;
        G[] = 0.0;
        l[] = 0.0;
    }

    void initialise_state_variables(Transient initial_temperature_distribution,
                                    Transient initial_pressure_distribution,
                                    Transient initial_velocity_distribution)
    {
        Real x = 0.5 * Geo.DELTA_X; // First cell position
        foreach(immutable i; 1..nCELLS+1)
        {
            p[i] = initial_pressure_distribution(x);
            T[i] = initial_temperature_distribution(x);
            v[i] = initial_velocity_distribution(x);
            x += Geo.DELTA_X;
        }

        // We don't define a specific boundary objects with their own
        // initialisation values for the PISO stream, so we just extrapolate
        p[0] = initial_pressure_distribution(0.0); // No pressure drop over first cell
        T[0] = 2 * T[1] - T[2];
        v[0] = 2 * v[1] - v[2];
        p[nCELLS+1] = 2 * p[nCELLS] - p[nCELLS-1];
        T[nCELLS+1] = 2 * T[nCELLS] - T[nCELLS-1];
        // Don't need to do v since it has one less cell
    }

    /* Updates other thermodynamic property arrays from state vars (p, T) */
    void update_derived_properties()
    {
        foreach(i; 0..nCELLS+2)
        {
            Eos.update(PT_INPUTS, p[i], T[i]);
            rho[i] = Eos.rhomass;
            mu[i]  = Eos.viscosity;
            e[i]   = Eos.umass;
            k[i]   = Eos.conductivity;
        }
    }

    /*
     * Calculates output properties specified in 'PROPS' for pressures and
     * temperatures specified in 'p' and 'T'.
     * Inputs:
     *     PROPS - array of coolprop property keys (i.e. iHmass for enthalpy)
     *     prop_arrays - variable number of double[] arrays to hold calculated
     *         properties; must be ordered the same as PROPS
     * Sample call:
     *      calculate_property_arrays(Eos, [iHmass, iDmass], p, T, h_out, rho_out);
     */
    void calculate_property_arrays(P...)(
            AbstractState Eos,
            coolprop.parameters[] PROPS,
            double[] p,
            double[] T,
            ref P prop_arrays)
    {
        assert(prop_arrays.length == PROPS.length);
        assert(p.length == T.length);
        foreach(i; 0..p.length)
        {
            Eos.update(PT_INPUTS, p[i], T[i]);
            static foreach(j; 0..prop_arrays.length)
                prop_arrays[j][i] = Eos.keyed_output(PROPS[j]);
        }
    }

    /*
     * Integrates stream using PISO scheme for timestep 'delta_t'.
     * Note that delta_t is not exactly the same value as 'PISO_PERIOD' (see
     * 'update' method for more info).
     */
    void integrate(double delta_t)
    {
        p_star[] = p[];

        auto a_i = new double[](nCELLS + 1);
        auto a_W = new double[](nCELLS + 1);
        auto a_E = new double[](nCELLS + 1);
        calculate_velocity_coefficients(delta_t, a_i, a_W, a_E);

        // Predictor step, solve discretised momentum eqs to find u_star
        formulate_momentum_balances(a_i, a_W, a_E, delta_t, A, b);
        auto u_star_temp = mldivide(A, b);
        // FIX: Would be really sweet to do this with an overload
        foreach(i; 1..nCELLS) u_star[i] = u_star_temp[i-1, 0];
        // v*_in = u_in, v*_out = u_out (Sections 9.3 and 9.4 Versteeg)
        u_star[0] = v[0];
        u_star[$-1] = v[$-1];

        // Corrector step 1, first pressure correction equation
        calculate_pressure_correction_coefficients_matrix(a_i, C);
        formulate_first_pressure_correction_equations(delta_t, d);
        auto p_dash_temp = mldivide(C, d);
        foreach(i; 1..nCELLS+1) p_dash[i] = p_dash_temp[i-1, 0];
        p_star_star[] = p_star[] + p_dash[];

        // Inlet (p[0]) pressure is not corrected since all pressures are eventually normalised to it
        // Outlet pressure is set via linear interpolation (only used for plotting)
        p_star_star[$-1] = 2 * p_star_star[$-2] - p_star_star[$-3];
        // Correct velocities
        u_star_star[1..nCELLS] =
            u_star[1..nCELLS] + Geo.A_FLOW * (p_dash[1..nCELLS] - p_dash[2..nCELLS+1]) / a_i[1..nCELLS];
        // Again, v**_in = u_in, v**_out = u_out (Sections 9.3 and 9.4 Versteeg)
        u_star_star[0] = v[0];
        u_star_star[$-1] = v[$-1];

        // Corrector step 2, solve second pressure correction equation
        formulate_second_pressure_correction_equations(a_i, a_W, a_E, delta_t, f);
        auto p_dash_dash_temp = mldivide(C, f);
        foreach(i; 1..nCELLS+1) p_dash_dash[i] = p_dash_dash_temp[i-1, 0];
        p_star_star_star[] = p_star_star[] + p_dash_dash[];
        // Outlet BC treated same as in corrector step 1
        p_star_star_star[$-1] = 2 * p_star_star_star[$-2] - p_star_star_star[$-3];
        // Correct velocities
        u_star_star_star[1..nCELLS] = u_star_star[1..nCELLS]
            + (   a_W[1..nCELLS] * (u_star_star[0..nCELLS-1] - u_star[0..nCELLS-1])
                + a_E[1..nCELLS] * (u_star_star[2..nCELLS+1] - u_star[2..nCELLS+1]) )
               / a_i[1..nCELLS]
            + Geo.A_FLOW * (p_dash_dash[1..nCELLS] - p_dash_dash[2..nCELLS+1]) / a_i[1..nCELLS];
        // Again, v***_in = u_in
        u_star_star_star[0] = v[0];
        u_star_star_star[$-1] = 2 * u_star_star_star[$-2] - u_star_star_star[$-3];
        // Force p[1] = p_in. PISO calculates a relative pressure field so we
        // need to force the first cell to the inlet pressure
        auto p_shift = p[0] - p_star_star_star[1];
        p_star_star_star[1..$] += p_shift;

        // Energy update
        formulate_energy_equation(delta_t, G, l);
        auto e_temp = mldivide(G, l);
        foreach(i; 1..nCELLS+1) e[i] = e_temp[i-1, 0];
        // For the moment, assume constant gradient at the outlet
        e[$-1] = 2 * e[$-2] - e[$-3];
        // Update temperatures based on internal energies
        foreach(I; 1..nCELLS+2) T[I] = calculate_t_from_p_e(Eos, p[I], e[I], T[I]);

        // Update state and derived variables
        // This differs from Versteeg in that it comes after the energy update,
        // which means that we calculate the fluxes with previous timestep
        // values, which is the typical approach for implicit methods (and req'd
        // for stability)
        v[] = u_star_star_star[];
        p[] = p_star_star_star[];
        update_derived_properties();

        // Compute next-timestep heat transfer coefficients so these are ready
        // when 'update_fluxes' is called in the main code
        htc[1..nCELLS+1] = compute_heat_transfer_coefficient();

        // Update mass flow rates and velocities at scalar nodes for the main q1d code
        update_velocity_and_mass_flow_at_scalar_nodes();

        // Time derivatives for substepping
        calculate_state_derivatives(delta_t);

        // Advance previous-timestep values
        p0[]    = p[];
        T0[]    = T[];
        v0[]    = v[];
        rho0[]  = rho[];
        e0[]    = e[];
        mu0[]   = mu[];
        k0[]    = k[];
        mdot0[] = mdot[];
        htc0[]  = htc[];
        v_scalar0[] = v_scalar[];
    }

    /* Find time derivatives of state variables. Used to implement substepping. */
    void calculate_state_derivatives(double delta_t)
    {
        dpdt[]        = (p[]         - p0[])        / delta_t;
        dTdt[]        = (T[]         - T0[])        / delta_t;
        dvdt[]        = (v[]         - v0[])        / delta_t;
        drhodt[]      = (rho[]       - rho0[])      / delta_t;
        dedt[]        = (e[]         - e0[])        / delta_t;
        dmudt[]       = (mu[]        - mu0[])       / delta_t;
        dkdt[]        = (k[]         - k0[])        / delta_t;
        dmdotdt[]     = (mdot[]      - mdot0[])     / delta_t;
        dhtcdt[]      = (htc[]       - htc0[])      / delta_t;
        dv_scalardt[] = (v_scalar[]  - v_scalar0[]) / delta_t;
    }

    void update_substep_arrays(double sub_delta_t)
    {
        p_sub[]        += (dpdt[]        * sub_delta_t);
        T_sub[]        += (dTdt[]        * sub_delta_t);
        v_sub[]        += (dvdt[]        * sub_delta_t);
        rho_sub[]      += (drhodt[]      * sub_delta_t);
        e_sub[]        += (dedt[]        * sub_delta_t);
        mu_sub[]       += (dmudt[]       * sub_delta_t);
        k_sub[]        += (dkdt[]        * sub_delta_t);
        mdot_sub[]     += (dmdotdt[]     * sub_delta_t);
        htc_sub[]      += (dhtcdt[]      * sub_delta_t);
        v_scalar_sub[] += (dv_scalardt[] * sub_delta_t);
    }

    /*
     * Calculates W and E face coefficients using the specified flux calculation
     * method
     */
    void calculate_neighbour_coefficients(
            double f_w, double f_e, double d_w, double d_e,
            ref double a_W, ref double a_E)
    {
        import std.math: fmax;
        if (Params.FLUX_METHOD == "upwind")
        {
            a_W = d_w + fmax(f_w, 0.0);
            a_E = d_e + fmax(-f_e, 0.0);
        }
        else
        {
            import std.format;
            throw new Exception(format!(
                "Flux method %s not implemented. " ~
                "Check spelling and case")(Params.FLUX_METHOD));
        }
    }

    /*
     * Calculates Darcy friction factor.
     * Valid for channels of hydraulic diameter 'dh' with surface roughness 'epsilon'.
     */
    double calculate_friction_factor(double re, double epsilon, double dh)
    {
        switch(Params.FR_CORRELATION)
        {
            case "bellos":
            {
                // Bellos (2018) formula (valid for all flow regimes)
                auto a = 1 / ( 1 + (re / 2712).pow(8.4) );
                auto b = 1 / ( 1 + (re / (150 + (dh / epsilon))).pow(1.8) );
                auto f = (64 / re).pow(a) * (0.75 * log(re / 5.37)).pow(2 * (a-1) * b)
                         * (0.88 * log(6.82 * dh / epsilon)).pow(2 * (a-1) * (1-b));
                return f;
            }
            default:
            {
                import std.format;
                throw new Exception(format!(
                    "Friction factor correlation correlation %s not implemented. " ~
                    "Check spelling and case.")(Params.FR_CORRELATION));
            }
        }
    }

    /* Calculates local Nusselt number using specified correlation */
    double calculate_nusselt_number(string nu_correlation)
    {
        switch(nu_correlation) // TODO: Just use 'Params.NU_CORRELATION'?
        {
            case "laminar-semicircle-constant-q":
            {
                // Fully-developed laminar flow in straight semicircular duct with
                // constant heat flux
                return 4.089;
            }
            default:
            {
                import std.format;
                throw new Exception(format!(
                    "Nusselt number correlation %s not implemented. " ~
                    "Check spelling and case.")(nu_correlation));
            }
        }
    }

    /*
     * Calculates coefficients a_W, a_i (a_P), and a_E for velocity terms in
     * momentum balances.
     * Used to formulate system of momentum equations and in source terms in
     * second pressure correction equation.
     */
    void calculate_velocity_coefficients(
            double delta_t,
            ref double[] a_i,
            ref double[] a_W,
            ref double[] a_E)
    {
        double f_w, f_e, d_w, d_e;
        foreach(i; 0..nCELLS+1)
        {
            // Big I indexes scalar cells, small i indexes velocity cells
            auto I = i + 1; // Add 1 since backward staggered grid is used

            // Convective fluxes and diffusive conductances (Versteeg Eqs 6.11)
            // Indexing W w P e E reflects Fig 6.3 Versteeg
            if (i == 0)
            {
                f_w = Geo.A_FLOW * ((0.25 * rho[I] + 0.75 * rho[I-1]) * v[i]);
                f_e = Geo.A_FLOW * (0.5 * ( ((rho[I+1] + rho[I]) * v[i+1] / 2) + ((rho[I] + rho[I-1]) * v[i] / 2) ));
            }
            else if (i == nCELLS)
            {
                f_w = Geo.A_FLOW * (0.5 * ( ((rho[I] + rho[I-1]) * v[i] / 2) + ((rho[I-1] + rho[I-2]) * v[i-1] / 2) ));
                f_e = Geo.A_FLOW * ((0.75 * rho[I] + 0.25 * rho[I-1]) * v[i]);
            }
            else
            {
                f_w = Geo.A_FLOW * (0.5 * ( ((rho[I] + rho[I-1]) * v[i] / 2) + ((rho[I-1] + rho[I-2]) * v[i-1] / 2) ));
                f_e = Geo.A_FLOW * (0.5 * ( ((rho[I+1] + rho[I]) * v[i+1] / 2) + ((rho[I] + rho[I-1]) * v[i] / 2) ));
            }
            if (Params.VISCOUS_FX_FLAG)
            {
                d_w = Geo.A_FLOW * mu[I-1] / Geo.DELTA_X;
                d_e = Geo.A_FLOW * mu[I] / Geo.DELTA_X;
            }
            else
            {
                d_w = 0.0;
                d_e = 0.0;
            }
            calculate_neighbour_coefficients(f_w, f_e, d_w, d_e, a_W[i], a_E[i]);

            // [FIX: Since the energy update is not yet performed, rho and rho_0
            // are the same, is it better to actually solve the energy equation
            // first?]
            auto a_P_c = 0.5 * (rho0[I] + rho0[I-1]) * Geo.V_CELL / delta_t;
            a_i[i] = a_W[i] + a_E[i] + f_e - f_w + a_P_c;
        }
    }

    /*
     * Formulates momentum balances as tridiagonal matrix system
     *     A * u_star = b,
     * which is solved to find corrected velocities u_star (Eq 6.12 in Versteeg).
     */
    void formulate_momentum_balances(
            double[] a_i, double[] a_W, double[] a_E,
            double delta_t,
            ref tdarr A, ref tdarr b)
    {
        foreach(i; 1..nCELLS)
        {
            // Big I indexes scalar cells, small i indexes velocity cells
            auto I = i + 1; // Add 1 since backward staggered grid is used

            // Compute source terms
            auto re = fabs((rho[I] + rho[I-1]) * v[i] * Geo.L_C / (mu[I] + mu[I-1]));
            auto f = calculate_friction_factor(re, Geo.EPSILON, Geo.D_H);
            auto b_i = - (0.25 * f * (rho[I] + rho[I-1]) * v[i] * fabs(v[i]) / Geo.D_H) * Geo.V_CELL;
            auto a_P0 = 0.5 * (rho0[I] + rho0[I-1]) * Geo.V_CELL / delta_t;
            // This is the known part in eq 6.12 (with the transient term added on)
            auto c_i = (p_star[I-1] - p_star[I]) * Geo.A_FLOW + b_i + (a_P0 * v0[i]);

            // Formulate root finding problem
            // Base index is i-1 since loop starts at 1 to account for boundary cells
            if (i == 1)
            {
                // Inlet boundary, u_W = u_In, so add a_W * u_W to constant term
                c_i += a_W[i] * v[0];
                A[i-1, i] = -a_E[i];
            }
            else if (i == nCELLS - 1)
            {
                // Outlet boundary, u_E = u_Out, so add a_E * u_E to constant term
                c_i += a_E[i] * v[$-1];
                A[i-1, i-2] = -a_W[i];
            }
            else
            {
                // Normal cell, put both neighbour coefficients into the A matrix
                A[i-1, i-2] = -a_W[i];
                A[i-1, i] = -a_E[i];
            }
            A[i-1, i-1] = a_i[i];
            b[i-1, 0] = c_i;
        }
    }

    /*
     * Calculates coefficients a_I-1, a_I, and a_I+1 for p_dash terms in both
     * pressure correction equations (Equations 6.32 and 6.57 in Versteeg).
     * Uses these coefficients to formulate C matrix of both pressure
     * correction equations,
     *     C * p_dash = d
     * and
     *     C * p_dash_dash = f.
     */
    void calculate_pressure_correction_coefficients_matrix(double[] a_i, ref tdarr C)
    {
        // Indexing starts at 1 since cell 0 is the ghost cell at the inlet
        foreach(I; 1..nCELLS+1)
        {
            // Big I indexes scalar cells, small i indexes velocity cells
            auto i = I - 1;

            auto a_Im1 = 0.5 * (rho[I] + rho[I-1]) * Geo.A_FLOW.pow(2) / a_i[i];
            auto a_Ip1 = 0.5 * (rho[I+1] + rho[I]) * Geo.A_FLOW.pow(2) / a_i[i+1];
            auto a_I = a_Im1 + a_Ip1;

            // Base index for C matrix is I-1 since loop accounts for boundary cells
            C[I-1, I-1] = a_I;
            if (I != 1) C[I-1, I-2] = -a_Im1; // Account for inlet boundary
            if (I != nCELLS) C[I-1, I] = -a_Ip1; // Account for outlet boundary
        }
    }

    /*
     * Formulates first pressure correction equations as tridiagonal matrix
     * system
     *     C * p_dash = d,
     * that is solved to find p_dash and thus first corrected pressures
     * p_star_star (Equation 6.32 in Versteeg).
     * Only finds d vector since C has already been calculated.
     */
    void formulate_first_pressure_correction_equations(double delta_t, ref tdarr d)
    {
        foreach(I; 1..nCELLS+1)
        {
            // Big I indexes scalar cells, small i indexes velocity cells
            auto i = I - 1;
            auto b_I_dash = 0.5 * Geo.A_FLOW *
                       ( (rho[I] + rho[I-1]) * u_star[i] - (rho[I+1] + rho[I]) * u_star[i+1] )
                       + (rho0[I] - rho[I]) * Geo.V_CELL / delta_t;
            d[I-1, 0] = b_I_dash;
        }
    }

    /*
     * Formulates second pressure correction equations as tridiagonal matrix
     * system
     *     C * p_dash_dash = f,
     * which is solved to find second corrected pressures p_star_star_star.
     * Eq 6.57 in Versteeg.
     */
    void formulate_second_pressure_correction_equations(
            double[] a_i, double[] a_W, double[] a_E,
            double delta_t,
            ref tdarr f)
    {
        foreach(I; 1..nCELLS+1)
        {
            // Big I indexes scalar cells, small i indexes velocity cells
            auto i = I - 1;

            // Calculate neighbour coefficients
            double b_I_dash_dash_i, b_I_dash_dash_ip1;
            if (I == 1)
            {
                // Inlet BC, west-face b_I_dash_dash term is 0
                b_I_dash_dash_i =
                    ( 0.5 * (rho[I] + rho[I-1]) * Geo.A_FLOW / a_i[i] )
                    * (  a_W[i] * (u_star_star[i] - u_star[i])
                       + a_E[i] * (u_star_star[i+1] - u_star[i+1]) );
                b_I_dash_dash_ip1 =
                    - ( 0.5 * (rho[I+1] + rho[I]) * Geo.A_FLOW / a_i[i+1] )
                    * (  a_W[i+1] * (u_star_star[i] - u_star[i])
                       + a_E[i+1] * (u_star_star[i+2] - u_star[i+2]) );
            }

            else if (I == nCELLS)
            {
                // Outlet BC, east-face b_I_dash_dash term is 0
                b_I_dash_dash_i =
                    ( 0.5 * (rho[I] + rho[I-1]) * Geo.A_FLOW / a_i[i] )
                    * (  a_W[i] * (u_star_star[i-1] - u_star[i-1])
                       + a_E[i] * (u_star_star[i+1] - u_star[i+1]) );
                b_I_dash_dash_ip1 =
                    - ( 0.5 * (rho[I+1] + rho[I]) * Geo.A_FLOW / a_i[i+1] )
                    * (  a_W[i+1] * (u_star_star[i] - u_star[i])
                       + a_E[i+1] * (u_star_star[i+1] - u_star[i+1]) );
            }

            else
            {
                b_I_dash_dash_i =
                    ( 0.5 * (rho[I] + rho[I-1]) * Geo.A_FLOW / a_i[i] )
                    * (  a_W[i] * (u_star_star[i-1] - u_star[i-1])
                       + a_E[i] * (u_star_star[i+1] - u_star[i+1]) );
                b_I_dash_dash_ip1 =
                    - ( 0.5 * (rho[I+1] + rho[I]) * Geo.A_FLOW / a_i[i+1] )
                    * (  a_W[i+1] * (u_star_star[i] - u_star[i])
                       + a_E[i+1] * (u_star_star[i+2] - u_star[i+2]) );
            }

            // Versteeg doesn't include the continuity terms in the source for
            // the 2nd pressure correction eq since the v** velocity field must
            // satisfy continuity (and thus the continuity terms sum to 0).
            // However, this is actually only guaranteed for interior cells, not
            // cells adjacent to the boundary cells (i.e. cell 1 and cell
            // nCELLS), so I include it.
            auto b_I_dash_dash = b_I_dash_dash_i + b_I_dash_dash_ip1
                            + 0.5 * Geo.A_FLOW * ( (rho[I] + rho[I-1]) * u_star_star[i] -
                                                   (rho[I+1] + rho[I]) * u_star_star[i+1] )
                            + (rho0[I] - rho[I]) * Geo.V_CELL / delta_t;
            f[I-1, 0] = b_I_dash_dash;
        }
    }

    /*
     * Formulates transient energy balance equation as matrix system
     *     G * e = l.
     */
    void formulate_energy_equation(double delta_t, ref tdarr G, ref tdarr l)
    {
        // Subscript s (for 'scalar volume') used to differentiate these
        // arrays from 'a_i', 'a_W', and 'a_E', which are used for velocity volumes
        auto a_P_s = new double[](nCELLS + 2);
        auto a_W_s = new double[](nCELLS + 2);
        auto a_E_s = new double[](nCELLS + 2);

        // Calculate linearised source
        auto s_u = new double[](nCELLS + 2);
        auto s_p = new double[](nCELLS + 2);
        calculate_linearised_energy_source_terms(s_u, s_p);

        // Only solve the energy equation for internal scalar cells
        foreach(I; 1..nCELLS+1)
        {
            auto i = I - 1;
            auto f_w = Geo.A_FLOW * (v[i] * (rho[I-1] + rho[I]) / 2);
            auto f_e = Geo.A_FLOW * (v[i+1] * (rho[I] + rho[I+1]) / 2);
            double d_w, d_e;
            if (Params.VISCOUS_FX_FLAG)
            {
                // FIX: These need to be reformulated for energy rather than temp gradient
                // auto d_w = Geo.A_FLOW * (k[I-1] + k[I]) / (2 * Geo.DELTA_X);
                // auto d_e = Geo.A_FLOW * (k[I] + k[I+1]) / (2 * Geo.DELTA_X);
                d_w = 0.0;
                d_e = 0.0;
            }
            else
            {
                d_w = 0.0;
                d_e = 0.0;
            }
            calculate_neighbour_coefficients(f_w, f_e, d_w, d_e, a_W_s[I], a_E_s[I]);
            auto a_P0 = rho0[I] * Geo.V_CELL / delta_t;
            // [FIX: Here, rho should actually be a function of the new e. Are
            // there any approximations that I can use to capture this behaviour?]
            auto a_P_c = rho[I] * Geo.V_CELL / delta_t;

            a_P_s[I] = a_W_s[I] + a_E_s[I] + f_e - f_w + a_P_c - s_p[I];
            auto c_I = (a_P0 * e0[I]) + s_u[I];

            G[I-1, I-1] = a_P_s[I];
            // Inlet boundary, add a_W_s * e_W to constant term
            if (I == 1) c_I += a_W_s[I] * e[I-1];
            else G[I-1, I-2] = -a_W_s[I];
            // Outlet boundary, add a_E_s * e_E to constant term
            if (I == nCELLS) c_I += a_E_s[I] * e[I+1];
            else G[I-1, I] = -a_E_s[I];
            l[I-1, 0] = c_I;
        }
    }

    /*
     * Evaluates source term of energy equation,
     *     b = A_flow * ( (p_i * u_i) - (p_i+1 * u_i+1) )
     *         - ( N_chans * (Nu * k / L_C) * Perim * Delta X * (T - T_wall) )
     *
     * NOTE: Now doesn't use T_wall, takes q_dot from main code!
     *
     * Takes thermo variables as inputs so that partial derivative of b w.r.t. e
     * can be calculated.
     */
    double[] calculate_energy_source_term(double[] p, double[] T, double[] rho,
                                          double[] mu, double[] e, double[] k)
    {
        // Values of Re, Nu, and f at the scalar nodes
        auto re = new double[](nCELLS + 2);
        auto f  = new double[](nCELLS + 2);
        auto nu = new double[](nCELLS + 2);
        re[1..nCELLS+1] = rho[1..nCELLS+1] * 0.5 * (v[0..nCELLS] + v[1..nCELLS+1])
                         * Geo.L_C / mu[1..nCELLS+1]; // take abs value in next loop
        foreach(I; 1..nCELLS+1)
        {
            re[I] = fabs(re[I]);
            f[I]  = calculate_friction_factor(re[I], Geo.EPSILON, Geo.D_H);
            nu[I] = calculate_nusselt_number(Params.NU_CORRELATION);
        }

        // Presure work term
        auto b_1 = new double[](nCELLS + 2);
        b_1[] = 0.0;
        b_1[1..nCELLS+1] = Geo.A_FLOW * p[1..nCELLS+1] * (v[0..nCELLS] - v[1..nCELLS+1]);

        // q_dot is calculated in the incompressible convective interface that
        // connects the stream with a wall
        auto s_out = new double[](nCELLS + 2);
        s_out[] = b_1[] + q_dot[];

        return s_out;
    }

    /*
     * Computes heat transfer coefficients (Nu * k / L_c) at the end of each
     * timestep so that these can be read by the main q1d code and used to
     * calculate cross-wall heat transfer rates in heat exchangers and other
     * components.
     */
    double[] compute_heat_transfer_coefficient()
    {
        auto nu = new double[](nCELLS + 2);
        auto htc_out = new double[](nCELLS); // Don't calculate for boundary cells
        foreach(I; 1..nCELLS+1) nu[I] = calculate_nusselt_number(Params.NU_CORRELATION);
        htc_out[] = nu[1..nCELLS+1] * k[1..nCELLS+1] / Geo.L_C;
        return htc_out;
    }

    /*
     * Updates velocity values at the scalar nodes using linear interpolation.
     * These values are used to interface with the main q1d code (which uses a
     * collocated velocity grid).
     * Also updates mass flow rates at scalar nodes from these velocities.
     */
    void update_velocity_and_mass_flow_at_scalar_nodes()
    {
        // First, update the velocities then use them to update the mass flows
        // For the boundaries, we just assume zero gradient
        v_scalar[0] = v[0];
        v_scalar[$-1] = v[$-1];
        foreach(i; 1..nCELLS+1) { v_scalar[i] = 0.5 * (v[i-1] + v[i]); }
        mdot[] = v_scalar[] * rho[] * Geo.A_FLOW;
    }

    /*
     * Calculates linearisation of source terms in energy equation,
     *     b ~= s_u + s_p * e.
     * Linearisation is performed as
     *     b ~= b_0 + db/de|p * (e - e_0),
     * so
     *     s_u = b_0 - db/de|p * e_0
     * and
     *     s_p = db/de|p.
     *
     * Requires partial derivative of b w.r.T. e.
     * Since (rho, e) updates cannot be used for incompressible fluids, we
     * calculate the derivatives by perturbing T and computing the resulting
     * change in e. This means that we have the partial derivative of b w.r.T.
     * e at constant p.
     */
    void calculate_linearised_energy_source_terms(ref double[] s_u, ref double[] s_p)
    {
        s_u = calculate_energy_source_term(p, T, rho, mu, e, k);

        // Initialise arrays used in finite differences
        auto t_plus    = new double[](nCELLS + 2);
        auto t_minus   = new double[](nCELLS + 2);
        auto rho_plus  = new double[](nCELLS + 2);
        auto rho_minus = new double[](nCELLS + 2);
        auto e_plus    = new double[](nCELLS + 2);
        auto e_minus   = new double[](nCELLS + 2);
        auto mu_plus   = new double[](nCELLS + 2);
        auto mu_minus  = new double[](nCELLS + 2);
        auto k_plus    = new double[](nCELLS + 2);
        auto k_minus   = new double[](nCELLS + 2);

        auto PROPS = [iDmass, iviscosity, iUmass, iconductivity];

        // FIX: Do I need to perturb pressure as well?
        // Incompressible so effects should be very small, but would be useful
        // if code is generalised to compressible

        // Calculate source term using central differencing
        double H = 1E-8;
        t_plus[]  = T[] * (1.0+H);
        t_minus[] = T[] * (1.0-H);
        calculate_property_arrays(Eos, PROPS, p, t_plus, rho_plus, mu_plus,
                                  e_plus, k_plus);
        auto b_plus = calculate_energy_source_term(p, t_plus, rho_plus, mu_plus,
                                                   e_plus, k_plus);
        calculate_property_arrays(Eos, PROPS, p, t_minus, rho_minus, mu_minus,
                                  e_minus, k_minus);
        auto b_minus = calculate_energy_source_term(p, t_minus, rho_minus, mu_minus,
                                                    e_minus, k_minus);
        auto delta_e = new double[](nCELLS + 2);
        delta_e[] = 0.5 * (e_plus[] - e_minus[]);
        s_p[] = (b_plus[] - b_minus[]) / delta_e[];
        // Modify s_u (see function description)
        s_u[] -= s_p[] * e[];
    }

    /*
     * Evaluates momentum equation over stream to check correctness of
     * steady-state solution.
     * A momentum balance is not evaluated over the first cell since this cell
     * is only used to hold boundary values.
     */
    void check_steady_momentum_balances()
    {
        import std.stdio: writeln;

        if(Params.FLUX_METHOD != "upwind")
        {
            import std.format;
            throw new Exception(format!(
                "\nMomentum balance check assumes that upwinding is used. "
                ~ "This function will not give meaningful results using flux "
                ~ "method %s.")(Params.FLUX_METHOD));
        }
        if(Params.VISCOUS_FX_FLAG)
        {
            writeln("\n\n WARNING: Momentum check assumes invsicid behaviour. "
                    ~ "You have viscous effects enabled for this simulation.");
        }

        auto errors = new double[nCELLS];

        // First cell is always 0, last cell is extrapolated so don't worry about it
        foreach(i; 1..nCELLS)
        {
            auto I = i + 1;
            auto re = fabs((rho[I] + rho[I-1]) * v[i] * Geo.L_C / (mu[I] + mu[I-1]));
            auto f = calculate_friction_factor(re, Geo.EPSILON, Geo.D_H);
            auto s_dx = - Geo.DELTA_X * f * (rho[I] + rho[I-1]) * v[i] * fabs(v[i])
                        / (4 * Geo.D_H);
            errors[i-1] = (p[I-1] - p[I]) / s_dx + 1;
        }
        writeln("\nMomentum error array: (normalised)");
        writeln(errors[0..$-2]);
    }

    /* Proper version of this not currently working. */
    void check_steady_energy_balances()
    {
        throw new Exception("This check currently needs to be fixed. T_wall is "
                ~ "no longer updated by this code, so this check is not valid "
                ~ "(T_wall is taken from the linked wall model from the main "
                ~ "compressible code).");
    }

    /*
     * Evaluates energy equation for each cell to check correctness of steady-state
     * solution. Needs to be revised so that T_wall isn't used.
     */
    // void check_steady_energy_balances()
    // {
        // import std.stdio: writeln;

        // // [FIX: This check isn't actually quite right (though it is close).
        // //  Need to add p div (v) terms at faces.]
        // if(Params.FLUX_METHOD != "upwind")
        // {
            // import std.format;
            // throw new Exception(format!(
                // "\nEnergy balance check assumes that upwinding is used. "
                // ~ "This function will not give meaningful results using flux "
                // ~ "method %s.")(Params.FLUX_METHOD));
        // }
        // if(Params.VISCOUS_FX_FLAG)
        // {
            // writeln("\n\n WARNING: Momentum check assumes invsicid behaviour. "
                    // ~ "You have viscous effects enabled for this simulation.");
        // }

        // auto errors = new double[nCELLS];
        // auto q_vec = new double[nCELLS];
        // foreach(I; 1..nCELLS+1)
        // {
            // auto i = I - 1;
            // auto nu = calculate_nusselt_number(Params.NU_CORRELATION);
            // auto s = - Geo.nCHANNELS * nu * k[I] * Geo.P * Geo.DELTA_X
                     // * (T[I] - T_wall[I]) / Geo.L_C;
            // q_vec[I-1] = s;
            // errors[I-1] = (   (rho[I-1] + rho[I]) * e[I-1] * v[i]
                            // - (rho[I] + rho[I+1]) * e[I] * v[i+1] )
                          // * 0.5 * Geo.A_FLOW / s + 1;
        // }
        // writeln("\nEnergy error array: (normalised)");
        // writeln(errors);
    // }
}

/*
 * Newton solver to find T based on known (p, e) inputs for incompressible fluid.
 * Inputs:
 *     p0 - known pressure
 *     e0 - known internal energy
 *     T  - guess of temperature
 *
 * This function is currently only used in PisoSolver, so it lives in
 * piso_solver.d for the moment. If it becomes more widely used, it should
 * potentially be rehomed.
 */
double calculate_t_from_p_e(
        AbstractState eos,
        double p0,
        double e0,
        double T,
        double tol=1.00E-4,
        uint max_iter=1_000_000)
{
    import std.math: fabs;
    double H = 1E-8;
    double e, e_plus, e_minus, f_dash, delta;
    foreach(i; 0..max_iter)
    {
        // Value of e for current estimate T
        eos.update(PT_INPUTS, p0, T);
        e = eos.keyed_output(iUmass);

        // Compute de/dt|p
        eos.update(PT_INPUTS, p0, (1+H) * T);
        e_plus = eos.keyed_output(iUmass);
        eos.update(PT_INPUTS, p0, (1-H) * T);
        e_minus = eos.keyed_output(iUmass);
        f_dash = (e_plus - e_minus) / (2*H*T);

        delta = (e0 - e) / f_dash;
        T += delta;
        if (fabs(delta) < tol) return T;
    }

    // If loop falls through, root finder has failed to converge
    throw new Exception(
        "Root-finding problem to calculate T from (e, p) did not converge.");
}

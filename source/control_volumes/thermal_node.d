module q1dcfd.control_volumes.thermal_node ;

import q1dcfd.control_volumes.node: Node ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.domains.domain: Domain ;
import q1dcfd.config: Real, NUMERICAL_TEMPERATURE_LIMIT, Address ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.utils.metaprogramming: PopulateStateFields, PopulateStateMap;

import std.format ;

/**
 * ThermalNode
 *
 * Base class for a solid element such as a pipe or heat exchanger wall, or
 * a lumped thermal mass
 */
abstract class ThermalNode: Node 
{
    // generate property fields
    enum STATE_FIELDS =
    [
        "T", // temperature (K)
        "heat_flux" // energy entering the volume (J/s)
    ] ;

    mixin(PopulateStateFields!(STATE_FIELDS)) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    // thermal conductivity (W/m.K)
    Real k ; 

    this(const Address address, const NodeGeometry geometry)
    {
        super(address, geometry);
    }

    override void check_well_posedness()
    {
        assert(this.T > 0 && T < NUMERICAL_TEMPERATURE_LIMIT, 
            format!"Temperature %f outside of expected bounds in (%s)"
            (T, identify)) ;
    }
}

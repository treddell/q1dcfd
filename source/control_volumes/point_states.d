module q1dcfd.control_volumes.point_states ;

import q1dcfd.utils.math: square ;
import q1dcfd.config: Real ;
import q1dcfd.correlations.standard_corr: FrictionFactorCorrelation,
    friction_factor_map ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.eos: EOSBase, generate_eos ;

import std.math ;
import std.format ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

enum VISCID
{
    FALSE = 0,
    TRUE
} ;

/** 
 * A fluid point state is a dimensionless node which forms a sub-state
 * of more complicated models, has no geometry, or balance equations
 */
abstract class FluidPointState(VISCID viscid)
{
    EOSBase eos ; // equation of state calculator

    // Fluid attributes
    Real rho ; // density (kg/m3)
    Real vol ; // specific volume (m3/kg)
    Real e ; // specific internal energy (J/kg)
    Real E ; // specific total energy (J/kg)
    Real ke ; // specific kinetic energy (J/kg)
    Real p ; // static pressure (Pa)
    Real h ; // static enthalpy (J/kg)
    Real H ; // total enthalpy (J/kg)
    Real T ; // total temperature (K)
    Real v ; // inertial flow velocity (m/s)
    Real a ; // speed of sound (m/s)
    Real M ; // Mach number (-)
    
    static if(viscid)
    {
        FrictionFactorCorrelation f_corr ;
        NodeGeometry geometry ;
        Real cp ; // specific heat capacity (J/kg.K)
        Real dv ; // dynamic viscosity (kg/s.m)
        Real k ; // thermal conductivity (W/m.K)
        Real pr;  // Prandtl number  (-)
        Real re;  // Reynolds number (-)
        Real f;  // friction factor (-)
    }
    
    Real[3] cons_state ; // conserved property vector

    private immutable string _id ; // identifier for debugging purposes
    string identify() {return _id ;}

    this(EOSBase parent_eos, immutable string id = "")
    {
        this._id = id;
        this.eos = generate_eos(parent_eos.get_tabular_data,
                                parent_eos.get_backend,
                                id) ;
    }

    /**
     * Updates all properies from conservative state variables:
     * (rho, rho v, rho E)
     */
    @fastmath void update(Real[3] state)
    out 
    {
        assert(this.rho > 0, "%s has non Positive Density!".format(identify)) ;
        assert(this.e > 0, "%s has non Positive Energy!".format(identify)) ;
        assert(this.p > 0, "%s has non Positive Pressure!".format(identify)) ;
        assert(this.T > 0, "%s has non Positive Temperature!".format(identify)) ;
        assert(this.a > 0, "%s has non Positive Speed of Sound!".format(identify)) ;
    }
    do
    {
        cons_state[] = state[] ;

        rho = cons_state[0] ;
        vol = 1.0/rho ;
        v = cons_state[1]*vol ;
        ke = 0.5*v.square ;
        e = cons_state[2]*vol - ke ;

        eos.update(rho, e) ;

        h = eos.h ;
        T = eos.T ;
        a = eos.a ;

        M = fabs(v)/a ;
        p = (h - e)*rho ;
        E = e + ke ;
        H = h + ke ;

        static if(viscid)
        {
            cp = eos.cp ;
            dv = eos.dv ;
            k = eos.k ;

            pr = dv*cp/k ;
            re = rho*fabs(v)*geometry.hydr_d/dv ;
            f = f_corr.evaluate() ;
        }
    }
}

final class InviscidPointState: FluidPointState!(VISCID.FALSE)
{
    this(EOSBase eos, immutable string id = "") {super(eos, id) ; }   
}

final class ViscidPointState: FluidPointState!(VISCID.TRUE)
{
    this(EOSBase eos,
         const string f_corr_name,
         NodeGeometry geometry,
         immutable string id)
    {
        super(eos, id) ;
        this.geometry = geometry ;
        this.f_corr = friction_factor_map(this, f_corr_name) ;
    }   
}

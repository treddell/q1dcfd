module q1dcfd.code_generation.generate_metadata_body;

import q1dcfd.types;

/**
 * Automatic code generation to produce a domain constructor and handler
 *
 * Produces get/set methods, initialisers, initial condition assignmnent,
 * discretisation method, and more
 */
mixin template GenerateDomainMetadataBody(NodeModel)
{
    // Generates all the private fields of DomainMetadata
    mixin GeneratePrivateFields!(NodeModel);

    // Generate all methods which depend upon the NodeModel
    mixin GenerateTypeSpecificMethods!(NodeModel);

    // void initialise_state(ref Real[] state, ref Node[] nodes)
    mixin GenerateStateInitialiser!(NodeModel);

    // generates void initialise_interior_interfaces(Node node, Interface[] interfaces)
    mixin GenerateInteriorInterfaces!(NodeModel);

    // void set_generic_properties(Node node)
    mixin GenerateSetProperties!(NodeModel);

    // void type_initialisation(string[string] defs)
    mixin GenerateTypeInitialisation!(NodeModel);

    // Generate metadata writer
    mixin GenerateMetadataWriter!(NodeModel);

    // Generates all body depending upon UDA's

    // Generate private fields
    mixin(GenerateUDAPrivateFields!(NodeModel));

    // Generate set_properties(NodeModel node) method
    mixin(GenerateUDASetProperties!(NodeModel));

    // Generate this
    mixin(GenerateConstructor!(NodeModel));
}

/**
 * Generate the fields and assignment flags for each property of NodeModel
 *
 */
template GenerateUDAPrivateFields(NodeModel)
{
    string GenerateUDAPrivateFields()
    {
        import std.format;
        import std.traits: hasUDA, fullyQualifiedName;

        string code = " private{";
        static foreach(member ; [__traits(allMembers, NodeModel)])
        {
            static if(hasUDA!(__traits(getMember, NodeModel, member), unique)
                      || hasUDA!(__traits(getMember, NodeModel, member), required))
            {
                code ~= "%s %s ;".format(fullyQualifiedName!NodeModel, member);
                code ~= "bool %s_assigned = false;".format(member[1..$]);
            }
        }
        return code ~ "}";
    }
}

/**
 * Generates the setters which assign the defined properties of each domain to every node
 * constructed by the discretisation
 */
template GenerateUDASetProperties(NodeModel)
{
    string GenerateUDASetProperties()
    {
        import std.format;
        import std.traits: hasUDA, fullyQualifiedName;

        string code = "void set_properties(%s node, double x_left, double x_right) {"
            .format(fullyQualifiedName!NodeModel);

        static foreach(member; [__traits(allMembers, NodeModel)])
        {
            static if(hasUDA!(__traits(getMember, NodeModel, member), unique)
                      || hasUDA!(__traits(getMember, NodeModel, member), required))
            {
                code ~= "if(%s_assigned) {node.set%s(%s) ;}".format(member[1..$], member, member);
            }
        }

        return code ~ "set_generic_properties(node, x_left, x_right);}";
    }
}

/**
 * Generates the constructor of the domain metadata for NodeModel
 */
template GenerateConstructor(NodeModel)
{
    string GenerateConstructor()
    {
        import std.format;
        import std.traits;

        string code = "this(string[string] defs) {";

        static foreach(member; [__traits(allMembers, NodeModel)])
        {
            static if(hasUDA!(__traits(getMember, NodeModel, member), unique)
                      || hasUDA!(__traits(getMember, NodeModel, member), required))
            {
                code ~= "if(`%s` in defs) {%s = defs.retrieve(`%s`).required!%s;"
                    .format(member[1..$],
                            member,
                            member[1..$],
                            fullyQualifiedName!(typeof(__traits(getMember, NodeModel, member))));
                code ~= "%s_assigned = true;}".format(member[1..$]);
            }
        }

        code ~= "type_initialisation(defs);".format(fullyQualifiedName!NodeModel);

        return code ~ "}";
    }
}

/**
 * Generates all generic private fields which are not based off UDA's
 */
mixin template GeneratePrivateFields(NodeModel)
{
    private
    {

        // fields shared by all domain types
        uint n_nodes;
        string[] save;
        double[] node_coords;

        static if(NodeModel.category.is_fluid_like)
        {
            // initial conditions, properties(x)
            MathFunction initial_temperature_distribution;
            MathFunction initial_pressure_distribution;
            MathFunction initial_velocity_distribution;

            // solver type
            backend eos_backend;

            // Fluid stream which the node is contained within
            Stream stream;
        }

        static if(NodeModel.category.is_viscid)
        {
            // correlation metadata
            string f_corr_type;
            string ht_corr_type;
            bool using_thin_film_heat_transfer;
        }

        static if(NodeModel.category.is_geometric)
        {
            // domain delta x
            double length;

            static if(NodeModel.category.is_fluid_like)
            {
                import q1dcfd.types.geometry: ChannelCrossSection;
                ChannelCrossSection cross_section;
            }
            else static if(NodeModel.category.is_solid)
            {
                import q1dcfd.types.geometry: WallCrossSection;
                WallCrossSection cross_section;
            }
            else
            {
                static assert(false, "Invalid Geometry");
            }
        }

        static if (NodeModel.category == node_type.fluid_subsonic_inflow)
        {
            MathFunction state_transient_temp;
            MathFunction flow_transient_temp;
        }
    }
}

/**
 * Generates the subroutine void initialise_state(ref Real[] state, ref Node[] nodes)
 * which sets all complicated or generic properties for
 * a category of node types
 */
mixin template GenerateSetProperties(NodeModel)
{
    void set_generic_properties(Node node)
    {
        if (NodeModel.category.is_geometric)
        {
            // generates the nodal geometry TODO
            node.set_geometry(cross_section, x_left, x_right);
        }
        
        if (NodeModel.category.is_fluid_like)
        {        
            // Add the node to the stream
            // TODO (must make sure geometry and stream is built first)
            stream.add_node(node);

            // create the equation of state solver for the node
            node.set_eos(eos_backend, stream.conservative_data);
        }
    }
}

/**
 * Generates the subroutine void initialise_state(ref Real[] state, ref Node[] nodes)
 * which initialises the state of every node described by the domain
 */
mixin template GenerateStateInitialiser(NodeModel)
{
    void initialise_state(ref Real[] state)
    {
        static if (NodeModel.category.is_fluid_like)
        {
            // generate a temporary equation of state to initialise the node
            auto eos = generate_eos(stream.primitive_data,
                                    backend.TTSE_recovery,
                                    "initialisation eos of %f".format(this.toString));

            // for every node, generate its initial state from the provided temperature
            // pressure and velocity profile averaged across the node
            // Calculate the local contribution to the state space and add it to the global
            // state space
            foreach (node ; local_nodes)
            {
                static if (NodeModel.category.is_geometric)
                {
                    auto p = initial_pressure_distribution
                        .integrate(node.geometry.x_left, node.geometry.x_right)
                        /node.geometry.length;
                    
                    auto T = initial_temperature_distribution
                        .integrate(node.geometry.x_left, node.geometry.x_right)
                        /node.geometry.length;
                    
                    auto v = initial_velocity_distribution
                        .integrate(node.geometry.x_left, node.geometry.x_right)
                        /node.geometry.length;
                }
                else
                {
                    auto p = initial_pressure_distribution.evaluate(0.0);
                    auto T = initial_temperature_distribution.evaluate(0.0);
                    auto v = initial_velocity_distribution.evaluate(0.0);
                }

                // update the equation of state at the averaged values
                eos.update(p, T);

                static if (NodeModel.category == node_type.fluid_volume_interior)
                {
                    state[node.address.index_l..node.address.index_r] = [
                        eos.rho,
                        eos.rho*v,
                        eos.rho*(eos.e + 0.5*v*v)];
                }

                // Update the node
                node.update(state, 0.0);
            }
        }
    }

}

/**
 * Generates the method void void initialise_interior_interfaces(Node node, Interface[] interfaces)
 * which produces all the interior interfaces used in the domain
 */
mixin template GenerateInteriorInterfaces(NodeModel)
{
    void initialise_interior_interfaces(Interface[] interfaces)
    {
        // For the interior points of a fluid channel, want to construct all interfaces
        // except on the first and last two nodes
        static if (NodeModel.category == node_type.fluid_volume_interior)
        {
            foreach (immutable i; node_index_l..node_index_l + n_nodes - 3)
            {
                interfaces ~= new from!"q1dcfd.interfaces".TransportInterface(
                    local_nodes[m], 
                    local_nodes[m+1],
                    local_nodes[m+2], 
                    local_nodes[m+3]);
            }
        }
    }
}

/**
 * Generates the subroutine void type_initialisation(string[string] defs)
 * which is called by the constructor.
 * Used to initialise the complicated and generic properties of the domain
 * which do not depend upon UDAs
 */
mixin template GenerateTypeInitialisation(NodeModel)
{
    void type_initialisation(string[string] defs)
    {
        import q1dcfd.utils.dictionary_tools: retrieve, required, greater, map_from,
            require_empty, defaults, subset;
        import q1dcfd.interfaces: Interface;
        import q1dcfd.experimental.node: Node;

        import std.conv: to;

        // check for unrecognised fields within the dictionary
        scope(exit) defs.require_empty(this.toString);

        // get the properties which are to be recorded
        save = defs
            .retrieve("save")
            .subset(get_recordable_properties!(NodeModel))
            .defaults(NodeModel.default_recorded_fields);

        // get the number of nodes
        n_nodes = defs
            .retrieve("nodes")
            .defaults(1)
            .greater(0);

        // store initial conditions
        static if(NodeModel.category.is_fluid_like)
        {
            import q1dcfd.eos: backend;
            import q1dcfd.utils.math: MathFunction;

            initial_temperature_distribution = defs
                .retrieve("initial_temperature")
                .required!MathFunction;
            
            initial_pressure_distribution = defs
                .retrieve("initial_pressure")
                .required!MathFunction;
            
            initial_velocity_distribution = defs
                .retrieve("initial_velocity")
                .required!MathFunction;

            eos_backend = defs
                .retrieve("eos")
                .defaults(backend.TTSE_recovery)
                .required!backend;
        }

        static if(NodeModel.category.is_geometric)
        {
            import q1dcfd.simulation: Simulation;
            
            length = defs
                .retrieve("length")
                .required!double
                .greater(0.0);

            cross_section = defs.retrieve("cross_section")
                .required!string
                .map_from(Simulation.get_geometries)
                .to!(typeof(cross_section));
        }

        static if(NodeModel.category.is_viscid)
        {
            f_corr_type = defs
                .retrieve("f_corr_name")
                .defaults("SwitchFrictionFactor");
            
            ht_corr_type = defs
                .retrieve("ht_corr_name")
                .defaults("SwitchHeatTransfer");
            
            using_thin_film_heat_transfer = defs
                .retrieve("thin_film_heat_transfer")
                .defaults(true);
        }

        static if(NodeModel.category == node_type.fluid_subsonic_inflow)
        {
            // setters will have been generated, but input will be passed in
            // from definition file as 2 MathFunctions, not 4 Transients
            state_transient_temp = defs
                .retrieve("state_transient")
                .defaults("Unassigned")
                .required!MathFunction;

            flow_transient_temp = defs
                .retrieve("flow_transient")
                .defaults("Unassigned")
                .required!MathFunction;
        }
    }
}

/**
 * Generates the method which writes out the domain metadata to the meta.txt file
 * This metadata is used to load the results for post processing
 */
mixin template GenerateMetadataWriter(NodeModel)
{
    void write_out_metadata(from!"std.stdio".File file)
    {
        import std.stdio: writefln;
        file.writefln("\n[DOM]: %s", name) ;
        file.writefln("type: %s: %s", NodeModel.stringof, "str") ;
        file.writefln("index_l: %u: %s", ode_index_l, "int") ;
        file.writefln("index_r: %u: %s", ode_index_l + order, "int") ;
        file.writefln("w_index_l: %u: %s", write_index_l, "int") ;
        file.writefln("w_index_r: %u: %s", write_index_l + w_order, "int") ;
        file.writefln("n_nodes: %u: %s", n_nodes, "int") ;
        file.writefln("node_coords: %(%s, %) : %s", node_coords, "float[]") ;
        file.writefln("data_contents: %s : %s", save, "str[]") ;
    }
}

/**
 * Generates all additional methods of DomainMetadata!(NodeModel)
 * which depend upon NodeModel
 */
mixin template GenerateTypeSpecificMethods(NodeModel)
{
    // generate the stream setter
    static if(NodeModel.category.is_fluid_like)
    {
        void set_stream(Stream stream) {this.stream = stream;}
    }
}

/**
 * Returns a list of the recordable properties of a node of type NodeModel
 */
string[] get_recordable_properties(NodeModel)()
{
    import std.traits: hasUDA;

    string[] result;

    static foreach(member; [__traits(allMembers, NodeModel)])
    {
        if (hasUDA!(__traits(getMember, NodeModel, member), recordableProperty))
        {
            result ~= member;
        }
    }
    return result;
}

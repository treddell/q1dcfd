module q1dcfd.code_generation.generate_node_body;

/**
 * Uses some template black magic to generate a method which maps a string input to
 * a recordable property.
 *
 * Used to dynamically retrieve properties to save to disk, based on a configuration
 * file input, all methods with the recordableProperty flag will be mapped
 */
template GenerateStringMappings(T)
{
    string GenerateStringMappings()
    {
        import std.format ;
        import std.traits: hasUDA, ReturnType, Parameters ;

        import q1dcfd.types.udas ;
        import q1dcfd.types: Real ;
        
        auto str = "
import q1dcfd.utils.errors: MapToNonExistentField ;
double map(const string key) { switch(key) {" ;

        // looks through each member within the scope of T
        static foreach (member; __traits(allMembers, T))
        {
            // Looks for members with @recordableProperty uda's
            static if(hasUDA!(__traits(getMember, T, member), recordableProperty))
            {
                // check that uda was set on method of expected type
                static assert(is(ReturnType!(__traits(getMember, T, member)) == Real),
                              "Found recordableProperty UDA on non Real member %s.%s"
                              .format(T.stringof, member)) ;
                static assert(Parameters!(__traits(getMember, T, member)).length == 0,
                              "Found recordableProperty UDA on member %s.%s with > 0 parameters"
                              .format(T.stringof, member)) ;

                // add case to generated code
                str ~= "case `%s`: return %s() ;".format(member, member) ;
            }
        }

        return str ~=
            "default: throw new MapToNonExistentField(key, `%s`) ; }}".format(T.stringof) ;
    }
}

/**
 * Uses the ancient forbidden secrets of the elder gods to automatically
 * generate a method (check_well_posedness) which runs a series of
 * invariant checks in the out contract of the update method
 *
 * Collects all methods using the updateInvariant flag, and runs them all in a
 * single check
 */
template GenerateInvariantConditions(T)
{
    string GenerateInvariantConditions()
    {
        import std.format ;
        import std.traits: hasUDA, ReturnType, Parameters ;

        import q1dcfd.types.udas ;

        string str = "void check_well_posedness() {" ;

        // looks through every member within scope of T
        static foreach(member ; [__traits(allMembers, T)])
        {
            // checks if member carries the updateInvariant UDA
            static if(hasUDA!(__traits(getMember, T, member), updateInvariant))
            {
                // check that member function is of the expected type
                static assert(is(ReturnType!(__traits(getMember, T, member)) == void),
                              "Found updateInvariant UDA on non void member %s.%s"
                              .format(T.stringof, member)) ;
                static assert(Parameters!(__traits(getMember, T, member)).length == 0,
                              "Found updateInvariant UDA on member %s.%s with > 0 parameters"
                              .format(T.stringof, member)) ;

                // add function call to generated code
                str ~= "%s() ;".format(member) ;
            }
        }

        return str ~ "}" ;
    }
}

/**
 * Generates the getters/setters and boilerplate code for a Node field using
 * @required and or @unique flags
 */
template getter_setter_code(string field_name,
                                    bool unique,
                                    bool required)
{
    string getter_setter_code()
    {
        import std.string ;

        string preamble, setter_opening, setter_closing, uniqueness_check, required_check ;

        preamble = "
    import q1dcfd.types: AssertionResult ;
    import std.format ;
    protected bool _FIELD_assigned = false ;
    typeof(_FIELD) FIELD() {return _FIELD ;}" ;

        setter_opening = "
    void set_FIELD(typeof(_FIELD) field)
    { " ;

        if(unique)
        {
            uniqueness_check = "
        import q1dcfd.utils.errors: InvalidDefinition ;
        if(_FIELD_assigned)
        {
            throw new InvalidDefinition(`@unique %s.FIELD multiply assigned`
                                        .format(this.identify)) ;
        } ";
        }
    
        setter_closing = "
        debug
        {
            import std.conv: to ;

            import q1dcfd.utils.logging: add_message_to_log_file ;
            `%s.FIELD assigned to %s`
                .format(this.identify, field.to!string)
                .add_message_to_log_file ;
        }

        _FIELD_assigned = true ;
        _FIELD = field ;
    } " ;

        if(required)
        {
            required_check = "
    @initCondition AssertionResult assert_FIELD_assigned()
    {
        if(_FIELD_assigned)
        {
            return typeof(return)(true, ``) ;
        }
        return typeof(return)(false, `@required %s.FIELD has not been assigned`
                              .format(this.identify)) ;
    } " ;
        } 

        return [preamble, setter_opening, uniqueness_check, setter_closing, required_check]
            .join
            .replace("FIELD", field_name);
    }
}


/**
 * Loops through a type, and generates getters, setters and other boilerplate
 * for all members of that type which have @unique or @required UDA's
 */
mixin template GenerateGetterSetters(T)
{
    // import std.format ;
    import std.traits: hasUDA ;

    // Loop through every symbol in the scope of T
    static foreach(member ; [__traits(allMembers, T)])
    {
        // Look for @required or @unique UDA's
        static if(hasUDA!(__traits(getMember, T, member), unique)
                  || hasUDA!(__traits(getMember, T, member), required))
        {
            import std.algorithm.searching: canFind ;
            import std.conv: to ;

            // check that the field name is prefixed with a single underscore
            static assert(member[0] == "_".to!char && member[1] != "_".to!char) ;
            immutable string field_name = member[1..$] ;

            // check that the field is private or protected
            enum protection = __traits(getProtection, __traits(getMember, T, member)) ;
            static assert(protection == "private" || protection == "protected",
                          "fields marked with @required or @unique must be private or protected") ;

            mixin(getter_setter_code!(field_name,
                                      hasUDA!(__traits(getMember, T, member), unique),
                                      hasUDA!(__traits(getMember, T, member), required)));
        }
    }
}

/**
 * Generates the check_initialisation_conditions method, which ensures that the supplied
 * initial parameters of the system are physical and consistent
 */
template GenerateInitialisationConditions(T)
{
    string GenerateInitialisationConditions()
    {
        import std.format ;
        import std.traits: hasUDA, ReturnType, Parameters ;

        import q1dcfd.types.udas ;
        import q1dcfd.types: AssertionResult ;

        string str = "
// import q1dcfd.types: AssertionResult ;
final AssertionResult check_initialisation_conditions() 
{
    string msg = `` ;
    bool success = true ;
    AssertionResult result; " ;

        // looks through every member within scope of T
        static foreach(member ; [__traits(allMembers, T)])
        {
            // guard to protect against access errors from previous code generation
            static if(__traits(compiles, __traits(getMember, T, member)))
            {
                // checks if member carries the updateInvariant UDA
                static if(hasUDA!(__traits(getMember, T, member), initCondition))
                {
                    // check that member function is of the expected type
                    static assert(is(ReturnType!(__traits(getMember, T, member)) == AssertionResult),
                                  "Found initCondition UDA on non AssertionResult member %s.%s"
                                  .format(T.stringof, member)) ;
                    static assert(Parameters!(__traits(getMember, T, member)).length == 0,
                                  "Found initCondition UDA on member %s.%s with > 0 parameters"
                                  .format(T.stringof, member)) ;

                    // add function call to generated code
                    str ~= "
    result = %s() ;
    if(!result.success)
    {
        msg ~= result.msg ~ `\n` ;
        success = false ;
    }"
                        .format(member) ;
                }
            }
        }
        str ~= "
    return AssertionResult(success, msg) ;
}" ;
        return str ;
    }
}

/**
 * Generate all cody for the body by calling all the sub code generation routines
 */
mixin template GenerateBody(T)
{
    // Build the string->function pointers map functionality
    mixin(GenerateStringMappings!(T)) ;

    // Generate the check_well_posedness method
    mixin(GenerateInvariantConditions!(T)) ;

    // Generate getter and setter functions
    mixin GenerateGetterSetters!(T) ;

    // Generate the check_initialisation_conditions method
    mixin(GenerateInitialisationConditions!(T)) ;
}

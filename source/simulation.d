module q1dcfd.simulation;

import q1dcfd.domains.domain: Domain;
import q1dcfd.control_volumes.node: Node;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.interfaces.transport_interface: TransportInterface, FluxCalculator;
import q1dcfd.interfaces.convective_interface: ConvectiveInterface;
import q1dcfd.flux_calculators: ReconstructionScheme, FluxLimiter,
    generate_muscl_scheme_left, generate_muscl_scheme_right,
    unlimited, generate_left_sided_scheme, generate_right_sided_scheme;
import q1dcfd.numerics: Integrator, RKCK45, RK4, DOPRI;
import q1dcfd.config: Real, STEADY_SOLVER_TIMEOUT, SystemProperty,
    N_LOG_INSTRUCTIONS_RECORDED;
import q1dcfd.lconfig: ROOT_DIRECTORY;
import q1dcfd.tables.extract_table_data: TableData;
import q1dcfd.utils.logging;
import q1dcfd.utils.errors: InvalidDefinition;
import q1dcfd.types.geometry: CrossSection, Dimensionless, build_cross_section;
import q1dcfd.initialisation.parser: Contents;
import q1dcfd.domains.build_domain: build_domain, DomainContainer;
import q1dcfd.connectors.connection: Connection;
import q1dcfd.connectors.connect: connect;
import q1dcfd.utils.metaprogramming: generate_save_method;
import q1dcfd.stream: Stream;
import q1dcfd.thermal_face: ThermalFace;

import std.format;
import std.string;
import std.array: join, array;
import std.algorithm: canFind;
import std.algorithm.searching: endsWith, minIndex;
import std.algorithm.comparison: min, max;
import std.algorithm.iteration: reduce, filter, each;
import std.parallelism: totalCPUs, parallel;
import std.exception: enforce;
import std.conv: to;
import std.stdio;
import std.file;

// Using the ldc fast math attributes
version(LDC) { import ldc.attributes: fastmath;} else {enum fastmath;}

/**
 * Maps the reconstruction scheme for each type of cell face reconstruction
 */
void set_reconstruction_scheme(
    out ReconstructionScheme scheme_left, 
    out ReconstructionScheme scheme_right, 
    out ReconstructionScheme boundary_left, 
    out ReconstructionScheme boundary_right,
    FluxLimiter limiter, const string method, const Real coeff)
{
    switch(method)
    {
        case "MUSCL":
        {
            scheme_left = generate_muscl_scheme_left(limiter, coeff);
            scheme_right = generate_muscl_scheme_right(limiter, coeff);
            boundary_left = generate_left_sided_scheme(limiter);
            boundary_right = generate_right_sided_scheme(limiter); 
            break;
        }
        default:
        {
            throw new InvalidDefinition("Reconstruction scheme %s not recognised"
                                        .format(method));
        }
    }
}

/**
 * Maps the flux limiting scheme to the assigned model in the definition
 * file
 */
FluxLimiter set_flux_limiter(const string method)
{
    switch(method)
    {
        case "unlimited":
            return unlimited;

        default:
            throw new InvalidDefinition(
                format!("Flux limiter %s not recognised")(method));
    }
}

/**
 * Maps the inviscid flux calculator to the assigned model in the definition 
 * file 
 */
FluxCalculator set_inviscid_flux_calculator(const string method)
{
    import q1dcfd.flux_calculators: ausmdv, ausmpmup;

    switch(method)
    {
        case "ausmdv": return ausmdv;
        case "ausmpmup": return ausmpmup;
        default:
        {
            throw new InvalidDefinition("Flux calculator %s not recognised"
                                        .format(method));
        }
    }
}

/**
 * Returns an integrator based on the provided name and time step control 
 * arguments
 */
Integrator build_integrator(const string integrator_type,
                            const Real min_delta_t, 
                            const Real max_delta_t,
                            const Real adap_sf,
                            const Real adap_tol)
{
    switch(integrator_type)
    {
        case "RKCK45": return new RKCK45(min_delta_t, max_delta_t, adap_sf, adap_tol);
        case "RK4" : return new RK4;
        case "DOPRI": return new DOPRI(min_delta_t, max_delta_t, adap_sf, adap_tol);
        default:
        {
            throw new InvalidDefinition("Integrator %s not recognized".format(integrator_type));
        }
    }
}

/** Simulation constant parameters which are defined during initialisation
 *
 * Optional Arguments:
 * end_time             : Simulation will terminate if this time is exceeded (s)
 * max_CFL              : maximum simulation CFL number
 * steady_tol           : Maximum steady state residual
 * save_interval        : Minimum interval of simulation time required to pass 
 *     before a write to disk operation
 * integrator           : ODE integration algorithm
 * min_delta_t          : minimum time step for adaptive step algorithm
 * max_delta_t          : maximum time step for adaptive step algorithms
 * adap_sf              : safety factor for adaptive step algorithms
 * adap_tol             : tolerance for adaptive step algorithms, desired maximum 
 *     relative error between adaptive estimates
 * flux_calculator      : inviscid flux calculator
 * flux_limiter         : flux limiting scheme
 * reconstruction       : reconstruction scheme
 * reconstruction_coeff : reconstruction scheme weighting coefficient
 *
 */
final class SimulationConstants
{
    immutable
    {
        // Integration
        uint max_cpus; // maximum number of cpu's available on machine

        string results_folder; // location of the results folder

        // simulation name
        string name;

        // log file name
        debug string log_file;
    
        Real end_time; // simulation will terminate after this amount of time
        Real max_CFL; // maximum CFL number
        Real save_interval;
    
        // global flux left and right reconstruction schemes
        ReconstructionScheme reconstruction_scheme_left; 
        ReconstructionScheme reconstruction_scheme_right;
        ReconstructionScheme reconstruction_boundary_left;
        ReconstructionScheme reconstruction_boundary_right;
    }
    // ODE integration algorithm
    Integrator integrator;

    // global flux calculator method  
    FluxCalculator inviscid_flux_calculator;
    
    // psuedo steady marching tolerance, maximum relative difference in iterates
    const Real psuedo_steady_tol;

    this(string[string] args)
    {
        import q1dcfd.utils.dictionary_tools: retrieve, defaults, greater, geq;
        import std.datetime;

        ReconstructionScheme temp_reconstruction_scheme_left; 
        ReconstructionScheme temp_reconstruction_scheme_right;
        ReconstructionScheme temp_reconstruction_boundary_left;
        ReconstructionScheme temp_reconstruction_boundary_right;

        set_reconstruction_scheme(
            temp_reconstruction_scheme_left, 
            temp_reconstruction_scheme_right, 
            temp_reconstruction_boundary_left, 
            temp_reconstruction_boundary_right, 
            args.retrieve("flux_limiter").defaults("unlimited").set_flux_limiter,
            args.retrieve("reconstruction").defaults("MUSCL"), 
            args.retrieve("reconstruction_coeff").defaults(1.0/3.0));

        reconstruction_scheme_left = cast(immutable) temp_reconstruction_scheme_left;
        reconstruction_scheme_right = cast(immutable) temp_reconstruction_scheme_right;
        reconstruction_boundary_left = cast(immutable) temp_reconstruction_boundary_left;
        reconstruction_boundary_right = cast(immutable) temp_reconstruction_boundary_right;

        name = args.retrieve("name").defaults("S" ~ Clock.currTime.toString);
        end_time = args.retrieve("end_time").defaults(1.0).greater(0.0);
        max_CFL = args.retrieve("max_CFL").defaults(0.99).greater(0.0);
        save_interval = args.retrieve("save_interval").defaults(0.0);
        
        // steady state solver parameters
        psuedo_steady_tol = args.retrieve("steady_tol").defaults(2.5E-5);

        auto min_delta_t = args.retrieve("min_delta_t").defaults(1.0E-7).geq(0.0);
        integrator = build_integrator(
            args.retrieve("integrator").defaults("RKCK45"), 
            min_delta_t,
            args.retrieve("max_delta_t").defaults(double.infinity).greater(min_delta_t), 
            args.retrieve("adap_sf").defaults(0.98).greater(0.0), 
            args.retrieve("adap_tol").defaults(1.0E-2).greater(0.0));
        
        inviscid_flux_calculator = args.retrieve("flux_calculator").defaults("ausmdv")
            .set_inviscid_flux_calculator;

        // set up the result folder
        import std.path: dirSeparator;
        results_folder = [ROOT_DIRECTORY, "results", name,""].join(dirSeparator);

        debug log_file = results_folder ~ "log.txt";

        max_cpus = totalCPUs - 1; //TODO: add in override option
    }
}

/**
 * The simulation handler, controls initialization, domain handling
 * and connection, time marching and input/output
 */
final class Simulation 
{
    import q1dcfd.types: AssertionCondition;

    // Devilish hack to get constant instantiated attributes in Static Class
    static SimulationConstants constants;
    alias constants this;

    static 
    {
        // Resulting time step (readonly outside of Simulation context)
        @property Real dt() {return _dt;}

        // Current simulation time, globally accessible, read only
        @property Real transient_time() {return _t;}

        // Public access to state, balance and order (read only)
        Real get_state(const size_t i) {return previous_state[i];}
        synchronized Real[] get_state() {return cast(Real[]) previous_state[];}
        Real get_balance(const size_t i) {return previous_balance[i];}
        Real[] get_balance() {return cast(Real[]) previous_balance[];}
        size_t order() {return previous_state.length;}
        size_t n_saved_fields() {return _n_saved_fields;}

        // Public access to nodes, domains and interfaces (read only)
        immutable(Node) get_node(const size_t n)
        {
            if(!data_lock) return cast(immutable)nodes[n];
            else
            {
                throw new Exception("Cannot read a node during integration as the "
                                    ~"state is partially updated");
            }
        }

        immutable(Domain) get_domain(const string key)
        {
            if(!data_lock) return cast(immutable)domains[key];
            else
            {
                throw new Exception("Cannot read a domain during integration as the "
                                    ~"state is partially updated");
            }
        }
        immutable(Stream) get_stream(const string key)
        {
            if(!data_lock) return cast(immutable) streams[key];
            else
            {
                throw new Exception("Cannot read a stream during integration as the "
                                    ~"state is partially updated");
            }
        }
        
        immutable(ThermalFace) get_face(const string key)
        {
            if(!data_lock) return cast(immutable) faces[key];
            else
            {
                throw new Exception("Cannot read a thermal interface during integration as the "
                                    ~"state is partially updated");
            }
        }
        string[] list_streams() {return streams.keys;}
        string[] list_faces() {return faces.keys;}
        size_t n_nodes() {return nodes.length;}
        size_t n_domains() {return domains.length;}
        size_t n_interfaces() {return interfaces.length;}

        bool pseudo_steady = false; // flag, is the steady state solver active?

        /// gets the stream which contains the fluid domain (key)
        immutable(Stream) get_stream_containing(const string key)
        {
            foreach(stream; streams)
            {
                if(stream.domains.canFind(key)) {return cast(immutable) stream;}
            }

            throw new Exception("Cannot find domain %s in any stream".format(key));
        }

        /// Get a list of the cross sections defined within the system
        immutable(CrossSection[]) get_geometries()
        {
            assert(false, "Not yet implemented");
        }

        debug
        {
            // a record of the last N log messages
            string[N_LOG_INSTRUCTIONS_RECORDED] log_buffer;
            
            // log message counter
            size_t log_counter = 0;
        }
    }

    static private
    {
        bool is_initialised = false; // has initialisation already taken place?

        Real _t = 0.0; // simulation time, private state
        Real _dt = Real.infinity; // time step, private state

        // maximum time step required to satisfy stability constraints
        Real dt_stability = Real.infinity; 

        // maximum time step required to satisfy accuracy constraints
        Real dt_accuracy = Real.infinity; 

        // configuration space and derivative
        Real[] state; // state X
        shared Real[] previous_state; // state X at the last complete time step
        Real[] balance; // state derivative dX/dt
        shared Real[] previous_balance; // state derivative dX/dt at the last complete time step
        Real[] save_state; // array of properties to save to disk

        // private counter of number of properties to write to disk
        size_t _n_saved_fields = 1; 

        // Domains, Nodes, data and Interface collections
        Node[] unordered_nodes; 
        Node[] ordered_nodes;
        Node[] nodes; //
        Domain[string] domains; // Collection of all domains in the system
        Stream[string] streams; // Collection of all streams in the system
        ThermalFace[string] faces; // Collection of all heat transfer interfaces
        Interface[] interfaces; // collection of all interfaces in the system
        Interface[] corrections; // Collection of all flux corrections in the system

        // seperate list of all convective interfaces
        ConvectiveInterface[] convective_interfaces;

        // system level saved properties
        SystemProperty[] saved_fields;

        // Public access to shared state is allowed?
        bool data_lock = false;
    }
    
    /*-------------------Allocation and Initialisation -----------------------*/
    
    /**
     * Custom static constructor acts similarly to a singleton constructor
     *
     * Loads all simulation parameters and constructs the internal representation
     * of the system to be simulated
     */

    static void initialise(Contents contents)
    {
        if(is_initialised) assert(false, "Simulation already initialised!");

        // assign constants
        constants = new SimulationConstants(contents.simulation_args);

        // Prepare results folder
        version(unittest) {}
        else 
        {
            if(Simulation.results_folder.exists) Simulation.results_folder.rmdirRecurse;
            Simulation.results_folder.mkdir;
        }

        // initialize log file
        debug log_header;
        debug log_manager_creation(contents.simulation_args);

        // Initialise everything
        auto geometries = construct_geometry_constructors(contents.geometry_table);
        auto ordered_domains = construct_domain_constructors(contents, geometries);
        Simulation.domains = ordered_domains.as_associative_array;
        debug log_domain_set(domains);
        Simulation.assign_tabular_data(contents.connection_table);
        Simulation.allocate_memory;
        Simulation.discretise(ordered_domains, nodes);
        Simulation.construct_connections(contents.connection_table, interfaces, nodes);
        Simulation.construct_save_state(ordered_domains);
        Simulation.link_boundaries(ordered_domains);
        Simulation.initialise_state_vector(ordered_domains);
        dt_stability = Simulation.update_stable_timestep(nodes);
        Simulation.assemble_node_queues;
        Simulation.update_fluxes; 
        Simulation.update_balance(balance);
        Simulation.initialise_control_system(ordered_domains);
        debug
        {
            log_message("[INITIALISATION STATE]"); 
            report_current_state;
        }        
        version(unittest) {} else {Simulation.write_metadata;}
        Simulation.is_initialised = true; // mark as initialised
        debug log_message("\n Initialisation completed");
    }

    /*
     * Disable the default constructor since object is a psuedo singleton
     */
    @disable this();

    /*
     * Allocates most memory, except for thermal face saved states
     * which cannot be constructed until the connections are made
     */
    private static void allocate_memory()
    {
        size_t n_nodes = 0;
        size_t n_odes = 0;

        foreach(domain; domains)
        {
            n_nodes += domain.n_nodes;
            n_odes += domain.order;
            _n_saved_fields += domain.w_order;
        }

        state.length = n_odes;
        previous_state.length = n_odes;
        balance.length = n_odes;
        previous_balance.length = n_odes;
        nodes.length = n_nodes;
        Simulation.integrator.reserve_workspace(n_odes);
        debug log_memory_allocation;
    }

    /*
     * Divides the node list into queues for parrallel computation
     * if the node is coupled, add it to the priority queue else add
     * to the general node queue
     */
    private static void assemble_node_queues()
    {
        ordered_nodes = nodes.filter!(node => node.priority_queue).array;
        unordered_nodes = nodes.filter!(node => !node.priority_queue).array;
        debug log_node_queues(ordered_nodes, unordered_nodes);
    }

    /*
     * Construct the connections between each domain and create interface states
     */
    private static void construct_connections(
        Connection[] connections, 
        ref Interface[] interfaces,
        ref Node[] nodes)
    {        
        debug log_starting_connection;
        foreach(connection; connections)
        {
            foreach(pair; connection.directed_pairs)
            {
                connect(domains[pair[0]], domains[pair[1]], connection, 
                    interfaces, nodes);
                debug log_connection(pair);
            }
        }
    }

    /* 
     * Initialises the state vector based upon the initial state defined within
     * each domain 
     */
    private static void initialise_state_vector(DomainContainer ordered_domains)
    {
        debug log_starting_initialisation;
        // Initialise the controllers first, as these set the boundary conditions
        foreach(domain; domains)
        {
            if(domain.type == "Controller")
            {
                domain.initialise_state(state, nodes);
                debug log_completed_initialisation(domain);
            }
        }

        // Initialise everything else
        foreach(domain; ordered_domains.domains)
        {
            if(domain.type != "Controller")
            {
                domain.initialise_state(state, nodes);
                debug log_completed_initialisation(domain);
            }
        }

        // Store initial state
        previous_state[] = state[];
        previous_balance[] = balance[];
    }

    /*
     * Control system has to read in some quantities computed during
     * 'update_fluxes' and 'update_balances', so need to initialise after
     * everything else.
     */
    private static void initialise_control_system(DomainContainer ordered_domains)
    {
        // Initialize observers in state-space controllers
        foreach(domain; domains)
        {
            import std.array: split;
            if(domain.node_type.split(".")[$-1] == "StateSpaceController")
            {
                import q1dcfd.domains: AbstractController;
                import std.conv: to;
                domain.to!AbstractController.initialise_control_system(nodes);
            }
        }
    }

    private static void link_boundaries(DomainContainer ordered_domains)
    {
        debug log_starting_linkages;
        foreach(domain; ordered_domains.domains)
        {
            domain.link_boundaries(nodes);
            domain.check_simulation_ready;
            debug log_completed_linkage(domain);
        }
    }

    /*
     * Create each node and assign to the Array of nodes 
     */
    private static void discretise(DomainContainer ordered_domains, 
        ref Node[] nodes)
    {
        debug log_beginning_discretisation;

        uint node_index_l = 0;
        uint index_l = 0;
        uint w_index_l = 1 + saved_fields.length.to!uint;

        foreach(domain; ordered_domains.domains)
        {
            debug log_discretising_domain_start(domain.name);

            domain.discretise(nodes, interfaces, node_index_l, index_l, w_index_l);
            node_index_l = domain.node_index_r;
            index_l += domain.order;
            w_index_l += domain.w_order;

            debug log_discretising_domain_done(domain.name);
        }
    }

    // assign the tabular data to each domain
    private static void assign_tabular_data(Connection[] connections)
    {
        TableData[string] fluid_conservative_data;
        TableData[string] fluid_isentropic_data;
        TableData[string] fluid_primitive_data;
        foreach(connection; connections)
        {
            // make sure each domain exists
            connection.check_domains_exist(domains);

            connection.set_tabular_data(fluid_conservative_data, 
                fluid_isentropic_data, fluid_primitive_data, streams, domains);
        }
    }
    
    // Construct each geometry 
    private static CrossSection[string] construct_geometry_constructors
        (string[string][string] geometry_table)
    {
        CrossSection[string] geometries = ["dimensionless": new Dimensionless];

        foreach(name, geometry_defs; geometry_table)
        {
            debug log_geometry_creation(name, geometry_defs);

            geometries[name] = build_cross_section(geometry_defs, name);
        }

        return geometries;
    }

    /*
     * Process the domains. Extract the metadata for each domain into a Domain
     * object and map to geometries.
     */
    private static DomainContainer construct_domain_constructors
        (ref Contents contents, CrossSection[string] geometries)
    {
        DomainContainer ordered_domains;

        foreach(i, defs; contents.domain_defs)
        {
            debug log_domain_creation(contents.domain_names[i], defs);

            build_domain(ordered_domains,
                         defs,
                         contents.domain_names[i],
                         geometries, 
                         contents.connection_table);
        }

        return ordered_domains;
    }

    /*
     * Allocates memory for the saved property vector, and generates delegates
     * Which write the state of each domain to this vector
     */
    private static void construct_save_state(DomainContainer ordered_domains)
    {
        save_state.length = _n_saved_fields;
        debug log_saved_state_memory_allocation;

        foreach(domain; ordered_domains.domains)
        {
            nodes[domain.node_index_l..domain.node_index_r]
                .each!(node => generate_save_method(node, domain.saved_fields));
        }
    }

    /*
     * Checks each initialisation condition and throws an exception if any fail
     */ 
    static void check_initialisation_conditions()
    {
        string msg = "";
        bool success = true;

        foreach(node; nodes)
        {
            auto result = node.check_initialisation_conditions();
            if(!result.success)
            {
                msg ~= result.msg ~ "\n";
                success = false;
            }
        }

        if(!success)
        {
            throw new InvalidDefinition("Failed initialisation checks: %s".format(msg));
        }
    }

    static void add_convective_interface(ConvectiveInterface face)
    {
        this.convective_interfaces ~= face;
        this.interfaces ~=  face;
    }
    
    // Add a system property to the save_state vector
    static void add_system_saved_property(SystemProperty property)
    {
        saved_fields ~= property;
        _n_saved_fields++;
    }

    // Add a flux correction to the queue
    static void add_correction(Interface correction)
    {
        this.corrections ~= correction;
    }

    // Add a heat transfer face to the list
    static void add_thermal_face(ThermalFace face)
    {
        this.faces[face.name] = face;
    }

    // create metadata file
    private static void write_metadata()
    {
        auto metafile = File(Simulation.results_folder ~ "meta.txt", "w");
        write_simulation_meta(metafile);
        foreach(domain; domains) domain.write_metadata_to_disk(metafile);
    }

    /// Clears all fields
    static void reset()
    {
        constants = null;
        is_initialised = false; 
        _dt = Real.infinity; 
        dt_stability = Real.infinity; 
        dt_accuracy = Real.infinity; 
        state = []; 
        balance = [] ;
        previous_state = [];
        previous_balance = [];
        save_state = []; 
        unordered_nodes = []; 
        ordered_nodes = [];
        nodes = []; //
        domains  = null; 
        streams = null;
        interfaces = [];
        convective_interfaces = [];
        corrections = [];
    }

    /*------------------------------- Solvers --------------------------------*/

    /*
     * Marches in pseudo time to a steady state
     */
    @fastmath static void steady()
    {
        if(!is_initialised) throw new Exception("Simulation not initialised");

        debug log_message("\n [STEADY STATE CALCULATION]");
        
        with(constants)
        {
            // flag psuedo steady time marching
            Simulation.pseudo_steady = true;
            scope(exit) Simulation.pseudo_steady = false; 

            import q1dcfd.utils.progress_bar: ConvergenceProgress;
            auto convergence_progress = new ConvergenceProgress(psuedo_steady_tol);

            size_t count = 0; // number of iterations
            Real pt_check = 0.0; // last psuedo time steady check
            Real pt = 0.0; // psuedo time

            // residual
            Real delta = 2.0*psuedo_steady_tol;

            // pick a suitable initial delta psuedo time
            _dt = min(dt_stability, integrator.max_delta_t);
            _dt = max(dt, integrator.min_delta_t);

            // Difference between new and previous time state
            auto delta_state = new Real[](state.length);

            // start the performance timer
            version(unittest) {}
            else 
            {
                import std.datetime.stopwatch: StopWatch, AutoStart;
                import core.time;
                auto stopwatch = StopWatch(AutoStart.yes);
            }
            
            scope(success)
            // check performance on exit
            {
                version(unittest) {}
                else 
                {
                    stopwatch.stop;
                    auto time_elapsed = stopwatch.peek.total!"msecs";
                    Real seconds = time_elapsed*1.0E-3 ;
                    convergence_progress.complete(seconds, pt);
                    debug log_message("done, in %f seconds".format(seconds));
                }
            }

            // Main body of pseudo steady state calculation
            try
            {
                scope(failure)
                {
                    // Failure in steady state calculation
                    convergence_progress.halt;
                    writefln("Error log created in %sERROR.txt", 
                        results_folder);
                }

                debug log_message("Starting iteration...");

                while(delta > psuedo_steady_tol)
                {
                    // When modelling only incompressible components, there is
                    // currently no state vector, so just skip the steady-state
                    // initialization procedure
                    if(state.length == 0) break;

                    // march state and calculate new time step accuracy constraints
                    dt_accuracy = integrate_time_step(_t, _dt);

                    // calculate new stability constraints
                    dt_stability = update_stable_timestep(nodes);

                    // pick smallest of accuracy and stability contraints or round 
                    // off to end time if within one interval of completion
                    _dt = min(dt_accuracy, dt_stability);

                    count++;
                    pt += _dt;

                    // if sufficient psuedo time has passed, then check for 
                    // convergence
                    if(pt - pt_check > 1.0E-3)
                    {
                        // Update delta
                        delta_state[] = (1.0 - cast(Real[]) previous_state[]/state[]);

                        import std.math: fabs;
                        delta = max(
                            fabs(reduce!(max)(delta_state)), 
                            fabs(reduce!(min)(delta_state)));

                        // update the comparison state
                        pt_check = pt;
                        previous_state[] = state[];
                        previous_balance[] = balance[];

                        version(unittest) {} else convergence_progress.update(delta, pt);
                    }

                    if(pt > STEADY_SOLVER_TIMEOUT)
                    {
                        throw new Exception(format!("Failed to converge to steady "
                            ~"state within %f psuedo seconds. Supply more accurate "
                            ~"initial guess, increase STEADY_SOLVER_TIMEOUT "
                            ~"parameter, or check system for stability/well "
                            ~"posedness")(STEADY_SOLVER_TIMEOUT));
                    }
                }
            }
            catch (Error e)
            // Psuedo Steady Simulation failure
            {
                // write error log and throw exception
                report_pseudosteady_failure(e, dt, count, delta);
                throw(e);
            }
        }
    }

    /*
     * Performs the simulation in time and writes to file
     */
    @fastmath static void simulate()
    {
        if(!is_initialised) throw new Exception("Simulation not initialised");

        with(constants)
        {
            size_t count = 0; // iteration count
            Real t_saved = 0; // last saved time

            // prepare the save file
            auto data_file = File(results_folder ~ "data", "w");

            // pick a suitable initial delta time
            _dt = min(dt_stability, integrator.max_delta_t);
            _dt = max(_dt, integrator.min_delta_t);

            // write out initial state
            version(unittest) {} 
            else
            {
                write_to_disk(data_file, _t);

                // start the performance timer
                import std.datetime.stopwatch: StopWatch, AutoStart;
                import core.time;
                import q1dcfd.utils.progress_bar: ProgressBar;

                // initialise dynamic progress bar
                auto progress = new ProgressBar(end_time);

                // initialiser timer
                auto stopwatch = StopWatch(AutoStart.yes);
            }

            scope(exit)
            // check performance on exit
            {
                version(unittest) {}
                else 
                {
                    stopwatch.stop;
                    auto time_elapsed = stopwatch.peek.total!"msecs";
                    auto minutes = time_elapsed/(60_000);
                    Real seconds = time_elapsed*1.0E-3 - minutes*60.0;
                    writefln("Total Runtime %u minutes %.3f seconds", minutes, seconds);
                }
            }

            // time march until completion
            version(unittest) {} else progress.update(0.0);
            try
            {
                scope(failure)
                {
                    version(unittest) {}
                    else 
                    {
                        // freeze the progress bar
                        progress.halt(_t);

                        // Tell the user that shit hit the fan
                        write("TRANSIENT SIMULATION FAILURE. ");
                        writefln("Error log created in %sERROR.txt", 
                            results_folder);
                        stdout.flush();
                    }
                }
                while(_t < end_time)
                {
                    // march state and calculate new time step accuracy constraints
                    dt_accuracy = integrate_time_step(_t, _dt);

                    // calculate new stability constraints
                    dt_stability = update_stable_timestep(nodes);

                    // pick smallest of accuracy and stability contraints or round 
                    // off to end time if within one interval of completion
                    _dt = min(dt_accuracy, dt_stability, end_time - _t);

                    count++;
                    _t += _dt;

                    // write out state
                    version(unittest) {}
                    else
                    {
                        // update progress bar 
                        progress.update(_t);

                        if(_t - t_saved > save_interval)
                        {
                            write_to_disk(data_file, _t);
                            t_saved = _t;
                        }

                        // store copy of state and balance
                        previous_state[] = state[];
                        previous_balance[] = balance[];
                    }
                }
            }
            catch (Error e)
            // Dynamic Simulation failure
            {
                // write error log and throw exception
                version(unittest) {} else report_transient_failure(e, _t, _dt, count);
                throw(e);
            }
        }
    }

    /*
     * Update the transfer of flux between each node 
     */
    @fastmath static void update_fluxes()
    {
        version(serial) {foreach(face; interfaces) face.update_flux; }
        else {foreach(face; interfaces.parallel) face.update_flux; }

        foreach(correction; corrections) correction.update_flux;
    }

    /*
     * Determine the maximum time step to satisfy CFL criteria stability 
     */
    @fastmath static Real update_stable_timestep(ref Node[] nodes) nothrow
    {
        return reduce!((a,b) => min(a, b.critical_dt))(Real.infinity, nodes)
            *constants.max_CFL;
    }

    /* 
     * Update the system configuration space derivative from the attributes of 
     * each node,
     */
    @fastmath static void update_balance(ref Real[] balance)
    {
        nodes.each!(node => node.update_balance(balance));
    }

    /*
     * Update the attributes of each node from the system configuration space 
     */
    @fastmath static void push_state(const ref Real[] state, const Real t)
    {
        import std.stdio;
        foreach(node; ordered_nodes) node.update(state, t);
        version(serial) foreach(node; unordered_nodes) node.update(state, t);
        else
        {
            foreach(node; unordered_nodes.parallel) node.update(state, t);
        }
    }

    /*
     * Full system update to time t 
     */
    @fastmath static void update(
        ref Real[] balance, 
        const ref Real[] state, 
        const Real t)
    {
        streams.each!(stream => stream.reset);
        faces.each!(face => face.reset);
        if(pseudo_steady) push_state(state, 0.0); else push_state(state, t);
        update_fluxes;
        update_balance(balance);
    }

    /**
     * Delegates the integration procedure to the integration algorithm and
     * locks public access to shared data to avoid parallel data racing
     */
    @fastmath static Real integrate_time_step(const Real time, const Real time_step)
    {
        // Need to disable public access to shared data during parallel processing
        data_lock = true;
        scope(exit) data_lock = false;

        // do the actual work
        return constants.integrator.update(&update, state, balance, time, time_step);
    }
    

    /* ----------------------------- Write to file -------------------------- */

    /*
     * populate the array to write to disk 
     */
    static void write_to_disk(File data_file, const Real t)
    {
        save_state[0] = t;

        // go through all the convective interfaces and add up total heat
        // transfer
        convective_interfaces.each!(face => face.record_heat_transfer);

        // loop through each node and copy its saved properties to the
        // write out vector
        nodes.each!(node => node.write_to(save_state));

        // loop through each system property and copy its saved properties
        // to the write out vector
        saved_fields.each!(field => field.write_to(save_state));

        // write the saved properties to disk
        data_file.rawWrite(save_state);
    }
}

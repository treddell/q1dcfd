module q1dcfd.correlations.maps ;

import q1dcfd.control_volumes: InflowDV, OutflowNode ;
import q1dcfd.config: Real ;
import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.initialisation: read_boundary_function ;
import q1dcfd.simulation: Simulation ;

/**
 * Map function as a function of inflow state, outflow state and shaft speed
 */
alias EfficiencyMap = Real delegate(InflowDV, OutflowNode, const Real) ;

/** 
 * Generates a static user defined efficiency map. Useful for testing purposes 
 * and low fidelity simulations around design point
 *
 * Note, while named 'constant efficiency', the input parameter can be a MathFunction(t).
 * If not a constant function then efficiency will be updated over time
 */
EfficiencyMap generate_constant_efficiency_map(const string efficiency_def)
{
    auto map = efficiency_def.read_boundary_function ;
    Real func(InflowDV inflow, OutflowNode outflow, const Real omega)
    {
        return map.evaluate(Simulation.transient_time) ;
    }

    return &func ;
}

/**
 * Inverts an efficiency map so that turbines and compressors can re-use the 
 * same jump condition class
 *
 * if cond = true, will invert the map, else returns the original map
 */
EfficiencyMap invert(EfficiencyMap map, const bool cond)
{
    Real func(InflowDV inflow, OutflowNode outflow, const Real omega)
    {
        return 1.0/map(inflow, outflow, omega) ;
    }
    return cond ? &func : map ;
}

@disable EfficiencyMap generate_map_from_data(string filename)
{
    // TODO: load the map and fit a surface to it, return as callable delegate
    // 'map'

    Real map(InflowDV, OutflowNode, const Real)
    {
        // implement
        return 0 ;
    }
    return &map ;
}


/**
 * parses an efficiency map definition and produces the corresponding map
 */
EfficiencyMap read_map(const string def_string)
{
    import std.string ;
    import std.range: retro ;
    import std.exception: enforce ;
    import std.format ;
    import std.conv: to ;

    auto i1 = def_string.indexOf("(") ;
    auto i2 = def_string.length - def_string.retro.indexOf(")") - 1 ;

    enforce!InvalidDefinition(i1 > 0 && i1 < i2 && i2 < def_string.length,
                              "Could not parse efficiency_map definition %s"
                              .format(def_string)) ;

    switch(def_string[0..i1])
    {
        case "constant_efficiency":
        {
            return generate_constant_efficiency_map(def_string[i1+1..i2]) ;
        }
        default:
        {
            throw new InvalidDefinition("Map type %s not understood".format(def_string[0..i1])) ;
        }
    }
}

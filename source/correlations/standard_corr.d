module q1dcfd.correlations.standard_corr ;

/*
A suite of friction factor and heat transfer correlations
*/

import std.math: sqrt, log10, log, pow, fabs ;
import std.format ;
import std.conv: to ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

import q1dcfd.config: Real ;
import q1dcfd.utils.math: square ;
import q1dcfd.control_volumes: FluidNode, ThermalNode, Atmosphere,
    ViscidPointState ;
import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.domains: FluidDomain ;

// Module level constants
enum Real LAMINAR_RE = 2320.0 ;
enum Real TURBULENT_RE = 3000.0 ;
enum Real LAMINAR_RA = 8.5E9 ;
enum Real TURBULENT_RA = 9.0E9 ;
enum Real ROUGHNESS_CRITERIA = 7.0E-4 ;
enum Real REC_DELTA_RE = 1.0/(TURBULENT_RE - LAMINAR_RE) ;
enum Real REC_DELTA_RA = 1.0/(TURBULENT_RA - LAMINAR_RA) ;

bool isViscousNode(T)()
{
    return (is(T == FluidNode) || is(T == ViscidPointState)) ;
}

// friction factor correlation mappings
FrictionFactorCorrelation friction_factor_map(NodeType)(NodeType node, string key)
if(isViscousNode!(NodeType))
{
    switch(key)
    {
        case "SwitchFrictionFactor": return new SwitchFrictionFactor!(NodeType)(node) ;
        case "Darcy": return new Darcy!(NodeType)(node) ;
        case "DarcySemiCircular": return new DarcySemiCircular!(NodeType)(node) ;
        case "Blasius": return new Blasius!(NodeType)(node) ;
        case "BhattiShah": return new BhattiShah!(NodeType)(node) ;
        case "Petukhov": return new Petukhov!(NodeType)(node) ;
        case "NoFriction": return new NoFriction ;
        
        default:
        {
            throw new InvalidDefinition(format!("Friction factor correlation %s"
                ~" not recognised")(key)) ;
        }
    }
}

/// Heat Transfer correlation mappings
HeatTransferCorrelation heat_transfer_map(
    FluidNode fluid_node,
    ThermalNode thermal_node,
    const string key,
    const bool thin_film)
{
    switch(key)
    {
        case "SwitchHeatTransfer":
        {
            if(thin_film) return new SwitchHeatTransfer!(true)(fluid_node, thermal_node) ;
            else return new SwitchHeatTransfer!(false)(fluid_node, thermal_node) ;
        }
        case "NoHeatTransfer": return new NoHeatTransfer ;
        
        default:
        {
            throw new InvalidDefinition(format!("Heat transfer correlation %s "
                ~"not recognised")(key)) ;
        }
    }
}

/**
 * Type which stores the friction factor correlation, all correlations must 
 * inherit from this class 
 */
interface FrictionFactorCorrelation
{
    // correlation to calculate the friction factor
    @fastmath Real evaluate() ;
}

/// Darcy friction factor correlation, suitable for Re<2320, for circular pipes
final class Darcy(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) {this.node = node ;}

    @fastmath Real evaluate() {return 64.0/node.re ;}
}

/**
 * A modification of the laminar darcy friction factor, preferred
 * for semi-circular pipes
 * Taken from "Dynamic modelling and transient analysis of a molten salt heated
 * recompression supercritical sCO2 Brayton cycle, 6th International sCO2
 * symposium, 2018, Pittsburgh
 */
final class DarcySemiCircular(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) {this.node = node ;}

    @fastmath Real evaluate() {return 63.12/node.re ;}
}

/// Turbulent correlation for smooth pipes, suitable for Re<10^6
final class Blasius(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) {this.node = node ;}

    @fastmath Real evaluate() {return 0.316/node.re.pow(0.25) ;}
}

/**
 * Bhatti and Shah (1987). Obtained from Thar Energy Presentation
 * 2018 scO2 symposium
 */
final class BhattiShah(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType)) 
{
    NodeType node ;

    this(NodeType node) { this.node = node ; }

    @fastmath Real evaluate() { return 0.00128 + 0.1143*node.re.pow(-0.311) ; }
}

/// Correlation suitable for rough pipes with Re > 4000 
final class ColebrookWhite(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    const Real cw_tol ; // convergence tolerance
    
    // correlation coefficient
    const Real alpha ;

    // correlation used to supply initial guess
    FrictionFactorCorrelation initial_guesser ; 

    NodeType node ;
    
    this(NodeType node, const Real tol = 1.0E-4) 
    {
        this.node = node ;
        this.cw_tol = tol ;
        this.alpha = node.geometry.delta/(3.71*node.geometry.hydr_d) ;
        this.initial_guesser = new Blasius!(NodeType)(node) ; 
    }

    @fastmath Real evaluate()
    {
        Real xnew, xguess ;

        // provide a reasonable initial guess for friction factor
        xguess = 1.0/sqrt(initial_guesser.evaluate()) ;
        xnew = 2.0*xguess ;

        // use fixed point iteration until the error is below some tolerance
        while(fabs(xguess -xnew) > cw_tol)
        {
            xnew = xguess ;
            xguess = -2.0*log10(alpha + 2.51*xguess/node.re) ;
        }
        return 1.0/xguess.square ;
    }
}

/// Valid for smooth tubes with 3000 < Re < 5.0E6 
final class Petukhov(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node)
    {
        this.node = node ;
    }
    @fastmath Real evaluate()
    {
        return 1.0/(0.79*log(node.re)-1.64).square ;
    }
}

/*
 * Switching function(s), to cover a range of flow characteristics
 */
final class SwitchFrictionFactor(NodeType): FrictionFactorCorrelation
if(isViscousNode!(NodeType))
{
    /* Default all Re switching model */

    // correlation to use for turbulent flow
    FrictionFactorCorrelation turbulent ; 

    // correlation to use for laminar flow
    FrictionFactorCorrelation laminar ;

    NodeType node ;
    this(NodeType node)
    {
        // select the turbulent model based off wall roughness
        if(node.geometry.delta/node.geometry.hydr_d < ROUGHNESS_CRITERIA)
        {
            this.turbulent = new Petukhov!(NodeType)(node) ;
        }
        else
        {
            this.turbulent = new ColebrookWhite!(NodeType)(node) ;
        }

        this.laminar = new Darcy!(NodeType)(node) ;
        this.node = node ;
    }

    @fastmath Real evaluate()
    {
        Real frac ;

        // Check if flow is fully laminar
        if(node.re<LAMINAR_RE)
        {
            return laminar.evaluate() ;
        }
        // Check if flow is transitional
        else if(node.re<TURBULENT_RE)
        {
            frac = (node.re - LAMINAR_RE)*REC_DELTA_RE ;
            return (frac*turbulent.evaluate() + (1.0-frac)*laminar.evaluate()) ;
        }
        // otherwise fully turbulent
        else
        {
            return turbulent.evaluate() ;
        }
    }
}

/// No viscous losses
final class NoFriction: FrictionFactorCorrelation
{
    @fastmath Real evaluate() {return 0.0 ;}
}

/*
 * Nussult Number Correlations
 */
interface NussultNumberCorr
{
    @fastmath Real evaluate() ;
}

/// Valid for Re < 2300 
final class LaminarNusselt(NodeType): NussultNumberCorr
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) {this.node = node ;}

    @fastmath override Real evaluate() { return 4.089 ;}
}

/// Valid for 0.5<Pr < 2000 and 3000 < Re < 5.0E6 
final class Gnielinski(NodeType): NussultNumberCorr
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) { this.node = node ;}

    @fastmath override Real evaluate()
    {
        Real k1 = 1.0 + 3.4*node.f ;
        Real k2 = 11.7 + 1.8*node.pr.pow(-1.0/3.0) ;

        return((0.125*node.f*node.re*node.pr)/(k1 + k2*sqrt(0.125*node.f)
            *(node.pr.pow(2.0/3.0) - 1.0)));
    }
}

/// Valid for 0.75 < Pr < 2.2  and  3500 < Re < 2.3E5
final class Ngo(NodeType): NussultNumberCorr
if(isViscousNode!(NodeType))
{
    NodeType node;
    this(NodeType node) { this.node = node; }

    @fastmath override Real evaluate()
    {
        return 0.1696 * node.re.pow(0.629) * node.pr.pow(0.317);
    }
}

/**
 * From Thar Energy Presentation, Heat Exchanger Tutorial
 * 2018 sCO2 Symposium
 */
final class Petukhov1970(NodeType): NussultNumberCorr
if(isViscousNode!(NodeType))
{
    NodeType node ;
    this(NodeType node) {this.node = node ;}

    @fastmath Real evaluate()
    {
        return 0.5*node.f*node.re*node.pr
            /(1.07 + 900.0/node.re - 600.0/(1.0 + 10*node.pr)
              + 12.7*sqrt(node.f*0.5)*(node.pr.pow(2.0/3.0) - 1.0)) ;
    }
}

/*
 * Heat Transfer Correlations
 */

interface HeatTransferCorrelation
{
    /* Base type for a heat transfer correlation */
    @fastmath Real evaluate() ;
}

final class SwitchHeatTransfer(bool thin_film): HeatTransferCorrelation
{
    /* default all parameter space switching model for heat transfer coefficient 
    */

    // correlation for laminar regime Nu
    NussultNumberCorr laminar_nu ; 

    // correlation for turbulent regime Nu
    NussultNumberCorr turbulent_nu ;

    FluidNode node ;

    static if(thin_film)
    {
        ViscidPointState thin_film_node ;
        ThermalNode thermal_node ;

        import q1dcfd.eos: EOSBase, generate_eos, backend ;
        EOSBase thin_film_eos ;
    }

    const Real rec_hydr_d ; // reciprocal hydraulic diameter

    this(FluidNode fluid_node, ThermalNode thermal_node)
    {
        this.node = fluid_node ;
        this.rec_hydr_d = 1.0/fluid_node.geometry.hydr_d ;

        static if(thin_film)
        {
            this.thin_film_node = new ViscidPointState(
                fluid_node.eos,
                fluid_node.f_corr_name,
                fluid_node.geometry,
                format!("Thin film state between %s->%s")
                (fluid_node.identify, thermal_node.identify)) ;
            
            this.thermal_node = thermal_node ;
            this.thin_film_eos = generate_eos(Simulation.get_stream_containing(
                                                  fluid_node.address.owner)
                                              .primitive_data,
                                              backend.TTSE_recovery,
                                              "Thin film (p,T) eos") ;

            this.laminar_nu = new LaminarNusselt!(ViscidPointState)(thin_film_node) ;
            // this.turbulent_nu = new Gnielinski!(ViscidPointState)(thin_film_node) ;
            this.turbulent_nu = new Ngo!(ViscidPointState)(thin_film_node) ;
        }
        else
        {
            this.laminar_nu = new LaminarNusselt!(FluidNode)(fluid_node) ;
            // this.turbulent_nu = new Gnielinski!(FluidNode)(fluid_node) ;
            this.turbulent_nu = new Ngo!(FluidNode)(fluid_node) ;
        }
    }

    @fastmath Real evaluate()
    do
    {
        // update the thin film conditions
        static if(thin_film)
        {
            auto p = node.p ;
            auto T = 0.5*(node.T + thermal_node.T) ;
            auto v = node.v ;

            thin_film_eos.update(p,T) ;

            auto rho = thin_film_eos.rho ;

            Real[3] thin_film_state = [
                thin_film_eos.rho, 
                thin_film_eos.rho*v, 
                thin_film_eos.rho*(thin_film_eos.e + 0.5*v.square)] ;

            thin_film_node.update(thin_film_state) ;
        }
        // fully laminar flow
        if(node.re<LAMINAR_RE)
        {
            return laminar_nu.evaluate()*node.k*rec_hydr_d;
        }
        // transitional flow
        else if(node.re<TURBULENT_RE)
        {
            import std.stdio;
            writeln("WARNING: Should not have transitional flow in these simulations.");

            Real frac = (node.re - LAMINAR_RE)*REC_DELTA_RE ;
            return (frac*turbulent_nu.evaluate() 
                + (1.0 - frac)*laminar_nu.evaluate())*node.k*rec_hydr_d ;
        }
        // fully turbulent flow
        else
        {
            return turbulent_nu.evaluate()*node.k*rec_hydr_d ;
        }
    }
}

/// No external heat transfer
final class NoHeatTransfer: HeatTransferCorrelation
{
    @fastmath Real evaluate() {return 0.0 ;}
}

/// Base class for an external flow heat transfer correlation 
abstract class ExternalFlowCorrelation
{
    Atmosphere atmosphere ;

    @fastmath abstract Real evaluate() ;
}

/** 
 * Correlation from Churchill and Chu for natural convection adjacent to a
 * vertical plane. Suitable for laminar and turbulent flows 0.1<Ra<1.0E9 
 */
final class Churchill: ExternalFlowCorrelation
{
    // correlation coefficients
    const Real alpha ; 
    const Real beta ;
    const Real rec_length ; // reciprocal of characteristic length

    this(Atmosphere atmosphere)
    {
        this.atmosphere = atmosphere ;

        // Assumed constant
        this.beta = 0.67/(1.0 + (0.492/atmosphere.pr).pow(9.0/16.0)).pow(4.0/9.0) ;
        this.alpha = 0.387/(1.0 + (0.492/atmosphere.pr).pow(9.0/16.0)).pow(8.0/27.0) ;
        this.rec_length = 1.0/atmosphere.char_length ;
    }

    @fastmath private Real laminar()
    {
        return atmosphere.k*(0.68 + beta*atmosphere.ra.pow(0.25))*rec_length ;
    }

    @fastmath private Real turbulent()
    {
        return atmosphere.k*(0.825 + alpha*atmosphere.ra.pow(1.0/6.0))*rec_length ;
    }

    @fastmath override Real evaluate()
    {
        if(atmosphere.ra < LAMINAR_RA)
        {
            return laminar() ;
        }
        else if(atmosphere.ra < TURBULENT_RA)
        {
            Real frac = (atmosphere.ra - LAMINAR_RA)*REC_DELTA_RA ;
            return frac*turbulent() + (1.0 - frac) * laminar() ;
        }
        else
        {
            return turbulent() ;
        }
    }
}

module q1dcfd.initialisation.parser ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.utils.dictionary_tools: retrieve ;
import q1dcfd.connectors.connection: Connection ;
import q1dcfd.utils.errors: DefinitionFileSyntaxError, InvalidDefinition ;

import std.file ;
import std.stdio ;
import std.algorithm. searching: startsWith, endsWith ;
import std.conv: to ;
import std.format ;
import std.string ;
import std.algorithm.searching: canFind ;
import core.stdc.stdlib: exit ;

// Optional simulation argument parameters
struct OptionalArguments
{
    const
    {
        string mode ;
        string save_override ;
    }

    this(const string mode, const string save_override)
    {
        this.mode = mode ;
        this.save_override = save_override ;
    }
}

// Splits a <key>:<value> style definition into key and value
void extract(const string statement, ref string key, ref string value)
{
    string[] splt = statement.split(":") ;
    
    if(splt.length == 2)
    {
        key = splt[0] ;
        value = splt[1] ;
    }
    else
    {
        throw new Exception(format!("DefinitionFile: Cannot interpret statement %s")
                            (statement)) ;
    }
}

/**
 * Checks a set of definitions and looks for an 'enabled' definition
 * 
 * if enabled is set to false, then returns false, otherwise removes the enabled
 * field and returns true
 *
 * Useful to turn controllers off/on without commenting out chunks of the definition
 * file
 */
bool is_enabled(const string name, string[string] defs)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required ;
    if("enabled" in defs)
    {
        auto enabled = defs.retrieve("enabled").required!string ;
        
        if(enabled == "true") return true ;
        else if(enabled == "!true") return false ;
        else if(enabled == "false") return false;
        else if(enabled == "!false") return true ;
        else
        {
            throw new InvalidDefinition(
                "enabled must be true/false for %s, instead found %s"
                .format(name, enabled)) ;
        }
    }
    return true ;
}

/*
 * Check the definitions dictionary for the given key, and if found then
 * replace the key with its value
 */
string check_definitions(const string key, const string[string] defs)
{
    if(key in defs) return defs[key] ;
    else if(key.startsWith("!"))
    {
        auto temp = key.replace("!", "") ;
        if(temp in defs) return "!" ~ defs[temp] ;
    }
    return key ;
}

/**
 * A definition file split into its blocks. Temporary storage container for 
 * all data in the definition file 
 */
struct Contents 
{
    string[string] simulation_args ;
    string[string] definitions ;
    string[string][string] geometry_table ;
    string[] domain_names ;
    string[string][] domain_defs ;
    Connection[] connection_table ;

    // Add a new block to the Contents 
    void include(string header, string[] block)
    {
        string key, value ; // keys and value for each input
        auto splt = header.split(":") ;
        string name = splt[1] ; // name of the current block
        header = splt[0] ;

        switch(header)
        {
            case "CrossSection":
            {
                foreach(statement ; block)
                {
                    statement.extract(key, value) ;
                    value = check_definitions(value, definitions) ;
                    geometry_table[name][key] = value ;
                }

                break ;
            }
            case "Define":
            {
                foreach(statement ; block)
                {
                    statement.extract(key, value) ;

                    if(key in definitions)
                    {
                        throw new Exception(format!("Parameter definition %s multiply"
                                                    ~"defined")(key)) ;
                    }
                    definitions[key] = value ;
                }
                break ;
            }
            case "Domain":
            {
                string[string] defs ;
            
                foreach(statement ; block)
                {
                    statement.extract(key, value) ;
                    value = check_definitions(value, definitions) ;
                    defs[key] = value ;
                }

                // looks for the enabled flag and forgets about the domain
                // if it is false
                if(!name.is_enabled(defs))  break ;

                domain_names ~= name ;
                domain_defs ~= defs ;

                break ;
            }
            case "Simulation":
            {
                simulation_args["name"] = name ;
            
                foreach(statement ; block)
                {
                    statement.extract(key, value) ;
                    value = check_definitions(value, definitions) ;
                    simulation_args[key] = value ;
                }
                break ;
            }
            case "Connect":
            {
                string[string] connection_args ;
                string[][] flowlines ;
                foreach(statement ; block)
                {
                    if(statement.canFind(":"))
                    // must be an argument
                    {
                        statement.extract(key, value) ;
                        value = check_definitions(value, definitions) ;
                        connection_args[key] = value ;           
                    }
                    // a connection path
                    else
                    {
                        flowlines ~= statement.split("->") ;
                    }
                }
                
                if(name == "")
                // no name assigned, give generic name
                {
                    name = connection_args["type"] ~ (connection_table.length + 1).to!string ;
                }

                // looks for the enabled flag and forgets about the block if
                // it is false
                if(!name.is_enabled(connection_args)) break ;
                
                connection_table ~= Connection(connection_args, flowlines, name) ;
                break ;
            }
            case "Stream":
            {
                block ~= "type:stream" ;
                goto case "Connect" ;
            }
            case "Convection":
            {
                block ~="type:convection" ;
                goto case "Connect" ;
            }
            case "controller-input":
            {
                block ~= "type:controller-input" ;
                goto case "Connect" ;
            }
            case "control-inventory":
            {
                block ~= "type:control-inventory" ;
                goto case "Connect" ;
            }
            case "control-state-in":
            {
                block ~= "type:control-state-in" ;
                goto case "Connect" ;
            }
            case "control-flow-in":
            {
                block ~= "type:control-flow-in" ;
                goto case "Connect" ;
            }
            case "control-motor-torque":
            {
                block ~= "type:control-motor-torque" ;
                goto case "Connect" ;
            }
            case "control-target-variable":
            {
                block ~= "type:control-target-variable" ;
                goto case "Connect" ;
            }
            case "axial-conduction":
            {
                block ~= "type:axial-conduction" ;
                goto case "Connect" ;
            }
            case "Channel":
            {
                block ~="type:Channel" ;
                goto case "Domain" ;
            }
            case "Pipe":
            {
                block ~= "type:Pipe" ;
                goto case "Domain" ;
            }
            case "ExternalValve":
            {
                block ~= "type:ExternalValve" ;
                goto case "Domain" ;
            }
            case "Compressor":
            {
                block ~= "type:Compressor" ;
                goto case "Domain" ;
            }
            case "BoundaryWall":
            {
                block ~= "type:BoundaryWall" ;
                goto case "Domain" ;
            }
            case "AxialWall":
            {
                block ~= "type:AxialWall" ;
                goto case "Domain" ;
            }
            case "Turbine":
            {
                block ~= "type:Turbine" ;
                goto case "Domain" ;
            }
            case "InterfaceCompressor":
            {
                block ~= "type:InterfaceCompressor" ;
                goto case "Domain" ;
            }
            case "InterfaceTurbine":
            {
                block ~= "type:InterfaceTurbine" ;
                goto case "Domain" ;
            }
            case "MapCompressor":
            {
                block ~= "type:MapCompressor" ;
                goto case "Domain" ;
            }
            case "MapTurbine":
            {
                block ~= "type:MapTurbine" ;
                goto case "Domain" ;
            }
            case "ParallelHE":
            {
                block ~= "type:ParallelHE" ;
                goto case "Domain" ;
            }
            case "Controller":
            {
                block ~= "type:Controller" ;
                goto case "Domain" ;
            }
            case "Inflow":
            {
                block ~= "type:Inflow" ;
                goto case "Domain" ;
            }
            case "Outflow":
            {
                block ~= "type:Outflow" ;
                goto case "Domain" ;
            }
            case "ChannelCrossSection":
            {
                block ~= "type:ChannelCrossSection" ;
                goto case "CrossSection" ;
            }
            case "WallCrossSection":
            {
                block ~= "type:WallCrossSection" ;
                goto case "CrossSection" ;
            }
            case "PisoSolver":
            {
                block ~= "type:PisoSolver";
                goto case "Domain";
            }
            case "IncompressibleChannel":
            {
                block ~="type:IncompressibleChannel" ;
                goto case "Domain" ;
            }
            case "connect-piso-solver":
            {
                block ~= "type:connect-piso-solver" ;
                goto case "Connect" ;
            }
            case "connect-piso-solver-to-wall":
            {
                block ~= "type:connect-piso-solver-to-wall" ;
                goto case "Connect" ;
            }
            case "connect-incompressible-boundaries":
            {
                block ~= "type:connect-incompressible-boundaries" ;
                goto case "Connect" ;
            }
            case "MixedHE":
            {
                block ~= "type:MixedHE" ;
                goto case "Domain" ;
            }
            default:
            {
                throw new Exception(format!("DefinitionFile: Unrecognized Header %s")
                                    (header)) ;
            }
        }
    }
}

// List of valid mode options
enum string[] VALID_MODES = ["transient", "inspect-only", "steady", "direct"] ;

// Checks for syntax errors in the current block
private void check_block(const string buffer, const long block_start, 
    const long block_end)
{
    if(block_start<0)
    {
        throw new DefinitionFileSyntaxError(format!"Missing '{' in expression %s"(buffer)) ;
    }
    if(block_end<0)
    {
        throw new DefinitionFileSyntaxError(format!"Missing '}' in expression %s"(buffer)) ;
    }
    if(block_end<block_start)
    {
        throw new DefinitionFileSyntaxError(format!"Mismatched '}' in expression %s"(buffer)) ;
    }
    if(buffer[block_start+1..block_end].canFind("{"))
    {
        throw new DefinitionFileSyntaxError(format!"Mismatched '{ in expression %s"(buffer)) ;
    }
}

// Check for syntax errors in the current statement
private void check_statement(const string statement, const long stmt_end)
{
    if(stmt_end < 0)
    {
        writefln("DefinitionFile SyntaxError: Missing ';' in statement %s", 
            statement) ;
        exit(1) ;
    }

    if(stmt_end == 0)
    {
        writefln("DefinitionFile SyntaxError: Mismatched ';' in statement %s", 
            statement) ;
        exit(1) ;
    }
}

// Parses a flattened definition file and extracts contents
Contents parse(string buffer)
{
    auto contents = Contents() ;
    string[] block ;
    int offset = 0 ;

    while(buffer.length > 0)
    {
        auto block_start = buffer.indexOf("{") ;
        auto block_end = buffer.indexOf("}") ;

        offset += block_start + 1;
        buffer.check_block(block_start, block_end) ;        
        
        string header = buffer[0..block_start] ;
        string block_contents = buffer[block_start+1..block_end] ;
        block = [] ;
        
        while(block_contents.length > 0)
        {
            auto stmt_end = block_contents.indexOf(";") ;

            block_contents.check_statement(stmt_end) ;
            
            block ~= block_contents[0..stmt_end] ;
            offset += stmt_end + 1 ;
            block_contents = block_contents[stmt_end + 1..$] ; 
        }
        contents.include(header, block) ;
        
        buffer = buffer[block_end+1..$] ;
    }
    
    return contents ;
}

// Flatten a human readable definition file into a parsable string
string squash(const string filename)
{
    auto file = File(filename, "r") ;
    string flattened_file ;
    
    foreach(line ; file.byLine)
    {
        string stripped_line = line.replace(" ","").to!string ;
        auto comment_start = stripped_line.indexOf("//") ;
        if(comment_start >= 0)
        {
            flattened_file ~= stripped_line[0..comment_start] ;
        }
        else
        {
            flattened_file ~= stripped_line ;
        }
    }
    return flattened_file ;
}

OptionalArguments process_input_args(string[] args)
// Process input optional arguments and return as an OptionalArguments struct
{
    string[] invalid_args ;
    bool mode_defined = false ;
    string mode = "transient" ;
    string save_override ;

    foreach(arg ; args)
    {
        if(!arg.startsWith("-"))
        {
            invalid_args ~= format!("input not understood: %s")(arg) ;
            continue ;
        }
        
        auto splt = arg.split("=") ;
        if(splt.length != 2)
        {
            invalid_args ~= format!("input not understood: %s")(arg) ;
            continue ;
        }

        auto key = splt[0].replace("-","") ;
        auto value = splt[1] ;
        
        if(key == "mode")
        {
            if(mode_defined)
            {
                invalid_args ~= "%s: multiple defintions for mode".format(arg) ;
            }
            else if(!VALID_MODES.canFind(value))
            {
                invalid_args ~= "mode %s not recognized. Valid options are %s"
                    .format(value, VALID_MODES.to!string) ;  
            }
            else
            {
                mode_defined = true ;
                mode = value ;
            }    
        }
        // override the results folder name
        else if(key == "save")
        {
            save_override = value ;
        }
        else
        {
            invalid_args ~= format!("input %s not a valid argument")(key) ;
        }
    }
    
    if(invalid_args.length > 0)
    {
        writefln("InvalidDefinition: Failed to parse optional arguments\n %s", 
            invalid_args.join("\n")) ;
        exit(1) ;
    }

    return OptionalArguments(mode, save_override) ;
}

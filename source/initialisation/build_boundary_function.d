module q1dcfd.initialisation.build_boundary_function ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Parses a string which defines a boundary condition transient and
 * the MathFunction object 
 *
 * Receives a definition of the form:  MathFunctionType(arg1, arg2, ...)
 *
 * breaks definition into a type and list of args by splitting the contents before
 * and within the outermost set of parenthesis. Has the capability to recursively
 * scan PiecewiseFunction definitions
 */
from!"q1dcfd.utils.math".MathFunction read_boundary_function(const string def_string)
{
    import q1dcfd.utils.math: Polynomial, PiecewiseFunction, Step, 
        DoubleStep, TripleStep, LinearRamp, CubicRamp, SqrtRamp, CubicSpline, Unassigned ;

    import std.stdio ;
    import std.range: retro ;
    import std.string: indexOf ;
    import std.conv: to ;
    
    scope(failure)
    {
        version(unittest) {}
        else writefln("Failed to parse boundary function definition %s", def_string);
    }

    // a parenthesis search
    auto paren_start = def_string.indexOf("(") ;
    auto paren_end = def_string.retro.indexOf(")") ;

    string func_args ;
    string type ;

    // No Parenthesis, check if constant value or Unassigned
    if(paren_start < 0 && paren_end < 0)
    {
        // Unassigned function
        if(def_string == "Unassigned") return new Unassigned ;

        // check if constant value else throw
        try
        {
            auto check_float = def_string.to!double ; // will throw if not numeric
        }
        catch(Exception e)
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;

            throw new InvalidDefinition("MathFunction Input %s not understood"
                                        .format(def_string)) ;
        }
        func_args = def_string  ;
        type = "Polynomial" ;
    }
    else if(paren_start > 0 && paren_end >= 0)
    {
        func_args = def_string[paren_start+1..$-paren_end-1] ;
        type = def_string[0..paren_start] ;
    }

    switch(type)
    {
        case "Polynomial": return build_simple_func!(Polynomial)(func_args) ;
        case "Step": return build_simple_func!(Step)(func_args) ;
        case "DoubleStep": return build_simple_func!(DoubleStep)(func_args) ;
        case "TripleStep": return build_simple_func!(TripleStep)(func_args) ;
        case "LinearRamp": return build_simple_func!(LinearRamp)(func_args) ;
        case "CubicRamp": return build_simple_func!(CubicRamp)(func_args) ;
        case "SqrtRamp": return build_simple_func!(SqrtRamp)(func_args) ;
        case "PiecewiseFunction": return build_piecewise_func(func_args) ;
        case "FromData": return build_from_data(func_args) ;
        case "Unassigned": return new Unassigned ;

        default:
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;
            
            throw new InvalidDefinition(
                "%s is not a recognised math function type".format(type)) ;
        }
    }
}

// Splits the arguments into an array and builds the MathFunction
private T build_simple_func(T)(const string func_args)
{
    import std.conv: to ;
    import std.string: split ;

    double[] args = func_args
        .split(",")
        .to!(double[]) ;
        
    return new T(args) ;
}

/* Parses a string of piecewise function arguments of the form: 
 *    [Func1(args1), Func2(args2),..], [x1, x2...]
 * and returns a PiecewiseFunction object
 */
private from!"q1dcfd.utils.math".MathFunction build_piecewise_func(const string func_args)
{
    import std.range: retro ;
    import std.string: indexOf, split, startsWith ;
    import std.algorithm.searching: canFind ;
    import std.conv: to ;
    import std.algorithm.iteration: map ;
    import std.array ;
    import std.format ;

    import q1dcfd.utils.math: PiecewiseFunction ;

    auto i1 = func_args.retro.indexOf("[") ;
    auto i2 = func_args.retro.indexOf("]") ;
    auto p1 = func_args.indexOf("[") ;
    auto p2 = func_args.indexOf("]") ; 
    
    string[] intervals_string = 
        func_args[func_args.length-i1..func_args.length-i2-1].split(",") ;
    
    double[] intervals = new double[](intervals_string.length) ;
    foreach(i, string_int ; intervals_string)
    {
        if(string_int.canFind("infinity"))
        {
            if(string_int.startsWith("-"))
            {
                intervals[i] = -double.infinity ;
            }
            else
            {
                intervals[i] = double.infinity ;
            }
        }
        else 
        {
            intervals[i] = string_int.to!double ;
        }
    }

    // split the second major argument into a list of subfunctions and parse
    // into MathFunction objects
    
    // a list of piecewise function string definitions
    string[] sub_functions ;

    // the current partially assembled subfunction
    string buffer ;
    
    foreach(snippet ; func_args[p1+1..p2].split(","))
    {
        // a continuation of a partially assembled statement
        if(buffer != "")
        {
            buffer ~= "," ~ snippet ;
            if(snippet[$-1].to!string == ")")
            {
                sub_functions ~= buffer ;
                buffer = "" ;
            }
            continue ;
        }

        // a fresh buffer, look for an open parenthesis
        if(snippet.canFind("("))
        {
            // function with a single argument
            if(snippet[$-1].to!string == ")") sub_functions ~= snippet ;
            // first argument of a multi argument function. Start fresh buffer
            else buffer ~= snippet ;
        }
        // a fresh buffer, does not contain an opening parenthesis
        // Probably a constant function
        else sub_functions ~= snippet ;
    }

    // check for an incomplete partially assembled subfunction
    if(buffer != "") throw new Exception(
        "Could not parse definition %s. Stuck on incomplete buffer %s"
        .format(func_args, buffer)) ;

    // check for an unexpected number of sub functions
    if(sub_functions.length != intervals.length) throw new Exception(
        "Expected %u piecewise functions, but found %u: %(%s,%)".format(
            intervals.length, sub_functions.length, sub_functions)) ;

    // translate each sub function
    auto pieces = sub_functions.map!(func => func.read_boundary_function).array ;
    
    return new PiecewiseFunction(pieces, intervals) ;
}

/**
 * Loads a set of (x,property) data from a saved binary file and fits a cubic
 * spline through the data
 */
private from!"q1dcfd.utils.math".MathFunction build_from_data(const string directory)
{
    import std.path ;
    import std.string: join ;
    import std.stdio ;
    import std.format ;

    import q1dcfd.utils.math: Polynomial, CubicSpline ;
    import q1dcfd.lconfig: ROOT_DIRECTORY ;

    auto abs_directory 
        = [ROOT_DIRECTORY, dirSeparator, "data", dirSeparator, directory].join ;
    auto f = File(abs_directory, "r") ;
    auto buf = f.rawRead(new double[](f.size/double.sizeof)) ;

    if(buf.length % 2 != 0) throw new Exception(
        "Input data %s not valid, length is not even".format(abs_directory)) ;

    auto x = buf[0..$/2] ;
    auto y = buf[$/2..$] ;

    // Many data points, use a cubic spline
    if(x.length > 3)
    {
        return new CubicSpline(x, y) ;
    }
    // Three data points, use a quadratic
    else if(x.length == 3)
    {
        auto dx20 = x[2] - x[0] ;
        auto dx21 = x[2] - x[1] ;
        auto dx10 = x[1] - x[0] ;

        auto a = y[2]/(dx20*dx21) - y[1]/(dx10*dx21) + y[0]/(dx10*dx20) ;
        auto b = -(y[2]*(x[0] + x[1])/(dx20*dx21) 
            - y[1]*(x[2] + x[0])/(dx10*dx21) + y[0]*(x[1] + x[2])/(dx10*dx20)) ;
        auto c = x[0]*x[1]*y[2]/(dx20*dx21) - x[0]*x[2]*y[1]/(dx10*dx21) 
            + x[1]*x[2]*y[0]/(dx10*dx20) ;

        return new Polynomial([c,b,a]) ;
    }
    // Two data points, a line
    else if(x.length == 2)
    {
        auto a = (y[1] - y[0])/(x[1] - x[0]) ;
        auto b = -(y[1] - y[0])/(x[1] - x[0])*x[0] + y[0] ;
        return new Polynomial([b, a]) ;
    }
    // Single data point, constant function
    else if(x.length == 1)
    {
        return new Polynomial([y[0]]) ;
    }
    // No data points, Uh oh
    else
    {
        throw new Exception("Data %s is empty!".format(abs_directory)) ;
    }
}

unittest
// Test for reading a constant numeric definition
{
    import std.conv: to ;
    import q1dcfd.utils.math: Polynomial ;

    auto transient = "1.5".read_boundary_function.to!Polynomial ;
    assert(transient.coeffs == [1.5]) ;
}

unittest
// Test for reading a polynomial function definition
{
    import std.conv: to ;
    import q1dcfd.utils.math: Polynomial ;
    
    auto transient = "Polynomial(2.0,4.0,-2.0)".read_boundary_function.to!Polynomial ;
    assert(transient.coeffs == [2.0, 4.0, -2.0]) ;
}

// Test for reading a piecewise function definition
unittest
{
    import std.conv: to ;
    import q1dcfd.utils.math: Polynomial, PiecewiseFunction ;

    auto transient = ("PiecewiseFunction([Polynomial(2.0,3.0),Polynomial(4.0)],"
                      ~"[1.0,infinity])")
        .read_boundary_function.to!PiecewiseFunction ;
    assert(transient.pieces.length == 2) ;
    assert(transient.pieces[0].to!Polynomial.coeffs == [2.0, 3.0]) ;
    assert(transient.pieces[1].to!Polynomial.coeffs == [4.0]) ;
    assert(transient.intervals == [1.0, double.infinity]) ;
}

// Unittesting a piecewise function definition containing constant functions
// and mixed function types
unittest
{
    import std.conv: to ;
    import q1dcfd.utils.math: PiecewiseFunction ;

    auto transient = ("PiecewiseFunction([0.0,Polynomial(1.0),Polynomial(1.0,"
                      ~"2.0)],[1.0,2.0,infinity])")
        .read_boundary_function.to!PiecewiseFunction ;
    
}

// Unittest for reading a step function definition
unittest
{
    import std.conv: to ;
    import q1dcfd.utils.math: Step ;

    auto transient = "Step(1.0,2.0,1.5)".read_boundary_function.to!Step ;
    assert(transient.evaluate(1.4999) == 1.0) ;
    assert(transient.evaluate(1.5001) == 2.0) ;
    assert(transient.evaluate(-double.infinity) == 1.0) ;
    assert(transient.evaluate(double.infinity) == 2.0) ;   
}

// Unittest for an invalid Definition
unittest
{
    import std.exception: assertThrown ;
    assertThrown(read_boundary_function("Polienomial(1.0,2.0)")) ;
    assertThrown(read_boundary_function("Polynomial 1.0,2.0)")) ;
    assertThrown(read_boundary_function("Polynomial(1.0,2.0")) ;
}

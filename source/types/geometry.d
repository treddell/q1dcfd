module q1dcfd.types.geometry;

// Geometry container for a quasi 1d control volume
// TODO: Deprecate, will be replaced with FluidCellGeometry and
// SolidCellGeometry
struct NodeGeometry 
{
    pragma(msg, "NodeGeometry requires deprecation");
    double length = 0.0; // node length (m)
    double transverse_length ; // transverse length in y or z orientation (m)
    double rA ; // control volume right side area (m2)
    double lA ; // control volume left side area (m2)
    double tA ; // control volume top area (m2)
    double bA ; // control volume bottom area (m2)
    double htA ; // total heat transfer area (m2)
    double volume = 0.0 ; // total volume contained (m3)
    double hydr_d ; // hydraulic diameter (m)
    double delta ; // average surface roughness
    double lx ; // co-ordinate of left side volume face 
    double rx ; // co-ordinate of right side volume face
    double specific_density ; // 1/volume (1/m3)
}

/**
 * Generates a cross sectional object from a parsed set of definitions
 */
CrossSection build_cross_section(string[string] defs,
                                 const string geom_name)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required ;

    // get the type of the geometry definition
    auto type = defs.retrieve("type").required!string ;

    switch(type)
    {
        case "ChannelCrossSection": return new ChannelCrossSection(defs, geom_name) ;
        case "WallCrossSection": return new WallCrossSection(defs, geom_name) ;
        case "Dimensionless": return new Dimensionless ;
        
        default:
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;

            throw new InvalidDefinition("CrossSection type %s not recognized".format(type)) ;
        }
    }
}


/**
 * A cell geometry generator
 *
 * Constructed with functions describing the profile of the geometry over some length
 *
 * Then constructs individual cell geometries when the discetisation is called
 */
interface CrossSection
{
    // TODO: may be unnessary when v0.3 is complete
    pure nothrow
    {
        double cross_area(const double x);
        double volume(const double xa, const double xb);
    }
}

/// CrossSection slot placeholder for 0D objects
/// Will be deprecated
final class Dimensionless: CrossSection 
{
    pragma(msg, "Dimensionless requires deprecation");

    // Does not occupy volume
    pure nothrow
    {
        double volume(const double xa, const double xb) {return 0.0 ;}

        // Does not occupy area
        double cross_area(const double x) {return 0.0 ;}
    }
} 

/**
 * Geometry constuctor for a channel
 */ 
final class ChannelCrossSection: CrossSection
{
    import q1dcfd.utils.math: MathFunction ;

    private
    {
        MathFunction _cross_area ;
        MathFunction _hydraulic_diameter ;
        MathFunction _heat_circumference ;
        MathFunction _roughness ;
    }

    this(string[string] defs, const string name)
    {
        import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;
        import q1dcfd.utils.dictionary_tools: required, require_empty, retrieve, defaults ;

        scope(exit) defs.require_empty(name) ;

        this._cross_area = defs.retrieve("cross_area").required!string
            .read_boundary_function ;

        this._hydraulic_diameter = defs.retrieve("hydraulic_diameter").required!string
            .read_boundary_function ;

        this._heat_circumference = defs.retrieve("heat_circumference").required!string
            .read_boundary_function ;

        this._roughness = defs.retrieve("roughness").defaults("0.0")
            .read_boundary_function ;
    }

    pure nothrow
    {
        double cross_area(const double x) {return _cross_area.evaluate(x) ;}

        double volume(const double xa, const double xb) {return _cross_area.integrate(xa, xb) ;}

        double heat_circumference(const double x) { return _heat_circumference.evaluate(x); }

        double heat_area(const double xa, const double xb)
        {
            return _heat_circumference.integrate(xa, xb) ;
        }

        double hydraulic_diameter(const double x) {return _hydraulic_diameter.evaluate(x) ;}

        double hydraulic_diameter(const double x_left, const double x_right)
        {
            return _hydraulic_diameter.integrate(x_left, x_right)/(x_right - x_left);
        }

        double roughness(const double x) {return _roughness.evaluate(x) ;}
    }
}

/**
 * Geometry constructor for a wall
 */
final class WallCrossSection: CrossSection
{
    import q1dcfd.utils.math: MathFunction ;

    private MathFunction _width ;

    immutable
    {
        double thickness ;
        double density ;
        double specific_heat ;
        double conductivity ;
    }

    this(string[string] defs, const string name)
    {
        import q1dcfd.utils.dictionary_tools: required, require_empty, retrieve, greater ;
        import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;

        scope(exit) defs.require_empty(name) ;
        
        this.thickness = defs.retrieve("thickness").required!double.greater(0.0) ;
        this._width = defs.retrieve("width").required!string.read_boundary_function ;
        this.density = defs.retrieve("density").required!double.greater(0.0) ;
        this.specific_heat = defs.retrieve("specific_heat").required!double.greater(0.0) ;
        this.conductivity = defs.retrieve("conductivity").required!double.greater(0.0) ;
    }

    pure nothrow 
    {
        double volume(const double xa, const double xb) {return thickness*_width.integrate(xa, xb) ;}

        double cross_area(const double x) {return _width.evaluate(x)*thickness ;}

        double width(const double x) {return _width.evaluate(x) ;}

        // Area of the xz plane of the cell
        double transverse_area(const double xa, const double xb) { return _width.integrate(xa, xb) ;}
    }
}

/**
 * Geometry characteristics of a fluid cell
 */
struct FluidCellGeometry
{
    immutable
    {
        // Cross sectional flow area on the left side of the interface (m2)
        double area_left;

        // Cross sectional flow area on the right side of the interface (m2)
        double area_right;

        // Volume contained within the cell (m3)
        double volume;
        
        // Co-ordinates of the left interface in the domain local frame (m)
        double x_left;

        // Co-ordinate of the right interface in the domain local frame (m)
        double x_right;

        // length of the cell (m)
        double length;

        // Effective heat transfer area of the cell (m2)
        double area_ht;

        // Average hydraulic diameter of the cell (m)
        double hydraulic_diameter;

        // reciprocal of volume (1/m3)
        double specific_density;
    }

    this(ChannelCrossSection cross_section,
         const double x_left,
         const double x_right)
    in
    {
        assert(x_right > x_left, "Cell occupies no volume");
    }
    do
    {
        this.x_left = x_left;
        this.x_right = x_right;
        this.length = x_right - x_left;
        this.area_left = cross_section.cross_area(x_left);
        this.area_right = cross_section.cross_area(x_right);
        this.volume = cross_section.volume(x_left, x_right);
        this.specific_density = 1/volume;
        this.area_ht = cross_section.heat_area(x_left, x_right);
        this.hydraulic_diameter = cross_section.hydraulic_diameter(x_left, x_right);
    }
}

/**
 * Geometry characteristics of a solid cell
 */
struct SolidCellGeometry
{
    immutable
    {
        // Cross sectional flow area on the left side of the interface (m2)
        double area_left;

        // Cross sectional flow area on the right side of the interface (m2)
        double area_right;

        // Volume contained within the cell (m3)
        double volume;
        
        // Co-ordinates of the left interface in the domain local frame (m)
        double x_left;

        // Co-ordinate of the right interface in the domain local frame (m)
        double x_right;

        // length of the cell (m)
        double length;
    }

    this(WallCrossSection cross_section,
         const double x_left,
         const double x_right)
    in
    {
        assert(x_right > x_left, "Cell occupies no volume");
    }
    do
    {
        this.x_left = x_left;
        this.x_right = x_right;
        this.length = x_right - x_left;
        this.area_left = cross_section.cross_area(x_left);
        this.area_right = cross_section.cross_area(x_right);
        this.volume = cross_section.volume(x_left, x_right);
    }
}

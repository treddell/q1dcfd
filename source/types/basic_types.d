module q1dcfd.types.basic_types ;

import q1dcfd.utils.metaprogramming: from ;

// Configure the global floating point precision 
// (set via compilation flag -prec=<float_prec>/<high_prec>

// default : double (64 bit) standard precision
// float_prec: float (32 bit) low precision
// high_prec: real (64bit/80bit) highest precision available on machine
version(float_prec) {alias Real = float ; }
else version(high_prec) {alias Real = real ; }
else {alias Real = double ;}

// condition which must return true, carries an error message with it
alias AssertionResult = from!"std.typecons".Tuple!(bool, "success", string, "msg") ;
alias AssertionCondition = AssertionResult delegate() ;

// Controller input port
alias InputPort = Real delegate() ;

// time varying 'boundary condition'
alias Transient = Real delegate(const Real) ;

/*
 * Stores a delegate which links to a property to save to file
 * Used for system level properties which are written to file
 */
struct SystemProperty
{
    void delegate(ref Real[]) write_to ;
    
    this(void delegate(ref Real[]) link) {this.write_to = link ; }
}


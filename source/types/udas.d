module q1dcfd.types.udas ;

/**
 * Lists the recognisable user defined attribute flags
 *
 * 0. required
 * 
 *  @required private T _field ;
 *
 *  Generates a getter "T field()", setter "void set_field(T field)" and adds an
 *  AssertionCondition to Simulation.initialisation_checks that set_field has been called
 *  at least once. Essentially generates the following code
 *
 *  ----------------------------------------------------------------
 *  import q1dcfd.simulation: Simulation ;
 *  import q1dcfd.config: AssertionResult ;
 *  
 *  private
 *  {
 *      bool _field_assigned = false ;
 *  }
 *
 *  T field() {return _field ;}
 *
 *  void set_field(T field)
 *  {
 *      _field_assigned = true ;
 *      _field = field ;
 *  }
 *
 *  private AssertionResult assert_field_assigned()
 *  {
 *      if(_field_assigned)
 *      {
 *          return typeof(return)(true, "") ;
 *      }
 *      return typeof(return)(false, "@required field has not been assigned")
 *  }
 *  
 *  Simulation.add_initialisation_condition(&assert_field_assigned) ;
 *
 *  1. unique
 *
 *  @unique private T _field
 *
 *  Similar to required, except does not generate an initialisation check, and adds
 *  a check to set_field to ensure that it is called only once
 *
 *  2. updateInvariant
 *
 *  @updateInvariant bool check_condition() {...}
 *
 *  Designates that the function must be run every time the nodes update method is exited
 *  if failed, will throw an exception and halt the simulation. Only functions returning bool
 *  and taking no input can be marked with @updateInvariant
 *
 *  3. balanceInvariant
 *
 *  @balanceInvariant bool check_condition() {...}
 *
 *  Identical to updateInvariant except check will be run after exiting update_balance method
 *
 *  4. recordableProperty
 *
 *  @recordableProperty Real prop() {...}
 *
 *  Designates that the result of the function may be saved to disk by indicating that the property
 *  should be saved from the definition script. Adds the function to a case statement of the nodes
 *  get(string key) method, only has an effect when used on classes deriving from Node
 *
 *  5. addToLibrary
 *
 *  @addToLibrary
 *  class Derived: Base {...}
 *
 *  Indicates that the class should be indexed so that it may be constructed and used in Simulations
 *  adds the class to the object construction script
 *
 *  6. initCondition
 *
 *  @initCondition
 *  AssertionResult check_condition() {...}
 *
 *  Indicates that the function should be added to the initialisation_conditions list which must be
 *  run before simulation can commence
 */
enum
{
    required, 
    unique,
    updateInvariant,
    balanceInvariant,
    recordableProperty,
    addToLibrary,
    initCondition
};

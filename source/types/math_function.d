module q1dcfd.types.math_function;

/** 
 * Base class, an analytical function with known derivatives and 
 * anti-derivatives
 */
abstract class MathFunction 
{
    pure nothrow
    {
        // evaluates the function at the point x
        double evaluate(const double x);

        // returns the analytical derivative of the function
        MathFunction derivative();

        // returns the antiderivative of f(x)
        MathFunction indefinite_integral();

        // A flag to indicate that the function is not the 'Unassigned' placeholder
        bool not_null() {return true;}
    }

    pure nothrow double integrate(const double xa, const double xb)
    {
        return indefinite_integral.evaluate(xb) - indefinite_integral.evaluate(xa);
    }
}

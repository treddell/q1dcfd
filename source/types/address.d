module q1dcfd.types.address ;

/*
 * Stores the global positional information of a node
 */
struct Address
{
    immutable
    {
        uint index_l ;
        uint index_r ;
        uint w_index_l ;
        uint id ;
        string owner ;
    }

    this(const uint index_l,
         const uint index_r,
         const uint w_index_l, 
         const uint id,
         const string owner)
    in 
    {
        assert(index_r >= index_l, "index_r<index_l") ;
    }
    do
    {
        this.index_l = index_l ;
        this.index_r = index_r ;
        this.w_index_l = w_index_l ;
        this.id = id ;
        this.owner = owner ;
    }
}


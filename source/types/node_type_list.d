module q1dcfd.types.node_type_list;

// Define a set of control volume/node/element types
enum node_type
{
    fluid_volume_interior = 0,
    fluid_subsonic_inflow,
    fluid_subsonic_outflow,
    fluid_momentum_energy_discontinuity,
    fluid_interior_junction,
    fluid_interior_mass_generation,
    thermal_interior,
    thermal_boundary,
    controller_node,
    fluid_point,
    fluid_thin_film,
}

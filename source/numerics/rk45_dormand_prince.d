module q1dcfd.numerics.rk45_dormand_prince ;

version(high_prec) { import std.math : fabs, pow ;}
else { import core.stdc.math: fabs, pow ;}

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

import std.algorithm.comparison: max, min ;
import std.algorithm.iteration: reduce ;
import q1dcfd.numerics.integrator: Integrator;
import q1dcfd.config : Real ;

final class DOPRI: Integrator 
{
    /**
    Adaptive step size Runge Kutta Dormand Prince method
    (default MATLAB ODE45 method)

    Is a First Stage as Last Method, meaning that the last stage of the current
    step is equal to the first stage of the next step, hence this result is
    cached between iterations, bringing the number of function evaluations per
    step down to 6

    Runge-Kutta methods do not distinguish between physical significance of
    variables and integrates each in parallel

    Initialize with:
        size_t size: order of the system of ODE's
        Real min_delta_t, initial_delta_t, max_delta_t: minimum, initial and
            maximum time step
        Real adap_sf, adap_tol: safety factor and maximum difference in
            estimates
    **/
    private 
    {
        Real[] k1 ;
        Real[] k2 ;
        Real[] k3 ;
        Real[] k4 ;
        Real[] k5 ;
        Real[] k6 ;
        Real[] error ;
        Real[] x_new ;
        Real[] z_new ;

        // constants
        enum Real c2 = 0.2 ;
        enum Real c3 = 0.3 ;
        enum Real c4 = 0.8 ;
        enum Real c5 = 8.0/9.0 ;

        enum Real a21 = 0.2 ;
        enum Real a31 = 0.075 ;
        enum Real a32 = 0.225 ;
        enum Real a41 = 44.0/45.0 ;
        enum Real a42 = -56.0/15.0 ;
        enum Real a43 = 32.0/9.0 ;
        enum Real a51 = 19372.0/6561.0 ;
        enum Real a52 = -25360.0/2187.0 ;
        enum Real a53 = 64448.0/6561.0 ;
        enum Real a54 = -212.0/729.0 ;
        enum Real a61 = 9017.0/3168.0 ;
        enum Real a62 = -355.0/33.0 ;
        enum Real a63 = 46732.0/5247.0 ;
        enum Real a64 = 49.0/176.0 ;
        enum Real a65 = -5103.0/18656.0 ;
        enum Real a71 = 35.0/384.0 ;
        enum Real a73 = 500.0/1113.0 ;
        enum Real a74 = 125.0/192.0 ;
        enum Real a75 = -2187.0/6784 ;
        enum Real a76 = 11.0/84.0 ;

        enum Real q1 = 5179.0/57600.0 ;
        enum Real q3 = 7571.0/16695.0 ;
        enum Real q4 = 393.0/640.0 ;
        enum Real q5 = -92097.0/339200.0 ;
        enum Real q6 = 187.0/2100.0 ;
        enum Real q7 = 0.025 ;
    }

    this(const Real min_delta_t, const Real max_delta_t, const Real adap_sf,
        const Real adap_tol) 
    {
        super(min_delta_t, max_delta_t, adap_sf, adap_tol) ;
    }

    override void reserve_workspace(const size_t size) 
    {
        super.reserve_workspace(size) ;
        this.k1.length = size ;
        this.k2.length = size ;
        this.k3.length = size ;
        this.k4.length = size ;
        this.k5.length = size ;
        this.k6.length = size ;
        this.error.length = size ;
        this.x_new.length = size ;
        this.z_new.length = size ;
    }

    @fastmath
    override Real update(
        void function(ref Real[], const ref Real[], const Real) march, 
        ref Real[] state, 
        ref Real[] balance, 
        const Real t, 
        const Real dt) 
    {
        // first stage
        x0[] = state[] ;
        k1[] = dt*balance[] ;
        state[] = x0[] + a21*k1[] ;

        march(balance, state, t + c2*dt) ;

        // second stage
        k2[] = dt*balance[] ;
        state[] = x0[] + a31*k1[] + a32*k2[] ;

        march(balance, state, t + c3*dt) ;

        // third stage
        k3[] = dt*balance[] ;
        state[] = x0[] + a41*k1[] + a42*k2[] + a43*k3[] ;

        march(balance, state, t + c4*dt) ;

        // fourth stage
        k4[] = dt*balance[] ;
        state[] = x0[] + a51*k1[] + a52*k2[] + a53*k3[] + a54*k4[] ;

        march(balance, state, t + c5*dt) ;

        // fifth stage
        k5[] = dt*balance[] ;
        state[] = x0[] + a61*k1[] + a62*k2[] + a63*k3[] + a64*k4[] + a65*k5[] ;

        march(balance, state, t + dt) ;

        // sixth stage
        k6[] = dt*balance[] ;

        // cache fifth order estimate
        x_new[] = x0[] + a71*k1[] + a73*k3[] + a74*k4[] + a75*k5[] + a76*k6[] ;
        state[] = x_new[] ;

        march(balance, state, t + dt) ;

        // fourth order accurate solution
        z_new[] = x0[] + q1*k1[] + q3*k3[] + q4*k4[] + q5*k5[]
            + q6*k6[] + q7*dt*balance[] ;

        error[] = 1.0 - x_new[]/z_new[] ;
        auto e = max(fabs(reduce!(max)(error)), fabs(reduce!(min)(error)), 
            Real.epsilon) ;
        auto dt_new = adap_sf * dt * pow(adap_tol /e, 0.25) ;

        // if sufficient accuracy not obtained, reduce step size and try again
        if(e > adap_tol) 
        {
            if(dt_new > min_delta_t) 
            {
                state[] = x0[] ;
                march(balance, state, t) ;
                return update(march, state, balance, t, max(dt*0.5, min_delta_t)) ;
            } 
            else 
            {
                dt_new = min_delta_t ;
            }
        }

        // if the adaptive step size is above the maximum allowed then truncate
        // to the max
        return min(dt_new, max_delta_t) ;
    }
}
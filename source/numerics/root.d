module q1dcfd.numerics.root ;

import q1dcfd.config: Real ;
import q1dcfd.utils.errors: NonConvergenceError ;
import q1dcfd.utils.math: square, sign ;

import std.math: fabs, sqrt ;
import std.format ;
import std.typecons: Tuple ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

// Define continuous and smooth well behaved functions
alias C1Function = double function(double) pure nothrow ;
alias C1Delegate = double delegate(double) pure nothrow ;

// State of root finder routine upon exit
enum exit_code
{
    SUCCESS = 0,
    MAX_ITERATIONS_EXCEEDED,
    ILL_POSED
}

// results tuple for pure root finders
private alias Result = Tuple!(Real, "x", exit_code, "exit_success", Real, "delta") ;

/**
 * Determines a root of the function f(x) using the secant method, based off a 
 * starting point x0.
 * 
 * Function is designed to be called using root1d handler
 * 
 * Terminates once |x[n+1] - x[n]| < tol or iterations > max_iter
 * 
 * Inputs:
 *     C1Function, C1Delegate: f(x) = 0 function to determine root of
 *     Real x0: initial guess for the root
 *     const ulong max_iter: Maximum number of iterates
 *     const Real tol: maximum difference between iterates
 *     const Real relaxation: relaxation factor on secant chord 
 * 
 * Returns:
 *     Result tuple: containing:
 *         x: root
 *         exit_success: 0 for no errors, >0 otherwise
 *         delta: final iteration difference in iterates
 */
@fastmath
pure nothrow Result secant(T)
(
    T f, 
    const Real x0, 
    const ulong max_iter = 50, 
    const Real tol=1.48E-8, 
    const Real relaxation = 1.0
)
    if(is(T==C1Function) || is(T==C1Delegate))
{
    auto x_prev = cast(Real) x0 ;
    auto x_new = (x0 >= 0) ? x0*(1.0 + 1.0E-4) + 1.0E-4 : x0*(1.0 + 1.0E-4) - 1.0E-4 ;
    auto y_prev = f(x_prev) ;
    auto y_new = f(x_new) ;
    auto delta = (x_new - x_prev)/(y_new - y_prev)*y_new*relaxation ;

    foreach(immutable i ; 0..max_iter)
    {
        if(fabs(delta)<tol) return Result(x_new, exit_code.SUCCESS, delta) ;
        
        // ill posed f(x) breakout error
        if(y_new - y_prev == 0) return Result(x_new, exit_code.ILL_POSED, delta) ;

        x_prev = x_new ;
        x_new -= delta ;
        y_prev = y_new ;
        y_new = f(x_new) ;
        delta = (x_new - x_prev)/(y_new - y_prev)*y_new*relaxation ;
    }

    // non convergence breakout
    return Result(x_new, exit_code.MAX_ITERATIONS_EXCEEDED, delta) ;
}

/**
 * Ridders method (Ridders, C. (1979). "A new algorithm for computing a single 
 * root of a real continuous function". IEEE Transactions on Circuits and Systems. 
 * 26: 979–980)
 * 
 * Simple but effective bracketing method with an order of convergence ~sqrt(2)
 * 
 * Designed to be called using root1d handler
 * 
 * Input:
 *     C1Function/C1Delegate : f(x) = 0 function to determine root of
 *     Real x0, x2: two values bounding the root
 *     const Real tol: maximum difference between iterates
 *     const ulong max_N: maximum number of iterates
 */
@fastmath
pure nothrow Result ridder(T)
(
    T f, 
    Real x0, 
    Real x2, 
    const Real tol = 1.48E-8,
    const ulong max_N = 100
)
    if(is(T==C1Function) || is(T==C1Delegate))
{
    Real x1, x3, f1, f3, delta ;
    auto f0 = f(x0) ;
    auto f2 = f(x2) ;

    foreach(immutable count ; 0..max_N)
    {
        delta = fabs(x2 - x0) ;
        if(delta < tol)
        {
            return Result(0.5*(x0 + x2), exit_code.SUCCESS, delta) ;
        }

        x1 = 0.5*(x0 + x2) ;
        f1 = f(x1) ;
        x3 = x1 + (x1 - x0)*(sign(f0)*f1)/(sqrt(f1.square-f0*f2)) ;
        f3 = f(x3) ;

        if(f1*f3 < 0 )
        {
            if(x1 < x3)
            {
                x0 = x1 ;
                x2 = x3 ;
            }
            else
            {
                x0 = x3 ;
                x1 = x1 ;
            }
        }
        else if(f0*f3 < 0)
        {
            x0 = x0 ;
            x2 = x3 ;
        }
        else
        {
            x0 = x3 ;
            x2 = x2 ;
        }
    }

    return Result(x0, exit_code.MAX_ITERATIONS_EXCEEDED, x2 - x0) ;
}

/**
 * Double attempt root finding method, first attempts to locate a root using 
 * the secant method, and it that fails then tries a bounded ridder method. 
 * If that still dosent work then you are really up shit creek 
 */
@fastmath
Real root1d(T)
(
    T f, 
    Real x0, 
    Real x2, 
    Real x_guess, 
    const Real tol = 1.48E-8, 
    const ulong max_N1 = 25, 
    const ulong max_N2 = 100,
    const Real relaxation = 1.0
)
    if(is(T==C1Function) || is(T==C1Delegate))
in 
{
    assert(tol > 0, "tolerance must be positive") ; 
    assert(f(x0)*f(x2) <= 0, "Interval (%f,%f) does not bracket a distinct root".format(x0, x2)) ;
    assert(relaxation > 0, "Secant method numerical relaxation must be positive") ;
}
out(result)
{
    assert(result <= x2 && result >= x0,
           "Root %f outside of expected bounds [%f,%f]".format(result, x0, x2)) ;
}
do
{
    // first pass, attempt to solve using a secant method
    string secant_msg ;
    auto secant_result = secant(f, x_guess, max_N1, tol, relaxation) ;

    final switch(secant_result.exit_success)
    {
        case exit_code.SUCCESS:
        {
            return secant_result.x ;
        }
        case exit_code.ILL_POSED:
        {
            secant_msg = ("|f(x[n+1]) - f(x[n])|<MACHINE_TOL but |x[n+1]-x[n]| = %f > tol = %f "
                          ~"in secant method.").format(secant_result.delta, tol) ;
            break ;
        }
        case exit_code.MAX_ITERATIONS_EXCEEDED:
        {
            secant_msg = format!("secant method failured to converge in %u "
                ~"iterations, final value was %f, difference in iterates was %f")
                (max_N1, secant_result.x, secant_result.delta) ;
            break ;
        }
    }

    // second pass, fall back to a ridders method
    string ridder_msg ;
    auto ridder_result = ridder(f, x0, x2, tol, max_N2) ;
    if(ridder_result.exit_success == exit_code.SUCCESS) return ridder_result.x ;

    ridder_msg = "Ridders method failed to converge to within "
         ~"tolerance. Final interval was (%f, %f)"
         .format(ridder_result.x, ridder_result.x + ridder_result.delta) ;

    string msg = "Failed both root finding passes: \n" ~ "Secant:\n" ~ secant_msg
        ~ "Ridder:\n" ~ ridder_msg ;

    // Failed both attempts
    throw new NonConvergenceError(msg) ;
}

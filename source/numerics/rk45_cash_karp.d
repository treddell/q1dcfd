module q1dcfd.numerics.rk45_cash_karp ;

import std.stdio ;

version(high_prec) { import std.math : fabs, pow ;}
else { import core.stdc.math: fabs, pow ;}

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

import std.algorithm.comparison: max, min ;
import std.algorithm.iteration: reduce ;
import q1dcfd.numerics.integrator: Integrator;
import q1dcfd.config : Real ;

final class RKCK45: Integrator
{
    /*
    Adaptive step size Runge Kutta Cash Karp method. Has better constraints to
    handle stiff equations

    Runge-Kutta methods do not distinguish between physical significance of
    variables and integrates each in parallel

    Initialize with:
        size_t size: order of the system of ODE's
        Real min_delta_t, initial_delta_t, max_delta_t: minimum, initial and
            maximum time step
        Real adap_sf, adap_tol: safety factor and maximum difference in
            estimates
    */
    private 
    {
        Real[] k1 ;
        Real[] k2 ;
        Real[] k3 ;
        Real[] k4 ;
        Real[] k5 ;
        Real[] k6 ;
        Real[] error ;
        Real[] x_new ;
        Real[] z_new ;

        // constants
        enum Real c2 = 0.2 ;
        enum Real c3 = 0.3 ;
        enum Real c4 = 0.6 ;
        enum Real c6 = 7.0/8.0 ;

        enum Real a21 = 0.2 ;
        enum Real a31 = 0.075 ;
        enum Real a32 = 0.225 ;
        enum Real a41 = 0.3 ;
        enum Real a42 = -0.9 ;
        enum Real a43 = 1.2 ;
        enum Real a51 = -11.0/54.0 ;
        enum Real a52 = 2.5 ;
        enum Real a53 = -70.0/27.0 ;
        enum Real a54 = 35.0/27.0 ;
        enum Real a61 = 1631.0/55296.0 ;
        enum Real a62 = 175.0/512.0 ;
        enum Real a63 = 575.0/13824.0 ;
        enum Real a64 = 44275.0/110592.0 ;
        enum Real a65 = 253.0/4096.0 ;

        enum Real q1 = 2825.0/27648.0 ;
        enum Real q3 = 18575.0/48384.0 ;
        enum Real q4 = 13525.0/55296.0 ;
        enum Real q5 = 277.0/14336.0 ;
        enum Real q6 = 0.25 ;

        enum Real p1 = 37.0/378.0 ;
        enum Real p3 = 250.0/621.0 ;
        enum Real p4 = 125.0/594.0 ;
        enum Real p6 = 512.0/1771.0 ;
    }

    this(const Real min_delta_t, const Real max_delta_t, const Real adap_sf,
        const Real adap_tol) 
    {
        super(min_delta_t, max_delta_t, adap_sf, adap_tol) ;
    }

    override void reserve_workspace(const size_t size) 
    {
        super.reserve_workspace(size) ;
        this.k1.length = size ;
        this.k2.length = size ;
        this.k3.length = size ;
        this.k4.length = size ;
        this.k5.length = size ;
        this.k6.length = size ;
        this.error.length = size ;
        this.x_new.length = size ;
        this.z_new.length = size ;
    }

    @fastmath
    override Real update(
        void function(ref Real[], const ref Real[], const Real) march,
        ref Real[] state, 
        ref Real[] balance, 
        const Real t,
        const Real dt) 
    {
        // first stage
        x0[] = state[] ;
        k1[] = dt*balance[] ;
        state[] = x0[] + a21*k1[] ;
        march(balance, state, t + c2*dt) ;

        // second stage
        k2[] = dt*balance[] ;
        state[] = x0[] + a31*k1[] + a32*k2[] ;
        march(balance, state, t + c3*dt) ;

        // third stage
        k3[] = dt*balance[] ;
        state[] = x0[] + a41*k1[] + a42*k2[] + a43*k3[] ;

        march(balance, state, t + c4*dt) ;

        // fourth stage
        k4[] = dt*balance[] ;
        state[] = x0[] + a51*k1[] + a52*k2[] + a53*k3[] + a54*k4[] ;

        march(balance, state, t + dt) ;

        // fifth stage
        k5[] = dt*balance[] ;
        state[] = x0[] + a61*k1[] + a62*k2[] + a63*k3[] + a64*k4[]
            + a65*k5[] ;

        march(balance, state, t + c6*dt) ;

        // sixth stage
        k6[] = dt*balance[] ;
        z_new[] = x0[] + p1*k1[] +p3*k3[] + p4*k4[] + p6*k6[] ;

        // cache fifth order estimate
        x_new[] = x0[] + q1*k1[] + q3*k3[] + q4*k4[] + q5*k5[] + q6*k6[] ;

        Real e, dt_new;
        if(error.length >= 1)
        {
            error[] = 1.0 - x_new[]/z_new[] ;
            e = max(fabs(reduce!(max)(error)), fabs(reduce!(min)(error)), 
                Real.epsilon) ;
            dt_new = adap_sf*dt*(adap_tol/e).pow(0.25) ;
        }
        else
        {
            // If only simulating incompressible components, the error vectory will
            // be empty, and the incompressible timestep will set the integration
            // timestep.
            e = 0.0;
            dt_new = Real.infinity;
        }

        // if sufficient accuracy not obtained, reduce step size and try again
        if(e > adap_tol) 
        {
            if(dt_new > min_delta_t) 
            {
                state[] = x0[] ;
                march(balance, state, t) ;
                return update(march, state, balance, t, max(dt*0.5, min_delta_t)) ;
            } 
            else 
            {
                dt_new = min_delta_t ;
            }
        } 

        state[] = z_new[] ;
        march(balance, state, t + dt) ;

        // if the adaptive step size is above the maximum allowed then truncate
        // to the max
        return min(dt_new, max_delta_t) ;
    }
}

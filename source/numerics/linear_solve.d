module q1dcfd.numerics.linear_solve ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

/**
 * Guassian elimination with partial pivoting and back subistitution
 *
 * Based upon an algorithm detailed in:
 * https://rosettacode.org/wiki/Gaussian_elimination#D
 *
 * With slight modification
 *
 */
@fastmath pure T[] gauss(T)(in T[][] A, in T[] b)
in 
{
    import std.algorithm: all ;
    assert(A.length == A[0].length) ;
    assert(A.length == b.length) ;
    assert(A.all!(row => row.length == A[0].length)) ;
}
do
{
    import std.math: fabs ;
    import std.numeric: dotProduct ;
    import std.algorithm ;
    import std.range: front, zip, array ;

    immutable m = b.length ;

    // set up augmented matrix
    auto B = A.zip(b).map!(c => [] ~ c[0] ~ c[1]).array;

    foreach(immutable k ; 0 .. B.length)
    {
        // find pivot for column k
        B[k..m].minPos!((x, y) => x[k] > y[k]).front.swap(B[k]) ;
        
        if(fabs(B[k][k]) < 10.0*T.epsilon)
        {
            throw new Exception("Matrix is singular or ill posed") ;
        }

        // do all rows below pivot
        foreach(immutable i ; k + 1 .. m)
        {
            B[i][k+1..m+1] -= B[k][k+1 .. m+1] * (B[i][k] / B[k][k]) ;

            // Fill lower triangular matrix with zeros
            B[i][k] = 0.0 ;
        }
    }

    // back substitution
    auto result = new T[](m) ;

    foreach_reverse(immutable i ; 0..m)
    {
        result[i] = (B[i][m] - B[i][i+1..m].dotProduct(result[i+1..m]))/B[i][i];
    }

    return result ;
}

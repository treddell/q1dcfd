module q1dcfd.numerics.integrator ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.config: Real ;

import std.exception: enforce ;
version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

abstract class Integrator
{
    /* abstract base class for an integrator

    Initialize with:
        size_t size: number of ODE's
        Real adap_sf: adaptive safety factor (for adaptive algorithms only)
        Real adap_tol: adpative tolerance (for adaptive algorithms only)

    methods:
        update(System system, Real t, Real dt): updates the system state
            from time t to time t + dt
    */
    protected Real[] x0 ;

    const Real adap_sf ;
    const Real adap_tol ;
    const Real min_delta_t ;
    const Real max_delta_t ;

    this(const Real min_delta_t, const Real max_delta_t, const Real adap_sf, 
        const Real adap_tol) 
    {
        this.min_delta_t = min_delta_t ;
        this.max_delta_t = max_delta_t ;
        this.adap_sf = adap_sf ;
        this.adap_tol = adap_tol ;
    }

    void reserve_workspace(const size_t size)
        // Reserve memory for the intermediate arrays
    {
        this.x0.length = size ;
    }

    @fastmath
    abstract Real update(
        void function(ref Real[], const ref Real[], const Real) march, 
        ref Real[] state, 
        ref Real[] balance, 
        const Real t, 
        const Real dt);
}
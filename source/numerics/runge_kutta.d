module q1dcfd.numerics.runge_kutta ;

import q1dcfd.numerics.integrator: Integrator;
import q1dcfd.config : Real ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

final class RK4: Integrator 
{
    // Classic Runge-Kutta 4th order algorithm
    enum Real C1 = 1.0/6.0 ;

    private 
    {
        Real[] k1 ;
        Real[] k2 ;
        Real[] k3 ;
    }

    this() 
    {
        // dummy inputs, rk4 is not adaptive algorithm
        super(Real.epsilon, Real.infinity, 1.0, 1.0) ;
    }

    override void reserve_workspace(const size_t size) 
    {
        super.reserve_workspace(size) ;
        this.k1.length = size ;
        this.k2.length = size ;
        this.k3.length = size ;
    }

    @fastmath
    override Real update(
        void function(ref Real[], const ref Real[], const Real) march, 
        ref Real[] state, 
        ref Real[] balance, 
        const Real t, 
        const Real dt) 
    {
        // first stage
        x0[] = state[] ;
        k1[] = dt*balance[] ;
        state[] = x0[] + 0.5*k1[] ;

        march(balance, state, t + 0.5*dt) ;

        // second stage
        k2[] = dt*balance[] ;
        state[] = x0[] + 0.5*k2[] ;

        march(balance, state, t + 0.5*dt) ;

        // third stage
        k3[] = dt*balance[] ;
        state[] = x0[] + k3[] ;

        march(balance, state, t + dt) ;

        //fourth stage
        state[] = x0[] + C1*(k1[] + 2.0*k2[] + 2.0*k3[] + dt*balance[]) ;
        march(balance, state, t + dt) ;

        return dt ;
    }
}
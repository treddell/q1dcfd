module q1dcfd.interfaces.root_interface ;

/* Root Interface type

An Interface describes an exchange of flux between neighbouring control volumes

*/

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

interface Interface
/* Ironic */
{
    @fastmath void update_flux() ;
}
module q1dcfd.interfaces.convective_interface ;

import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.control_volumes.thermal_node: ThermalNode ;
import q1dcfd.correlations: HeatTransferCorrelation, heat_transfer_map ;
import q1dcfd.config: Real ;
import q1dcfd.thermal_face: ThermalFace ;

import std.format ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

final class ConvectiveInterface: Interface 
{
    /*
     * Interface for a fixed grid solid to fluid element, describes heat
     * transfer between the two
     */
    private
    {
        ThermalNode solid_node ; // The solid control volume
        FluidNode fluid_node ; // the fluid control volume
        HeatTransferCorrelation heat_corr ; // heat transfer correlation
        Real q_dot ; // heat transfer
        ThermalFace face ;
        void delegate(Real) record_transfer;
    }

    this(ThermalNode solid_node,
         FluidNode fluid_node,
         ThermalFace face,
         const string name) 
    {
        this.solid_node = solid_node ;
        this.fluid_node = fluid_node ;
        this.face = face ;
        this.record_transfer = generate_qdot_record(face) ;

        this.heat_corr = heat_transfer_map(
            fluid_node, solid_node, fluid_node.ht_corr_name,
            fluid_node.thin_film_heat_transfer) ;
    }

    @fastmath override void update_flux()
    /* Calculate total heat transfer between the solid and fluid volumes */
    out
    {
        import std.math: isNaN ;
        assert(fluid_node.htc >=0 && !fluid_node.htc.isNaN, 
            format!("Invalid Heat Transfer Coefficient %f at interface of %s "
            ~"and %s")(fluid_node.htc, solid_node.identify, fluid_node.identify)) ;
    }
    do
    {
        // Calculates the heat flux
        fluid_node.htc = heat_corr.evaluate ;
        q_dot = fluid_node.htc*fluid_node.geometry.htA*(solid_node.T - fluid_node.T) ;

        // add heat flux to each neighbouring node
        solid_node.heat_flux = solid_node.heat_flux - q_dot ;
        fluid_node.flux.extern_heat += q_dot ;
    }

    // record the heat transfer and add to the total interface heat transfer
    void record_heat_transfer()
    {
        record_transfer(q_dot*fluid_node.n_channels) ;
    }
}

// generate a record function for heat flux over the interface
private void delegate(Real) generate_qdot_record(ThermalFace face)
{
    void record(Real q_dot)
    {
        face.add_heat(q_dot) ;
    }

    return &record ;
}

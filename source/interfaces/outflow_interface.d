module q1dcfd.interfaces.outflow_interface ;

import q1dcfd.config: Real ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.interfaces.transport_interface: TransportInterface ;
import q1dcfd.simulation: Simulation ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

final class OutflowInterface: TransportInterface
{
    /* outflow interface flux state. Very similar to FlowFlowInterface
    except it is used to describe the flux of an outflow boundary condition
    Hence the reconstruction is performed using two estimates of one sided 
    states and only left side flux is assigned

    rright is unused, hence outflow is used as an input placeholder
    */
    this(FluidNode interior_2, FluidNode interior_1, FluidNode outflow)
    {
        super(interior_2, interior_1, outflow, outflow) ;
        this.length = interior_1.geometry.length ;
        this.rec_length = 1.0/this.length ;

        this.reconstruct_right = Simulation.reconstruction_boundary_right ;
    }

    @fastmath override void update_flux()
    // Full mass, momentum and energy Flux calculation
    {
        update_reconstruction ; // perform reconstruction
        update_inviscid_flux(this) ; // perform inviscid flux calculation
        flux[2] += calculate_heat_flux ; // add internal conductive flux

        left.flux.right[] = flux[]*left.geometry.rA ;
        right.flux.left[] = left.flux.right[] ;
    }
}

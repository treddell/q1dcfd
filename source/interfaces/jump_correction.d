module q1dcfd.interfaces.jump_correction ;

import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;

import std.math: fabs ;

final class JumpCorrection: Interface
{
    /+
     + the flux calculations used for the interior interfaces either side
     + of a jump discontunuity will not preserve properties correctly using
     + the normal method. This correction maps the upstream flux to the downstream
     + minus a change in energy and momentum from external work extraction/
     + injection
     +/
    FluidNode upstream ;
    FluidNode downstream ;
    FluidNode upstream_jump_state ;
    FluidNode downstream_jump_state ;
    
    this(
        FluidNode upstream,
        FluidNode upstream_jump_state,
        FluidNode downstream_jump_state,
        FluidNode downstream)
    {
        this.upstream = upstream ;
        this.downstream = downstream ;
        this.downstream_jump_state = downstream_jump_state ;
        this.upstream_jump_state = upstream_jump_state ;
    }

    override void update_flux()
    {
        auto delta_H = upstream_jump_state.H - downstream_jump_state.H ;
        auto delta_F = upstream_jump_state.p*upstream.geometry.rA
            - downstream_jump_state.p*downstream.geometry.lA ;
        auto delta_v = fabs(upstream_jump_state.v) - fabs(downstream_jump_state.v) ;

        downstream.flux.left[0] = upstream.flux.right[0] ;
        downstream.flux.left[1] = upstream.flux.right[1]
            - delta_F - delta_v*upstream.flux.right[0] ;
        downstream.flux.left[2] = upstream.flux.right[2]
            - delta_H*upstream.flux.right[0] ;

        downstream_jump_state.flux.right[] = downstream.flux.left[] ;
        upstream_jump_state.flux.left[] = upstream.flux.right[] ;
    }
}

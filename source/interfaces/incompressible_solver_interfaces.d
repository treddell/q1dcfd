module q1dcfd.interfaces.incompressible_solver_interfaces;

import q1dcfd.control_volumes: Node, PisoSolver, IncompressibleNode,
    IncompressibleBoundary;
import q1dcfd.config: Real;
import q1dcfd.interfaces.root_interface: Interface;

version(LDC) { import ldc.attributes: fastmath ;} else { enum fastmath; }

final class SolverToChannel: Interface 
{
    PisoSolver solver;
    IncompressibleNode[] channel;

    this(PisoSolver solver, IncompressibleNode[] channel)
    {
        this.solver = solver;
        this.channel = channel;
    }

    @fastmath override void update_flux()
    {
        foreach(immutable i; 0..channel.length)
        {
            // Using substepping
            // Add 1 to account for boundary inlet cell in incompressible solver
            channel[i].p    = solver.p_sub[i+1];
            channel[i].T    = solver.T_sub[i+1];
            channel[i].rho  = solver.rho_sub[i+1];
            channel[i].mu   = solver.mu_sub[i+1];
            channel[i].e    = solver.e_sub[i+1];
            channel[i].k    = solver.k_sub[i+1];
            channel[i].htc  = solver.htc_sub[i+1];
            channel[i].mdot = solver.mdot_sub[i+1];
            // PISO uses a staggered velocity grid, v_scalar are the
            // interpolated values of velocity at the scalar nodes
            channel[i].v    = solver.v_scalar_sub[i+1];

            // qdot is per channel for incompressible nodes but total for solver
            solver.q_dot[i+1] = channel[i].qdot_extern * channel[i].n_channels;
        }
    }
}

final class IncompressibleInflowInterface: Interface
{
    PisoSolver solver;
    IncompressibleBoundary inflow;

    this(IncompressibleBoundary inflow, PisoSolver solver)
    {
        this.solver = solver;
        this.inflow = inflow;
    }

    @fastmath override void update_flux()
    {
        // Write values from inflow to solver
        solver.p[0]   = inflow.p;
        solver.T[0]   = inflow.T;
        solver.v[0]   = inflow.v;
        solver.rho[0] = inflow.rho;
        solver.mu[0]  = inflow.mu;
        solver.e[0]   = inflow.e;
        solver.k[0]   = inflow.k;
    }
}

final class IncompressibleOutflowInterface: Interface
{
    PisoSolver solver;
    IncompressibleBoundary outflow;

    this(IncompressibleBoundary outflow, PisoSolver solver)
    {
        this.solver  = solver;
        this.outflow = outflow;
    }

    @fastmath override void update_flux()
    {
        // Store values from solver in outflow
        outflow.p   = solver.p[$-1];
        outflow.T   = solver.T[$-1];
        outflow.v   = solver.v[$-1];
        outflow.rho = solver.rho[$-1];
        outflow.mu  = solver.mu[$-1];
        outflow.e   = solver.e[$-1];
        outflow.k   = solver.k[$-1];
        outflow.mdot= solver.mdot[$-1];
    }
}



module q1dcfd.interfaces.turbomachine_interface;

import q1dcfd.lconfig: ROOT_DIRECTORY;
import q1dcfd.config: Real, Transient;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.control_volumes.fluid_node: FluidNode;
import q1dcfd.control_volumes.point_states: InviscidPointState;
import q1dcfd.tables.extract_table_data: TableData;
import q1dcfd.utils.math: square;
import q1dcfd.simulation: Simulation;
import q1dcfd.maps.turbine: Turbine;
import q1dcfd.maps.compressor: Compressor;
import q1dcfd.stream: Stream;

version(high_prec) { import std.math: fabs; }
else               { import core.stdc.math: fabs; }

version(LDC) { import ldc.attributes: fastmath; }
else         { enum fastmath; }

/**
 * Special type of interface that captures the discontinuity in momentum and
 * energy that occurs over a turbomachine.
 * To do this, applies different momentum and energy fluxes on the left and
 * right sides of the interface.
 * See interface_turbomachine.d for more details on the procedure by which the
 * momentum and energy discontinuities are calculated.
 *
 * Flux calculation procedure is as follows:
 * - Set T_in = T_left and P_in = P_left
 * - Set P_out = P_right
 * - Calculate mass_flux = f(P_in/P_out)
 * - Calculate W_sh = g(T_in, P_in/P_out)
 * - Set e_flux_in = mass_flux * e_in, set e_flux_out = e_flux_in - W_sh
 * - Calculate v_in = mass_flux/rho_in, v_out = mass_flux/rho_out
 * - Calculate momentum fluxes as v_flux_in = mass_flux * v_in + P_in,
 *   v_flux_out = mass_flux * v_out + P_out
 * - Assign relevant values to FluidNodes left and right
 */
abstract class TurbomachineInterface : Interface
{
    FluidNode left;
    FluidNode right;

    Real length; // heat transfer length
    Real area;   // mean flow area

    // Transient speed; // Rotational speed, kg/s
    Real speed = 1.0;

    Real mdot;      // Mass flow
    Real delta_h;   // Enthalpy change
    Real w_sh;      // Shaft work
    Real mass_flux; // Mass flux
    Real v_flux_in, v_flux_out; // Momentum fluxes
    Real e_flux_in, e_flux_out; // Energy Fluxes

    this(FluidNode left, FluidNode right)
    {
        this.left  = left;
        this.right = right;

        // Either side of the turbine interface should always be just one channel
        assert(left.n_channels == 1);
        assert(right.n_channels == 1);
        // With the way this currently works, need to ensure that we have equal
        // flow area either side of the interface (eventually would be nice to
        // capture flow area changing)
        assert(left.geometry.rA * left.n_channels ==
               right.geometry.lA * right.n_channels);
        area = right.geometry.lA * right.n_channels;

        this.length = 0.5*(left.geometry.length + right.geometry.length);
    }

    /* Full mass, momentum and energy flux calculation */
    @fastmath void update_flux()
    {
        import std.typecons;
        import q1dcfd.utils.math: square;

        Real[3] flux_upstream, flux_downstream;

        // Upwind inlet state and downwind outlet pressure
        double rho_in = left.rho;
        double h_in   = left.h;
        double T_in   = left.T;
        double P_in   = left.p;
        double P_out  = right.p;

        double[string] result = solve_turbomachine(speed, T_in, P_in, P_out);
        mdot = result["mdot"];
        delta_h = result["delta_h"];
        double rho_out = result["rho_out"];

        // Calculate mass and momentum fluxes
        mass_flux = mdot / area;
        double v_in = mass_flux / rho_in;
        double v_out = mass_flux / rho_out;
        v_flux_in = mass_flux * v_in + P_in;
        v_flux_out = mass_flux * v_out + P_out;

        // Calculate shaft work
        w_sh = (delta_h + 0.5*v_out.square - 0.5*v_in.square) * mdot;

        // Calculate energy flux in and out
        e_flux_in = mass_flux * h_in + 0.5 * v_in.square;
        e_flux_out = e_flux_in + w_sh / area;

        // Assemble fluxes
        flux_upstream   = [mass_flux, v_flux_in, e_flux_in];
        flux_downstream = [mass_flux, v_flux_out, e_flux_out];

        left.flux.right[] = flux_upstream[] * area;
        right.flux.left[] = flux_downstream[] * area;
    }

    double[string] solve_turbomachine(
        double speed, double T_in, double P_in, double P_out);
}

final class TurbineInterface : TurbomachineInterface
{
    Turbine Turb;

    this(FluidNode left, FluidNode right, const string coolprop_fluid,
            const string map_name)
    {
        string map_dir = "/source/maps/" ~ map_name;
        Turb = new Turbine(ROOT_DIRECTORY ~ map_dir, coolprop_fluid);
        super(left, right);
    }

    override double[string] solve_turbomachine(
        double speed, double T_in, double P_in, double P_out)
    {
        return Turb.calc_from_er(speed, T_in, P_in, P_out);
    }
}

final class CompressorInterface : TurbomachineInterface
{
    Compressor Comp;

    this(FluidNode left, FluidNode right, Stream stream,
         Real initial_mdot, Real initial_P_out, string map_name)
    {
        string map_dir = "/source/maps/" ~ map_name;
        Comp = new Compressor(ROOT_DIRECTORY ~ map_dir,
                stream, initial_P_out);

        // Compressor needs an initial guess of mass flow rate since a root
        // finder is used in its calc_from_er method
        mdot = initial_mdot;

        super(left, right);
    }

    override double[string] solve_turbomachine(
        double speed, double T_in, double P_in, double P_out)
    {
        return Comp.calc_from_er(speed, T_in, P_in, P_out, mdot);
    }
}


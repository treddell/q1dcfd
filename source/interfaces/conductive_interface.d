module q1dcfd.interfaces.conductive_interface ;

import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.control_volumes: ThermalNode;
import q1dcfd.config: Real ;

import std.format ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

final class ConductiveInterface: Interface
{
    /++ 
    + Interface between two aligned thermal control volumes. Calculates
    + the conductive heat flux between each and assigns to each node
    +
    + Can connect nodes axially (left->right) or transversely 
    +    (upper (right) -> lower(left))
    +
    + Wall properties are considered to be constant, by extension heat transfer
    + coefficient is also constant. Fixed grid only
    +/
    ThermalNode right ; // right side solid node
    ThermalNode left ; // left side solid node
    const Real htc ; // heat transfer coefficient (W/K)

    this(ThermalNode right, ThermalNode left, const bool is_transverse = false)
    {
        Real area, length ;

        this.right = right ;
        this.left = left ;

        if(left.type == "TemperatureNode" && right.type != "TemperatureNode")
        {
            if(!is_transverse)
            {
                area = right.geometry.lA  ;
                length = right.geometry.length ;
            }
            else
            {
                area = right.geometry.bA ;
                length = right.geometry.transverse_length ;
            }
            this.htc = area*right.k/length ;
        }
        else if(left.type != "TemperatureNode" && right.type == "TemperatureNode")
        {
            if(!is_transverse)
            {
                area = left.geometry.rA ;
                length = left.geometry.length ;
            }
            else
            {
                area = left.geometry.tA ;
                length = left.geometry.transverse_length ;
            }
            this.htc = 0.5*area*left.k/length ;
        }
        else if(left.type != "TemperatureNode" && right.type != "TemperatureNode")
        {
            if(!is_transverse)
            {
                area = 0.5*(right.geometry.lA + left.geometry.rA) ;
                length = 0.5*(right.geometry.length + left.geometry.length) ;
            }
            else
            {
                area = 0.5*(right.geometry.bA + left.geometry.tA) ;
                length = 0.5*(right.geometry.transverse_length 
                    + left.geometry.transverse_length) ;
            }
            this.htc = 0.5*area*(right.k + left.k)/length ;
        }
        else
        {
            import q1dcfd.utils.errors: InvalidDefinition ;
            throw new InvalidDefinition(format!("Cannot create interface between"
                ~" boundary conditions %s and %s")(right.identify, left.identify)) ;
        }
    }

    @fastmath override void update_flux()
    // Calculate the solid-solid conductive flux
    {
        Real q_dot = htc*(right.T - left.T) ;
        right.heat_flux = right.heat_flux - q_dot ;
        left.heat_flux = left.heat_flux + q_dot ;
    }
}
module q1dcfd.interfaces ;

public import q1dcfd.interfaces.root_interface ;
public import q1dcfd.interfaces.transport_interface ;
public import q1dcfd.interfaces.inflow_interface ;
public import q1dcfd.interfaces.outflow_interface ;
public import q1dcfd.interfaces.conductive_interface ;
public import q1dcfd.interfaces.convective_interface ;
public import q1dcfd.interfaces.outflow_extrapolation ;
public import q1dcfd.interfaces.inflow_extrapolation ;
public import q1dcfd.interfaces.jump_correction ;
public import q1dcfd.interfaces.stagnation_interfaces;
public import q1dcfd.interfaces.turbomachine_interface ;
public import q1dcfd.interfaces.map_turbomachine_interfaces ;
public import q1dcfd.interfaces.incompressible_solver_interfaces ;
public import q1dcfd.interfaces.incompressible_convective_interface ;

module q1dcfd.interfaces.inflow_interface ;

import q1dcfd.config: Real ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.interfaces.transport_interface: TransportInterface ;
import q1dcfd.simulation: Simulation ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

final class InflowInterface: TransportInterface
{
    /* Inflow interface flux state. Very similar to FlowFlowInterface
    except it is used to describe the flux of an inflow boundary condition
    Hence the reconstruction is performed using two estimates of one sided 
    states and only right side flux is assigned

    lleft slot is unused, hence inflow is used as an input placeholder
    */
    this(FluidNode inflow, FluidNode interior_1, FluidNode interior_2)
    {
        super(inflow, inflow, interior_1, interior_2) ;
        this.length = interior_1.geometry.length ;
        this.rec_length = 1.0/this.length ;

        this.reconstruct_left = Simulation.reconstruction_boundary_left ; 
    }

    @fastmath override void update_flux()
    // Full mass, momentum and energy Flux calculation
    {
        update_reconstruction ; // perform reconstruction
        update_inviscid_flux(this) ; // perform inviscid flux calculation
        flux[2] += calculate_heat_flux ; // add internal conductive flux

        right.flux.left[] = flux[]*right.geometry.lA ;
        left.flux.right[] = right.flux.left[] ;
    }
}

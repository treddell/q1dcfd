module q1dcfd.interfaces.incompressible_convective_interface;

import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.control_volumes: Node, FluidNode, ThermalNode, IncompressibleNode;
import q1dcfd.config: Real;
import q1dcfd.thermal_face: ThermalFace;

import std.format;

version(LDC) { import ldc.attributes: fastmath;} else {enum fastmath;}

final class IncompressibleConvectiveInterface: Interface 
{
    private
    {
        ThermalNode solid_node;
        IncompressibleNode fluid_node;
        Real q_dot;
        ThermalFace face;
        void delegate(Real) record_transfer;
    }

    this(ThermalNode solid_node,
         IncompressibleNode fluid_node,
         ThermalFace face,
         const string name) 
    {
        this.solid_node = solid_node;
        this.fluid_node = fluid_node;
        this.face = face;
        this.record_transfer = generate_qdot_record(face);
        Real q_dot = 0; // Value req'd for first timestep, assume 0
    }

    @fastmath override void update_flux()
    {
        q_dot = fluid_node.htc * fluid_node.geometry.htA * (solid_node.T - fluid_node.T);
        fluid_node.qdot_extern = q_dot;
        solid_node.heat_flux = solid_node.heat_flux - q_dot;
    }

    // record the heat transfer and add to the total interface heat transfer
    void record_heat_transfer()
    {
        record_transfer(q_dot*fluid_node.n_channels);
    }
}

// Generate a record function for heat flux over the interface
private void delegate(Real) generate_qdot_record(ThermalFace face)
{
    void record(Real q_dot)
    {
        face.add_heat(q_dot);
    }

    return &record;
}


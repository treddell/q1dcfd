module q1dcfd.interfaces.map_turbomachine_interfaces;

import q1dcfd.control_volumes: FluidNode, MapTurbomachineNode;
import q1dcfd.config: Real;
import q1dcfd.interfaces.root_interface: Interface;

version(LDC) { import ldc.attributes: fastmath; } else { enum fastmath; }

/*
 * Used to
 *  - communicate turbomachine inlet conditions (p_in, T_in) from plenum to
 *    map-based turbomachine node and
 *  - communicate turbomachine inlet flux (calculated with map model) to plenum.
 *
 * inlet_plenum_node is just the inlet plenum node closest to the turbomachine
 * (i.e.  it is node inlet_plenum[1], where inlet_plenum has nodes 0 and 1).
 */
final class MapTurbomachineInletInterface: Interface 
{
    FluidNode inlet_plenum_node;
    MapTurbomachineNode map_turbomachine_node;

    this(FluidNode inlet_plenum_node, MapTurbomachineNode map_turbomachine_node)
    {
        this.inlet_plenum_node = inlet_plenum_node;
        this.map_turbomachine_node = map_turbomachine_node;
    }

    @fastmath override void update_flux()
    {
        // Give the turbomachine it's inlet conditions from the plenum
        map_turbomachine_node.rho_in = inlet_plenum_node.rho;
        map_turbomachine_node.h_in   = inlet_plenum_node.h;
        map_turbomachine_node.T_in   = inlet_plenum_node.T;
        map_turbomachine_node.p_in   = inlet_plenum_node.p;

        // Read the flux values calculated by the turbomachine model.
        // flux_integral means the raw flux values have been multiplied by area
        // (as is required).
        inlet_plenum_node.flux.right[] = map_turbomachine_node.flux_integral_upstream[];
    }
}

/*
 * As above but for outlet.
 * Here we are using node outlet_plenum[0].
 */
final class MapTurbomachineOutletInterface: Interface 
{
    FluidNode outlet_plenum_node;
    MapTurbomachineNode map_turbomachine_node;

    this(MapTurbomachineNode map_turbomachine_node, FluidNode outlet_plenum_node)
    {
        this.outlet_plenum_node = outlet_plenum_node;
        this.map_turbomachine_node = map_turbomachine_node;
    }

    @fastmath override void update_flux()
    {
        // Give the turbomachine it's outlet conditions from the plenum
        map_turbomachine_node.p_out = outlet_plenum_node.p;

        // Read the flux values calculated by the turbomachine model.
        // flux_integral means the raw flux values have been multiplied by area
        // (as is required).
        outlet_plenum_node.flux.left[] = map_turbomachine_node.flux_integral_downstream[];
    }
}


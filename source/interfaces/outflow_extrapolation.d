module q1dcfd.interfaces.outflow_extrapolation ;

import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.control_volumes.outflow_node: OutflowNode ;
import q1dcfd.config: Real ;
import q1dcfd.interfaces.root_interface: Interface ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

@fastmath
void third_order_backward(FluidNode[] interior, FluidNode outflow)
/** Third order 4 point backward extrapolation estimate **/
{
    Real dx = interior[$-1].geometry.length ;

    outflow.drhodx = (
        -2.0*interior[$-3].rho + 9.0*interior[$-2].rho 
        - 18.0*interior[$-1].rho + 11.0*outflow.rho)/(6.0*dx) ;

    outflow.dvdx = (
        -2.0*interior[$-3].v + 9.0*interior[$-2].v 
        - 18.0*interior[$-1].v + 11.0*outflow.v)/(6.0*dx) ;

    outflow.dpdx = (
        -2.0*interior[$-3].p + 9.0*interior[$-2].p 
        - 18.0*interior[$-1].p + 11.0*outflow.p)/(6.0*dx) ;
}

@fastmath
void fourth_order_backward(FluidNode[] interior, FluidNode outflow)
/** Fourth order 5 point backward extrapolation estimate **/
{
     Real dx = interior[$-1].geometry.length ;

     outflow.drhodx = (
        3.0*interior[$-4].rho - 16*interior[$-3].rho + 36.0*interior[$-2].rho 
        - 48.0*interior[$-1].rho + 25.0*outflow.rho)/(12.0*dx) ;

    outflow.dvdx = (
        3.0*interior[$-4].v - 16*interior[$-3].v + 36.0*interior[$-2].v
        - 48.0*interior[$-1].v + 25.0*outflow.v)/(12.0*dx) ;

    outflow.dpdx = (
        3.0*interior[$-4].p - 16*interior[$-3].p + 36.0*interior[$-2].p
        - 48.0*interior[$-1].p + 25.0*outflow.p)/(12.0*dx) ;
}

@fastmath
void fifth_order_backward(FluidNode[] interior, FluidNode outflow)
/** Fifth order 6 point backward extrapolation estimate **/
{
    Real dx = interior[$-1].geometry.length ;

    outflow.drhodx = (
        - 12.0*interior[$-5].rho + 75.0*interior[$-4].rho 
        - 200*interior[$-3].rho + 300.0*interior[$-2].rho 
        - 300.0*interior[$-1].rho + 137.0*outflow.rho)
        /(60.0*dx) ;

    outflow.dvdx = (
        - 12.0*interior[$-5].v + 75.0*interior[$-4].v 
        - 200*interior[$-3].v + 300.0*interior[$-2].v 
        - 300.0*interior[$-1].v + 137.0*outflow.v)
        /(60.0*dx) ;

    outflow.dpdx = (
        - 12.0*interior[$-5].p + 75.0*interior[$-4].p 
        - 200*interior[$-3].p + 300.0*interior[$-2].p 
        - 300.0*interior[$-1].p + 137.0*outflow.p)
        /(60.0*dx) ;
}

@fastmath
void sixth_order_backward(FluidNode[] interior, FluidNode outflow)
/** Sixth order 7 point backward extrapolation estimate **/
{
    Real dx = interior[$-1].geometry.length ;

    outflow.drhodx = (
        10*interior[$-6].rho - 72.0*interior[$-5].rho + 225.0*interior[$-4].rho 
        - 400*interior[$-3].rho + 450.0*interior[$-2].rho 
        - 360.0*interior[$-1].rho + 147.0*outflow.rho)
        /(60.0*dx) ;

    outflow.dvdx = (
        10*interior[$-6].v - 72.0*interior[$-5].v + 225.0*interior[$-4].v 
        - 400*interior[$-3].v + 450.0*interior[$-2].v 
        - 360.0*interior[$-1].v + 147.0*outflow.v)
        /(60.0*dx) ;

    outflow.dpdx = (
        10*interior[$-6].p - 72.0*interior[$-5].p + 225.0*interior[$-4].p 
        - 400*interior[$-3].p + 450.0*interior[$-2].p 
        - 360.0*interior[$-1].p + 147.0*outflow.p)
        /(60.0*dx) ;
}

@fastmath
void seventh_order_backward(FluidNode[] interior, FluidNode outflow)
/** Seventh order 8 point backward extrapolation estimate **/
{
    Real dx = interior[$-1].geometry.length ;

    outflow.drhodx = (
        -60*interior[$-7].rho + 490*interior[$-6].rho - 1764.0*interior[$-5].rho 
        + 3675.0*interior[$-4].rho - 4900*interior[$-3].rho 
        + 4410.0*interior[$-2].rho - 2940.0*interior[$-1].rho 
        + 1089.0*outflow.rho)/(420.0*dx) ;

    outflow.dvdx = (
        -60*interior[$-7].v + 490*interior[$-6].v - 1764.0*interior[$-5].v 
        + 3675.0*interior[$-4].v - 4900*interior[$-3].v
        + 4410.0*interior[$-2].v - 2940.0*interior[$-1].v 
        + 1089.0*outflow.v)/(420.0*dx) ;

    outflow.dpdx = (
        -60*interior[$-7].p + 490*interior[$-6].p - 1764.0*interior[$-5].p 
        + 3675.0*interior[$-4].p - 4900*interior[$-3].p 
        + 4410.0*interior[$-2].p - 2940.0*interior[$-1].p 
        + 1089.0*outflow.p)/(420.0*dx) ;
}

final class OutflowExtrapolation: Interface 
{
    /** Extrapolates outward going characteristics from the interior domain to the
    boundary. Used for far-field pressure outflow boundary conditions

    update_flux method is actually calculating one sided gradients and assigning
    to the outflow node. Name is re-used for polymorphic purposes

    TODO: make extrapolation configurable from definition
    **/
    OutflowNode outflow ;
    FluidNode[] interior ;
    void function(FluidNode[], FluidNode) extrapolation ;

    this(FluidNode[] interior, OutflowNode outflow)
    {
        this.outflow = outflow ;
        this.interior = interior ;
        this.extrapolation = &third_order_backward ;
    }

    @fastmath override void update_flux()
    /* extrapolate dv/dx, dp/dx and d(rho)/dx from the interior control volumes. 
    Assign to outflow node.
    */
    {
        extrapolation(interior, outflow) ;
    }
}
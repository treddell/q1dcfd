module q1dcfd.interfaces.inflow_extrapolation ;

import q1dcfd.control_volumes: FluidNode, InflowDV ;
import q1dcfd.config: Real ;
import q1dcfd.interfaces.root_interface: Interface ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

@fastmath
void third_order_forward(FluidNode inflow, FluidNode[] interior)
/** Third order 4 point forward extrapolation estimate **/
{
    Real dx = interior[0].geometry.length ;

    inflow.dvdx = (
        - 11.0*inflow.v + 18.0*interior[0].v - 9.0*interior[1].v 
        + 2.0*interior[2].v)/(6.0*dx) ;

    inflow.dpdx = (
        - 11.0*inflow.p + 18.0*interior[0].p - 9.0*interior[1].p 
        + 2.0*interior[2].p)/(6.0*dx) ;
}

@fastmath
void fourth_order_forward(FluidNode inflow, FluidNode[] interior)
/** Fourth order 5 point forward extrapolation estimate **/
{
    Real dx = interior[0].geometry.length ;

    inflow.dvdx = (
        - 25.0*inflow.v + 48.0*interior[0].v - 36.0*interior[1].v 
        + 16.0*interior[2].v - 3.0*interior[3].v)/(12.0*dx) ;

    inflow.dpdx = (
        - 25.0*inflow.p + 48.0*interior[0].p - 36.0*interior[1].p 
        + 16.0*interior[2].p - 3.0*interior[3].p)/(12.0*dx) ;
}

@fastmath
void fifth_order_forward(FluidNode inflow, FluidNode[] interior)
/** Fifth order 6 point forward extrapolation estimate
**/
{
    Real dx = interior[0].geometry.length ;

    inflow.dvdx = (
        - 137.0*inflow.v + 300.0*interior[0].v - 300.0*interior[1].v 
        + 200.0*interior[2].v - 75.0*interior[3].v + 12.0*interior[4].v)
        /(60.0*dx) ;

    inflow.dpdx = (
        - 137.0*inflow.p + 300.0*interior[0].p - 300.0*interior[1].p 
        + 200.0*interior[2].p - 75.0*interior[3].p + 12.0*interior[4].p)
        /(60.0*dx) ;
}

@fastmath
void sixth_order_forward(FluidNode inflow, FluidNode[] interior)
/** Sixth order 7 point forward extrapolation estimate
**/
{
    Real dx = interior[0].geometry.length ;

    inflow.dvdx = (
        - 147.0*inflow.v + 360.0*interior[0].v - 450.0*interior[1].v 
        + 400.0*interior[2].v - 225.0*interior[3].v + 72.0*interior[4].v
        - 10.0*interior[5].v)/(60.0*dx) ;

    inflow.dpdx = (
        - 147.0*inflow.p + 360.0*interior[0].p - 450.0*interior[1].p 
        + 400.0*interior[2].p - 225.0*interior[3].p + 72.0*interior[4].p
        - 10.0*interior[5].p)/(60.0*dx) ;
}

@fastmath
void seventh_order_forward(FluidNode inflow, FluidNode[] interior)
/** Seventh order 8 point forward extrapolation estimate
**/
{
    Real dx = interior[0].geometry.length ;

    inflow.dvdx = (
        - 1089.0*inflow.v + 2940.0*interior[0].v - 4410.0*interior[1].v 
        + 4900.0*interior[2].v - 3675.0*interior[3].v + 1764.0*interior[4].v
        - 490.0*interior[5].v + 60.0*interior[6].v)/(420.0*dx) ;

    inflow.dpdx = (
        - 1089.0*inflow.p + 2940.0*interior[0].p - 4410.0*interior[1].p 
        + 4900.0*interior[2].p - 3675.0*interior[3].p + 1764.0*interior[4].p
        - 490.0*interior[5].p + 60.0*interior[6].p)/(420.0*dx) ;
}

final class InflowExtrapolation: Interface 
{
    /* Extrapolates outward going characteristics from the interior domain to 
    the boundary. Used for density-velocity inflow boundary conditions

    update_flux method is actually calculating one sided gradients and assigning
    to the inflow node. Name is re-used for polymorphic purposes

    TODO: make extrapolation configurable from definition
    */
    FluidNode inflow ;
    FluidNode[] interior ;
    void function(FluidNode, FluidNode[]) extrapolation ;

    this(FluidNode inflow, FluidNode[] interior)
    {
        this.inflow = inflow ;
        this.interior = interior ;
        this.extrapolation = &third_order_forward ;
    }

    @fastmath override void update_flux()
    /* extrapolate dv/dx and dp/dx from the interior control volumes. 
    Assign to inflow node.
    */
    {
        extrapolation(inflow, interior) ;
    }
}
module q1dcfd.interfaces.transport_interface ;

import q1dcfd.config: Real ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.control_volumes.fluid_node: FluidNode ;
import q1dcfd.control_volumes.point_states: InviscidPointState ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.utils.math: square ;
import q1dcfd.simulation: Simulation ;

import std.stdio ;
import std.format ;
version(high_prec) {import std.math: fabs ;}
else {import core.stdc.math: fabs ;}

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

alias FluxCalculator = pure nothrow void function(TransportInterface) ;

/**
 * Defines a flux interface between two interior fluid cells, the flux is 
 * calculated via defining left and right interface point states with 
 * properties determined using a MUSCL interpolation. An AUSMDV flux calculator 
 * is then used to calculate the interface flux, which is mapped to each cell
 *
 * lleft   left    right   rright
 * ---------------------------------
 * .       .       |       .       .    o = interface_l
 * .       .      o|x      .       .    x = interface_r
 * .       .       |       .       .
 * ---------------------------------
 * |-------|
 * length
 *
 * Flux across | interface is computed based off a reconstruction 
 * between all four cells
 */

class TransportInterface: Interface
{
    FluidNode lleft ;
    FluidNode left ;
    FluidNode rright ;
    FluidNode right ;
    InviscidPointState interface_l ;
    InviscidPointState interface_r ;
    
    Real length ; // heat transfer length
    Real rec_length ; //reciprocal of heat transfer length
    Real area ; // Total area at interface
    Real area_l ; // Area per channel of left interface
    Real area_r ; // Area per channel of right cell interface
    Real[3] flux ; // mass, momentum and energy flux

    FluxCalculator update_inviscid_flux ; 

    // left/right state reconstruction calculators
    import q1dcfd.flux_calculators.reconstructions: ReconstructionScheme ;
    ReconstructionScheme reconstruct_left ;
    ReconstructionScheme reconstruct_right ;

    this(FluidNode lleft, FluidNode left, FluidNode right, FluidNode rright)
    {
        this.lleft = lleft ;
        this.left = left ;
        this.right = right ;
        this.rright = rright ;

        // Create the left/right interface point states
        this.interface_l = new InviscidPointState(left.eos,
            "Reconstructed Point State (left) on Interface (%s)->(%s)"
            .format(left.identify, right.identify)) ;
        this.interface_r = new InviscidPointState(right.eos,
            "Reconstructed Point State (right) on Interface (%s)->(%s)"
            .format(left.identify, right.identify)) ;

        this.length = 0.5*(left.geometry.length + right.geometry.length) ;
        this.rec_length = 1.0/this.length ;

        this.area = 0.5*(left.geometry.rA*left.n_channels
                         + right.geometry.lA*right.n_channels);
        this.area_l = this.area/left.n_channels ;
        this.area_r = this.area/right.n_channels ;
        
        this.update_inviscid_flux = Simulation.inviscid_flux_calculator ;
        this.reconstruct_left = Simulation.reconstruction_scheme_left ;
        this.reconstruct_right = Simulation.reconstruction_scheme_right ;
    }

    /**
     * Updates the left/right point states using the designated reconstruction
     * schemes
     */
    @fastmath void update_reconstruction()
    {
        Real[3] x_left = reconstruct_left(lleft, left, right);
        Real[3] x_right = reconstruct_right(left, right, rright);

        interface_l.update(x_left) ;
        interface_r.update(x_right) ;
    }

    @fastmath void update_flux() 
    /** Full mass, momentum and energy Flux calculation **/
    {
        update_reconstruction ; // perform reconstruction
        update_inviscid_flux(this) ; // perform inviscid flux calculation
        flux[2] += calculate_heat_flux ; // add internal conductive flux

        // Assign flux to each node
        left.flux.right[] = flux[]*area_l ;
        right.flux.left[] = flux[]*area_r ;
    }

    @fastmath Real calculate_heat_flux() 
    /* Calculate the conductive heat flux at the interface to second order */
    {
        Real k = 0.5*(left.k + right.k) ; // average conductivity
        return -k*(right.T - left.T)*rec_length ;
    }
}

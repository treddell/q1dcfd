module q1dcfd.interfaces.stagnation_interfaces;

import q1dcfd.control_volumes: FluidNode, InflowFromStagnation,
        OutflowToStagnation;
import q1dcfd.config: Real;
import q1dcfd.interfaces.root_interface: Interface;

version(LDC) { import ldc.attributes: fastmath; } else { enum fastmath; }

/*
 * Provides a reference to the interior flow state for the boundary
 * condition object.
 */
final class InteriorFlowConditions: Interface 
{
    FluidNode inflow;
    FluidNode[] interior;
    InflowFromStagnation inflow_from_stagnation; // Can I just use this one?

    this(FluidNode inflow, FluidNode[] interior)
    {
        this.inflow = inflow;
        this.interior = interior;
        this.inflow_from_stagnation = cast(InflowFromStagnation) inflow;
    }

    // Write first-cell values to boundary condition object
    @fastmath override void update_flux()
    {
        inflow_from_stagnation.p1   = interior[0].p;
        inflow_from_stagnation.rho1 = interior[0].rho;
        inflow_from_stagnation.v1   = interior[0].v;

        // import std.stdio;
        // writeln("v1 = ", interior[0].v);
        // writeln("v-1 = ", interior[$-1].v);
    }
}

/*
 * Provides a reference to the interior flow state for the boundary
 * condition object.
 */
final class OutflowInteriorFlowConditions: Interface
{
    FluidNode outflow;
    FluidNode[] interior;
    OutflowToStagnation outflow_to_stagnation;

    this(FluidNode outflow, FluidNode[] interior)
    {
        this.outflow = outflow;
        this.interior = interior;
        this.outflow_to_stagnation = cast(OutflowToStagnation) outflow;
    }

    // Write first-cell values to boundary condition object
    @fastmath override void update_flux()
    {
        outflow_to_stagnation.rho1  = interior[$-1].rho;
        outflow_to_stagnation.v1    = interior[$-1].v;
        outflow_to_stagnation.h1    = interior[$-1].h;
    }
}


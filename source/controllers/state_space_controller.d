module q1dcfd.controllers.state_space_controller;

import q1dcfd.utils.metaprogramming: from, PopulateStateMap, PopulateStateFields;

/*
 * Wishlist:
 * - I could get away with just using the observer_map, rather than
 *   observer_array.
 * - There isn't any conclusive input checking outside debug mode (could handle
 *   using string AAs).
 * - All object types (walls, streams, flow sources, etc) should have outputs
 *   and constraints.
 */

final class StateSpaceController : from!"q1dcfd.controllers".AbstractControlNode
{
    import q1dcfd.types.geometry: CrossSection;
    import q1dcfd.config : Transient, Real, Address;
    import q1dcfd.state_space_control: ObsDef, IObserver;
    import q1dcfd.state_space_control.stream: Stream, StreamDef, build_stream;
    import q1dcfd.state_space_control.wall: Wall, WallDef, build_wall;
    import q1dcfd.state_space_control.flow_source: FlowSource, FlowSourceDef,
            build_flow_source;
    import q1dcfd.state_space_control: Lti;
    import q1dcfd.state_space_control.mpc: Mpc, MpcSolver;

    // TODO: These should depend on jobfile.
    enum STATE_FIELDS = [
        "W_net_ref",
        "T_in_T_ref",
        "N_min",
        "N_max",
        "T_turb_in_max",
        "dT_motor_max",
        "dmdot_pump_max",
    ];
    mixin(PopulateStateFields!(STATE_FIELDS));
    mixin(PopulateStateMap!(STATE_FIELDS));

    CrossSection[string] geometries;
    MpcSolver mpc_solver;
    Mpc mpc;
    Lti lti;
    uint n_states, n_inputs, n_outputs, n_cntrl_outs;
    uint n_constraints; // number of internal state constraints, not user spec'd ones
    string[string] mpc_defs;
    const Transient[string] target_outputs;
    immutable Real controller_dt; // controller timestep
    Real t_prev = 0.0;

    ObsDef[] observer_defs;
    IObserver[] observer_array;
    IObserver[string] observer_map;
    StreamDef[] stream_defs;
    Stream[] stream_array;
    Stream[string] stream_map;
    WallDef[] wall_defs;
    Wall[] wall_array;
    FlowSourceDef[] flow_source_defs;
    FlowSource[] flow_source_array;
    FlowSource[string] flow_source_map;

    this(const Address address,
         const uint n_controls,
         const uint n_sensors, // incorrectly named n_inputs in main code
         const Real[string] params,
         const bool[string] flags,
         const Transient[string] target_outputs)
    {
        super(address, n_controls, n_sensors);

        import std.conv: to;
        controller_dt = params["controller_dt"].to!Real;
        this.target_outputs = target_outputs;
    }

    override @property string type() { return "StateSpaceController"; }

    /*
     * Continuously update all the observers and update the controller at the
     * specified update rate.
     */
    override void update(const ref Real[] state, const Real t)
    {
        foreach(stream; stream_array) stream.update(t, input_ports);
        foreach(observer; observer_array) observer.update(t, input_ports);

        immutable Real t_start = 10.0; // time to init controller, TODO: job input
        Real dt = t - t_prev;
        if(dt >= controller_dt && t >= t_start) // Time for an MPC update
        {
            update_zref(lti, t);
            lti.update_ukm1();
            foreach(observer; observer_array) observer.control_update(lti, mpc);
            foreach(stream; stream_array) stream.control_update(lti, mpc);
            foreach(wall; wall_array) wall.update(lti);
            foreach(flow_source; flow_source_array) flow_source.update(lti, mpc);
            // foreach(observer; observer_array) observer.display_state_estimate();
            lti.c2d;
            mpc_solver.calculate_mpc_update(mpc, lti);
            foreach(observer; observer_array) observer.update_actuators(lti, _output_ports);
            t_prev = t;
            // lti.print_lti_matrices();

            //------------------------------------------------------------------
            // Save some stuff (TODO: hacky, magic numbers)
            import q1dcfd.state_space_control.flow_source: TurbomachineryFlow;
            auto tfs = cast(TurbomachineryFlow) flow_source_map["turbomachinery_fs"];
            N_min = tfs.N_min_0;
            N_max = tfs.N_max;
            W_net_ref = lti.ZRef[8, 0]; // save output targets
            T_in_T_ref = lti.ZRef[6, 0];
        }
    }

    void initialise_ss_controller(
            CrossSection[string] geometries,
            string jobfile_name)
    {
        this.geometries = geometries;
        // Fill out observer, stream, wall, and flow source defs
        exec_jobfile(jobfile_name);
        build_observers();
        build_streams();
        foreach(def; wall_defs) wall_array ~= build_wall(def, observer_map);
        build_flow_sources();

        // Check that the number of targets in the qdef file matches the number
        // of controlled outputs in the mpc jobfile
        import std.format;
        if(target_outputs.length != n_cntrl_outs)
            throw new Exception(format!(
                    "ERROR: Number of reference trajectories "
                    ~ "specified in qdef file (%d) does not match number of outputs "
                    ~ "specified in mpc jobfile (%d).")
                    (target_outputs.length, n_cntrl_outs));

        mpc = Mpc(mpc_defs, controller_dt, n_states, n_inputs, n_cntrl_outs,
                n_constraints);
        lti = new Lti(controller_dt, n_states, n_inputs, n_outputs, n_cntrl_outs);
        mpc_solver = new MpcSolver(mpc);
    }

    /*
     * At this point, the simulation model has (main code) has state values, so we
     * can start to initialize the control system.
     */
    void initialise_control_system()
    {
        foreach(observer; observer_array)
            observer.initialise_state_estimate(input_ports);
        foreach(observer; observer_array) observer.initialise_u(lti);
        // Now component observers have state values, init flow sources and streams
        foreach(flow_source; flow_source_array) flow_source.initialise();
        foreach(stream; stream_array) stream.initialise();
    }


protected:
    void build_observers()
    {
        import q1dcfd.state_space_control: build_observer;
        import std.conv: to;

        uint index_start = 0;   // state vector index
        uint u_index_start = 0; // control vector index
        uint y_index_start = 0; // output vector index
        uint z_index_start = 0; // controlled output vector index
        uint c_index_start = 0; // internal constraint vector index
        observer_array.length = observer_defs.length;
        foreach(i, def; observer_defs)
        {
            auto observer = build_observer(def, geometries);
            observer_array[i] = observer;
            observer.populate_observer_map(observer_map);
            index_start = observer.populate_state_indices(index_start);
            u_index_start = observer.populate_input_indices(u_index_start);
            y_index_start = observer.populate_output_indices(y_index_start);
            z_index_start = observer.populate_cntrl_out_indices(z_index_start);
            c_index_start = observer.populate_constraint_indices(c_index_start);
        }
        n_states      = index_start;
        n_inputs      = u_index_start;
        n_outputs     = y_index_start;
        n_cntrl_outs  = z_index_start;
        n_constraints = c_index_start;

        // Display component control state indices
        import std.stdio;
        writeln("\nOBSERVER MAP");
        foreach(key; observer_map.byKey())
            writeln(key, " istart : ", observer_map[key].index_start);
        writeln();

        if(n_cntrl_outs == 0)
        {
            throw new Exception("ERROR: No controlled outputs defined. Control "
                ~ "problem is ill-posed.");
        }
    }

    void build_streams()
    {
        // Extra controlled outputs can be included in Stream objects
        uint z_index_start = n_cntrl_outs;
        uint c_index_start = n_constraints;
        foreach(def; stream_defs)
        {
            auto stream = build_stream(def, observer_map);
            z_index_start = stream.populate_cntrl_out_indices(z_index_start);
            c_index_start = stream.populate_constraint_indices(c_index_start);
            stream_array ~= stream;
            stream_map[stream.stream_name] = stream;
        }
        n_cntrl_outs = z_index_start;
        n_constraints = c_index_start;
    }

    void build_flow_sources()
    {
        // Extra controlled outputs and constraints can be included in
        // FlowSource objects.
        uint z_index_start = n_cntrl_outs;
        uint c_index_start = n_constraints;
        foreach(def; flow_source_defs)
        {
            auto flow_source = build_flow_source(def, observer_map, stream_map);
            z_index_start = flow_source.populate_cntrl_out_indices(z_index_start);
            c_index_start = flow_source.populate_constraint_indices(c_index_start);
            flow_source_array ~= flow_source;
            flow_source_map[flow_source.name] = flow_source;
        }
        n_cntrl_outs = z_index_start;
        n_constraints = c_index_start;
    }

    void exec_jobfile(string jobfile_name)
    {
        switch(jobfile_name)
        {
            mixin(load_job!("job_recuperator"));
            mixin(load_job!("mpc_pche_design_point"));
            // HT Oil cases
            mixin(load_job!("ht_oil_mpc_comp_hx_turb"));
            mixin(load_job!("ht_oil_mpc_comp_hx_turb_temp_tracking"));
            // Old job files, not sure they are valid anymore
            // mixin(load_job!("mpc_comp_hx_turb"));
            // mixin(load_job!("mpc_comp_hx_turb_thesis_test"));
            // mixin(load_job!("mpc_comp_hx_turb_thesis_test_speed"));
            // mixin(load_job!("mpc_comp_hx_turb_thesis"));
            // mixin(load_job!("mpc_comp_hx_turb_high_temp"));
            // // Molten salt cases
            // mixin(load_job!("mpc_comp_heater_turb_salt"));
            // mixin(load_job!("mpc_comp_heater_turb_salt_test"));
            default:
                import q1dcfd.utils.errors: InvalidDefinition;
                import std.format;
                throw new InvalidDefinition(
                    "Unrecognized jobfile %s for state-space controller."
                    .format(jobfile_name));
        }
        // TODO: Should do some input checking on these job files.
    }

    void update_zref(ref Lti lti, const Real t)
    {
        import std.conv: to;
        import std.array: split;
        foreach(key; target_outputs.byKey)
            lti.ZRef[key.split("_")[$-1].to!ulong, 0] = target_outputs[key](t);
    }
}

template load_job(string job_name)
{
string load_job()
{
    string str_out = 
        `case "` ~ job_name ~ `":` ~
        `    import q1dcfd.state_space_control.jobs.` ~ job_name ~ `;` ~
        `    observer_defs    = make_defs;
             stream_defs      = make_streams;
             wall_defs        = make_walls;
             flow_source_defs = make_flow_sources;
             mpc_defs         = make_mpc_defs;
             break;
        `;
    return str_out;
}
}


// // Code to test global indexing
// import std.stdio;
// import std.conv: to;
// import q1dcfd.state_space_control: HxObserver;
// writeln("recup start  : ", observer_map["recuperator"].index_start);
// writeln("recup end    : ", observer_map["recuperator"].index_end);
// writeln("hot_c start  : ", observer_map["recuperator"].to!HxObserver.HotChannel.index_start);
// writeln("hot_c end    : ", observer_map["recuperator"].to!HxObserver.HotChannel.index_end);
// writeln("cold_c start : ", observer_map["recuperator"].to!HxObserver.ColdChannel.index_start);
// writeln("cold_c end   : ", observer_map["recuperator"].to!HxObserver.ColdChannel.index_end);
// writeln("wall_c start : ", observer_map["recuperator"].to!HxObserver.Wall.index_start);
// writeln("wall_c end   : ", observer_map["recuperator"].to!HxObserver.Wall.index_end);

// Maybe some useful documenation
/*
 * Functionality for Mpc class:
 * - Store controller settings
 * - Calculate MPC update using optimizer
 * - Store complete system LTI model (or at least have access to it)
 * - Handle system-wide state estimation
 * - Send update signals to actuators
 * - Compute LTI model from state vector and component models.
 */

/*
 * Constant stream pressure approach:
 * - LTI models are calculated using internal energies of adjacent cells.
 * - For each stream, there is some boundary state.
 * - For the incompressible streams that interface with the outside of the loop,
 *   then this boundary state is simply the boundary condition of that stream;
 *   it makes sense to treat these as state variables that just update as
 *   v_k+1 = v_k.
 * - For the actual sCO2 loop, these boundary variables are calculated from the
 *   turbomachinery maps (can get a linearized update version of this).
 * - Pressures are treated similarly (though the update equation is different).
 */

module q1dcfd.controllers.pid_controller ;

import q1dcfd.controllers.abstract_controller: AbstractControlNode ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.utils.metaprogramming: PopulateStateMap, PopulateStateFields ;

import std.algorithm.comparison: min, max ;

/*
 * Retrieves the bias from the definition if assigned, else produces
 * a default bias function
 */
private Transient set_pid_bias(AbstractControlNode controller,
                       const bool[string] flags,
                       const Transient[string] target_outputs)
{
    // set the bias to be the previous controller value, this effectively
    // makes the output an absolute parameter rather than a delta change
    if("delta_bias" in flags && flags["delta_bias"])
    {
        return (const Real t) {return controller.output_ports[0] ;} ;
    }
    // no bias
    else
    {
        return target_outputs["bias"] ;
    }
}

/**
 * A standard proportional, integral, derivative controller
 * measures the value of some input state (x) relative to a target value
 * (xt). Sets a change in output value to some port of:
 *
 * delta(output) = k_p(x - xt) + k_i int^t_0 (x - xt) dt + k_d d(x - xt)/dt
 *
 * Unless this causes the output to exceed some specified (min, max) bounds
 * in which case it is truncated to those values
 */
final class PIDController: AbstractControlNode
{
    enum uint order() {return 0 ;}

    // generate runtime introspective state property getter/setters
    enum STATE_FIELDS = [
        "error", /*Controller Error */
        "integral_error", /* integral (e(t)) dt from 0->current time */
        "derivative_error" /* d(e(t))/dt */
    ] ;

    mixin(PopulateStateFields!(STATE_FIELDS)) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    Real last_update_time = 0.0 ; // last controller update
    Real update_period ; // controller update period

    // Control parameters
    const
    {
        Real k_p ; // proportional gain
        Real k_i ; // integral gain
        Real k_d ; // derivative gain
        Real max_output ; // maximum controller output
        Real min_output ; // minimum controller output
        Transient target_output ; // target output variable
        Transient bias ; // the controller bias
    }

    override @property string type() {return "PIDController" ;}

    this(const Address address,
         const uint n_controls, 
         const uint n_inputs, 
         const Real[string] params,
         const bool[string] flags,
         const Transient[string] target_outputs)
    {
        super(address, n_controls, n_inputs);

        // controller specific initialisation
        this.target_output = target_outputs["output"] ;
        this.k_p = params["k_p"] ;
        this.k_i = params["k_i"] ;
        this.k_d = params["k_d"] ;
        this.max_output = params["max"] ;
        this.min_output = params["min"] ;

        this.error = 0.0 ;
        this.integral_error = 0.0 ;
        this.update_period = params["period"] ;

        this.bias = this.set_pid_bias(flags, target_outputs) ;
       
        // schedule an initial update time
        this.critical_dt = this.update_period ;
    }

    override void update(const ref Real[] state, const Real t)
    {
        // check the time elapsed since the last update
        auto controller_dt = t - last_update_time ;

        // time elapsed is less than controller period, schedule update and continue
        if(controller_dt < update_period)
        {
            critical_dt = update_period - controller_dt ;
            return ;
        }

        // time for an update
        auto prev_error = error ;
        error = target_output(t) - input_ports[0]() ;
        integral_error = integral_error + (prev_error + error)*controller_dt*0.5;
        derivative_error = (error - prev_error)/controller_dt ;

        // controller unconstrained output
        auto target = bias(t) + k_p*error + k_i*integral_error + k_d*derivative_error ;

        // store output for later use
        _output_ports[0] = min(max(target, min_output), max_output) ;

        // mark update complete, set new schedule
        last_update_time = t ;
        critical_dt = update_period ;
    }
}

/**
  * A P controller, except that the input is adjusted to steps
  * of a certain length of <burst_on_duration>, followed by an off period
  * of <burst_off_duration>
  */
final class PBurstController: AbstractControlNode
{
    enum uint order() {return 0 ;}

    // generate runtime introspective state property getter/setters
    enum STATE_FIELDS = [ "error", /*Controller proportional Error */] ;

    mixin(PopulateStateFields!(STATE_FIELDS)) ;
    mixin(PopulateStateMap!(STATE_FIELDS)) ;

    Real burst_start_time = -double.infinity ;

     // last controller update
    Real last_update_time = 0.0 ;

    // controller update schedule
    Real scheduled_update_time ;

    // controller is currently undergoing a burst
    bool burst_on = true ;

    // Control parameters
    const
    {
        Real k_p ; // proportional gain
        Real max_output ; // maximum controller output
        Real min_output ; // minimum controller output
        Transient target_output ; // target output variable
        Transient bias ; // the controller bias
        Real burst_off_duration ; // length of time for an on burst
        Real burst_on_duration ; // length of time to remain off between bursts
    }

    override @property string type() {return "PIDBurst" ; } 
    
    this(const Address address,
         const uint n_controls, 
         const uint n_inputs, 
         const Real[string] params,
         const bool[string] flags,
         const Transient[string] target_outputs)
    {
        this.burst_on_duration = params["burst_on_duration"] ;
        this.burst_off_duration = params["burst_off_duration"] ;
        this.critical_dt = burst_on_duration ;

        this.k_p = params["k_p"] ;
        this.max_output = params["max"] ;
        this.min_output = params["min"] ;
        this.error = 0.0 ;
        this.target_output = target_outputs["output"] ;
        this.bias = this.set_pid_bias(flags, target_outputs) ;

        super(address, n_controls, n_inputs) ;
    }

    override void update(const ref Real[] state, const Real t)
    {
        // check the time elapsed since the last update
        auto controller_dt = t - last_update_time ;

        // time elapsed is less than controller period, schedule update and continue
        if(controller_dt < scheduled_update_time)
        {
            critical_dt = scheduled_update_time - controller_dt ;
            return ;
        }

        // otherwise perform an update, switch on or off and calculate new state
        burst_on = !burst_on ;

        if(burst_on)
        {
            error = target_output(t) - input_ports[0]() ;

            // controller unconstrained output
            auto target = bias(t) + k_p*error ;

            // store output for later use
            _output_ports[0] = min(max(target, min_output), max_output) ;
        }
        else
        {
            _output_ports[0] = bias(t) ;
        }

        last_update_time = t ;
        scheduled_update_time = burst_on ? burst_on_duration : burst_off_duration ;
        critical_dt = scheduled_update_time ;
    }
}

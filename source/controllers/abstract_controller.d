module q1dcfd.controllers.abstract_controller ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Base class of a control node.
 *
 *  Defines a set of control variables, which are linked to the boundary 
 *  conditions of the system
 */

abstract class AbstractControlNode: from!"q1dcfd.control_volumes.node".Node
{
    import q1dcfd.config: Real, InputPort, Address ;

    // interfaces to map to each boundary condition
    protected Real[] _output_ports ;
    Real[] output_ports ;
    InputPort[] input_ports ;

    this(const Address address, const uint n_controls, const uint n_inputs)
    {
        super(address, from!"q1dcfd.types.geometry".NodeGeometry()) ;

        this._output_ports.length = n_controls ;
        this.output_ports.length = n_controls ;
        this.input_ports.length = n_inputs ;
    }

    override void update_balance(ref Real[] balance) 
    {
        output_ports[] = _output_ports[] ;
    }

    // Sets the initial output port values 
    void set_initial_state(const ref Real[] state)
    {
        this._output_ports[] = state[] ;
        this.output_ports[] = state[] ;
    }
}


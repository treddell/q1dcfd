module q1dcfd.controllers.set_ratio ;

import q1dcfd.controllers.abstract_controller: AbstractControlNode ;
import q1dcfd.config: Real, Transient, Address ;
import q1dcfd.utils.metaprogramming: PopulateStateMap, PopulateStateFields ;

import std.algorithm.comparison: min, max ;

/**
 * Assigns an output to be related to an input by a ratio 
 *
 * The output takes the form
 * o(t) = ratio(t)*i(t) 
 */
class SetRatio: AbstractControlNode
{
    enum uint order() {return 0 ;}
    enum string[] STATE_FIELDS = [] ;
    override @property string type() {return "SetRatio" ;}

    mixin(PopulateStateFields!([])) ;
    mixin(PopulateStateMap!([])) ;

    // Control parameters
    const Transient ratio ;

     this(const Address address,
          const uint n_controls, 
          const uint n_inputs, 
          const Real[string] params,
          const bool[string] flags,
          const Transient[string] target_outputs)
    {
        super(address, n_controls, n_inputs);

        // controller specific initialisation
        this.ratio = target_outputs["ratio"] ;
    }

    override void update(const ref Real[] state, const Real t)
    {
        // store output for later use
        _output_ports[0] = ratio(t)*input_ports[0]() ;
    }
}


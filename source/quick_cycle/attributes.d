module q1dcfd.quick_cycle.attributes ;

import q1dcfd.quick_cycle.objects ;
import coolprop ;

import std.format ;

// A bunch of cycle attributes
bool is_non_condensing(AbstractState state)
{
    /*
     * checks whether a specified state is to the right of the
     * saturation region, useful to ensure a compressor/pump inlet
     * state does not pass through a saturation region
     */
    if(state.phase == iphase_supercritical ||
       state.phase == iphase_supercritical_liquid ||
       state.phase == iphase_supercritical_gas ||
       state.phase == iphase_gas)
    {
        return true ;
    }

    return false ;
}

string is_power_cycle(T)(T r) if(
    is(T == SimpleBraytonStates) || is(T == RecuperatedBraytonStates))
{
    /*
     * Runs a series of checks to ensure that the result object is a valid
     * power cycle. Returns a string containing an error message or nothing
     * if a valid power cycle
     */

    string msg ;

    // The turbine outlet must be cooler than the turbine inlet
    if(r.turbine_outlet.T >= r.turbine_inlet.T)
    {
        msg ~= format!("Turbine outlet temperature (%f) exceeds inlet (%f), "
                       ~ "invalid cycle\n")(r.turbine_outlet.T, r.turbine_inlet.T) ;
    }

    // the compressor outlet must be hotter than the compressor inlet
    if(r.compressor_inlet.T >= r.compressor_outlet.T)
    {
        msg ~= format!("Compressor inlet temperature (%f) exceeds outlet (%f), "
                       ~ "invalid cycle\n")(r.compressor_inlet.T, r.compressor_outlet.T) ;
    }

    // the efficiency must not exceed carnot
    if(r.efficiency >= 1.0 - r.compressor_inlet.T/r.turbine_inlet.T)
    {
        msg ~= format!("Inconsistent Cycle: Efficiency %f exceeds Carnot %f")
            (r.efficiency, 1.0 - r.compressor_inlet.T/r.turbine_inlet.T) ;
    }
    return msg ;
}

string is_brayton_cycle(T)(T r) if(
    is(T == SimpleBraytonStates) || is(T == RecuperatedBraytonStates))
{
    /*
     * Checks that the result object is both a power cycle, and a Brayton Cycle
     * returns an error message if false, else nothing if true
     */
    auto msg = r.is_power_cycle ;

    if(!r.compressor_inlet.is_non_condensing)
    {
        msg ~= format!("compressor inlet state is phase %s, not a valid Brayton Cycle")
            (r.compressor_inlet.phase) ;
    }

    return msg ;
}

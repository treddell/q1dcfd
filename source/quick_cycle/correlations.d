module q1dcfd.quick_cycle.correlations ;

double pinch_point(double high_pressure, double low_pressure)
{
    /*
     * Correlation to estimate the location of the CO2 pinch point, for a heat
     * exchanger with high pressure (high_pressure) in Pa
     * and low pressure (low_pressure) in Pa
     *
     * Based off Ladislav et al 2016, Pinch Point Analysis of Heat Exchangers for
     * Supercritical Carbon Dioxide with Gaseous Admixtures in CCS Systems
     */

    // Cast to MPa
    high_pressure /= 1.0E6 ;
    low_pressure /= 1.0E6 ;

    import std.algorithm.mutation: swap ;
    if(high_pressure > low_pressure) swap(high_pressure, low_pressure) ;

    return (0.0008*low_pressure^^2 - 0.0297*low_pressure+0.0865)*high_pressure^^2
        + (0.0291*low_pressure^^2 + 1.2782*low_pressure - 3.9973)*high_pressure
        + (0.1363*low_pressure^^2 - 5.7124*low_pressure + 44.384) + 273 ;
}

module q1dcfd.quick_cycle.brayton ;

import q1dcfd.quick_cycle.objects ;
import q1dcfd.quick_cycle.processes ;
import q1dcfd.quick_cycle.correlations ;
import q1dcfd.quick_cycle.attributes ;
import coolprop ;

import std.format ;
import std.conv ;

/*
 * Calculates all the states and major characterisitics of a
 * Simple Brayton cycle, which, operates between known high and
 * low side pressures (turbine_inlet_pressure) and (turbine_outlet_pressure),
 * and temperatures (turbine_inlet_temperature, compressor_inlet_temperature)
 * with known efficiencies, and loss factors.
 */
SimpleBraytonStates design_simple_brayton(
    const string fluid,
    const double turbine_inlet_pressure,
    const double turbine_inlet_temperature,
    const double turbine_outlet_pressure,
    const double compressor_inlet_temperature,
    const double turbine_efficiency,
    const double compressor_efficiency,
    const double high_pressure_loss = 0.0,
    const double low_pressure_loss = 0.0)
in
{
    assert(turbine_inlet_pressure > turbine_outlet_pressure,
           "Turbine outlet pressure exceeds inlet, invalid") ;
    assert(turbine_efficiency >= 0 && turbine_efficiency <= 1,
           format!("Turbine efficiency must be strictly 0<=eta<=1, not %f")
           (turbine_efficiency)) ;
    assert(compressor_efficiency >= 0 && compressor_efficiency <= 1,
           format!("Compressor efficiency must be strictly 0<=eta<=1, not %f")
           (compressor_efficiency)) ;
    assert(low_pressure_loss >= 0 && low_pressure_loss <= 1,
           format!("Low pressure side loss must be strictly 0<=eta<=1, not %f")
           (low_pressure_loss)) ;
    assert(high_pressure_loss >= 0 && high_pressure_loss <= 1,
           format!("High pressure side loss must be strictly 0<=zeta<=1, not %f")
           (high_pressure_loss)) ;
}
out(result)
{
    auto msg = (cast (SimpleBraytonStates) result).is_brayton_cycle ;
    assert(msg == "", msg) ;    
}
do
{
    // calculate the inlet state
    auto turbine_inlet_state = new AbstractState("HEOS", fluid) ;
    turbine_inlet_state.update(PT_INPUTS, turbine_inlet_pressure, turbine_inlet_temperature) ;

    // calculate the outlet state
    auto turbine_outlet_state = expansion_process(
        turbine_inlet_state,
        turbine_efficiency,
        turbine_outlet_pressure) ;

    // calculate turbine work
    auto turbine_work = turbine_inlet_state.hmass - turbine_outlet_state.hmass ;

    // calculate the compressor inlet state
    auto compressor_inlet_pressure = turbine_outlet_pressure*(1.0 - low_pressure_loss) ;
    auto compressor_inlet_state = new AbstractState("HEOS", fluid) ;
    compressor_inlet_state.update(
        PT_INPUTS, compressor_inlet_pressure, compressor_inlet_temperature) ;

    // calculate the heat rejection
    auto heat_out = turbine_outlet_state.hmass - compressor_inlet_state.hmass ;

    // calculate the compressor outlet state
    auto compressor_outlet_pressure = turbine_inlet_pressure/(1.0 - high_pressure_loss) ;
    auto compressor_outlet_state = compression_process(
        compressor_inlet_state,
        compressor_efficiency,
        compressor_outlet_pressure) ;

    // calculate the compressor work
    auto compressor_work = compressor_outlet_state.hmass - compressor_inlet_state.hmass ;

    // calculate the net power
    auto power = turbine_work - compressor_work ;

    // calculate the heat input
    auto heat_in = turbine_inlet_state.hmass - compressor_outlet_state.hmass ;

    // calculate the efficiency
    auto efficiency = power/heat_in ;

    return SimpleBraytonStates(
        turbine_inlet_state,
        turbine_outlet_state,
        compressor_inlet_state,
        compressor_outlet_state,
        efficiency,
        power,
        turbine_work,
        compressor_work,
        heat_in,
        heat_out) ;
}

RecuperatedBraytonStates design_recuperated_brayton(
    const string fluid,
    const double turbine_inlet_pressure,
    const double turbine_inlet_temperature,
    const double turbine_outlet_pressure,
    const double compressor_inlet_temperature,
    const double turbine_efficiency,
    const double delta_temperature,
    const double compressor_efficiency,
    const double high_pressure_recuperator_loss = 0.0,
    const double low_pressure_recuperator_loss = 0.0,
    const double turbine_inlet_loss = 0.0,
    const double turbine_outlet_loss = 0.0,
    const double compressor_inlet_loss = 0.0,
    const double compressor_outlet_loss = 0.0
    )
in
{
    assert(turbine_inlet_pressure > turbine_outlet_pressure,
           "Turbine outlet pressure exceeds inlet, invalid") ;
    assert(high_pressure_recuperator_loss >= 0 && high_pressure_recuperator_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (high_pressure_recuperator_loss)) ;
    assert(low_pressure_recuperator_loss >= 0 && low_pressure_recuperator_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (low_pressure_recuperator_loss)) ;
    assert(turbine_inlet_loss >= 0 && turbine_inlet_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (turbine_inlet_loss)) ;
    assert(turbine_outlet_loss >= 0 && turbine_outlet_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (turbine_outlet_loss)) ;
    assert(compressor_inlet_loss >= 0 && compressor_inlet_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (compressor_inlet_loss)) ;
    assert(compressor_outlet_loss >= 0 && compressor_outlet_loss <= 1,
           format!("All pressure loss factors must be strictly 0<=zeta<=1, not %f")
           (compressor_outlet_loss)) ;
    assert(delta_temperature >= 0, format!("Must have a recuperator temperature "
                                            ~"delta T >=0. Not %f")(delta_temperature)) ;
}
do
{
    /*
     * Designs a recuperated Brayton cycle operating between
     * specified pressures and temperatures, and with specified
     * pressure loss ratios, and heat exchanger delta temperatures
     */

    // calculate equivilant simple cycle pressure loss factors
    auto equivilant_high_pressure_loss =
        -(-turbine_inlet_loss - compressor_outlet_loss
          - high_pressure_recuperator_loss + turbine_inlet_loss*compressor_outlet_loss
          + turbine_inlet_loss*high_pressure_recuperator_loss
          + compressor_outlet_loss*high_pressure_recuperator_loss
          - turbine_inlet_loss*compressor_outlet_loss*high_pressure_recuperator_loss) ;

    auto equivilant_low_pressure_loss =
        -(-turbine_outlet_loss - compressor_inlet_loss
          - low_pressure_recuperator_loss + turbine_outlet_loss*compressor_inlet_loss
          + turbine_outlet_loss*low_pressure_recuperator_loss
          + compressor_inlet_loss*low_pressure_recuperator_loss
          - turbine_outlet_loss*compressor_inlet_loss*low_pressure_recuperator_loss) ;
    
    auto simple_states = design_simple_brayton(
        fluid,
        turbine_inlet_pressure,
        turbine_inlet_temperature,
        turbine_outlet_pressure,
        compressor_inlet_temperature,
        turbine_efficiency,
        compressor_efficiency,
        equivilant_high_pressure_loss,
        equivilant_low_pressure_loss) ;

    // evaluate all pressures
    auto recuperator_high_outlet_pressure =
        turbine_inlet_pressure/(1.0 - turbine_inlet_loss) ;
    auto recuperator_high_inlet_pressure =
        recuperator_high_outlet_pressure/(1.0 - high_pressure_recuperator_loss) ;
    auto compressor_outlet_pressure =
        recuperator_high_inlet_pressure/(1.0 - compressor_outlet_loss) ;
    
    auto recuperator_low_inlet_pressure =
        turbine_outlet_pressure*(1.0 - turbine_outlet_loss) ;
    auto recuperator_low_outlet_pressure =
        recuperator_low_inlet_pressure*(1.0 - low_pressure_recuperator_loss) ;
    auto compressor_inlet_pressure =
        recuperator_low_outlet_pressure*(1.0 - compressor_inlet_loss) ;

    // calculate the pinch point if applicable
    double pinch_temperature = 0.0;
    if(fluid == "CO2")
    {
        pinch_temperature = pinch_point(
            0.5*(recuperator_high_inlet_pressure + recuperator_high_outlet_pressure),
            0.5*(recuperator_low_inlet_pressure + recuperator_low_outlet_pressure)) ;
    }

    // calculate the cooler inlet temperature
    double cooler_inlet_temperature ;
    if(pinch_temperature > simple_states.compressor_outlet.T)
    {
        cooler_inlet_temperature = pinch_temperature + delta_temperature ;
    }
    else
    {
        cooler_inlet_temperature = simple_states.compressor_outlet.T + delta_temperature ;
    }

    auto cooler_inlet_state = new AbstractState("HEOS", fluid) ;
    cooler_inlet_state.update(PT_INPUTS,
                              recuperator_low_outlet_pressure,
                              cooler_inlet_temperature) ;

    // calculate the cooler inlet conditions
    auto high_pressure_recuperator_outlet = new AbstractState("HEOS", fluid) ;
    high_pressure_recuperator_outlet.update(
        PT_INPUTS,
        recuperator_low_outlet_pressure,
        cooler_inlet_temperature) ;

    // calculate the heater inlet conditions
    auto low_pressure_recuperator_outlet = recuperation_process(
        simple_states.turbine_outlet,
        cooler_inlet_state,
        simple_states.compressor_outlet,
        high_pressure_recuperator_loss) ;

    // calculate net heat flows and efficiencies
    auto heat_recup = low_pressure_recuperator_outlet.hmass
        - simple_states.compressor_outlet.hmass ;
    auto heat_in = simple_states.heat_in - heat_recup ;
    auto heat_out = simple_states.heat_out - heat_recup ;
    auto efficiency = simple_states.power/heat_in ;

    // calculate recuperator inlet conditions
    auto high_pressure_recuperator_inlet_enthalpy = high_pressure_recuperator_outlet.hmass
        - heat_recup ;
    auto high_pressure_recuperator_inlet = new AbstractState("HEOS", fluid) ;
    high_pressure_recuperator_inlet.update(
        HmassP_INPUTS,
        high_pressure_recuperator_inlet_enthalpy,
        recuperator_high_inlet_pressure) ;

    return RecuperatedBraytonStates(
        simple_states.turbine_inlet,
        simple_states.turbine_outlet,
        high_pressure_recuperator_outlet,
        cooler_inlet_state,
        simple_states.compressor_inlet,
        simple_states.compressor_outlet,
        efficiency,
        simple_states.power,
        simple_states.turbine_work,
        simple_states.compressor_work,
        heat_in,
        heat_out,
        heat_recup) ;
}

RecuperatedBraytonStates optimise_recuperated_brayton(
    const string fluid,
    const double turbine_inlet_pressure,
    const double turbine_inlet_temperature,
    const double compressor_inlet_temperature,
    const double turbine_efficiency,
    const double delta_temperature,
    const double compressor_efficiency,
    const double min_pressure,
    const double high_pressure_recuperator_loss = 0.0,
    const double low_pressure_recuperator_loss = 0.0,
    const double turbine_inlet_loss = 0.0,
    const double turbine_outlet_loss = 0.0,
    const double compressor_inlet_loss = 0.0,
    const double compressor_outlet_loss = 0.0)
in
{
    assert(min_pressure > 0, "Negative Pressure!") ;
}
do
{
    /*
     * Designs a recuperated Brayton cycle to maximise efficiency
     * Searches for the optimal compressor inlet pressure based upon
     * some minimum pressure. If a minimum pressure is not given
     * or is less than pmin then pmin will be selected
     * will search for solutions both to the left and the right
     * of the saturation region, so long as states do not cross
     * into the saturated region
     *
     * NOT GUARANTEED TO DETERMINE A GLOBAL MINIMUM
     */

    // start with an initial guess pressure
    static RecuperatedBraytonStates result ;

    // define the function to optimise
    double zero_function(double p)
    {
        try
        {
            result = design_recuperated_brayton(
                fluid,
                turbine_inlet_pressure,
                turbine_inlet_temperature,
                p,
                compressor_inlet_temperature,
                turbine_efficiency,
                delta_temperature,
                compressor_efficiency,
                high_pressure_recuperator_loss,
                low_pressure_recuperator_loss,
                turbine_inlet_loss,
                turbine_outlet_loss,
                compressor_inlet_loss,
                compressor_outlet_loss) ;
        }
        // If an error is thrown then optimisation method has probably
        // wondered into the wrong neighberhood, return strong negative bias and
        // continue
        catch(CoolPropException e)
        {
            return 2.0 ;
        }

        // If not a valid cycle configuration, return strong negative bias and continue
        if(!result.compressor_inlet.is_non_condensing)
        {
            return 2.0 ;
        }

        return -result.efficiency ;
    }

    import std.numeric ;
    auto optimal = findLocalMin!(double)(&zero_function,
                                         min_pressure,
                                         turbine_inlet_pressure) ;

    return result ;
}

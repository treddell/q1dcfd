module q1dcfd.quick_cycle.processes ;

import coolprop ;
import q1dcfd.quick_cycle.objects ;

AbstractState recuperation_process(
    AbstractState hot_side_in,
    AbstractState hot_side_out,
    AbstractState cold_side_in,
    const double zeta = 0)
in
{
    assert(hot_side_in.T > hot_side_out.T, "Hot side in temperature must be "
           ~"hottest point") ;
    assert(hot_side_in.T > cold_side_in.T, "Hot side in temperature must be "
           ~"hottest point") ;
    assert(hot_side_out.T >= cold_side_in.T, "cold side inlet temperature "
           ~ "is greater than hot side outlet temperature, impossible heat exchanger") ;
    assert(zeta >= 0 && zeta <= 1, "pressure loss factor zeta must be strictly 0<=zeta<=1") ;
}
do
{
    /*
     * Calculates the cold side exit state of an internal heat exchanger
     * Based upon the known hot side inlet and outlet states, and the cold
     * side inlet state. Assumes the heat exchanger has  a pressure loss
     * fraction of zeta
     */

    auto cold_exit_pressure = cold_side_in.p *(1 - zeta) ;
    auto cold_exit_enthalpy = cold_side_in.hmass + (hot_side_in.hmass - hot_side_out.hmass) ;
    
    auto cold_side_out = new AbstractState("HEOS", cold_side_in.name) ;
    cold_side_out.update(HmassP_INPUTS, cold_exit_enthalpy, cold_exit_pressure) ;
    return cold_side_out ;
}

RecuperationStates dual_recuperation_process(
    AbstractState hot_side_out,
    AbstractState cold_side_in,
    const double cold_exit_temperature,
    const double zeta_hot = 0,
    const double zeta_cold = 0)
in
{
    assert(hot_side_out.T >= cold_side_in.T, "cold inlet temperature "
           ~"is greater than hot exit temperature. Impossible heat exchanger") ;
    assert(cold_exit_temperature > cold_side_in.T, "Cold side exit temperature "
           ~"is cooler than the inlet temperature") ;
    assert(zeta_hot >= 0 && zeta_hot <= 1, "All pressure loss factors must be strictly [0,1]") ;
    assert(zeta_cold >= 0 && zeta_cold <= 1, "All pressure loss factors must be strictly [0,1]") ;
}
do
{
    /*
     * Calculates the hot side inlet conditions and cold side outlet conditions
     * of an internal heat exchanger of effectiveness eta, based upon known
     * cold side outlet and hot side inlet conditions
     * zeta_hot/zeta_cold are the pressure loss factors over each stream
     */

    auto cold_exit_pressure = cold_side_in.p*(1 - zeta_cold) ;
    auto hot_side_in = new AbstractState("HEOS", hot_side_out.name) ;
    auto cold_side_out = new AbstractState("HEOS", cold_side_in.name) ;

    cold_side_out.update(PT_INPUTS, cold_exit_pressure, cold_exit_temperature) ;

    auto hot_inlet_enthalpy = hot_side_out.hmass
        + (cold_side_out.hmass - cold_side_in.hmass) ;
    auto hot_inlet_pressure = hot_side_out.p/(1.0 - zeta_hot) ;

    hot_side_in.update(HmassP_INPUTS, hot_inlet_enthalpy, hot_inlet_pressure) ;

    return RecuperationStates(hot_side_in, cold_side_out) ;
}

AbstractState expansion_process(
    AbstractState inlet_state,
    const double isentropic_efficiency,
    const double outlet_pressure)
in
{
    assert(isentropic_efficiency > 0 && isentropic_efficiency <= 1,
           "isentropic_efficiency must be strictly 0<eta<=1") ;
    assert(outlet_pressure < inlet_state.p, "Outlet pressure is higher than inlet pressure") ;
}
do
{
    /*
     * Calculates the outlet conditions for an expansion process
     * from inlet_state to a new state at pressure (outlet_pressure)
     * along a process of isentropic efficiency
     */

    auto outlet_state = new AbstractState("HEOS", inlet_state.name) ;
    auto isentropic_outlet_state = new AbstractState("HEOS", inlet_state.name) ;

    isentropic_outlet_state.update(PSmass_INPUTS, outlet_pressure, inlet_state.smass) ;
    auto outlet_enthalpy = inlet_state.hmass
    - isentropic_efficiency*(inlet_state.hmass - isentropic_outlet_state.hmass) ;
    outlet_state.update(HmassP_INPUTS, outlet_enthalpy, outlet_pressure) ;

    return outlet_state ;
}

AbstractState compression_process(
    AbstractState inlet_state,
    const double isentropic_efficiency,
    const double outlet_pressure)
in
{
    assert(isentropic_efficiency > 0 && isentropic_efficiency <= 1,
           "isentropic_efficiency must be strictly 0<eta<=1") ;
    assert(outlet_pressure > inlet_state.p, "Outlet pressure is lower than inlet pressure") ;
}
do
{
    /*
     * Calculates the outlet conditions for an compression process
     * from inlet_state to a new state at pressure (outlet_pressure)
     * along a process of isentropic_efficiency
     */

    auto outlet_state = new AbstractState("HEOS", inlet_state.name) ;
    auto isentropic_outlet_state = new AbstractState("HEOS", inlet_state.name) ;

    isentropic_outlet_state.update(PSmass_INPUTS, outlet_pressure, inlet_state.smass) ;
    auto outlet_enthalpy = inlet_state.hmass
        - (inlet_state.hmass - isentropic_outlet_state.hmass)/isentropic_efficiency ;
    outlet_state.update(HmassP_INPUTS, outlet_enthalpy, outlet_pressure) ;

    return outlet_state ;
}

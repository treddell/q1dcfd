module q1dcfd.quick_cycle.app_interface ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Takes sorted command line input and runs the relevant quick cycle design
 * routines
 */
void run_quick_cycle_design(const string[] args)
{
    import std.range ;
    
    import q1dcfd.quick_cycle.brayton ;
    import q1dcfd.utils.dictionary_tools: required, retrieve, greater, defaults, geq, leq,
        require_empty ;

    auto keys = args[1..($+1)/2] ;
    auto values = args[($+1)/2..$] ;
    string[string] dict ;

    foreach(pair ; zip(keys, values)) dict[pair[0]] = pair[1] ;

    // check for unrecongised fields
    scope(exit) dict.require_empty("CycleDesigner") ;

    switch(args[0])
    {
        case "brayton":
        {
            design_recuperated_brayton(
                dict.retrieve("fluid").required!string,
                dict.retrieve("turbine_inlet_pressure").required!double.greater(0),
                dict.retrieve("turbine_inlet_temperature").required!double.greater(0),
                dict.retrieve("turbine_outlet_pressure").required!double.greater(0),
                dict.retrieve("compressor_inlet_temperature").required!double.greater(0),
                dict.retrieve("turbine_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("delta_temperature").defaults(0.0).geq(0.0),
                dict.retrieve("compressor_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("high_pressure_recuperator_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("low_pressure_recuperator_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("turbine_inlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("turbine_outlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("compressor_inlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("compressor_outlet_loss").defaults(0.0).geq(0.0).leq(1.0)
            ).report ;

            break ;
        }

        case "optimise-brayton":
        {
            optimise_recuperated_brayton(
                dict.retrieve("fluid").required!string,
                dict.retrieve("turbine_inlet_pressure").required!double.greater(0.0),
                dict.retrieve("turbine_inlet_temperature").required!double.greater(0.0),
                dict.retrieve("compressor_inlet_temperature").required!double.greater(0.0),
                dict.retrieve("turbine_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("delta_temperature").defaults(0.0).geq(0.0),
                dict.retrieve("compressor_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("min_pressure").required!double.greater(0.0),
                dict.retrieve("high_pressure_recuperator_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("low_pressure_recuperator_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("turbine_inlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("turbine_outlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("compressor_inlet_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("compressor_outlet_loss").defaults(0.0).geq(0.0).leq(1.0)
            ).report ;

            break ;
        }

        case "simple-brayton":
        {
            design_simple_brayton(
                dict.retrieve("fluid").required!string,
                dict.retrieve("turbine_inlet_pressure").required!double.greater(0.0),
                dict.retrieve("turbine_inlet_temperature").required!double.greater(0.0),
                dict.retrieve("turbine_outlet_pressure").required!double.greater(0.0),
                dict.retrieve("compressor_inlet_temperature").required!double.greater(0.0),
                dict.retrieve("turbine_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("compressor_efficiency").defaults(1.0).geq(0.0).leq(1.0),
                dict.retrieve("high_pressure_loss").defaults(0.0).geq(0.0).leq(1.0),
                dict.retrieve("low_pressure_loss").defaults(0.0).geq(0.0).leq(1.0)
            ).report ;

            break ;
        }

        default:
        {
            import std.format ;

            throw new Exception("Cycle %s not understood".format(args[0])) ;
        }
    }
}

/**
 * Reports the output properties of the brayton cycle
 */
void report(T)(T states)
if(is(T == from!"q1dcfd.quick_cycle.objects".RecuperatedBraytonStates)
   || is(T == from!"q1dcfd.quick_cycle.objects".SimpleBraytonStates))
{
    import std.stdio ;
    import q1dcfd.quick_cycle.objects ;

    writeln("Turbine Inlet State:") ;
    writefln("Temperature: %f K", states.turbine_inlet.T) ;
    writefln("Pressure: %f MPa", states.turbine_inlet.p/1.0E6) ;

    writeln("\nTurbine Outlet State:") ;
    writefln("Temperature: %f K", states.turbine_outlet.T) ;
    writefln("Pressure: %f MPa", states.turbine_outlet.p/1.0E6) ;

    writeln("\nCompressor Inlet State:") ;
    writefln("Temperature: %f K", states.compressor_inlet.T) ;
    writefln("Pressure: %f MPa", states.compressor_inlet.p/1.0E6) ;

    writeln("\nCompressor Outlet State:") ;
    writefln("Temperature: %f K", states.compressor_outlet.T) ;
    writefln("Pressure: %f MPa", states.compressor_outlet.p/1.0E6) ;

    static if(is(T == RecuperatedBraytonStates))
    {
        writeln("\nHeater Inlet State:") ;
        writefln("Temperature: %f K", states.high_pressure_recuperator_outlet.T) ;
        writefln("Pressure: %f MPa", states.high_pressure_recuperator_outlet.p/1.0E6) ;

        writeln("\nCooler Inlet State:") ;
        writefln("Temperature: %f K", states.low_pressure_recuperator_outlet.T) ;
        writefln("Pressure: %f MPa", states.low_pressure_recuperator_outlet.p/1.0E6) ;
    }

    writeln("\nCycle Properties") ;
    writefln("Heating power: %f J/kg", states.heat_in) ;
    writefln("Heat rejection: %f J/kg", states.heat_out) ;
    static if(is(T== RecuperatedBraytonStates))
    {
        writefln("Heat recuperated: %f J/kg", states.heat_recup) ;
    }
    writefln("Compressor work: %f J/kg", states.compressor_work) ;
    writefln("Turbine work: %f J/kg", states.turbine_work) ;
    writefln("Thermal efficiency: %f", states.efficiency) ;
}

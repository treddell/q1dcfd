module q1dcfd.quick_cycle.objects ;

import coolprop ;
import std.typecons ;

// implemented cycle types and their id's
enum CYCLE_TYPES
{
    SIMPLE_BRAYTON = 1,
    RECUPERATED_BRAYTON = 2,
} ;

// return tuple for the dual_recuperation_process
alias RecuperationStates = Tuple!(
    AbstractState, "hot_side_in",
    AbstractState, "cold_side_out") ;

// The states of a simple Brayton Cycle
alias SimpleBraytonStates = Tuple!(
    AbstractState, "turbine_inlet",
    AbstractState, "turbine_outlet",
    AbstractState, "compressor_inlet",
    AbstractState, "compressor_outlet",
    double, "efficiency",
    double, "power",
    double, "turbine_work",
    double, "compressor_work",
    double, "heat_in",
    double, "heat_out") ;

// The states of a recuperated Brayton Cycle
alias RecuperatedBraytonStates = Tuple!(
    AbstractState, "turbine_inlet",
    AbstractState, "turbine_outlet",
    AbstractState, "high_pressure_recuperator_outlet",
    AbstractState, "low_pressure_recuperator_outlet",
    AbstractState, "compressor_inlet",
    AbstractState, "compressor_outlet",
    double, "efficiency",
    double, "power",
    double, "turbine_work",
    double, "compressor_work",
    double, "heat_in",
    double, "heat_out",
    double, "heat_recup") ;

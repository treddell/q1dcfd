module q1dcfd.thermal_face ;

import q1dcfd.config: Real, SystemProperty ;
import q1dcfd.simulation: Simulation ;

import std.stdio ; 

final class ThermalFace
{
    /*
     * Tracks heat transfer across a boundary consisting of multiple nodes
     */

    immutable
    {
        string name ;
        string domain1 ;
        string domain2 ;
    }
    
    private
    {
        Real _q_dot ;
        long q_dot_index = -1 ;
    }

    this(const string name, const string domain1, const string domain2,
         const bool save)
    {
        this.name = name ;
        this.domain1 = domain1 ;
        this.domain2 = domain2 ;        

        if(save)
        {
            q_dot_index = Simulation.n_saved_fields ;
            Simulation.add_system_saved_property(
                SystemProperty(
                    delegate (ref Real[] save_state)
                    {
                        save_state[q_dot_index] = q_dot ;
                
                    })) ;
        }

        reset ;
    }
    
    // add a heat transfer contribution
    void add_heat(Real q_dot) {this._q_dot += q_dot ;}

    @property Real q_dot() {return _q_dot ;}

    void reset() {this._q_dot = 0 ;}

    void write_metadata_to_disk(File file)
    /*
     * Writes the metadata of the face to meta.txt. Used by the post
     * processor to reconstruct the system
     */
    {
        file.writefln("\n[FACE]: %s", name) ;
        file.writefln("domains: %s->%s: %s", domain1, domain2, "str[]") ;
        file.writefln("q_dot_index: %u: %s", q_dot_index, "int") ;
    }
}

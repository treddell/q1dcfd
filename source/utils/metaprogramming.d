module q1dcfd.utils.metaprogramming ;

/**
 * Assigns a list of state attributes to the current scope via a mixin 
 * statement. Used to populate nodal class definitions in a way which allows
 * the names of each field to be used in runtime introspection
 *
 * WILL BE DEPRECATED
 */
template PopulateStateFields(string[] fields)
{
    import std.format ;

    string PopulateStateFields()
    {
        string str_out = "" ;
        foreach(field ; fields)
        {
            str_out ~= format!("private Real _%s ;")(field) ;
            str_out ~= format!("@property Real %s() {return _%s ;} ")
                (field, field) ;
            str_out ~= format!("@property Real %s(Real val) {return _%s=val ;}")
                (field, field) ;
        }

        return str_out ;
    }
}

/**
 * When used with mixin, generates a method map(string), which retrieves
 * a state attribute from a string representing the attribute name. Used for
 * runtime introspection
 *
 * WILL BE DEPRECATED
 */
template PopulateStateMap(string[] fields)
{
    import std.format ;

    string PopulateStateMap()
    {
        string str_out = format!("override Real map(string prop) "
            ~"{switch(prop) {")() ;
        foreach(field ; fields)
        {
            str_out ~= format!("case `%s`: return %s ;")(field, field);
        }

        return str_out ~"default: assert(false, `attempt to access undefined "
        ~ "state field`) ;}}";
    }
}

/**
 * Used to generate a procedure which calls the relevant propertyFields and saves them
 * to the result vector
 */ 
void generate_save_method(T)(T node, const string[] saved_props)
{
    import q1dcfd.config: Real ;
    
    node.write_to = (ref Real[] saved_state)
    {
        foreach(immutable i, field ; saved_props)
        {
            saved_state[node.w_index_l + i] = node.map(field) ;
        }
    } ;
}

/**
 * from import idiom, based on https://dlang.org/blog/2017/02/13/a-new-import-idiom/
 *
 * Use as:
 *     from!"module".object ...
 */
template from(const string module_name)
{
    mixin("import from = " ~ module_name ~ ";") ;
}

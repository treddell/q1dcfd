module q1dcfd.utils.logging ;

import std.stdio ;
import std.datetime ;
import std.format ;
import std.conv: to ;
import std.algorithm.iteration ;

import q1dcfd.config: Real ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.domains.domain: Domain ;
import q1dcfd.control_volumes: Node ;
import q1dcfd.stream: Stream ;
import q1dcfd.thermal_face: ThermalFace ;

/**
 * writes to a log file a report of the current configuration space
 */
debug void report_current_state()
{
    auto log_file = File(Simulation.log_file, "a") ;

    for(size_t i = 0 ; i < Simulation.order ;i++)
    {
        log_file.writefln("%-5u %-15f %f ",i, Simulation.get_state(i), 
            Simulation.get_balance(i)) ;
    }
}

debug void log_message(string msg)
{
    auto log_file = File(Simulation.log_file, "a") ;
    log_file.writeln(msg) ;
}

/*
 * Reports a simulation failure by printing the current stored state 
 */
void report_transient_failure(T)(T e, Real t, Real dt, ulong count)
{
    auto log_file = File(Simulation.results_folder ~ "ERROR.txt", "w") ;
    string now = Clock.currTime.toSimpleString ;

    log_file.writeln(format!("Error log for simulation %s, created on %s\n"
        ~"An exception occured within the transient simulation loop "
        ~"on time iteration %u at time %f with dt = %f \n")
        (Simulation.name, now, count, t, dt)) ;

    log_file.writeln("[MSG, STACK TRACE]") ;
    log_file.writeln(e) ; // log the exception

    // log the state upon failure
    log_file.writeln("\n[FAILURE STATE]") ;
    log_file.writefln("%-5s %-15s %s","index", "state", "balance") ;
    
    for(size_t i = 0 ; i < Simulation.order ;i++)
    {
        log_file.writefln("%-5u %-15f %f ",i, Simulation.get_state(i), 
            Simulation.get_balance(i)) ;
    }
}

/* 
 * Reports a simulation failure by printing the current stored state 
 */
void report_pseudosteady_failure(T)(T e, Real dt, ulong count, Real delta)
{
    auto log_file = File(Simulation.results_folder ~"ERROR.txt", "w") ;
    string now = Clock.currTime.toSimpleString ;

    log_file.writefln(format!("Error log for simulation %s created on %s\n"
        ~"An exception occured within the steady state solver, on pseudutime "
        ~"iteration %u with delta psuedo time = %f. Last successful residual "
        ~"calculation was %f")(Simulation.name, now, count, dt, delta)) ;

    // log the state upon failure
    log_file.writeln("\n[FAILURE STATE]") ;
    log_file.writefln("%-5s %-15s %s","index", "state", "balance") ;
    
    for(size_t i = 0 ; i < Simulation.order ;i++)
    {
        log_file.writefln("%-5u %-15f %f ",i, Simulation.get_state(i), 
            Simulation.get_balance(i)) ;
    }
}

/*
 * Records information for an entire time step, used by -debug flag to write a 
 * log file
 */
debug void log_time_step(Real t, Real dt, ulong count, string filename)
{
    auto log_file = File(filename, "a") ;
    log_file.writefln("\nTIME STEP: %u, t = %f, dt = %f", count, t, dt) ;
    log_file.writefln("\n") ;

    // Report the current state and balance 
    log_file.writefln("%-5s %-15s %s","index", "state", "balance") ;
    report_current_state ;
    log_file.writeln("\n") ;
}

/*
 * create some header text for a log file (using -debug=(>=1)) 
 */
debug void log_header()
{
    string now = Clock.currTime.toSimpleString ;

    auto log_file = File(Simulation.log_file, "w") ;
    log_file.writefln("Log file for simulation %s, created on %s", Simulation.name, now);
    log_file.writeln("") ;
}

/// Make a record in the log file that a state update was performed
debug void log_state_update(Real t, Real dt)
{
    auto log_file = File(Simulation.log_file, "a") ;
    log_file.writefln("\nSTATE UPDATE with t = %f, dt = %f\n", t, dt) ;
}

/*
 * writes out the simulation metadata to disk.
 * Final argument in writefln statements, tells the post processing parser what 
 * type it should interpret the field as 
 */
void write_simulation_meta(File meta)
{
    string now = Clock.currTime.toSimpleString ;

    string precision ;
    if(typeid(Real) is typeid(real))
    {
        precision = "real" ;
    }
    else if(typeid(Real) is typeid(double))
    {
        precision = "double" ;
    }
    else if(typeid(Real) is typeid(float))
    {
        precision = "float" ;
    }

    meta.writefln("Metadata file for simulation %s, created on %s", 
        Simulation.name, now) ;
    meta.writefln("This file is machine read and should not be modified") ;
    meta.writefln("") ;

    meta.writefln("[SYSTEM DATA]") ;
    meta.writefln("precision: %s: %s", precision, "str") ;
    meta.writefln("n_ode: %u: %s", Simulation.order, "int") ;
    meta.writefln("n_fields: %u: %s", Simulation.n_saved_fields, "int") ;
    meta.writefln("n_nodes: %u: %s" , Simulation.n_nodes, "int") ;
    meta.writefln("n_domains: %u", Simulation.n_domains, "int") ;
    meta.writefln("n_interfaces: %u", Simulation.n_interfaces, "int") ;
    meta.writefln("integrator: %s: %s", typeid(Simulation.integrator), "str") ;
    meta.writefln("min_delta_t: %f: %s", Simulation.integrator.min_delta_t, "float") ;
    meta.writefln("max_delta_t: %f : %s", Simulation.integrator.max_delta_t, "float") ;
    meta.writefln("adap_sf: %f: %s", Simulation.integrator.adap_sf, "float") ;
    meta.writefln("adap_tol: %f : %s", Simulation.integrator.adap_tol, "float") ;
    meta.writefln("max_CFL: %f: %s", Simulation.max_CFL, "float") ;
    meta.writefln("end_time: %f: %s", Simulation.end_time, "float") ;
    meta.writefln("save_incriment: %f : %s", Simulation.save_interval, "float") ;
    meta.writeln("") ;

    meta.writeln("[STREAM DATA]") ;
    Simulation
        .list_streams
        .each!(stream => (cast(Stream) Simulation.get_stream(stream))
               .write_metadata_to_disk(meta)) ;
    meta.writeln("") ;

    meta.writeln("[FACE DATA]") ;
    Simulation
        .list_faces
        .each!(face => (cast(ThermalFace) Simulation.get_face(face))
               .write_metadata_to_disk(meta)) ;
    meta.writeln("") ;

    meta.writeln("[DOMAIN DATA]") ;
}

debug void log_manager_creation(string[string] args)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;

    log_file.writeln("\n[SIMULATION MANAGER]") ;
    log_file.write("Initialised simulation with parameters:") ;
    log_file.writeln(args) ;
}

debug void log_beginning_discretisation()
{
    "[CONSTRUCTING ABSTRACT INTERNAL REPRESENTATION]\n"
        .add_message_to_log_file ;
}

debug void log_discretising_domain_start(const string name)
{
    "[discretising %s]".format(name).add_message_to_log_file ;
}

debug void log_discretising_domain_done(const string name)
{
    "[%s done]\n".format(name).add_message_to_log_file ;
}

debug void log_geometry_creation(const string name, const string[string] geometry_defs)
{
    "Creating geometry `%s` with definitions %s\n"
        .format(name, geometry_defs.to!string)
        .add_message_to_log_file ;
}

debug void log_domain_creation(const string name, const string[string] domain_defs)
{
    "Creating domain `%s` with definitions %s\n"
        .format(name, domain_defs.to!string)
        .add_message_to_log_file;
}

debug void log_memory_allocation()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[ALLOCATING MEMORY]") ;
    log_file.writefln("Allocated space for %u configuration variables",
        Simulation.order) ;
    log_file.writefln("Allocated space for %u nodes", Simulation.n_nodes) ;
}

debug void log_saved_state_memory_allocation()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[ALLOCATING SAVED PROPERTIES MEMORY]") ;
    log_file.writefln("Allocated a saved state vector of length %s", 
        Simulation.n_saved_fields) ;
}

debug void log_starting_connection()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[CONSTRUCTING CONNECTIONS]") ;
}

debug void log_connection(string[2] pair)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("[done] %s -> %s", pair[0], pair[1]) ;
}

debug void log_node_queues(Node[] ordered_nodes, Node[] unordered_nodes)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[NODE JOB QUEUE]") ;

    log_file.writefln("Node list divided into %u ordered nodes, %u unordered "
        ~"nodes\n",ordered_nodes.length, unordered_nodes.length) ;
}

debug void log_starting_linkages()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[CONSTRUCTING CONTROL LINKAGES]") ;
}

debug void log_completed_linkage(Domain domain)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("[done] %s", domain.name) ;
}

debug void log_starting_initialisation()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n[INITIALISING STATE]") ;
}

debug void log_completed_initialisation(Domain domain)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("[done] %s ",domain.name) ;
}

debug void log_domain_set(Domain[string] domains)
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    log_file.writefln("\n Full domain set is %s",domains.keys.to!string) ;
}

/*
 * Adds a message directly to the log file
 */
debug void add_message_to_log_file(const string message)
{
    version(unittest)
    {
        return ; // log file will not be initialised for tests
    }
    else
    {
        auto log_file = File(Simulation.constants.log_file, "a") ;
        log_file.writeln(message) ;
    }
}

/*
 * Adds a message to the temporary log buffer
 */
debug void add_message_to_log_buffer(const string message)
{
    version(unittest)
    {
        return ; // log file will not be initialised for tests
    }
    else
    {
        Simulation.log_buffer[Simulation.log_counter] = message ;
        Simulation.log_counter++ ;
        if(Simulation.log_counter == Simulation.log_buffer.length)
        {
            Simulation.log_counter = 0 ;
        }
    }
}

/*
 * Writes the contents of the log buffer to disk then clears it
 */
debug void write_log_buffer_to_file()
{
    auto log_file = File(Simulation.constants.log_file, "a") ;
    foreach(i ; Simulation.log_counter..Simulation.log_buffer.length)
    {
        log_file.writeln(Simulation.log_buffer[i]) ;
    }
    foreach(i ; 0..Simulation.log_counter)
    {
        log_file.writeln(Simulation.log_buffer[i]) ;
    }

    Simulation.log_buffer[] = "" ;
}

module q1dcfd.utils.pprint ;

// Pretty print functions for q1dcfd

void completion_print(string begin_msg, string end_msg)
/* Completes a write statement of the form write(begin_msg)
With a line of dots until the end of the terminal, followed by end_msg */
{
    import core.sys.posix.unistd;
    import core.sys.posix.sys.ioctl;
    import std.algorithm.comparison: max ;
    import std.stdio ;

    winsize ws ;
    ulong n_dots = 0 ;

    if(ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) != -1)
    {
        n_dots = max(ws.ws_col - begin_msg.length - end_msg.length - 1, 0) ;
    }

    foreach(i ; 0..n_dots) {write(".") ;}
    write(end_msg) ;
    write("\n") ;
    stdout.flush() ;
}
module q1dcfd.utils.convert_fluid_props ;

import q1dcfd.config: Real ;
import q1dcfd.utils.math: square ;
import q1dcfd.numerics.root: root1d ;
import q1dcfd.utils.errors: NonConvergenceError ;
import q1dcfd.eos: EOSBase ;

import std.algorithm.comparison: max, min ;
import std.algorithm.iteration: reduce ;
import std.format ;
version(high_prec) import std.math: fabs ; 
else import core.stdc.math: fabs ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

/**
 * Determines the conservative (rho, e) state on the far side of a linked 
 * boundary discontinuity.
 *
 * Assumes h,s, rho*v of the upstream state and rho*(e + 1/2 v^2) of the
 * downstream state is known. The calculates a property change across the boundary
 * of the form:
 *
 * h_d - h_u = eta*(h_ds - h_u)
 * rho_d v_d = rho_u v_u
 *
 *Where h_ds = h(p_d, s_u) and eta is some effective efficiency
 */
@fastmath
void cons_from_pressure_step(
    EOSBase conservative_eos,
    EOSBase isentropic_eos,
    ref Real[2] conservative_vars, 
    const Real dm, 
    const Real h1, 
    const Real s1,
    const Real rhoE, 
    const Real eta, 
    const Real tol = 1.0E-6, 
    const int max_iter = 25)
{
    Real pd, pe, hd, he, hp ; // partial derivatives
    Real j11, j12, j21, j22 ; // jacobian elements
    Real rho = conservative_vars[0] ;
    Real e = conservative_vars[1] ;
    Real p, v2 ; // pressure/velocity squared downstream
    Real[2] delta ; // difference in successive iterates
    Real[2] delta_rel ; // relative difference in successive iterates
    Real[2] F ; // // zero function
    Real detJ ; // jacobian determinant
    Real dm2 = dm.square ;

    foreach(i ; 0..max_iter)
    {
        conservative_eos.update(rho, e) ;
        p = rho*(conservative_eos.h - e) ;
        hd = conservative_eos.dhdD ;
        he = conservative_eos.dhde ;
        pd = conservative_eos.h - e + rho*hd ;
        pe = rho*(he - 1.0) ;
        v2 = dm2/rho.square ;

        isentropic_eos.update(p, s1) ;
        hp = isentropic_eos.dhdp ;

        j11 = hd - eta*pd*hp ;
        j12 = he - eta*pe*hp ;
        j21 = -e - 0.5*v2 ;
        j22 = -rho ;

        detJ = j11*j22 - j21*j12 ;

        F[] = [
            conservative_eos.h - h1 - eta*(isentropic_eos.h - h1) ,
            rhoE - rho*(e + 0.5*v2) 
        ] ;

        delta[] = [(j22*F[0] - j12*F[1])/detJ, (-j21*F[0] + j11*F[1])/detJ] ;

        delta_rel[0] = delta[0]/rho ;
        delta_rel[1] = delta[1]/e ;

        if(max(fabs(reduce!(max)(delta_rel)), fabs(reduce!(min)(delta_rel))) < tol)
        {
            conservative_vars = [rho, e] ;
            return ;
        }
        else
        {
            rho -= delta[0] ;
            e -= delta[1] ;
        }
    }

    throw new NonConvergenceError(
        "Newtons method failed to convergence in %u iterations. Relative residual is %(%f, %)"
        .format(max_iter, delta_rel)) ;
}

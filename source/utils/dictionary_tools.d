module q1dcfd.utils.dictionary_tools ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Temporary structure used to store a result extracted from a definition
 * script and undergoing pre-processing.
 *
 * Stores the field key and value, or else the key and a boolean denoting
 * the field as empty
 */
struct GenericField(T)
{
    const string key ;
    const T value ;
    const bool empty ;

    alias value this ;

    // Non empty constructor
    this(const string key, const T value)
    {
        this.key = key ;
        this.value = value ;
        this.empty = false ;
    }

    // Empty constructor
    this(const string key)
    {
        this.key = key ;
        this.value = value.init ;
        this.empty = true ;
    }
}

/**
 * Retrieves a value from a dictionary and casts the result to a GenericField
 * If the key is not in the dictionary then returns an empty GenericField
 */
GenericField!(string) retrieve(ref string[string] args, const string key)
{
    if(key in args)
    {
        scope(exit) args.remove(key) ;
        return GenericField!(string)(key, args[key]) ;
    }
    return GenericField!(string)(key) ;
}

/**
 * Takes a GenericField and if it is empty then casts it to a default value
 * of the correct type. If it is not empty then casts the value to the correct
 * type if it is not already so
 */
GenericField!(T) defaults(T,M)(const GenericField!(M) field, const T default_val)
{
    import std.conv: to ;
    import std.format ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    if(field.empty) return GenericField!(T)(field.key, default_val) ;
    else
    {
        try
        {
            return GenericField!(T)(field.key, field.value.to!T) ;
        }
        catch(Exception e)
        {
            throw new InvalidDefinition("Could not cast %s=%s from type %s to type %s"
                    .format(field.key, field.value, typeid(M).toString, typeid(T).toString)) ;
        }
    }
}

/**
 * Takes a generic field and casts to the correct type. If the field is empty
 * then throws an error
 */
GenericField!(T) required(T, M)(const GenericField!(M) field)
{
    import std.conv ;
    import std.format ;
    
    import q1dcfd.utils.errors: InvalidDefinition ;
    import q1dcfd.types: MathFunction, Transient;

    if(field.empty) throw new InvalidDefinition("Required field %s not found".format(field.key)) ;

    // attempt to parse string and convert to a math function
    static if (is(T == MathFunction))
    {
        try
        {
            return GenericField!(T)(field.key, field.value.read_boundary_function);
        }
        catch(Exception e)
        {
            throw new InvalidDefinition(("Attempted to cast %s=%s to a MathFunction. But failed"
                                         ~ " with traceback %s")
                                        .format(field.key, field.value, e.msg));
        }
    }
    // Attempts to cast to a math function, and return the evaluate function
    else static if(is(T == Transient))
    {
        try
        {
            return GenericField!(T)(field.key, &field.value.read_boundary_function.evaluate);
        }
        catch(Exception e)
        {
            throw new InvalidDefinition(("Attempted to cast %s=%s to a MathFunction. But failed"
                                         ~ " with traceback %s")
                                        .format(field.key, field.value, e.msg));
        }
    }
    // Attempt the cast
    else
    {
        try
        {
            return GenericField!(T)(field.key, field.value.to!T) ;
        }
        catch(Exception e)
        {
            throw new InvalidDefinition(
                "Could not cast %s=%s to type %s"
                .format(field.key, field.value, typeid(T).toString)) ;
        }
    }
}

// All valid comparison options
private enum compare_type {less, leq, greater, geq} ;

/*
 * Runs a comparison on a GenericField and a value, throws an error if the comparison
 * condition is not satisfied
 *
 * Acts as a template class for different comparions types, valid options are
 * less, leq, greater, geq
 */
private GenericField!(T) comparison(T, compare_type ty)(const GenericField!(T) field,
                                                        const T comp_value)
if(from!"std.traits".isNumeric!(T))
{
    import std.format ;
    import std.conv: to ;
    import std.exception: enforce ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    static if(ty == compare_type.less)
    {
        enforce!InvalidDefinition(field.value < comp_value,
                                  "Field %s must be strictly < %s"
                                  .format(field.key, comp_value.to!string)) ;
    }
    else static if(ty == compare_type.leq)
    {
        enforce!InvalidDefinition(field.value <= comp_value,
                                  "Field %s must be strictly <= %s"
                                  .format(field.key, comp_value.to!string)) ;

    }
    else static if(ty == compare_type.greater)
    {
        enforce!InvalidDefinition(field.value > comp_value,
                                  "Field %s must be strictly > %s"
                                  .format(field.key, comp_value.to!string)) ;
    }
    else
    {
        enforce!InvalidDefinition(field.value >= comp_value,
                                  "Field %s must be strictly >= %s"
                                  .format(field.key, comp_value.to!string)) ;
    }
    
    return field ;
}

/**
 * Checks the value of a GenericField to ensure it is strictly less than some bound
 * Throws an error if false, else returns the GenericField
 */
GenericField!(T) less(T)(const GenericField!(T) field, const T max_value)
{
    return comparison!(T, compare_type.less)(field, max_value) ;
}

/**
 * Checks the value of a GenericField to ensure it is less than or equal to some bound
 * Throws an error if false, else returns the GenericField
 */
GenericField!(T) leq(T)(const GenericField!(T) field, const T max_value)
{
    return comparison!(T, compare_type.leq)(field, max_value) ;
}

/**
 * Checks the value of a GenericField to ensure it is strictly greater than some bound
 * Throws an error if false, else returns the GenericField
 */
GenericField!(T) greater(T)(const GenericField!(T) field, const T min_value)
{
    return comparison!(T, compare_type.greater)(field, min_value) ;
}

/**
 * Checks the value of a GenericField to ensure it is greater than or equal to some bounds
 * Throws an error if false, else returns the GenericField
 */
GenericField!(T) geq(T)(const GenericField!(T) field, const T min_value)
{
    return comparison!(T, compare_type.geq)(field, min_value) ;
}

/**
 * Reads a generic comma seperated string field, restructures it as an
 * array of types, and checks that the array is a proper
 * subset of <superset>
 */
GenericField!(T[]) subset(T)(const GenericField!(string) field, const T[] superset)
{
    import std.string ;
    import std.exception: enforce ;
    import std.algorithm.searching: canFind ;
    import std.format ;
    import std.conv: to ;

    import q1dcfd.utils.errors: InvalidDefinition ;

    auto sub = field.value.split(",") ;
    if(sub.length == 0) return GenericField!(T[])(field.key) ;
    foreach(elem ; sub)
    {
        enforce!InvalidDefinition(superset.canFind(elem),
                                  "%s is not a valid property".format(elem)) ;
    }
    
    return GenericField!(T[])(field.key, sub.to!(T[])) ;
}

/**
 * Maps a string[string] key: value to a type T by mapping value to that of the
 * provided T[string] dictionary
 *
 * Throws an exception if value is not an element of map
 */
T map_from(T)(const GenericField!(string) field, T[string] map)
{
    import std.format ;
    import std.exception: enforce ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    enforce!InvalidDefinition(field.value in map,
                              "value %s of field %s not found in map"
                              .format(field.value, field.key)) ;

    return map[field.value] ;
}

/**
 * Request that a source dictionary give up key <old_key> and give it to
 * a target dictionary as <new_key>
 */
string[string] request(
    string[string] target,
    ref string[string] source,
    const string old_key,
    string new_key = "")
{
    if(old_key !in source) return target ;

    // optional argument not provided, use default key
    if(new_key == "") new_key = old_key ;

    target[new_key] = source[old_key] ;
    source.remove(old_key) ;
    return target ;
}

/**
 * Demand that a source dictionary give up <old_key> and give it to a target
 * dictionary as <new_key>
 *
 * Will throw an error if the demand cannot be met
 */
string[string] demand(
    string[string] target,
    ref string[string] source,
    const string old_key,
    string new_key = "")
{
    import std.format ;
    import std.exception: enforce ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    enforce!InvalidDefinition(old_key in source,
                              "Key %s required but not found".format(old_key)) ;

    // optional argument not provided, use default key
    if(new_key == "") new_key = old_key ;

    target[new_key] = source[old_key] ;
    source.remove(old_key) ;
    return target ;
}

/**
 * Same as request but preserves the key in the original AA
 */
string[string] request_copy(
    string[string] target,
    ref string[string] source,
    const string old_key,
    string new_key = "")
{
    if(old_key !in source) return target ;

    // optional argument not provided, use default key
    if(new_key == "") new_key = old_key ;

    target[new_key] = source[old_key] ;
    return target ;
}

/**
 * Same as demand but preserves the key in the original AA
 */
string[string] demand_copy(
    string[string] target,
    ref string[string] source,
    const string old_key,
    string new_key = "")
{
    import std.format ;
    import std.exception: enforce ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    enforce!InvalidDefinition(old_key in source,
                              "Key %s required but not found".format(old_key)) ;

    // optional argument not provided, use default key
    if(new_key == "") new_key = old_key ;

    target[new_key] = source[old_key] ;
    return target ;
}

/**
 * Functional method to create an empty associative array inline
 */
T[string] new_dict(T)()
{
    T[string] result ;
    return result ;
}

/**
 * Make sure an associative array is empty, and throw error if not
 */
void require_empty(string[string] defs, const string context)
{
    import std.exception: enforce ;
    import std.conv: to ;
    import std.format ;
    import q1dcfd.utils.errors: InvalidDefinition ;

    enforce!InvalidDefinition(defs.length == 0,
                              "Unexpected fields: %s found in Block %s"
                              .format(defs.keys.to!string, context)) ;
}


module q1dcfd.utils.progress_bar ;

import std.stdio: write, writeln, stdout, writef ;
import std.conv: to ;
import std.datetime: Clock;
import core.sys.posix.unistd;
import core.sys.posix.sys.ioctl;
import core.time ;
import std.exception: enforce ;
import core.thread ;
import q1dcfd.config: Real ;

// Will revert to this width if the terminal is too skinny
enum size_t DEFAULT_WIDTH = 40 ; 

class ProgressBar 
/* A simple dynamic progress bar */
{ 
    const Real total ;
    const Real r_total ;
    double last_update_time ;
    size_t barsize ;
    const string pre_statement = "Simulating |" ;
    const Real update_period ;
    char[] progress_bar ;

    this(const Real total, const double update_period = 0.1) 
    {

        enforce(total > 0, "Cannot give a non positive total count") ;
        enforce(update_period >= 0, "Cannot have a negative update_period") ;

        winsize ws ;

        this.total = total ;
        this.r_total = 1.0/total ;
        this.last_update_time = Clock.currTime!(ClockType.coarse).toUnixTime ;
        this.update_period = update_period ;

        if(ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) != -1) 
        {
            this.barsize = ws.ws_col - this.pre_statement.length - 10;
        }
        if(barsize <= 0) 
        {
            this.barsize = DEFAULT_WIDTH ;
        }

        this.progress_bar.length = barsize ;
    }

    void update(Real count) 
    // update the completion status to count/total
    {
        double t = Clock.currTime!(ClockType.coarse).toUnixTime ;
        Real frac ;
        size_t n_bars ;

        if (count >= total) 
        {
            // finished, print completion screen then newline
            write("\r") ;
            write(pre_statement) ;
            for(size_t i = 0 ; i < this.barsize; i++) write("\u2588") ;

            write("| [DONE] ") ;
            stdout.flush() ;
            write("\n") ;
        }

        else if (t - last_update_time >= update_period) 
        {
            // update the progress bar
            frac = count*r_total ;
            n_bars = to!size_t(frac*this.barsize) ;

            write("\r") ;
            write(pre_statement) ;
            for(size_t i = 0 ; i < n_bars; i++) write("\u2588") ;
            for(size_t i = n_bars ; i < this.barsize ; i++) write(" ") ;
            writef("| %.2f ", frac*100) ;
            write("%") ;

            stdout.flush() ;

            this.last_update_time = t ;
        }
    }

    void halt(Real count)
    // print progress upon unscheduled termination 
    {
        Real frac ;
        size_t n_bars ;

        // update the progress bar
        frac = count*r_total ;
        n_bars = to!size_t(frac*this.barsize) ;

        write("\r") ;
        write(pre_statement) ;
        for(size_t i = 0 ; i < n_bars; i++) write("\u2588") ;
        for(size_t i = n_bars ; i < this.barsize ; i++) write(" ") ;
        writef("| %.2f ", frac*100) ;
        write("%\n") ;

        stdout.flush() ;
    }
}

class ConvergenceProgress
/**
Keeps a running display of the convergence to steady state progress and time
taken
**/
{
    const Real target_residual ;
    const string pre_statement = "Iteration to steady state: " ;
    const size_t n_dots ;
    const size_t screen_width ;

    this(const Real target_residual)
    {
        this.target_residual = target_residual ;

        winsize ws ;
        
        // get screen size
        if(ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) != -1) 
        {
            import std.algorithm.comparison: max ;
            screen_width = max(ws.ws_col, 0) ;
            n_dots = max(screen_width - pre_statement.length - 12, 0)  ;
        }
    }

    void update(Real residual, Real psuedotime)
    {
        write("\r") ;
        foreach(i ; 0..screen_width) write(" ") ;
        write("\r") ;
        write(pre_statement) ;
        writef("psuedotime elapsed=%.3f, residual/target_residual = %.3g/%.3g",
            psuedotime, residual, target_residual) ;

        stdout.flush() ;
    }

    void complete(Real real_time, Real psuedotime)
    {
        write("\r") ;
        foreach(i ; 0..screen_width) write(" ") ;
        write("\r") ;
        write(pre_statement) ;
        writef("converged in %.3g real seconds, %.3g pseudo seconds", real_time, 
            psuedotime) ;
        write("\n") ;
        stdout.flush() ;
    }

    void halt()
    {
        write("\r") ;
        write(pre_statement) ;

        foreach(i ; 0..n_dots) write(".") ;
        write(" [FAILED]") ;
        write("\n") ;
        stdout.flush() ;
    }
}
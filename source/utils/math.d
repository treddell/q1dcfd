module q1dcfd.utils.math ;

/// Generic square function
pure nothrow T square(T)(T val) { return val*val ;}

/// Generic cube function
pure nothrow T cube(T)(T val) { return val*val*val ;}

/// signum 'sign' function
pure nothrow T sign(T)(T val) { return val < 0 ? -1 : 1 ; }

// Workaround until overhaul is complete (TODO: Delete)
public import q1dcfd.types: MathFunction;

/**
 * A default MathFunction placeholder
 */
final class Unassigned: MathFunction
{
    private enum ERR = "Attempt to evaluate method of unassigned boundary function" ;

    pure override nothrow
    {
        double evaluate(const double x) {throw new Error(ERR) ;}
        MathFunction derivative() {throw new Error(ERR) ;}
        MathFunction indefinite_integral() {throw new Error(ERR);}

        // Is the Unassigned placeholder
        @property bool not_null() {return false ;}
    }
}

/**
 * An expression of the form
 *
 * y(x) = a0 x^(b0) + a1 x^(b1) + ...
 *
 * for provided real valued constants [a0, a1, ...]
 *
 * and real valued indicies [b0, b1, ...]
 */
class ExponentialPolynomial: MathFunction
{
    const double[] coeffs ;
    const double[] indices ;

    this(const double[] coeffs, const double[] indices) pure nothrow
    in
    {
        assert(coeffs.length == indices.length)   ;
    }
    do
    {
        this.coeffs = coeffs ;
        this.indices = indices ;
    }

    override pure nothrow double evaluate(const double x)
    {
        import std.range: zip ;
        import std.math: pow ;

        double result = 0.0 ;

        // LDC 1.8, 1.9 compatability, TODO: review in future
        version(LDC)
        {
            foreach(i ; 0..coeffs.length)
            {
                result += coeffs[i]*x.pow(indices[i]) ;
            }
        }
        else
        // DMD 2.0.82 allows this block
        {
            foreach(coeff, index ; zip(coeffs, indices))
            {
                result += coeff*x.pow(index) ;
            }
        }
        return result ;
    }

    override pure nothrow ExponentialPolynomial derivative()
    {
        import std.range: zip ;
        double[] new_coeffs ;
        double[] new_indices ;

        foreach(immutable i; 0..coeffs.length)
        {
            if(indices[i] == 0.0) continue ;
            new_coeffs ~= coeffs[i]*indices[i] ;
            new_indices ~= indices[i] - 1 ;
        }

        return new ExponentialPolynomial(new_coeffs, new_indices) ; 
    }

    override pure nothrow ExponentialPolynomial indefinite_integral()
    {
        auto new_coeffs = new double[](coeffs.length) ;
        auto new_indices = new double[](indices.length) ;

        foreach(immutable i ; 0..coeffs.length)
        {
            new_indices[i] = indices[i] + 1 ;
            new_coeffs[i] = coeffs[i]/new_indices[i] ;
        }

        return new ExponentialPolynomial(new_coeffs, new_indices) ;
    }
}

/** 
 * data structure for a polynomial, loads coefficients into memory
 * Then evaluates with poly.evaluate(x)
 * Initialize with:
 *     double coeffs: [a0, a1, a2,...] = coefficients of each term of the
 *     polynomial: a0 + a1*x + a2*x^2 + ...
 */
class Polynomial: MathFunction
{
    const double[] coeffs ;

    this(const double[] coeffs) pure nothrow
    {
        this.coeffs = coeffs ;
    }

    // evaluate the polynomial at x
    override pure nothrow double evaluate(const double x)
    {
        double result = coeffs[0] ;
        double x_pow = 1.0 ;

        foreach(immutable coeff ; coeffs[1..coeffs.length])
        {
            x_pow *= x ;
            result += coeff* x_pow ;
        }

        return result ;
    }

    // Returns the derivative of the polynomial as a new struct
    override pure nothrow Polynomial derivative()
    {
        if(coeffs.length == 1)
        {
            return new Polynomial([0.0]) ;
        }

        auto new_coeffs = new double[](coeffs.length - 1) ;

        foreach(immutable i ; 1..coeffs.length)
        {
            new_coeffs[i-1] = i*coeffs[i] ;
        }

        return new Polynomial(new_coeffs) ; 
    }

    override pure nothrow Polynomial indefinite_integral()
    {
        auto new_coeffs = new double[](coeffs.length + 1) ;
        new_coeffs[0] = 0.0 ;

        foreach(immutable i ; 1..new_coeffs.length)
        {
            new_coeffs[i] = coeffs[i-1]/i ;
        }

        return new Polynomial(new_coeffs) ;
    }
}

/**
 *  A function defined in terms of piecewise subfunctions
 *  Initialize with:
 *  pieces: list of function objects
 *  intervals: list of upper domain limit for each function in segments.
 *      The lower limit of the first function is assumed to be zero, the
 *      upper limit of the last function must be long enough to account
 *      for the desired simulation time
 */
class PiecewiseFunction: MathFunction
{
    MathFunction[] pieces ;
    const double[] intervals ;

    this(MathFunction[] pieces, const double[] intervals) pure nothrow
    in 
    {
        import std.algorithm.sorting: isStrictlyMonotonic ;
        assert(pieces.length == intervals.length, "Number of function pieces"
            ~ " does not match number of intervals") ;

        assert(intervals[0] >= 0, "First interval endpoint must be non-negative") ;
        assert(intervals.isStrictlyMonotonic, "Intervals must be strictly monotonic") ;
    }
    do
    {
        this.pieces = pieces ;
        this.intervals = intervals ;
    }

    override pure nothrow double evaluate(const double x)
    // evaluate function at x, performs a binary search to find correct
    // interval
    {
        import std.range ;
        auto n_interval = intervals.assumeSorted.lowerBound(x).length ;
        n_interval = n_interval == pieces.length ? pieces.length - 1 : n_interval ;
        return pieces[n_interval].evaluate(x) ;
    }

    override pure nothrow PiecewiseFunction derivative()
    // Return the derivative of the function
    {
        auto new_segments = new MathFunction[](pieces.length) ;

        foreach(immutable i, func ; pieces)
        {
            new_segments[i] = func.derivative ;
        }

        return new PiecewiseFunction(new_segments, intervals.dup) ;
    }

    override pure nothrow PiecewiseFunction indefinite_integral()
    {
        auto new_segments = new MathFunction[](pieces.length) ;

        foreach(immutable i, func ; pieces)
        {
            new_segments[i] = func.indefinite_integral ;
        }

        return new PiecewiseFunction(new_segments, intervals.dup) ;
    }

    /**
     * Adds up the integrals over each piecewise region
     */
    override pure nothrow double integrate(const double xa, const double xb)
    {
        import std.algorithm.comparison: max, min ;

        double total = 0.0 ;

        foreach(immutable i, func ; pieces)
        {
            auto interval_start = (i == 0) ? 0.0 : intervals[i-1] ;

            if(interval_start > xb) continue ;
            if(intervals[i] < xa) continue ;

            auto a = max(interval_start, xa) ;
            auto b = min(intervals[i], xb) ;

            total += func.integrate(a, b) ;
        }   

        return total ;
    }
}

/** Defines a step input of the form:
 *
 * f(x) =  { y1 , x < x_step ,
 * { y2 , x >= x_step
 */
final class Step: PiecewiseFunction
{
    this(const double y1, const double y2, const double x_step) pure nothrow
    {
        super([new Polynomial([y1]), new Polynomial([y2])], 
            [x_step, double.infinity]) ;
    }
    
    this(const double[] args) pure nothrow
    // Alternate constructor, input arguments as array
    in 
    {
        assert(args.length == 3) ;
    }
    do
    {
        this(args[0], args[1], args[2]) ;
    }
}

final class DoubleStep: PiecewiseFunction
{
    this(const double y1,
         const double y2,
         const double y3,
         const double x12,
         const double x23
         ) pure nothrow
    {
        super([new Polynomial([y1]), new Polynomial([y2]), new Polynomial([y3])], 
            [x12, x23, double.infinity]);
    }

    this(const double[] args) pure nothrow
    // Alternate constructor, input arguments as array
    in 
    {
        assert(args.length == 5) ;
    }
    do
    {
        this(args[0], args[1], args[2], args[3], args[4]);
    }
}

final class TripleStep: PiecewiseFunction
{
    this(const double y1,
         const double y2,
         const double y3,
         const double y4,
         const double x12,
         const double x23,
         const double x34
         ) pure nothrow
    {
        super([new Polynomial([y1]), new Polynomial([y2]), new Polynomial([y3]),
                new Polynomial([y4])],
            [x12, x23, x34, double.infinity]);
    }

    this(const double[] args) pure nothrow
    // Alternate constructor, input arguments as array
    in 
    {
        assert(args.length == 7) ;
    }
    do
    {
        this(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
    }
}

/**
 * Defines a continuous linear ramp between (x0,y0) and (x1, y1)
 *
 *        { y0                                  , x <= x0
 * y(x) = { y0 + (y1 - y0)/(x1 - x0) * (x - x0) , x0 < x < x1
 *        { y1                                  , x >= x1
 *
 * Continuity of the derivatives is not guaranteed at corners of
 * intervals, CubicRamp is suggested for most situations
 */
final class LinearRamp: PiecewiseFunction
{
    this(const double x0, const double y0, const double x1, const double y1) pure
    {
        // coefficients of the linear segment
        auto coeffs = [y0 - (y1 - y0)/(x1 - x0)*x0, (y1 -y0)/(x1-x0)] ;

        super([new Polynomial([y0]), new Polynomial(coeffs), new Polynomial([y1])],
              [x0, x1, double.infinity]) ;
    }

    // alternate array constructor
    this(const double[] args) pure
    in
    {
        assert(args.length == 4) ;
    }
    do

    {
        this(args[0], args[1], args[2], args[3]) ;
    }
}

/** Defines a C^1 smooth cubic ramp function of the form
 *
 *            { y0                    , x <= x0
 *    y(x) =  { ax^3 + bx^2 + cx + d  , x0 < x < x1
 *            { y1                    , x > x1
 *
 *    With smoothness conditions
 *
 * lim x->x0 {y} = y0
 * lim x->x1 {y} = y1
 * lim x->x0 {y'} = 0
 * lim x->x1 {y'} = 0
 */
final class CubicRamp: PiecewiseFunction
{
    this(const double x0, const double y0, const double x1, const double y1) pure
    {
        import q1dcfd.numerics.linear_solve: gauss ;

        immutable double[][] A = [
            [1, x0, x0.square, x0.cube    ],
            [0, 1 , 2*x0     , 3*x0.square],
            [1, x1, x1.square, x1.cube    ],
            [0, 1 , 2*x1     , 3*x1.square]
        ] ;

        immutable double[] b = [y0, 0, y1, 0] ;

        auto coeffs = gauss(A, b) ;
        super(
            [new Polynomial([y0]),
             new Polynomial(coeffs), 
             new Polynomial([y1])],
            [x0, x1, double.infinity]) ;
    }

    // alternate constructor, input arguments as array
    this(const double[] args) pure
    in 
    {
        assert(args.length == 4) ;
    }
    do
    {
        this(args[0], args[1], args[2], args[3]) ;
    }
}

/**
 * Fits a square root curve between two points, useful for calculating
 * variable hydraulic diameters of linear area changes etc
 *
 * Fits a model of the form
 *
 *        { y1            , x <= x1
 * y(x) = { a sqrt(x) + b , x1 < x < x2
 *        { y2            , x>= x2
 *
 * for parameters (a,b) to provided points (x1, y1), (x2, y2)
 */
final class SqrtRamp: PiecewiseFunction
{
    this(const double x1, const double y1, const double x2, const double y2)
    in
    {
        assert(x1 >= 0, "Negative x1 provided for square root fit") ;
        assert(x2 > x1, "x2 must be strictly > x1") ;
    }
    do
    {
        import std.math: sqrt ;

        auto determinant = (sqrt(x1) - sqrt(x2)) ;
        auto a1 = (y1 - y2)/determinant ;
        auto a2 = (- sqrt(x2)*y1 + sqrt(x1)*y2)/determinant ;

        super(
            [new Polynomial([y1]),
             new ExponentialPolynomial([a1, a2], [0.5, 0.0]),
             new Polynomial([y2])],
            [x1, x2, double.infinity]) ;
    }

    // alternative constructor, input arguments as array
    this(const double[] args)
    in
    {
        assert(args.length == 4) ;
    }
    do
    {
        this(args[0], args[1], args[2], args[3]) ;
    }
}

/**
 * Fit a cubic spline through a set of x,y training data with natural
 * boundary conditions
 */
final class CubicSpline: PiecewiseFunction
{
    this(double[] x, double[] y) pure
    in 
    {
        assert(x.length == y.length,"mismatched x,y data") ;
        assert(x.length > 3) ;
    }
    do 
    {
        auto h = new double[](x.length - 1) ;
        auto A = new double[][](x.length -2, x.length - 2) ;
        auto b = new double[](x.length - 2) ;

        h[] = x[1..$] - x[0..$-1] ;
        foreach(ref col ; A) col[] = 0.0 ; // zero A matrix

        foreach(immutable i ; 0..b.length)
        {
            if(i==0)
            {
                A[i][i] = 2*(h[i] + h[i+1]) ;
                A[i][i+1] = h[i+1] ;
            }
            else if(i==b.length - 1)
            {
                A[i][i-1] = h[i-1] ;
                A[i][i] = 2*(h[i] + h[i+1]) ;
            }
            else
            {
                A[i][i-1] = h[i-1] ;
                A[i][i] = 2*(h[i] + h[i+1]) ;
                A[i][i+1] = h[i+1] ;
            }

            b[i] = 6.0/h[i+1]*(y[i+2] - y[i+1]) - 6.0/h[i]*(y[i+1] - y[i]) ;
        }

        import q1dcfd.numerics.linear_solve: gauss ;
        auto sol = gauss(A, b) ;
        auto sigma = new double[](sol.length + 2) ;
        sigma[0] = 0.0 ;
        sigma[1..$-1] = sol[] ;
        sigma[$-1] = 0.0 ;

        // set up the coefficient vectors
        auto c0 = new double[](y.length) ;
        auto c1 = new double[](y.length) ;
        auto c2 = new double[](y.length) ;
        auto c3 = new double[](y.length) ;

        c0[] = y[] ;
        c1[0..$-1] = (y[1..$] - y[0..$-1])/h[0..$] 
            - (2*sigma[0..$-1] + sigma[1..$]) * h[0..$]/6.0 ;
        c2[] = 0.5*sigma[] ;
        c3[0..$-1] = (sigma[1..$] - sigma[0..$-1])/(6.0*h[0..$]) ;

        c1[$-1] = 0.0 ;
        c3[$-1] = 0.0 ;

        // reshape into standard power series form
        auto temp1 = new double[](y.length) ;
        auto temp2 = new double[](y.length) ;
        auto temp3 = new double[](y.length) ;

        temp1[] = x[]*c2[] ;
        temp2[] = x[]*c3[] ;
        temp3[] = x[]*temp2[] ;

        auto p0 = new double[](y.length) ;
        auto p1 = new double[](y.length) ;
        auto p2 = new double[](y.length) ;
        auto p3 = new double[](y.length) ;

        p0[] = c0[] - x[]*c1[] + x[]*temp1[] - x[]*temp3[] ;
        p1[] =            c1[] - 2.0*temp1[] + 3.0*temp3[] ;
        p2[] =                          c2[] - 3.0*temp2[] ;
        p3[] =                                        c3[] ;
  
        // generate a polynomial for each segment
        auto segments = new MathFunction[](y.length-1) ;
        foreach(immutable i ; 0..y.length-1)
        {
            segments[i] = new Polynomial([p0[i], p1[i], p2[i], p3[i]]) ;
        }
        super(segments, x[1..$]) ;
    }
}

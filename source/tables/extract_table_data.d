module q1dcfd.tables.extract_table_data ;

import std.stdio: File, writeln ;
import std.array: join, split, replace ;
import std.math: isNaN ;
import std.string ;
import std.conv: to ;
import std.algorithm.searching: startsWith, endsWith ;
import std.path: dirSeparator ;

/**
 * An ordered data structure which contains the data and meta-data of 
 * a fluids tabular taylor series extrapolation data set
 */
final class TableData 
{    
    // meta data
    immutable
    {
        string i1_str ; // first input variable
        string i2_str ; // second input variable
        string fluid_str ; // fluid type
        double i1_min ; // lowest i1 (inclusive) which may be estimated
        double i1_max ; // largest i1 (non-incl) which may be estimated
        double i2_min ; // lowest i2 (inclusive) which may be estimated
        double i2_max ; // largest i2 (non-incl) which may be estimated
        uint n_variables ; // number of variables calculated
        uint n1 ; // number of intervals along input 1 axis
        uint n2 ; // number of intervals along input 2 axis
        uint n_columns ; // number of data columns
        string[] output_vars ; // array of output variables

        double[] data ; // the tabular data

        // Derived attributes
        double i1_step ;
        double i2_step ;
        double i1_step_rec ;
        double i2_step_rec ;
        double i1_lowest ; // lowest exact i1 data point
        double i1_largest ; // largest exact i1 data point
        double i2_lowest ; // lowest exact i2 data point
        double i2_largest ; // largest exact i2 data point
}
    this(string data_folder)  
    out
    {
        assert(this.n1 > 0) ;
        assert(this.n2 > 0) ;
        assert(this.n_variables > 0) ;
        assert(this.n_columns > this.n_variables) ;
        assert(this.i1_min < this.i1_max) ;
        assert(this.i2_min < this.i2_max) ;
        assert(this.i1_str !is "") ;
        assert(this.i2_str !is "") ;
        assert(this.fluid_str !is "") ;
        assert(this.output_vars.length == this.n_variables) ;
        assert(!this.i1_min.isNaN) ;
        assert(!this.i2_min.isNaN) ;
        assert(!this.i1_max.isNaN) ;
        assert(!this.i2_max.isNaN) ;
    }   
    do
    {

        string fluid_str ;
        string i1_str ;
        string i2_str ;
        double i1_min ;
        double i2_min ;
        double i1_max ;
        double i2_max ;
        uint n1 ;
        uint n2 ;
        uint n_variables ;
        uint n_columns ;
        string[] output_vars ;

        // make sure there is a path separator on the end
        if(!data_folder.endsWith(dirSeparator)) 
        {
            data_folder = data_folder ~ dirSeparator ;
        }

        // Read the meta file
        string meta_filename = [data_folder, "meta.txt"].join() ;
        auto meta_file = File(meta_filename, "r") ;
        auto lines = meta_file.byLine() ;

        foreach(line ; lines) 
        {
            if(line.startsWith("fluid")) 
            {
                fluid_str = line
                    .split(":")[1]
                    .strip
                    .replace(" ","")
                    .to!string ;
            } 
            else if(line.startsWith("input 1 var")) 
            {
                i1_str = line
                    .split(":")[1]
                    .strip
                    .to!string ;
            } 
            else if(line.startsWith("input 2 var")) 
            {
                i2_str = line
                    .split(":")[1]
                    .strip
                    .to!string ;
            } 
            else if(line.startsWith("input 1 min")) 
            {
                i1_min = line
                    .split(":")[1]
                    .strip
                    .to!double ;
            } 
            else if(line.startsWith("input 2 min")) 
            {
                i2_min = line
                    .split(":")[1]
                    .strip
                    .to!double ;
            }
            else if(line.startsWith("input 1 max")) 
            {
                i1_max = line
                    .split(":")[1]
                    .strip
                    .to!double ;
            }
            else if(line.startsWith("input 2 max")) 
            {
                i2_max = line
                    .split(":")[1]
                    .strip
                    .to!double ;
            }
            else if(line.startsWith("input 1 N")) 
            {
                n1 = line
                    .split(":")[1]
                    .strip
                    .to!uint ;
            }
            else if(line.startsWith("input 2 N")) 
            {
                n2 = line
                    .split(":")[1]
                    .strip
                    .to!uint ;
            }
            else if(line.startsWith("output variable count")) 
            {
                n_variables = line
                    .split(":")[1]
                    .strip
                    .to!uint ;
            }
            else if(line.startsWith("output column count")) 
            {
                n_columns = line
                    .split(":")[1]
                    .strip
                    .to!uint ;
            }
            else if(line.startsWith("output variables")) 
            {
                string temp_string = line
                    .split(":")[1]
                    .strip
                    .to!string ;

                output_vars = temp_string
                    .replace("(","")
                    .replace(")","")
                    .replace(" ","")
                    .replace("'","")
                    .split(",") ;

                if(output_vars[$ -1] == "")
                {
                    output_vars = output_vars[0..$-1] ;
                }
            }
        }

        // Assign local variables to immutable state since this cannot be done
        // inside the loop
        this.fluid_str = fluid_str ;
        this.i1_str = i1_str ;
        this.i2_str = i2_str ;
        this.i1_min = i1_min ;
        this.i1_max = i1_max ;
        this.i2_min = i2_min ;
        this.i2_max = i2_max ;
        this.n1 = n1 ;
        this.n2 = n2 ;
        this.n_variables = n_variables ;
        this.n_columns = n_columns ;
        this.output_vars = cast(immutable string[]) output_vars ;

        // Derived values assigned directly to immutable state
        this.i1_step = (this.i1_max - this.i1_min)/to!double(this.n1) ;
        this.i2_step = (this.i2_max - this.i2_min)/to!double(this.n2) ;
        this.i1_step_rec = 1.0/this.i1_step ;
        this.i2_step_rec = 1.0/this.i2_step ;
        this.i1_lowest = this.i1_min + 0.5*i1_step ;
        this.i1_largest = this.i1_max - 0.5*i1_step ;
        this.i2_lowest = this.i2_min + 0.5*i2_step ;
        this.i2_largest = this.i2_max - 0.5*i2_step ;

        ulong len = n1 * n2 * n_columns;
        double[] data_arr;
        data_arr.length = len;

        // Now read the data file and import the raw data
        string data_filename = [data_folder, "data.b"].join() ;  
        auto data_file = File(data_filename, "r") ;
        this.data = cast(immutable) 
            data_file.rawRead(data_arr) ;
            // data_file.rawRead(new double[](n1*n2*n_columns)) ;
    }
}

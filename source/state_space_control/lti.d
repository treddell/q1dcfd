module q1dcfd.state_space_control.lti;

import q1dcfd.config: Real;
import q1dcfd.state_space_control.expm: expm;

import mir.ndslice;
import lubeck;

alias odarr = Slice!(Real*, 1);
alias tdarr = Slice!(Real*, 2);

/*
 * Wishlist:
 * - Set visibility attributes for variables.
 * - Ideally, many members would be immutable (might allows some useful compiler
 *   optimizations). To enable this, would have to parse controller jobfile
 *   during state_space_controller initialization. This might be the way to go
 *   if I continue to work on this code in the future.
 */

/*
 * Continuous and discrete time LTI model for linearisation about nonequilibrium
 * points.
 *
 * Some notes:
 *  - C is Cz: it gives only the controlled outputs (and dk is dkz)
 *  - C is just a copy of Cy, with only the rows corresponding to the controlled
 *    outputs.
 *  - ZHatk is the predicted values of the next-timestep controlled outputs
 *  - dk is the current estimate of the output disturbance vector
 *  - We have UBarkm1 not UBar since U and UBar are not known till after the MPC
 *    update. Currently, this is always 0 since we relinearise at each step.
 *  - Currently don't need to worry about ZTilde, this is only used to calculate
 *    output disturbance.
 *  - ZHatk is already calculated in the QP code.
 *  - UBarkm1 and XBar are currently always zero due to relinearization.
 */
class Lti
{
    import std.math: sqrt;
    import std.conv: to;

    const uint n_states, n_inputs, n_outputs, n_cntrl_outs;
    const Real dt;            // Discrete timestep
    tdarr A, B, C, Cy, D, F0, // Continuous time matrices
          Ad, Bd, F0d,        // Discrete time matrices
          Adb, Bdb, F0db,     // Move-blocked discrete time matrices
          dk, Xk, Ukm1, Zk, Uk,
          // Yk,  // (currently just using Z)
          ZRef,   // Target outputs
          ZHatk,  // Predicted values of the next-timestep controlled outputs
          ZTilde, // Prediction error, ZHatk - Zk
          XBar, UBarkm1, ZBar;

    this(
        Real dt,
        uint n_states,
        uint n_inputs,
        uint n_outputs,
        uint n_cntrl_outs)
    {
        this.n_states     = n_states;
        this.n_inputs     = n_inputs;
        this.n_outputs    = n_outputs;
        this.n_cntrl_outs = n_cntrl_outs;
        this.dt           = dt;
        A       = slice!Real(n_states, n_states);
        B       = slice!Real(n_states, n_inputs);
        Cy      = slice!Real(n_outputs, n_states);
        C       = slice!Real(n_cntrl_outs, n_states);
        D       = slice!Real(n_cntrl_outs, n_inputs);
        F0      = slice!Real(n_states, 1);
        Ad      = slice!Real(n_states, n_states);
        Bd      = slice!Real(n_states, n_inputs);
        F0d     = slice!Real(n_states, 1);
        Adb     = slice!Real(n_states, n_states);
        Bdb     = slice!Real(n_states, n_inputs);
        F0db    = slice!Real(n_states, 1);
        dk      = slice!Real(n_cntrl_outs, 1);
        XBar    = slice!Real(n_states, 1);
        Uk      = slice!Real(n_inputs, 1);
        UBarkm1 = slice!Real(n_inputs, 1);
        ZBar    = slice!Real(n_cntrl_outs, 1);
        Xk      = slice!Real(n_states, 1);
        Ukm1    = slice!Real(n_inputs, 1);
        // Yk      = slice!Real(n_outputs, 1);
        Zk      = slice!Real(n_cntrl_outs, 1);
        ZHatk   = slice!Real(n_cntrl_outs, 1);
        ZTilde  = slice!Real(n_cntrl_outs, 1);
        ZRef    = slice!Real(n_cntrl_outs, 1);
        A[]     = 0.0;
        B[]     = 0.0;
        C[]     = 0.0;
        D[]     = 0.0;
        F0[]    = 0.0;
        Ad[]    = 0.0;
        Bd[]    = 0.0;
        F0d[]   = 0.0;
        dk[]    = 0.0;
        // Currently these always stay zero, since using perturbed variables
        XBar[]    = 0.0;
        UBarkm1[] = 0.0;
        ZBar[]    = 0.0;
    }

    void update_ukm1()
    {
        Ukm1[] = Uk[];
    }

    /*
     * Converts continuous-time LTI model defined by matrices A, B, and F0 to
     * discrete-time LTI model defined by matrices Ad, Bd, and F0d.
     * Computes initial Ad and Bd simultaneously via matrix exponential of
     * augmented matrix EM for discretisation timestep.
     * Then computes F0d via same procedure using identity matrix instead of B
     * matrix.
     * n = n_states, m = n_controls
     */
    void c2d()
    {
        // Compute discrete-time model using timestep dt
        // Ad, Bd
        auto EM = slice!Real(n_states+n_inputs, n_states+n_inputs);
        auto M  = slice!Real(n_states+n_inputs, n_states+n_inputs);
        EM[] = 0.0;
        EM[0..n_states, 0..n_states] = A[] * dt;
        EM[0..n_states, n_states..n_states+n_inputs] = B[] * dt;
        M = expm(EM);
        Ad[] = M[0..n_states, 0..n_states];
        Bd[] = M[0..n_states, n_states..n_states+n_inputs];
        // F0d
        auto EN = slice!Real(2*n_states, 2*n_states);
        auto N  = slice!Real(2*n_states, 2*n_states);
        auto I  = slice!Real(n_states, n_states);
        I = eye(n_states);
        EN[] = 0.0;
        EN[0..n_states, 0..n_states] = A[] * dt;
        EN[0..n_states, n_states..2*n_states] = I * dt;
        N = expm(EN);
        F0d[] = mtimes(N[0..n_states, n_states..2*n_states], F0);

        //----------------------------------------------------------------------
        // Compute the move blocked discrete-time model
        // Adb, Bdb
        // double dtb = 0.2; // TODO: Input from MPC file!!
        // EM[] = 0.0;
        // EM[0..n_states, 0..n_states] = A[] * dtb;
        // EM[0..n_states, n_states..n_states+n_inputs] = B[] * dtb;
        // M = expm(EM);
        // Adb[] = M[0..n_states, 0..n_states];
        // Bdb[] = M[0..n_states, n_states..n_states+n_inputs];
        // // F0db
        // EN[] = 0.0;
        // EN[0..n_states, 0..n_states] = A[] * dtb;
        // EN[0..n_states, n_states..2*n_states] = I * dtb;
        // N = expm(EN);
        // F0db[] = mtimes(N[0..n_states, n_states..2*n_states], F0);
    }

    /*
     * Computes matrix exponential of square matrix using scaling and squaring
     * algorithm. Similar to that used in matlab built-in 'expm'.
     */
    tdarr expm_old(tdarr A)
    {
        assert(A.length!0 == A.length!1);

        // Make powers of 2 array [TODO: Shift this to constructor]
        auto pows2 = slice!Real(30);
        pows2[0] = 1;
        foreach(i; 1..30) { pows2[i] = pows2[i-1] * 2; }

        // Scaling step
        auto norm2A = norm_2(A);
        assert(norm2A < pows2[$-1]);
        ulong j;
        foreach(i; 1..pows2.length!0)
        {
            if (pows2[i] > norm2A) { j = i; break; }
        }
        A[] = A[] / pows2[j];

        // Compute Pade approximant then undo scaling
        A = padem(A);
        foreach(k; 0..j) A = mtimes(A, A);

        return A;
    }

    /*
     * Horner evaluation of the [q/q] Pade approximant of square matrix 'A'.
     * Defaults to 13th order approximation.
     */
    tdarr padem(tdarr A, int p=13)
    {
        assert(A.length!0 == A.length!1);
        auto n = A.length!0;

        // Make powers of 2 array
        auto pows2 = slice!Real(30);
        pows2[0] = 1;
        foreach(i; 1..30) { pows2[i] = pows2[i-1] * 2; }

        // Evaluate Pade coefficients [TODO: Put this in constructor]
        auto coeffs = slice!Real(p+1);
        coeffs[0] = 1.0;
        foreach(k; 0..p)
        {
            Real kD = to!Real(k);
            coeffs[k+1] = coeffs[k] * ((p - kD) / ((kD + 1) * (2 * p - kD)));
        }

        auto I  = slice!Real(n, n);
        auto A2 = slice!Real(n, n);
        auto Q  = slice!Real(n, n);
        auto P  = slice!Real(n, n);
        auto E  = slice!Real(n, n);
        I   = eye(to!int(n));
        A2  = mtimes(A, A);
        Q[] = pows2[p] * I[];
        P[] = pows2[p-1] * I[];
        auto odd = 1;

        foreach_reverse(k; 0..p-1)
        {
            if (odd == 1) Q[] = mtimes(Q, A2) + coeffs[k] * I[];
            else P[] = mtimes(P, A2) + coeffs[k] * I[];
            odd = 1 - odd;
        }
        if (odd == 1)
        {
            Q = mtimes(Q, A);
            Q[] = Q[] - P[];
            E[] = -1 * (I[] + 2 * mldivide(Q, P));
        }
        else
        {
            P = mtimes(P, A);
            Q[] = Q[] - P[];
            E[] = I[] + 2 * mldivide(Q, P);
        }
        return E;
    }

    /*
     * Computes 2-norm of square matrix 'A'.
     * 2-norm of a matrix is defined as the square root of the maximum eigenvalue of
     * A'A.
     */
    Real norm_2(tdarr A)
    {
        assert(A.length!0 == A.length!1);
        auto ATA = slice!Real(A.length!0, A.length!0);
        ATA = mtimes(A.transposed, A);
        auto eigs = eigSymmetric('L', ATA);
        // Real norm2A = sqrt(eigs.values.max_val);
        Real norm2A = sqrt(max_odarr(eigs.values));
        return norm2A;
    }

    /* Makes an ndslice identity matrix of size n */
    tdarr eye(ulong n)
    {
        auto I = slice!Real(n, n);
        I[] = 0.0;
        I.diagonal[] = 1.0;
        return I;
    }

    /* Find max value of 1D ndslice array of Reals */
    Real max_odarr(odarr arr)
    {
        auto max = arr[0];
        foreach(i; 1..arr.length!0) { if (arr[i] > max) { max = arr[i]; } }
        return max;
    }

    void print_lti_matrices()
    {
        import std.stdio: writeln;
        // Continuous
        writeln("\nA:");
        foreach(i; 0..A.length!0) writeln(A[i, 0..$]);
        writeln("\nB:");
        foreach(i; 0..B.length!0) writeln(B[i, 0..$]);
        writeln("\nC:");
        foreach(i; 0..C.length!0) writeln(C[i, 0..$]);
        writeln("\nD:");
        foreach(i; 0..D.length!0) writeln(D[i, 0..$]);
        writeln("\nF0:");
        foreach(i; 0..F0.length!0) writeln(F0[i, 0..$]);
        // Discrete
        writeln("\n\nAd:");
        foreach(i; 0..Ad.length!0) writeln(Ad[i, 0..$]);
        writeln("\nBd:");
        foreach(i; 0..Bd.length!0) writeln(Bd[i, 0..$]);
        writeln("\nF0d:");
        foreach(i; 0..F0d.length!0) writeln(F0d[i, 0..$]);
        // States, outputs, controls
        writeln("\n\nX = ");
        writeln(Xk[0..$, 0], "\n");
        writeln("\nUkm1 = ");
        writeln(Ukm1[0..$, 0], "\n");
        writeln("\nZ = ");
        writeln(Zk[0..$, 0], "\n");
    }
}


module q1dcfd.state_space_control.correlations;

import q1dcfd.config: Real;

NuCorrelation generate_nu_correlation(string correlation_name)
{
    if(correlation_name == "Ngo")
        return new Ngo();
    else if(correlation_name == "Laminar")
        return new Laminar();
    else
    {
        import std.format;
        throw new Exception(format!("Invalid Nusselt number correlation %s.")
                (correlation_name));
    }
}

abstract class NuCorrelation
{
    Real calculate_nu(const Real Re, const Real Pr);
}

final class Ngo : NuCorrelation
{
    override Real calculate_nu(const Real Re, const Real Pr)
    {
        import std.math: pow;
        return 0.1696 * Re.pow(0.629) * Pr.pow(0.317);
    }
}

final class Laminar : NuCorrelation
{
    override Real calculate_nu(const Real Re, const Real Pr)
    {
        return 4.089;
    }
}


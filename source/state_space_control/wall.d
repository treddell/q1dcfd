module q1dcfd.state_space_control.wall;

import q1dcfd.state_space_control.observer: IObserver;
import q1dcfd.state_space_control.lti: Lti;

/*
 * Wishlist:
 * - Currently the global state indices for the 'flow source' components (i.e.
 *   pumps or compressors) must be specified manually.
 *   I can't think of a neat way around this, but would be nice to implement if
 *   I do find one. [I think I changed this and it's now automatic?]
 * - Split the 'process_observer_array' function into smaller parts.
 * - The hot_flow_source and cold_flow_source objects are really observers (not
 *   flow sources), should change name to reflect this.
 */

class WallDef
{
    string name;
    string[] observers;
    string[string] flow_sources;

    this(string name, string[] observers, string[string] flow_sources)
    {
        foreach(key; flow_sources.byKey)
        {
            import std.format;
            import std.algorithm: canFind;
            if(!(canFind(["hot_flow_source", "cold_flow_source"], key)))
            {
                throw new Exception(format!(
                    "ERROR: Invalid entry %s in flow_sources in WallDef %s. "
                    ~ "Keys in 'flow_sources' should be 'hot_flow_source' "
                    ~ "and/or 'cold_flow_source'")(key, name));
            }
        }

        this.name = name;
        this.observers = observers;
        this.flow_sources = flow_sources;
    }
}

Wall build_wall(
    WallDef def,
    IObserver[string] observer_map)
{
    auto wall_obs_array = new IObserver[](def.observers.length);
    foreach(i, observer_name; def.observers)
    {
        if(!(observer_name in observer_map))
        {
            import std.format;
            throw new Exception(format!(
                "Observer %s not found in observer_map, check that this observer"
                ~ " is defined in the jobfile.")(observer_name));
        }
        wall_obs_array[i] = observer_map[observer_name];
    }

    IObserver[string] flow_source_dict;
    if("hot_flow_source" in def.flow_sources)
        flow_source_dict["hot"] = observer_map[def.flow_sources["hot_flow_source"]];
    if("cold_flow_source" in def.flow_sources)
        flow_source_dict["cold"] = observer_map[def.flow_sources["cold_flow_source"]];

    return new Wall(def.name, wall_obs_array, flow_source_dict);
}

/*
 * Stores all thermo properties and state variables for a wall object.
 * Req'd to interface with observers.
 */
class WallProps
{
    import q1dcfd.config: Real;
    import q1dcfd.state_space_control.metaprogramming: InitPderArrays,
           InitPublicPropArrays;

    enum wall_props = ["k_hot", "T_hot", "Nu_hot", "k_cold", "T_cold",
            "Nu_cold", "T_wall"];
    // Make partial derivative variables dkde_hot, dkde_cold, etc
    enum pder_props     = ["k", "T", "Nu"];
    enum pder_vars_hot  = ["e_hot", "p_hot", "mdot_hot"];
    enum pder_vars_cold = ["e_cold", "p_cold", "mdot_cold"];
    mixin(InitPublicPropArrays!(wall_props));
    mixin(InitPderArrays!(pder_props, pder_vars_hot));
    mixin(InitPderArrays!(pder_props, pder_vars_cold));

    this(uint n_cells)
    {
        mixin(InitPublicPropArrays!(wall_props, "n_cells"));
        mixin(InitPderArrays!(pder_props, pder_vars_hot, "n_cells"));
        mixin(InitPderArrays!(pder_props, pder_vars_cold, "n_cells"));
    }
}

/*
 * Used to form LTI model of a 'wall' component.
 * Analogous to 'stream', but for sequential solid elements.
 */
class Wall
{
    import q1dcfd.config: Real;
    import q1dcfd.state_space_control: ChannelObserver, WallObserver;

public:
    WallProps props;
    immutable string wall_name;

protected:
    IObserver[] observer_array;
    uint[2][string] wall_map;
    uint[string][] wall_to_global; // maps prop & wall index to global state index
    struct Geo { Real[] a_wall, cp, rho, n_channels_hot, n_channels_cold; }
    Geo geo;
    immutable uint n_cells;
    bool[] hot_chan_connected;
    bool[] cold_chan_connected;
    int i_p_hot     = -1;
    int i_mdot_hot  = -1;
    int i_p_cold    = -1;
    int i_mdot_cold = -1;

public:
    this(string wall_name, ref IObserver[] observer_array,
            IObserver[string] flow_source_dict)
    {
        this.wall_name = wall_name;
        populate_flow_source_indices(flow_source_dict);
        this.observer_array = observer_array;
        auto n_cells_wall = process_observer_array(observer_array);
        n_cells = n_cells_wall;
        props = new WallProps(n_cells);
        populate_geo(observer_array);
    }

    void update(Lti lti)
    {
        foreach(observer; observer_array)
            observer.write_to_wall_prop_vectors(wall_name, props);
        update_lti_model(lti);
    }

protected:
    void populate_flow_source_indices(IObserver[string] flow_source_dict)
    {
        if("hot" in flow_source_dict)
        {
            i_p_hot    = flow_source_dict["hot"].global_index(0, "p_out");
            i_mdot_hot = flow_source_dict["hot"].global_index(0, "mdot");
        }
        if("cold" in flow_source_dict)
        {
            i_p_cold    = flow_source_dict["cold"].global_index(0, "p_out");
            i_mdot_cold = flow_source_dict["cold"].global_index(0, "mdot");
        }
    }

    /* Checks input 'observer_array' is valid and populates wall indices */
    uint process_observer_array(IObserver[] observer_array)
    {
        // Iterate through components and populate wall indices
        uint index_start = 0;
        foreach(i, observer; observer_array)
        {
            // Check input array is OK
            import std.format;
            if(observer.type != "WallObserver")
            {
                throw new Exception(format!(
                    "ERROR: All components in a wall must be of types "
                    ~ "WallObserver, but a component of wall %s is of type %s.")(
                    wall_name, observer.type));
            }
            auto wall_obs = cast(WallObserver) observer;
            // Check correct flow_source indices have been provided
            if(wall_obs.hot_channel_connected)
            {
                if(i_p_hot < 0 || i_mdot_hot < 0)
                    throw new Exception(format!(
                        "ERROR: Component %s of Wall %s is connected to a hot "
                        ~ "channel, but hot channel pressure and mass flow rate "
                        ~ "indices were not provided")(observer.name, wall_name));
            }
            if(wall_obs.cold_channel_connected)
            {
                if(i_p_cold < 0 || i_mdot_cold < 0)
                    throw new Exception(format!(
                        "ERROR: Component %s of Wall %s is connected to a cold "
                        ~ "channel, but cold channel pressure and mass flow rate "
                        ~ "indices were not provided")(observer.name, wall_name));
            }

            // Add the wall indexing information
            observer.populate_wall_indices(wall_name, index_start);
            wall_map[observer.name] = [
                observer.wall_istart(wall_name),
                observer.wall_iend(wall_name)];

            // Form mapping from wall index to global state indices
            wall_to_global.length += (observer.n_cells);
            uint k = 0; // Observer cell index
            foreach(j; observer.wall_istart(wall_name)..
                        observer.wall_iend(wall_name) + 1)
            {
                wall_to_global[j]["T_wall"] = observer.global_index(k, "T");
                if(wall_obs.hot_channel_connected)
                {
                    if(wall_obs.hot_channel_reversed)
                        wall_to_global[j]["e_hot"] = wall_obs.hot_channel.global_index(
                                wall_obs.n_cells - 1 - k, "e");
                    else
                        wall_to_global[j]["e_hot"] =
                            wall_obs.hot_channel.global_index(k, "e");
                }
                if(wall_obs.cold_channel_connected)
                {
                    if(wall_obs.cold_channel_reversed)
                        wall_to_global[j]["e_cold"] = wall_obs.cold_channel.global_index(
                                wall_obs.n_cells - 1 - k, "e");
                    else
                        wall_to_global[j]["e_cold"] =
                            wall_obs.cold_channel.global_index(k, "e");
                }
                k++;
            }

            // Flag connections to channels if they exist
            auto hot_chan_connected_next = new bool[](observer.n_cells);
            if(wall_obs.hot_channel_connected) hot_chan_connected_next[] = true;
            else                               hot_chan_connected_next[] = false;
            hot_chan_connected ~= hot_chan_connected_next;

            auto cold_chan_connected_next = new bool[](observer.n_cells);
            if(wall_obs.cold_channel_connected) cold_chan_connected_next[] = true;
            else                                cold_chan_connected_next[] = false;
            cold_chan_connected ~= cold_chan_connected_next;

            index_start = observer.wall_iend(wall_name) + 1;
        }
        return index_start;
    }

    void populate_geo(IObserver[] observer_array)
    {
        geo.a_wall.length = n_cells;
        geo.cp.length     = n_cells;
        geo.rho.length    = n_cells;
        geo.n_channels_hot.length  = n_cells;
        geo.n_channels_cold.length = n_cells;

        foreach(i, observer; observer_array)
        {
            Real[][string] geo_dict = observer.get_geo_dict();
            geo.rho[
                observer.wall_istart(wall_name)..
                observer.wall_iend(wall_name) + 1] = geo_dict["rho"];
            geo.cp[
                observer.wall_istart(wall_name)..
                observer.wall_iend(wall_name) + 1] = geo_dict["cp"];
            geo.a_wall[
                observer.wall_istart(wall_name)..
                observer.wall_iend(wall_name) + 1] = geo_dict["a_wall"];

            // Get n_channels
            auto wall_obs = cast(WallObserver) observer;
            if(wall_obs.hot_channel_connected)
            {
                Real[][string] hot_geo_dict = wall_obs.hot_channel.get_geo_dict();
                geo.n_channels_hot[
                    observer.wall_istart(wall_name)..
                    observer.wall_iend(wall_name) + 1] =
                        hot_geo_dict["n_channels"][0];
            }
            else
            {
                geo.n_channels_hot[
                    observer.wall_istart(wall_name)..
                    observer.wall_iend(wall_name) + 1] = double.nan;
            }
            if(wall_obs.cold_channel_connected)
            {
                Real[][string] cold_geo_dict = wall_obs.cold_channel.get_geo_dict();
                geo.n_channels_cold[
                    observer.wall_istart(wall_name)..
                    observer.wall_iend(wall_name) + 1] =
                        cold_geo_dict["n_channels"][0];
            }
            else
            {
                geo.n_channels_cold[
                    observer.wall_istart(wall_name)..
                    observer.wall_iend(wall_name) + 1] = double.nan;
            }
        }
    }

    void update_lti_model(Lti lti)
    {
        import std.math: PI, pow;

        // Form LTI model for each wall state variable.
        // Heat transfer is toggled by the 'hot_channel_connected' and
        // 'cold_channel_connected' variables.
        foreach(i; 0..wall_to_global.length)
        {
            // Derivative wrt T_wall
            lti.A[wall_to_global[i]["T_wall"], wall_to_global[i]["T_wall"]] =
                ((- PI * geo.n_channels_cold[i] * props.k_cold[i] * props.Nu_cold[i]) - (PI * geo.n_channels_hot[i] * props.k_hot[i] * props.Nu_hot[i])) / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);

            if(hot_chan_connected[i])
            {
                // Derivative wrt e_hot
                lti.A[wall_to_global[i]["T_wall"], wall_to_global[i]["e_hot"]] =
                    PI * geo.n_channels_hot[i] * (
                        props.Nu_hot[i] * (-props.T_wall[i] + props.T_hot[i]) * props.dkde_hot[i]
                        + props.k_hot[i] * (-props.T_wall[i] + props.T_hot[i]) * props.dNude_hot[i]
                        + props.dTde_hot[i] * props.k_hot[i] * props.Nu_hot[i])
                    / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);

                // Derivative wrt p_hot
                lti.A[wall_to_global[i]["T_wall"], i_p_hot] =
                    PI * geo.n_channels_hot[i] * (
                        props.Nu_hot[i] * (-props.T_wall[i] + props.T_hot[i]) * props.dkdp_hot[i]
                    + props.k_hot[i] * (-props.T_wall[i] + props.T_hot[i]) * props.dNudp_hot[i]
                    + props.dTdp_hot[i] * props.k_hot[i] * props.Nu_hot[i])
                    / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);

                // Derivative wrt mDotHot
                lti.A[wall_to_global[i]["T_wall"], i_mdot_hot] =
                    (PI * geo.n_channels_hot[i] * props.k_hot[i] * (-props.T_wall[i] + props.T_hot[i]) * props.dNudmdot_hot[i]) / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);
            }

            if(cold_chan_connected[i])
            {
                // Derivative wrt e_cold
                lti.A[wall_to_global[i]["T_wall"], wall_to_global[i]["e_cold"]] =
                    PI * geo.n_channels_cold[i] * (
                        props.Nu_cold[i] * (-props.T_wall[i] + props.T_cold[i]) * props.dkde_cold[i]
                    + props.k_cold[i] * (-props.T_wall[i] + props.T_cold[i]) * props.dNude_cold[i]
                    + props.dTde_cold[i] * props.k_cold[i] * props.Nu_cold[i])
                    / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);

                // Derivative wrt p_cold
                lti.A[wall_to_global[i]["T_wall"], i_p_cold] =
                    PI * geo.n_channels_cold[i] * (
                        props.Nu_cold[i] * (-props.T_wall[i] + props.T_cold[i]) * props.dkdp_cold[i]
                    + props.k_cold[i] * (-props.T_wall[i] + props.T_cold[i]) * props.dNudp_cold[i]
                    + props.dTdp_cold[i] * props.k_cold[i] * props.Nu_cold[i])
                    / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);

                // Derivative wrt mDot_cold
                lti.A[wall_to_global[i]["T_wall"], i_mdot_cold] =
                    (PI * geo.n_channels_cold[i] * props.k_cold[i] * (-props.T_wall[i] + props.T_cold[i]) * props.dNudmdot_cold[i]) / (geo.a_wall[i] * geo.cp[i] * geo.rho[i]);
            }
        }
    }
}


module q1dcfd.state_space_control.observer_defs;

import q1dcfd.config: Real;

/*
 * Wishlist:
 * - A lot of the stuff in here should be immutable or const?
 * - Should put some sensible error checking on inputs, currently most input
 *   errors result in silet failures which is not good at all (have started for
 *   HxObsDef, follow this format for others).
 */

/*
 * 'ObsDef' classs store data to specify a component model (observer and LTI model)
 * that forms part of a system-level state-space controller.
 */
abstract class ObsDef
{
    string type;
    string name;
    uint n_cells;

    this(string[string] dict, string type)
    {
        import std.conv: to;
        this.type    = type;
        this.name    = dict["name"];
        this.n_cells = dict["n_cells"].to!uint;
    }
}

class ChannelObsDef : ObsDef
{
    Real length;
    uint n_channels;
    Real k_L, k_d;
    string fluid, eos_backend, stream_name, geo_name;
    const uint[string] sensor_map;
    string[] outputs=[];

    // Optional constraints
    bool e_min_constraint = false;
    bool e_max_constraint = false;
    Real e_out_min = 0.0;
    Real e_out_max = 0.0;

    this(
        string[string] channel_dict,
        const uint[string] sensor_map,
        string[] outputs=[])
    {
        super(channel_dict, "ChannelObsDef");

        import std.conv: to;
        length          = channel_dict["length"].to!Real;
        n_channels      = channel_dict["n_channels"].to!uint;
        k_L             = channel_dict["k_L"].to!Real;
        k_d             = channel_dict["k_d"].to!Real;
        fluid           = channel_dict["fluid"];
        eos_backend     = channel_dict["eos_backend"];
        stream_name     = channel_dict["stream_name"];
        geo_name        = channel_dict["geo_name"];
        this.sensor_map = sensor_map;
        this.outputs    = outputs;

        if("e_out_min" in channel_dict)
        {
            e_min_constraint = true;
            e_out_min = channel_dict["e_out_min"].to!Real;
        }
        if("e_out_max" in channel_dict)
        {
            e_max_constraint = true;
            e_out_max = channel_dict["e_out_max"].to!Real;
        }
    }
}

class WallObsDef : ObsDef
{
    Real length;
    uint n_channels;
    string geo_name;

    this(string[string] wall_dict)
    {
        super(wall_dict, "WallObsDef");

        import std.conv: to;
        length     = wall_dict["length"].to!Real;
        n_channels = wall_dict["n_channels"].to!uint;
        geo_name   = wall_dict["geo_name"];
    }
}

class HxObsDef : ObsDef
{
    Real length;
    string config; // counter-flow or co-flow
    uint n_channels;
    Real k_L, k_d;
    string hot_fluid, hot_eos_backend, hot_stream_name, hot_geo_name,
              hot_nu_correlation;
    string cold_fluid, cold_eos_backend, cold_stream_name,
              cold_geo_name, cold_nu_correlation;
    string wall_geo_name;
    const uint[string] hot_sensor_map, cold_sensor_map;
    string[] hot_outputs, cold_outputs;

    // Optional constraints
    bool hot_e_min_constraint = false;
    bool cold_e_max_constraint = false;
    Real e_hot_min = 0.0;
    Real e_cold_max = 0.0;

    this(
        string[string] hx_dict,
        const uint[string] hot_sensor_map,
        const uint[string] cold_sensor_map,
        string[] hot_outputs=[],
        string[] cold_outputs=[]
        )
    {
        super(hx_dict, "HxObsDef");

        check_input_data(hx_dict);

        import std.conv: to;
        length               = hx_dict["length"].to!Real;
        config               = hx_dict["config"];
        n_channels           = hx_dict["n_channels"].to!uint;
        k_L                  = hx_dict["k_L"].to!Real;
        k_d                  = hx_dict["k_d"].to!Real;
        hot_fluid            = hx_dict["hot_fluid"];
        hot_eos_backend      = hx_dict["hot_eos_backend"];
        hot_stream_name      = hx_dict["hot_stream_name"];
        hot_geo_name         = hx_dict["hot_geo_name"];
        hot_nu_correlation   = hx_dict["hot_nu_correlation"];
        cold_fluid           = hx_dict["cold_fluid"];
        cold_eos_backend     = hx_dict["cold_eos_backend"];
        cold_stream_name     = hx_dict["cold_stream_name"];
        cold_geo_name        = hx_dict["cold_geo_name"];
        cold_nu_correlation  = hx_dict["cold_nu_correlation"];
        wall_geo_name        = hx_dict["wall_geo_name"];
        this.hot_sensor_map  = hot_sensor_map;
        this.cold_sensor_map = cold_sensor_map;
        this.hot_outputs     = hot_outputs;
        this.cold_outputs    = cold_outputs;

        if("e_hot_min" in hx_dict)
        {
            hot_e_min_constraint = true;
            e_hot_min = hx_dict["e_hot_min"].to!Real;
        }
        if("e_cold_max" in hx_dict)
        {
            cold_e_max_constraint = true;
            e_cold_max = hx_dict["e_cold_max"].to!Real;
        }
    }

    void check_input_data(string[string] dict)
    {
        import q1dcfd.utils.errors: InvalidDefinition;
        import std.algorithm: canFind;
        import std.format;

        if(!canFind(["coflow", "counterflow"], dict["config"]))
            throw new InvalidDefinition(
                    "Error in definition of heat exchanger %s, config is %s, "
                    ~ " but must either 'coflow' or 'counterflow'."
                    .format(dict["name"], dict["config"])
                    );
    }
}

class PumpObsDef : ObsDef
{
    Real a_in, mdot_max, mdot_dot_max;
    string fluid, eos_backend, stream_name;
    const uint[string] sensor_map, actuator_map;
    string[] outputs;

    this(
        string[string] pump_dict,
        const uint[string] sensor_map,
        const uint[string] actuator_map,
        string[] outputs=[])
    {
        import std.conv: to;
        pump_dict["n_cells"] = to!string(1); // Must have 1 cell
        super(pump_dict, "PumpObsDef");

        a_in         = pump_dict["a_in"].to!Real;
        fluid        = pump_dict["fluid"];
        eos_backend  = pump_dict["eos_backend"];
        stream_name  = pump_dict["stream_name"];
        mdot_max     = pump_dict["mdot_max"].to!Real;
        mdot_dot_max = pump_dict["mdot_dot_max"].to!Real;

        this.sensor_map   = sensor_map;
        this.actuator_map = actuator_map;
        this.outputs      = outputs;
    }
}

class TurbomachineObsDef : ObsDef
{
    Real a_in, a_out, i_rotor;
    string fluid, eos_backend, stream_name, turbomachine_type;
    const uint[string] sensor_map, actuator_map;
    string[] outputs;
    bool synchronous_flag;

    this(
        string[string] turbomachine_dict,
        const uint[string] sensor_map,
        const uint[string] actuator_map,
        string[] outputs=[])
    {
        import std.conv: to;
        turbomachine_dict["n_cells"] = to!string(1); // Must have 1 cell
        super(turbomachine_dict, turbomachine_dict["turbo_obs_type"]);

        a_in        = turbomachine_dict["a_in"].to!Real;
        a_out       = turbomachine_dict["a_out"].to!Real;
        fluid       = turbomachine_dict["fluid"];
        eos_backend = turbomachine_dict["eos_backend"];
        stream_name = turbomachine_dict["stream_name"];
        turbomachine_type = turbomachine_dict["turbomachine_type"];
        synchronous_flag  = turbomachine_dict["synchronous_flag"].to!bool;
        if (!synchronous_flag) i_rotor = turbomachine_dict["i_rotor"].to!Real;

        assert(turbomachine_type == "turbine" || turbomachine_type == "compressor");

        this.sensor_map   = sensor_map;
        this.actuator_map = actuator_map;
        this.outputs      = outputs;
    }
}

class TurbineObsDef : TurbomachineObsDef
{
    string coolprop_fluid, map_name;
    Real er_design, mfp_design, T0_design, P0_design;

    this(
        string[string] turbomachine_dict,
        const uint[string] sensor_map,
        const uint[string] actuator_map,
        string[] outputs=[])
    {
        turbomachine_dict["turbomachine_type"] = "turbine";
        turbomachine_dict["turbo_obs_type"] = "TurbineObsDef";
        super(turbomachine_dict, sensor_map, actuator_map, outputs);

        import std.conv: to;
        coolprop_fluid = turbomachine_dict["coolprop_fluid"];
        map_name       = turbomachine_dict["map_name"];
        er_design      = turbomachine_dict["er_design"].to!Real;
        mfp_design     = turbomachine_dict["mfp_design"].to!Real;
        T0_design      = turbomachine_dict["T0_design"].to!Real;
        P0_design      = turbomachine_dict["P0_design"].to!Real;
    }
}

class CompressorObsDef : TurbomachineObsDef
{
    string map_name;
    Real p_0;
    Real k_dh = 1.0;
    Real k_eta = 1.0;

    this(
        string[string] turbomachine_dict,
        const uint[string] sensor_map,
        const uint[string] actuator_map,
        string[] outputs=[])
    {
        turbomachine_dict["turbomachine_type"] = "compressor";
        turbomachine_dict["turbo_obs_type"] = "CompressorObsDef";
        super(turbomachine_dict, sensor_map, actuator_map, outputs);

        import std.conv: to;
        map_name = turbomachine_dict["map_name"];
        p_0      = turbomachine_dict["p_0"].to!Real;
        if("k_dh" in turbomachine_dict)
            k_dh  = turbomachine_dict["k_dh"].to!Real;
        if("k_eta" in turbomachine_dict)
            k_eta = turbomachine_dict["k_eta"].to!Real;
    }
}


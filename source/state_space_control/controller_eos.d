module q1dcfd.state_space_control.controller_eos;
/**
 * Provides tools so that either q1dcfd or CoolProp (either HEOS or INCOMP)
 * EOS objects may be used interchangeably by the control system code.
 * See 'ControllerEos' for details of interface that is implemented by the
 * EOS backends.
 */

import q1dcfd.config: Real;

ControllerEos generate_controller_eos(string backend_type, string fluid, string stream="")
{
    ControllerEos Eos;
    if(backend_type == "Q1D") {
        Eos = new Q1dEos(backend_type, fluid, stream);
    }
    else if(backend_type == "HEOS" || "INCOMP") {
        Eos = new CoolpropEos(backend_type, fluid);
    }
    else {
        import std.format;
        throw new Exception(format!("Invalid observer backend, %s.")(backend_type));
    }
    return Eos;
}

/* Fluid state for single cell. Supplies rootfinder guesses. */
struct FluidState { Real e, p, k, h, rho, T, mu; }
FluidState null_fs_guess()
{
    FluidState null_fs;
    return null_fs;
}

enum InputPairs { p_e, p_T, rho_e }

template GeneratePropertyFields(string[] props)
{
import std.format;
string GeneratePropertyFields()
{
    string str_out = "";
    foreach(prop; props)
    {
        str_out ~= format!("private double _%s; ")(prop);
        str_out ~= format!("@property double %s() { return _%s; } ")(prop, prop);
    }
    return str_out;
}
}

template UpdateThermoProps(string[] prop_map)
{
import std.format;
import std.array: split;
import std.string: strip;
string UpdateThermoProps()
{
    string str_out = "";
    foreach(pair; prop_map)
    {
        auto p = pair.split(":");
        str_out ~= format!("_%s = Eos.%s; ")(p[0].strip, p[1].strip);
    }
    return str_out;
}
}

abstract class ControllerEos
{
    immutable string[] FLUID_PROPS = ["e", "p", "k", "h", "rho", "T", "mu", "cp"];
    mixin(GeneratePropertyFields!(FLUID_PROPS));
    string backend_type;
    string fluid;

    string invalid_guess(string prop)
    {
        auto str_out = "Invalid guess in rootfinder for " ~ prop
            ~ " (value is NaN), check that guess of fluid state has been"
            ~ " provided.";
        return str_out;
    }

    this(string backend_type, string fluid)
    {
        this.backend_type = backend_type;
        this.fluid = fluid;
    }

    /*
     * Updates EOS object with any backend to a specified state.
     * Some non-native input pairs are supported using root-finding operations.
     * Input properties 'i1' and 'i2' must be ordered as per 'input_pair'.
     * 'guess_state' not required for native input pairs.
     */
    void update_eos(
            InputPairs input_pair,
            Real i1,
            Real i2,
            FluidState guess_state = null_fs_guess);
}

final class Q1dEos : ControllerEos
{
    import q1dcfd.eos: EOSBase, backend, generate_eos;
    import q1dcfd.simulation: Simulation;
    import q1dcfd.control_volumes.node : Node;

    EOSBase Eos;
    EOSBase EosPT;

    // Maps the properties in FLUID_PROPS to appropriate keys for the EOS backend
    // For the q1d backend these are exactly the same except mu : dv
    immutable string[] PROP_MAP = [
            "e   : e", // fluid_prop : eos_key
            "p   : p",
            "k   : k",
            "h   : h",
            "rho : rho",
            "T   : T",
            "mu  : dv",
            "cp  : cp"
        ];

    // stream is the name of the stream in the qdef file
    this(string backend_type, string fluid, string stream)
    {
        super(backend_type, fluid);

        Eos = generate_eos(
            Simulation.get_stream(stream).conservative_data,
            backend.TTSE_recovery,
            "Controller EOS object with (rho,e) inputs.");
        EosPT = generate_eos(
            Simulation.get_stream(stream).primitive_data,
            backend.TTSE_recovery,
            "Controller EOS object with (p,T) inputs.");
    }

    override void update_eos(
            InputPairs input_pair,
            double i1,
            double i2,
            FluidState guess_state = null_fs_guess)
    {
        // Update the state of the underlying EOS
        switch(input_pair)
        {
            case InputPairs.rho_e:
                Eos.update(i1, i2);
                break;
            case InputPairs.p_T:
                EosPT.update(i1, i2);
                Eos.update(EosPT.rho, EosPT.e);
                break;
            case InputPairs.p_e:
                update_eos_from_p_e(i1, i2, guess_state.rho);
                break;
            default:
                throw new Exception("Currently unsupported input pair specified.");
        }
        // Update all the properties to the new values
        mixin(UpdateThermoProps!(PROP_MAP));
    }

    /*
     * Newton-based method to set (rho, e)-based EOS using (p, e) inputs.
     * Assumes that 'eos' has been updated with (rho, e) from previous timestep.
     *
     * Solves for root of f(rho) = p(rho, e0) - p0 for known state (p0, e0).
     *
     * Solves rho[k+1] = rho[k] - f(rho) / f'(rho)
     * until |delta| = |rho[k+1] - rho[k]| < tol
     *
     * f' can be evaluated analytically as
     * f'(rho) = d(p)/d(rho)|e (rho).
     *
     * EOS does not have d(p)/d(rho), but does have d(h)/d(rho) so
     * p = rho*(h(rho, e) - e)
     * and f' =d(p)/d(rho)|e = (h(rho, e) - e) + rho d(h)/d(rho) | e
     */
    void update_eos_from_p_e(
        double p0,  // Known pressure
        double e0,  // Known internal energy
        double rho, // Guess of density
        double tol = 1.00E-4,
        uint max_iter = 1_000_000)
    {
        import std.math: fabs, isNaN;
        import q1dcfd.utils.errors: NonConvergenceError;

        if(isNaN(rho)) throw new Exception(invalid_guess("rho"));

        double f_dash, delta;

        foreach(i; 0..max_iter)
        {
            Eos.update(rho, e0);
            f_dash = Eos.h - e0 + rho * Eos.dhdD;
            if (f_dash == 0.0) {
                throw new NonConvergenceError("Derivative of function is zero");
            }
            delta = (p0 - Eos.p) / f_dash;
            rho += delta;
            if (fabs(delta) < tol)
            {
                Eos.update(rho, e0);
                return;
            }
        }

        import std.stdio;
        import std.conv: to;
        writeln("Maximum number of iterations, ", to!string(max_iter), ", exceeded.");
        writeln("delta (change in density between last two guess) = ", delta);
        throw new NonConvergenceError(
            "Newton's method failed to converge when computing (rho, e) from (p, e).");
    }
}

final class CoolpropEos : ControllerEos
{
    import coolprop;

    // Maps the properties in FLUID_PROPS to appropriate keys for the EOS backend
    immutable string[] PROP_MAP = [
            "e   : umass", // fluid_prop : coolprop_key
            "p   : p",
            "k   : conductivity",
            "h   : hmass",
            "rho : rhomass",
            "T   : T",
            "mu  : viscosity",
            "cp  : cpmass"
        ];

    AbstractState Eos;
    void delegate(InputPairs input_pair, double i1, double i2,
            FluidState guess_state) set_eos_state;

    this(string backend_type, string fluid)
    {
        super(backend_type, fluid);
        Eos = new AbstractState(backend_type, fluid);

        if (backend_type == "HEOS")
            this.set_eos_state = &set_eos_state_heos;
        else if (backend_type == "INCOMP")
            this.set_eos_state = &set_eos_state_incomp;
        else
        {
            import std.format;
            throw new Exception(format!("Unsupported backend, %s,
                        specified.")(backend_type));
        }
    }

    /*
     * For HEOS backend, supports all input pairs.
     * For the INCOMP backend, supports the native input pairs plus (p, e) and (rho,
     * e) inputs via root-finding operations.
     */
    override void update_eos(
            InputPairs input_pair,
            double i1,
            double i2,
            FluidState guess_state = null_fs_guess)
    {
        // First, update the state of the underlying EOS
        set_eos_state(input_pair, i1, i2, guess_state);
        // Next, we update all the properties to the new values
        mixin(UpdateThermoProps!(PROP_MAP));
    }

    /* Sets state of Coolprop EOS with HEOS backend */
    void set_eos_state_heos(
            InputPairs input_pair,
            double i1,
            double i2,
            FluidState guess_state)
    {
        // eos.update(input_pair, i1, i2);
        throw new Exception("Need to implement standard HEOS calls");
    }

    /* Sets state of Coolprop EOS with INCOMP backend */
    void set_eos_state_incomp(
            InputPairs input_pair,
            double i1,
            double i2,
            FluidState guess_state)
    {
        switch(input_pair)
        {
            case InputPairs.p_T:
                Eos.update(PT_INPUTS, i1, i2);
                break;
            case InputPairs.p_e:
                update_eos_from_p_e(Eos, i1, i2, guess_state.T);
                break;
            case InputPairs.rho_e:
                update_eos_from_rho_e(Eos, i1, i2, guess_state.p);
                break;
            default:
                throw new Exception(
                    "Specified Coolprop update pair not yet supported by "
                    ~ "CoolpropEos.");
        }
    }

    /*
     * Newton solver to find T based on known (p, e) inputs for incompressible fluid.
     * Inputs:
     *     p0 - known pressure
     *     e0 - known internal energy
     *     T  - guess of temperature
     */
    void update_eos_from_p_e(
            AbstractState eos,
            double p0,
            double e0,
            double T,
            double tol=1.00E-4,
            uint max_iter=1_000_000)
    {
        import std.math: fabs, isNaN;

        if(isNaN(T)) throw new Exception(invalid_guess("T"));

        double H = 1E-8;
        double e, e_plus, e_minus, f_dash, delta;
        foreach(i; 0..max_iter)
        {
            // Value of e for current estimate T
            eos.update(PT_INPUTS, p0, T);
            e = eos.keyed_output(iUmass);

            // Compute de/dt|p
            eos.update(PT_INPUTS, p0, (1+H) * T);
            e_plus = eos.keyed_output(iUmass);
            eos.update(PT_INPUTS, p0, (1-H) * T);
            e_minus = eos.keyed_output(iUmass);
            f_dash = (e_plus - e_minus) / (2*H*T);

            delta = (e0 - e) / f_dash;
            T += delta;
            if (fabs(delta) < tol)
            {
                eos.update(PT_INPUTS, p0, T);
                return;
            }
        }

        // If loop falls through, root finder has failed to converge
        throw new Exception(
            "Root-finding problem to calculate T from (e, p) did not converge.");
    }

    /*
     * Newton solver to find p based on known (rho, e) inputs for incompressible fluid.
     * Inputs:
     *     rho0 - known density
     *     e0   - known internal energy
     *     p    - guess of pressure
     */
    void update_eos_from_rho_e(
            AbstractState eos,
            double rho0,
            double e0,
            double p,
            double tol=1.00E-4,
            uint max_iter=1_000_000)
    {
        import std.math: fabs, isNaN;

        if(isNaN(p)) throw new Exception(invalid_guess("p"));

        double H = 1E-8;
        double e, e_plus, e_minus, f_dash, delta;
        foreach(i; 0..max_iter)
        {
            // Value of e for current estimate T
            eos.update(DmassP_INPUTS, rho0, p);
            e = eos.keyed_output(iUmass);

            // Compute de/dt|p
            eos.update(DmassP_INPUTS, rho0, (1+H) * p);
            e_plus = eos.keyed_output(iUmass);
            eos.update(DmassP_INPUTS, rho0, (1-H) * p);
            e_minus = eos.keyed_output(iUmass);
            f_dash = (e_plus - e_minus) / (2*H*p);

            delta = (e0 - e) / f_dash;
            p += delta;
            if (fabs(delta) < tol)
            {
                eos.update(DmassP_INPUTS, rho0, p);
                return;
            }
        }

        // If loop falls through, root finder has failed to converge
        throw new Exception(
            "Root-finding problem to calculate p from (rho, e) did not converge.");
    }
}


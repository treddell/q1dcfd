module q1dcfd.state_space_control.jobs.mpc_pche_design_point;

import q1dcfd.state_space_control: ObsDef, HxObsDef, PumpObsDef,
        StreamDef, WallDef, FlowSourceDef;

// For this jobfile, outputs (ordered) are
// Z = [e_cold_out, mdot_cold]
// U = [hot_pump_mdot, cold_pump_mdot]

/*
 * Wishlist:
 * - Would be much nicer to specify the outputs in a single dict rather than
 *   specifying them for each observer.
 */

// Global MPC settings
string[string] make_mpc_defs()
{
    string[string] mpc_params = [
        "hu" : "20",
        "hp" : "20",
        "hw" : "1",
        "hh" : "0",
        "Q"  : "1e1, 1e-8",
        "R"  : "5E-1, 5E-1"
    ];
    return mpc_params;
}

ObsDef[] make_defs()
{
    ObsDef[] observer_defs;

    // Recuperator
    const uint[string] hot_sensor_map = [
            "p_in"    : 0,
            "h_in"    : 1,
            "mdot_in" : 2,
            "T_in"    : 3,
            "e_out"   : 4,
            "h_out"   : 5,
            "T_out"   : 6];
    const uint[string] cold_sensor_map = [
            "p_in"    : 7,
            "h_in"    : 8,
            "mdot_in" : 9,
            "T_in"    : 10,
            "e_out"   : 11,
            "h_out"   : 12,
            "T_out"   : 13];
    string[string] recuperator_dict = [
            // Hx settings
            "name"       : "recuperator",
            "n_cells"    : "6",
            "length"     : "1.0", // m
            "config"     : "counterflow",
            "n_channels" : "1600",
            "k_L"        : "0.5",
            "k_d"        : "0.1",
            // Channel settings
            "hot_fluid"           : "CO2",
            "hot_eos_backend"     : "Q1D",
            "hot_stream_name"     : "low_pressure_stream",
            "hot_geo_name"        : "channel_cross_section",
            "hot_nu_correlation"  : "Ngo",
            "cold_fluid"          : "CO2",
            "cold_eos_backend"    : "Q1D",
            "cold_stream_name"    : "high_pressure_stream",
            "cold_geo_name"       : "channel_cross_section",
            "cold_nu_correlation" : "Ngo",
            "wall_geo_name"       : "pche_wall_cross_section"];
    auto recuperator_def = new HxObsDef(recuperator_dict, hot_sensor_map,
            cold_sensor_map);
    observer_defs ~= recuperator_def;

    // LP (hot) pump
    const uint[string] hot_pump_sensor_map = [
            "p"                 : 0,
            "e"                 : 20,
            "T"                 : 3,
            "mdot"              : 2,
            "mdot_pump"         : 14,
            "mdot_dot_pump"     : 15,
            "mdot_dot_dot_pump" : 16
    ];
    const uint[string] hot_pump_actuator_map = [
            "mdot_target" : 0
    ];
    string[string] hot_pump_dict = [
            "name"         : "hot_pump",
            "a_in"         : "0.001413744",
            "fluid"        : "CO2",
            "eos_backend"  : "Q1D",
            "stream_name"  : "low_pressure_stream",
            "mdot_max"     : "30",
            "mdot_dot_max" : "6.0"];
    auto hot_pump_def = new PumpObsDef(hot_pump_dict, hot_pump_sensor_map,
            hot_pump_actuator_map);
    observer_defs ~= hot_pump_def;

    // HP (cold) pump
    const uint[string] cold_pump_sensor_map = [
            "p"                 : 7,
            "e"                 : 21,
            "T"                 : 10,
            "mdot"              : 9,
            "mdot_pump"         : 17,
            "mdot_dot_pump"     : 18,
            "mdot_dot_dot_pump" : 19
    ];
    const uint[string] cold_pump_actuator_map = [
            "mdot_target" : 1
    ];
    string[string] cold_pump_dict = [
            "name"         : "cold_pump",
            "a_in"         : "0.001413744",
            "fluid"        : "CO2",
            "eos_backend"  : "Q1D",
            "stream_name"  : "high_pressure_stream",
            "mdot_max"     : "20",
            "mdot_dot_max" : "3.0"];
    string[] cold_pump_outs = ["mdot"];
    auto cold_pump_def = new PumpObsDef(cold_pump_dict, cold_pump_sensor_map,
            cold_pump_actuator_map, cold_pump_outs);
    observer_defs ~= cold_pump_def;

    return observer_defs;
}

StreamDef[] make_streams()
{
    StreamDef[] stream_defs;

    string[string] hot_stream_dict = [
        "name"        : "hot_stream",
    ];
    string[] hot_stream_observers = ["hot_pump", "recuperator_hot"];
    auto hot_stream_def = new StreamDef(hot_stream_dict, hot_stream_observers);
    stream_defs ~= hot_stream_def;

    string[string] cold_stream_dict = [
        "name"        : "cold_stream",
    ];
    string[] cold_stream_observers = ["cold_pump", "recuperator_cold"];
    string[] cold_stream_outs = ["e_out"];
    auto cold_stream_def = new StreamDef(cold_stream_dict,
            cold_stream_observers, cold_stream_outs);
    stream_defs ~= cold_stream_def;

    return stream_defs;
}

WallDef[] make_walls()
{
    WallDef[] wall_defs;

    string[] recup_wall_observers = ["recuperator_wall"];
    string[string] recup_wall_flow_sources = [
        "hot_flow_source"  : "hot_pump",
        "cold_flow_source" : "cold_pump",
        ];
    auto recuperator_wall_def = new WallDef("recuperator", recup_wall_observers,
            recup_wall_flow_sources);
    wall_defs ~= recuperator_wall_def;

    return wall_defs;
}

FlowSourceDef[] make_flow_sources()
{
    FlowSourceDef[] flow_source_defs;
    string[string] empty_stream_dict;

    string[string] hot_pump_dict = ["pump" : "hot_pump"];
    auto hot_pump_def = new FlowSourceDef("pump", "hot_pump", hot_pump_dict,
            empty_stream_dict);
    flow_source_defs ~= hot_pump_def;

    string[string] cold_pump_dict = ["pump" : "cold_pump"];
    auto cold_pump_def = new FlowSourceDef("pump", "cold_pump", cold_pump_dict,
            empty_stream_dict);
    flow_source_defs ~= cold_pump_def;

    return flow_source_defs;
}


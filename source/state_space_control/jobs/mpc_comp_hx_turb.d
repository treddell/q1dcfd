module q1dcfd.state_space_control.jobs.mpc_comp_hx_turb;

import q1dcfd.state_space_control: ObsDef, ChannelObsDef, HxObsDef, PumpObsDef,
        StreamDef, WallDef, FlowSourceDef, CompressorObsDef, TurbineObsDef;

// For this jobfile, inputs & outputs (ordered) are
// Z = []
// U = []

/*
 * Wishlist:
 * - Would be much nicer to specify the outputs in a single dict rather than
 *   specifying them for each observer.
 */

// Global MPC settings
string[string] make_mpc_defs()
{
    string[string] mpc_params = [
        "hu" : "10",
        "hp" : "10",
        "hw" : "1",
        "hh" : "0",
        // speed_compressor is untracked (used for surge constraint)
        "Q"  : "1E0, 0, 0", // [mdot_compressor, speed_compressor, T_motor]
        // More regularization on dT_motor_dt required to obtain smooth performance
        "R"  : "5E-4, 5E-8" // [dT_motor_dt (compressor), mdot_target (hot-side)]
    ];
    return mpc_params;
}

ObsDef[] make_defs()
{
    ObsDef[] observer_defs;

    // -------------------- SENSOR MAPS --------------------
    // Compressor
    const uint[string] compressor_sensor_map = [
            "p_in"        : 0,
            "T_in"        : 1,
            "p_out"       : 2,
            "T_out"       : 3,
            "mdot"        : 4,
            "speed"       : 5,
            "T_load"      : 6,
            "T_motor"     : 42,
            "dT_motor_dt" : 43
    ];
    const uint[string] compressor_actuator_map = [
            "dT_motor_dt" : 0
    ];
    // Turbine
    const uint[string] turbine_sensor_map = [
            "p_in"    : 7,
            "T_in"    : 8,
            "p_out"   : 9,
            "T_out"   : 10,
            "mdot"    : 11,
            "speed"   : 12,
            "T_load"  : 13
    ];

    // Comp-to-hx pipe
    const uint[string] comp_to_hx_pipe_map = [
            "p_in"     : 14,
            "h_in"     : 15,
            "T_in"     : 16,
            "mdot_in"  : 17,
            "e_out"    : 18,
            "T_out"    : 19
    ];
    // Recuperator cold side
    const uint[string] hx_cold_map = [
            "p_in"     : 20,
            "h_in"     : 21,
            "T_in"     : 22,
            "mdot_in"  : 23,
            "e_out"    : 24,
            "T_out"    : 25
    ];
    // Recuperator-to-turb pipe
    const uint[string] hx_to_turb_pipe_map = [
            "p_in"     : 26,
            "h_in"     : 27,
            "T_in"     : 28,
            "mdot_in"  : 29,
            "e_out"    : 30,
            "T_out"    : 31
    ];

    // Recuperator hot stream
    const uint[string] hx_hot_map = [
            "p_in"     : 32,
            "h_in"     : 33,
            "T_in"     : 34,
            "mdot_in"  : 35,
            "e_out"    : 36,
            "T_out"    : 37
    ];
    const uint[string] hot_pump_sensor_map = [
            "p"                 : 32,
            "e"                 : 38,
            "T"                 : 34,
            "mdot"              : 35,
            "mdot_pump"         : 39,
            "mdot_dot_pump"     : 40,
            "mdot_dot_dot_pump" : 41
    ];
    const uint[string] hot_pump_actuator_map = [
            "mdot_target" : 1
    ];

    // -------------------- COMPONENTS --------------------
    string[string] comp_to_hx_dict = [
            "name"        : "comp_to_hx_pipe",
            "n_cells"     : "2",
            "length"      : "0.24", // plenum and adjoining pipe
            "n_channels"  : "1",
            "k_L"         : "0.5",
            "k_d"         : "0.0",
            "fluid"       : "CO2",
            "eos_backend" : "Q1D",
            "stream_name" : "flow_path",
            "geo_name"    : "turbo_cross_section"
    ];
    auto comp_to_hx_def = new ChannelObsDef(comp_to_hx_dict, comp_to_hx_pipe_map);
    observer_defs ~= comp_to_hx_def;

    string[string] hx_to_turb_dict = [
            "name"        : "hx_to_turb_pipe",
            "n_cells"     : "2",
            "length"      : "0.24", // plenum and adjoining pipe
            "n_channels"  : "1",
            "k_L"         : "0.5",
            "k_d"         : "0.0",
            "fluid"       : "CO2",
            "eos_backend" : "Q1D",
            "stream_name" : "flow_path",
            "geo_name"    : "turbo_cross_section"
    ];
    auto hx_to_turb_def = new ChannelObsDef(hx_to_turb_dict, hx_to_turb_pipe_map);
    observer_defs ~= hx_to_turb_def;

    string[string] hx_dict = [
            // Hx settings
            "name"       : "hx",
            "n_cells"    : "4",
            "length"     : "0.6", // m
            "config"     : "counterflow",
            "n_channels" : "40000",
            "k_L"        : "0.5",
            "k_d"        : "0.0",
            // Channel settings
            "hot_fluid"           : "CO2",
            "hot_eos_backend"     : "Q1D",
            "hot_stream_name"     : "flow_path",
            "hot_geo_name"        : "pche_cross_section",
            "hot_nu_correlation"  : "Ngo",
            "cold_fluid"          : "CO2",
            "cold_eos_backend"    : "Q1D",
            "cold_stream_name"    : "flow_path",
            "cold_geo_name"       : "pche_cross_section",
            "cold_nu_correlation" : "Ngo",
            "wall_geo_name"       : "pche_wall_cross_section"];
    auto hx_def = new HxObsDef(hx_dict, hx_hot_map, hx_cold_map);
    observer_defs ~= hx_def;

    string[string] compressor_dict = [
            "name"              : "compressor",
            "a_in"              : "0.0314159265",
            "a_out"             : "0.0314159265",
            "i_rotor"           : "0.0007",
            "fluid"             : "CO2",
            "eos_backend"       : "Q1D",
            "stream_name"       : "flow_path",
            "synchronous_flag"  : "false",
            "map_name"          : "compressor-data",
            "p_0"               : "12.9645E6" // OK if this is approx
    ];
    string[] compressor_outs = ["mdot", "speed", "T_motor"];
    auto compressor_def = new CompressorObsDef(compressor_dict, compressor_sensor_map,
            compressor_actuator_map, compressor_outs);
    observer_defs ~= compressor_def;

    string[string] turbine_dict = [
            "name"              : "turbine",
            "a_in"              : "0.0314159265",
            "a_out"             : "0.0314159265",
            "fluid"             : "CO2",
            "eos_backend"       : "Q1D",
            "coolprop_fluid"    : "CO2",
            "stream_name"       : "flow_path",
            "synchronous_flag"  : "true",
            "map_name"          : "turbine-data",
            "er_design"         : "1.5",
            "mfp_design"        : "1.785",
            "T0_design"         : "500.0",
            "P0_design"         : "12.53E6"
    ];
    // string[] turbine_outs = ["mdot"]; // Haven't implemented eqs for these yet
    string[] turbine_outs;
    const uint[string] turbine_actuator_map; // synchronous, no actuator
    auto turbine_def = new TurbineObsDef(turbine_dict, turbine_sensor_map,
            turbine_actuator_map, turbine_outs);
    observer_defs ~= turbine_def;

    string[string] hot_pump_dict = [
            "name"         : "hot_pump",
            "a_in"         : "0.0314159265",
            "fluid"        : "CO2",
            "eos_backend"  : "Q1D",
            "stream_name"  : "flow_path",
            "mdot_max"     : "30.0",
            "mdot_dot_max" : "3.0"];
    auto hot_pump_def = new PumpObsDef(hot_pump_dict, hot_pump_sensor_map,
            hot_pump_actuator_map);
    observer_defs ~= hot_pump_def;

    return observer_defs;
}

StreamDef[] make_streams()
{
    StreamDef[] stream_defs;

    string[string] cold_stream_dict = [
        "name"        : "cold_stream",
    ];
    string[] cold_stream_observers = ["compressor", "comp_to_hx_pipe",
        "hx_cold",  "hx_to_turb_pipe"];
    auto cold_stream_def = new StreamDef(cold_stream_dict, cold_stream_observers);
    stream_defs ~= cold_stream_def;

    string[string] hot_stream_dict = [
        "name"        : "hot_stream",
    ];
    string[] hot_stream_observers = ["hot_pump", "hx_hot"];
    auto hot_stream_def = new StreamDef(hot_stream_dict, hot_stream_observers);
    stream_defs ~= hot_stream_def;

    return stream_defs;
}

WallDef[] make_walls()
{
    WallDef[] wall_defs;

    string[] hx_wall_observers = ["hx_wall"];
    string[string] hx_wall_flow_sources = [
        "hot_flow_source"  : "hot_pump",
        "cold_flow_source" : "compressor",
        ];
    auto hx_wall_def = new WallDef("hx_wall", hx_wall_observers,
            hx_wall_flow_sources);
    wall_defs ~= hx_wall_def;

    return wall_defs;
}

FlowSourceDef[] make_flow_sources()
{
    FlowSourceDef[] flow_source_defs;
    string[string] empty_stream_dict;

    string[string] hot_pump_fs_dict = ["pump" : "hot_pump"];
    auto hot_pump_fs_def = new FlowSourceDef("pump", "hot_pump_fs",
            hot_pump_fs_dict, empty_stream_dict);
    flow_source_defs ~= hot_pump_fs_def;

    // fs_dict for turbomachine pair takes form
    //      ["compressor" : <comp-observer-name>,
    //       "turbine"    : <turb-observer-name>]
    string[string] cold_fs_dict = [
        "compressor" : "compressor",
        "turbine"    : "turbine"
    ];
    auto cold_fs_def = new FlowSourceDef("turbomachines", "turbomachines_fs",
            cold_fs_dict, empty_stream_dict);
    flow_source_defs ~= cold_fs_def;

    return flow_source_defs;
}



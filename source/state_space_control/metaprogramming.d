module q1dcfd.state_space_control.metaprogramming;

/*
 * Wishlist:
 * - Implement public or protected switch for these initializers.
 */

/*
 * Same as InitPropArrays, but for single-cell observers.
 */
template InitPropValues(string[] props)
{
string InitPropValues()
{
    import std.format;
    string str_out = "";
    foreach(prop; props)
    {
        str_out ~= format!("private Real _%s; ")(prop);
        str_out ~= format!("@property Real %s() { return _%s; }; ")(prop, prop);
    }
    return str_out;
}
}

/*
 * Assigns and sizes dynamic arrays with names in 'props' to the current scope.
 * Properties are externally read-only.
 */
template InitPropArrays(string[] props, string length="")
{
string InitPropArrays()
{
    import std.conv: to;
    import std.format;
    string str_out = "";
    foreach(prop; props)
    {
        static if (length.length)
            str_out ~= format!("_%s.length = %s; ")(prop, length);
        else
        {
            str_out ~= format!("private Real[] _%s; ")(prop);
            str_out ~= format!("@property Real[] %s() { return _%s; }; ")
                (prop, prop);
            str_out ~= format!("@property Real %s(uint i) { return _%s[i]; }; ")
                (prop, prop);
        }
    }
    return str_out;
}
}

// TODO: This template is just a public version of the one above... could put in
// a public switch and just have one template.
template InitPublicPropArrays(
        string[] props,
        string length="")
{
string InitPublicPropArrays()
{
    import std.conv: to;
    import std.format;
    string str_out = "";
    foreach(prop; props)
    {
        static if (length.length)
            str_out ~= format!("%s.length = %s; ") (prop, length);
        else
            str_out ~= format!("Real[] %s; ")(prop);
    }
    return str_out;
}
}

/* Initialises partial derivative arrays (i.e. dkde) */
template InitPderArrays(
        string[] props,
        string[] pder_vars=[],
        string length="")
{
string InitPderArrays()
{
    import std.conv: to;
    import std.format;
    string str_out = "";
    foreach(prop; props)
    {
        foreach(pder_var; pder_vars)
        {
            static if (length.length)
                str_out ~= format!("d%sd%s.length = %s; ") (prop, pder_var, length);
            else
                str_out ~= format!("Real[] d%sd%s; ")(prop, pder_var);
        }
    }
    return str_out;
}
}


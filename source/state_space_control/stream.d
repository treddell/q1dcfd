module q1dcfd.state_space_control.stream;

import q1dcfd.config: Real, InputPort;
import q1dcfd.state_space_control.observer: IObserver;
import q1dcfd.state_space_control.lti: Lti;
import q1dcfd.state_space_control.mpc: Mpc;

import mir.ndslice;
import lubeck;

/*
 * Wishlist:
 * - Currently assumes one state variable per cell, to relax this assumption,
 *   see formation of the 'stream_to_global' array.
 * - I think if you wanted to do this really nicely for arbitrary systems, you
 *   would have to define some inheritance hierachy with 'thermal' and 'flow
 *   source' components inheriting from AObserver.
 * - Currently only supports channels as 'thermal' components (maybe this is
 *   right though).
 * - All components are now only part of one stream, so can make stream indexing
 *   a standard array (rather than aa).
 * - Split the 'process_observer_array' function into smaller parts.
 */

class StreamDef
{
    string name;
    string[] observers;
    string[] z_vars, c_vars;

    this(
        string[string] dict,
        string[] observers,
        string[] z_vars = new string[0],
        string[] c_vars = new string[0])
    {
        this.name      = dict["name"];
        this.observers = observers;
        this.z_vars    = z_vars;
        this.c_vars    = c_vars;
    }
}

Stream build_stream(
    StreamDef def,
    IObserver[string] observer_map)
{
    auto stream_obs_array = new IObserver[](def.observers.length);
    foreach(i, observer_name; def.observers)
    {
        if(!(observer_name in observer_map))
        {
            import std.format;
            throw new Exception(format!(
                "Observer %s not found in observer_map, check that this observer"
                ~ " is defined in the jobfile.")(observer_name));
        }
        stream_obs_array[i] = observer_map[observer_name];
    }
    return new Stream(def, stream_obs_array, def.z_vars, def.c_vars);
}

/*
 * Stores all thermo properties and state variables for a stream object.
 * Req'd to interface with observers.
 */
class Props
{
    import q1dcfd.config: Real;
    import q1dcfd.state_space_control.metaprogramming: InitPderArrays,
           InitPublicPropArrays;

    // TODO: dxdt should explicitly be dedt?
    Real p, mdot;
    enum stream_props = ["v", "e", "k", "h", "rho", "T", "Nu", "T_wall", "dxdt"];
    enum pder_props   = ["k", "h", "rho", "T", "Nu"];
    enum pder_vars    = ["e", "p", "mdot"];
    mixin(InitPublicPropArrays!(stream_props));
    mixin(InitPderArrays!(pder_props, pder_vars));

    this(uint n_cells)
    {
        mixin(InitPublicPropArrays!(stream_props, "n_cells"));
        mixin(InitPderArrays!(pder_props, pder_vars, "n_cells"));
    }
}

/*
 * Used to form LTI model of flow 'streams'.
 * A stream is defined as some 'source' component (i.e. pump, turbomachine, etc),
 * connected to a series of 'thermal' components.
 * To form the LTI model, we first form property vectors for the entire stream,
 * then use these vectors to compute an analytical LTI model.
 */
class Stream
{
    import q1dcfd.config: Real;
    import q1dcfd.state_space_control: ChannelObserver, WallObserver,
           PumpObserver, TurbomachineObserver;

public:
    Props props;
    immutable string stream_name;
    immutable uint n_cells;
    uint stream_global_index(string var, uint i) { return stream_to_global[i][var]; }
    IObserver[] observer_array; // TODO: should be protected, write getter.

    Geo geo; // TODO: should be private
    Real e_out_predicted, e_out_measured, T_out_measured;
    
    @property Real get_y_tilde() { return y_tilde; } // TODO: delete later

protected:
    immutable string[] flow_observers = ["PumpObserver", "TurbomachineObserver"];
    immutable string[] thermal_observers = ["ChannelObserver"];
    uint[2][string] stream_map;
    uint[string][] stream_to_global; // maps prop & stream index to global state index
    struct Geo { Real[] flow_area, delta_x, n_channels; Real vol; }
    bool[] wall_connected;
    Real[] q_connected;

    string[] _output_array = ["e_out", "T_out", "Q_in"];
    string[] z_vars;
    uint z_index_start;
    uint z_index_end;
    immutable uint n_cntrl_outs;
    uint[string] cntrl_out_map;

    string[] c_vars;
    uint c_index_start;
    uint c_index_end;
    immutable uint n_constraints;
    uint[string] constraint_map;

    immutable Real k_L = 0.7; // observer gain
    immutable Real k_d = 0.15; // integrating disturbance gain
    // immutable Real k_L = 0.0; // observer gain
    // immutable Real k_d = 0.0; // integrating disturbance gain
    Real[] L, e_corr;
    Real y_tilde = 0.0; // output prediction error
    Real dy = 0.0; // output disturbance
    Real t_prev = 0.0;
    Real dt_stable = 9E16;

public:
    this(
        StreamDef def,
        ref IObserver[] observer_array,
        string[] z_vars = new string[](0),
        string[] c_vars = new string[](0))
    {
        import std.algorithm.searching: canFind;

        this.stream_name = def.name;
        this.observer_array = observer_array;
        auto n_cells_stream = process_observer_array(observer_array);
        n_cells = n_cells_stream;
        props = new Props(n_cells);
        populate_geo(observer_array);
        e_corr.length = n_cells;
        initialize_observer_gain_array();

        this.z_vars = z_vars;
        n_cntrl_outs = cast(uint) z_vars.length;
        foreach(i, var; z_vars)
        {
            if(! _output_array.canFind(var))
            {
                import std.format;
                throw new Exception(format!(
                    "ERROR: Invalid controlled output %s in Stream %s.")
                    (var, stream_name));
            }
            cntrl_out_map[var] = cast(uint) i;
        }

        this.c_vars = c_vars;
        n_constraints = cast(uint) c_vars.length;
        foreach(i, var; c_vars)
        {
            if(! _output_array.canFind(var))
            {
                import std.format;
                throw new Exception(format!(
                    "ERROR: Invalid constraint %s in Stream %s.")
                    (var, stream_name));
            }
            constraint_map[var] = cast(uint) i;
        }
    }

    uint z_global_index(string var)
    {
        return z_index_start + cntrl_out_map[var];
    }

    uint populate_cntrl_out_indices(uint z_in)
    {
        if(n_cntrl_outs > 0)
        {
            z_index_start = z_in;
            z_index_end = z_index_start + n_cntrl_outs - 1;
            return z_index_end + 1;
        }
        else
        {
            z_index_start = -1;
            z_index_end = -1;
            return z_in;
        }
    }

    uint populate_constraint_indices(uint c_in)
    {
        if(n_constraints > 0)
        {
            c_index_start = c_in;
            c_index_end = c_index_start + n_constraints - 1;
            return c_index_end + 1;
        }
        else
        {
            c_index_start = -1;
            c_index_end = -1;
            return c_in;
        }
    }

    /*
     * Stream update: 
     * 1. Measure inlet conditions from first component and outlet conditions
     *    from last component.
     * 2. Loop through components and compute their outlet conditions (e).
     * 3. For each component, set inlet conditions as stream pressure and outlet
     *    e of previous component.
     * 4. Compute observer gains using final outlet conditions.
     */
    void update(const Real t, const InputPort[] input_ports)
    {
        Real dt = t - t_prev;
        if(dt >= dt_stable)
        {
            dt_stable = 9E16; // Revise the update rate

            // Take measurements and compute props for first component.
            auto first_obs = observer_array[0];
            first_obs.update_state_estimate(t, dt, input_ports);
            Real p_stream    = first_obs.get_predicted_output("p");
            Real mdot_stream = first_obs.get_predicted_output("mdot");
            Real h_out_i     = first_obs.get_predicted_output("h");

            // TODO: Should just run the update_state_estimates here?

            // Loop through observers and update their inlet conditions
            foreach(observer; observer_array[1..$])
            {
                auto chan_obs = cast(ChannelObserver) observer;
                chan_obs.set_inlet_conditions(mdot_stream, p_stream, h_out_i);
                h_out_i = chan_obs.get_predicted_output("h");
            }

            // Use final observer for outputs
            auto final_obs = cast(ChannelObserver) observer_array[$-1];
            T_out_measured = input_ports[final_obs.sensors.T_out]();
            e_out_measured = input_ports[final_obs.sensors.e_out]();
            e_out_predicted = final_obs.get_predicted_output("e") + dy;
            Real y_tilde = e_out_measured - e_out_predicted;
            e_corr[] = L[] * y_tilde;
            dy += k_d * y_tilde;
            // First one is flow source, don't need to correct
            foreach(observer; observer_array[1..$])
            {
                uint[2] i_obs = stream_map[observer.name];
                observer.update_e_corr(e_corr[i_obs[0]..i_obs[1]+1]);
                if(observer.dt_stable < dt_stable) dt_stable = observer.dt_stable;
            }

            // Set the new stable timestep for all observers
            dt_stable = dt_stable / 10; // make it 10x faster. // TODO 2008
            if(dt_stable > 0.02) dt_stable = 0.02;
            foreach(observer; observer_array[1..$]) observer.set_dt_stable(dt_stable);
            t_prev = t;
        }
    }

    void control_update(ref Lti lti, ref Mpc mpc)
    {
        foreach(observer; observer_array)
            observer.write_to_stream_prop_vectors(stream_name, props);
        write_outputs(lti);
        update_lti_model(lti);
        update_constraints(mpc);
    }

    /* Set initial update rate according to component with strictest requirement */
    void initialise()
    {
        foreach(observer; observer_array[1..$]) // exclude 'flow source'
            if(observer.dt_stable < dt_stable) dt_stable = observer.dt_stable;
        // Make all observers in stream have same update rate
        foreach(observer; observer_array[1..$]) observer.set_dt_stable(dt_stable);
    }

protected:
    /* Checks input 'observer_array' is valid and populates stream indices */
    uint process_observer_array(IObserver[] observer_array)
    {
        uint index_start = 0;
        foreach(i, observer; observer_array)
        {
            // -----------------------------------------------------------------
            // Check input array is OK
            import std.algorithm: canFind;
            import std.format;
            import std.array: join;
            if(i == 0) // first component must be a 'flow source'
            {
                if(!canFind(flow_observers, observer.type))
                {
                    throw new Exception(format!(
                        "ERROR: First component in a stream must be one of types %s,"
                        ~ "but first component of stream %s is of type %s.")(
                        flow_observers.join(", "), stream_name, observer.type));
                }
                if(observer.n_cells != 1)
                {
                    throw new Exception(format!(
                        "ERROR: 'Flow source' (first) component in stream must "
                        ~ "have only one cell, but first component of stream %s "
                        ~ "of type %s has %d cells.")(
                        stream_name, observer.type, observer.n_cells));
                }
            }
            else // remaining components must be 'thermal components'
            {
                if(!canFind(thermal_observers, observer.type))
                {
                    throw new Exception(format!(
                        "ERROR: Components in a stream must be of types %s (excluding "
                        ~ "first component), but a component of %s is of type %s.")(
                        flow_observers.join(", "), stream_name, observer.type));
                }
                // This implementation currently assumes one state variable per
                // cell for thermal components
                if(observer.n_states_per_cell != 1)
                {
                    throw new Exception(format!(
                        "ERROR: Currently, 'thermal' stream components must "
                        ~ "have one state variable per cell, but component %s"
                        ~ "in stream %s has %d states per cell")(
                        observer.name, stream_name, observer.n_states_per_cell));
                }
            }

            // -----------------------------------------------------------------
            // Add the stream indexing information
            observer.populate_stream_indices(stream_name, index_start);
            stream_map[observer.name] = [
                observer.stream_istart(stream_name),
                observer.stream_iend(stream_name)];

            // -----------------------------------------------------------------
            // Form mapping from stream index to global state indices
            stream_to_global.length += (observer.n_cells);
            if(i == 0)
            {
                // Assuming one cell here (checked in initialization)
                // 'Flow source' component must be located at stream index 0
                stream_to_global[0]["e"] = observer.global_index(0, "e_out");
                stream_to_global[0]["p"] = observer.global_index(0, "p_out");
                stream_to_global[0]["mdot"] = observer.global_index(0, "mdot");
            }
            else
            {
                // This part assumes component is a channel
                if(observer.type != "ChannelObserver")
                {
                    throw new Exception("ERROR: Stream currently assumes "
                            ~ "thermal components are channels.");
                }
                auto chan_obs = cast(ChannelObserver) observer;

                // Iterate over observer cells and create mappings
                uint k = 0; // Observer cell index
                foreach(j; observer.stream_istart(stream_name)..
                            observer.stream_iend(stream_name) + 1)
                {
                    stream_to_global[j]["e"] = observer.global_index(k, "e");
                    if(chan_obs.wall_connected)
                    {
                        if(chan_obs.wall_reversed)
                        {
                            stream_to_global[j]["Tw"] = chan_obs.wall.global_index(
                                    chan_obs.n_cells - 1 - k, "T");
                        }
                        else
                            stream_to_global[j]["Tw"] = chan_obs.wall.global_index(k, "T");
                    }
                    k++;
                }
            }

            // -----------------------------------------------------------------
            // If 'observer' is a channel, flag a connected wall if it exists
            auto wall_connected_next = new bool[](observer.n_cells);
            auto q_connected_next    = new Real[](observer.n_cells);
            if(observer.type == "ChannelObserver")
            {
                auto chan_obs = cast(ChannelObserver) observer;
                if(chan_obs.wall_connected)
                {
                    wall_connected_next[] = true;
                    q_connected_next[]    = 1.0;
                }
                else
                {
                    wall_connected_next[] = false;
                    q_connected_next[]    = 0.0;
                }
            }
            else
            {
                wall_connected_next[] = false;
                q_connected_next[]    = 0.0;
            }
            wall_connected ~= wall_connected_next;
            q_connected    ~= q_connected_next;

            // -----------------------------------------------------------------
            index_start = observer.stream_iend(stream_name) + 1;
        }
        return index_start;
    }

    /*
     * For a stream, we assume that the internal energy estimates are corrected
     * using a gain array that scales from zero at the inlet to k_L at the outlet.
     * This array scales linearly over cells with heat transfer and is constant
     * over other cells.
     * This scaling is used since the inlet measurements are directly measured
     * and are therefore correct (within sensor error).
     */
    void initialize_observer_gain_array()
    {
        import std.algorithm.iteration: sum;
        L.length = n_cells;
        auto delta_L_arr = new Real[](n_cells);
        Real delta_L = k_L / (sum(q_connected));
        delta_L_arr[] = delta_L * q_connected[];
        L[0] = 0.0; // flow source, not q connected
        foreach(i; 1..n_cells) L[i] = L[i-1] + delta_L_arr[i];
    }

    void populate_geo(IObserver[] observer_array)
    {
        geo.vol = 0.0; // total volume
        geo.flow_area.length  = n_cells;
        geo.delta_x.length    = n_cells;
        geo.n_channels.length = n_cells;
        foreach(i, observer; observer_array)
        {
            Real[][string] geo_dict = observer.get_geo_dict();
            geo.flow_area[
                observer.stream_istart(stream_name)..
                observer.stream_iend(stream_name) + 1] = geo_dict["flow_area"];
            geo.delta_x[
                observer.stream_istart(stream_name)..
                observer.stream_iend(stream_name) + 1] = geo_dict["delta_x"];
            geo.n_channels[
                observer.stream_istart(stream_name)..
                observer.stream_iend(stream_name) + 1] = geo_dict["n_channels"][0];
        }
        // First component (cell 0) is pump or turbomachine, start counting from cell 1.
        // Note: flow area already captures n_chans
        foreach(i; 1..n_cells) geo.vol += geo.flow_area[i] * geo.delta_x[i];
    }

    void write_outputs(ref Lti lti)
    {
        foreach(var; z_vars)
        {
            uint z_ind = z_index_start + cntrl_out_map[var]; // row of C matrix
            if(var == "e_out")
            {
                // Get global indices of the e cells for the last observer in
                // the stream.
                auto final_obs = observer_array[$-1];
                uint e_i_end = final_obs.global_index(final_obs.n_cells - 1, "e");
                uint e_i_endm1 = final_obs.global_index(final_obs.n_cells - 2, "e");
                lti.C[z_ind, e_i_end] = 1.5;
                lti.C[z_ind, e_i_endm1] = -0.5;
                lti.Zk[z_ind, 0] = final_obs.get_predicted_output("e") + dy;
                // This is not needed! lti.dk[z_ind, 0] = dy; // already in Zk
            }
            else if(var == "T_out")
            {
                auto final_obs = cast(ChannelObserver) observer_array[$-1]; // TODO: Remove cast
                uint e_i_end = final_obs.global_index(final_obs.n_cells - 1, "e");
                uint e_i_endm1 = final_obs.global_index(final_obs.n_cells - 2, "e");
                lti.C[z_ind, e_i_end] = 1 * final_obs.dTde[$-1];
                // lti.C[z_ind, stream_to_global[0]["p"]] = 1.0 * final_obs.dTdp[$-1];

                // lti.C[z_ind, e_i_end] = 1.5 * final_obs.dTde[$-1];
                // lti.C[z_ind, e_i_endm1] = -0.5 * final_obs.dTde[$-2];
                // // Should next term be in (works w/o though)?
                // // lti.C[z_ind, stream_to_global[0]["p"]] =
                    // // 1.5 * final_obs.dTdp[$-1] - 0.5 * final_obs.dTdp[$-2];

                lti.Zk[z_ind, 0] = T_out_measured;
            }
            else if(var == "Q_in")
            {
                auto fs_obs = observer_array[0];
                auto final_obs = observer_array[$-1];
                uint mdot_fs = fs_obs.global_index(0, "mdot");
                uint e_i_end = final_obs.global_index(final_obs.n_cells - 1, "e");
                lti.C[z_ind, mdot_fs] = props.e[0] - props.e[$-1];
                lti.C[z_ind, e_i_end] = -props.mdot;
                lti.Zk[z_ind, 0] = props.mdot * (props.e[0] - props.e[$-1]);
            }
            else
                throw new Exception("ERROR: Output variable " ~ var
                    ~ " not implemented for Stream.");
        }
    }

    void update_constraints(ref Mpc mpc)
    {
        foreach(var; c_vars)
        {
            if(var == "T_out")
            {
                // TODO: I think this works now, formulation below is equivalent though.
                // // T_out must be less than T_out_max.
                // auto row = c_index_start + constraint_map[var];
                // immutable T_out_max = 540.0; // K
                // mpc.GammaL[row, z_global_index(var)] = 1.0;
                // mpc.gL[row, 0] = -1.0 * T_out_max;
                // mpc.ZHigh[z_global_index(var), 0] = T_out_max; // Just to show

                immutable T_out_max = 568.0; // K
                mpc.ZHighW[z_global_index(var), 0] = 1E4;
                mpc.ZVars[2*z_global_index(var)+1] = 1;
                mpc.ZHigh[z_global_index(var), 0] = T_out_max;
            }
            else
                throw new Exception("ERROR: Constraint variable " ~ var
                    ~ " not implemented for Stream.");
        }
    }

    void update_lti_model(ref Lti lti)
    {
        import std.math: PI, pow;

        // Form LTI model for each stream state variable
        // Heat transfer is toggled by the 'wall_connected' variable
        // Ignore first cell, it is the 'flow source' and is handled elsewhere
        foreach(i; 1..stream_to_global.length)
        {
            // Derivative wrt ei
            lti.A[stream_to_global[i]["e"], stream_to_global[i]["e"]] =
                - ((props.mdot * props.dhde[i]) / (geo.flow_area[i] * geo.delta_x[i] * props.rho[i]))
                - ((props.mdot * (-props.h[i] + props.h[i-1]) * props.drhode[i]) / (geo.flow_area[i] * geo.delta_x[i] * pow(props.rho[i], 2)));

            // Derivative wrt ei-1
            lti.A[stream_to_global[i]["e"], stream_to_global[i-1]["e"]] =
                (props.mdot * (props.dhde[i-1])) / (geo.flow_area[i] * geo.delta_x[i] * props.rho[i]);

            // Derivative wrt p0 (first cell)
            lti.A[stream_to_global[i]["e"], stream_to_global[0]["p"]] =
                - ((props.mdot * props.dhdp[i]) / (geo.flow_area[i] * geo.delta_x[i] * props.rho[i]))
                - ((props.mdot * (-props.h[i] + props.h[i-1]) * props.drhodp[i]) / (geo.flow_area[i] * geo.delta_x[i] * pow(props.rho[i], 2)));

            // Derivative wrt mDot0 (first cell) 
            lti.A[stream_to_global[i]["e"], stream_to_global[0]["mdot"]] =
                + (props.h[i-1] - props.h[i]) / (geo.flow_area[i] * geo.delta_x[i] * props.rho[i]);

            // Add heat transfer terms
            if(wall_connected[i])
            {
                // wrt ei
                lti.A[stream_to_global[i]["e"], stream_to_global[i]["e"]] +=
                    - ((PI * geo.n_channels[i] * props.Nu[i] * (-props.T_wall[i] + props.T[i])  * props.dkde[i]) / (geo.flow_area[i] * props.rho[i]))
                    + ((PI * geo.n_channels[i] * props.k[i] * props.Nu[i] * (-props.T_wall[i] + props.T[i]) * props.drhode[i]) / (geo.flow_area[i] * pow(props.rho[i], 2)))
                    - ((PI * geo.n_channels[i] * props.k[i] * props.Nu[i] * props.dTde[i]) / (geo.flow_area[i] * props.rho[i]))
                    - ((PI * geo.n_channels[i] * props.k[i] * (-props.T_wall[i] + props.T[i]) * props.dNude[i]) / (geo.flow_area[i] * props.rho[i]));

                // wrt p0
                lti.A[stream_to_global[i]["e"], stream_to_global[0]["p"]] +=
                    - ((PI * geo.n_channels[i] * props.Nu[i] * (-props.T_wall[i] + props.T[i])  * props.dkdp[i]) / (geo.flow_area[i] * props.rho[i]))
                    + ((PI * geo.n_channels[i] * props.k[i] * props.Nu[i] * (-props.T_wall[i] + props.T[i]) * props.drhodp[i]) / (geo.flow_area[i] * pow(props.rho[i], 2)))
                    - ((PI * geo.n_channels[i] * props.k[i] * props.Nu[i] * props.dTdp[i]) / (geo.flow_area[i] * props.rho[i]))
                    - ((PI * geo.n_channels[i] * props.k[i] * (-props.T_wall[i] + props.T[i]) * props.dNudp[i]) / (geo.flow_area[i] * props.rho[i]));

                // Derivative wrt Twi
                lti.A[stream_to_global[i]["e"], stream_to_global[i]["Tw"]] = // no +=, only term!
                    (PI * geo.n_channels[i] * props.k[i] * props.Nu[i]) / (geo.flow_area[i] * props.rho[i]);

                // wrt mDot0
                lti.A[stream_to_global[i]["e"], stream_to_global[0]["mdot"]] +=
                    - (PI * geo.n_channels[i] * props.k[i] * (props.T[i] - props.T_wall[i]) * props.dNudmdot[i]) / (geo.flow_area[i] * props.rho[i]);
            }
        }
    }
}

// [Applied to update_lti_model... also useful for docs]
//
// RHS of the LTI equations uses stream indexing
// LHS uses global LTI indexing
// Stream indexing is cell-wise but global LTI indexing is variable-wise... but
// just one state variable per cell for 'thermal components', so map can just be
// uint[uint].

// [This could be used to fill out the doc string... ]
// How this module will work:
//
// - Read input array, do some checking to see if it is valid.
// - Assign stream indicies to each component (check thermal flow components
//   don't already have entries).
// - Copy required geometric parameters into stream vectors.
// - Each controller timestep,
//    1) read in observer property vectors, 
//    2) calculate extra property vectors required to form LTI model,
//    3) form LTI model.


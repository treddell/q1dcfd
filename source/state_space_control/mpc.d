module q1dcfd.state_space_control.mpc;

import q1dcfd.state_space_control.lti: Lti;
import q1dcfd.config: Real;
import mir.ndslice;
import lubeck;

import std.stdio, std.conv, std.array;

/*
 * Wishlist:
 * - Initialize all of the structs in their own constructors rather than in the
 *   'MpcSolver' constructor.
 * - Improve the interfaces between this module and the rest of the code.
 *   Currently, the MpcSolver class has defined boundaries: it takes the MPC
 *   struct and LTI model as inputs and returns the optimal outputs in lti.Uk
 * - Have 'calculate_mpc_update' return a tdarr object Uk rather than writing
 *   directly to lti.
 * - Specify upper and lower constraint arrays separately then combine into
 *   single array in MpcSolver.
 */

extern(C) int run_gurobi(int nRows, int nCols, int nInputs,
        Real *Q, Real *c, Real *A, Real *rhs, Real *w, Real *UOpt);

alias odarr = Slice!(Real*, 1);
alias tdarr = Slice!(Real*, 2);

/*
 * Global MPC parameters.
 * Here nOUTPUTS refers to the number of controlled outputs (n_cntrl_outs in
 * Lti), not the number of measured outputs.
 * A different naming convention is used here to match the original MPC code.
 *
 * Includes arrays UVars, DUVars, ZVars to toggle simple min or max constraints
 * on or off.
 * Format of UVars is u0_low, u0_high, u1_low, u1_high, etc, so that upper or
 * lower constraints can be toggled individually.
 *
 * Also includes GammaL and gL which are constraint matrices for internal
 * component constraints; these constraints may be general linear inequalities
 * involving any output variables --- i.e. they are not simple min or max
 * constraints.
 * Currently these are set from observers and not from the jobfile.
 */
struct Mpc
{
public:
    // MPC parameters
    @property uint HU (){ return _hu; }
    @property uint HP (){ return _hp; }
    @property uint HW (){ return _hw; }
    @property uint HB (){ return _hb; }
    @property uint nSTATES()      { return _n_states; }
    @property uint nINPUTS()      { return _n_inputs; }
    @property uint nOUTPUTS()     { return _n_outputs; }
    @property uint nCONST()       { return _n_const; }
    @property uint nCONSTRAINTS() { return _n_constraints; }
    @property Real[] Q() { return _q; }
    @property Real[] Qb() { return _qb; }
    @property Real[] Qt() { return _qt; }
    @property Real[] R() { return _r; }
    @property Real[] Rb() { return _rb; }
    @property Real dt() { return _dt; }

    // Constraint parameters are modified according to current state
    tdarr ULow,  UHigh, DULow, DUHigh, ZLow,  ZHigh; // constraint values
    tdarr DULowB, DUHighB; // For move blocking
    odarr UVars, DUVars, ZVars; // toggle constraints on or off
    tdarr GammaL, gL; // Linear inequality constraints on Z
    tdarr ULowW, UHighW, DULowW, DUHighW, ZLowW, ZHighW, GammaLW;

private:
    uint _hu, _hp; // control and prediction horizons
    uint _hw, _hb; // window and block parameters
    uint _n_states, _n_inputs, _n_outputs, _n_constraints, _n_const;
    Real[] _q, _qb,_qt, _r, _rb; // weighting matrices
    Real _dt;      // MPC timestep

public:
    this(
        string[string] mpc_params,
        Real mpc_dt,
        uint n_states,
        uint n_inputs,
        uint n_controlled_ouputs,
        uint n_constraints)
    {
        import std.format;
        import std.conv: to;
        import std.string: strip;

        _dt = mpc_dt;
        _hu = mpc_params["hu"].to!int;
        _hp = mpc_params["hp"].to!int;
        _hw = mpc_params["hw"].to!int;
        _hb = mpc_params["hb"].to!int;
        _n_states      = n_states;
        _n_inputs      = n_inputs;
        _n_outputs     = n_controlled_ouputs;
        _n_constraints = n_constraints;

        if(_hu != _hp)
            throw new Exception(
                "ERROR: Currently Hu must be set to Hp due to minor bug in "
                ~ "linear MPC implementation.");
        if(_hb != _hp)
            throw new Exception(
                "ERROR: Currently Hb must be set to Hp due to bug in "
                ~ "move blocking implementation.");

        // QP objective function weighting matrices
        string[] q_str  = mpc_params["Q"].split(",");
        string[] qb_str = mpc_params["Qb"].split(",");
        string[] qt_str = mpc_params["Qt"].split(",");
        string[] r_str  = mpc_params["R"].split(",");
        string[] rb_str = mpc_params["Rb"].split(",");
        if(q_str.length != nOUTPUTS)
            throw new Exception(format!(
                "ERROR: Output weights (Q) specified in 'mpc_params' should be of "
                 ~ "length n_outputs (here, %d), but is of length %d.")(
                 nOUTPUTS, q_str.length));
        if(qb_str.length != nOUTPUTS)
            throw new Exception(format!(
                "ERROR: Blocked output weights (Qb) specified in 'mpc_params' should be of "
                 ~ "length n_outputs (here, %d), but is of length %d.")(
                 nOUTPUTS, qb_str.length));
        if(qt_str.length != nOUTPUTS)
            throw new Exception(format!(
                "ERROR: Terminal Output weights (Qt) specified in 'mpc_params' should be of "
                 ~ "length n_outputs (here, %d), but is of length %d.")(
                 nOUTPUTS, qt_str.length));
        if(r_str.length != nINPUTS)
            throw new Exception(format!(
                "ERROR: Move suppression weights (R) specified in 'mpc_params' "
                ~ "should be of length n_inputs (here, %d), but is of length %d.")(
                nINPUTS, r_str.length));
        if(rb_str.length != nINPUTS)
            throw new Exception(format!(
                "ERROR: Blocked move suppression weights (Rb) specified in 'mpc_params' "
                ~ "should be of length n_inputs (here, %d), but is of length %d.")(
                nINPUTS, r_str.length));
        _q.length  = q_str.length;
        _qb.length = qb_str.length;
        _qt.length = qt_str.length;
        _r.length  = r_str.length;
        _rb.length = rb_str.length;
        foreach(i; 0.._q.length) _q[i]   = to!Real(q_str[i].strip);
        foreach(i; 0.._qb.length) _qb[i] = to!Real(qb_str[i].strip);
        foreach(i; 0.._qt.length) _qt[i] = to!Real(qt_str[i].strip);
        foreach(i; 0.._r.length) _r[i]   = to!Real(r_str[i].strip);
        foreach(i; 0.._rb.length) _rb[i] = to!Real(rb_str[i].strip);

        // Constraints
        UVars    = slice!Real(2*nINPUTS);
        DUVars   = slice!Real(2*nINPUTS);
        ZVars    = slice!Real(2*nOUTPUTS);
        ULow     = slice!Real(nINPUTS, 1);
        UHigh    = slice!Real(nINPUTS, 1);
        DULow    = slice!Real(nINPUTS, 1);
        DUHigh   = slice!Real(nINPUTS, 1);
        ZLow     = slice!Real(nOUTPUTS, 1);
        ZHigh    = slice!Real(nOUTPUTS, 1);
        DULowB   = slice!Real(nINPUTS, 1);
        DUHighB  = slice!Real(nINPUTS, 1);

        // TODO: Should set this from jobfile.
        // Currently, hardcode input constraints on and output constraints off
        // Format of UVars (& ZVars etc) is u0_low, u0_high, u1_low, u1_high,
        // etc, so that upper or lower constraints can be toggled individually.
        UVars[]  = 1; // all on
        DUVars[] = 1;
        ZVars[]  = 0; // all off
        ZLow[]   = 0.0;
        ZHigh[]  = 0.0;

        // Constraint weights; all constraints are optionally soft constraints
        ULowW   = slice!Real(nINPUTS, 1);
        UHighW  = slice!Real(nINPUTS, 1);
        DULowW  = slice!Real(nINPUTS, 1);
        DUHighW = slice!Real(nINPUTS, 1);
        ZLowW   = slice!Real(nOUTPUTS, 1);
        ZHighW  = slice!Real(nOUTPUTS, 1);
        // Initialize weights, nan means implement as hard constraint
        ULowW[]   = double.nan;
        UHighW[]  = double.nan;
        DULowW[]  = double.nan;
        DUHighW[] = double.nan;
        // TODO: Probably better to make Z weights invalid to force specification
        ZLowW[]   = 1.0;
        ZHighW[]  = 1.0;

        // Generalized Z constraints
        GammaL    = slice!Real(nCONSTRAINTS, nOUTPUTS);
        gL        = slice!Real(nCONSTRAINTS, 1);
        GammaLW   = slice!Real(nCONSTRAINTS, 1); // weights
        GammaL[]  = 0.0;
        gL[]      = 0.0;
        GammaLW[] = 1.0;
    }
}

/* Quadratic program objective function */
struct Q
{
    tdarr BigQ, BigR, BigC, BigD, Psi, Upsilon, Theta, CInvUpsilon, Epsilon,
            Beta, Xi, Be, Dk, GObj, HObj, E, F, G, FBar, Gamma, W, FBarMax,
            EW, FW, GW,
            GammaThetaMax, WMax, BigOmega, SmallOmega, OmegaW;
}

/* Gurobi inputs */
struct G
{
    Real[] HDbl, BigOmegaDbl, GDbl, SmallOmegaDbl, OmegaWDbl, UOpt;
    int nRows, nCols;
}

final class MpcSolver
{
    tdarr TRef;
    G Gur;
    Q Qp;

    this(Mpc mpc)
    {
        TRef = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);
        Gur.UOpt.length = mpc.nINPUTS * mpc.HU; // Gurobi inputs

        // Formulation of QP is as per Maciejowski Chap 2, modified to
        // incorporate D matrix.
        // CInvUpsilon is stored because it is used to assemble Theta.
        Qp.BigQ    = slice!Real(mpc.HP*mpc.nOUTPUTS, mpc.HP*mpc.nOUTPUTS);
        Qp.BigR    = slice!Real(mpc.HU*mpc.nINPUTS,  mpc.HU*mpc.nINPUTS);
        Qp.BigC    = slice!Real(mpc.HP*mpc.nOUTPUTS, mpc.HP*mpc.nSTATES);
        Qp.BigD    = slice!Real(mpc.HP*mpc.nOUTPUTS, mpc.HP*mpc.nINPUTS);
        Qp.Psi     = slice!Real(mpc.nOUTPUTS*mpc.HP, mpc.nSTATES);
        Qp.Upsilon = slice!Real(mpc.nOUTPUTS*mpc.HP, mpc.nINPUTS);
        Qp.Theta   = slice!Real(mpc.nOUTPUTS*mpc.HP, mpc.nINPUTS*mpc.HU);
        Qp.Beta    = slice!Real(mpc.nOUTPUTS*mpc.HP, mpc.nINPUTS);
        Qp.Be      = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);
        Qp.Xi      = slice!Real(mpc.nOUTPUTS*mpc.HP, mpc.nINPUTS*mpc.HU);
        Qp.Dk      = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);
        Qp.Epsilon = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);
        Qp.GObj    = slice!Real(mpc.nINPUTS*mpc.HU,  1);
        Qp.HObj    = slice!Real(mpc.nINPUTS*mpc.HU,  mpc.nINPUTS*mpc.HU);
        Qp.CInvUpsilon = slice!Real(mpc.nSTATES*mpc.HP, mpc.nINPUTS);
        // QP constraint matrices
        // Total output constraints is min/max constraints plus general constraints
        uint nOutputConstraints = (2*mpc.nOUTPUTS + mpc.nCONSTRAINTS) * mpc.HP;
        Qp.E             = slice!Real(2*mpc.nINPUTS*mpc.HU,  mpc.nINPUTS*mpc.HU+1);
        Qp.F             = slice!Real(2*mpc.nINPUTS*mpc.HU,  mpc.nINPUTS*mpc.HU+1);
        Qp.G             = slice!Real(nOutputConstraints,    mpc.nOUTPUTS*mpc.HP+1);
        Qp.EW            = slice!Real(2*mpc.nINPUTS*mpc.HU,  1);
        Qp.FW            = slice!Real(2*mpc.nINPUTS*mpc.HU,  1);
        Qp.GW            = slice!Real(nOutputConstraints,    1);
        Qp.FBar          = slice!Real(2*mpc.nINPUTS*mpc.HU,  mpc.nINPUTS*mpc.HU);
        Qp.Gamma         = slice!Real(nOutputConstraints,    mpc.nOUTPUTS*mpc.HP);
        Qp.W             = slice!Real(2*mpc.nINPUTS*mpc.HU,  mpc.nINPUTS*mpc.HU);
        Qp.FBarMax       = slice!Real(2*mpc.nINPUTS*mpc.HU,  1);
        Qp.GammaThetaMax = slice!Real(nOutputConstraints,    1);
        Qp.WMax          = slice!Real(2*mpc.nINPUTS*mpc.HU,  1);
        Qp.BigOmega      = slice!Real((4*mpc.nINPUTS*mpc.HU) + nOutputConstraints,
                mpc.nINPUTS*mpc.HU);
        Qp.SmallOmega    = slice!Real((4*mpc.nINPUTS*mpc.HU) + nOutputConstraints, 1);
        Qp.OmegaW        = slice!Real((4*mpc.nINPUTS*mpc.HU) + nOutputConstraints, 1);

        // Initialise invariant parts of QP objective matrices
        AssembleBigQBigR(mpc, Qp);
    }

    void clean_qp_matrices()
    {
        // Qp.BigQ, Qp.BigR stay constant.
        Gur.UOpt[]         = 0.0;
        Qp.BigC[]          = 0.0;
        Qp.BigD[]          = 0.0;
        Qp.Psi[]           = 0.0;
        Qp.Upsilon[]       = 0.0;
        Qp.Theta[]         = 0.0;
        Qp.Beta[]          = 0.0;
        Qp.Be[]            = 0.0;
        Qp.Xi[]            = 0.0;
        Qp.Dk[]            = 0.0;
        Qp.Epsilon[]       = 0.0;
        Qp.GObj[]          = 0.0;
        Qp.HObj[]          = 0.0;
        Qp.CInvUpsilon[]   = 0.0;
        Qp.E[]             = 0.0;
        Qp.F[]             = 0.0;
        Qp.G[]             = 0.0;
        Qp.FBar[]          = 0.0;
        Qp.Gamma[]         = 0.0;
        Qp.W[]             = 0.0;
        Qp.FBarMax[]       = 0.0;
        Qp.GammaThetaMax[] = 0.0;
        Qp.WMax[]          = 0.0;
        Qp.BigOmega[]      = 0.0;
        Qp.SmallOmega[]    = 0.0;
    }

    /*
     * Solves tracking MPC problem defined by MPC settings 'mpc', LTI model 'lti'
     * (with reference trajectory lti.ZRef).
     * Stores optimal control inputs in lti.Uk.
     */
    void calculate_mpc_update(ref Mpc mpc, ref Lti lti)
    {
        clean_qp_matrices();

        // MPC solves for ZBar = Z - Zk, set reference trajectory as ZRef - Zk
        foreach(i; 0..mpc.HP)
            TRef[i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS, 0..1] = lti.ZRef[] - lti.Zk[];
        // Express U & Z constraints relative to current operating point
        mpc.ULow[]  = mpc.ULow[] - lti.UBarkm1[] - lti.Ukm1[];
        mpc.UHigh[] = mpc.UHigh[] - lti.UBarkm1[] - lti.Ukm1[];
        mpc.ZLow[]  = mpc.ZLow[] - lti.ZBar[] - lti.Zk[];
        mpc.ZHigh[] = mpc.ZHigh[] - lti.ZBar[] - lti.Zk[];
        // Convert rate constraints to discrete time
        immutable double dtb = 0.2; // TODO: from jobfile.
        mpc.DULowB[]  = mpc.DULow[] * dtb;
        mpc.DUHighB[] = mpc.DUHigh[] * dtb;
        mpc.DULow[]   = mpc.DULow[] * mpc.dt;
        mpc.DUHigh[]  = mpc.DUHigh[] * mpc.dt;

        // Assemble QP matrices
        AssembleBigCBigD(mpc, lti, Qp);
        AssemblePsi(mpc, lti, Qp);
        AssembleUpsilonBe(mpc, lti, Qp);
        AssembleDk(mpc, lti, Qp);
        AssembleBeta(mpc, Qp);
        AssembleXi(mpc, Qp);
        AssembleTheta(mpc, Qp);
        AssembleEFG(mpc, Qp);
        AssembleFBar(mpc, Qp);
        Qp.Gamma[] = Qp.G[0..$, 0..$-1];
        Qp.W[] = Qp.E[0..$, 0..$-1];

        // Assemble QP objective function matrices
        AssembleEpsilon(TRef, lti.XBar, lti.UBarkm1, mpc, Qp);
        AssembleGobjHobj(mpc, Qp);

        // Assemble QP constraints matrices
        AssembleFBarMax(mpc, Qp, lti.UBarkm1);
        AssembleGammaThetaMax(lti.XBar, lti.UBarkm1, mpc, Qp);
        AssembleOmegaMax(mpc, Qp);
        AssembleOmegas(mpc, Qp);

        // Format inputs for Gurobi
        MakeGurobiInputStruct(mpc, Qp, Gur);

        // // For debugging
        // PrintQpFormulationMatrices(Qp);
        // PrintQpMatrices(Qp);

        // Compute MPC input
        auto UOptSlice = slice!Real(mpc.nINPUTS * (mpc.HP + 1), 1);
        UOptSlice[] = 0.0; // Final entry is not computed (or used) so set as 0
        int gurErr;
        gurErr = run_gurobi(Gur.nRows, Gur.nCols, mpc.nINPUTS,
                Gur.HDbl.ptr, Gur.GDbl.ptr, Gur.BigOmegaDbl.ptr,
                Gur.SmallOmegaDbl.ptr, Gur.OmegaWDbl.ptr,
                Gur.UOpt.ptr);
        ProcessOptimalInputs(mpc, Gur, lti, UOptSlice);
        DisplayMpcOutputPredictions(mpc, lti, UOptSlice);

        // The ZLow constraints are shifted by Zk at MPC update, so they need to
        // be zeroed (then re-updated by the observers) each step
        mpc.ZLow[] = 0.0;
        mpc.ZHigh[] = 0.0;
    }
}

protected:
void DisplayMpcOutputPredictions(Mpc mpc, Lti lti, tdarr UOptSlice)
{
    // Displays optimal future output trajectory as predicted by MPC
    // These variables are local and separate to XBar, UBar etc in lti struct
    auto X    = slice!Real(mpc.nSTATES, 1);
    auto ZBar = slice!Real(mpc.nOUTPUTS, 1);
    auto XBar = slice!Real(mpc.nSTATES, 1);
    auto UBar = slice!Real(mpc.nINPUTS, 1);
    XBar[] = 0.0;
    UBar[] = 0.0;

    // Print the constraints
    writeln("\nConstraints:");
    writeln("ZLow  = ", mpc.ZLow[] + lti.ZBar[] + lti.Zk[]);
    writeln("ZHigh = ", mpc.ZHigh[] + lti.ZBar[] + lti.Zk[]);

    // Compute and display Z0 (add UStar to Ukm1 then find outputs)
    // Don't add dk to Z0 since it is directly measured
    UBar[] = UBar[] + UOptSlice[0*mpc.nINPUTS..1*mpc.nINPUTS];
    ZBar[] = mtimes(lti.C, XBar) + mtimes(lti.D, UBar);
    writeln("\nMPC optimal output trajectory:\n");
    writeln("Z0  = ", ZBar[0..$, 0] + lti.Zk[0..$, 0]);

    // Loop through prediction horizon and compute predicted outputs
    foreach(k; 1..mpc.HP+1)
    {
        // Compute next X value
        if (k <= mpc.HB) XBar[] = mtimes(lti.Ad, XBar) + mtimes(lti.Bd, UBar) + lti.F0d[];
        else             XBar[] = mtimes(lti.Adb, XBar) + mtimes(lti.Bdb, UBar) + lti.F0db[];
        UBar[] = UBar[] + UOptSlice[k*mpc.nINPUTS..(k+1)*mpc.nINPUTS];
        // Compute output
        ZBar[] = mtimes(lti.C, XBar) + mtimes(lti.D, UBar);

        // Dodgy way of doing '=' sign padding
        if (k < 10)
            writeln("Z", k, "  = ", ZBar[0..$, 0] + lti.Zk[0..$, 0] + lti.dk[0..$, 0]);
        else
            writeln("Z", k, " = ", ZBar[0..$, 0] + lti.Zk[0..$, 0] + lti.dk[0..$, 0]);
    }
    writeln("\n");
}

/*
 * Takes QP soln from Gurobi and stores in 'UOptSlice', then calculates actual
 * optimal inputs using from previous input (Ukm1).
 * Stores optimal inputs in lti.Uk for use by simulation code.
 */
void ProcessOptimalInputs(Mpc mpc, G Gur, ref Lti lti, ref tdarr UOptSlice)
{
    foreach(m; 0..mpc.nINPUTS*mpc.HU) { UOptSlice[m, 0] = Gur.UOpt[m]; }
    foreach(i; mpc.HU..mpc.HP)
    {
        foreach(j; 0..mpc.nINPUTS)
        {
            UOptSlice[i*j, 0] = Gur.UOpt[mpc.nINPUTS * (mpc.HU - 1) + j];
        }
    }
    lti.Uk[0..$, 0] =
        lti.Ukm1[0..$, 0] + lti.UBarkm1[0..$, 0] + UOptSlice[0..mpc.nINPUTS, 0];

    uint print_length;
    if(mpc.HP < 10) print_length = mpc.HP;
    else print_length = 10;
    writeln("ZRef:  ", lti.ZRef[0..$, 0]);
    writeln("UOpt:  ", Gur.UOpt[0..mpc.nINPUTS]);
    writeln("Uk:    ", lti.Uk[0..$, 0]);
    writeln("UOptSlice: ", UOptSlice[0..print_length*mpc.nINPUTS, 0]);

    // Compute predicted next-timestep output values and store
    auto XBarkp1 = slice!Real(mpc.nSTATES, 1);
    auto UBark   = slice!Real(mpc.nINPUTS, 1);
    auto UBarkp1 = slice!Real(mpc.nINPUTS, 1);
    UBark[0..$, 0] = lti.UBarkm1[0..$, 0] + UOptSlice[0..mpc.nINPUTS, 0];
    if (mpc.HB == 0) XBarkp1[] = mtimes(lti.Adb, lti.XBar) + mtimes(lti.Bdb, UBark) + lti.F0db[];
    else             XBarkp1[] = mtimes(lti.Ad, lti.XBar) + mtimes(lti.Bd, UBark) + lti.F0d[];

    UBarkp1[] = UBark[] + UOptSlice[1*mpc.nINPUTS..2*mpc.nINPUTS];
    // Index k, since this is used NEXT timestep to find output error,
    // Don't need dk since Zk includes it? TODO: think this is OK but confirm..
    lti.ZHatk[] = mtimes(lti.C, XBarkp1) + mtimes(lti.D, UBarkp1) + lti.dk + lti.Zk; //FIX0924
    // lti.ZHatk[] = mtimes(lti.C, XBarkp1) + mtimes(lti.D, UBarkp1) + lti.Zk;
    writeln("ZHatk: ", lti.ZHatk);
}

Real[][] Convert2dNdsliceToDoubleArray(tdarr A)
{
    // Converts 2D mir ndslice to array of doubles
    ulong rows = A.length!0;
    ulong cols = A.length!1;
    Real[][] B = new Real[][](rows, cols);
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
            B[i][j] = A[i, j];
    }
    return B;
}

Real[] Convert1dNdsliceToDoubleArray(tdarr A)
{
    assert(A.length!1 == 1);
    ulong rows = A.length!0;
    Real[] B = new Real[](rows);
    for (int i = 0; i < rows; i++) B[i] = A[i, 0];
    return B;
}

int[] Convert1dNdsliceToIntArray(tdarr A)
{
    import std.conv: to;
    assert(A.length!1 == 1);
    ulong rows = A.length!0;
    int[] B = new int[](rows);
    for (int i = 0; i < rows; i++) B[i] = to!int(A[i, 0]);
    return B;
}

void MakeGurobiInputStruct(Mpc mpc, Q Qp, ref G Gur)
{
    // Makes struct of type 'G' that contains input QP matrices for Gurobi
    // 2D arrays must be reshaped to 1D and stored with their dimensions
    // Reshape H and BigOmega arrays
    int err;
    ulong lenH        = Qp.HObj.length!0 * Qp.HObj.length!1;
    ulong lenBigOmega = Qp.BigOmega.length!0 * Qp.BigOmega.length!1;
    tdarr HDbl        = slice!Real(lenH, 1);
    tdarr BigOmegaDbl = slice!Real(lenBigOmega, 1);
    HDbl              = Qp.HObj.reshape([lenH, 1], err);
    BigOmegaDbl       = Qp.BigOmega.reshape([lenBigOmega, 1], err);
    // Assign arrays, nRows, and nCols
    Gur.HDbl          = Convert1dNdsliceToDoubleArray(HDbl);
    Gur.GDbl          = Convert1dNdsliceToDoubleArray(Qp.GObj);
    Gur.BigOmegaDbl   = Convert1dNdsliceToDoubleArray(BigOmegaDbl);
    Gur.SmallOmegaDbl = Convert1dNdsliceToDoubleArray(Qp.SmallOmega);
    Gur.OmegaWDbl     = Convert1dNdsliceToDoubleArray(Qp.OmegaW);
    Gur.nRows = to!int(Qp.BigOmega.length!0);
    Gur.nCols = to!int(Qp.BigOmega.length!1);
}

void QuadraticFormTest(Q Qp, ref G )
{
    // Answer should be 9
    auto QTest = slice!Real(3, 3);
    QTest[] = [[1, 1, 0], [0, 1, 1], [0, 0, 1]];
    auto XTest = slice!Real(3, 1);
    XTest[] = [[2], [1], [1]];
    auto QTXQ = slice!Real(1, 1);
    QuadraticForm(XTest, QTest, QTXQ);
    writeln(QTXQ);
}

tdarr Add2dNdslice(tdarr A, tdarr B)
{
    // Adds Ndslice arrays 'A' and 'B'
    // Check dimensions are consistent
    assert(A.length!0 == B.length!0);
    assert(A.length!1 == B.length!1);
    auto C = slice!Real(A.length!0, A.length!1);
    for (int i = 0; i < A.length!0; i++)
    {
        for (int j = 0; j < A.length!1; j++)
        {
            C[i][j] = A[i][j] + B[i][j];
        }
    }
    return C;
}

tdarr NdsliceToPower(tdarr A, int p)
{
    // Raises square mir ndslice array 'A' to power 'p'
    assert(A.length!0 == A.length!1); // Must be square
    assert(p >= 0); // Exponent must be nonegative
    auto ARaised = A.slice; // Copy original matrix
    if (p == 0)
    {
        // Return identity matrix by convention
        ARaised[] = 0.0;
        ARaised.diagonal[] = 1.0;
        return ARaised;
    }
    else
    {
        // Regular behaviour
        for (int i = 0; i < p-1; i++) ARaised = mtimes(ARaised, A);
        return ARaised;
    }
}

tdarr ConstTimes2dNdslice(tdarr A, Real c)
{
    // Multiplies 2D Ndslice array 'c' by (possibly negative) constant 'c'
    auto Mpc = slice!Real(A.length!0, A.length!0);
    auto B = slice!Real(A.length!0, A.length!1);
    Mpc[] = 0.0;
    Mpc.diagonal[] = c;
    B = mtimes(Mpc, A);
    return B;
}

tdarr Eye(int n)
{
    // Makes an ndslice identity matrix of size n
    auto I = slice!Real(n, n);
    I[] = 0.0;
    I.diagonal[] = 1.0;
    return I;
}

void QuadraticForm(tdarr A, tdarr B, ref tdarr C)
{
    // Computes quadratic form C = A'BA using ndslice arrays
    assert(A.length!0 == B.length!1);
    assert(C.length!0 == A.length!1);
    assert(C.length!1 == A.length!1);
    C = mtimes(A.transposed, B.mtimes(A));
}

void AssembleBigQBigR(Mpc mpc, ref Q Qp)
{
    // Asssembles 'BigQ' and 'BigR' matrices for MPC formulation
    // Currently assumes
    //  1) Q and R are constant over all time steps,
    //  2) Hw = 1.
    Real[] QDiag;
    for (int i = 0; i < mpc.HP-1; i++) {
        if (i < mpc.HB) QDiag = join([QDiag, mpc.Q]);
        else            QDiag = join([QDiag, mpc.Qb]);
    }
    QDiag = join([QDiag, mpc.Qt]);
    Qp.BigQ[] = 0.0;
    Qp.BigQ.diagonal[] = QDiag;

    Real[] RDiag;
    for (int i = 0; i < mpc.HU; i++) {
        if (i < mpc.HB) RDiag = join([RDiag, mpc.R]);
        else            RDiag = join([RDiag, mpc.Rb]);
    }
    Qp.BigR[] = 0.0;
    Qp.BigR.diagonal[] = RDiag;
}

void AssembleBigCBigD(Mpc mpc, Lti lti, ref Q Qp)
{
    // Asssembles 'BigC' matrix for MPC formulation
    Qp.BigC[] = 0.0;
    Qp.BigD[] = 0.0;
    // Iterate through row blocks and fill in C matrix
    for (int i = 0; i < mpc.HP; i++)
    {
        Qp.BigC[i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS,
                i*mpc.nSTATES..(i+1)*mpc.nSTATES] = lti.C;
        Qp.BigD[i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS,
                i*mpc.nINPUTS..(i+1)*mpc.nINPUTS] = lti.D;
    }
}

void AssemblePsi(Mpc mpc, Lti lti, ref Q Qp)
{
    // Assembles Psi matrix from Eq 3.5 Maciejowski
    // Temp matrix to hold states before premultiplication by BigC
    auto PsiTemp = slice!Real(mpc.nSTATES*mpc.HP, mpc.nSTATES);
    for (int i = 0; i < mpc.HP; i++)
    {
        auto AToI = slice!Real(mpc.nSTATES, mpc.nSTATES);
        if (i < mpc.HB) AToI = NdsliceToPower(lti.Ad, i+1);
        else            AToI = NdsliceToPower(lti.Adb, i+1);
        PsiTemp[i*mpc.nSTATES..(i+1)*mpc.nSTATES, 0..$] = AToI;
    }
    Qp.Psi = mtimes(Qp.BigC, PsiTemp);
}

void AssembleUpsilonBe(Mpc mpc, Lti lti, ref Q Qp)
{
    // Assembles Upsilon matrix from Eq 3.5 Maciejowski
    // Temp matrix to hold states before premultiplication by BigC
    // FIX: Could add early opt-out for AjF terms if F0 ~= 0.
    auto BeTemp = slice!Real(mpc.nSTATES*mpc.HP, 1);
    foreach(i; 0..mpc.HP)
    {
        auto SumAjb = slice!Real(mpc.nSTATES, mpc.nINPUTS);
        auto SumAjF = slice!Real(mpc.nSTATES, 1);
        SumAjb[] = 0.0;
        SumAjF[] = 0.0;
        foreach(j; 0..i+1)
        {
            auto AToJ = slice!Real(mpc.nSTATES, mpc.nSTATES);
            if (i < mpc.HB) {
                AToJ = NdsliceToPower(lti.Ad, j);
                SumAjb[] = SumAjb[] + mtimes(AToJ, lti.Bd);
                SumAjF[] = SumAjF[] + mtimes(AToJ, lti.F0d);
            }
            else {
                AToJ = NdsliceToPower(lti.Adb, j);
                SumAjb[] = SumAjb[] + mtimes(AToJ, lti.Bdb);
                SumAjF[] = SumAjF[] + mtimes(AToJ, lti.F0db);
            }
        }
        Qp.CInvUpsilon[i*mpc.nSTATES..(i+1)*mpc.nSTATES, 0..$] = SumAjb;
        BeTemp[i*mpc.nSTATES..(i+1)*mpc.nSTATES, 0] = SumAjF[0..$, 0];
    }
    Qp.Upsilon = mtimes(Qp.BigC, Qp.CInvUpsilon);
    Qp.Be = mtimes(Qp.BigC, BeTemp);
}

void AssembleTheta(Mpc mpc, ref Q Qp)
{
    // Assembles Theta matrix similar to that in Eq 3.5 Maciejowski
    // This matrix is modified by adding 'Xi' to account for D matrix
    // Temp matrix to hold states before premultiplication by BigC
    auto ThetaTemp = slice!Real(mpc.nSTATES*mpc.HP, mpc.nINPUTS*mpc.HU);
    ThetaTemp[] = 0.0;
    foreach(col; 0..mpc.HU)
    {
        ThetaTemp[col*mpc.nSTATES..$, col*mpc.nINPUTS..(col+1)*mpc.nINPUTS] =
            Qp.CInvUpsilon[0..$-(col*mpc.nSTATES), 0..$];
    }
    Qp.Theta[] = mtimes(Qp.BigC, ThetaTemp) + Qp.Xi[];
}

void AssembleBeta(Mpc mpc, ref Q Qp)
{
    // Assembles Beta matrix from my formulation of MPC with the D matrix
    // Temp matrix to hold values before premultiplication by BigD
    auto BetaTemp = slice!Real(mpc.nINPUTS*mpc.HP, mpc.nINPUTS);
    auto I = slice!Real(mpc.nINPUTS, mpc.nINPUTS);
    I = Eye(mpc.nINPUTS);
    foreach(i; 0..mpc.HP)
    {
        BetaTemp[i*mpc.nINPUTS..(i+1)*mpc.nINPUTS, 0..$] = I;
    }
    Qp.Beta = mtimes(Qp.BigD, BetaTemp);
}

void AssembleXi(Mpc mpc, ref Q Qp)
{
    // Assembles Xi matrix from my formulation of MPC with the D matrix
    // Temp matrix to hold values before premultiplication by BigD
    auto XiTemp = slice!Real(mpc.nINPUTS*mpc.HP, mpc.nINPUTS*mpc.HU);
    XiTemp[] = 0.0;
    auto I = slice!Real(mpc.nINPUTS, mpc.nINPUTS);
    I = Eye(mpc.nINPUTS);
    int k = 0;
    foreach(i; 0..mpc.HP)
    {
        if(k < mpc.HU) { k++; }
        foreach(j; 0..k)
        {
            XiTemp[i*mpc.nINPUTS..(i+1)*mpc.nINPUTS,
                   j*mpc.nINPUTS..(j+1)*mpc.nINPUTS] = I;
        }
    }
    Qp.Xi = mtimes(Qp.BigD, XiTemp);
}

void AssembleDk(Mpc mpc, Lti lti, ref Q Qp)
{
    // Matrix used to account for steady-state prediction error
    foreach(i; 0..mpc.HP)
    {
        Qp.Dk[i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS, 0] = lti.dk[0..$, 0];
        // Qp.Dk[i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS, 0] = 0.0; // 0430, doesn't actually need
    }
}

void AssembleEpsilon(tdarr TRef, tdarr XBar, tdarr UBar, Mpc mpc, ref Q Qp)
{
    // Assembles Epsilon (uncontrolled tracking error) matrix from Eq 3.6
    // Maciejowski
    Qp.Epsilon[] = TRef[] - mtimes(Qp.Psi, XBar) - mtimes(Qp.Upsilon, UBar)
                   - mtimes(Qp.Beta, UBar) - Qp.Be[] - Qp.Dk[];

    auto PsiXBarPlusUpsilonU = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);
    PsiXBarPlusUpsilonU[] = mtimes(Qp.Psi, XBar) + mtimes(Qp.Upsilon, UBar);
}

void AssembleGobjHobj(Mpc mpc, ref Q Qp)
{
    // FIX: should rename these as ThetaBar and Phi
    // Assembles GObj and HObj matrices from Eq 3.11 - 3.12 Maciejowski
    Qp.GObj = mtimes(Qp.BigQ, Qp.Epsilon);
    Qp.GObj = mtimes(Qp.Theta.transposed, -2*Qp.GObj[]);

    auto HTemp = slice!Real(mpc.nINPUTS*mpc.HU, mpc.nINPUTS*mpc.HU);
    QuadraticForm(Qp.Theta, Qp.BigQ, HTemp);
    Qp.HObj = Add2dNdslice(HTemp, Qp.BigR);
}

tdarr AssembleBoundsVector(odarr Vars, tdarr BndsLow, tdarr BndsHigh,
        int nElements, int horizon)
{
    // Assembles lower and upper bounds vectors for E, F, and G matrices
    // nElements is nINPUTS, nOUTPUTS, or nSTATES
    // horizon is HU or HP
    int err;
    int length = 2*nElements;
    Real c = -1.0;
    BndsHigh = ConstTimes2dNdslice(BndsHigh, c);
    auto Vec = concatenation!1(BndsLow, BndsHigh).slice;
    Vec = Vec.reshape([length, 1], err);

    // Vars toggles constraint on or off, '*' is elementwise here
    Vec[0..$, 0] = Vec[0..$, 0] * Vars[];

    // Concatenate (for each prediction step)
    auto BndsVec = Vec;
    for (int i = 0; i < horizon-1; i++)
        BndsVec  = concatenation!0(BndsVec, Vec).slice;

    return BndsVec;
}

tdarr AssembleBlockedBoundsVector(odarr Vars, tdarr BndsLow, tdarr BndsHigh,
        tdarr BndsLowB, tdarr BndsHighB,
        int nElements, int horizon, int horizonBlocked)
{
    // Assembles lower and upper bounds vectors for E, F, and G matrices
    // nElements is nINPUTS, nOUTPUTS, or nSTATES
    // horizon is HU or HP
    int err;
    int length = 2*nElements;
    Real c = -1.0;
    BndsHigh = ConstTimes2dNdslice(BndsHigh, c);
    auto Vec = concatenation!1(BndsLow, BndsHigh).slice;
    Vec = Vec.reshape([length, 1], err);
    BndsHighB = ConstTimes2dNdslice(BndsHighB, c);
    auto VecB = concatenation!1(BndsLowB, BndsHighB).slice;
    VecB = VecB.reshape([length, 1], err);
    // TODO: Not sure about this trailing 'B' notation...

    // Vars toggles constraint on or off, '*' is elementwise here
    Vec[0..$, 0] = Vec[0..$, 0] * Vars[];
    VecB[0..$, 0] = VecB[0..$, 0] * Vars[];

    // Concatenate (for each prediction step)
    auto BndsVec = Vec;
    for (int i = 0; i < horizon-1; i++)
    {
        if (i < horizonBlocked) BndsVec = concatenation!0(BndsVec, Vec).slice;
        else                    BndsVec = concatenation!0(BndsVec, VecB).slice;
    }

    return BndsVec;
}


tdarr AssembleWeightsVector(tdarr WLow, tdarr WHigh, int nElements, int horizon)
{
    // Assembles constraint weights vectors for E, F, and G matrices.
    // nElements is nINPUTS, nOUTPUTS, or nSTATES.
    // horizon is HU or HP.
    int err;
    int length = 2*nElements;
    auto Vec = concatenation!1(WLow, WHigh).slice;
    Vec = Vec.reshape([length, 1], err);
    // Concatenate (for each prediction step)
    auto WVec = Vec;
    for (int i = 0; i < horizon-1; i++) WVec = concatenation!0(WVec, Vec).slice;
    return WVec;
}

tdarr AssembleConstrainedVarsArray(odarr VarsVec, int nElements)
{
    // Assembles left hand side template for E, F, and G matrices.
    // These consist of -1s and 1s to select lower and upper bounded variables
    // respectively
    // nElements is nINPUTS, nOUTPUTS, or nSTATES
    // Varsvec is UVars, DUVars, or ZVars
    // Make templates to select constrained inputs and outputs
    auto Template = slice!Real(2*nElements, nElements);
    Template[] = 0.0;
    for (int i = 0; i < nElements; i++) Template[i*2..i*2+2, i] = [-1.0, 1.0];

    // Multiply by selection matrices to choose which variables are constrained
    auto VarsArr = slice!Real(2*nElements, 2*nElements);
    VarsArr[] = 0.0;
    VarsArr.diagonal[] = VarsVec;
    Template = mtimes(VarsArr, Template);
    return Template;
}

/*
 * G takes the form G = [GSimple, GCombined]^T.
 * GSimple contains simple min/max constraints on Z, while GCombined contains
 * aribitrary linear inequalities involving outputs.
 */
void AssembleEFG(Mpc mpc, ref Q Qp)
{
    // Assembles E, F, and G constraint matrices from Eq 2.21 - 2.23 Maciejowski
    Qp.E[] = 0.0;
    Qp.F[] = 0.0;
    Qp.G[] = 0.0;

    // Make templates to select constrained inputs and outputs
    auto DUTemplate = slice!Real(2*mpc.nINPUTS, mpc.nINPUTS);
    auto UTemplate  = slice!Real(2*mpc.nINPUTS, mpc.nINPUTS);
    auto ZTemplate  = slice!Real(2*mpc.nOUTPUTS, mpc.nOUTPUTS);
    UTemplate  = AssembleConstrainedVarsArray(mpc.UVars, mpc.nINPUTS);
    DUTemplate = AssembleConstrainedVarsArray(mpc.DUVars, mpc.nINPUTS);
    ZTemplate  = AssembleConstrainedVarsArray(mpc.ZVars, mpc.nOUTPUTS);

    // Assemble bounds matrices
    auto DUBnds = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    auto UBnds  = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    auto ZBnds  = slice!Real(2*mpc.nOUTPUTS*mpc.HP, 1);
    DUBnds = AssembleBlockedBoundsVector(
            mpc.DUVars, mpc.DULow, mpc.DUHigh, mpc.DULowB, mpc.DUHighB,
            mpc.nINPUTS, mpc.HU, mpc.HB);
    UBnds  = AssembleBoundsVector(mpc.UVars, mpc.ULow, mpc.UHigh, mpc.nINPUTS, mpc.HU);
    ZBnds  = AssembleBoundsVector(mpc.ZVars, mpc.ZLow, mpc.ZHigh, mpc.nOUTPUTS, mpc.HP);

    // TODO: Manually handle DU currently due to blocking

    // Assemble weights matrices
    auto DUW = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    auto UW  = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    auto ZW  = slice!Real(2*mpc.nOUTPUTS*mpc.HP, 1);
    DUW = AssembleWeightsVector(mpc.DULowW, mpc.DUHighW, mpc.nINPUTS, mpc.HU);
    UW  = AssembleWeightsVector(mpc.ULowW, mpc.UHighW, mpc.nINPUTS, mpc.HU);
    ZW  = AssembleWeightsVector(mpc.ZLowW, mpc.ZHighW, mpc.nOUTPUTS, mpc.HP);

    // Fill E, F, and G matrices (GSimple section)
    // U and DeltaU have same dimensionality so can fill E and F together
    for (int i = 0; i < mpc.HU; i++) {
        Qp.E[i*2*mpc.nINPUTS..(i+1)*2*mpc.nINPUTS,
             i*mpc.nINPUTS..(i+1)*mpc.nINPUTS] = DUTemplate;
        Qp.F[i*2*mpc.nINPUTS..(i+1)*2*mpc.nINPUTS,
             i*mpc.nINPUTS..(i+1)*mpc.nINPUTS] = UTemplate;
    }
    for (int i = 0; i < mpc.HP; i++) {
        Qp.G[i*2*mpc.nOUTPUTS..(i+1)*2*mpc.nOUTPUTS,
             i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS] = ZTemplate;
    }
    Qp.E[0..$,$-1..$] = DUBnds;
    Qp.F[0..$,$-1..$] = UBnds;
    Qp.G[0..2*mpc.HP*mpc.nOUTPUTS,$-1..$] = ZBnds; // GSimple
    Qp.EW[0..$,0..$] = DUW;
    Qp.FW[0..$,0..$] = UW;
    Qp.GW[0..2*mpc.HP*mpc.nOUTPUTS,0..$] = ZW;

    // Generalized output constraints
    // There are 2*HP*nOUTS rows for top section of z
    // There are HP*nCONS rows for bottom section of z
    uint row_start = 2*mpc.HP*mpc.nOUTPUTS;
    foreach(i; 0..mpc.HP) {
        Qp.G[row_start+(i*mpc.nCONSTRAINTS)..row_start+((i+1)*mpc.nCONSTRAINTS),
             i*mpc.nOUTPUTS..(i+1)*mpc.nOUTPUTS] = mpc.GammaL;
        Qp.G[row_start+(i*mpc.nCONSTRAINTS)..row_start+((i+1)*mpc.nCONSTRAINTS),
            $-1..$] = mpc.gL;
        Qp.GW[row_start+(i*mpc.nCONSTRAINTS)..row_start+((i+1)*mpc.nCONSTRAINTS),
            0..$] = mpc.GammaLW;
    }
}

void AssembleFBar(Mpc mpc, ref Q Qp)
{
    auto Fi = slice!Real(2*mpc.nINPUTS*mpc.HU, mpc.nINPUTS);
    auto FBari = slice!Real(2*mpc.nINPUTS*mpc.HU, mpc.nINPUTS);
    FBari[] = 0.0;
    for (int i = mpc.HU; i > 0; i--)
    {
        Fi[] = Qp.F[0..$,(i-1)*mpc.nINPUTS..i*mpc.nINPUTS];
        FBari = Add2dNdslice(FBari, Fi);
        Qp.FBar[0..$,(i-1)*mpc.nINPUTS..i*mpc.nINPUTS] = FBari;
    }
}

void AssembleFBarMax(Mpc mpc, ref Q Qp, tdarr UBar)
{
    // FIX: Should add comments similar to in AssembleGammaThetaMax so that I
    // can follow mathematical working more easily
    auto FTemp = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    auto f = slice!Real(2*mpc.nINPUTS*mpc.HU, 1);
    FTemp = mtimes(Qp.F[0..$, 0..mpc.nINPUTS], UBar);
    f[] = Qp.F[0..$,$-1..$];
    FTemp = Add2dNdslice(FTemp, f);
    Real c = -1.0; // Multiply by -1
    Qp.FBarMax = ConstTimes2dNdslice(FTemp, c);
}

void AssembleGammaThetaMax(tdarr XBar, tdarr UBar, Mpc mpc, ref Q Qp)
{
    auto GammaThetaMaxTemp = slice!Real((2*mpc.nOUTPUTS + mpc.nCONSTRAINTS)*mpc.HP, 1);
    auto g = slice!Real((2*mpc.nOUTPUTS + mpc.nCONSTRAINTS)*mpc.HP, 1);
    auto FreeResponse = slice!Real(mpc.nOUTPUTS*mpc.HP, 1);

    // TODO: Could store free response from before.
    FreeResponse[] = mtimes(Qp.Psi, XBar) + mtimes(Qp.Upsilon, UBar) +
        Qp.Be[] + Qp.Dk[];
    GammaThetaMaxTemp[] = mtimes(Qp.Gamma, FreeResponse);
    g[] = Qp.G[0..$,$-1..$];
    Qp.GammaThetaMax[] = -1*GammaThetaMaxTemp[] + -1*g[];
}

void AssembleOmegaMax(Mpc mpc, ref Q Qp)
{
    Qp.WMax[] = -1.0 * Qp.E[0..$, $-1..$];
}

void AssembleOmegas(Mpc mpc, ref Q Qp)
{
    // Assembles 'Omega' and 'omega' matrices from Maciejowski Eq. 3.44
    // Omega = [Fbar; Gamma*Theta; W]
    // omega = [FbarMax; GammaThetaMax; WMax]
    auto GammaTheta = slice!Real((2*mpc.nOUTPUTS + mpc.nCONSTRAINTS)*mpc.HP, mpc.nINPUTS*mpc.HU);
    GammaTheta = mtimes(Qp.Gamma, Qp.Theta);

    // TODO: Not needed?
    foreach(i; 0..GammaTheta.length!0)
    {
        import std.math: abs;
        double max_val = 0.0;
        foreach(j; 0..GammaTheta.length!1)
            if(abs(GammaTheta[i, j]) > max_val) max_val = abs(GammaTheta[i, j]);
    }

    Qp.BigOmega = concatenation!0(Qp.FBar, GammaTheta, Qp.W).slice;
    Qp.SmallOmega = concatenation!0(Qp.FBarMax, Qp.GammaThetaMax, Qp.WMax).slice;
    Qp.OmegaW = concatenation!0(Qp.FW, Qp.GW, Qp.EW).slice;

    // import std.stdio;
    // writeln("\n\nPrinting constraint matrices...\n");
    // writeln("\nFBar = ");
    // foreach(i; 0..Qp.FBar.length!0) writeln(Qp.FBar[i, 0..$]);
    // writeln("\nFBarMax = ");
    // foreach(i; 0..Qp.FBarMax.length!0) writeln(Qp.FBarMax[i, 0..$]);
    // writeln("\nGamma*Theta = ");
    // foreach(i; 0..GammaTheta.length!0) writeln(GammaTheta[i, 0..$]);
    // writeln("\nGammaThetaMax = ");
    // foreach(i; 0..Qp.GammaThetaMax.length!0) writeln(Qp.GammaThetaMax[i, 0..$]);
    // writeln("\nW = ");
    // foreach(i; 0..Qp.W.length!0) writeln(Qp.W[i, 0..$]);
    // writeln("\nWMax = ");
    // foreach(i; 0..Qp.WMax.length!0) writeln(Qp.WMax[i, 0..$]);

}

void PrintQpFormulationMatrices(Q Qp)
{
    // Currently only includes some of the matrices.
    import std.stdio;
    writeln("\n\nPrinting matrices used to form QP...\n");
    writeln("\nObjective matrices:");
    writeln("\nEpsilon = ");
    foreach(i; 0..Qp.Epsilon.length!0) writeln(Qp.Epsilon[i, 0..$]);
    writeln("\nTheta = ");
    foreach(i; 0..Qp.Theta.length!0) writeln(Qp.Theta[i, 0..$]);
    writeln("\nBigQ = ");
    foreach(i; 0..Qp.BigQ.length!0) writeln(Qp.BigQ[i, 0..$]);
    writeln("\nBigR = ");
    foreach(i; 0..Qp.BigR.length!0) writeln(Qp.BigR[i, 0..$]);
    // writeln("\nHObj = ");
    // foreach(i; 0..Qp.HObj.length!0) writeln(Qp.HObj[i, 0..$]);
}

void PrintQpMatrices(Q Qp)
{
    import std.stdio;
    writeln("\n\nPrinting QP matrices...\n");
    writeln("\nObjective matrices:");
    writeln("\nHObj = ");
    foreach(i; 0..Qp.HObj.length!0) writeln(Qp.HObj[i, 0..$]);
    writeln("\nGObj = ");
    foreach(i; 0..Qp.GObj.length!0) writeln(Qp.GObj[i, 0..$]);
    writeln("\nConstraint matrices:");
    writeln("\nBigOmega = ");
    foreach(i; 0..Qp.BigOmega.length!0) writeln(Qp.BigOmega[i, 0..$]);
    writeln("\nSmallOmega = ");
    foreach(i; 0..Qp.SmallOmega.length!0) writeln(Qp.SmallOmega[i, 0..$]);
    writeln("\n");
}



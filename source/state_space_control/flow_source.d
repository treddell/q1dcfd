module q1dcfd.state_space_control.flow_source;

import q1dcfd.state_space_control.observer: IObserver;
import q1dcfd.state_space_control.lti: Lti;
import q1dcfd.state_space_control.mpc: Mpc;
import q1dcfd.state_space_control.stream: Stream;

/*
 * 'FlowSource' objects compute LTI models for the inlet conditions of streams:
 * p_in, e_in, mdot.
 * They also compute LTI models for any of their own internal state variables.
 * Currently, there are two types:
 * - pump
 * - turbomachine pair.
 *
 * Turbomachines must be compared since their time derivatives are
 * interdependent.
 * Current implementation for turbomachine pairs is valid for an open cycle
 * (i.e. compressor inlet from constant prop reservoir and turbine outlet to
 * constant prop reservoir).
 */
abstract class FlowSource
{
public:
    immutable string name;

protected:
    string[] z_vars;
    immutable uint n_cntrl_outs, n_constraints;
    uint z_index_start, z_index_end;;
    uint c_index_start, c_index_end;;
    string[] _output_array;
    uint[string] cntrl_out_map;

public:
    this(
        string name,
        string[] out_arr,
        string[] z_vars,
        immutable uint n_constraints)
    {
        import std.algorithm.searching: canFind;

        this.name = name;
        this._output_array = out_arr;
        this.z_vars = z_vars;
        this.n_constraints = n_constraints;
        n_cntrl_outs = cast(uint) z_vars.length;

        // Check controlled outputs and form mapping
        foreach(i, var; z_vars)
        {
            if(! _output_array.canFind(var))
            {
                import std.format;
                throw new Exception(format!(
                    "ERROR: Invalid controlled output %s in FlowSource %s.")
                    (var, name));
            }
            cntrl_out_map[var] = cast(uint) i;
        }
    }

    void update(Lti lti, Mpc mpc)
    {
        update_lti_model(lti);
        update_constraints(mpc);
    }

    abstract void update_lti_model(Lti lti);

    void update_constraints(ref Mpc mpc) {}

    uint populate_constraint_indices(uint c_in)
    {
        if(n_constraints > 0)
        {
            c_index_start = c_in;
            c_index_end = c_index_start + n_constraints - 1;
            return c_index_end + 1;
        }
        else
        {
            c_index_start = -1;
            c_index_end = -1;
            return c_in;
        }
    }

    uint populate_cntrl_out_indices(uint z_in)
    {
        if(n_cntrl_outs > 0)
        {
            z_index_start = z_in;
            z_index_end = z_index_start + n_cntrl_outs - 1;
            return z_index_end + 1;
        }
        else
        {
            z_index_start = -1;
            z_index_end = -1;
            return z_in;
        }
    }

    /* Any initialization routines that need observer state estimates */
    void initialise() {}
}

class FlowSourceDef
{
    string name, type;
    string[string] observer_dict;
    string[string] stream_dict;
    string[] z_vars;

    this(
        string type,
        string name,
        string[string] observer_dict,
        string[string] stream_dict,
        string[] z_vars = new string[0])
    {
        if(!(type == "pump" || type == "turbomachines" || type == "turbomachinery_flow"))
        {
            import std.format;
            throw new Exception(format!(
                "ERROR: 'FlowSource' must be of type 'pump', 'turbomachinery_flow', "
                ~ "'turbomachinery_flow' but flow source object %s has type input %s.")
                (name, type));
        }

        if(type == "pump")
        {
            foreach(key; observer_dict.byKey)
            {
                if(key != "pump")
                    throw new Exception("ERROR: Observer type " ~ key ~ " is not "
                        ~ "valid for a pump flow source");
            }
        }

        this.type = type;
        this.name = name;
        this.observer_dict = observer_dict;
        this.stream_dict = stream_dict;
        this.z_vars = z_vars;
    }
}

FlowSource build_flow_source(
    FlowSourceDef def,
    IObserver[string] observer_map,
    Stream[string] stream_map)
{
    switch(def.type)
    {
        case "pump":
            return new Pump(def.name, observer_map[def.observer_dict["pump"]],
                    def.z_vars);
        case "turbomachines":
            return new TurbomachinePair(def.name,
                    observer_map[def.observer_dict["compressor"]],
                    observer_map[def.observer_dict["turbine"]],
                    def.z_vars);
        case "turbomachinery_flow":
            return new TurbomachineryFlow(def.name,
                    observer_map[def.observer_dict["compressor"]],
                    observer_map[def.observer_dict["turbine"]],
                    stream_map[def.stream_dict["hp_stream"]],
                    def.z_vars);
        default:
            import std.format;
            throw new Exception(format!(
                "ERROR: 'FlowSource' must be of type 'pump' or 'turbomachines', "
                ~ "but flow source object %s has type input %s.")(def.name, def.type));
    }
}

class Pump : FlowSource
{
    import q1dcfd.config: Real;
    import q1dcfd.state_space_control: PumpObserver;

protected:
    IObserver pump_observer;
    immutable Real i_rotor, c_rotor, k_rotor;
    uint[string] pump_to_global; // global state indices of pump vars

public:
    this(string name, IObserver observer, string[] z_vars)
    {
        immutable uint n_constrs_tmp = 0;
        string[] output_array_tmp;
        super(name, output_array_tmp, z_vars, n_constrs_tmp);

        this.pump_observer = observer;

        auto pump_observer = cast(PumpObserver) observer;
        Real[string] pump_dict = pump_observer.get_pump_params_dict();
        i_rotor = pump_dict["i_rotor"];
        k_rotor = pump_dict["k_rotor"];
        c_rotor = pump_dict["c_rotor"];

        pump_to_global = [
            "e_out"       : observer.global_index(0, "e_out"),
            "p_out"       : observer.global_index(0, "p_out"),
            "mdot"        : observer.global_index(0, "mdot"),
            "mdot_dot"    : observer.global_index(0, "mdot_dot"),
            "mdot_target" : observer.u_global_index("mdot_target")];
    }

protected:
    override void update_lti_model(Lti lti)
    {
        lti.A[pump_to_global["mdot"], pump_to_global["mdot_dot"]] = 1.0;
        lti.A[pump_to_global["mdot_dot"], pump_to_global["mdot"]] = -k_rotor / i_rotor;
        lti.A[pump_to_global["mdot_dot"], pump_to_global["mdot_dot"]] = -c_rotor / i_rotor;

        lti.B[pump_to_global["mdot_dot"], pump_to_global["mdot_target"]] = k_rotor / i_rotor;
    }
}

class TurbomachineryFlow : FlowSource
{
    import mir.ndslice;
    import mir.interpolate.linear;
    import lubeck;

    import q1dcfd.config: Real;
    import q1dcfd.state_space_control: CompressorObserver, TurbineObserver;
    import q1dcfd.maps: Compressor, Turbine;

    alias odarr = Slice!(Real*, 1);
    alias iodarr = Slice!(immutable(Real)*, 1);

    // TODO: These should be private (hack for saving).
    Real N_min_0 = 1; // Overwritten, guess for root finder
    Real N_max, dN_min_dT_tin;

protected:
    CompressorObserver compressor;
    TurbineObserver turbine;
    Compressor comp_model; // map model
    Turbine turb_model; // map model

    Stream hp_stream;
    uint[string] fs_to_global;
    Linear!(Real, 1, immutable(Real)*) mdot_star_interp;
    Linear!(Real, 1, immutable(Real)*) P_high_star_interp;

public:
    this(
        string name,
        IObserver compressor_observer,
        IObserver turbine_observer,
        Stream hp_stream,
        string[] z_vars)
    {
        immutable uint n_constrs_tmp = 2;
        auto output_array_tmp = ["P_net", "P_comp", "P_turb", "eta", "delta_mdot"];
        super(name, output_array_tmp, z_vars, n_constrs_tmp);

        this.compressor = cast(CompressorObserver) compressor_observer;
        this.turbine    = cast(TurbineObserver) turbine_observer;
        this.hp_stream  = cast(Stream) hp_stream;

        comp_model = cast(Compressor) compressor.turbomachine_model;
        turb_model = cast(Turbine) turbine.turbomachine_model;

        fs_to_global = [
            "compressor_e_out"   : compressor_observer.global_index(0, "e_out"),
            "compressor_p_out"   : compressor_observer.global_index(0, "p_out"),
            "compressor_mdot"    : compressor_observer.global_index(0, "mdot"),
            "compressor_speed"   : compressor_observer.global_index(0, "speed"),
            "turbine_e_out"      : turbine_observer.global_index(0, "e_out"),
            "turbine_p_out"      : turbine_observer.global_index(0, "p_out"),
            "turbine_mdot"       : turbine_observer.global_index(0, "mdot"),
            "turbine_speed"      : turbine_observer.global_index(0, "speed"),
            "compressor_T_motor" : compressor_observer.global_index(0, "T_motor"),
            "compressor_dT_motor_dt" : compressor_observer.u_global_index("dT_motor_dt")
        ];
    }

    override void update_constraints(ref Mpc mpc)
    {
        // Update the min and max speed constraints based on current op point
        immutable H = 1E-8;
        Real mdot_diff = turbine.mdot - compressor.mdot;
        N_max = comp_model.compute_max_speed(compressor.p_in, compressor.T_in);
        N_min_0 = compute_N_min(turbine.T_in, N_min_0, mdot_diff);
        Real N_min_plus_T_t_in = compute_N_min((1+H) * turbine.T_in, N_min_0, mdot_diff);
        dN_min_dT_tin = (N_min_plus_T_t_in - N_min_0) / (H * turbine.T_in);
        Real N_min_plus_mdot_diff = compute_N_min(turbine.T_in, N_min_0, (1+H) * mdot_diff);
        Real dN_min_ddelta_mdot = (N_min_plus_mdot_diff - N_min_0) / (H * mdot_diff);

        // Speed must be greater than speed_min.
        //  kN = 1 + speed_margin
        //  speed >=  kN * (speed_min_0 + dN_min_dT_tin * T_t_in_bar + ...)
        // Reformulate for general constraint as
        //  0 <= (kN * speed_min_0 - speed_0) - speed_bar + (kN * dN_min_dT_tin) * T_tin_bar)
        immutable speed_margin_low = 0.06;
        immutable weight_low = 1E7;
        immutable k_N = 1 + speed_margin_low;
        auto row = c_index_start; // First constraint.
        mpc.GammaL[row, compressor.z_global_index("speed")] = -1.0;
        mpc.gL[row, 0] = (k_N * N_min_0 - compressor.speed);
        uint i_e_turb_in = hp_stream.z_global_index("e_out");
        uint i_p_turb_in = compressor.z_global_index("p_out");
        // mpc.GammaL[row, i_e_turb_in] = dN_min_dT_tin * turbine.dT_in_de_in;
        // mpc.GammaL[row, i_p_turb_in] = dN_min_dT_tin * turbine.dT_in_dp_in; // Back in?
        // mpc.GammaL[row, fs_to_global["turbine_mdot"]] = dN_min_ddelta_mdot;
        // mpc.GammaL[row, fs_to_global["compressor_mdot"]] = -1.0 * dN_min_ddelta_mdot;
        mpc.GammaLW[row, 0] = weight_low;
        mpc.ZLow[compressor.z_global_index("speed"), 0] = (1 + speed_margin_low) * N_min_0; // Display

        // Speed must be less than speed_max.
        //     speed_0 + speed_bar <= speed_max
        // Reformulate for general constraint as
        //     speed_0 + speed_bar - (1 - speed_margin) * N_max <= 0
        immutable speed_margin_high = 0.06;
        immutable weight_high = 1E7;
        row++;
        mpc.GammaL[row, compressor.z_global_index("speed")] = 1.0;
        mpc.gL[row, 0] = compressor.speed - (1 - speed_margin_high) * N_max;
        mpc.GammaLW[row, 0] = weight_high;
        mpc.ZHigh[compressor.z_global_index("speed"), 0] = (1 - speed_margin_high) * N_max; // Display

        // // To cross-check, simple constraints.
        // // Before, 0.06 margin and 0.01 weight.
        // immutable speed_margin_low = 0.10;
        // mpc.ZVars[2*compressor.z_global_index("speed")] = 1;
        // mpc.ZLow[compressor.z_global_index("speed"), 0] = (1 + speed_margin_low) * N_min_0;
        // mpc.ZLowW[compressor.z_global_index("speed"), 0] = 1E3;
        // immutable speed_margin_high = 0.06;
        // mpc.ZVars[2*compressor.z_global_index("speed")+1] = 1;
        // mpc.ZHigh[compressor.z_global_index("speed"), 0] = (1 - speed_margin_high) * N_max;
        // mpc.ZHighW[compressor.z_global_index("speed"), 0] = 0.0025; // was 0.005, or 0.0001

    }

    override void initialise()
    {
        compute_compressor_constraint_lines();
    }

protected:
    /*
     * This function assumes synchronous turbine operation.
     *
     * This LTI model assumes that the inlet temperatures do not vary much and
     * have little effect on mdot and p_high.
     * These assumptions will need to be relaxed for closed-cycle modelling.
     *
     * This function forms LTI model only for variables of interest to open
     * cycle: mdot (should be ~= for comp and turb), compressor speed,
     * compressor p_out ( = turbine p_in), compressor T_out.
     */
    override void update_lti_model(Lti lti)
    {
        enum M { FULL, FAST_NEW, FAST_OLD, BASIC }
        M model;
        model = M.FAST_NEW;
        // model = M.BASIC;

        switch(model)
        {
        case M.FULL: // REALLY ADVANCED MODEL
        {
            // Motor torque varies as first-order hold (piecewise linear)
            lti.F0[fs_to_global["compressor_T_motor"], 0] = compressor.dT_motor_dt;
            lti.B[fs_to_global["compressor_T_motor"], fs_to_global["compressor_dT_motor_dt"]] = 1.0;

            // Compute speed derivative (computed first, since required for many other eqs)
            lti.F0[fs_to_global["compressor_speed"], 0] =
                    (compressor.T_motor - compressor.T_load) / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] =
                    - compressor.dT_load_dspeed / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] =
                    - compressor.dT_load_dmdot / (compressor.i_rotor * compressor.speed_nominal);
            // Compressor T_in term in A matrix is assumed negligble here
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] =
                    1 / (compressor.i_rotor * compressor.speed_nominal);

            // Compressor mass flow rate (used in compresssor mass flow rate eq)
            Real sum_vol_x_drho_dp_high = 0.0;
            // Start at i=1 as cell 0 is `flow source' cell
            foreach(i; 1..hp_stream.n_cells)
                sum_vol_x_drho_dp_high += hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhodp[i];
            Real mdot_comp_scale = 1 / (sum_vol_x_drho_dp_high * compressor.dp_out_dmdot);
            lti.F0[fs_to_global["compressor_mdot"], 0] = mdot_comp_scale * (compressor.mdot - turbine.mdot);
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] = mdot_comp_scale;
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["turbine_mdot"]] = -1 * mdot_comp_scale;
            // // Derivatives with respect to hp stream internal energies; over HP stream:
            // //      sum <mdot_turb_scale> * Vi * drhode_i
            // foreach(i; 1..hp_stream.n_cells)
            // {
                // Real e_coeff = -1 * mdot_comp_scale * hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhode[i];
                // lti.A[fs_to_global["compressor_mdot"], 0..$] =
                    // lti.A[fs_to_global["compressor_mdot"], 0..$]
                    // + e_coeff * lti.A[hp_stream.stream_global_index("e", i), 0..$];
                // lti.F0[fs_to_global["compressor_mdot"], 0] =
                    // lti.F0[fs_to_global["compressor_mdot"], 0]
                    // + e_coeff * lti.F0[hp_stream.stream_global_index("e", i), 0];
            // }
            // dN_c_dt_term
            Real mdot_c_scale_wrt_N_c = -1 * (1 / compressor.dp_out_dmdot) * compressor.dp_out_dspeed;
            lti.A[fs_to_global["compressor_mdot"], 0..$] = lti.A[fs_to_global["compressor_mdot"], 0..$]
                    + mdot_c_scale_wrt_N_c * lti.A[fs_to_global["compressor_speed"], 0..$];
            lti.F0[fs_to_global["compressor_mdot"], 0] = lti.F0[fs_to_global["compressor_mdot"], 0]
                    + mdot_c_scale_wrt_N_c * lti.F0[fs_to_global["compressor_speed"], 0];

            // Compressor p_out (=p_high) and e_out
            Real p_scale_wrt_speed = compressor.dp_out_dspeed;
            Real p_scale_wrt_mdot = compressor.dp_out_dmdot;
            Real e_scale_wrt_speed = compressor.de_out_dspeed;
            Real e_scale_wrt_mdot  = compressor.de_out_dmdot;

            // Pressure (calculated from compressor)
            lti.F0[fs_to_global["compressor_p_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * p_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

            // Compressor e_out
            lti.F0[fs_to_global["compressor_e_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * e_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * e_scale_wrt_mdot;

            // Turbine mass flow rate (now that p_out derivatives are available); beware double negatives!
            Real mdot_t_scale_wrt_speed_c = (1 / turbine.dp_in_dmdot) * compressor.dp_out_dspeed;
            Real mdot_t_scale_wrt_mdot_c = (1 / turbine.dp_in_dmdot) * compressor.dp_out_dmdot;
            Real mdot_t_scale_wrt_T_in_t = (1 / turbine.dp_in_dmdot) * (-1 * turbine.dp_in_dT_in);
            Real mdot_t_scale_wrt_e_in_t = mdot_t_scale_wrt_T_in_t * hp_stream.props.dTde[$-1];
            Real mdot_t_scale_wrt_p_high = mdot_t_scale_wrt_T_in_t * hp_stream.props.dTdp[$-1];
            lti.F0[fs_to_global["turbine_mdot"], 0] =
                    mdot_t_scale_wrt_speed_c * lti.F0[fs_to_global["compressor_speed"], 0]
                    + mdot_t_scale_wrt_mdot_c  * lti.F0[fs_to_global["compressor_mdot"], 0]
                    // + mdot_t_scale_wrt_e_in_t  * lti.F0[hp_stream.stream_global_index("e", hp_stream.n_cells-1), 0]
                    + mdot_t_scale_wrt_p_high  * lti.F0[fs_to_global["compressor_p_out"], 0];
            lti.A[fs_to_global["turbine_mdot"], 0..$] =
                    mdot_t_scale_wrt_speed_c * lti.A[fs_to_global["compressor_speed"], 0..$]
                    + mdot_t_scale_wrt_mdot_c  * lti.A[fs_to_global["compressor_mdot"], 0..$]
                    // + mdot_t_scale_wrt_e_in_t  * lti.A[hp_stream.stream_global_index("e", hp_stream.n_cells-1), 0..$]
                    + mdot_t_scale_wrt_p_high  * lti.A[fs_to_global["compressor_p_out"], 0..$];
            break;
        }

        //----------------------------------------------------------------------
        case M.FAST_NEW: // ADVANCED MODEL, FAST TIMESCALE, BUT MORE RIGOROUS
        {
            // Motor torque varies as first-order hold (piecewise linear)
            lti.F0[fs_to_global["compressor_T_motor"], 0] = compressor.dT_motor_dt;
            lti.B[fs_to_global["compressor_T_motor"], fs_to_global["compressor_dT_motor_dt"]] = 1.0;

            // Compute speed derivative (computed first, since required for many other eqs)
            lti.F0[fs_to_global["compressor_speed"], 0] =
                    (compressor.T_motor - compressor.T_load) / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] =
                    - compressor.dT_load_dspeed / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] =
                    - compressor.dT_load_dmdot / (compressor.i_rotor * compressor.speed_nominal);
            // Compressor T_in term in A matrix is assumed negligble here
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] =
                    1 / (compressor.i_rotor * compressor.speed_nominal);

            // Turbine mass flow rate (used in compresssor mass flow rate eq)
            // For turbine, ddelta_p_dmdot = -1 * dp_in_dmdot  [used in eqs below].
            Real sum_vol_x_drho_dp_high = 0.0;
            // Start at i=1 as cell 0 is `flow source' cell
            foreach(i; 1..hp_stream.n_cells)
                sum_vol_x_drho_dp_high += hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhodp[i];
            // Account for changing high-side density, assuming that dedt stays
            // constant [not true but hopefully close enough].
            //   dmdot = sum Vi drho_dei dedti
            Real dmdot = 0.0;
            foreach(i; 1..hp_stream.n_cells) // dxdt is dedt (x means state vars)
                dmdot += hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhode[i] * hp_stream.props.dxdt[i];
            Real p_scale_wrt_mdot_t = turbine.dp_in_dmdot; // TODO: new approach
            // Real p_scale_wrt_mdot_t = -1 * (1 / (1 + (-1 * turbine.dp_in_dT_in * turbine.dT_in_dp_in))) * -1 * turbine.dp_in_dmdot; // Old approach
            Real mdot_turb_scale = (1 / sum_vol_x_drho_dp_high) * (1 / p_scale_wrt_mdot_t);
            lti.F0[fs_to_global["turbine_mdot"], 0] = mdot_turb_scale * (compressor.mdot - turbine.mdot - dmdot);
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_mdot"]] = mdot_turb_scale;
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["turbine_mdot"]] = -1 * mdot_turb_scale;

            import std.stdio;
            writeln("\n\ndelta_mdot = ", dmdot, "\n");

            // Pressure (calculated from turbine); p_out_compressor=p_high
            lti.F0[fs_to_global["compressor_p_out"], 0] =
                lti.F0[fs_to_global["turbine_mdot"], 0] * p_scale_wrt_mdot_t;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot_t;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["turbine_mdot"]] =
                lti.A[fs_to_global["turbine_mdot"], fs_to_global["turbine_mdot"]] * p_scale_wrt_mdot_t;

            // Compressor mass flow rate
            Real mdot_c_scale_wrt_speed_c = (-1 / compressor.dp_out_dmdot) * compressor.dp_out_dspeed;
            Real mdot_c_scale_wrt_mdot_t = (-1 / compressor.dp_out_dmdot) * -1 * turbine.dp_in_dmdot;
            Real mdot_c_scale_wrt_p_high = (-1 / compressor.dp_out_dmdot) * -1 * turbine.dp_in_dT_in * turbine.dT_in_dp_in;
            lti.F0[fs_to_global["compressor_mdot"], 0] =
                    mdot_c_scale_wrt_speed_c * lti.F0[fs_to_global["compressor_speed"], 0]
                 +  mdot_c_scale_wrt_mdot_t  * lti.F0[fs_to_global["turbine_mdot"], 0];
                 // +  mdot_c_scale_wrt_p_high  * lti.F0[fs_to_global["compressor_p_out"], 0]; // dp_dT_t_in is small
            lti.A[fs_to_global["compressor_mdot"], 0..$] =
                    mdot_c_scale_wrt_speed_c * lti.A[fs_to_global["compressor_speed"], 0..$]
                 +  mdot_c_scale_wrt_mdot_t  * lti.A[fs_to_global["turbine_mdot"], 0..$];
                 // +  mdot_c_scale_wrt_p_high  * lti.A[fs_to_global["compressor_p_out"], 0..$]; // Ditto

            // Compressor e_out
            Real e_scale_wrt_speed = compressor.de_out_dspeed;
            Real e_scale_wrt_mdot  = compressor.de_out_dmdot;
            lti.F0[fs_to_global["compressor_e_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * e_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * e_scale_wrt_mdot;
            break;
        }

        //----------------------------------------------------------------------
        case M.FAST_OLD: // ADVANCED MODEL, FAST TIMESCALE
        {
            // Motor torque varies as first-order hold (piecewise linear)
            lti.F0[fs_to_global["compressor_T_motor"], 0] = compressor.dT_motor_dt;
            lti.B[fs_to_global["compressor_T_motor"], fs_to_global["compressor_dT_motor_dt"]] = 1.0;

            // Compute speed derivative (computed first, since required for many other eqs)
            lti.F0[fs_to_global["compressor_speed"], 0] =
                    (compressor.T_motor - compressor.T_load) / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] =
                    - compressor.dT_load_dspeed / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] =
                    - compressor.dT_load_dmdot / (compressor.i_rotor * compressor.speed_nominal);
            // Compressor T_in term in A matrix is assumed negligble here
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] =
                    1 / (compressor.i_rotor * compressor.speed_nominal);

            // Turbine mass flow rate (used in compresssor mass flow rate eq)
            // For turbine, ddelta_p_dmdot = -1 * dp_in_dmdot  [used in eqs below].
            Real sum_vol_x_drho_dp_high = 0.0;
            // Start at i=1 as cell 0 is `flow source' cell
            foreach(i; 1..hp_stream.n_cells)
                sum_vol_x_drho_dp_high += hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhodp[i];
            Real mdot_turb_scale = 1 / (sum_vol_x_drho_dp_high * -1 * turbine.dp_in_dmdot);
            // Account for changing high-side density, assuming that dedt stays
            // constant [not true but hopefully close enough].
            //   dmdot = sum Vi drho_dei dedti
            Real dmdot = 0.0;
            foreach(i; 1..hp_stream.n_cells) // dxdt is dedt (x means state vars)
                dmdot += hp_stream.geo.delta_x[i] * hp_stream.geo.flow_area[i] * hp_stream.props.drhode[i] * hp_stream.props.dxdt[i];
            lti.F0[fs_to_global["turbine_mdot"], 0] = mdot_turb_scale * (turbine.mdot - compressor.mdot + dmdot);
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_mdot"]] = -1 * mdot_turb_scale;
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["turbine_mdot"]] = mdot_turb_scale;

            // Nice eqs for the flow rates
            // dmdot_t_dt = (V_{high} * ddelta_p_t_dmdot_t * sum(drho_i_dp_high))^-1  * (mdot_t - mdot_c)
            // dmdot_c_dt = - dmdot_c_ddelta_p_c * (ddelta_p_c_dNs_c * dNs_c_dt + ddelta_p_t_dmdot_t * dmdot_t_dt)

            // Compressor mass flow rate
            Real mdot_c_scale_wrt_speed_c = (-1 / compressor.dp_out_dmdot) * compressor.dp_out_dspeed;
            Real mdot_c_scale_wrt_mdot_t = (-1 / compressor.dp_out_dmdot) * -1 * turbine.dp_in_dmdot;
            lti.F0[fs_to_global["compressor_mdot"], 0] =
                    mdot_c_scale_wrt_speed_c * lti.F0[fs_to_global["compressor_speed"], 0]
                    + mdot_c_scale_wrt_mdot_t * lti.F0[fs_to_global["turbine_mdot"], 0];
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]] =
                    mdot_c_scale_wrt_speed_c * lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]];
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] =
                    mdot_c_scale_wrt_speed_c * lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]]
                    + mdot_c_scale_wrt_mdot_t * lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_mdot"]];
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]] =
                    mdot_c_scale_wrt_speed_c * lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]];
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["turbine_mdot"]] =
                    mdot_c_scale_wrt_mdot_t * lti.A[fs_to_global["turbine_mdot"], fs_to_global["turbine_mdot"]];

            // Pressure (calculated from compressor)
            // Compressor p_out (=p_high)
            Real p_scale_wrt_speed = compressor.dp_out_dspeed;
            Real p_scale_wrt_mdot = compressor.dp_out_dmdot;
            lti.F0[fs_to_global["compressor_p_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * p_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

            // Compressor e_out
            Real e_scale_wrt_speed = compressor.de_out_dspeed;
            Real e_scale_wrt_mdot  = compressor.de_out_dmdot;
            lti.F0[fs_to_global["compressor_e_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * e_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * e_scale_wrt_mdot;
            break;
        }

        //----------------------------------------------------------------------
        case M.BASIC: // BASIC MODEL
        {
            // Motor torque varies as first-order hold (piecewise linear)
            lti.F0[fs_to_global["compressor_T_motor"], 0] = compressor.dT_motor_dt;
            lti.B[fs_to_global["compressor_T_motor"], fs_to_global["compressor_dT_motor_dt"]] = 1.0;

            // Compute speed derivative first since everything else is a multiple of it
            lti.F0[fs_to_global["compressor_speed"], 0] =
                    (compressor.T_motor - compressor.T_load) / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] =
                    - compressor.dT_load_dspeed / (compressor.i_rotor * compressor.speed_nominal);
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] =
                    - compressor.dT_load_dmdot / (compressor.i_rotor * compressor.speed_nominal);
            // Compressor T_in term in A matrix is assumed negligble here
            lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] =
                    1 / (compressor.i_rotor * compressor.speed_nominal);

            // Assuming that compressor inlet temp is constant and turbine pressure
            // ratio is not dependent on inlet temperature (is this assumption OK?),
            // the LTI models for the other state variables (mdot, p_high,
            // compressor T_out) are multiples of the compressor speed LTI model.
            // [Will need to relax this assumption for closed cycle.]
            Real mdot_scale_wrt_speed = (1 / (compressor.dp_out_dmdot - turbine.dp_in_dmdot)) * -1 * compressor.dp_out_dspeed;

            // // Scaling for pressure using turbine model, assuming that dP_in_dT_in
            // // is small for turbine (ideal gas region, should be approx true)
            // Real p_scale_wrt_mdot = turbine.dp_in_dmdot;

            // Scale for pressure using compressor model
            Real p_scale_wrt_speed = compressor.dp_out_dspeed;
            Real p_scale_wrt_mdot = compressor.dp_out_dmdot;

            // For compressor e_out, we have scaling terms for mdot and compressor speed
            Real e_scale_wrt_speed = compressor.de_out_dspeed;
            Real e_scale_wrt_mdot  = compressor.de_out_dmdot;

            // // Scalings
            // import std.stdio;
            // writeln();
            // writeln("mdot_scale_wrt_speed = ", mdot_scale_wrt_speed);
            // writeln("p_scale_wrt_mdot     = ", p_scale_wrt_mdot);
            // writeln("e_scale_wrt_speed    = ", e_scale_wrt_speed);
            // writeln("e_scale_wrt_mdot     = ", e_scale_wrt_mdot);

            // Mass flow
            lti.F0[fs_to_global["compressor_mdot"], 0] = lti.F0[fs_to_global["compressor_speed"], 0] * mdot_scale_wrt_speed;
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]] =
                    lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * mdot_scale_wrt_speed;
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] =
                    lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * mdot_scale_wrt_speed;
            lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]] =
                    lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * mdot_scale_wrt_speed;

            // For this model compressor and turbine have same mass flow update
            lti.F0[fs_to_global["turbine_mdot"], 0] = lti.F0[fs_to_global["compressor_mdot"], 0];
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_speed"]] =
                    lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]];
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_mdot"]] =
                    lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]];
            lti.A[fs_to_global["turbine_mdot"], fs_to_global["compressor_T_motor"]] =
                    lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]];

            // // Pressure (calculated from turbine)
            // lti.F0[fs_to_global["compressor_p_out"], 0] = lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;
            // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
                    // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;
            // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
                // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;
            // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
                    // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

            // Pressure (calculated from compressor)
            lti.F0[fs_to_global["compressor_p_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * p_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;

            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;

            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;

            lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

            // Compressor e_out
            lti.F0[fs_to_global["compressor_e_out"], 0] =
                lti.F0[fs_to_global["compressor_speed"], 0] * e_scale_wrt_speed
                + lti.F0[fs_to_global["compressor_mdot"], 0] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * e_scale_wrt_mdot;
            lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * e_scale_wrt_speed
                + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * e_scale_wrt_mdot;
            break;
        }

        //----------------------------------------------------------------------
        default:
        {
            throw new Exception("ERROR: Invalid FlowSource model specified.");
        }
        }

        //----------------------------------------------------------------------
        // Update the output variables; TODO: Move to function
        foreach(var; z_vars)
        {
            uint z_global_index = z_index_start + cntrl_out_map[var]; // row of C matrix
            if(var == "P_net")
            {
                // import std.stdio;
                // writeln("\n");
                // writeln("NET POWER DERIVATIVES:");
                // writeln("dWc_dmdot = ", compressor.dWdot_sh_dmdot);
                // writeln("dWc_dNc   = ", compressor.dWdot_sh_dspeed);
                // writeln("dWt_dTin  = ", turbine.dWdot_sh_dT_in);
                // writeln("dWt_dmdot = ", turbine.dWdot_sh_dmdot);
                // writeln("\n");
                // writeln("TURBINE:");
                // writeln("dp_in_dmdot = ", turbine.dp_in_dmdot);
                // writeln("dp_in_dT_in = ", turbine.dp_in_dT_in);
                // writeln("\n");

                // Implements the equation
                //   P_net_bar = dWdot_t/dT_in_t * T_in_t_bar + dWdot_t/dmdot_t * mdot_t_bar
                //               + dWdot_c/dmdot_c * mdot_c_bar + dWdot_c/dN_c * N_c_bar
                lti.Zk[z_global_index, 0] = turbine.w_sh + compressor.w_sh;
                // Turbine
                uint i_e_in = hp_stream.stream_global_index("e", hp_stream.n_cells-1);
                Real dT_in_dp_in_turb = hp_stream.props.dTdp[$-1]; // TODO: No longer using pders from turbine model.
                Real dT_in_de_in_turb = hp_stream.props.dTde[$-1];
                lti.C[z_global_index, i_e_in] = turbine.dWdot_sh_dT_in * dT_in_de_in_turb;
                // (Next line is derivative wrt p_high(:=compressor_p_out))
                lti.C[z_global_index, fs_to_global["compressor_p_out"]] = turbine.dWdot_sh_dT_in * dT_in_dp_in_turb;
                lti.C[z_global_index, fs_to_global["turbine_mdot"]]     = turbine.dWdot_sh_dmdot;
                // Compressor
                lti.C[z_global_index, fs_to_global["compressor_mdot"]]  = compressor.dWdot_sh_dmdot;
                lti.C[z_global_index, fs_to_global["compressor_speed"]] = compressor.dWdot_sh_dspeed;
            }
            else if(var == "P_comp")
            {
                lti.Zk[z_global_index, 0] = compressor.w_sh;
                lti.C[z_global_index, fs_to_global["compressor_mdot"]] = compressor.dWdot_sh_dmdot;
                lti.C[z_global_index, fs_to_global["compressor_speed"]] = compressor.dWdot_sh_dspeed;
            }
            else if(var == "P_turb")
            {
                lti.Zk[z_global_index, 0] = turbine.w_sh;
                uint i_e_in = hp_stream.stream_global_index("e", hp_stream.n_cells-1);
                Real dT_in_dp_in_turb = hp_stream.props.dTdp[$-1]; // TODO: No longer using pders from turbine model.
                Real dT_in_de_in_turb = hp_stream.props.dTde[$-1];
                lti.C[z_global_index, i_e_in] = turbine.dWdot_sh_dT_in * dT_in_de_in_turb;
                // Next line is derivative wrt p_high (=compressor_p_out)
                lti.C[z_global_index, fs_to_global["compressor_p_out"]] = turbine.dWdot_sh_dT_in * dT_in_dp_in_turb;
                lti.C[z_global_index, fs_to_global["turbine_mdot"]] = turbine.dWdot_sh_dmdot;
            }
            else if(var == "delta_mdot")
            {
                lti.Zk[z_global_index, 0] = turbine.mdot - compressor.mdot;
                lti.C[z_global_index, fs_to_global["turbine_mdot"]] = 1.0;
                lti.C[z_global_index, fs_to_global["compressor_mdot"]] = -1.0;
            }
            else
                throw new Exception("ERROR: Output variable " ~ var
                    ~ " not implemented for TurbomachineObserver.");
        }
    }

    // Hacked in, should be inside turbomachinery code.
    Real compute_w_dot_comp(Real mdot, Real N, Real p_in, Real T_in)
    {
        import q1dcfd.utils.math: square;
        Real[string] results;
        results = comp_model.calc_downstream(mdot, N, p_in, T_in);
        Real v_in = mdot / (compressor.a_in * results["rho_in"]);
        Real v_out = mdot / (compressor.a_out * results["rho_out"]);
        Real w_sh = (results["delta_h"] + 0.5*v_out.square - 0.5*v_in.square) * mdot;
        return w_sh;
    }
    // Same.
    Real compute_w_dot_turb(Real mdot, Real N, Real p_in, Real T_in)
    {
        import q1dcfd.utils.math: square;
        Real[string] results;
        results = turb_model.calc_downstream(mdot, N, p_in, T_in);
        Real v_in = mdot / (turbine.a_in * results["rho_in"]);
        Real v_out = mdot / (turbine.a_out * results["rho_out"]);
        Real w_sh = (results["delta_h"] + 0.5*v_out.square - 0.5*v_in.square) * mdot;
        return w_sh;
    }

    /*
     * Computes compressor speed constraints as a function of turbine inlet
     * temperature.
     * Assumes fixed compressor inlet conditions and fixed turbine outlet
     * pressure and speed.
     * These constraints depend on the match between the turbine and compressor
     * (not the components individually).
     *
     * Steps:
     * Make line
     *     mdot_star = fc1(N_c)  [should be able to do this as N_corr...]
     * Based on compressor size, compute line
     *     P_high_star = fc2(mdot_star) = fc2(fc1(N_c)).
     * Turbine model gives high side-pressure
     *     P_high = P_low * ft(mdot_turb, P_high, T_turb_in).
     * We assume that the current difference between the compressor and turbine
     * mass flow rates (delta_mdot) persists, so
     *     mdot_turb = mdot_star + delta_mdot.
     * The point where both the the turbine model gives P_high at P_high_star
     * and mdot_star + delta_mdot is the surge limit.
     * To find the surge point, solve
     *     0 = P_high_star - P_low_turb * ft(mdot_turb, P_high, T_turb_in)
     * i.e. solve
     *     0 = fc2(N_c) - P_low_turb * ft(fc1(N_c) + delta_mdot, fc2(N_c), T_turb_in).
     */
    void compute_compressor_constraint_lines()
    {
        // plot_compressor_operating_point();
        import std.math: isNaN;
        import std.format;

        // Check that the compressor and turbine observers know their low-side
        // conditions at this point.
        if(isNaN(compressor.p_in) || isNaN(compressor.T_in))
        {
            throw new Exception(format!("Invalid compressor inlet conditions "
                    ~ "(p_in = %.4g, T_in = %.1f), check compressor model has "
                    ~ " been initialized.")(compressor.p_in, compressor.T_in));
        }
        if(isNaN(turbine.p_in) || isNaN(turbine.T_in))
        {
            throw new Exception(format!("Invalid turbine inlet conditions "
                    ~ "(p_in = %.4g, T_in = %.1f), check turbine model has "
                    ~ " been initialized.")(turbine.p_in, turbine.T_in));
        }

        // Make mdot_star = f1(N_s) line (eventually this will find mdot_star =
        // f1(N_corr)).
        uint n_steps = 50;
        Real speed_max = comp_model.compute_max_speed(compressor.p_in,
                compressor.T_in);
        Real delta_speed = (speed_max - comp_model.I_speed_min) /
            cast(Real) (n_steps-1);
        auto speed = new Real[](n_steps);
        auto mdot_star = new Real[](n_steps);
        foreach(i; 0..n_steps)
        {
            speed[i] = comp_model.I_speed_min + i * delta_speed;
            mdot_star[i] = compute_mdot_star(comp_model, speed[i]);
        }

        // Make line P_high_star = f2(mdot_star) accounting for scaling
        auto P_high_star = new Real[](n_steps);
        foreach(i; 0..n_steps)
        {
            try
            {
                auto results = comp_model.calc_downstream(mdot_star[i],
                        speed[i], compressor.p_in, compressor.T_in);
                P_high_star[i] = results["P_out"];
            }
            catch(comp_model.SpeedOverrun) {} // Do nothing
        }

        // Make the interpolators
        auto speed_odarr = slice!Real(speed.length);
        auto mdot_star_odarr = slice!Real(mdot_star.length);
        auto P_high_star_odarr = slice!Real(P_high_star.length);
        speed_odarr[] = speed[];
        mdot_star_odarr[] = mdot_star[];
        P_high_star_odarr[] = P_high_star[];
        mdot_star_interp = linear!double(cast(iodarr) speed_odarr, mdot_star_odarr);
        P_high_star_interp = linear!double(cast(iodarr) speed_odarr, P_high_star_odarr);

        //----------------------------------------------------------------------
        // Test the root finder.
        immutable auto TEST = false;
        if(TEST)
        {
            import std.math: fabs;

            immutable Real H = 1E-8;
            immutable Real tol = 1.0E-4;
            immutable uint max_iter = 100;

            Real T_turb_in = 511.15; // K
            Real N_0 = 1.00;
            Real mdot_diff = turbine.mdot - compressor.mdot;

            // Manually find the root (approximately)
            auto error = new Real[](n_steps);
            foreach(i; 0..n_steps)
                error[i] = fabs(P_high_star[i] - turbine.p_out * turb_model.calc_pr(mdot_star[i], P_high_star[i], T_turb_in));

            import std.stdio;
            import std.algorithm.searching: minIndex;
            writeln("\n");
            writeln("speed = ", speed);
            writeln("mdot_star = ", mdot_star);
            writeln("P_high_star = ", P_high_star);
            writeln("error = ", error);
            writeln("\nN_surge (root-finder) = ", compute_N_min(T_turb_in, N_0, mdot_diff));
            writeln("N_surge (approx) = ", speed[minIndex(error)]);
            writeln("N_max = ", comp_model.compute_max_speed(compressor.p_in,
                        compressor.T_in));
            writeln("\n");

            import core.stdc.stdlib;
            exit(-1);
        }
    }

    /* Compute mdot that maximizes compressor P_out for given speed */
    Real compute_mdot_star(Compressor comp_model, Real speed)
    {
        import std.format;
        import std.algorithm.searching: maxIndex;

        Real[string] results;
        uint n_steps = 100;
        Real delta_mdot = (comp_model.I_mass_max - comp_model.I_mass_min) /
            cast(Real) (n_steps-1);
        auto mdot_arr = new Real[](n_steps);
        auto P_out_arr = new Real[](n_steps);

        foreach(i; 0..n_steps)
        {
            mdot_arr[i] = comp_model.I_mass_min + i * delta_mdot;
            try
            {
                results = comp_model.calc_downstream(mdot_arr[i],
                        speed, compressor.p_in, compressor.T_in);
                P_out_arr[i] = results["P_out"];
            }
            catch(comp_model.SurgeLineOverrun e) { P_out_arr[i] = 0.0; }
            catch(comp_model.MassflowOverrun e)  { P_out_arr[i] = 0.0; }
            catch(comp_model.SpeedOverrun e)     { P_out_arr[i] = 0.0; }
        }

        // Take the mass flow that corresponds to max P_out
        auto i_max = maxIndex(P_out_arr);
        // if(P_out_arr[i_max] == 0.0)
            // throw new Exception(format!(
                    // "Error initializing compressor constraints; no successful mass flow "
                    // ~ "rates trialled for compressor speed %.1f.")(speed));

        return mdot_arr[i_max];
    }

    /*
     * Solves N[k+1] = N[k] - f(N) / f'(N)
     * until |delta| = |N[k+1] - N[k]| < tol
     *
     * f' is evaluated numerically.
     * See 'compute_compressor_constraint_lines' for more info.
     */
    Real compute_N_min(Real T_turb_in, Real N_0, Real delta_mdot)
    {
        import std.math: fabs;

        immutable Real H = 1E-8;
        immutable Real tol = 1.0E-4;
        immutable uint max_iter = 100;

        // Real delta_mdot = turbine.mdot - compressor.mdot;

        Real N = N_0;
        Real delta, f, f_plus, f_dash;
        foreach(i; 0..max_iter)
        {
            f = P_high_star_interp(N) - turbine.p_out * turb_model.calc_pr(mdot_star_interp(N) + delta_mdot, P_high_star_interp(N), T_turb_in);
            f_plus = P_high_star_interp(N*(1+H)) - turbine.p_out * turb_model.calc_pr(mdot_star_interp(N*(1+H)) + delta_mdot, P_high_star_interp(N*(1+H)), T_turb_in);
            f_dash = (f_plus - f) / (H*N);
            delta = -f / f_dash;
            N += delta;
            if (fabs(delta) < tol) return N;
        }

        throw new Exception(
            "Root-finding problem used to calculate surge constraint did not "
            ~ "converge.");
    }

    void plot_compressor_operating_point()
    {
        // Plotting compressor operating point.
        // P_out (target) = 1.178e+07
        double Speed_0 = 0.9157;
        double T_in_0  = 320.5;
        double P_in_0  = 8.629e+06;
        uint N = 100;

        // Here we want to try a bunch of mass flow rates
        auto mdot_arr  = new double[](N);
        auto P_out_arr = new double[](N);
        auto dh_arr    = new double[](N);
        auto eta_arr   = new double[](N);
        Real delta_mdot = (comp_model.I_mass_max - comp_model.I_mass_min) /
            cast(double) (N-1);
        double[string] results;
        foreach(i; 0..N)
        {
            mdot_arr[i] = comp_model.I_mass_min + i * delta_mdot;
            try
            {
                results = comp_model.calc_downstream(mdot_arr[i],
                        Speed_0, P_in_0, T_in_0);
                P_out_arr[i] = results["P_out"];
                dh_arr[i]    = results["delta_h"];
                eta_arr[i]   = results["eta"];
            }
            catch(Exception e) { P_out_arr[i] = 0.0; }
        }

        import std.stdio;
        writeln("\n");
        writeln("N_eq      = ", results["N_eq"]);
        writeln("mdot_arr  = ", mdot_arr);
        writeln("P_out_arr = ", P_out_arr);
        writeln("dh_arr    = ", dh_arr);
        writeln("eta_arr   = ", eta_arr);
        writeln("\n");

        import std.stdio;
        writeln("speed_min = ", comp_model.I_speed_min);
        writeln("speed_max = ", comp_model.I_speed_max);

        import core.stdc.stdlib;
        exit(-1);
    }
}

// TODO: Remove all the duplicated code out of TurbomachinePair.

class TurbomachinePair : FlowSource
{
    import mir.ndslice;
    import mir.interpolate.linear;
    import lubeck;

    import q1dcfd.config: Real;
    import q1dcfd.state_space_control: CompressorObserver, TurbineObserver;
    import q1dcfd.maps: Compressor, Turbine;

    alias odarr = Slice!(Real*, 1);
    alias iodarr = Slice!(immutable(Real)*, 1);

protected:
    CompressorObserver compressor;
    TurbineObserver turbine;
    Compressor comp_model; // map model
    Turbine turb_model; // map model

    uint[string] fs_to_global; // global state indices for comp and turb
    Linear!(Real, 1, immutable(Real)*) mdot_star_interp;
    Linear!(Real, 1, immutable(Real)*) P_high_star_interp;

public:
    this(
        string name,
        IObserver compressor_observer,
        IObserver turbine_observer,
        string[] z_vars)
    {
        import std.stdio;
        writeln("ERROR: Class TurbomachinePair should currently not be used, "
                ~"though it will be back.");
        import core.stdc.stdlib;
        exit(-1);

        immutable uint n_constrs_tmp = 0;
        string[] output_array_tmp;
        super(name, output_array_tmp, z_vars, n_constrs_tmp);

        this.compressor = cast(CompressorObserver) compressor_observer;
        this.turbine    = cast(TurbineObserver) turbine_observer;

        comp_model = cast(Compressor) compressor.turbomachine_model;
        turb_model = cast(Turbine) turbine.turbomachine_model;

        fs_to_global = [
            "compressor_e_out"   : compressor_observer.global_index(0, "e_out"),
            "compressor_p_out"   : compressor_observer.global_index(0, "p_out"),
            "compressor_mdot"    : compressor_observer.global_index(0, "mdot"),
            "compressor_speed"   : compressor_observer.global_index(0, "speed"),
            "turbine_e_out"      : turbine_observer.global_index(0, "e_out"),
            "turbine_p_out"      : turbine_observer.global_index(0, "p_out"),
            "turbine_mdot"       : turbine_observer.global_index(0, "mdot"),
            "turbine_speed"      : turbine_observer.global_index(0, "speed"),
            "compressor_T_motor" : compressor_observer.global_index(0, "T_motor"),
            "compressor_dT_motor_dt" : compressor_observer.u_global_index("dT_motor_dt")
        ];
    }

    // TODO: Need to remove all this duplicate code.
    override void update_constraints(ref Mpc mpc)
    {
        import std.stdio;
        writeln("\n\nNeed to clean up this class. Quitting.\n\n");
        import core.stdc.stdlib;
        exit(-1);
    }

    override void initialise()
    {
        compute_compressor_constraint_lines();
    }

protected:
    /*
     * This function assumes synchronous turbine operation.
     *
     * This LTI model assumes that the inlet temperatures do not vary much and
     * have little effect on mdot and p_high.
     * These assumptions will need to be relaxed for closed-cycle modelling.
     *
     * This function forms LTI model only for variables of interest to open
     * cycle: mdot (should be ~= for comp and turb), compressor speed,
     * compressor p_out ( = turbine p_in), compressor T_out.
     */
    override void update_lti_model(Lti lti)
    {
        // Motor torque varies as first-order hold (piecewise linear)
        lti.F0[fs_to_global["compressor_T_motor"], 0] = compressor.dT_motor_dt;
        lti.B[fs_to_global["compressor_T_motor"], fs_to_global["compressor_dT_motor_dt"]] = 1.0;

        // Compute speed derivative first since everything else is a multiple of it
        lti.F0[fs_to_global["compressor_speed"], 0] =
                (compressor.T_motor - compressor.T_load) / (compressor.i_rotor * compressor.speed_nominal);
        lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] =
                - compressor.dT_load_dspeed / (compressor.i_rotor * compressor.speed_nominal);
        lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] =
                - compressor.dT_load_dmdot / (compressor.i_rotor * compressor.speed_nominal);
        // Compressor T_in term in A matrix is assumed negligble here
        lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] =
                1 / (compressor.i_rotor * compressor.speed_nominal);

        // Assuming that compressor inlet temp is constant and turbine pressure
        // ratio is not dependent on inlet temperature (is this assumption OK?),
        // the LTI models for the other state variables (mdot, p_high,
        // compressor T_out) are multiples of the compressor speed LTI model.
        // [Will need to relax this assumption for closed cycle.]
        Real mdot_scale_wrt_speed = (1 / (compressor.dp_out_dmdot - turbine.dp_in_dmdot)) * -1 * compressor.dp_out_dspeed;

        // // Scaling for pressure using turbine model, assuming that dP_in_dT_in
        // // is small for turbine (ideal gas region, should be approx true)
        // Real p_scale_wrt_mdot = turbine.dp_in_dmdot;

        // Scale for pressure using compressor model
        Real p_scale_wrt_speed = compressor.dp_out_dspeed;
        Real p_scale_wrt_mdot = compressor.dp_out_dmdot;

        // For compressor e_out, we have scaling terms for mdot and compressor speed
        Real e_scale_wrt_speed = compressor.de_out_dspeed;
        Real e_scale_wrt_mdot  = compressor.de_out_dmdot;

        // // Scalings
        // import std.stdio;
        // writeln();
        // writeln("mdot_scale_wrt_speed = ", mdot_scale_wrt_speed);
        // writeln("p_scale_wrt_mdot     = ", p_scale_wrt_mdot);
        // writeln("e_scale_wrt_speed    = ", e_scale_wrt_speed);
        // writeln("e_scale_wrt_mdot     = ", e_scale_wrt_mdot);

        // Mass flow
        lti.F0[fs_to_global["compressor_mdot"], 0] = lti.F0[fs_to_global["compressor_speed"], 0] * mdot_scale_wrt_speed;
        lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * mdot_scale_wrt_speed;
        lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * mdot_scale_wrt_speed;
        lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]] =
                lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * mdot_scale_wrt_speed;

        // // Pressure (calculated from turbine)
        // lti.F0[fs_to_global["compressor_p_out"], 0] = lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;
        // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
                // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;
        // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
            // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;
        // lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
                // lti.A[fs_to_global["compressor_mdot"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

        // Pressure (calculated from compressor)
        lti.F0[fs_to_global["compressor_p_out"], 0] =
              lti.F0[fs_to_global["compressor_speed"], 0] * p_scale_wrt_speed
            + lti.F0[fs_to_global["compressor_mdot"], 0] * p_scale_wrt_mdot;

        lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_speed"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * p_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * p_scale_wrt_mdot;

        lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_mdot"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * p_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * p_scale_wrt_mdot;

        lti.A[fs_to_global["compressor_p_out"], fs_to_global["compressor_T_motor"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * p_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * p_scale_wrt_mdot;

        // Compressor e_out
        lti.F0[fs_to_global["compressor_e_out"], 0] =
              lti.F0[fs_to_global["compressor_speed"], 0] * e_scale_wrt_speed
            + lti.F0[fs_to_global["compressor_mdot"], 0] * e_scale_wrt_mdot;
        lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_speed"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_speed"]] * e_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_speed"]] * e_scale_wrt_mdot;
        lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_mdot"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_mdot"]] * e_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_mdot"]] * e_scale_wrt_mdot;
        lti.A[fs_to_global["compressor_e_out"], fs_to_global["compressor_T_motor"]] =
              lti.A[fs_to_global["compressor_speed"], fs_to_global["compressor_T_motor"]] * e_scale_wrt_speed
            + lti.A[fs_to_global["compressor_mdot"],  fs_to_global["compressor_T_motor"]] * e_scale_wrt_mdot;

        // IMPORTANT:
        // For an compressor-turbine system, we don't care about the turbine
        // downstream conditions for control, so we don't model them here.
        // Turbine e_out dynamics must be added to model an open cycle and
        // low-side pressure dynamics must be added to model a closed cycle.
    }

    void compute_compressor_constraint_lines()
    {
        // plot_compressor_operating_point();
        import std.math: isNaN;
        import std.format;

        // Check that the compressor and turbine observers know their low-side
        // conditions at this point.
        if(isNaN(compressor.p_in) || isNaN(compressor.T_in))
        {
            throw new Exception(format!("Invalid compressor inlet conditions "
                    ~ "(p_in = %.4g, T_in = %.1f), check compressor model has "
                    ~ " been initialized.")(compressor.p_in, compressor.T_in));
        }
        if(isNaN(turbine.p_in) || isNaN(turbine.T_in))
        {
            throw new Exception(format!("Invalid turbine inlet conditions "
                    ~ "(p_in = %.4g, T_in = %.1f), check turbine model has "
                    ~ " been initialized.")(turbine.p_in, turbine.T_in));
        }

        // Make mdot_star = f1(N_s) line (eventually this will find mdot_star =
        // f1(N_corr)).
        uint n_steps = 50;
        Real speed_max = comp_model.compute_max_speed(compressor.p_in,
                compressor.T_in);
        Real delta_speed = (speed_max - comp_model.I_speed_min) /
            cast(Real) (n_steps-1);
        auto speed = new Real[](n_steps);
        auto mdot_star = new Real[](n_steps);
        foreach(i; 0..n_steps)
        {
            speed[i] = comp_model.I_speed_min + i * delta_speed;
            mdot_star[i] = compute_mdot_star(comp_model, speed[i]);
        }

        // Make line P_high_star = f2(mdot_star) accounting for scaling
        auto P_high_star = new Real[](n_steps);
        foreach(i; 0..n_steps)
        {
            try
            {
                auto results = comp_model.calc_downstream(mdot_star[i],
                        speed[i], compressor.p_in, compressor.T_in);
                P_high_star[i] = results["P_out"];
            }
            catch(comp_model.SpeedOverrun) {} // Do nothing
        }

        // Make the interpolators
        auto speed_odarr = slice!Real(speed.length);
        auto mdot_star_odarr = slice!Real(mdot_star.length);
        auto P_high_star_odarr = slice!Real(P_high_star.length);
        speed_odarr[] = speed[];
        mdot_star_odarr[] = mdot_star[];
        P_high_star_odarr[] = P_high_star[];
        mdot_star_interp = linear!double(cast(iodarr) speed_odarr, mdot_star_odarr);
        P_high_star_interp = linear!double(cast(iodarr) speed_odarr, P_high_star_odarr);

        //----------------------------------------------------------------------
        // Test the root finder.
        immutable auto TEST = false;
        if(TEST)
        {
            import std.math: fabs;

            immutable Real H = 1E-8;
            immutable Real tol = 1.0E-4;
            immutable uint max_iter = 100;

            Real T_turb_in = 511.15; // K
            Real N_0 = 1.00;

            // Manually find the root (approximately)
            auto error = new Real[](n_steps);
            foreach(i; 0..n_steps)
                error[i] = fabs(P_high_star[i] - turbine.p_out * turb_model.calc_pr(mdot_star[i], P_high_star[i], T_turb_in));

            import std.stdio;
            import std.algorithm.searching: minIndex;
            writeln("\n");
            writeln("speed = ", speed);
            writeln("mdot_star = ", mdot_star);
            writeln("P_high_star = ", P_high_star);
            writeln("error = ", error);
            writeln("\nN_surge (root-finder) = ", compute_N_min(T_turb_in, N_0));
            writeln("N_surge (approx) = ", speed[minIndex(error)]);
            writeln("N_max = ", comp_model.compute_max_speed(compressor.p_in,
                        compressor.T_in));
            writeln("\n");

            import core.stdc.stdlib;
            exit(-1);
        }
    }

    /* Compute mdot that maximizes compressor P_out for given speed */
    Real compute_mdot_star(Compressor comp_model, Real speed)
    {
        import std.format;
        import std.algorithm.searching: maxIndex;

        Real[string] results;
        uint n_steps = 100;
        Real delta_mdot = (comp_model.I_mass_max - comp_model.I_mass_min) /
            cast(Real) (n_steps-1);
        auto mdot_arr = new Real[](n_steps);
        auto P_out_arr = new Real[](n_steps);

        foreach(i; 0..n_steps)
        {
            mdot_arr[i] = comp_model.I_mass_min + i * delta_mdot;
            try
            {
                results = comp_model.calc_downstream(mdot_arr[i],
                        speed, compressor.p_in, compressor.T_in);
                P_out_arr[i] = results["P_out"];
            }
            catch(comp_model.SurgeLineOverrun e) { P_out_arr[i] = 0.0; }
            catch(comp_model.MassflowOverrun e)  { P_out_arr[i] = 0.0; }
            catch(comp_model.SpeedOverrun e)     { P_out_arr[i] = 0.0; }
        }

        // Take the mass flow that corresponds to max P_out
        auto i_max = maxIndex(P_out_arr);
        // if(P_out_arr[i_max] == 0.0)
            // throw new Exception(format!(
                    // "Error initializing compressor constraints; no successful mass flow "
                    // ~ "rates trialled for compressor speed %.1f.")(speed));

        return mdot_arr[i_max];
    }

    /*
     * Solves N[k+1] = N[k] - f(N) / f'(N)
     * until |delta| = |N[k+1] - N[k]| < tol
     *
     * f' is evaluated numerically.
     * See 'compute_compressor_constraint_lines' for more info.
     */
    Real compute_N_min(Real T_turb_in, Real N_0)
    {
        import std.math: fabs;

        immutable Real H = 1E-8;
        immutable Real tol = 1.0E-4;
        immutable uint max_iter = 100;

        Real delta_mdot = turbine.mdot - compressor.mdot;

        Real N = N_0;
        Real delta, f, f_plus, f_dash;
        foreach(i; 0..max_iter)
        {
            f = P_high_star_interp(N) - turbine.p_out * turb_model.calc_pr(mdot_star_interp(N) + delta_mdot, P_high_star_interp(N), T_turb_in);
            f_plus = P_high_star_interp(N*(1+H)) - turbine.p_out * turb_model.calc_pr(mdot_star_interp(N*(1+H)) + delta_mdot, P_high_star_interp(N*(1+H)), T_turb_in);
            f_dash = (f_plus - f) / (H*N);
            delta = -f / f_dash;
            N += delta;
            if (fabs(delta) < tol) return N;
        }

        throw new Exception(
            "Root-finding problem used to calculate surge constraint did not "
            ~ "converge.");
    }
}


module q1dcfd.state_space_control.observer;

import q1dcfd.config: Real, InputPort;
import q1dcfd.types.geometry: CrossSection, ChannelCrossSection, WallCrossSection;
import q1dcfd.state_space_control.observer_defs: ObsDef, ChannelObsDef, WallObsDef,
       HxObsDef, PumpObsDef, TurbineObsDef, CompressorObsDef;
import q1dcfd.state_space_control.metaprogramming:  InitPropArrays,
       InitPropValues, InitPderArrays;
import q1dcfd.state_space_control.stream: Props;
import q1dcfd.state_space_control.wall: WallProps;
import q1dcfd.state_space_control.lti: Lti;
import q1dcfd.state_space_control.mpc: Mpc;

import mir.ndslice;
import lubeck;

alias odarr = Slice!(Real*, 1);
alias tdarr = Slice!(Real*, 2);

/*
 * Wishlist:
 * - If this observer class was a q1d domain it could potentially be integrated
 *   with results saving, though this would require making each fv cell its own
 *   object.
 * - Otherwise, saving might be able to be hacked in using a decorator.
 *   This is actually a far better approach if I can get it to work.
 * - geo objects for channel and wall observers should actually be protected
 *   with public getters.
 * - Rewrite this code to use arrays of 'FlowState' objects, rather than regular
 *   property arrays. (Jamie was saying this is slow though.)
 * - The 'fluid' observers have some common functions and members that could be
 *   taken out into some abstract class.
 * - For PumpObserver, would be best to define some general pump constants
 *   struct and share this between simulation and control codes.
 * - Would be nice to add partial derivative property calculation into eos.
 * - For compound components such as heat exchangers, form state indexing
 *   parameters from sub components.
 * - Add checks for connected walls and channels having same n_cells.
 * - I can probably do away with the 'ConvectiveInterface' class
 * - Implement constaints on state vars not just outputs
 */

/*
 * Three main types of component-level observers exist:
 * 1) Thermal observers, which estimate spatial property distributions in
 *    components where thermal dynamics dominate system dynamics, such as pipes
 *    and heat exchangers.
 * 2) Flow observers, which read pressures and mass flow rates for components
 *    that control flow of compressible fluids (i.e. turbomachines).
 * 3) Other observers, which measure state of other components such as pumps and
 *    valves.
 *
 * All of these observer subtypes communicate their state estimates back to a
 * state-space controller via this interface.
 */
interface IObserver
{
public:
    @property string name();
    @property string type();
    @property uint n_cells();
    @property uint n_states();
    @property uint n_inputs();
    @property uint n_outputs();
    @property uint n_cntrl_outs();
    @property uint n_constraints();
    @property uint n_states_per_cell();
    @property uint index_start();
    @property uint index_end();
    @property uint u_index_start();
    @property uint u_index_end();
    @property uint y_index_start();
    @property uint y_index_end();
    @property uint z_index_start();
    @property uint z_index_end();
    @property uint c_index_start();
    @property uint c_index_end();

    uint global_index(uint i, string prop_key);
    uint u_global_index(string prop_key);
    uint y_global_index(string prop_key);
    uint z_global_index(string prop_key);
    void populate_observer_map(ref IObserver[string] observer_map);
    uint populate_state_indices(uint index_start);
    uint populate_input_indices(uint u_index_start);
    uint populate_output_indices(uint y_index_start);
    uint populate_cntrl_out_indices(uint z_index_start);
    uint populate_constraint_indices(uint c_index_start);

    void initialise_state_estimate(const InputPort[] input_ports);
    void initialise_u(ref Lti lti);
    void update(const Real t, const InputPort[] input_ports);
    void control_update(ref Lti lti, ref Mpc mpc);
    void update_actuators(ref Lti lti, ref Real[] _output_ports);
    void write_to_lti_model(ref Lti lti);
    void display_state_estimate();
    Real get_predicted_output(string prop);
    Real get_measured_output(string prop);

    void populate_stream_indices(string stream_name, uint index_start);
    uint stream_istart(string stream_name);
    uint stream_iend(string stream_name);
    void write_to_stream_prop_vectors(string stream_name, ref Props props);
    Real[][string] get_geo_dict();

    void populate_wall_indices(string wall_name, uint index_start);
    uint wall_istart(string wall_name);
    uint wall_iend(string wall_name);
    void write_to_wall_prop_vectors(string wall_name, ref WallProps props);

    // TODO: Should be specific to channel observer [integrate w stream]
    @property Real e_out_measured();
    @property Real dt_stable();
    void set_dt_stable(Real new_dt_stable);
    void update_e_corr(Real[] e_corr_new);

    // TODO: Should be protected (change stream observer structure to fix)
    void update_state_estimate(const Real t, const Real dt,
            const InputPort[] input_ports);

protected:
    void update_measurements(const InputPort[] input_ports);
    void compute_partial_derivatives();
    void update_constraints(ref Mpc mpc);
}

IObserver build_observer(ObsDef def, CrossSection[string] geometries)
{
    import std.conv: to;

    switch(def.type)
    {
        case "PumpObsDef":
            CrossSection[string] geo_dict;
            auto observer = new CflUpdate!PumpObserver(def.to!PumpObsDef, geo_dict);
            return cast(IObserver) observer;

        case "ChannelObsDef":
            CrossSection[string] geo_dict = [
                "channel" : geometries[def.to!ChannelObsDef.geo_name]];
            auto observer = new CflUpdate!ChannelObserver(def.to!ChannelObsDef, geo_dict);
            return cast(IObserver) observer;

        // WallObserver objects are always built as part of a HX, so not
        // included in this factory

        case "HxObsDef":
            CrossSection[string] geo_dict = [
                "hot_channel"  : geometries[def.to!HxObsDef.hot_geo_name],
                "cold_channel" : geometries[def.to!HxObsDef.cold_geo_name],
                "wall"         : geometries[def.to!HxObsDef.wall_geo_name]];
            auto observer = new SimpleUpdate!HxObserver(def.to!HxObsDef, geo_dict);
            return cast(IObserver) observer;

        case "TurbineObsDef":
            CrossSection[string] geo_dict; // No geo params reqd for turbomachines
            auto observer = new CflUpdate!TurbineObserver(def.to!TurbineObsDef, geo_dict);
            return cast(IObserver) observer;

        case "CompressorObsDef":
            CrossSection[string] geo_dict; // No geo params reqd for turbomachines
            auto observer = new CflUpdate!CompressorObserver(def.to!CompressorObsDef, geo_dict);
            return cast(IObserver) observer;

        default:
            import q1dcfd.utils.errors: InvalidDefinition;
            import std.format;
            throw new InvalidDefinition(
                "Unrecognized observer definition type %s in state-space controller."
                .format(def.type));
    }
}

abstract class AObserver : IObserver
{
protected:
    immutable uint[string] _state_map, _input_map, _output_map;
    immutable string _name, _type;
    immutable uint _n_cells, _n_states_per_cell, _n_states, _n_inputs,
              _n_outputs, _n_cntrl_outs, _n_constraints;
    uint _index_start, _index_end, _u_index_start, _u_index_end, _y_index_start,
            _y_index_end, _z_index_start, _z_index_end, _c_index_start, _c_index_end;
    uint[2][string] _stream_indices;
    uint[2][string] _wall_indices;
    string[] z_vars;
    uint[string] _cntrl_out_map;

    // TODO: all the indexing variables above should actually be ints not uints
    // so that using index -1 throws error (rather than mapping to a very large
    // positive int).

public:
    this(
        ObsDef def,
        immutable string type,
        immutable uint[string] _state_map,
        immutable uint[string] _input_map,
        immutable uint[string] _output_map,
        immutable uint _n_states_per_cell,
        immutable uint _n_constraints,
        string[] z_vars=[]
        )
    {
        _name = def.name;
        _type = type;
        this._state_map  = _state_map;
        this._input_map  = _input_map;
        this._output_map = _output_map;
        _n_cells = def.n_cells;
        this._n_states_per_cell = _n_states_per_cell;
        this._n_states      = cast(uint) (_state_map.length * _n_states_per_cell);
        this._n_inputs      = cast(uint) _input_map.length;
        this._n_outputs     = cast(uint) _output_map.length;
        this._n_constraints = _n_constraints;

        // Check controlled outputs and form mapping
        foreach(i, var; z_vars)
        {
            if(!(var in _output_map))
            {
                import std.format;
                throw new Exception(format!(
                    "ERROR: Invalid controlled output %s for observer %s.")
                    (var, name));
            }
            _cntrl_out_map[var] = cast(uint) i;
        }

        import std.conv: to;
        this.z_vars = z_vars;
        this._n_cntrl_outs = z_vars.length.to!uint;
    }

    @property string name() { return _name; }
    @property string type() { return _type; }
    @property uint n_cells() { return _n_cells; }
    @property uint n_states() { return _n_states; }
    @property uint n_inputs() { return _n_inputs; }
    @property uint n_outputs() { return _n_outputs; }
    @property uint n_cntrl_outs() { return _n_cntrl_outs; }
    @property uint n_constraints() { return _n_constraints; }
    @property uint n_states_per_cell() { return _n_states_per_cell; }
    @property uint index_start() { return _index_start; }
    @property uint index_end() { return _index_end; }
    @property uint u_index_start() { return _u_index_start; }
    @property uint u_index_end() { return _u_index_end; }
    @property uint y_index_start() { return _y_index_start; }
    @property uint y_index_end() { return _y_index_end; }
    @property uint z_index_start() { return _z_index_start; }
    @property uint z_index_end() { return _z_index_end; }
    @property uint c_index_start() { return _c_index_start; }
    @property uint c_index_end() { return _c_index_end; }

    /* Gets global state index of state var corresponding to prop_key at cell i */
    uint global_index(uint i, string prop_key)
    {
        import std.format;
        assert(i < n_cells, format!(
                    "ERROR: Trying to get index for cell %d for component %s,"
                    ~ " but this component only has %d cells.")(i, name, n_cells));
        return index_start + (i * n_states_per_cell) + _state_map[prop_key];
    }
    /* Gets global control index of control var corresponding to key */
    uint u_global_index(string prop_key)
    {
        return u_index_start + _input_map[prop_key];
    }
    uint y_global_index(string prop_key)
    {
        return y_index_start + _output_map[prop_key];
    }
    uint z_global_index(string prop_key)
    {
        return z_index_start + _cntrl_out_map[prop_key];
    }
    void populate_observer_map(ref IObserver[string] observer_map)
    {
        observer_map[name] = this;
    }
    // uint populate_state_indices(uint index_start) {};
    uint populate_input_indices(uint u_index_start)
    {
        if(n_inputs > 0)
        {
            _u_index_start = u_index_start;
            _u_index_end = u_index_start + n_inputs - 1;
            return _u_index_end + 1;
        }
        else
        {
            _u_index_start = -1;
            _u_index_end = -1;
            return u_index_start;
        }
    }
    uint populate_output_indices(uint y_index_start)
    {
        if(n_outputs > 0)
        {
            _y_index_start = y_index_start;
            _y_index_end = y_index_start + n_outputs - 1;
            return _y_index_end + 1;
        }
        else
        {
            _y_index_start = -1;
            _y_index_end = -1;
            return y_index_start;
        }
    }
    uint populate_cntrl_out_indices(uint z_index_start)
    {
        if(n_cntrl_outs > 0)
        {
            _z_index_start = z_index_start;
            _z_index_end = z_index_start + n_cntrl_outs - 1;
            return _z_index_end + 1;
        }
        else
        {
            _z_index_start = -1;
            _z_index_end = -1;
            return z_index_start;
        }
    }
    uint populate_constraint_indices(uint c_index_start)
    {
        if(n_constraints > 0)
        {
            _c_index_start = c_index_start;
            _c_index_end = c_index_start + n_constraints - 1;
            return _c_index_end + 1;
        }
        else
        {
            _c_index_start = -1;
            _c_index_end = -1;
            return c_index_start;
        }
    }
    // void write_to_lti_model(Lti lti) {}

    // void initialise_state_estimate(const InputPort[] input_ports) {}
    void update(const Real t, const InputPort[] input_ports) {}
    void initialise_u(ref Lti lti)
    {
        assert(n_inputs == 0, "ERROR: Observer type " ~ type ~ " has control "
                ~ "inputs, but 'initialise_u' is not implemented.");
    }
    void control_update(ref Lti lti, ref Mpc mpc)
    {
        compute_partial_derivatives();
        write_to_lti_model(lti);
        update_constraints(mpc);
    }
    void update_actuators(ref Lti lti, ref Real[] _output_ports)
    {
        assert(n_inputs == 0, "ERROR: Observer type " ~ type ~ " has input "
                ~ "variables but 'update_actuators' is not implemented.");
    }
    // void display_state_estimate() {}
    Real get_predicted_output(string prop)
    {
        throw new Exception(
            "ERROR: 'get_predicted_output' not implemented for observer type "
            ~ type ~ ".");
    }
    Real get_measured_output(string prop)
    {
        throw new Exception(
            "ERROR: 'get_measured_output' not implemented for observer type "
            ~ type ~ ".");
    }

    @property Real e_out_measured()
    {
        throw new Exception(
            "ERROR: 'e_out_measured' not implemented for observer type "
            ~ type ~ ".");
    }
    @property Real dt_stable()
    {
        throw new Exception(
            "ERROR: 'dt_stable' not implemented for observer type "
            ~ type ~ ".");
    }
    void set_dt_stable(Real new_dt_stable)
    {
        throw new Exception(
            "ERROR: 'set_dt_stable' not implemented for observer type "
            ~ type ~ ".");
    }
    void update_e_corr(Real[] e_corr_new)
    {
        throw new Exception(
            "ERROR: 'update_e_corr' not implemented for observer type "
            ~ type ~ ".");
    }

    // void populate_stream_indices(string stream_name, uint index_start);
    override uint stream_istart(string stream_name)
        { return _stream_indices[stream_name][0]; }
    override uint stream_iend(string stream_name)
        { return _stream_indices[stream_name][1]; }
    // Overriden if observer type has a StreamGeo object
    Real[][string] get_geo_dict()
    {
        import std.format;
        throw new Exception(format!(
            "Invalid call to 'get_geo_dict', stream geometry is not "
            ~ "defined for observer type %s")(type));
    }
    void write_to_stream_prop_vectors(string stream_name, ref Props props) {}

    void populate_wall_indices(string wall_name, uint index_start)
    {
        throw new Exception(
            "ERROR: Only 'WallObserver' objects can be connected to walls.");
    }
    override uint wall_istart(string wall_name)
        { return _wall_indices[wall_name][0]; }
    override uint wall_iend(string wall_name)
        { return _wall_indices[wall_name][1]; }
    void write_to_wall_prop_vectors(string wall_name, ref WallProps props) {}

    void update_state_estimate(const Real t, const Real dt,
            const InputPort[] input_ports) {}

protected:
    void update_measurements(const InputPort[] input_ports) {}
    void compute_partial_derivatives() {}
    void update_constraints(ref Mpc mpc) {}
}

class ChannelObserver : AObserver
{
    import q1dcfd.state_space_control: ControllerEos, InputPairs, FluidState,
            generate_controller_eos, ChannelObsDef;

    enum props =
            ["v", "e", "k", "h", "rho", "T", "mu", "cp", "Pr", "Re", "Nu"];
    mixin(InitPropArrays!(props));

    // TODO: Make private with public getters
    enum pder_props   = ["k", "h", "rho", "T", "Nu"];
    enum pder_vars    = ["e", "p", "mdot"];
    mixin(InitPderArrays!(pder_props, pder_vars));

// private:

public:
    immutable Real length;
    immutable uint n_channels;
    // flow_area is total (over all channels)
    struct Channelgeo { Real[] delta_x, flow_area, channel_perimeter, l_c, q_area; }
    Channelgeo geo;
    WallObserver wall; // wall connected to this channel, may be null
    ConvectiveInterface conv_interface; // interface corresponding to wall
    @property bool q_connected() { return _q_connected; }
    @property bool wall_connected() { return _wall_connected; }
    @property bool wall_reversed()  { return _wall_reversed; }

    override @property Real e_out_measured() { return _e_out_measured; }
    override @property Real dt_stable() { return _dt_stable; }
    override void set_dt_stable(Real new_dt_stable) { _dt_stable = new_dt_stable; }

    SensorsIndex sensors; // TODO: Should be protected

protected:
    immutable Real max_cfl = 0.4;
    struct SensorsIndex { uint p_in, mdot_in, h_in, T_in, e_out, T_out; }
    ControllerEos eos;
    Real p, mdot, h_in, _e_out_measured, _T_out_measured; // boundaries
    Real _dt_stable;
    Real[] qdot; // heat transfer
    Real[] dxdt;
    Real[] e_corr; // correction computed by connected stream
    immutable Real k_L, k_d; // observer and integrating disturbance gains
    bool _q_connected    = false; // heat transfer flag
    bool _wall_connected = false;
    bool _wall_reversed  = false;

    // Optional constraints
    bool e_min_constraint = false;
    bool e_max_constraint = false;
    Real e_out_min = 0.0;
    Real e_out_max = 0.0;

public:
    this(
        ObsDef a_def,
        CrossSection[string] geo_dict
        )
    {
        import std.conv: to;
        auto def = a_def.to!ChannelObsDef;

        immutable uint[string] state_map_tmp = ["e" : 0];
        immutable uint n_states_per_cell = 1;
        immutable uint[string] input_map_tmp;
        immutable uint[string] output_map_tmp = ["e_out" : 0];
        uint n_constraints_tmp = 0;
        // Optional constraints
        if(def.e_min_constraint)
        {
            n_constraints_tmp += 1;
            e_min_constraint = true;
            e_out_min = def.e_out_min;
        }
        if(def.e_max_constraint)
        {
            e_max_constraint = true;
            n_constraints_tmp += 1;
            e_out_max = def.e_out_max;
        }
        immutable uint n_constraints_imm = n_constraints_tmp;

        super(a_def, "ChannelObserver", state_map_tmp, input_map_tmp,
                output_map_tmp, n_states_per_cell, n_constraints_imm,
                def.outputs);

        length     = def.length;
        n_channels = def.n_channels;
        k_L        = def.k_L;
        k_d        = def.k_d;

        dxdt.length   = n_cells;
        qdot.length   = n_cells;
        e_corr.length = n_cells;
        e_corr[] = 0.0;

        mixin(InitPropArrays!(props, "n_cells"));
        mixin(InitPderArrays!(pder_props, pder_vars, "n_cells"));
        initialise_geometry(geo_dict["channel"].to!ChannelCrossSection);
        eos = generate_controller_eos(def.eos_backend, def.fluid, def.stream_name);

        sensors.p_in    = def.sensor_map["p_in"];
        sensors.h_in    = def.sensor_map["h_in"];
        sensors.T_in    = def.sensor_map["T_in"];
        sensors.mdot_in = def.sensor_map["mdot_in"];
        sensors.e_out   = def.sensor_map["e_out"];
        sensors.T_out   = def.sensor_map["T_out"];

        // Set heat transfer terms to zero, if connected to a convective
        // interface, this vector is overriden each timestep
        qdot[] = 0.0;
    }

    void display_state_estimate()
    {
        // We just want to print the temp and pressure profiles from inlet to
        // outlet; also print e since this is used for EOS update.
        import std.stdio;
        writeln("\n\n", name~":");
        writeln("mdot = ", mdot);
        writeln("p    = ", p);
        writeln("T    = ", T);
        writeln("e    = ", e);
    }

    /*
     * Infer initial state estimate from boundary conditions at first timestep.
     * p and mdot are assumed constant and T is assumed to vary linearly from
     * inlet to outlet; other properties calculated from (p, T).
     */
    void initialise_state_estimate(const InputPort[] input_ports)
    {
        // TODO: We are now assuming fixed pressures (set by inlet component) in
        // streams; this approach should be reflected in the state estimate
        // initialization procedure.

        import std.conv: to;
        Real T_in  = input_ports[sensors.T_in]();
        Real T_out = input_ports[sensors.T_out]();
        update_measurements(input_ports);

        Real delta_T = (T_out - T_in) / (n_cells + 1);
        foreach(i; 0..n_cells)
        {
            if(i == 0) _T[i] = T_in + delta_T;
            else       _T[i] = _T[i-1] + delta_T;

            eos.update_eos(InputPairs.p_T, p, T[i]);
            _e[i] = eos.e;
            _rho[i] = eos.rho; // req'd for initial guesses
        }

        update_derived_props();
        update_stable_timestep();
    }

    void write_to_lti_model(ref Lti lti)
    {
        // TODO: Have shifted outputs from component observers to streams,
        // should delete here.

        foreach(i; 0..n_cells) lti.Xk[global_index(i, "e"), 0] = e[i];
        // no control inputs
        lti.F0[index_start..index_end+1, 0] = dxdt[];

        // Outputs (first-order extrapolation)
        // lti.Yk = ...
        lti.Cy[y_global_index("e_out"), global_index(n_cells - 1, "e")] = 1.5;
        lti.Cy[y_global_index("e_out"), global_index(n_cells - 2, "e")] = -0.5;
        // Controlled outputs
        foreach(var; z_vars)
        {
            if(var == "e_out")
            {
                lti.Zk[z_global_index("e_out"), 0] = 1.5 * e[$-1] - 0.5 * e[$-2];
                lti.C[z_global_index("e_out"), global_index(n_cells - 1, "e")] = 1.5;
                lti.C[z_global_index("e_out"), global_index(n_cells - 2, "e")] = -0.5;
            }
            else
                throw new Exception("ERROR: Output variable " ~ var
                    ~ " not implemented for PumpObserver.");
        }
    }

    override void compute_partial_derivatives()
    {
        foreach(i; 0..n_cells)
        {
            auto pders_e = compute_stream_property_partials(
                    e[i], p, get_fluid_state(i), "e");
            dhde[i]   = pders_e["dhde"];
            drhode[i] = pders_e["drhode"];
            dTde[i]   = pders_e["dTde"];
            dkde[i]   = pders_e["dkde"];
            auto pders_p = compute_stream_property_partials(
                    e[i], p, get_fluid_state(i), "p");
            dhdp[i]   = pders_p["dhdp"];
            drhodp[i] = pders_p["drhodp"];
            dTdp[i]   = pders_p["dTdp"];
            dkdp[i]   = pders_p["dkdp"];
        }

        // Also need the Nu partials in this case
        if(wall_connected)
        {
            foreach(i; 0..n_cells)
            {
                auto nu_pders = calculate_nu_partials(e[i], p, get_fluid_state(i),
                        mdot, geo.flow_area[i], geo.l_c[i]);
                dNude[i]    = nu_pders["dNude"];
                dNudp[i]    = nu_pders["dNudp"];
                dNudmdot[i] = nu_pders["dNudmdot"];
            }
        }
    }

    override void write_to_stream_prop_vectors(string stream_name, ref Props props)
    {
        static foreach(var; ["v", "e", "k", "h", "rho", "T", "dhde", "drhode",
                "dTde", "dkde", "dhdp", "drhodp", "dTdp", "dkdp", "dxdt"])
        {
            mixin("props."~var
                ~"[stream_istart(stream_name)..stream_iend(stream_name)+1]="~var~"[];");
        }

        // If wall is connected, export the Nusselt number array, wall temps, Nu
        // partial derivatives
        // j indexes channel observer, i indexes stream
        if(wall_connected)
        {
            props.Nu[stream_istart(stream_name)..stream_iend(stream_name)+1] =
                conv_interface.Nu[];
            static foreach(var; ["dNude", "dNudp", "dNudmdot"])
            {
                mixin("props."~var
                    ~"[stream_istart(stream_name)..stream_iend(stream_name)+1]="~var~"[];");
            }

            uint j = 0;
            foreach(i; stream_istart(stream_name)..stream_iend(stream_name) + 1)
            {
                if(wall_reversed) props.T_wall[i] = wall.T[$-1-j];
                else              props.T_wall[i] = wall.T[j];
                j++;
            }
        }
    }

    /* Fill in parameters for indexing within global state-space model. */
    uint populate_state_indices(uint index_start)
    {
        _index_start = index_start;
        _index_end = index_start + (n_cells * n_states_per_cell) - 1;
        return _index_end + 1;
    }

    void populate_stream_indices(string stream_name, uint index_start)
    {
        // Channels should only be part of one stream
        if(_stream_indices.length > 0)
        {
            import std.format;
            throw new Exception(format!(
                "Channel observer %s has been connected to multiple streams.")
                (name));
        }
        _stream_indices[stream_name] = [index_start, index_start + n_cells - 1];
    }

    /* Positive qdot means heat transfer to wall, so make negative here */
    void set_qdot(Real[] qdot_interface) { qdot[] = -qdot_interface[]; }

    void set_nu(Real[] Nu_interface) { Nu[] = Nu_interface[]; }

    /* Reqd for integration with a Stream object */
    override Real[][string] get_geo_dict()
    {
        import std.conv: to;
        Real[][string] geo_dict = [
            "flow_area"  : geo.flow_area,
            "delta_x"    : geo.delta_x,
            "n_channels" : [n_channels.to!Real]
        ];
        return geo_dict;
    }

    void connect_wall(
            WallObserver wall, ConvectiveInterface conv_interface, bool reversed)
    {
        if(_wall_connected)
        {
            import std.format;
            throw new Exception(format!(
                "ERROR: Channel %s is connected to more than one wall.")(name));
        }
        this.wall = wall;
        this.conv_interface = conv_interface;
        _wall_connected = true;
        _q_connected = true;
        _wall_reversed = reversed;
    }

    /* Calculates observer outputs for state correction using nth-order recon */
    Real calculate_outputs()
    {
        return (1.5 * e[$-1] - 0.5 * e[$-2]);
        // Second order
        // return 1.75 * e[$-1] - 1.0 * e[$-2] + 0.25 * e[$-3];
    }

    override Real get_predicted_output(string prop)
    {
        if     (prop == "e") return (1.5 * e[$-1] - 0.5 * e[$-2]);
        else if(prop == "h") return (1.5 * h[$-1] - 0.5 * h[$-2]);
        else
            throw new Exception(
                "ERROR: Property " ~ prop ~ " is not an output of observer type "
                ~ type ~ ".");
    }

    // TODO: Delete
    override Real get_measured_output(string prop)
    {
        if(prop == "T")
        {
            import std.stdio;
            writeln("Deprecated.");
            import core.stdc.stdlib;
            exit(-1);
            return _T_out_measured;
        }
        else
            throw new Exception(
                "ERROR: Property " ~ prop ~ " is not an output measurement for"
                ~ " observer type " ~ type ~ ".");
    }

    override void update_e_corr(Real[] e_corr_new)
    {
        assert(e_corr_new.length == e_corr.length);
        e_corr[] = e_corr_new[];
    }

    // ChannelObserver update algorithm:
    // 0. At t=0, read initial temperature profiles and inlet pressures for each
    // component in a given stream.
    //
    // Stream update: 
    // 1. Measure inlet conditions from first component and outlet conditions
    //    from last component.
    // 2. Loop through components and compute their outlet conditions (e).
    // 3. For each component, set inlet conditions as stream pressure and outlet
    //    e of previous component.
    // 4. Compute observer gains using final outlet conditions.
    //
    // Component update: 
    // 5. Using new inlet conditions, evolve state estimate.
    // 6. Correct using observer correction factor.

    override void update_state_estimate(
            const Real t,
            const Real dt,
            const InputPort[] input_ports)
    {
        // update_measurements(input_ports); NOW DONE BY STREAM!
        dxdt = calculate_state_derivative();
        _e[] += (dxdt[] * dt) + e_corr[];
        update_derived_props();
        update_stable_timestep();
    }

    void set_inlet_conditions(Real mdot, Real p, Real h)
    {
        this.mdot = mdot;
        this.p    = p;
        this.h_in = h;
    }

protected:
    override void update_measurements(const InputPort[] input_ports)
    {
        // Don't update T measurements since these are only req'd during
        // initialization
        p    = input_ports[sensors.p_in]();
        mdot = input_ports[sensors.mdot_in]();
        h_in = input_ports[sensors.h_in]();
        _e_out_measured = input_ports[sensors.e_out]();
        _T_out_measured = input_ports[sensors.T_out]();
    }

    void update_derived_props()
    {
        foreach(i; 0..n_cells)
        {
            eos.update_eos(InputPairs.p_e, p, e[i], get_fluid_state(i));
            _k[i]   = eos.k;
            _h[i]   = eos.h;
            _rho[i] = eos.rho;
            _T[i]   = eos.T;
            _mu[i]  = eos.mu;
            _cp[i]  = eos.cp;
            _v[i]   = mdot / (rho[i] * geo.flow_area[i]);
            _Pr[i]  = mu[i] * cp[i] / k[i];
            _Re[i]  = rho[i] * v[i] * geo.l_c[i] / mu[i];
        }
    }

    Real[] calculate_state_derivative()
    {
        auto delta_h = new Real[](n_cells);
        auto dedt = new Real[](n_cells);
        auto mdot_dh = new Real[](n_cells);
        delta_h[] = (h_in ~ h)[0..$-1] - h[]; // cat w/ inflow BC to find delta_h
        mdot_dh[] = mdot * delta_h[];
        dedt[] = ((mdot * delta_h[]) + qdot[]) / (geo.flow_area[] * geo.delta_x[] * rho[]);
        return dedt;
    }

    /* Evaluates CFL condition for each cell then selects the required timestep. */
    void update_stable_timestep()
    {
        import std.algorithm.searching: minElement;
        auto dt_array = new double[](n_cells);
        dt_array[] = max_cfl * geo.delta_x[] / v[];
        dt_array[] = max_cfl * geo.delta_x[] / v[];
        _dt_stable = minElement(dt_array);
    }

    /* Stores fluid props at cell i in a FluidState object */
    FluidState get_fluid_state(ulong i)
    {
        FluidState fluid_state;
        fluid_state.p    = p; // p constant over stream
        fluid_state.e    = e[i];
        fluid_state.k    = k[i];
        fluid_state.h    = h[i];
        fluid_state.rho  = rho[i];
        fluid_state.T    = T[i];
        fluid_state.mu   = mu[i];
        return fluid_state;
    }

    override void update_constraints(ref Mpc mpc)
    {
        if(e_min_constraint)
        {
            mpc.ZVars[2*z_global_index("e_out")] = 1;
            mpc.ZLow[z_global_index("e_out"), 0] = e_out_min;
            import std.stdio;
            writeln("Shouldn't be here. Not sure this is working.");
            import core.stdc.stdlib;
            exit(-1);
        }
        if(e_max_constraint)
        {
            mpc.ZVars[2*z_global_index("e_out")+1] = 1;
            mpc.ZHigh[z_global_index("e_out"), 0] = e_out_max;
            import std.stdio;
            writeln("Shouldn't be here. Not sure this is working.");
            import core.stdc.stdlib;
            exit(-1);
        }
    }

    /*
     * This controller code uses a different geometry model to the main q1dcfd
     * geometry model.
     * In future, it would be good to rewrite the codes to use the same geometry
     * model.
     * Currently, as a temporary measure, we just perform some checks to see if
     * the input q1dcfd geometry can be converted to a controller geometry model.
     * See comments below for details of these checks.
     */
    void initialise_geometry(ChannelCrossSection geometry)
    {
        import std.conv: to;
        import std.math: approxEqual, PI, pow;

        geo.delta_x.length           = n_cells;
        geo.flow_area.length         = n_cells;
        geo.channel_perimeter.length = n_cells;
        geo.l_c.length               = n_cells;
        geo.q_area.length            = n_cells;

        Real dx = length / to!Real(n_cells);
        foreach(i; 0..n_cells)
        {
            Real x = (to!Real(i) + 0.5) * dx;
            geo.delta_x[i]           = dx;
            geo.flow_area[i]         = geometry.cross_area(x) * to!Real(n_channels);
            geo.channel_perimeter[i] = geometry.heat_circumference(x);
            geo.l_c[i]               = geometry.hydraulic_diameter(x);
            geo.q_area[i]            = geo.channel_perimeter[i] * geo.delta_x[i]
                    * to!Real(n_channels);

            // If the input q1dcfd channel geometry can be converted to a
            // Channelgeo object, then the following conditions hold:
            //   flow_area = 1/4 * (heat_circumference / pi)**2
            //   hydraulic_diameter = heat_circumference / pi
            // These conditions imply that the hydraulic diameter is the real
            // diameter and the heat circumference is the real circumference.
            if(    !approxEqual(geometry.cross_area(x),
                       PI * (geometry.heat_circumference(x) / (2 * PI)).pow(2))
                || !approxEqual(geometry.hydraulic_diameter(x),
                       geometry.heat_circumference(x) / PI))
            {
                import std.stdio;
                writefln("\ncross area (input) = %.3f", geometry.cross_area(x));
                writefln("cross_area (from channel perimeter) = %.3f",
                        0.25 * (geometry.heat_circumference(x) / PI).pow(2));
                writefln("\nhydraulic diameter (input) = %.3f",
                        geometry.hydraulic_diameter(x));
                writefln("hydraulic diameter (from channel perimeter) = %.3f",
                        geometry.heat_circumference(x) / PI);

                throw new Exception(
                        "geometry model not compatible with control system code."
                        ~ "\nEnsure that\n"
                        ~ "flow_area = 1/4 * (heat_circumference / pi)**2  and "
                        ~ "hydraulic_diameter = heat_circumference / pi,\n"
                        ~ "i.e. that the hydraulic diameter is the real channel "
                        ~ "diameter and that heat circumference is the real "
                        ~ "channel circumference."
                        );
            }
        }
    }

    /*
     * Calculates partial derivatives of v, e, k, h, rho, T wrt e or p.
     * Used to form stream LTI model.
     */
    double[string] compute_stream_property_partials(
            double ei, double pi, FluidState fs, string wrt)
    {
        auto H = 1e-8;
        auto pders_out = new double[](4);
        auto props_plus = new double[](4);
        auto props_minus = new double[](4);

        switch(wrt)
        {
            case "e":
                eos.update_eos(InputPairs.p_e, pi, (1+H)*ei, fs);
                props_plus = [eos.h, eos.rho, eos.T, eos.k];
                eos.update_eos(InputPairs.p_e, pi, (1-H)*ei, fs);
                props_minus = [eos.h, eos.rho, eos.T, eos.k];
                pders_out[] = (props_plus[] - props_minus[]) / (2 * H * ei);
                double[string] outs = [
                    "dhde"   : pders_out[0],
                    "drhode" : pders_out[1],
                    "dTde"   : pders_out[2],
                    "dkde"   : pders_out[3]
                ];
                return outs;

            case "p":
                eos.update_eos(InputPairs.p_e, (1+H)*pi, ei, fs);
                props_plus = [eos.h, eos.rho, eos.T, eos.k];
                eos.update_eos(InputPairs.p_e, (1-H)*pi, ei, fs);
                props_minus = [eos.h, eos.rho, eos.T, eos.k];
                pders_out[] = (props_plus[] - props_minus[]) / (2 * H * pi);
                double[string] outs = [
                    "dhdp"   : pders_out[0],
                    "drhodp" : pders_out[1],
                    "dTdp"   : pders_out[2],
                    "dkdp"   : pders_out[3]
                ];
                return outs;

            default:
                throw new Exception("Incorrect property specified. Must be 'e' or 'p'.");
        }
    }

    /* Calculates partial derivatives of Nu wrt controller state vars */
    double[string] calculate_nu_partials(Real ei, Real pi, FluidState fs, Real mdoti,
            Real flow_area, Real l_c)
    {
        Real calc_nu_state_vars(Real ei, Real pi, FluidState fs, Real mdoti,
                Real flow_area, Real l_c)
        {
            eos.update_eos(InputPairs.p_e, pi, ei, fs);
            auto mui = eos.mu;
            auto Pri = eos.cp * eos.mu / eos.k;
            auto Rei = mdoti * l_c / (mui * flow_area);
            return conv_interface.nu_corr.calculate_nu(Rei, Pri);
        }

        auto H = 1e-8;

        // wrt e
        auto nu_plus  = calc_nu_state_vars((1+H)*ei, pi, fs, mdoti, flow_area, l_c);
        auto nu_minus = calc_nu_state_vars((1-H)*ei, pi, fs, mdoti, flow_area, l_c);
        auto dNude = (nu_plus - nu_minus) / (2 * H * ei);

        // wrt p
        nu_plus  = calc_nu_state_vars(ei, (1+H)*pi, fs, mdoti, flow_area, l_c);
        nu_minus = calc_nu_state_vars(ei, (1-H)*pi, fs, mdoti, flow_area, l_c);
        auto dNudp = (nu_plus - nu_minus) / (2 * H * pi);

        // wrt mdot
        nu_plus  = calc_nu_state_vars(ei, pi, fs, (1+H)*mdoti, flow_area, l_c);
        nu_minus = calc_nu_state_vars(ei, pi, fs, (1-H)*mdoti, flow_area, l_c);
        auto dNudmdot = (nu_plus - nu_minus) / (2 * H * mdoti);

        double[string] outs = [
            "dNude"    : dNude,
            "dNudp"    : dNudp,
            "dNudmdot" : dNudmdot];
        return outs;
    }
}

class WallObserver : AObserver
{
public:
    immutable Real length;
    immutable uint n_channels;
    // wall_area is total (over all channels)
    struct Wallgeo { Real[] delta_x, cp, rho, k, a_wall; }
    Wallgeo geo;
    ChannelObserver hot_channel; // channels connected to wall, may be null
    ChannelObserver cold_channel;
    ConvectiveInterface hot_interface; // interface corresponding to channel
    ConvectiveInterface cold_interface;
    @property bool hot_channel_connected()  { return _hot_channel_connected; }
    @property bool hot_channel_reversed()   { return _hot_channel_reversed; }
    @property bool cold_channel_connected() { return _cold_channel_connected; }
    @property bool cold_channel_reversed()  { return _cold_channel_reversed; }
    override @property Real dt_stable() { return _dt_stable; }

protected:
    enum props = ["T"];
    mixin(InitPropArrays!(props));
    immutable Real max_cfl = 0.5;
    Real[] qdot; // Net heat transfer to wall
    Real[] T_hot_0, T_cold_0; // Initial stream temps
    Real[] dxdt;
    bool _hot_channel_connected  = false;
    bool _hot_channel_reversed   = false;
    bool _cold_channel_connected = false;
    bool _cold_channel_reversed  = false;
    Real _dt_stable = 0.001;

public:
    this(
        ObsDef a_def,
        CrossSection[string] geo_dict,
        )
    {
        immutable uint[string] state_map_tmp = ["T" : 0];
        immutable uint n_states_per_cell = 1;
        immutable uint[string] input_map_tmp;
        immutable uint[string] output_map_tmp;
        immutable uint n_constraints_tmp = 0;
        super(a_def, "WallObserver", state_map_tmp, input_map_tmp,
                output_map_tmp, n_states_per_cell, n_constraints_tmp);

        import std.conv: to;
        auto def = a_def.to!WallObsDef;

        length     = def.length;
        n_channels = def.n_channels;

        mixin(InitPropArrays!(props, "n_cells"));
        initialise_geometry(geo_dict["wall"].to!WallCrossSection);
        dxdt.length     = n_cells;
        qdot.length     = n_cells;
        T_hot_0.length  = n_cells;
        T_cold_0.length = n_cells;
        qdot[] = 0.0;
    }

    void display_state_estimate()
    {
        import std.stdio;
        writeln("\n", name~":");
        writeln("T_w = ", T);
    }

    void initialise_state_estimate(const InputPort[] input_ports)
    {
        _T[] = 0.5 * (T_hot_0[] + T_cold_0[]);
    }

    void write_to_lti_model(ref Lti lti)
    {
        foreach(i; 0..n_cells) lti.Xk[global_index(i, "T"), 0] = T[i];
        // no control inputs
        lti.F0[index_start..index_end+1, 0] = dxdt[];
        // no outputs
    }

    override void write_to_stream_prop_vectors(string stream_name, ref Props props)
    {
        throw new Exception("ERROR: Wall temperatures should be accessed via channel."
                ~ " This function should not be called for a WallObserver.");
    }

    override void write_to_wall_prop_vectors(string wall_name, ref WallProps props)
    {
        props.T_wall[wall_istart(wall_name)..wall_iend(wall_name)+1] = T[];

        // Iterate over wall cells and copy in reqd properties, reversed if reqd
        // j indexes wall observer, i indexes wall (lti model)
        if(hot_channel_connected)
        {
            uint j = 0;
            foreach(i; wall_istart(wall_name)..wall_iend(wall_name) + 1)
            {
                static foreach(var; ["k", "T", "Nu", "dkde", "dTde", "dNude",
                    "dkdp", "dTdp", "dNudp", "dkdmdot", "dTdmdot", "dNudmdot"])
                {
                    if(hot_channel_reversed)
                        mixin("props."~var~"_hot[i] = hot_channel."~var~"[$-1-j];");
                    else
                        mixin("props."~var~"_hot[i] = hot_channel."~var~"[j];");
                }
            }
        }

        if(cold_channel_connected)
        {
            uint j = 0;
            foreach(i; wall_istart(wall_name)..wall_iend(wall_name) + 1)
            {
                static foreach(var; ["k", "T", "Nu", "dkde", "dTde", "dNude",
                    "dkdp", "dTdp", "dNudp", "dkdmdot", "dTdmdot", "dNudmdot"])
                {
                    if(cold_channel_reversed)
                        mixin("props."~var~"_cold[i] = cold_channel."~var~"[$-1-j];");
                    else
                        mixin("props."~var~"_cold[i] = cold_channel."~var~"[j];");
                }
            }
        }
    }

    /* To initialize wall, need initial stream temps. */
    void set_initial_stream_temps(const Real[] T_hot, const Real[] T_cold)
    {
        T_hot_0[]  = T_hot[];
        T_cold_0[] = T_cold[];
    }

    /* Fill in parameters for indexing within global state-space model. */
    uint populate_state_indices(uint index_start)
    {
        _index_start = index_start;
        _index_end = index_start + (n_cells * n_states_per_cell) - 1;
        return _index_end + 1;
    }

    void populate_stream_indices(string stream_name, uint index_start)
    {
        throw new Exception("ERROR: Walls should not be linked directly in streams."
                ~ " This function should not be called for a WallObserver.");
    }

    override void populate_wall_indices(string wall_name, uint index_start)
    {
        if(_wall_indices.length > 0)
        {
            import std.format;
            throw new Exception(format!(
                "ERROR: Wall observer %s has been connected to multiple walls.")
                (name));
        }
        _wall_indices[wall_name] = [index_start, index_start + n_cells - 1];
    }

    /*
     * Add heat transfer from an interface to wall.
     * This is not necessarily the total qdot since wall may be connected to either
     * one or two interfaces.
     * qdot must be reset between timesteps.
     */
    void add_qdot(const Real[] qdot_interface) { qdot[] += qdot_interface[]; }
    void reset_qdot() { qdot[] = 0.0; }

    override Real[][string] get_geo_dict()
    {
        import std.conv: to;
        Real[][string] geo_dict = [
            "rho"    : geo.rho,
            "cp"     : geo.cp,
            "a_wall" : geo.a_wall
        ];
        return geo_dict;
    }

    void connect_hot_channel(
            ChannelObserver channel,
            ConvectiveInterface conv_interface,
            bool reversed)
    {
        if(_hot_channel_connected)
        {
            import std.format;
            throw new Exception(format!(
                "ERROR: Wall %s is connected to more than one hot channel.")(name));
        }
        this.hot_channel = channel;
        hot_interface = conv_interface;
        _hot_channel_connected = true;
        _hot_channel_reversed = reversed;
    }

    void connect_cold_channel(
            ChannelObserver channel,
            ConvectiveInterface conv_interface,
            bool reversed)
    {
        if(_cold_channel_connected)
        {
            import std.format;
            throw new Exception(format!(
                "ERROR: Wall %s is connected to more than one cold channel.")(name));
        }
        this.cold_channel = channel;
        cold_interface = conv_interface;
        _cold_channel_connected = true;
        _cold_channel_reversed = reversed;
    }


    override void update_state_estimate(
            const Real t,
            const Real dt,
            const InputPort[] input_ports)
    {
        dxdt[] = qdot[] / (geo.a_wall[] * geo.delta_x[] * geo.rho[] * geo.cp[]);
        _T[] += dxdt[] * dt;
    }

protected:
    /* No sensors for wall so do nothing here. */
    override void update_measurements(const InputPort[] input_ports) {}

    void initialise_geometry(WallCrossSection geometry)
    {
        import std.conv: to;

        geo.delta_x.length = n_cells;
        geo.cp.length      = n_cells;
        geo.rho.length     = n_cells;
        geo.a_wall.length  = n_cells;
        geo.k.length       = n_cells;

        Real dx = length / to!Real(n_cells);
        foreach(i; 0..n_cells)
        {
            Real x         = (to!Real(i) + 0.5) * dx;
            geo.delta_x[i] = dx;
            geo.cp[i]      = geometry.specific_heat;
            geo.rho[i]     = geometry.density;
            geo.k[i]       = geometry.conductivity;
            geo.a_wall[i]  = geometry.cross_area(x) * n_channels;
        }
    }
}

class HxObserver : AObserver
{
public:
    immutable string config;
    immutable Real length;
    immutable uint n_channels;
    CflUpdate!ChannelObserver hot_channel;
    CflUpdate!ChannelObserver cold_channel;
    CflUpdate!WallObserver wall;
    override @property Real dt_stable() { return _dt_stable; }
    override void set_dt_stable(Real new_dt_stable) { _dt_stable = new_dt_stable; }

protected:
    ConvectiveInterface hot_interface;
    ConvectiveInterface cold_interface;
    Real _dt_stable;

public:
    this(
        ObsDef a_def,
        CrossSection[string] geo_dict
        )
    {
        import std.conv: to;
        auto def = a_def.to!HxObsDef;

        immutable uint[string] state_map_tmp; // n/a for hxs (use chan and wall maps)
        immutable uint n_states_per_cell = 3;
        immutable uint[string] input_map_tmp;
        // TODO: Put in correct values so we can access outputs
        immutable uint[string] output_map_tmp = [
            "e_out_hot"  : 9999999,
            "e_out_cold" : 9999999];
        immutable uint n_constraints_tmp = 0;

        string[] output_props;
        foreach(out_prop; def.hot_outputs) output_props ~= out_prop ~ "_hot";
        foreach(out_prop; def.cold_outputs) output_props ~= out_prop ~ "_cold";

        super(a_def, "HxObserver", state_map_tmp, input_map_tmp, output_map_tmp,
                n_states_per_cell, n_constraints_tmp, output_props);

        config     = def.config;
        length     = def.length;
        n_channels = def.n_channels;

        ObsDef[string] def_aa = make_channel_and_wall_defs(def);
        hot_channel = new CflUpdate!ChannelObserver(
                def_aa["HotDef"], ["channel" : geo_dict["hot_channel"]]);
        cold_channel = new CflUpdate!ChannelObserver(
                def_aa["ColdDef"], ["channel" : geo_dict["cold_channel"]]);
        wall = new CflUpdate!WallObserver(
                def_aa["WallObsDef"], ["wall" : geo_dict["wall"]]);

        hot_interface = new ConvectiveInterface(name ~ "_hot_interface", false,
                hot_channel, wall, def.hot_nu_correlation);
        cold_interface = new ConvectiveInterface(name ~ "_cold_interface",
                config == "counterflow", cold_channel, wall, def.cold_nu_correlation);

        hot_channel.connect_wall(wall, hot_interface, false);
        cold_channel.connect_wall(wall, cold_interface, config == "counterflow");
        wall.connect_hot_channel(hot_channel, hot_interface, false);
        wall.connect_cold_channel(cold_channel, cold_interface, config == "counterflow");
    }

    override void populate_observer_map(ref IObserver[string] observer_map)
    {
        observer_map[name] = this;
        observer_map[hot_channel.name] = hot_channel;
        observer_map[cold_channel.name] = cold_channel;
        observer_map[wall.name] = wall;
    }

    void display_state_estimate()
    {
        hot_channel.display_state_estimate();
        cold_channel.display_state_estimate();
        wall.display_state_estimate();
    }

    void initialise_state_estimate(const InputPort[] input_ports)
    {
        hot_channel.initialise_state_estimate(input_ports);
        cold_channel.initialise_state_estimate(input_ports);
        // Wall takes initial guess from stream temps
        wall.set_initial_stream_temps(hot_channel.T, cold_channel.T);
        wall.initialise_state_estimate(input_ports);
        wall.reset_qdot();
        hot_interface.update();
        cold_interface.update();
        update_stable_timestep();
    }

    uint populate_state_indices(uint index_start)
    {
        _index_start = index_start;
        index_start = hot_channel.populate_state_indices(index_start);
        index_start = cold_channel.populate_state_indices(index_start);
        index_start = wall.populate_state_indices(index_start);
        _index_end = wall._index_end;
        return _index_end + 1;
    }

    override uint populate_output_indices(uint y_index_start)
    {
        _y_index_start = y_index_start;
        _y_index_end = y_index_start + n_outputs - 1;
        y_index_start = hot_channel.populate_output_indices(y_index_start);
        y_index_start = cold_channel.populate_output_indices(y_index_start);
        y_index_start = wall.populate_output_indices(y_index_start);
        return _y_index_end + 1;
    }

    override uint populate_cntrl_out_indices(uint z_index_start)
    {
        if(n_cntrl_outs > 0)
        {
            _z_index_start = z_index_start;
            _z_index_end = z_index_start + n_cntrl_outs - 1;
            z_index_start = hot_channel.populate_cntrl_out_indices(z_index_start);
            z_index_start = cold_channel.populate_cntrl_out_indices(z_index_start);
            z_index_start = wall.populate_cntrl_out_indices(z_index_start);
            return _z_index_end + 1;
        }
        else
        {
            _z_index_start = -1;
            _z_index_end = -1;
            return z_index_start;
        }
    }

    void populate_stream_indices(string stream_name, uint index_start)
    {
        throw new Exception("ERROR: Heat exchangers cannot be directly used in "
            ~ " streams, you must use a specific HX channel, i.e. "
            ~ "<HxObserver>.hot_channel.");
    }

    void write_to_lti_model(ref Lti lti)
    {
        hot_channel.write_to_lti_model(lti);
        cold_channel.write_to_lti_model(lti);
        wall.write_to_lti_model(lti);
    }

    override uint global_index(uint i, string prop_key)
    {
        // TODO: Could eventually implement this mapping
        throw new Exception("ERROR: Cannot currently find the global index of "
                ~ "HX state variables by directly calling HX object. Instead, "
                ~ "call specific channel, i.e. <HxObs>.hot_channel.global_index");
    }

    override void control_update(ref Lti lti, ref Mpc mpc)
    {
        hot_channel.control_update(lti, mpc);
        cold_channel.control_update(lti, mpc);
        wall.control_update(lti, mpc);
    }

    override void update_state_estimate(
            const Real t,
            const Real dt,
            const InputPort[] input_ports)
    {
        hot_channel.update(t, input_ports);
        cold_channel.update(t, input_ports);
        wall.update(t, input_ports);
        wall.reset_qdot();
        hot_interface.update();
        cold_interface.update();
        update_stable_timestep();
    }

protected:
    /* Find stable time step from streams. */
    void update_stable_timestep()
    {
        auto dt_hot  = hot_channel.dt_stable;
        auto dt_cold = cold_channel.dt_stable;
        if(dt_hot < dt_cold) _dt_stable = dt_hot;
        else                 _dt_stable = dt_cold;
    }

    /* For HXs, this is handled at the component level */
    override void update_measurements(const InputPort[] input_ports) {}

    ObsDef[string] make_channel_and_wall_defs(HxObsDef def)
    {
        import std.conv: to;

        string[string] hot_channel_dict = [
                "name"        : def.name ~ "_hot",
                "length"      : def.length.to!string,
                "n_cells"     : def.n_cells.to!string,
                "fluid"       : def.hot_fluid,
                "eos_backend" : def.hot_eos_backend,
                "stream_name" : def.hot_stream_name,
                "geo_name"    : def.hot_geo_name,
                "n_channels"  : def.n_channels.to!string,
                "k_L"         : def.k_L.to!string,
                "k_d"         : def.k_d.to!string];
        if(def.hot_e_min_constraint)
            hot_channel_dict["e_out_min"] = def.e_hot_min.to!string;

        string[string] cold_channel_dict = [
                "name"        : def.name ~ "_cold",
                "length"      : def.length.to!string,
                "n_cells"     : def.n_cells.to!string,
                "fluid"       : def.cold_fluid,
                "eos_backend" : def.cold_eos_backend,
                "stream_name" : def.cold_stream_name,
                "geo_name"    : def.cold_geo_name,
                "n_channels"  : def.n_channels.to!string,
                "k_L"         : def.k_L.to!string,
                "k_d"         : def.k_d.to!string];
        if(def.cold_e_max_constraint)
            cold_channel_dict["e_out_max"] = def.e_cold_max.to!string;

        string[string] wall_dict = [
                "name"        : def.name ~ "_wall",
                "length"      : def.length.to!string,
                "n_cells"     : def.n_cells.to!string,
                "geo_name"    : def.wall_geo_name,
                "n_channels"  : def.n_channels.to!string];

        ObsDef[string] def_aa = [
                "HotDef" : new ChannelObsDef(hot_channel_dict,
                        def.hot_sensor_map, def.hot_outputs),
                "ColdDef" : new ChannelObsDef(cold_channel_dict,
                        def.cold_sensor_map, def.cold_outputs),
                "WallObsDef" : new WallObsDef(wall_dict)
        ];

        return def_aa;
    }
}

/*
 * Calculates convective heat transfer rate between a fluid stream and wall.
 * To facilitate modelling of counter-flow HXs, allows either reversed or
 * forward indexing of wall temperature vector.
 * Convective interface is always aligned in the direction of flow in the fluid
 * channel (i.e. interface.qdot[i] always corresponds with stream.qdot[i]).
 */
class ConvectiveInterface
{
    import q1dcfd.state_space_control: NuCorrelation, generate_nu_correlation;

public:
    immutable string name;
    immutable uint n_cells;
    immutable bool reversed;

protected:
    ChannelObserver channel;
    WallObserver wall;
    NuCorrelation nu_corr;
    Real[] qdot, Nu, U; // U is heat transfer coefficient

public:
    this(
        const string name,
        bool reversed,
        ChannelObserver channel,
        WallObserver wall,
        string nu_corr_name
        )
    {
        this.name     = name;
        this.reversed = reversed;
        this.channel  = channel;
        this.wall     = wall;
        this.n_cells  = channel.n_cells;
        assert(channel.n_cells == wall.n_cells,
                "Convective interface in observer currently requires that wall "
                ~ "and channel have same number of cells.");
        qdot.length   = n_cells;
        U.length      = n_cells;
        Nu.length     = n_cells;

        this.nu_corr = generate_nu_correlation(nu_corr_name);
    }

    void update()
    {
        update_qdot();
        export_qdot_nu();
    }

    void update_qdot()
    {
        foreach(i; 0..n_cells)
            Nu[i] = nu_corr.calculate_nu(channel.Re[i], channel.Pr[i]);
        U[] = Nu[] * channel.k[] / channel.geo.l_c[];
        auto delta_T = new Real[](n_cells);
        if(reversed) foreach(i; 0..n_cells) delta_T[i] = channel.T[i] - wall.T[$-1-i];
        else         foreach(i; 0..n_cells) delta_T[i] = channel.T[i] - wall.T[i];
        qdot[] = U[] * channel.geo.q_area[] * delta_T[];
    }

    void export_qdot_nu()
    {
        channel.set_qdot(qdot);
        channel.set_nu(Nu);
        if(reversed)
        {
            import std.algorithm.mutation: reverse;
            auto qdot_flipped = new Real[](n_cells);
            qdot_flipped = qdot;
            qdot_flipped.reverse();
            wall.add_qdot(qdot_flipped);
        }
        else wall.add_qdot(qdot);
    }
}

class PumpObserver : AObserver
{
    import q1dcfd.state_space_control: ControllerEos, InputPairs, FluidState,
            generate_controller_eos, PumpObsDef;

    enum props =
            ["mdot", "p", "v", "e", "k", "h", "rho", "T", "mu", "cp", "Pr", "Re"];
    mixin(InitPropValues!(props));
    mixin(InitPropValues!(["mdot_target", "mdot_pump", "mdot_dot_pump",
                "mdot_dot_dot_pump"]));

public:
    immutable uint inputs_per_cell = 1;
    // No stability constraints, so just use a fixed update rate
    override @property Real dt_stable() { return _dt_stable; }
    override void set_dt_stable(Real new_dt_stable) { _dt_stable = new_dt_stable; }

protected:
    struct SensorsIndex
    {
        uint p, T, e, rho, mdot, mdot_pump, mdot_dot_pump, mdot_dot_dot_pump;
    }
    SensorsIndex sensors;
    struct ActuatorIndex { uint mdot_target; }
    ActuatorIndex actuators;
    ControllerEos eos;
    Real _dt_stable = 0.001;
    Real mdot_max, mdot_min, mdot_dot_max; // constraints
    Real a_in, i_rotor, k_rotor, c_rotor, omega_n, zeta, k_pump; // parameters

public:
    this(
        ObsDef a_def,
        CrossSection[string] geo_dict
        )
    {
        import std.conv: to;
        auto def = a_def.to!PumpObsDef;

        immutable uint[string] state_map_tmp = [
            "e_out"    : 0,
            "p_out"    : 1,
            "mdot"     : 2,
            "mdot_dot" : 3];
        immutable uint n_states_per_cell = 4;
        immutable uint[string] input_map_tmp = [
            "mdot_target" : 0];
        immutable uint[string] output_map_tmp = [
            "mdot" : 0];
        immutable uint n_constraints_tmp = 0;
        super(a_def, "PumpObserver", state_map_tmp, input_map_tmp,
                output_map_tmp, n_states_per_cell, n_constraints_tmp,
                def.outputs);

        a_in         = def.a_in;
        mdot_max     = def.mdot_max;
        mdot_dot_max = def.mdot_dot_max;
        // TODO: We could put in a check that a_in is correct by comparing
        // v_calculated and v_measured.

        initialize_pump_model();
        eos = generate_controller_eos(def.eos_backend, def.fluid, def.stream_name);

        // Mappings from q1d sensor IDs to sensor names for this observer
        sensors.p    = def.sensor_map["p"];
        sensors.T    = def.sensor_map["T"];
        sensors.e    = def.sensor_map["e"];
        sensors.mdot = def.sensor_map["mdot"];
        sensors.mdot_pump         = def.sensor_map["mdot_pump"];
        sensors.mdot_dot_pump     = def.sensor_map["mdot_dot_pump"];
        sensors.mdot_dot_dot_pump = def.sensor_map["mdot_dot_dot_pump"];

        // Same as sensors but for actuator IDs
        actuators.mdot_target = def.actuator_map["mdot_target"];
    }

    // TODO: Should be a different function?
    override Real get_predicted_output(string prop)
    {
        if      (prop == "p")    return p;
        else if (prop == "e")    return e;
        else if (prop == "h")    return h;
        else if (prop == "mdot") return mdot;
        else throw new Exception(
                "ERROR: Invalid output property for PumpObserver.");
    }

    void display_state_estimate()
    {
        import std.stdio;
        writeln("\n", name~":");
        writeln("mdot  = ", mdot);
        writeln("p_out = ", p);
        writeln("T_out = ", T);
        writeln("e_out = ", e);
    }

    /* For a pump, we can just measure everything. */
    void initialise_state_estimate(const InputPort[] input_ports)
    {
        update_measurements(input_ports);
        update_derived_props();
    }

    /* Fill in parameters for indexing within global state-space model. */
    uint populate_state_indices(uint index_start)
    {
        _index_start = index_start;
        _index_end = index_start + n_states_per_cell - 1;
        return _index_end + 1;
    }

    void populate_stream_indices(string stream_name, uint index_start)
    {
        // Pumps can only be part of 1 stream
        if(_stream_indices.length > 1)
        {
            import std.format;
            throw new Exception(format!(
                "Pump observer %s has been connected to multiple streams.")
                (name));
        }
        _stream_indices[stream_name] = [index_start, index_start + n_cells - 1];
    }

    override void write_to_stream_prop_vectors(string stream_name, ref Props props)
    {
        // Just need one partial derivative: dhde
        auto H = 1E-8;
        eos.update_eos(InputPairs.p_e, p, (1+H)*e, get_fluid_state());
        auto h_plus = eos.h;
        eos.update_eos(InputPairs.p_e, p, (1-H)*e, get_fluid_state());
        auto h_minus = eos.h;
        props.dhde[stream_istart(stream_name)] = (h_plus - h_minus) / (2 * H * e);

        // PumpObserver just has one cell
        static foreach (var; ["v", "e", "k", "h", "rho", "T"])
            mixin("props."~var~"[stream_istart(stream_name)] = " ~var~";");

        // p and mdot are constant for stream
        // mdot and mdot_pump are always extremely close (stagnation reservoir
        // drives mdot to mdot_pump by quickly modulating pressure)
        props.p = p;
        props.mdot = mdot;
    }

    void write_to_lti_model(ref Lti lti)
    {
        lti.Xk[global_index(0, "e_out"), 0] = e;
        lti.Xk[global_index(0, "p_out"), 0] = p;
        lti.Xk[global_index(0, "mdot"), 0] = mdot;
        lti.Xk[global_index(0, "mdot_dot"), 0] = mdot_dot_pump;

        // e and p stay constant, no entries reqd in F0
        lti.F0[global_index(0, "mdot"), 0] = mdot_dot_pump;
        lti.F0[global_index(0, "mdot_dot"), 0] = mdot_dot_dot_pump;

        // Outputs
        // lti.Yk = ...
        lti.Cy[y_global_index("mdot"), global_index(0, "mdot")] = 1;
        foreach(var; z_vars)
        {
            if(var == "mdot")
            {
                lti.Zk[z_global_index("mdot"), 0] = mdot;
                lti.C[z_global_index("mdot"), global_index(0, "mdot")] = 1;
            }
            else
                throw new Exception("ERROR: Output variable " ~ var
                    ~ " not implemented for PumpObserver.");
        }
    }

    /* Reqd for integration with a Stream object */
    override Real[][string] get_geo_dict()
    {
        Real[][string] geo_dict = [
            "flow_area"  : [Real.nan],
            "delta_x"    : [Real.nan],
            "n_channels" : [Real.nan]
        ];
        return geo_dict;
    }

    Real[string] get_pump_params_dict()
    {
        Real[string] pump_params_dict = [
            "k_rotor" : k_rotor,
            "i_rotor" : i_rotor,
            "c_rotor" : c_rotor
        ];
        return pump_params_dict;
    }

    /* Apply MPC updates to main simulation code */
    override void update_actuators(ref Lti lti, ref Real[] _output_ports)
    {
        _output_ports[actuators.mdot_target] =
            lti.Uk[u_global_index("mdot_target"), 0];
    }

    override void initialise_u(ref Lti lti)
    {
        lti.Uk[u_global_index("mdot_target"), 0] = mdot;
        lti.Ukm1[u_global_index("mdot_target"), 0] = mdot;
    }

    override void update_state_estimate(
            const Real t,
            const Real dt,
            const InputPort[] input_ports)
    {
        update_measurements(input_ports);
        update_derived_props();
    }

protected:
    override void update_measurements(const InputPort[] input_ports)
    {
        _T    = input_ports[sensors.T]();
        _p    = input_ports[sensors.p]();
        _mdot = input_ports[sensors.mdot]();
        _mdot_pump         = input_ports[sensors.mdot_pump]();
        _mdot_dot_pump     = input_ports[sensors.mdot_dot_pump]();
        _mdot_dot_dot_pump = input_ports[sensors.mdot_dot_dot_pump]();
    }

    void update_derived_props()
    {
        eos.update_eos(InputPairs.p_T, p, T);
        _e   = eos.e;
        _k   = eos.k;
        _h   = eos.h;
        _rho = eos.rho;
        _T   = eos.T;
        _mu  = eos.mu;
        _cp  = eos.cp;
        _v   = mdot / (rho * a_in);
        _Pr  = mu * cp / k;
    }

    /* Stores fluid props at cell i in a FluidState object */
    FluidState get_fluid_state()
    {
        // Just one cell here
        FluidState fluid_state;
        fluid_state.p    = p;
        fluid_state.e    = e;
        fluid_state.k    = k;
        fluid_state.h    = h;
        fluid_state.rho  = rho;
        fluid_state.T    = T;
        fluid_state.mu   = mu;
        return fluid_state;
    }

    /*
     * This is a copy of the initialization code for the compressible and
     * incompressible pumps, with only the variables that we need for the
     * observer and LTI model.
     */
    void initialize_pump_model()
    {
        import std.math: PI;
        i_rotor = 1;   // Rotational inertia (kg m^2)
        omega_n = 2.0; // natural frequency (Hz)
        zeta    = 1.3; // damping ratio

        // Performance limits
        Real omega_max = 1000 * 2 * PI / 60;
        Real omega_min = 10 * 2 * PI / 60; // don't allow zero flow for stability

        // Calculated parameters (to give desired omega, zeta)
        import std.math: pow;
        k_rotor = i_rotor * pow(omega_n, 2);
        c_rotor = 2 * i_rotor * zeta * omega_n;
        k_pump = mdot_max / omega_max;
        mdot_min = k_pump * omega_min;
    }

    override void update_constraints(ref Mpc mpc)
    {
        // Continuous-time rate constraints
        mpc.DULow[u_global_index("mdot_target"), 0]  = -mdot_dot_max;
        mpc.DUHigh[u_global_index("mdot_target"), 0] =  mdot_dot_max;

        mpc.ULow[u_global_index("mdot_target"), 0]  = 2.0; // TODO
        // mpc.ULow[u_global_index("mdot_target"), 0]  = mdot_min;
        mpc.UHigh[u_global_index("mdot_target"), 0] = mdot_max;
    }
}

// TODO: Better to handle synchronous vs asynchronous turbomachines via template
// mixins rather than a flag

abstract class TurbomachineObserver : AObserver
{
    import q1dcfd.state_space_control: ControllerEos, InputPairs, FluidState,
            generate_controller_eos, TurbomachineObsDef;
    import q1dcfd.maps.turbomachine: Turbomachine;

    enum props = ["dT_motor_dt", "T_load", "T_motor", "speed", "mdot", "p_in",
               "T_in", "e_in", "h_in", "rho_in", "v_in", "p_out", "T_out",
               "e_out", "h_out", "rho_out", "v_out", "w_sh", "eta"];
    mixin(InitPropValues!(props));

public:
    immutable uint inputs_per_cell = 1;
    // No stability constraints, so just use a fixed update rate
    immutable string turbomachine_type; // compressor or turbine
    immutable Real i_rotor, a_in, a_out; // parameters
    immutable bool synchronous_flag; // fixed speed or not?
    @property Real speed_nominal() { return _speed_nominal; }
    override @property Real dt_stable() { return _dt_stable; }
    override void set_dt_stable(Real new_dt_stable) { _dt_stable = new_dt_stable; }
    Turbomachine turbomachine_model;

protected:
    struct SensorsIndex
    {
        uint dT_motor_dt, T_motor, T_load, speed, mdot, p_in, T_in, p_out,
             T_out, eta;
    }
    SensorsIndex sensors;
    struct ActuatorIndex { uint dT_motor_dt; }
    ActuatorIndex actuators;
    ControllerEos eos;
    Real _dt_stable = 0.001;
    // Bad way to handle sync vs async below...
    immutable uint[string] state_map_tmp, input_map_tmp, output_map_tmp;
    immutable uint n_states_per_cell, n_constraints_tmp;
    Real _speed_nominal;

public:
    this(ObsDef a_def, CrossSection[string] geo_dict)
    {
        import std.conv: to;
        auto def = a_def.to!TurbomachineObsDef;
        turbomachine_type = def.turbomachine_type;

        // Set fixed speed or not (if asynchronous, T_motor is an extra state
        // and dT_motor_dt is an input)
        synchronous_flag = def.synchronous_flag;
        if (!synchronous_flag)
        {
            // Turbomachines have their downstream properties as state variables.
            // Mass flow rate should be equal at outlet of joined turbomachines so
            // this state var will be duplicated (may need to handle eventually?).
            state_map_tmp = [
                "e_out"   : 0,
                "p_out"   : 1,
                "mdot"    : 2,
                "speed"   : 3,
                "T_motor" : 4];
            n_states_per_cell = 5; // always 1 cell only
            input_map_tmp = ["dT_motor_dt" : 0];
            output_map_tmp = [
                "mdot"    : 0,
                "speed"   : 1,
                "T_motor" : 2,
                "W_dot_s" : 3,  // shaft work
                "eta"     : 4,  // efficiency
                "e_out"   : 5,
                "p_out"   : 6];
            n_constraints_tmp = 0;

            // Asynchronous compressors will always have a surge line.
            // Currently this is implemented as a constraint on Z, so definition
            // file must have mdot and speed (constrained vars) specified as
            // outputs.
            // TODO: Remove these checks once constraints on X are implemented.
            import std.algorithm: canFind;
            if (   !canFind(def.outputs, "mdot")
                || !canFind(def.outputs, "speed")
                || !canFind(def.outputs, "T_motor"))
                throw new Exception("ERROR: Currently, to handle surge constraint, "
                        ~ "'mdot', 'speed', and 'T_motor' must be outputs for all "
                        ~ "asynchronous compressors.");
        }
        else
        {
            // See comments about states for asynchronous case above
            state_map_tmp = [
                "e_out" : 0,
                "p_out" : 1,
                "mdot"  : 2,
                "speed" : 3];
            n_states_per_cell = 4; // always 1 cell only
            output_map_tmp = [
                "mdot"  : 0,
                "e_out" : 1,
                "p_out" : 2];
            n_constraints_tmp = 0;
        }
        super(a_def, "TurbomachineObserver", state_map_tmp, input_map_tmp,
                output_map_tmp, n_states_per_cell,  n_constraints_tmp,
                def.outputs);

        a_in  = def.a_in;
        a_out = def.a_out;
        if (!synchronous_flag) i_rotor = def.i_rotor;

        eos = generate_controller_eos(def.eos_backend, def.fluid, def.stream_name);

        // Mappings from q1d sensor IDs to sensor names for this observer
        sensors.T_load = def.sensor_map["T_load"];
        sensors.speed  = def.sensor_map["speed"];
        sensors.mdot   = def.sensor_map["mdot"];
        sensors.p_in   = def.sensor_map["p_in"];
        sensors.T_in   = def.sensor_map["T_in"];
        sensors.p_out  = def.sensor_map["p_out"];
        sensors.T_out  = def.sensor_map["T_out"];
        sensors.eta    = def.sensor_map["eta"];
        if (!synchronous_flag)
        {
            sensors.T_motor = def.sensor_map["T_motor"];
            sensors.dT_motor_dt = def.sensor_map["dT_motor_dt"];
            actuators.dT_motor_dt = def.actuator_map["dT_motor_dt"];
        }
    }

    // TODO: Should be a different function?
    override Real get_predicted_output(string prop)
    {
        if      (prop == "p")    return p_out;
        else if (prop == "e")    return e_out;
        else if (prop == "h")    return h_out;
        else if (prop == "mdot") return mdot;
        else throw new Exception(
                "ERROR: Invalid output property for TurbomachineObserver.");
    }

    void display_state_estimate()
    {
        import std.stdio;
        writeln("\n", name~":");
        writeln("speed = ", speed);
        writeln("mdot  = ", mdot);
        writeln("p_out = ", p_out);
        writeln("e_out = ", e_out);
    }

    /* For a turbomachine, we can just measure everything. */
    void initialise_state_estimate(const InputPort[] input_ports)
    {
        update_measurements(input_ports);
        update_derived_props();
    }

    /* Fill in parameters for indexing within global state-space model. */
    uint populate_state_indices(uint index_start)
    {
        _index_start = index_start;
        _index_end = index_start + n_states_per_cell - 1;
        return _index_end + 1;
    }

    /*
     * We define turbomachines as belonging to the single stream which starts at
     * their outlet (and thus they may be part of only one stream).
     */
    void populate_stream_indices(string stream_name, uint index_start)
    {
        if(_stream_indices.length > 1)
        {
            import std.format;
            throw new Exception(format!(
                "Turbomachine observer %s has been connected to multiple streams.")
                (name));
        }
        _stream_indices[stream_name] = [index_start, index_start + n_cells - 1];
    }

    override void write_to_stream_prop_vectors(string stream_name, ref Props props)
    {
        // Just need one partial derivative: (dhde)_out
        auto H = 1E-8;
        eos.update_eos(InputPairs.p_e, p_out, (1+H)*e_out, get_fluid_state_out());
        auto h_out_plus = eos.h;
        eos.update_eos(InputPairs.p_e, p_out, (1-H)*e_out, get_fluid_state_out());
        auto h_out_minus = eos.h;
        props.dhde[stream_istart(stream_name)] = (h_out_plus - h_out_minus) / (2 * H * e_out);

        static foreach (var; ["v", "e", "h", "T"])
            mixin("props."~var~"[stream_istart(stream_name)] = " ~ var ~ "_out;");

        // Turbomachine sets the pressure for the stream flowing out of it
        props.p = p_out;
        props.mdot = mdot;
    }

    void write_to_lti_model(ref Lti lti)
    {
        lti.Xk[global_index(0, "e_out"), 0] = e_out;
        lti.Xk[global_index(0, "p_out"), 0] = p_out;
        lti.Xk[global_index(0, "mdot"), 0]  = mdot; // Better to avg over turb and comp?
        lti.Xk[global_index(0, "speed"), 0] = speed;
        if (!synchronous_flag) lti.Xk[global_index(0, "T_motor"), 0] = T_motor;

        // TODO: Add more possible outputs (eta, PR, etc)

        // Outputs (Cy : all outputs, C : controlled outputs)
        lti.Cy[y_global_index("mdot"), global_index(0, "mdot")] = 1;
        lti.Cy[y_global_index("e_out"), global_index(0, "e_out")] = 1;
        lti.Cy[y_global_index("p_out"), global_index(0, "p_out")] = 1;
        if (!synchronous_flag)
        {
            lti.Cy[y_global_index("speed"), global_index(0, "speed")] = 1;
            lti.Cy[y_global_index("T_motor"), global_index(0, "T_motor")] = 1;
        }
        foreach(var; z_vars)
        {
            if(var == "mdot")
            {
                lti.Zk[z_global_index("mdot"), 0] = mdot;
                lti.C[z_global_index("mdot"), global_index(0, "mdot")] = 1;
            }
            else if(var == "speed")
            {
                lti.Zk[z_global_index("speed"), 0] = speed;
                lti.C[z_global_index("speed"), global_index(0, "speed")] = 1;
            }
            else if(var == "T_motor")
            {
                lti.Zk[z_global_index("T_motor"), 0] = T_motor;
                lti.C[z_global_index("T_motor"), global_index(0, "T_motor")] = 1;
            }
            else if(var == "e_out")
            {
                lti.Zk[z_global_index("e_out"), 0] = e_out;
                lti.C[z_global_index("e_out"), global_index(0, "e_out")] = 1;
            }
            else if(var == "p_out")
            {
                lti.Zk[z_global_index("p_out"), 0] = p_out;
                lti.C[z_global_index("p_out"), global_index(0, "p_out")] = 1;
            }
            else
                throw new Exception("ERROR: Output variable " ~ var
                    ~ " not implemented for TurbomachineObserver.");
        }
    }

    override Real[][string] get_geo_dict()
    {
        Real[][string] geo_dict = [
            "flow_area"  : [Real.nan],
            "delta_x"    : [Real.nan],
            "n_channels" : [Real.nan]
        ];
        return geo_dict;
    }

    /* Apply MPC updates to main simulation code */
    override void update_actuators(ref Lti lti, ref Real[] _output_ports)
    {
        if (!synchronous_flag)
        {
            _output_ports[actuators.dT_motor_dt] =
                lti.Uk[u_global_index("dT_motor_dt"), 0];
        }
    }

    override void initialise_u(ref Lti lti)
    {
        if (!synchronous_flag)
        {
            lti.Uk[u_global_index("dT_motor_dt"), 0]   = dT_motor_dt;
            lti.Ukm1[u_global_index("dT_motor_dt"), 0] = dT_motor_dt;
        }
    }

    override void update_state_estimate(
            const Real t,
            const Real dt,
            const InputPort[] input_ports)
    {
        update_measurements(input_ports);
        update_derived_props();
    }

protected:
    override void update_measurements(const InputPort[] input_ports)
    {
        _T_load = input_ports[sensors.T_load]();
        _speed  = input_ports[sensors.speed]();
        _mdot   = input_ports[sensors.mdot]();
        _p_in   = input_ports[sensors.p_in]();
        _T_in   = input_ports[sensors.T_in]();
        _p_out  = input_ports[sensors.p_out]();
        _T_out  = input_ports[sensors.T_out]();
        _eta    = input_ports[sensors.eta]();
        _w_sh   = T_load * (speed * speed_nominal);
        if(!synchronous_flag) {
            _T_motor     = input_ports[sensors.T_motor]();
            _dT_motor_dt = input_ports[sensors.dT_motor_dt]();
        }
    }

    void update_derived_props()
    {
        eos.update_eos(InputPairs.p_T, p_in, T_in);
        _e_in   = eos.e;
        _h_in   = eos.h;
        _rho_in = eos.rho;
        _v_in   = mdot / (rho_in * a_in);

        eos.update_eos(InputPairs.p_T, p_out, T_out);
        _e_out   = eos.e;
        _h_out   = eos.h;
        _rho_out = eos.rho;
        _v_out   = mdot / (rho_out * a_out);
    }

    /* Collects inlet fluid props in a FluidState object */
    FluidState get_fluid_state_in()
    {
        FluidState fluid_state;
        fluid_state.p    = p_in;
        fluid_state.e    = e_in;
        fluid_state.h    = h_in;
        fluid_state.T    = T_in;
        fluid_state.rho  = rho_in;
        return fluid_state;
    }

    FluidState get_fluid_state_out()
    {
        FluidState fluid_state;
        fluid_state.p    = p_out;
        fluid_state.e    = e_out;
        fluid_state.h    = h_out;
        fluid_state.T    = T_out;
        fluid_state.rho  = rho_out;
        return fluid_state;
    }

   /*
    * Compute partial derivatives of turbomachine T_load, P_out, and e_out with
    * respect to controller state variables (mdot, p_in, speed, e_in).
    * Inputs:
    *  • result_0 - current operating point turbo model results
    *  • dvar     - state variable to differentiate w.r.t.
    *  • var_0    - value of 'dvar' at current operating point
    */
    Real[string] compute_turbomachine_partials(double[string] result_0,
            const string dvar, Real var_0, Real H=1E-3)
    {
        import q1dcfd.utils.math: square;
        // immutable H = 1E-3; // TODO-0303
        double[string] result_plus;
        switch(dvar)
        {
            case "mdot":
                result_plus = turbomachine_model.calc_downstream((1+H)*mdot,
                        speed, p_in, T_in);
                break;
            case "p_in":
                result_plus = turbomachine_model.calc_downstream(mdot, speed,
                        (1+H)*p_in, T_in);
                break;
            case "T_in":
                result_plus = turbomachine_model.calc_downstream(mdot, speed,
                        p_in, (1+H)*T_in);
                break;
            case "speed":
                result_plus = turbomachine_model.calc_downstream(mdot,
                        (1+H)*speed, p_in, T_in);
                break;
            default:
                throw new Exception("ERROR: Invalid variable to differentiate"
                        ~ " w.r.t. in function 'compute_turbomachine_partials',"
                        ~ " must be one of [p_in, p_out, T_in, speed].");
        }

        auto mdot_plus       = result_plus["mdot"];
        auto delta_h_plus    = result_plus["delta_h"];
        auto rho_out_plus    = result_plus["rho_out"];
        auto e_out_plus      = result_plus["e_out"];
        auto p_out_plus      = result_plus["P_out"];
        auto PR_plus         = result_plus["PR"];
        auto PR              = result_0["PR"]; // not stored by default
        auto mdot_surge_plus = result_plus["mdot_surge"];
        auto mdot_surge      = result_0["mdot_surge"]; // not stored by default

        // IMPORTANT, TODO: rho_in should change for some
        auto v_in_plus = mdot_plus / (a_in * rho_in);
        auto v_out_plus = mdot_plus / (a_out * rho_out_plus);
        // TODO: Should calculate w_sh and T_load inside compressor model
        auto w_sh_plus = (delta_h_plus + 0.5*v_out_plus.square -
                0.5*v_in_plus.square) * mdot_plus;
        // Account for change in speed when computing pders w.r.t. speed
        Real T_load_plus;
        if(dvar == "speed")
            T_load_plus = w_sh_plus / ((1+H)*speed *
                        turbomachine_model.get_speed_nominal());
        else
            T_load_plus = w_sh_plus / (speed *
                turbomachine_model.get_speed_nominal());

        return [
            "T_load" : (T_load_plus - T_load) / (H * var_0),
            "P_out"  : (p_out_plus - p_out) / (H * var_0),
            "e_out"  : (e_out_plus - e_out) / (H * var_0),
            "PR"     : (PR_plus - PR) / (H * var_0),
            "mdot_surge" : (mdot_surge_plus - mdot_surge) / (H * var_0)
        ];
    }

    override void update_constraints(ref Mpc mpc)
    {
        if(!synchronous_flag)
        {
            // TODO: Jobfile input for these constraints.

            // MANUAL PLOT INPUT!!! (QSUMMARY)
            mpc.ULow[u_global_index("dT_motor_dt"), 0]  = -15.0;
            mpc.UHigh[u_global_index("dT_motor_dt"), 0] =  15.0;

            // These are second derivatives of U, effectively unconstrained
            mpc.DULow[u_global_index("dT_motor_dt"), 0]  = -1000.0;
            mpc.DUHigh[u_global_index("dT_motor_dt"), 0] =  1000.0;
        }
    }
}

/* Open-cycle compressor */
class CompressorObserver : TurbomachineObserver
{
    // Open cycle compressor has P_high = P_out as state var, so find partial
    // derivatives of P_out and derive with respect to P_in.
    enum pders = ["dT_load_dp_in", "dT_load_dmdot", "dT_load_dspeed",
               "dp_out_dp_in", "dp_out_dmdot", "dp_out_dspeed", "de_out_dp_in",
               "de_out_dmdot", "de_out_dspeed", "dmdot_surge_dspeed",
               "dWdot_sh_dmdot", "dWdot_sh_dspeed"];
    mixin(InitPropValues!(pders));

    Real mdot_surge;

    this(ObsDef a_def, CrossSection[string] geo_dict)
    {
        super(a_def, geo_dict);

        import std.conv: to;
        auto def = a_def.to!TurbomachineObsDef;
        initialise_turbomachine_model(def);
    }

    void initialise_turbomachine_model(ObsDef def)
    {
        import q1dcfd.lconfig: ROOT_DIRECTORY;
        import q1dcfd.maps: Compressor;
        import q1dcfd.stream: Stream;
        import q1dcfd.simulation: Simulation;
        import q1dcfd.state_space_control: CompressorObsDef;
        import std.conv: to;

        auto c_def = def.to!CompressorObsDef;

        Stream stream = cast(Stream) Simulation.get_stream(c_def.stream_name);
        string map_dir = "/source/maps/" ~ c_def.map_name;
        turbomachine_model = new Compressor(ROOT_DIRECTORY ~ map_dir, stream,
                c_def.p_0, c_def.k_dh, c_def.k_eta);
        _speed_nominal = turbomachine_model.get_speed_nominal();
    }

    // TODO 2008: Force speed and T_motor to be in Z [this may automatically
    // happen if "speed" not in z_vars etc...].
    override void update_constraints(ref Mpc mpc)
    {
        super.update_constraints(mpc);

        if(!synchronous_flag)
        {
            // Motor torque must be 0 < T_motor < T_motor_max
            immutable Real T_m_max = 200.0;
            mpc.ZVars[2*z_global_index("T_motor")..2*z_global_index("T_motor")+2] = 1;
            mpc.ZLow[z_global_index("T_motor"), 0] = 0.0;
            mpc.ZHigh[z_global_index("T_motor"), 0] = T_m_max;
            mpc.ZHighW[z_global_index("T_motor"), 0] = 1E6;
        }
    }

    // Deprecated
    void update_constraints_old(ref Mpc mpc)
    {
        super.update_constraints(mpc);

        if(!synchronous_flag)
        {
            import std.stdio;
            writeln("\nMake sure have n_constraints =/= 0 if we get to here.");
            import core.stdc.stdlib;
            exit(-1);

            // Output constraints are expressed in terms of perturbed ('bar') variables
            // First constraint is surge
            //      mdot_0 + mdot_bar >=  mdot_surge + dmdot_surge_dspeed * speed_bar
            auto row = c_index_start; // First constraint
            immutable Real surge_margin = 0.03;
            // These constraints are on outputs (i.e. absolute values)
            mpc.GammaL[row, z_global_index("speed")] = dmdot_surge_dspeed;
            mpc.GammaL[row, z_global_index("mdot")] = -1.0;
            mpc.gL[row, 0] =
                (mdot_surge - mdot) * (1+surge_margin);
        }
    }

    override void compute_partial_derivatives()
    {
        // These results should already stored in this class, but currently
        // recomputing for safety [could add debug mode check or use stored vals
        // otherwise]
        Real[string] result_0 = turbomachine_model.calc_downstream(
                mdot, speed, p_in, T_in);
        mdot_surge = result_0["mdot_surge"]; // store for surge constraint

        // Open cycle compressor, so just need pders wrt p_out, mdot, and speed
        auto pders_p_in  = compute_turbomachine_partials(result_0, "p_in", p_in);
        auto pders_mdot  = compute_turbomachine_partials(result_0, "mdot", mdot);
        auto pders_speed = compute_turbomachine_partials(result_0, "speed", speed);
        _dT_load_dp_in  = pders_p_in["T_load"];
        _dp_out_dp_in   = pders_p_in["P_out"];
        _de_out_dp_in   = pders_p_in["e_out"];
        _dT_load_dmdot  = pders_mdot["T_load"];
        _dp_out_dmdot   = pders_mdot["P_out"];
        _de_out_dmdot   = pders_mdot["e_out"];
        _dT_load_dspeed = pders_speed["T_load"];
        _dp_out_dspeed  = pders_speed["P_out"];
        _de_out_dspeed  = pders_speed["e_out"];
        // For surge constraint
        _dmdot_surge_dspeed  = pders_speed["mdot_surge"];

        // Partial derivatives of shaft power.
        // For compressor,
        //   Wdot = T_load * N_c * N_c0 
        // where N_c is normalized shaft speed (state var) and N_c0 is nominal.
        // As compressor is asynchronous,
        //   d/dx Wdot = N_c0 * d/dx (N_c * T_load),  [product rule]
        //   d/dx Wdot = N_c0 * (d/dx N_c * T_load + N_c * d/dx T_load).
        // dN_c_dmdot = 0 (both are states), so,
        _dWdot_sh_dmdot = speed_nominal * dT_load_dmdot * speed;
        // dN_c_dN_c = 1, so,
        _dWdot_sh_dspeed = speed_nominal * (1 * T_load + dT_load_dspeed * speed);

        // import std.stdio;
        // writeln("dWdot_sh_dmdot  = ", dWdot_sh_dmdot);
        // writeln("dWdot_sh_dspeed = ", dWdot_sh_dspeed);
        // import core.stdc.stdlib;
        // exit(-1);

        // // For debugging
        // make_turbo_map_plots();
        // make_pder_plots(result_0);
    }

    void make_turbo_map_plots()
    {
        import std.conv: to;
        import q1dcfd.utils.math: square;

        // Make speed and mdot arrays
        uint n_points = 50;
        double mdot_start = 8.0;
        double mdot_end   = 14.0;
        double speed_start = 0.7;
        double speed_end   = 1.2;
        double delta_mdot  = (mdot_end - mdot_start) / to!double(n_points);
        double delta_speed = (speed_end - speed_start) / to!double(n_points);

        // Inputs
        auto mdot_arr  = new double[](n_points);
        auto speed_arr = new double[](n_points);
        // Outputs
        auto T_load_vs_mdot  = new double[](n_points);
        auto p_out_vs_mdot   = new double[](n_points);
        auto T_load_vs_speed = new double[](n_points);
        auto p_out_vs_speed  = new double[](n_points);

        // Compute T_load and P_out arrays
        double[string] result;
        foreach(i; 0..n_points)
        {
            // Compute values for changing mdot
            mdot_arr[i] = mdot_start + to!double(i) * delta_mdot;
            result = turbomachine_model.calc_downstream(mdot_arr[i], speed,
                    p_in, T_in);
            auto mdot_i    = result["mdot"];
            auto delta_h_i = result["delta_h"];
            auto rho_out_i = result["rho_out"];
            auto p_out_i   = result["P_out"];
            auto v_in_i = mdot_i / (a_in * rho_in);
            auto v_out_i = mdot_i / (a_out * rho_out_i);
            auto w_sh_i = (delta_h_i + 0.5*v_out_i.square -
                    0.5*v_in_i.square) * mdot_i;
            auto T_load_i = w_sh_i / (speed *
                    turbomachine_model.get_speed_nominal());
            T_load_vs_mdot[i] = T_load_i;
            p_out_vs_mdot[i]  = p_out_i;

            // Compute values for changing speed
            speed_arr[i] = speed_start + to!double(i) * delta_speed;
            result = turbomachine_model.calc_downstream(mdot, speed_arr[i],
                    p_in, T_in);
            mdot_i    = result["mdot"];
            delta_h_i = result["delta_h"];
            rho_out_i = result["rho_out"];
            p_out_i   = result["P_out"];
            v_in_i = mdot_i / (a_in * rho_in);
            v_out_i = mdot_i / (a_out * rho_out_i);
            w_sh_i = (delta_h_i + 0.5*v_out_i.square -
                    0.5*v_in_i.square) * mdot_i;
            T_load_i = w_sh_i / (speed *
                    turbomachine_model.get_speed_nominal());
            T_load_vs_speed[i] = T_load_i;
            p_out_vs_speed[i]  = p_out_i;
        }

        // Display for plotting (to eyeball the gradients)
        import std.stdio;
        writeln("\nOp point conditions:");
        writeln("p_in  = ", p_in);
        writeln("p_out = ", p_out);
        writeln("mdot  = ", mdot);
        writeln("T_in  = ", T_in);

        writeln("\nResults:");
        writeln("mdot_arr        : ", mdot_arr);
        writeln("speed_arr       : ", speed_arr);
        writeln("T_load_vs_mdot  : ", T_load_vs_mdot);
        writeln("p_out_vs_mdot   : ", p_out_vs_mdot);
        writeln("T_load_vs_speed : ", T_load_vs_speed);
        writeln("p_out_vs_speed  : ", p_out_vs_speed);
        writeln();
    }

    /*
     * Logarithmically vary H and display the resulting values of
     * dp_out_dmdot, dT_load_dmdot, dp_out_dspeed, dT_load_dspeed.
     */
    void make_pder_plots(Real[string] result_0)
    {
        import std.conv: to;
        import q1dcfd.utils.math: square;

        enum H_arr = [
            2.30258512e-08, 2.32634201e-04, 4.65299485e-04, 6.98018888e-04,
            9.30792425e-04, 1.16362011e-03, 1.39650195e-03, 1.62943796e-03,
            1.86242816e-03, 2.09547255e-03, 2.32857115e-03, 2.56172397e-03,
            2.79493103e-03, 3.02819233e-03, 3.26150789e-03, 3.49487772e-03,
            3.72830184e-03, 3.96178026e-03, 4.19531298e-03, 4.42890003e-03,
            4.66254141e-03, 4.89623714e-03, 5.12998723e-03, 5.36379169e-03,
            5.59765054e-03, 5.83156378e-03, 6.06553144e-03, 6.29955352e-03,
            6.53363004e-03, 6.76776100e-03, 7.00194643e-03, 7.23618633e-03,
            7.47048072e-03, 7.70482960e-03, 7.93923300e-03, 8.17369093e-03,
            8.40820339e-03, 8.64277040e-03, 8.87739198e-03, 9.11206813e-03,
            9.34679887e-03, 9.58158421e-03, 9.81642416e-03, 1.00513187e-02,
            1.02862680e-02, 1.05212718e-02, 1.07563304e-02, 1.09914436e-02,
            1.12266115e-02, 1.14618341e-02, 1.16971114e-02, 1.19324435e-02,
            1.21678302e-02, 1.24032718e-02, 1.26387681e-02, 1.28743192e-02,
            1.31099251e-02, 1.33455858e-02, 1.35813013e-02, 1.38170716e-02,
            1.40528968e-02, 1.42887768e-02, 1.45247117e-02, 1.47607015e-02,
            1.49967461e-02, 1.52328457e-02, 1.54690002e-02, 1.57052097e-02,
            1.59414740e-02, 1.61777934e-02, 1.64141677e-02, 1.66505970e-02,
            1.68870813e-02, 1.71236205e-02, 1.73602149e-02, 1.75968642e-02,
            1.78335686e-02, 1.80703281e-02, 1.83071426e-02, 1.85440122e-02,
            1.87809369e-02, 1.90179168e-02, 1.92549517e-02, 1.94920418e-02,
            1.97291870e-02, 1.99663874e-02, 2.02036430e-02, 2.04409538e-02,
            2.06783197e-02, 2.09157409e-02, 2.11532173e-02, 2.13907490e-02,
            2.16283359e-02, 2.18659780e-02, 2.21036755e-02, 2.23414282e-02,
            2.25792362e-02, 2.28170996e-02, 2.30550183e-02, 2.32929923e-02];

        // Make speed and mdot arrays
        auto n_points = H_arr.length;
        // Outputs
        auto dp_out_dmdot_arr   = new double[](n_points);
        auto dT_load_dmdot_arr  = new double[](n_points);
        auto dp_out_dspeed_arr  = new double[](n_points);
        auto dT_load_dspeed_arr = new double[](n_points);

        double[string] result;
        foreach(i; 0..n_points)
        {
            auto pders_mdot  = compute_turbomachine_partials(result_0, "mdot",
                    mdot, H_arr[i]);
            auto pders_speed = compute_turbomachine_partials(result_0, "speed",
                    speed, H_arr[i]);
            dp_out_dmdot_arr[i]   = pders_mdot["P_out"];
            dT_load_dmdot_arr[i]  = pders_mdot["T_load"];
            dp_out_dspeed_arr[i]  = pders_speed["P_out"];
            dT_load_dspeed_arr[i] = pders_speed["T_load"];
        }

        // Display for plotting
        import std.stdio;
        writeln("\nOp point conditions:");
        writeln("p_in  = ", p_in);
        writeln("p_out = ", p_out);
        writeln("mdot  = ", mdot);
        writeln("T_in  = ", T_in);

        writeln("\nResults:");
        writeln("H_arr : ", H_arr);
        writeln("dp_out_dmdot_arr   = ", dp_out_dmdot_arr);
        writeln("dT_load_dmdot_arr  = ", dT_load_dmdot_arr);
        writeln("dp_out_dspeed_arr  = ", dp_out_dspeed_arr);
        writeln("dT_load_dspeed_arr = ", dT_load_dspeed_arr);
        writeln();

    }
}

/* Open-cycle turbine */
class TurbineObserver : TurbomachineObserver
{
    // Open cycle turbine has P_high = P_in as state var, so find partial
    // derivatives of P_in and derive with respect to P_out.
    enum pders = ["dT_load_dmdot", "dT_load_dspeed", "dT_load_dp_in",
               "dp_in_dmdot", "dp_in_dspeed", "dp_in_dT_in", "de_out_dmdot",
               "de_out_dspeed", "dWdot_sh_dT_in", "dWdot_sh_dp_in",
               "dWdot_sh_dmdot", "dT_in_de_in", "dT_in_dp_in"];
    mixin(InitPropValues!(pders));
    // To handle varying outlet pressure [for closed cycle etc], need to
    // implement function to compute these partial derivatives
    // enum pders = ["dT_load_dp_out", "dp_in_dp_out", "de_out_dp_out"];

    this(ObsDef a_def, CrossSection[string] geo_dict)
    {
        super(a_def, geo_dict);

        import std.conv: to;
        auto def = a_def.to!TurbomachineObsDef;
        initialise_turbomachine_model(def);
    }

    void initialise_turbomachine_model(ObsDef def)
    {
        import q1dcfd.lconfig: ROOT_DIRECTORY;
        import q1dcfd.maps: Turbine;
        import q1dcfd.state_space_control: TurbineObsDef;
        import std.conv: to;

        auto t_def = def.to!TurbineObsDef;
        string map_dir = "/source/maps/" ~ t_def.map_name;
        turbomachine_model = new Turbine(ROOT_DIRECTORY ~ map_dir,
                t_def.coolprop_fluid, t_def.er_design, t_def.mfp_design,
                t_def.T0_design, t_def.P0_design);
        _speed_nominal = turbomachine_model.get_speed_nominal();
    }

    override void compute_partial_derivatives()
    {
        // These results should already stored in this class, but currently
        // recomputing for safety [could add debug mode check or use stored vals
        // otherwise]
        Real[string] result_0 = turbomachine_model.calc_downstream(
                mdot, speed, p_in, T_in);

        // Open cycle turbine, so just need pders wrt mdot, and speed
        auto pders_mdot  = compute_turbomachine_partials(result_0, "mdot", mdot);
        auto pders_speed = compute_turbomachine_partials(result_0, "speed", speed);
        auto pders_T_in  = compute_turbomachine_partials(result_0, "T_in", T_in);
        auto pders_p_in  = compute_turbomachine_partials(result_0, "p_in", p_in);
        _dT_load_dmdot  = pders_mdot["T_load"];
        _de_out_dmdot   = pders_mdot["e_out"];
        _dT_load_dspeed = pders_speed["T_load"];
        _de_out_dspeed  = pders_speed["e_out"];
        _dT_load_dp_in  = pders_p_in["T_load"];
        double dT_load_dT_in = pders_T_in["T_load"]; // temp

        // Partial derivatives of shaft power.
        // For turbine,
        //   Wdot = T_load * N_t * N_t0 
        // where N_t is normalized shaft speed (state var) and N_t0 is nominal.
        // As turbine is synchronous,
        //   d/dx Wdot = N_t * N_t0 * d/dx T_load.
        _dWdot_sh_dT_in = speed_nominal * speed * dT_load_dT_in;
        _dWdot_sh_dmdot = speed_nominal * speed * _dT_load_dmdot;
        _dWdot_sh_dp_in = speed_nominal * speed * _dT_load_dp_in;

        // For turbine, we want the derivatives of P_high = P_in, but only have
        // 'calc_downstream' function, so get these from PR
        auto dPR_dmdot  = pders_mdot["PR"];
        auto dPR_dspeed = pders_speed["PR"];
        auto dPR_dT_in  = pders_T_in["PR"];
        // For turbine, P_in = P_out * PR, where P_out = const.
        // Thus, can compute dP_in_dx = P_out * dPR_dx [slight error in this
        // process since changes in inlet condition scaling are not accounted
        // for, though these effects should be small].
        _dp_in_dmdot  = p_out * dPR_dmdot;
        _dp_in_dspeed = p_out * dPR_dspeed;
        _dp_in_dT_in  = p_out * dPR_dT_in;

        // Fluid partial derivatives
        auto H = 1E-8;
        eos.update_eos(InputPairs.p_e, p_in, (1+H)*e_in, get_fluid_state_in());
        auto T_in_plus = eos.T;
        _dT_in_de_in = (T_in_plus - T_in) / (H * e_in);
        eos.update_eos(InputPairs.p_e, (1+H)*p_in, e_in, get_fluid_state_in());
        T_in_plus = eos.T;
        _dT_in_dp_in = (T_in_plus - T_in) / (H * p_in);
    }

    //--------------------------------------------------------------------------
    // A quick and dirty observer for turbine mass flow; TODO
    //
    // Approach:
    //  + Set dmdot_t_dt.
    //  + Each step, update mdot_t according to output error.
    //  + First check will just be that without an integrating disturbance, it
    //    behaves as before.
    //  + How to compute mdot_t derivative?
    //     At the end of the MPC update, compute the derivative of mdot_t
    //     from MPC outputs.

    // Real mdot_measured;
    // Real dmdot_t_dt = 0.0;
    // Real L = 0.9;
    // Real kd = 0.05;
    // Real d = 0.0;

    // override void update_state_estimate(
            // const Real t,
            // const Real dt,
            // const InputPort[] input_ports)
    // {
        // update_measurements(input_ports);
        // update_derived_props();

        // Real y_tilde = mdot_measured - (mdot + d);
        // _mdot = mdot + (dmdot_t_dt * dt) + (L * y_tilde);
        // d = d + (kd * y_tilde);

        // Real d_max = 0.1;
        // if     (d > d_max) d  = d_max;
        // else if(d < -d_max) d = -d_max;
    // }

    // protected override void update_measurements(const InputPort[] input_ports)
    // {
        // mdot_measured = input_ports[sensors.mdot]();
        // _T_load = input_ports[sensors.T_load]();
        // _speed  = input_ports[sensors.speed]();
        // _p_in   = input_ports[sensors.p_in]();
        // _T_in   = input_ports[sensors.T_in]();
        // _p_out  = input_ports[sensors.p_out]();
        // _T_out  = input_ports[sensors.T_out]();
        // _eta    = input_ports[sensors.eta]();
        // _w_sh   = T_load * (speed * speed_nominal);
        // if(!synchronous_flag) {
            // _T_motor     = input_ports[sensors.T_motor]();
            // _dT_motor_dt = input_ports[sensors.dT_motor_dt]();
        // }
    // }

    // override void initialise_state_estimate(const InputPort[] input_ports)
    // {
        // _mdot   = input_ports[sensors.mdot]();
        // update_measurements(input_ports);
        // update_derived_props();
    // }

    // override void write_to_lti_model(ref Lti lti)
    // {
        // super.write_to_lti_model(lti);
        // lti.Zk[z_global_index("mdot"), 0] += d;
    // }
}

class CflUpdate(T) : T
{
    Real t_prev = 0.0;

    this(ObsDef def, CrossSection[string] geometries) { super(def, geometries); }

    /*
     * Observers may update asynchronously, and they use timescale-separated
     * model, so required update rate is lower than main compressible code.
     */
    override void update(const Real t, const InputPort[] input_ports)
    {
        Real dt = t - t_prev;
        // This condition means we will violate the CFL condition slightly, but
        // since max_cfl is conservative, this shouldn't cause problems
        if(dt >= dt_stable)
        {
            update_state_estimate(t, dt, input_ports);
            t_prev = t;
        }
    }
}

class SimpleUpdate(T) : T
{
    Real t_prev = 0.0;

    this(ObsDef def, CrossSection[string] geometries) { super(def, geometries); }

    override void update(const Real t, const InputPort[] input_ports)
    {
        Real dt = t - t_prev;
        update_state_estimate(t, dt, input_ports);
        t_prev = t;
    }
}

/*
 * Some comments I don't feel like deleting yet:
 *
 * There is just one set of boundary inputs (w) but both measured and
 * estimated outputs (y and y_hat)
 * w: h_in, p_in, mdot_in
 * y: e_out
 *
 * For a heat exchanger model it may be desireable at some point to correct
 * state estimates via matrix operations.
 * Forming the measured output vector in this case would be fairly routine
 * and would be need to be hard-coded.
 * However, it kind of makes sense to do the correction step in a matrix since
 * we need to form the state matrix anyway.
 */




import q1dcfd.utils.metaprogramming: from;
import q1dcfd.types;

/**
 * Subsonic outflow boundary condition Base class
 * Uses the navier stokes consistent boundary condition method
 *
 * Specific outflow boundary condition inherits from this base class
 */

abstract class NSSubsonicOutflow: from!"q1dcfd.experimental.node".SubsonicOutflow
{
    import q1dcfd.types;

    protected
    {
        Real _rho, _e, _v, _p, _h, _T, _M, _a, _k, _F0r,
            _F1r, _F2r, _qcond, _dpdx, _drhodx, _dvdx, _p_inf;

        // scale factor for rate of accoustic wave reflection
        @unique double _refl_coeff = 1.0;

        // boundary condition function
        @required @unique
        Transient _target_function;
    }

    override @recordableProperty
    {
        Real dt_density() {return Simulation.get_balance(address.index_l);}
        Real dt_momentum() {return Simulation.get_balance(address.index_l + 1);}
        Real dt_energy() {return Simulation.get_balance(address.index_l + 2);}
        Real dvdt() {return (dt_momentum - _v*dt_density)/_rho;}
        Real dedt() {return (dt_energy - dt_density*E)/_rho - _v*dvdt;}
        Real ke() {return 0.5*_v*_v;}
        Real x() { return eos.x;}
        Real s() {return eos.s;}
        Real cp() {return eos.cp;}
        Real dv() {return eos.dv;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt;}
        Real dpdt() {return dt_density*(_h - _e) - _rho*dedt;}
        Real F0l() { return _F0l;}
        Real F1l() { return _F1l;}
        Real F2l() { return _F2l;}
        Real mdot() {return _F0l;} // assumes mass flow equal to interface dmdt
    }

    override void update(const ref Real[] state, const Real t)
    {
        _p_inf = _target_function(t);

        super.update(state, t);
    }
}

/**
 * NSOutflowTargetMass
 * 
 * An outflow boundary condition which adjusts a far field pressure
 * in order to achieve a target mass flow rate
 *
 * Has a configuration space dimension of 4, state is [rho, rho v, rho E, p_inf]
 */
final class NSOutflowTargetMass: NSSubsonicOutflow
{
    import q1dcfd.types;
    import q1dcfd.code_generation.generate_node_body: GenerateBody;

    // Boilerplate generator
    mixin GenerateBody!(typeof(this));

    private @unique
    {
        double _mass_coeff = 1.0E6;
        double _max_dpdt = 1.0E6;
        double mdot_target = 1.0;
    }

    // four independent ODE's
    static uint order() {return 4;}

    /**
     * update method uses the generic conservative variable update
     * scheme, but also pulls p_inf from the state space vector
     * and updates the target mass flow rate
     */
    override void update(const ref Real[] state, const Real t)
    {
        super.update(state, t);
        _p_inf = state[address.index_r - 1];
        _mdot_target = _target_function(t);
    }

    /**
     * update balance method uses the navier stokes boundary condition
     * method, and sets the rate of change of far field pressure as
     * proportionally to the deviation from target mass flow rate
     * or as an upper/lower magnitude if this rate is too high
     */
    override void update_balance(ref Real[] balance)
    {
        import std.algorithm.comparison: max, min;

        super.update_balance(balance);

        // calculate an objective pressure rate of change and truncate
        // to within acceptable limits if necessary
        auto dpdt_uncorrected =  _mass_coeff*(mdot - _mdot_target);
        if(dpdt_uncorrected > 0)
        {
            balance[address.index_l + 3] = min(_max_dpdt, dpdt_uncorrected);
        }
        else
        {
            balance[address.index_l + 3] = max(-_max_dpdt, dpdt_uncorrected);
        }
    }
}

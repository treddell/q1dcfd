import q1dcfd.utils.metaprogramming: from;

/**
 * NSSubsonicInflow
 *
 * Base class for a boundary condition using the Navier Stokes Consistent Boundary Condition
 * Method
 */
abstract class NSSubsonicInflow: from!"q1dcfd.experimental.node".SubsonicInflow
{
    import q1dcfd.types;

    protected
    {
        Real _rho, _e, _v, _p, _h, _T, _M, _a, _k, _F0r,
            _F1r, _F2r, _qcond, _dvdx, _dpdx;
    }

    override @recordableProperty
    {
        Real rho() {return _rho;}
        Real vol() {return 1.0/_rho;}
        Real e() {return _e;}
        Real E() {return _e + ke;}
        Real ke() {return 0.5*_v*_v}
        Real p() {return _p;}
        Real h() {return _h}
        Real H() {return _h + ke;}
        Real T() {return _T;}
        Real v() {return _v;}
        Real a() {return _a;}
        Real M() {return _M;}
        Real dm() {return _dm;}
        Real k() {return _k;}
        Real dv() {return eos.dv;}
        Real cp() {return eos.cp}
        Real x() { return eos.x;}
        Real s() {return eos.s;}
        Real qcond() {return _qcond;}
        Real F0r() {return _F0r;}
        Real F1r() {return _F1r;}
        Real F2r() {return _F2r;}
        Real mdot() {return _F0r;} // assumes mass flow is equal to interface dmdt
        Real dt_density() {return _dt_density;}
        Real dt_momentum() {return _dt_density*_v + _rho*_dvdt;}
        Real dt_energy() {return Simulation.get_balance(address.index_l);}
        Real dvdt() {return _dvdt;}
        Real dedt() {return (_dt_energy - E*_dt_density)*_vol - _v*_dvdt;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt;}
        Real dpdt() {return dt_density*(h - e) - rho*dedt;}
    }

    double critical_dt() {return double.infinity;}
}

/**
 * NSSubsonicInflowDV
 * 
 * An inflow boundary condition with user defined density and velocity inflow
 * transients (and their time derivatives). Uses the Navier Stokes Consistent 
 * Boundary Condition Method to extrapolate characteristics leaving the 
 * computational domain, and constructs the balance equations from these.
 * As both density and velocity are assigned, Only the energy equation is 
 * unconstrained
 */

final class NSSubsonicInflowDV: NSSubsonicInflow
{
    import q1dcfd.types;
    import q1dcfd.code_generation.generate_node_body: GenerateBody;

    // Boilerplate generator
    mixin GenerateBody!(typeof(this)) 

    private @required @unique
    {
        Transient density_transient, density_rate_transient, velocity_transient,
            velocity_rate_transient;
    }

    /// Only has the energy balance equation
    static uint order() {return 1;}

    void update(const ref Real[] state, const Real t)
    {
        import std.math: fabs;

        _rho = density_transient(t);
        _dt_density = density_rate_transient(t);
        _v = velocity_transient(t);
        _dvdt = velocity_rate_transient(t);
        
        _vol = 1.0/rho;
        _e = state[index_l]*_vol - ke;
        _dm = _rho*_v;

        eos.update(_rho, _e);

        _h = eos.h;
        _T = eos.T;
        _k = eos.k;
        _a = eos.a;

        _M = fabs(_v)/_a;
        _p = (_h - _e)*_rho;
    }

    void update_balance(ref Real[] balance) 
    {
        import q1dcfd.utils.math: square;

        double L1, L2, L5, d1, d2, d5;

        // upstream travelling accoustic wave
        L1 = (_v - _a)*(_dpdx - _rho*_a*_dvdx);

        // downstream travelling accoustic wave
        L5 = L1 - 2.0*_rho*a*_dvdt;

        // downstream travelling convective wave
        L2 = - _a.square*_dt_density - 0.5*(L5 + L1);

        d1 = (L2 + 0.5*(L5 + L1))/_a.square;
        d5 = 0.5*(L5 + L1);
        d2 = (L5 - L1)/(2.0*_rho*_a);

        balance[address.index_l] = -H*d1 - _dm*d2 - 
            (_rho*_e + _p)/(_rho*_a.square)*d5;
    }
}

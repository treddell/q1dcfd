Automatic setter generation from DomainMetadata

class Obj: AbstractType
{
    protected
    {
        @required T _thing;

        @unique S _foo = default_val;

        @required @unique R _bar;
    }
}

Setters wilyl be generated for each of the above properties, DomainMetadata needs to automatically
call them

DomainMetadata(NodeType)
{
    mixin GenerateMetadataBody!(NodeType);

    ...
}


Should produce:

DomainMetadata(NodeType)
{
    private
    {
        T thing;
        S foo;
        R bar;

        bool thing_defined = false;
        bool foo_defined = false;
        bool bar_defined = false;
    }

    // type specific initialisation routines

    this(string[string] defs)
    {
        if(`thing` in defs)
        {
            thing = defs.retrieve(`thing`).required!T;
            thing_defined = true;
        }

        if(`foo` in defs)
        {
            foo = defs.retrieve(`foo`).required!S;
            foo_defined = true;
        }

        if(`bar` in defs)
        {
            bar = defs.retrieve(`bar`).required!R;
            bar_defined = true;
        }

        // use the initialisation routine for the particular type
        type_initialisation(defs);
    }

    void set_properties(NodeType node)
    {
        if(thing_defined) node.set_thing(T thing);
        if(foo_defined) node.set_foo(S foo);
        if(bar_defined) node.set_bar(R bar);
    }
}

........Automatically generated metadata constructor

DomainMetadata build_metadata(string[string] defs)
{
    auto model = defs.retrieve(`model`);

    switch(model)
    {
        case "Obj1":
        {
            return new Metadata!(Obj1)(defs);
        }
        case "Obj2":
        {
            return new Metadata!(Obj2)(defs);
        }
        default:
        {
            assert(false, ...);
        }       
    }
}

..........What to do about compound domains? Can just keep manual constructors

Need to add case to !required, which recognises MathFunctions and automatically casts

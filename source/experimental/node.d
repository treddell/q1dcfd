/// WIP conceptual model of nodal overhaul. May or may not be included in final release

import q1dcfd.types;

interface Node
{
    /// Nodal string identifier, for debugging purposes
    final string identify()
    {
        return "%s.%s, id:%u, address:%u,%u"
            .format(address.owner, address.type, address.id address.index_l);
    }

    /// Nodalstring verbose identifier. Contains equal or greater information
    /// than identify
    string describe();

    /// get the address information of the node
    ref Address address();

    /// Set the address of the node
    void register_address(Address address);

    /// number of state space variables used by the node
    static uint order();

    /// return the node fundamental type
    static node_type category();

    /**
     * time taken for dominant eigenvector to cross node. Used to construct
     * a maximum stable time step.
     *
     * If cell has no impact on stability then infinity can be used
     */
    double critical_dt();

    /**
     * Update the state of the node from its vector of configuration
     * state variables
     */
    void update(const ref Real[] state, const Real t);

    /**
     * Calculate the balance of the system
     */
    void update_balance(ref Real[] balance);

    /*
     * --------------------- Automatic method generation -----------------------
     */
    
    /**
     * Checks that the state following a full system update is well posed.
     * Is only executed when contracts are enabled
     * 
     * AUTOMATICALLY GENERATED
     */
    void check_well_posedness();

    /**
     * Checks that the state is fully initialised prior to commencing simulation
     * 
     * AUTOMATICALLY GENERATED
     */
    AssertionResult check_initialisation_conditions();

    /**
     * The properties to be recorded by default
     */
    string[] default_recorded_fields();
}

// Define a set of attributes for the possible element types

/// Describes the flow of fluid
bool is_fluid_like(node_type type)
{
    return (type == fluid_volume_interior || type == fluid_subsonic_inflow ||
            type == fluid_subsonic_outflow || type == fluid_interior_junction ||
            type == fluid_interior_mass_generation || type == fluid_thin_film ||
            type == fluid_point);
}

/// node has internal viscous shear
bool is_viscid(node_type type)
{
    return (type == fluid_volume_interior || type == fluid_thin_film ||
            type == fluid_interior_junction || type == fluid_interior_mass_generation);
}

/// Node describes a solid element
bool is_solid(node_type type)
{
    return (type == thermal_interior || type == thermal_boundary);
}

/// Node has dimensions
bool is_geometric(node_type type)
{
    return (type == fluid_volume_interior || type == fluid_interior_junction ||
            type == thermal_interior || type == fluid_interior_mass_generation);
}

/// Node is fluidlike and has changes in state densities
bool has_transport(node_type type)
{
    return (type.is_fluid_like && type != node_type.fluid_momentum_energy_discontinuity
            && type != node_type.fluid_thin_film && type != node_type.fluid_point);
}

bool has_heat_transfer(node_type type)
{
    return type.is_viscid || type == thermal_interior;
}

/**
 * Fluid like attributes, used for node class generation
 */
mixin template FluidLike
{
    // equation of state calculator
    protected EOSBase eos;

    abstract @recordableProperty
    {
        Real rho(); // density (kg/m3)
        Real vol(); // specific volume (m3/kg)
        Real e(); // specific internal energy (J/kg)
        Real E(); // specific total energy (J/kg)
        Real ke(); // specific kinetic energy (J/kg)
        Real p(); // static pressure (Pa)
        Real h(); // static enthalpy (J/kg)
        Real H(); // total enthalpy (J/kg)
        Real T(); // total temperature (K)
        Real v(); // inertial flow velocity (m/s)
        Real a(); // speed of sound (m/s)
        Real s(); // entropy (J/kg.K)
        Real M(); // Mach number (-)
        Real x(); // vapour fraction (-)
        Real dm(); // mass flux (kg/m2.s)
    }

    void set_eos(backend backend_model, TableData data)
    {
        this.eos = generate_eos(stream.conservative_data,
                                backend_model,
                                identify) ;
    }

    // Vector of 'conservation form' variables/ density variables
    final Real[3] cons_state()
    {
        Real[3] result = [rho, dm, rho*E];
        return result;
    }

    // Conditions to check after calling 'update' method in debug build
    final protected @updateInvariant
    {
        void valid_density()
        {
            assert(rho > 0 && !rho.isNaN,
                   "Invalid density %f in (%s)".format(rho, identify));
        }

        void valid_internal_energy()
        {
            assert(e > 0 && !e.isNaN,
                   "Invalid internal energy %f in (%s)".format(e, identify));
        }

        void valid_pressure()
        {
            assert(p > 0 && !p.isNaN,
                   "Invalid pressure %f in %s".format(p, identify));
        }

        void valid_enthalpy()
        {
            assert(h > 0 &&!h.isNaN,
                   "Invalid pressure %f in %s".format(h, identify));
        }

        void valid_temperature()
        {
            assert(T > 0 && !T.isNaN,
                   "Invalid temperature %f in %s".format(T, identify));
        }

        void valid_entropy()
        {
            assert(s > 0 && !s.isNaN,
                   "Invalid entropy %f in %s".format(s, identify));
        }

        void valid_speed_of_sound()
        {
            assert(a > 0 && !a.isNaN,
                   "Invalid speed of sound %f in %s".format(a, identify));
        }

        void valid_velocity()
        {
            assert(!v.isNaN,
                   "Invalid flow velocity %f in %s".format(v, identify));
        }
    }
}

/**
 * Solid like attributes. Used to generate the nodal class
 */
mixin template SolidLike
{
    abstract @recordableProperty Real T(); // Temperature (K)

    final protected @updateInvariant
    {
        void valid_temperature()
        {
            assert(T > 0 && !T.isNaN, "Invalid temperature %f in cell %s"
                   .format(identify, T));
        }
    }
}

/**
 * Attributes of a viscous fluid cell
 */
mixin template Viscid
{
    abstract @recordableProperty
    {
        Real cp(); // specific heat capacity (J/kg.K)
        Real dv(); // dynamic viscosity (kg/s.m)
        Real k(); // thermal conductivity (W/m.K)
        Real pr(); // Prandtl number  (-)
        Real re(); // Reynolds number (-)
        Real f(); // friction factor (-)
        Real shear(); // cell viscous shear (kg/m^2 s^2)
        Real p_loss(); // power loss due to shear (W)
        Real htc(); // heat transfer coefficient (W/mK)
        Real nu();  // nusselt number
    }

    final protected @updateInvariant
    {
        void valid_specific_heat()
        {
            assert(cp > 0 && !cp.isNaN,
                   "Invalid specific heat coefficient %f in %s at T=%f, p=%f"
                   .format(cp, identify, T, p));
        }
        
        void valid_viscosity()
        {
            assert(dv > 0 && !dv.isNaN,
                   "Invalid dynamic viscosity %f in %s at T=%f, p=%f"
                   .format(dv, identify, T, p));}
        
        void valid_conductivity()
        {
            assert(k > 0 && !k.isNaN,
                   "Invalid thermal conductivity %f in %s at T=%f, p=%f"
                   .format(k, identify, T, p));
        }
            
        void valid_prandtl()
        {
            assert(pr > 0 && !pr.isNaN,
                   "Invalid Prandtl Number %f in %s at T=%f, p=%f"
                   .format(pr, identify, T, p));
        }

        void valid_friction()
        {
            assert(f >= 0 && !f.isNaN,
                   "Invalid friction factor %f in %s at T=%f, p=%f"
                   .format(f, identify, T, p));
        }

        void valid_reynolds()
        {
            assert(re >= 0 && !re.isNaN,
                   "Invalid Reynolds number %f in %s at T=%f, p=%f"
                   .format(re, identify, T, p));
        }
            
        void valid_shear()
        {
            assert(shear <= 0 && !shear.isNaN,
                   "Invalid Shear %f in %s at T=%f, p=%f"
                   .format(shear, identify, T, p));
        }

        void valid_pressure_loss()
        {
            assert(p_loss <= 0 && !p_loss.isNaN,
                   "Invalid Power Loss %f in %s at T=%f, p=%f"
                   .format(p_loss, identify, T, p));
        }

        void valid_htc()
        {
            assert(htc >= 0 && !htc.isNaN,
                   "Invalid heat transfer coefficienct %f in %s at T=%f, p=%f"
                   .format(htc, identify, T, p));
        }
    }
}

/**
 * Attributes for a fluid like cell with finite volume which undergoes property transport
 */
mixin template TransportLike
{
    abstract @recordableProperty
    {
        Real F0r(); // right side mass flow rate (kg/s)
        Real F0l(); // left side mass flow rate (kg/s)
        Real F1r(); // right side force (kg.m/s2)
        Real F1l(); // left side force (kg.m/s2)
        Real F2r(); // right side energy flow (kg m2/s3)
        Real F2l(); // left side flow (kg m2/s3)
        Real mdot(); // cell average mass flow rate (kg/s)
        Real dt_density(); // density rate of change (kg/m3.s)
        Real dt_momentum(); // momentum density rate of change (kg /m2.s)
        Real dt_energy(); // total energy density rate of change (J/m3.s)
        Real dvdt(); // fluid acceleration (m/s2)
        Real dedt(); // specific internal energy rate of change (J/kg.s)
        Real dTdt(); // temperature rate of change (K/s)
        Real dpdt(); // rate of change of pressure (Pa/s)
    }
}

/**
 * Attributes for a cell which undergoes heat transfer
 */ 
mixin template HeatTransfer
{
    abstract @recordableProperty
    {
        Real qconv(); // heat transfer into volume via convection (W)
        Real qcond(); // heat transfer into volume via conduction (W)
    }

    final private @updateInvariant
    {
        void valid_heat_transfer()
        {
            assert(!qcond.isNan && !qconv.isNaN,
                   ("Invalid external heat transfer into cell %s,"
                    ~" convection: %f,"
                    ~" conduction %f").format(identify, qconv, qcond));
        }
    }
}

/**
 * Attributes for a solid cell with finite volume
 */
mixin template GeometricSolid
{
    import q1dcfd.types.geometry: SolidCellGeometry, WallCrossSection;

    protected SolidCellGeometry _geometry;

    void set_geometry(WallCrossSection cross_section,
                      const double x_left,
                      const double x_right)
    {
        _geometry = new SolidCellGeometry(cross_section, x_left, x_right);
    }
}

/**
 * Attributes for a fluid cell with finite volume
 */
mixin template GeometricFluid
{
    import q1dcfd.types.geometry: FluidCellGeometry, ChannelCrossSection;

    // cell fluid mass (kg)
    abstract @recordableProperty Real mass(); 

    protected FluidCellGeometry _geometry;

    void set_geometry(ChannelCrossSection cross_section,
                      const double x_left,
                      const double x_right)
    {
        _geometry = new FluidCellGeometry(cross_section, x_left, x_right);
    }
}

/**
 * Attributes for a fluid cell which generates its own mass
 */
mixin template MassGeneration
{
    abstract @recordableProperty Real mdot_external(); // external mass flow rate
}

/**
 * Abstract node type, used as a template for constructing each control volume
 * /cell or node
 */
abstract class AbstractNode(node_type type): Node
{
    import std.math: isNaN;
    import std.format;

    // private address object
    private Address _address;
    
    // retrieve information on the address of the node in the global state space,
    // write out state space, and the owning domain and type
    final ref Address address() {return _address;}

    /**
     * Set the address of the node
     */
    final void register_address(Address address) { this._address = address;}

    // Populates the saved property vector with the properties of the
    // node being saved
    protected void delegate(ref Real[] save_state) _write_to;

    // Implement node category
    final static node_type category() {return type;}

    // **************** Compose the class based upon the node type ****************

    static if(type.is_fluid_like)
    {
        // Generate shared attributes for all fluid like nodes
        mixin FluidLike;

        // Generate attributes for all fluid like cells with finite mass storage
        static if(type.is_geometric) mixin GeometricFluid;

        // Generate attributes for all fluid cells with viscous effects
        static if(type.is_viscid) mixin Viscid;

        // Generate attributes for all fluid cells which experience flux across their
        // boundaries
        static if(type.has_transport) mixin TransportLike;

        // Generate attributes for all fluid cells which have mass generation source terms
        static if(type == fluid_interior_mass_generation) mixin MassGeneration;
    }
    else static if(type.is_solid)
    {
        // Generate attributes shared by all solid nodes
        mixin SolidLike;

        // Generate attributes shared by all solid nodes that have finite volume
        static if(type.is_geometric) mixin GeometricSolid;
    }

    // Generate attributes for all cells which have undergo heat transfer 
    static if(type.has_heat_transfer) mixin HeatTransfer;
}

/// An interior fluid cell with finite volume
alias InteriorFluidCell = AbstractNode!node.type.fluid_volume_interior;

/// A subsonic inflow boundary condition, dimensionless
alias SubsonicInflow = AbstractNode!node_type.fluid_subsonic_inflow;

/// A subsonic outflow boundary condition, dimensionless
alias SubsonicOutflow = AbstractNode!node_type.fluid_subsonic_outflow;

/// A jump discontinuity with conserved mass flow
alias Discontinuity = AbstractNode!node_type.fluid_momentum_energy_discontinuity;

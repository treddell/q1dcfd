module q1dcfd.experimental.domain_metadata;

import q1dcfd.utils.from;

/**
 * Publically accesible members of DomainMetadata
 */
interface DomainMetadata
{
    import q1dcfd.experimental.node: Node;

    /**
     * Store the positions of the domain within the system node list, and link
     * to the simulation manager, actual work is performed by the derived classes,
     * which, generate each control volume and its internal interfaces
     */
    void discretise(ref Node[] nodes,
                    ref from!"q1dcfd.interfaces".Interface[] interfaces);

    /**
     * Initialises the state of every node described by the domain
     */
    void initialise_state(ref Real[] state, ref Node[] nodes);

    /** 
     * Get the global nodal address from the local nodal address
     */
    uint node_global_id(const int n);

    /**
     * Number of ordinary differential equations described by the domain
     */
    uint n_odes();

    /**
     * Number of nodes contained within the domain
     */
    uint n_nodes();

    /**
     * Number of properties to be written to disk
     */
    uint n_recorded_properties();

    /**
     * Sets the indices of the domains address in the configuration space,
     * node list, and list of recorded properties
     */
    void set_indices(const uint ode_index_l,
                     const uint node_index_l,
                     const uint write_index_l);

    /**
     * Writes out the metadata of the domain to file
     */
    void write_out_metadata(from!"std.stdio".File file);
}

final class DomainMetadata(NodeModel): DomainMetadata
{
    // Generate the setters for the particular node model, and the constructor for
    // the DomainMetadata object and the type specific fields
    mixin GenerateDomainMetadataBody!(typeof(NodeModel));

    private
    {
        uint node_index_l;
        uint ode_index_l;
        uint write_index_l;
        uint _n_nodes;

        NodeModel[] local_nodes;
    }

    void discretise(ref Node[] nodes,
                    ref from!"q1dcfd.interfaces".Interface[] interfaces)
    {
        // Construct each control volume and add to the simulation list
        static if (NodeModel.category.is_geometric)
        {
            auto dx = length/n_nodes;
        }

        node_coords.reserve(n_nodes);

        foreach(immutable i; 0..n_nodes)
        {
            // global node ID
            auto id = node_index_l + i;

            // Generate the node
            auto node = new NodeModel() ;

            // Assign the address of the node
            node.register_address(Address(ode_index_l + i*NodeModel.order,
                                          ode_index_l + (i + 1)*NodeModel.order,
                                          write_index_l + i*save.length,
                                          id,
                                          name));

            // run the setter routine of the particular node model
            static if (NodeModel.category.is_geometric)
            {
                set_properties(node, i*dx, (i + 1)*dx);
                node_coords[i] = dx*(2*i + 1)
            }
            else
            {
                set_properties(node, 0.0, 0.0);
                node_coords[i] = 0.0;
            }
            

            // add node to the global list
            nodes[id] = node;
            
            debug
            {
                import q1dcfd.utils.logging;
                "Constructed %s".format(node.describe).add_message_to_log_file ;
            }
        }

        // keep a local reference to all nodes within the domain
        local_nodes[] = nodes[node_index_l..node_index_l + n_nodes].to!(NodeModel[]);

        // construct interfaces for all interior connections
        initialise_interior_interfaces(interfaces);
    }

    uint node_global_id(const int n)
    {
        import std.math: abs ;
        import std.exception: enforce ;
        import std.format;

        import q1dcfd.utils.errors: InvalidDefinition ;
        
        auto m = n < 0 ? n_nodes.to!int + n : n ; // actual index

        // make sure indexing is not out of bounds
        enforce!InvalidDefinition(
            abs(m) < n_nodes,
            "Attempt to reference out of bounds node %u, domain %s contains only %u nodes"
            .format(n, name, n_nodes)) ;

        return node_index_l + m ;
    }

    uint n_odes()
    {
        return _n_nodes*NodeModel.order;
    }

    uint n_nodes()
    {
        return _n_nodes;
    }

    uint n_recorded_properties()
    {
        return n_nodes*save.length;
    }

    void set_indices(const uint ode_index_l,
                     const uint node_index_l,
                     const uint write_index_l)
    {
        this.ode_index_l = ode_index_l;
        this.node_index_l = node_index_l;
        this.write_index_l = write_index_l;
    }
}

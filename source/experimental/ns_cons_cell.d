import q1dcfd.utils.metaprogramming: from;

/**
 * Navier Stokes Cell using conservative variables
 *
 * Single phase, quasi 1 dimensional nodel model. Fluid properties are 
 * calculated dynamically from conservation of mass, momentum and energy.
 * Viscous losses are accounted for via bulk correlations.
 */
final class NSConsCell: from!"q1dcfd.experimental.node".InteriorFluidCell
{
    import q1dcfd.types;
    import q1dcfd.code_generation.generate_node_body: GenerateBody;

    // Boilerplate generator
    mixin GenerateBody!(typeof(this)) 

    // Listing of all the private attributes
    private
    {
        // number of identical channels along the flow section
        @unique uint _n_channels = 1;

        // cached properties
        Real _rho, _vol, _e, _v, _p, _h, _T, _M, _a, _cp, _dv, _k, _pr, _re, _F0r, _F0l,
            _F1r, _F1l, _F2r, _F2l, _qconv, _qcond, _shear, _p_loss;
    }

    @initCondition
    {
        // Check that the number of channels is not 0 or negative
        AssertionResult assert_channels_positive()
        {
            return AssertionResult(_n_channels > 0, "Cannot have non positive n_channels");
        }
    }

    /**
     * Three independent ODE's are the balance of mass, momentum and energy
     */
    static uint order() {return 3;}

    override @recordableProperty
    {
        Real rho() {return _rho;}
        Real vol() {return _vol;}
        Real e() {return _e;}
        Real E() {return _e + ke;}
        Real ke() {return 0.5*_v*_v}
        Real p() {return _p;}
        Real h() {return _h}
        Real H() {return _h + ke;}
        Real T() {return _T;}
        Real v() {return _v;}
        Real a() {return _a;}
        Real s() {return _s;}
        Real M() {return _M;}
        Real x() {return eos.x;}
        Real dm() {return _dm;}
        Real cp() {return _cp;}
        Real dv() {return _dv;}
        Real k() {return _k;}
        Real pr() {return _pr;}
        Real re() {return _re;}
        Real f() {return _f;}
        Real shear() {return _shear;}
        Real p_loss() {return _p_loss;}
        Real htc() {return _htc;}
        Real nu() {return _nu;}
        Real qconv() {return _qconv;}
        Real qcond() {return _qcond;}
        Real F0r() {return _F0r;}
        Real F0l() {return _F0l;}
        Real F1r() {return _F1r;}
        Real F1l() {return _F1l;}
        Real F2r() {return _F2r;}
        Real F2l() {return _F2l;}
        Real mdot() {return dm*geometry.cross_area;}
        Real dt_density() {return Simulation.get_balance(address.index_l);}
        Real dt_momentum() {return Simulation.get_balance(address.index_l + 1);}
        Real dt_energy() {return Simulation.get_balance(address.index_l + 2);}
        Real dvdt() {return (dt_momentum - v*dt_density)/rho;}
        Real dedt() {return (dt_energy - dt_density*E)/rho -v*dvdt;}
        Real dTdt() {return dt_density*eos.dTdD + eos.dTde*dedt;}
        Real dpdt() {return dt_density*(h - e) - rho*dedt;}
    }
    
    void update(const ref Real[] state, const Real t)
    {
        import std.math: fabs;

        // reset flux
        _qconv = 0.0;
        _qcond = 0.0;

        _rho = state[index_l];
        _vol = 1.0/_rho;
        _dm = state[index_l + 1];
        _v = _dm*vol;
        _e = state[index_l + 2]*vol - ke;

        // perform the backend state calculation
        eos.update(rho, e);

        // cache some calculated properties, and calculate extensive properties
        _h = eos.h;
        _T = eos.T;
        _cp = eos.cp;
        _dv = eos.dv;
        _k = eos.k;
        _a = eos.a;
        _p = (h - e)*rho;
        _pr = dv*cp/k;
        _re = rho*fabs(v)*geometry.hydr_d/dv;

        // TODO: move to convective interface
        // _f = f_corr.evaluate();
        // _shear = -sign(v)*f*rho*ke/geometry.hydr_d;
        // _p_loss = _shear*fabs(v);

        debug
        {
            import q1dcfd.utils.logging: add_message_to_log_buffer;
            import std.format;
            
            ("Updated %s with rho = %f, dm = %f, v = %f, e = %f, h = %f, T = %f,"
             ~" cp = %f, dv = %f, k = %f, a = %f, p = %f, pr = %f, re = %f, f = %f,"
             ~" shear = %f, p_loss = %f")
                .format(identify, rho, dm, v, e, h, T, cp, dv, k, a, p, pr, re,
                        f, shear, p_loss)
                .add_message_to_log_buffer;
        }
    }

    /**
     * Evaluate the Transport equations
     */
    void update_balance(ref Real[] balance)
    {
        // conservation of mass
        balance[address.index_l] = -(_F0r - _F0l)*geometry.specific_density;

        // momentum balance
        balance[address.index_l + 1] = -(_F1r - _F1l)*geometry.specific_density + _shear;

        // energy balance
        balance[address.index_l + 2] = -(_F2r - _F2l + _qconv + _qcond)*geometry.specific_density
            + _p_loss;
    }

    /**
     * Critical delta time is the time taken for fastest relative accoustic
     * wave to cross the computational domain
     */
    double critical_dt()
    {
        import std.math: fabs;
        return geometry.length/(fabs(v) + a);
    }

    override string describe()
    {
        return (identify ~ " channels: %u, left_area: %f, right_area: %f volume: %f,"
                ~" hydraulic_diameter: %f, heat_area: %f, surface_roughness: %f, length: %f")
            .format(n_channels, geometry.lA, geometry.rA, geometry.volume, geometry.hydr_d,
                    geometry.htA, geometry.delta, geometry.length);
    }
}

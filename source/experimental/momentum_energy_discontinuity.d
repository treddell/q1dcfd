import q1dcfd.utils.metaprogramming: from ;

/**
 * A discontinuity in momentum and energy modelling a property change
 * across the length of a turbomachine
 */
final class Discontinuity(UpstreamType, DownstreamType): from!"q1dcfd.experimental.node".Discontinuity
{
    import q1dcfd.config: Real, Transient ;
    import q1dcfd.correlations.maps: EfficiencyMap ;
    import q1dcfd.eos: EOSBase ;

    static uint order() {return 0;}

    private
    {
        // Shaft speed of the turbomachine
        Transient shaft_speed ;

        // isentropic efficiency across the discontinuity
        EfficiencyMap calculate_efficiency ;

        // pressure-entropy equation of state
        EOSBase isentropic_eos ;

        Real[2] _cons_vars ;

        // Ratio of upstream to downstream area
        Real _area_ratio ;

        // upstream and downstream sub-states
        UpstreamType upstream ; 
        DownstreamType downstream ;
    }

    public
    {
        // density, velocity and rates extrapolated over the discontinuity
        // These are temporary variables which are cast to the downstream state
        // when needed
        Real rho_extrapolated ;
        Real v_extrapolated ;
        Real drhodt_extrapolated ;
        Real dvdt_extrapolated ;
    }

    @property
    {
        // static specific enthalpy difference across the discontinuity (J/kg)
        Real delta_h() {return upstream.h - downstream.h ;}

        // total specific enthalpy difference across the discontinuity (J/kg)
        Real delta_H() {return upstream.H - downstream.H ;}

        // conserved mass flow rate across the discontinuity (kg/s)
        Real mdot() {return upstream.mdot ;}

        // net power change across the discontinuity (W)
        Real power() {return delta_H*mdot ;}

        Real eta() {return _eta ;}
    }    

    this(const Address address,
         from!"q1dcfd.stream".Stream stream,
         EfficiencyMap calculate_efficiency,
         Transient shaft_speed,
         from!"q1dcfd.eos".backend backend_model)
    {
        super(address) ;

        this.eos = generate_eos(stream.conservative_data,
                                backend_model,
                                identify) ;

        this.isentropic_eos = generate_eos(stream.isentropic_data,
                                           backend_model,
                                           identify) ;

        this.calculate_efficiency = calculate_efficiency ;
        this.shaft_speed = shaft_speed ;
    }

    override void update(const ref Real[] state, const Real t)
    out
    {
        check_well_posedness ;
    }
    do
    {
        import q1dcfd.utils.convert_fluid_props: cons_from_pressure_step ;
        import q1dcfd.simulation: Simulation ;

        // calculate the isentropic efficiency across the discontinuity
        _eta = calculate_efficiency(downstream, upstream, shaft_speed(t)) ;

        // calculate the downstream conditions and store them as temporary
        // extrapolated variable
        cons_from_pressure_step(conservative_eos, isentropic_eos, _cons_vars,
                                upstream.dm, upstream.h, upstream.s,
                                state[downstream.index_l], eta) ;

        rho_extrapolated = _cons_vars[0] ;
        v_extrapolated = upstream.rho*upstream.v*_area_ratio/cons_vars[0] ;

        // TODO: Bug, Simulation.dt is not deterministic, uses temporary dt, may be
        // inf
        drhodt_extrapolated = (rho_extrapolated - downstream.rho)/Simulation.dt ;

        dvdt_extrapolated = (v_extrapolated - downstream.v)/Simulation.dt ;
    }

    // Does not contribute ODE's to the state space
    override void update_balance(ref Real[] balance) {}

    // No impact upon stability
    double critical_dt() {return double.infinity ;}

    // WIP
    void link_to_downstream(DownstreamType down)
    {
        this.downstream = down ;

        // down.set_inflow_density((Real t) => {rho_extrapolated ;}) ;
    }
}

"""
Initialize the auto suggester and populate with path information
"""

import os
from prompt_toolkit.completion import WordCompleter
from q1dcfd_auto_versioner import scripts_directory, results_directory

def initialize_autocompletion():
    "Prepopulates the autocompletion prompt with a set of suggestions"

    autocompletion = WordCompleter([],  WORD = True)

    scripts = os.listdir(scripts_directory)
    scripts[:] = (f for f in scripts if f.endswith(".qdef"))

    result_folders = os.listdir(results_directory)

    for qdef in scripts: 
        autocompletion.words.append("".join(["run ", qdef]))

    for result in result_folders:
        autocompletion.words.append(''.join(['load ', result]))

    # add in table generation command form
    autocompletion.words.append("tablegen <tableName> <fluid> p1:p2 T1:T2 --args")

    # add in design command formats
    autocompletion.words.append(("design simple-brayton -fluid=\n"
                                 " -turbine_inlet_pressure=\n"
                                 " -turbine_inlet_temperature=\n"
                                 " -turbine_outlet_pressure=\n"
                                 " -compressor_inlet_temperature=\n"
                                 " -turbine_efficiency=1.0\n"
                                 " -compressor_efficiency=1.0\n"
                                 " -high_pressure_loss=0.0\n"
                                 " -low_pressure_loss=0.0\n"))

    autocompletion.words.append(("design brayton -fluid=\n"
                                 " -turbine_inlet_pressure=\n"
                                 " -turbine_inlet_temperature=\n"
                                 " -turbine_outlet_pressure=\n"
                                 " -compressor_inlet_temperature=\n"
                                 " -turbine_efficiency=1.0\n"
                                 " -delta_temperature=\n"
                                 " -compressor_efficiency=1.0\n"
                                 " -high_pressure_recuperator_loss=0.0\n"
                                 " -low_pressure_recuperator_loss=0.0\n"
                                 " -turbine_inlet_loss=0.0\n"
                                 " -turbine_outlet_loss=0.0\n"
                                 " -compressor_inlet_loss=0.0\n"
                                 " -compressor_outlet_loss=0.0\n"))

    autocompletion.words.append(("design optimise-brayton -fluid=\n"
                                 " -turbine_inlet_pressure=\n"
                                 " -turbine_inlet_temperature=\n"
                                 " -compressor_inlet_temperature=\n"
                                 " -turbine_efficiency=1.0\n"
                                 " -delta_temperature=\n"
                                 " -compressor_efficiency=1.0\n"
                                 " -min_pressure=\n"
                                 " -high_pressure_recuperator_loss=0.0\n"
                                 " -low_pressure_recuperator_loss=0.0\n"
                                 " -turbine_inlet_loss=0.0\n"
                                 " -turbine_outlet_loss=0.0\n"
                                 " -compressor_inlet_loss=0.0\n"
                                 " -compressor_outlet_loss=0.0\n"))

    return autocompletion

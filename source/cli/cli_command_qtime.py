"""
qtime command for command line interface REPL
"""

import sys, traceback
import matplotlib.pyplot as plt
from cli_utils import clean_input, split_fields_arguments
from plot_tools import gauges, dimensionless_gauge
from matplotlib_mappings import DEFAULT_COLOURS

def command_qtime(inp, workspace):
    "Produces a plot of results vs time"
    time, err_string = _get_time_array(workspace)
    if err_string: print(err_string) ; return

    # process the input
    parse_input = clean_input(inp.split(" "))
    if err_string: print(err_string) ; return

    attr, err_string = _retrieve_attr(parse_input)
    if err_string: print(err_string) ; return

    default_args = {"xlabel": "time (s)"}
    fields, args, err_string = split_fields_arguments(parse_input[2:], default_args)
    if err_string: print(err_string) ; return

    domains, err_string = _retreve_domains(fields, attr, workspace)
    if err_string: print(err_string) ; return

    # no spatial indices provided, choose first and last points
    # or check if the property being plotted is 0 dimensional
    space_slices, err_string = _parse_spatial_cuts(args)

    _produce_qtime_plot(domains, attr, time, space_slices, args)

def command_qtsum(inp, workspace):
    """
    Produces a single line plot of results vs time, where results are summed
    over all requested domains.
    """
    time, err_string = _get_time_array(workspace)
    if err_string: print(err_string) ; return

    # process the input
    parse_input = clean_input(inp.split(" "))
    if err_string: print(err_string) ; return

    attr, err_string = _retrieve_attr(parse_input)
    if err_string: print(err_string) ; return

    default_args = {"xlabel": "time (s)"}
    fields, args, err_string = split_fields_arguments(parse_input[2:], default_args)
    if err_string: print(err_string) ; return

    domains, err_string = _retreve_domains(fields, attr, workspace)
    if err_string: print(err_string) ; return

    # no spatial indices provided, choose first and last points
    # or check if the property being plotted is 0 dimensional
    space_slices, err_string = _parse_spatial_cuts(args)

    _produce_qtsum_plot(domains, attr, time, space_slices, args)

def command_qtqin(inp, workspace):
    """
    Produces a single line plot of deltaq over time for the requested domain.
    """
    time, err_string = _get_time_array(workspace)
    if err_string: print(err_string) ; return

    # process the input
    parse_input = clean_input(inp.split(" "))
    if err_string: print(err_string) ; return

    default_args = {"xlabel": "time (s)"}
    fields, args, err_string = split_fields_arguments(parse_input[1:], default_args)
    if err_string: print(err_string) ; return

    attrs = ['h', 'mdot']
    domains, err_string = _retrieve_domains_multi_attrs(fields, attrs, workspace)
    if err_string: print(err_string); return

    _produce_qtqin_plot(domains, time, args)

def _retrieve_domains_multi_attrs(fields, attrs, workspace):
    domains = []
    for field in fields:
        if not field in workspace:
            return [], (f"Was unable to retrieve domain: {field}, was not "
                        "found in workspace.")
        for attr in attrs:
            if not hasattr(workspace[field], attr):
                return [], (f"Unable to produce plot, domain: {field} does not "
                    f"have attribute {attr}.")
        domains.append(workspace[field])
    return domains, ""

def _retreve_domains(fields, attr, workspace):
    "Reads each field string and retrieves data from the workspace"

    domains = []

    for field in fields:

        # input is a domain
        if not field in workspace:
            return [], (f"Was unable to retrieve domain: {field}, was not "
                        "found in workspace")

        if not hasattr(workspace[field],attr):
            return [], (f"Unable to produce plot, domain: {field} does not "
                   f"have attribute: {attr}")

        domains.append(workspace[field])

    return domains, ""

def _get_time_array(workspace):
    "Checks that the workspace contains a time array, and if so extracts it"

    if "time" not in workspace:
        return [], (f"workspace does not appear to contain a 'time' array. "
            "Unable to produce plot")

    if len(workspace["time"]) < 2:
        return [], ("Need at least two points in time to plot a property(t) "
                    "vs t plot")

    return workspace["time"], ""

def _retrieve_attr(inputs):
    "retrieve the attribute to be plotted from the input command"

    if len(inputs) < 3:
        return "", ("Must provide attribute to plot as 'qtime <attribute> "
                    "<domain1>...'")

    return inputs[1], ""

def _parse_spatial_cuts(args):
    "Parse the provides spatial distributions or produce some default"

    if not "slices" in args:
        return  [0, -1], ""

    # spatial indices provided
    try:
        return [int(x) for x in args["slices"].split(",")], ""
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        return [] ,(f"Was unable to parse input {args['slices']} into a "
                    "list of integer indices. Failed with traceback: "
                    f" {exc_traceback}")

def _produce_qtime_plot(domains, attr, time, space_slices, args):
    "Generate the plot from the parsed arguments"

    # Produce the plot object
    colours = iter(DEFAULT_COLOURS)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)

    try:
        for domain in domains:
            colour = next(colours)
            if hasattr(domain, "node_coords"):
                gauges(ax, domain, attr, time, space_slices, colour)
            else:
                dimensionless_gauge(ax, domain, attr, time, colour)

        ax.legend()
        ax.set_xlabel(args.get("xlabel",'time (s)'))
        ax.set_ylabel(args.get("ylabel",attr))
        plt.show()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

def _produce_qtsum_plot(domains, attr, time, space_slices, args):
    "Generate the plot from the parsed arguments"
    import numpy as np

    colours = iter(DEFAULT_COLOURS)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)

    try:
        # Sum domain data
        colour = next(colours)
        data = np.zeros(getattr(domains[0], attr).shape)
        for domain in domains:
            new_data = getattr(domain, attr)
            assert(new_data.shape == data.shape)
            data += new_data

        ax.plot(time, data, color = colour)
        # ax.legend()
        ax.set_xlabel(args.get("xlabel",'time (s)'))
        ax.set_ylabel(args.get("ylabel",attr))
        plt.show()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

# load compressor_hx_turbine_mpc_testing
# qtqin hx[0]

def _produce_qtqin_plot(domains, time, args):
    colours = iter(DEFAULT_COLOURS)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(True)

    try:
        for domain in domains:
            colour = next(colours)
            # Compute deltaQ over all time (mdot_in * h_in - mdot_out * h_out)
            dq_dot =  (getattr(domain, 'h')[:, 0]  * getattr(domain, 'mdot')[:, 0]) \
                    - (getattr(domain, 'h')[:, -1] * getattr(domain, 'mdot')[:, -1])
            ax.plot(time, dq_dot, color = colour)

        # ax.legend()
        ax.set_xlabel(args.get("xlabel",'time (s)'))
        ax.set_ylabel(args.get("ylabel", 'q_in'))
        plt.show()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()


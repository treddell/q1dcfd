"""
design command for command line interface REPL
"""
import os, sys, traceback, subprocess
import numpy as np

from cli_utils import clean_input, process_arguments, multiple_replace
from q1dcfd_auto_versioner import app_exec

REQUIRED_ARGUMENTS =  {
    "brayton":{
        "fluid", "turbine_inlet_pressure", "turbine_inlet_temperature",
        "turbine_outlet_pressure","compressor_inlet_temperature",
        "delta_temperature"},

    "simple-brayton": {
        "fluid", "turbine_inlet_pressure", "turbine_inlet_temperature",
        "turbine_outlet_pressure","compressor_inlet_temperature"},

    "optimise-brayton": {
        "fluid", "turbine_inlet_pressure", "turbine_inlet_temperature",
        "min_pressure","compressor_inlet_temperature",
        "delta_temperature"}}

DEFAULT_ARGUMENTS = {
    "brayton": {
        "turbine_efficiency": "1.0",
        "compressor_efficiency": "1.0",
        "high_pressure_recuperator_loss": "0.0",
        "low_pressure_recuperator_loss": "0.0",
        "turbine_inlet_loss": "0.0",
        "turbine_outlet_loss": "0.0",
        "compressor_inlet_loss": "0.0",
        "compressor_outlet_loss": "0.0"},
    
    "simple-brayton": {
        "turbine_efficiency": "1.0",
        "compressor_efficiency": "1.0",
        "high_pressure_loss": "0.0",
        "low_pressure_loss": "0.0"},
    
    "optimise-brayton": {
        "turbine_efficiency": "1.0",
        "compressor_efficiency": "1.0",
        "high_pressure_recuperator_loss": "0.0",
        "low_pressure_recuperator_loss": "0.0",
        "turbine_inlet_loss": "0.0",
        "turbine_outlet_loss": "0.0",
        "compressor_inlet_loss": "0.0",
        "compressor_outlet_loss": "0.0"}}
    
def _run_cycle_design_command(args, mode):
    "Passes a command string over to the application"

    if not (REQUIRED_ARGUMENTS[mode] < args.keys()):
        print(f"expects at minimum arguments {REQUIRED_ARGUMENTS}, but found {args.keys()}")
        return

    # give default arguments
    for key,val in DEFAULT_ARGUMENTS[mode].items():
        if key not in args.keys():
            args[key] = val

    # check for the existence of None valued arguments, as these may result in a
    # segfault
    for key,val in args.items():
        if not val:
            print("None arguments found: {0}"
                  .format([key for key,val in args.items() if not val]))
            return
            
    # if the arguments are good, generate a command to send to the executable
    args_expression = " ".join([*args.keys(), *args.values()])
    cmmnd = f"{app_exec} cycle-design {mode} {args_expression}"
    
    try:
        exit_code = subprocess.call(cmmnd, shell = True)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

def command_design(inp):
    """
    Parses input and runs a quick cycle 
    Expects to receive an input of the form:

    design <type> <-fields=values>

    Allowed types:

    brayton, simple-brayton, optimise-brayton
    """
    
    parse_input = clean_input(multiple_replace(inp, blemishes={"\n"})
                              .split(' '))

    if(len(parse_input) == 1):
        print("Cycle type and arguments not specified")
        return

    cycle_type = parse_input[1]
    args, err_string = process_arguments(parse_input[2:])

    if(cycle_type not in DEFAULT_ARGUMENTS.keys()):
        print(f"{cycle_type} is not a recognised cycle. Options are {run_cycle_design.keys()}")
        return

    if err_string:
        print(err_string)
        return

    # generate the command string and pass to the executable
    _run_cycle_design_command(args, cycle_type)

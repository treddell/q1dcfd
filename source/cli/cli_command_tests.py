"""
tests command for q1dcfd command line interface

Runs the test suite
"""

import sys, traceback, subprocess, os, shutil, matplotlib.pyplot as plt

from cli_command_build import build
from cli_utils import clean_input, print_centred
from q1dcfd_auto_versioner import tables_directory, app_exec
from cli_command_tablegen import command_tablegen

def command_tests(inp, workspace):
    """
    * Parses input and produces a command to send to bash to compile and runs
      library tests

    * Generates a test data set

    * Runs the unittests

    * Runs the integration tests

    * Runs repl and post processing tests TODO
    """

    config_specified = False
    flags = clean_input(inp.split(' '))[1:]

    # Check if a configuration is specified, else use the default
    for i, flag in enumerate(flags):
        if flag.startswith("--config=serial"):
            flags[i] = "--config=tests-serial"
            config_specified = True
            break

    if not config_specified:
        flags.append("--config=tests")

    flags.append("--build=unittest")

    # compile the test suite
    print("\nCompiling library unit and integration tests...")
    exit_code = build("build", flags)

    if exit_code == False:
        return

    print("Generating test tabular test data set...")
    plt.ion()
    command_tablegen((f"tablegen TEST CO2 8.0E6:25E6 273:900 --N1=50 --N2=50"))
    plt.close()

    # execute the test suite
    print("\nRunning unit and integration tests...")
    try:
        exit_code = subprocess.call(app_exec, shell = True)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

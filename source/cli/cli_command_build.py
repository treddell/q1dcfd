"""
build command for q1dcfd command line interface
"""

import sys, traceback, subprocess, os

from cli_utils import clean_input
from q1dcfd_auto_versioner import root_directory

def command_build(inp, autocompletion):
    " Parse the input command and delegate to dub"

    flags = clean_input(inp.split(' '))[1:]

    config_specified = False
    build_type_specified = False

    for flag in flags:
        if flag.startswith("--config"):
            config_specified = True
        elif flag.startswith("--build"):
            build_type_specified = True 

        if build_type_specified and config_specified:
            break

    if not config_specified:
        flags.append("--config=standard")
    if not build_type_specified:
        flags.append("--build=release-nobounds")

    build("build", flags)

def build(option, flags = []):
    "perform the build with specified flags"

    cmmnd = ["dub", f"--root={root_directory}", option, *flags]

    try:
        out = (subprocess.run(cmmnd, stdout=subprocess.PIPE, check=True)
               .stdout
               .decode('utf-8'))
    except Exception:
        print("\nBuild Process Failed!")
        return False
    else:
        if out: print(out)
        print("Build process completed without errors")
        return True

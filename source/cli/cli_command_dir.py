"""
dir command for command line interface REPL
"""

from cli_utils import clean_input

def command_dir(inp, workspace):
    "parse input and print out information about the requested directory"
    parse_input = clean_input(inp.split(' '))

    # lower level workpsace directory
    if len(parse_input) == 1:
        print("Workspace contains the following namespaces:")
        print(list(workspace.keys()))
        return

    # invalid input
    elif len(parse_input) > 2:
        print("multiple inputs given for dir, will not proceed")
        return

    # directory of object in the workspace
    else:
        target = parse_input[1]

    if target in workspace:
        print([field for field in dir(workspace[target])
               if not field.startswith('_')])
    else:
        print((f"Input {target} not understood, must be an object "
            " in the workspace"))

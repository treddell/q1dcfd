"""
qplot command for command line interface REPL
"""
import sys, traceback
import matplotlib.pyplot as plt
from cli_utils import clean_input, split_fields_arguments

def _parse_rejoin_arguments(parse_input):
    # splitting by whitespace will break string labels, need to rejoin them
    proper_inputs = []
    rejoining_string = False
    string = ""
    for inp in parse_input:
        if '"' in inp:
            if not rejoining_string:
                rejoining_string = True
            else:
                rejoining_string = False 
                string += inp.replace('"','')
                proper_inputs.append(string)
                string = ""
                continue

        if rejoining_string:
            string += inp.replace('"','')
            string += " "
        else:
            proper_inputs.append(inp) 

    return proper_inputs

def _retrieve_x_data(X_str, workspace):
    "Get the x data from the workspace "
    x_split = X_str.split('.')
    x_field = x_split[0]
    x_attr  = ''.join(x_split[1:])
    
    if not x_attr:
        x_extension = ''.join(['workspace["',  x_field,'"]'])
    else:
        x_extension = ''.join(['workspace["',  x_field,'"]','.',x_attr])

    try:
        return(eval(x_extension))
        
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return []

def _retrieve_y_data(fields, workspace):
    "Get the y data from the workspace"

    Y = []
    
    for field in fields:
        y_split = field.split('.')
        y_field = y_split[0]
        y_attr  = ''.join(y_split[1:])
        if not y_attr:
            y_extension = ''.join(['workspace["',  y_field,'"]'])
        else:
            y_extension = ''.join(['workspace["',  y_field,'"]',".",y_attr])
            
        try:
            Y.append(eval(y_extension))
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exc()
            return []

    return Y

def _produce_qplot(X, Y, args):
    " Produce the plot object"
    
    try:
        fig = plt.figure()
        ax  = fig.add_subplot(111)
        for y in Y: ax.plot(X, y)

        ax.grid(True)
        ax.set_xlabel(args.get("xlabel",""))
        ax.set_ylabel(args.get("ylabel",""))
        plt.show()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

def command_qplot(inp, workspace):
    "parses input and produces the requested plot"

    proper_inputs = _parse_rejoin_arguments(clean_input(inp.split(' ')))

    if len(proper_inputs) < 3:
        print("Must specify (X, Y) data")
        return

    default_args = {"xlabel": proper_inputs[1]}
    fields, args, err_string = split_fields_arguments(proper_inputs[2:],
                                                       default_args)
    if err_string: print(err_string) ; return

    X = _retrieve_x_data(proper_inputs[1], workspace)
    if X == []: return

    Y = _retrieve_y_data(fields, workspace)
    if Y == []: return

    _produce_qplot(X, Y, args)

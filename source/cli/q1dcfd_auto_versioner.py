""" Automatically updates the version number from the mercurial changeset
"""
import subprocess, os

directory = os.path.dirname(os.path.realpath(__file__)) # where am I?
root_directory = os.sep.join(directory.split(os.sep)[:-2])
src_directory = os.sep.join(directory.split(os.sep)[:-1])
git_directory = os.sep.join([root_directory,".git"])

test_directory = os.sep.join([root_directory, "tests"])
results_directory = os.sep.join([root_directory,"results",""])
scripts_directory = os.sep.join([root_directory, 'definition-scripts'])
tables_directory = os.sep.join([root_directory,"tabular_data"])

version_txt = os.sep.join([src_directory,'version.txt'])
lconfig_d = os.sep.join([src_directory,"lconfig.d"])
app_exec = f"{root_directory}{os.sep}q1dcfd"

def initialize():
    """
    Instructs the shell to run and retrieve hg commands to find the current 
    version. Returns the version as a string, and the path to lconfig.d
    Also saves version and hg changeset info to q1dcfd/version.txt
    """

    # make sure all necessary folders are in the lower directory
    root_directory_contents = os.listdir(root_directory)
    
    if not 'definition-scripts' in root_directory_contents:
        os.mkdir(os.sep.join([root_directory,'definition-scripts']))
    if not 'results' in root_directory_contents:
        os.mkdir(os.sep.join([root_directory,'results']))
    if not 'tabular_data' in root_directory_contents:
        os.mkdir(os.sep.join([root_directory,'tabular_data']))

    # retrieve the cached version
    if os.path.isfile(version_txt):
        with open(version_txt, 'r') as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith('version'):
                    cached_version = (line.rstrip()
                        .replace(' ','')
                        .replace('"','')
                        .split("=")[1])
    else:
        cached_version = "?"

    try:
        # check the mercurial changeset
        git_num = (subprocess
            .check_output(['git', '--git-dir', git_directory,
                           'rev-list', 'HEAD', "--count"])
            .strip()
            .decode('utf-8'))

        major_version = '0'
        minor_version = '1'

        # mercurial commit number of current sub version
        commit_of_current_minor_vers = 313
        build_version = str(int(git_num) - commit_of_current_minor_vers)

        current_version = f"{major_version}.{minor_version}.{build_version}"
        if not cached_version == current_version:
            # changes have been made, update changeset
            with open(version_txt, 'w') as f:
                f.write(f'version = "{current_version}"\n')
                f.write(f'commit = "{git_num}"\n')

    except:
        # Not running the mercurial distribution
        current_version = cached_version

    return current_version, lconfig_d

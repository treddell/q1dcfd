"""
bash command for command line interface
"""
import subprocess, os, sys, traceback
from cli_utils import clean_input

def command_bash(inp):
    "parses input and runs the resulting bash command, displays output"

    cmmnd = clean_input(inp.split(' '))[1:]

    try:
        out = (subprocess.run(cmmnd, stdout=subprocess.PIPE)
               .stdout.decode('utf-8'))
        print(out)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

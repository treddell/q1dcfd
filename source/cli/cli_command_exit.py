"""
exit command for command line interface REPL
"""

from cli_utils import progress_print

def command_exit(inp):
    """parse the input string, it is a valid exit command then return a break
    order
    """

    if inp.replace(' ','') == 'exit':
        print('\nThanks for using q1dcfd. Have a nice day :) ')
        progress_print(f'',"\n")
        return True
    else:
        print("exit command does not take arguments")
        return False

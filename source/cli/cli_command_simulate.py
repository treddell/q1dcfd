"""
run command for command line interface REPL
"""

from cli_utils import clean_input, progress_print
import traceback, os, sys, shutil, datetime, time, subprocess
from q1dcfd_auto_versioner import app_exec, scripts_directory, results_directory

def command_simulate(inp, autocompletion):
    "parses an input command and runs a simulation"

    parse_input = clean_input(inp.split(' '))
    if len(parse_input) == 1:
        print("Must specify the definition file to simulate")
        return

    def_file = parse_input[1]
    args = " ".join(parse_input[2:])
    def_file = os.sep.join([scripts_directory, def_file])

    # get a list of the pre-existing results
    result_folders = os.listdir(results_directory)

    # Generate the run command to pass to the executable via bash
    cmmnd = f"{app_exec} q1dcfd {def_file} {args}"

    try:
        # Run the simulation
        exit_code = subprocess.call(cmmnd, shell = True)
    except Exception:
        # Failure to run the simulation, print exception
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
    if not exit_code:
        # add results to autocompletion
        new_results = set(os.listdir(results_directory)) - set(result_folders)

        for result in new_results:
            load_result = ''.join(['load ', result])
            autocompletion.words.append(load_result)

        # copy the qdef script to the results folder
        _copy_def_script_to_results(def_file)

def _copy_def_script_to_results(def_file):
    "copies a definition script to a result folder, so that the simulation is repeatable"

    with open(def_file) as f:
        lines = f.readlines()

        for line in lines:
            if line.startswith("Simulation"):
                name = line.split(":")[1].strip()

                shutil.copyfile(def_file,
                                os.sep.join([results_directory, name, name + ".qdef"]))
                return
        

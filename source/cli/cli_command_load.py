"""
load command for command line interface REPL
"""
import os, sys, traceback
import numpy as np

from cli_utils import (progress_print, clean_input, multiple_replace,
                       process_arguments)
from q1dcfd_auto_versioner import results_directory

# Ensures that the results are reconstructed by numpy to the correct precision
_PRECISION_MAP = {
    'double': np.float64,
    'float': np.float32,
    'real': np.longdouble
}

# fields with these names will not be visible in results within the workspace
_DEFAULT_EXCLUSIONS = ["index_l", "index_r", "w_index_l", "w_index_r",
                      "name", "n_nodes"]

# mapping between stream property keys and the name of the .npy file
_STREAM_PROPERTIES = {
    "mass_index":"mass.npy",
    "energy_index":"energy.npy",
    "T_avg_index":"T_avg.npy",
    "p_avg_index":"p_avg.npy",
    "traversal_time_index": "traversal_time.npy"
}

class Result:
    "Storage structure for a domain and all its fields"

    def __init__(self, params):
        for key, value in params.items():
            if key in _DEFAULT_EXCLUSIONS: continue
            setattr(self, key, value)

def _structure_raw_data(path, keep_raw = False):
    " Reads a raw binary file and structures results into ordered fields for "
    " easy memmap access "

    # read the meta file and extract all relevant information
    meta = _read_meta_data(path + 'meta.txt')

    # get the raw binary data
    precision = meta.get("precision", "double")
    data = np.memmap(path + 'data', dtype = _PRECISION_MAP[precision], mode="r") 

    # shape the raw data into a timestep vs attribute array
    n_saved_props = meta["n_fields"]
    n_rows = int(data.shape[0]/(meta["n_fields"]))
    data   = np.reshape(data, (n_rows, meta["n_fields"]))

    # make the lower level data directory structure
    os.mkdir(os.sep.join([path,"fields"]))

    # save time array
    np.save(os.sep.join([path, "fields", "time"]), data[:,0])

    # loop through every domain and save the data as npy files in a domain 
    # subfolder
    for name, domain in meta["domains"].items():
        n_nodes = domain["n_nodes"]
        symbols = domain["data_contents"]
        n_symbols = len(symbols)
        w_index_l = domain["w_index_l"]

        os.mkdir(os.sep.join([path, "fields", name]))
        for j, symbol in enumerate(symbols):

            field = np.lib.format.open_memmap(
                os.sep.join([path, "fields", name, symbol + ".npy"]),
                dtype = np.float64, mode = "w+", shape = (n_rows, n_nodes))

            for i in range(n_nodes):
                field[:,i] = data[:, w_index_l + i*n_symbols + j]

    # read each stream and produce structured subfolders
    for name, stream in meta["streams"].items():
        
        os.mkdir(os.sep.join([path, "fields", name]))

        # stream has no saved time varying properties. Create an empty folder
        # and continue
        if(not any(key in stream for key in _STREAM_PROPERTIES.keys())): continue
            
        for key, value in _STREAM_PROPERTIES.items():
            if(key in stream):
                field = np.lib.format.open_memmap(
                    os.sep.join([path, "fields", name, value]),
                    dtype = np.float64, mode = "w+", shape = (n_rows,))

                field[:] = data[:, stream[key]]

    # read all thermal interfaces and produce structured subfolders
    for name, face in meta["faces"].items():
        if("q_dot_index" not in face): continue

        os.mkdir(os.sep.join([path, "fields", name]))

        if("q_dot_index" in face):
            field = np.lib.format.open_memmap(
                os.sep.join([path, "fields", name, "qdot.npy"]),
                dtype = np.float64, mode="w+", shape = (n_rows,))

            field[:] = data[:, face["q_dot_index"]]
            
    # remove the raw data to save space
    if not keep_raw:
        os.remove(path + "data")

def _load_results_into_fields(path):
    "Open a results folder and post process all data"

    # read the meta file and extract all relevant information
    meta = _read_meta_data(path + 'meta.txt')

    # load all results
    domains = {}
    for abs_path, dirs, files in os.walk(os.sep.join([path,"fields"])):
        name = abs_path.split(os.sep)[-1]

        # load 'loose' data such as time arrays
        if name == "fields":
            for file in [file for file in files if file.endswith(".npy")]:
                domains[file.replace(".npy","")] = (
                    np.lib.format.open_memmap(os.sep.join([abs_path, file])))

        # load domain data
        elif name in meta["domains"]:
            domains.update(_load_datatypes(meta["domains"], name, abs_path, files))

        # load stream data
        elif name in meta["streams"]:
            domains.update(_load_datatypes(meta["streams"], name, abs_path, files))

        # load face data
        elif name in meta["faces"]:
            domains.update(_load_datatypes(meta["faces"], name, abs_path, files))
        
    return domains

def _load_datatypes(meta_dict, name, abs_path, files):
    "Loads data from meta_dict as memory maps into a subdictionary"
    domain = {name:Result(meta_dict[name])}
    for file in [file for file in files if file.endswith(".npy")]:
        setattr(domain[name], file.replace(".npy", ""),
            np.lib.format.open_memmap(os.sep.join([abs_path, file])))

    return domain

def _read_dir(results_dir, result_file):
    "Read the result file sub-directory and return a list of contents"
    "or error"

    path = os.sep.join([results_directory, result_file, ""])

    if not os.path.exists(path):
        return "", [], f'directory {path} does not exist'

    contents = os.listdir(path)
    err_string = ""

    if "fields" not in contents and "data" not in contents:
        err_string += (f"Results folder {path} contains neither a raw"
            f" 'data' file, or a 'fields' sub-directory, cannot load") 
    
    if not 'meta.txt' in contents:
        err_string += f"Results folder {path} does not contain a meta.txt file"

    return path, contents, err_string

def _add_keys_to_autocomplete(autocompletion, domains):
    "Adds all the loaded results into the autocompletion suggester list"
    
    for key in domains.keys():

        # ignore empty results
        if hasattr(domains[key], "data_contents"):
            if not domains[key].data_contents: continue
            
        # want to remove the brackets to avoid spam
        super_key = key.split("[")[0]
        if super_key not in autocompletion.words:
            autocompletion.words.append(super_key)
        if key  == 'time':
            continue
            
        for attr in [x for x in dir(domains[key]) if not x.startswith('_')]:
            autocompletion.words.append(''.join([key,'.',attr]))

def command_load(inp, workspace, autocompletion):
    "parses input and loads results into workspace"

    parse_input = clean_input(inp.split(' '))

    # read the results folder directory
    if len(parse_input) == 1:
        print("Must enter a results folder to load")
        return []

    path, contents, err_string = _read_dir(results_directory, parse_input[1])
    args, err_string2 = process_arguments(parse_input[2:])
    
    if err_string or err_string2:
        print(err_string + err_string2)
        return []

    # first time load, structure raw binary file into .npy tree structure
    if("fields" not in contents):
        try:
            progress_print("Structuring Results")
            _structure_raw_data(path, args.get("keep_raw", False))
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            progress_print("Structuring Results", "[FAILED]\n")
            traceback.print_exc()
            return []
        else:
            progress_print("Structuring Results", "[DONE]\n")
            
    # load the results folder directory
    try:
        progress_print(f'Loading results folder {path}')
        domains = _load_results_into_fields(path)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        progress_print(f'Loading results folder {path}',"[FAILED]\n")
        traceback.print_exc()
        return []
    else:
        # success, add fields to workspace
        progress_print(f'Loading results folder {path}',"[DONE]\n")
        workspace.update(domains)         
        _add_keys_to_autocomplete(autocompletion, domains)
        return domains.keys()

def _read_meta_data(filename):
    "Reads a metadata file and returns a dictionary of contents"
    
    flag_reading_system = False
    flag_reading_streams = False
    flag_reading_faces = False
    flag_reading_domains = False

    metadata = {"domains":{}, "streams": {}, "faces": {}}
    domain = {}
    stream = {}
    face = {}

    with open(filename, "r") as f:
        # read and process each line
        for line in f.readlines():

            # read system specific metadata
            if flag_reading_system:
                _read_metadata_to_dict(metadata, line)

            # read stream specific metadata
            elif flag_reading_streams:
                # add the previous stream data to the master metadata list
                # and start on the next stream
                if line.startswith("[STREAM]"):
                    name = line.split(":")[1]
                    name = multiple_replace(name)
                    if stream:
                        metadata["streams"][stream["name"]] = stream
                    stream = {"name": name}

                _read_metadata_to_dict(stream, line)

            # read thermal interface specific metadata
            elif flag_reading_faces:
                # add the previous face data to the master metadata list
                # and start on the next face
                if line.startswith("[FACE]"):
                    name = line.split(":")[1]
                    name = multiple_replace(name)
                    if face:
                        metadata["faces"][face["name"]] = face
                    face = {"name": name}

                _read_metadata_to_dict(face, line)

            # read domain specific metadata
            elif flag_reading_domains:
                # add the previous domain metadata to the master metadata list
                # and start on the next domain
                if line.startswith("[DOM]"):
                    name = line.split(":")[1]
                    name = multiple_replace(name)
                    if domain:
                        metadata["domains"][domain["name"]] = domain
                    domain = {"name":name}
                
                _read_metadata_to_dict(domain, line)

            if "[SYSTEM DATA]" in line:
                flag_reading_system = True
            # switch to domain specific metadata reader
            elif "[STREAM DATA]" in line:
                flag_reading_system = False
                flag_reading_streams = True
            elif "[FACE DATA]" in line:
                # complete the last stream
                if stream:
                    metadata["streams"][stream["name"]] = stream
                flag_reading_streams = False
                flag_reading_faces = True
            elif "[DOMAIN DATA]" in line:
                # complete the last face
                if stream:
                    metadata["faces"][face["name"]] = face
                flag_reading_faces = False 
                flag_reading_domains = True

        # complete the last domain
        if domain:
            metadata["domains"][domain["name"]] = domain

    return metadata

def _read_metadata_to_dict(dic, line):
    "Parses a line and extracts its contents into a dictionary"
    line = multiple_replace(line)
    if not line:
        return
    else:
        split = line.split(":")
        if len(split) != 3:
            return

        key, value, ty = line.split(":")
        dic.update({key: _to_type(value, ty)})
  
def _to_type(value, ty):
    "Convert a parsed string into the required type"
    if ty == "str":
        return value
    elif ty == "str[]":
        if(value.startswith("[")): value = value[1:]
        if(value.endswith("]")): value = value[:-1]
        splits = value.split(",")
        if splits == [""]:
            return []
        return splits
    elif ty == "int":
        return int(value)
    elif ty == "float":
        return float(value)
    elif ty =="float[]":
        value = multiple_replace(value,{"[","]"})
        return [float(val) for val in value.split(",")]
    else:
        raise TypeError(f"Type {ty} not understood")

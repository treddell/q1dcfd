"""
del command for command line interface REPL
"""

from cli_utils import clean_input

def command_del(inp, workspace, autocompletion):
    "parse the input and mark successful objects for deletion"
    parse_input = clean_input(inp.split(' '))

    if len(parse_input) == 1:
        print("Must specify an object to delete")
        return None

    targets = parse_input[1:]
    if targets == ["WORKSPACE"]:
        targets = dict(workspace)

    field_deletions = []
    for target in targets:
        split_target = target.split('.')

        target_field = split_target[0]
        target_attr  = ''.join(split_target[1:])

        if not target_field in workspace:
            print((f"Not deleting {target}, not found in the workspace"))
            continue

        if not target_attr:
            # deletion is occuring over a field, mark for complete removal
            field_deletions.append(target)
            workspace.pop(target)

            search_field = ''.join([target_field,'.'])

            autocompletion.words[:] = (
                word for word in autocompletion.words
                    if not word.startswith(search_field)
            )
        else:
            # deleting an attribute
            exec(''.join(['del workspace["',target_field, '"].',target_attr]))
            autocompletion.words.remove(target)

    return field_deletions

import CoolProp.CoolProp as CP
import numpy as np
import datetime, os, struct, itertools, shutil, sys, time
import matplotlib.pyplot as plt

from coolprop_mappings import COOLPROP_PARAMETERS_MAP, VALID_OUTPUTS,\
    VALID_PAIRS, NUMERICAL_DERIVATIVES, COOLPROP_PHASE_MAP, VALID_PHASES

MAXIMUM_DERIVATIVE_ATTEMPTS = 10

class MakeTable(object):
    """
    Makes a table of flow values for tabular taylor series interpolation

    Input:
        table_loc       : table name and location from local directory.
        I1              : state input 1 (string)
        I2              : state input 2 (string)
        I1_max          : maximum value of I1
        I2_max          : maximum value of I2
        N1              : maximum number of subdivisions along input 1 axis
        N2              : maximum number of subdivisions along input 2 axis
        fluid_name      : CoolProp name of the fluid (string)
        EOS             : 'HEOS', 'REFPROP', or one of CoolProps cubic equations
                            of state
        include         : string of symbols to output delimited by commas,
                            eg: 'rho,p,T'
        exclude_phase   : string of phase types to exclude
        tol_consistency : maximum error in fluid input <-> output reversibility
        warn_consistency: maximum error in fluid input <-> output reversibility
                            to raise a warning
        ignore_failure  : True/False, if True will not attempt to patch
                            convergence failures
    """

    def __init__(self, table_loc, fluid_name, I1_key, I2_key, I1_min, I1_max,
            I2_min, I2_max,
            N1               = 200,
            N2               = 200,
            EOS              = 'HEOS',
            include          = 'rho, h, p, T, Cp, k, mu',
            exclude_phase    = '',
            tol_consistency  = 1.0E-2,
            warn_consistency = 1.0E-5,
            ignore_failure   = False,
            delay_parameter_plot = False
        ):

        self.table_loc       = table_loc
        self.fluid_name      = fluid_name
        self.I1_key          = I1_key
        self.I2_key          = I2_key
        self.I1_min          = I1_min
        self.I1_max          = I1_max
        self.I2_min          = I2_min
        self.I2_max          = I2_max
        self.N1              = N1
        self.N2              = N2
        self.EOS             = EOS
        self.include         = include
        self.exclude_phase   = exclude_phase
        self.tol_consistency = tol_consistency
        self.warn_consistency= warn_consistency
        self.ignore_failure  = ignore_failure
        self.delay_parameter_plot = delay_parameter_plot

        # --------------------------- failure counts  --------------------------
        self.failure_excluded_state = []
        self.failure_convergence    = []
        self.failure_consistency    = []
        self.warning_consistency    = []
        self.patches                = []

        # ------------------ Determine the input type --------------------------
        self.inputs = {I1_key, I2_key}
        # ----------------------------- get list of outputs --------------------
        self.ordered_outputs = tuple(include.replace(' ','').split(','))
        self.outputs         = set(self.ordered_outputs)

        # ---------------- get set of ignored phase types ---------------------
        self.ignore_phases = set(exclude_phase.replace(' ','').split(','))

        if '' in self.ignore_phases:
            self.ignore_phases.remove('')

        if self.ignore_phases == None:
            self.ignore_phases = set()

        # make sure the input is consistent
        self.check_input()

        # -------------------------- initialize fluid --------------------------
        self.fluid = CP.AbstractState(EOS, fluid_name)

        # define a copy of the fluid class for numerical derivative calculations
        self.perturbed_fluid = CP.AbstractState(EOS, fluid_name)

        # ------------ map the input pair to the coolprop input pair -----------
        self.I1_map = COOLPROP_PARAMETERS_MAP[I1_key]
        self.I2_map = COOLPROP_PARAMETERS_MAP[I2_key]

        # ----------------------- map the output values ------------------------
        self.output_count = len(self.outputs) * 6
        self.output_map = []

        for output in self.ordered_outputs:
            self.output_map.append(COOLPROP_PARAMETERS_MAP[output])

        # --------------------- map the excluded phases ------------------------
        self.phase_map = set()

        for phase in self.ignore_phases:
            self.phase_map.add(COOLPROP_PHASE_MAP[phase])

        # ---------------------- define the table spacing ----------------------
        self.I1_spacing = (I1_max - I1_min)/N1
        self.I2_spacing = (I2_max - I2_min)/N2

        # ---------------------- mesh of all data points -----------------------
        self.mesh = itertools.product(
            np.linspace(I1_min + self.I1_spacing*0.5,
                        I1_max - self.I1_spacing*0.5, N1),
            np.linspace(I2_min + self.I2_spacing*0.5,
                        I2_max - self.I2_spacing*0.5, N2)
            )

        self.N_rows   = N1*N2

        # ----------------------- make the table folder ------------------------
        if not self.table_loc.endswith('/'):
            self.table_loc += '/'

        if not os.path.exists(table_loc):
            os.makedirs(table_loc)

    # -------------------------- INPUT VALIDATION ------------------------------
    def check_input(self):
        " Makes sure the input is consistent "
        if not self.inputs in VALID_PAIRS:
            raise ValueError(
                (f"Input pairs {self.inputs} not a valid combination, "
                f"allowable pairs are {VALID_PAIRS}")
            )

        if not self.outputs <= VALID_OUTPUTS:
            bad_input = self.outputs - (VALID_OUTPUTS & self.outputs)
            raise ValueError(
                (f"Output arguments not valid. The invalid arguments are "
                f" {bad_input}")
            )

        if not self.ignore_phases <= VALID_PHASES:
            bad_input = self.ignore_phases - (VALID_PHASES & self.ignore_phases)
            raise ValueError(
                f'input for excluded_phase: {bad_input}, not recognized'
            )

        if not self.I1_max > self.I1_min:
            raise ValueError(
                (f"I1_max ({self.I1_max}) must be greater than I1_min "
                f"({self.I1_min})")
            )

        if not self.I2_max > self.I2_min:
            raise ValueError(
                (f"I2_max ({self.I2_max}) must be greater than I2_min "
                f"({self.I2_min})")
            )

        if (    self.N1 < 1
            or  self.N2 < 1
            or  not type(self.N1) == int
            or  not type(self.N2) == int
            ):

            raise ValueError(
                (f"Number of inputs N1 = {self.N1}, N2 = {self.N2} must be a "
                "positive integer")
            )

        if self.inputs & self.outputs:
            bad_input = inputs & outputs
            print(
                (f"fastflow.make_table: Output parameters {bad_input} were "
                f"specified as inputs. These will be removed")
            )
            outputs -= inputs

    # ---------------------------- WRITE TABLE ---------------------------------
    def write_out_table(self):
        "Write out the table to file and display the failure cases"
        self.write_out_data_file()

        # ------------- share the juicy outcomes with the eager user -----------
        if (    not self.failure_excluded_state
            and not self.failure_consistency
            and not self.failure_convergence
            and not self.warning_consistency
            and not self.patches
            ):

            self.write_out_meta_file('')
            print(f'Table built sucessfully in directory {self.table_loc}')

        else:

            n_fconv   = len(self.failure_convergence)
            n_fcons   = len(self.failure_consistency)
            n_fes     = len(self.failure_excluded_state)
            n_wcons   = len(self.warning_consistency)
            n_patches = len(self.patches)

            number_of_failures = n_fconv + n_fcons + n_fes
            number_of_warnings = n_wcons + n_patches

            # write out leading failure message
            if number_of_failures > 0:
                failure_message = (f"{number_of_failures} input pairs were not"
                    " generated.")
            else:
                failure_message = ''

            # write out excluded state failures
            if n_fes >0:
                excluded_message = (f"\n{n_fes} input pairs resulted in an "
                f"excluded fluid phase {self.ignore_phases}")
            else:
                excluded_message = ''

            # write out convergence failures
            if n_fconv > 0:
                convergent_message = (f"\n{n_fconv} input pairs failed "
                    "to converge")
            else:
                convergent_message = ''

            # write out consistency failures
            if n_fcons > 0:
                consistency_message = (f"\n{n_fcons} input pairs were "
                "inconsistent")
            else:
                consistency_message = ''

            # write out consistency_warnings
            if n_wcons > 0:
                warn_cons_message = (f"\n{n_wcons} input pairs had "
                "inconsistency exeeding the warning threshold")
            else:
                warn_cons_message = ''

            # write out patch warnings
            if n_patches > 0:
                patch_warnings = (f"\n{n_patches} input pairs failed initial"
                " convergence but were sucessfully patched")
            else:
                patch_warnings = ''

            messages = (f"Table built in directory {self.table_loc}. ")

            messages = ''.join([
                messages, failure_message, excluded_message, convergent_message,
                consistency_message, warn_cons_message, patch_warnings
            ])

            print(messages)

            # ------------------- write the meta file --------------------------
            self.write_out_meta_file(messages)

            # plot the success/fail calculations
            print("Generating State Space Calculation Convergence Map...")

            plt.figure()
            plt.title(f'Calculation Failures')

            if self.failure_convergence:
                vals = np.array(self.failure_convergence)
                X = vals[:,0]
                Y = vals[:,1]

                plt.plot(X, Y, 'xr',
                    label      = 'convergence failure',
                )

            if self.failure_consistency:
                vals = np.array(self.failure_consistency)
                X = vals[:,0]
                Y = vals[:,1]

                plt.plot(X, Y, '*', color = 'red',
                    label      = 'consistency failure'
                )

            if self.failure_excluded_state:
                vals = np.array(self.failure_excluded_state)
                X = vals[:,0]
                Y = vals[:,1]

                plt.plot(X, Y, '.', color = 'gray',
                    label      = 'excluded phase',
                    markersize = 1
                )

            if self.warning_consistency:
                vals = np.array(self.warning_consistency)
                X = vals[:,0]
                Y = vals[:,1]

                plt.plot(X, Y, '*', color = 'orange',
                    label      = 'consistency warning'
                )

            if self.patches:
                vals = np.array(self.patches)
                X = vals[:,0]
                Y = vals[:,1]
                plt.plot(X, Y, 'x', color = 'blue',
                    label      = 'convergence failure (patched)'
                )

            plt.xlabel(self.I1_key)
            plt.ylabel(self.I2_key)
            plt.xlim(self.I1_min, self.I1_max)
            plt.ylim(self.I2_min, self.I2_max)
            plt.legend()
            plt.tight_layout()
            plt.savefig(self.table_loc + 'calculation_failure.pdf',
                bbox_inches = 'tight')
            if not self.delay_parameter_plot:
                plt.show()

    def write_out_meta_file(self, messages):
        "Save the meta file to disk"
        with open (self.table_loc + "meta.txt", 'w') as f:

            f.write(
                'fastflow fluid properties tabular taylor series expansions'
            )
            f.write('\n')
            f.write(f'Created on: {datetime.datetime.now()} \n')
            f.write(f'fluid: {self.fluid_name} \n')
            f.write(f'generating database: {self.EOS} \n')
            f.write('All values in SI units \n \n')

            f.write('DO NOT MODIFY THIS FILE \n \n')

            f.write('[INPUT 1] \n')
            f.write(f'input 1 var: {self.I1_key} \n')
            f.write(f'input 1 min: {self.I1_min} \n')
            f.write(f'input 1 max: {self.I1_max} \n')
            f.write(f'input 1 N: {self.N1} \n')
            f.write('input 1 grid type: linear \n \n')

            f.write('[INPUT 2] \n')
            f.write(f'input 2 var: {self.I2_key} \n')
            f.write(f'input 2 min: {self.I2_min} \n')
            f.write(f'input 2 max: {self.I2_max} \n')
            f.write(f'input 2 N: {self.N2} \n')
            f.write('input 2 grid type: linear \n \n')

            f.write('[OUTPUTS] \n')
            f.write(f'output variables: {self.ordered_outputs} \n')
            f.write(f'output variable count: {int(self.output_count/6)} \n')
            f.write(f'output column count: {self.output_count + 1} \n' )

            output_columns = ['row_valid, ']
            for output in self.ordered_outputs:
                output_columns.extend([
                    f'{output} , '
                    f'd({output})/d({self.I1_key}) ,',
                    f'd({output})/d({self.I2_key}) ,',
                    f'd2({output})/d({self.I1_key})^2 ,',
                    f'd2({output})/d({self.I2_key})^2 ,',
                    f'd2({output})/d({self.I1_key})d({self.I2_key}) ,'
                ])

            column_contents_string = ''.join(output_columns)
            f.write(f'column contents: {column_contents_string} \n \n')

            f.write('[GENERATION LOG] \n')
            f.write(messages)

    def write_out_data_file(self):
        " Write out the binary data file to disk"
        record_structure = struct.Struct('<d' + 'd'*self.output_count)

        with open(self.table_loc + "data.b", 'wb') as f:
            N_rows_done = 0

            for I1, I2 in self.mesh:
                row = self.calculate_row(I1, I2)

                # ------------ write record to binary file -------------------
                f.write(record_structure.pack(*row))
                N_rows_done += 1

                if N_rows_done % 100 == 0 or N_rows_done == self.N_rows:
                    sys.stdout.write(
                        (f"\rGenerated {N_rows_done} table entries of "
                            f"{self.N_rows}")
                    )
                    sys.stdout.flush()
        sys.stdout.write("\n")
        sys.stdout.flush()

    # ------------------------------ CALCULATIONS  -----------------------------
    def calculate_row(self, I1, I2):
        "Calculates all outputs and derivatives for a single input pair"

        fluid = self.fluid # construct a pointer for convinience
        row = []
        is_converged = True

        try:
            # try evaluating at this input state
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1, self.I2_map, I2
                )
            )

        except ValueError:
            # calculation failure
            if self.inputs == {'p', 'T'}:
                # could be too close to the saturation curve
                is_on_envelope = self.check_PT_INPUTS(I1,I2)
                if not is_on_envelope:
                    is_converged, I1, I2 = self.patcher(I1, I2)
            else:
                is_converged, I1, I2 = self.patcher(I1, I2)

        # if the error can not be sufficiently patched then return a failure
        if not is_converged:
            return self.fail_convergence(I1, I2)

        # check if the fluid state is in the list of excluded phases
        if fluid.phase() in self.phase_map:
            return self.fail_excluded_state(I1,I2)

        # Make sure the inputs are consistent
        is_consistent = self.check_consistency(I1, I2)
        if not is_consistent:
            return self.fail_consistency(I1, I2)

        row.append(0)
        # not an invalid state, will continue
        for out_string, output in zip(self.ordered_outputs, self.output_map):
            x00 = fluid.keyed_output(output) # output (x) at x(I1, I2)
            row.append(x00)

            # ------ do we need to calculate a numerical derivative ? ------
            if out_string in NUMERICAL_DERIVATIVES:

                # ------ step sizes for numerical partial derivatives ---------
                delta_I1 = 0.0001*self.I1_spacing
                delta_I2 = 0.0001*self.I2_spacing

                try:
                    row.extend(
                        self.calculate_numerical_derivative_first_order(
                            I1, I2, output, x00, delta_I1, delta_I2
                        )
                    )

                except ValueError:
                    # could not compute derivatives
                    return self.fail_convergence(I1, I2)

            elif out_string == "Cp":
                # First order approximation of constant pressure specific heat
                row.extend(
                    self.calculate_cp_derivative_first_order(
                    fluid, output
                    )
                )

            elif out_string == "Cv":
                # First order approximation of constant volume specific heat
                row.extend(
                    self.calculate_cv_derivative_first_order(fluid, output)
                )

            elif out_string == "a":
                # First order approximation of sound speed
                row.extend(
                    self.calculate_sound_speed_derivative_first_order(
                        fluid, output
                    )
                )

            # If we are lucky, we can use a pre-implemented two phase analytical
            #   derivative
            elif fluid.phase() == COOLPROP_PHASE_MAP['twophase']:
                try:
                    row.extend(
                        self.calculate_analytical_two_phase_derivatives(
                            fluid, output
                        )
                    )

                except ValueError:
                    # two phase derivative not implemeted in CoolProp

                    try:
                        row.extend(
                            self.calculate_numerical_derivatives(
                                I1, I2, output, x00, delta_I1, delta_I2
                            )
                        )

                    except ValueError:
                        # could not compute derivatives
                        return self.fail_convergence(I1, I2)

            # Single phase analytical calculation
            else:
                row.extend(
                    self.calculate_analytical_one_phase_derivatives(
                        fluid, output
                    )
                )

        return row

    def calculate_analytical_one_phase_derivatives(self, fluid, out):
        """ Calculate the analytical derivatives of a single phase fluid
        'exactly' using the inbuilt equations of state in CoolProp

        Input:
            fluid: coolprop initialized AbstractState instance
            out  : map to the output parameter
        Returns:
            ( dx/dI1, dx/dI2, d^2(x)/dI1^2, d^2(x)/dI2^2, d^2(x)/d(I1)d(I2))
        """

        # create some pointers
        I1 = self.I1_map
        I2 = self.I2_map

        dI1    = fluid.first_partial_deriv(out, I1, I2)
        dI2    = fluid.first_partial_deriv(out, I2, I1)
        ddI1   = fluid.second_partial_deriv(out, I1, I2, I1, I2)
        ddI2   = fluid.second_partial_deriv(out, I2, I1, I2, I1)
        ddI1I2 = fluid.second_partial_deriv(out, I1, I2, I2, I1)

        return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_analytical_two_phase_derivatives(self, fluid, out):
        """ Calculate the analytical derivatives of a two phase fluid 'exactly'
        using the inbuilt equations of state in CoolProp

        Input:
            fluid: coolprop initialized AbstractState instance
            out: map to the output parameter
        Returns:
            ( dx/dI1, dx/dI2, d^2(x)/dI1^2, d^2(x)/dI2^2, d^2(x)/d(I1)d(I2))
        """

        # create some pointers
        I1 = self.I1_map
        I2 = self.I2_map

        dI1    = fluid.first_two_phase_deriv(out, I1, I2)
        dI2    = fluid.first_two_phase_deriv(out, I2, I1)
        ddI1   = fluid.second_two_phase_deriv(out, I1, I2, I1, I2)
        ddI2   = fluid.second_two_phase_deriv(out, I2, I1, I2, I1)
        ddI1I2 = fluid.second_two_phase_deriv(out, I1, I2, I2, I1)
        return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_numerical_derivative_first_order(
        self, I1, I2, out, x00, delta_I1, delta_I2, depth = 0
    ):
        """
        Calculates both first order derivatives numerically, sets second order
        derivatives equal to zero

        Inputs:
            fluid             : coolprop initialized AbstractState instance
            I1,I2             : the location of the inputs, centre point of the
                                derivatives
            delta_I1, delta_I2: step sizes used to generate the finite
                                differences
            out               : map to the output parameter
            x00               : value of x(I1, I2)
        Returns:
            ( dx/dI1, dx/dI2, d^2(x)/dI1^2, d^2(x)/dI2^2, d^2(x)/d(I1)d(I2))
        """
        if depth >= MAXIMUM_DERIVATIVE_ATTEMPTS:
            raise ValueError(f'Derivative calculation failed {depth} times')

        fluid = self.perturbed_fluid

        try:
            # -------------------------- +0 node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 + delta_I1, self.I2_map, I2
                )
            )

            xp0 = fluid.keyed_output(out)

            # -------------------------- -0 node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 - delta_I1, self.I2_map, I2
                )
            )

            xn0 = fluid.keyed_output(out)

        except ValueError:
            # reduce step size and try again
            delta_I1 *= 0.5
            return self.calculate_numerical_derivative_first_order(
                I1, I2, out, x00, delta_I1, delta_I2, depth = depth + 1
            )

        try:
            # -------------------------- 0+ node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1, self.I2_map, I2 + delta_I2
                )
            )

            x0p = fluid.keyed_output(out)

            # -------------------------- 0- node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1, self.I2_map, I2 - delta_I2
                )
            )

            x0n = fluid.keyed_output(out)

        except ValueError:
            # reduce step size and try again
            delta_I2 *= 0.5
            return self.calculate_numerical_derivative_first_order(
                I1, I2, out, x00, delta_I1, delta_I2, depth = depth + 1
            )

        dI1    = (xp0 - xn0)/(2.0*delta_I1)
        dI2    = (x0p - x0n)/(2.0*delta_I2)
        ddI1   = 0.0
        ddI2   = 0.0
        ddI1I2 = 0.0

        return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_numerical_derivatives(self,
        I1, I2, out, x00, delta_I1, delta_I2, depth = 0
    ):
        """
        Calculates all first and second order derivatives numerically.

        Inputs:
            fluid: coolprop initialized AbstractState instance
            I1,I2: the location of the inputs, centre point of the derivatives
            delta_I1, delta_I2: step sizes used to generate the finite
                                differences
            out: map to the output parameter
            x00: value of x(I1, I2)
        Returns:
            ( dx/dI1, dx/dI2, d^2(x)/dI1^2, d^2(x)/dI2^2, d^2(x)/d(I1)d(I2))
        """
        if depth >= MAXIMUM_DERIVATIVE_ATTEMPTS:
            raise ValueError(f'Derivative calculation failed {depth} times')

        fluid = self.perturbed_fluid

        try:
            # -------------------------- +0 node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 + delta_I1, self.I2_map, I2
                )
            )

            xp0 = fluid.keyed_output(out)

            # -------------------------- -0 node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 - delta_I1, self.I2_map, I2
                )
            )

            xn0 = fluid.keyed_output(out)

        except ValueError:
            # reduce step size and try again
            delta_I1 *= 0.5
            return self.calculate_numerical_derivatives(
                I1, I2, out, x00, delta_I1, delta_I2, depth = depth + 1
            )

        try:
            # -------------------------- 0+ node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1, self.I2_map, I2 + delta_I2
                )
            )

            x0p = fluid.keyed_output(out)

            # -------------------------- 0- node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1, self.I2_map, I2 - delta_I2
                )
            )

            x0n = fluid.keyed_output(out)

        except ValueError:
            # reduce step size and try again
            delta_I2 *= 0.5
            return self.calculate_numerical_derivatives(
                I1, I2, out, x00, delta_I1, delta_I2, depth = depth + 1
            )

        try:
            # -------------------------- ++ node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 + delta_I1, self.I2_map, I2 + delta_I2
                )
            )

            xpp = fluid.keyed_output(out)

            # -------------------------- +- node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 + delta_I1, self.I2_map, I2 - delta_I2
                )
            )

            xpn = fluid.keyed_output(out)

            # -------------------------- -+ node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 - delta_I1, self.I2_map, I2 + delta_I2
                )
            )

            xnp = fluid.keyed_output(out)

            # -------------------------- -- node -----------------------
            fluid.update(
                *CP.generate_update_pair(
                    self.I1_map, I1 - delta_I1, self.I2_map, I2 - delta_I2
                )
            )

            xnn = fluid.keyed_output(out)

        except ValueError:
            # reduce step size and try again
            delta_I1 *= 0.5
            delta_I2 *= 0.5
            return self.calculate_numerical_derivatives(
                I1, I2, out, x00, delta_I1, delta_I2, depth = depth + 1
            )

        dI1    = (xp0 - xn0)/(2.0*delta_I1)
        dI2    = (x0p - x0n)/(2.0*delta_I2)
        ddI1   = (xp0 + xn0 - 2.0*x00)/(delta_I1*delta_I1)
        ddI2   = (x0p + x0n - 2.0*x00)/(delta_I2*delta_I2)
        ddI1I2 = (xpp + xnn - xpn - xnp)/(4.0*delta_I1*delta_I2)

        return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_sound_speed_derivative_first_order(self, fluid, out):
        """
        Can analytically calculate the first derivatives of sound speed as a
        second derivative of pressure :

        a = sqrt(dp/drho)|s , so

        da/dx|y = 1/2a * (d^2 p)/(d(rho)|s d(x)|y)

        assumes all higher order derivatives are zero

        """

        a00 = fluid.speed_sound()

        if fluid.phase() == COOLPROP_PHASE_MAP['twophase']:
            I1       = fluid.keyed_output(self.I1_map)
            I2       = fluid.keyed_output(self.I2_map)
            delta_I1 = 0.0001*self.I1_spacing
            delta_I2 = 0.0001*self.I2_spacing

            return self.calculate_numerical_derivative_first_order(
                I1, I2, out, a00, delta_I1, delta_I2
            )

        else:
            dI1 = 0.5/ a00 * fluid.second_partial_deriv(
                CP.iP, CP.iDmass, CP.iSmass, self.I1_map, self.I2_map
            )

            dI2 = 0.5/a00 * fluid.second_partial_deriv(
                CP.iP, CP.iDmass, CP.iSmass, self.I2_map, self.I1_map
            )

            ddI1   = 0.0
            ddI2   = 0.0
            ddI1I2 = 0.0

            return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_cp_derivative_first_order(self, fluid, out):
        """
        Can analytically calculate the first derivatives of constant pressure
        specific heat as second derivative of enthalpy :

        Cp = dh/dT|p so

        dCp/dx|y = d^2(h)/(d(T)|p d(x)|y)

        assumes all higher order derivatives are zero

        """

        if fluid.phase() == COOLPROP_PHASE_MAP['twophase']:
            x00 = fluid.cpmass()
            I1 = fluid.keyed_output(self.I1_map)
            I2 = fluid.keyed_output(self.I2_map)
            delta_I1 = 0.0001*self.I1_spacing
            delta_I2 = 0.0001*self.I2_spacing

            return self.calculate_numerical_derivative_first_order(
                I1, I2, out, x00, delta_I1, delta_I2
            )

        else:
            dI1 = fluid.second_partial_deriv(
                CP.iHmass, CP.iT, CP.iP, self.I1_map, self.I2_map
            )

            dI2 = fluid.second_partial_deriv(
                CP.iHmass, CP.iT, CP.iP, self.I2_map, self.I1_map
            )

            ddI1   = 0.0
            ddI2   = 0.0
            ddI1I2 = 0.0

            return dI1, dI2, ddI1, ddI2, ddI1I2

    def calculate_cv_derivative_first_order(self, fluid, out):
        """
        Can analytically calculate the first derivatives of constant volume
        specific heat as second derivative of internal energy :

        Cv = de/dT|rho so

        dCv/dx|y = d^2(e)/(d(T)|rho d(x)|y)

        assumes all higher order derivatives are zero
        """

        if fluid.phase() == COOLPROP_PHASE_MAP['twophase']:
            x00 = fluid.cvmass()
            I1 = fluid.keyed_output(self.I1_map)
            I2 = fluid.keyed_output(self.I2_map)
            delta_I1 = 0.0001*self.I1_spacing
            delta_I2 = 0.0001*self.I2_spacing

            return self.calculate_numerical_derivative_first_order(
                I1, I2, out, x00, delta_I1, delta_I2
            )

        else:
            dI1 = fluid.second_partial_deriv(
                CP.iUmass, CP.iT, CP.iDmass, self.I1_map, self.I2_map
            )

            dI2 = fluid.second_partial_deriv(
                CP.iUmass, CP.iT, CP.iDmass, self.I2_map, self.I1_map
            )

            ddI1   = 0.0
            ddI2   = 0.0
            ddI1I2 = 0.0

            return dI1, dI2, ddI1, ddI2, ddI1I2

    # ----------------------- EXCEPTION MANAGEMENT -----------------------------

    def patcher(self, I1, I2):
        """ Convergence Failure Patching Routine
        Currently attempts to find a point near the original input which
        converges, tests 8 points near the original. If these all fail, then
        marks as a convergence failure, otherwise marks as a patch

        Should make this significantly more rigorous in future
        """

        if self.ignore_failure:
            return False, I1, I2

        delta_I1 = self.I1_spacing*0.1
        delta_I2 = self.I2_spacing*0.1
        patcher_values = np.zeros((5,5, self.output_count), dtype = float)

        shifted_inputs = (
            (delta_I1, 0.0), (-delta_I1, 0.0), (0.0, delta_I2),
            (0.0, -delta_I2), (delta_I1, delta_I2), (delta_I1, -delta_I2),
            (-delta_I1, delta_I2), (-delta_I1, - delta_I2)
        )

        for (I1_s, I2_s) in shifted_inputs:
            try:
                self.perturbed_fluid.update(
                    *CP.generate_update_pair(
                        self.I1_map, I1 + delta_I1, self.I2_map, I2
                    )
                )

                self.patches.append([I1, I2])
                return True, I1_s, I2_s

            except ValueError:
                pass

        return False, I1, I2

    def fail_consistency(self,I1, I2):
        "Inconsistent input <-> output reversibility"
        self.failure_consistency.append([I1, I2])
        return [-1]*(self.output_count + 1)

    def fail_excluded_state(self, I1, I2):
        "The input falls into an excluded phase regions"
        self.failure_excluded_state.append([I1, I2])
        return [-1]*(self.output_count + 1)

    def fail_convergence(self, I1, I2):
        "The backend calculator failed to converge"
        self.failure_convergence.append([I1,I2])
        return [-1]*(self.output_count + 1)

    # ------------------------ QUALITY OF CALCULATION CHECKS -------------------
    def check_PT_INPUTS(self, I1, I2):
        "Look for pressure temperature inputs too close to the saturation curve"

        if self.I1_map == CP.iT:
            T = I1
            p = I2
        else:
            p = I1
            T = I2

        # snap to either the lower or upper phase envelope
        self.perturbed_fluid.update(CP.PT_INPUTS,p, T + 1)

        if self.perturbed_fluid.phase() == COOLPROP_PHASE_MAP['gas']:
            self.fluid.update(CP.QT_INPUTS, 1.0, T)
            return True

        elif self.perturbed_fluid.phase() == COOLPROP_PHASE_MAP['liquid']:
            self.fluid.update(CP.QT_INPUTS, 0.0, T)
            return True

        elif self.perturbed_fluid.phase() in {
                COOLPROP_PHASE_MAP['supercritical'],
                COOLPROP_PHASE_MAP['supercritical_gas'],
                COOLPROP_PHASE_MAP['supercritical_liquid']
            }:

            self.fluid.update(
                CP.PT_INPUTS, self.fluid.p_critical(), self.fluid.T_critical()
            )
            return True

        else:
            return False

    def check_consistency(self, I1, I2):
        """
        Checking for reversibility of the equation of state, failure could
        indicate a poorly behaved or innacurate underlying equation of state
        """
        if not self.inputs == {'p', 'h'}:
            try:
                # transform to pressure enthalpy configuration space
                p, h = self.fluid.p(), self.fluid.hmass()
                self.fluid.update(CP.HmassP_INPUTS, h, p)

            except ValueError:
                return False

        else:
            try:
                # transform to density energy configuration space
                rho, e = self.fluid.rhomass(), self.fluid.umass()
                self.fluid.update(CP.DmassUmass_INPUTS, rho, e)

            except ValueError:
                return False

        # Do the consistency check
        I1_check = self.fluid.keyed_output(self.I1_map)
        I2_check = self.fluid.keyed_output(self.I2_map)

        err1 = abs(1.0 - I1_check/I1)
        err2 = abs(1.0 - I2_check/I2)

        # Failed outright
        if(     err1 > self.tol_consistency
            or  err2 > self.tol_consistency
        ):
            return False

        # Passed, but above warning threshold
        elif(
                err1 > self.warn_consistency
            or  err2 > self.warn_consistency
        ):
            self.warning_consistency.append([I1, I2])
            return True

        # Passed with flying colours
        else:
            return True

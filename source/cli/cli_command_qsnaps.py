"""
qseries command for command line interface REPL
"""

import sys, traceback
import matplotlib.pyplot as plt
from cli_utils import clean_input, find_nearest
from plot_tools import plot_series, plot_series_stylish

def _pull_contents(workspace, fields, attr):
    """
    Extract a list of domains and dictionary of arguments from a list of
    field inputs
    """

    domains = []
    args = {}
    
    # sort remaining args into fields and parameters
    for field in fields:
        if field.startswith("-"):
            # input is an argument
            try: key, value = field.split("=")
            except Exception: key = field; value = None
            # except Exception: return [], {}, f"argument {field} not understood"
            
            args[key[1:]] = value
            continue 

        if not field in workspace:
            return [], {}, (f"Was unable to retrieve domain: {field}, was not "
                            "found in workspace")

        target = workspace[field]
        
        # is a stream
        if hasattr(target, "domains"):
            for dom in target.domains:
                if not hasattr(workspace[dom], attr):
                    return [], {}, (f"Unable to produce plot, domain: {dom} does not "
                                    f"have attribute: {attr}")
                domains.append(workspace[dom])
        # is a domain
        else:
            if not hasattr(target, attr):
                return [], {}, (f"Unable to produce plot, domain: {field} does not "
                                f"have attribute: {attr}")
            domains.append(target)

    return domains, args, ""

def _get_time_slices(workspace, args):
    """ 
    Returns a list of time indices to plot spatial distributions along
    """

    # no time indices or values provided
    if not "slices" in args and not "times" in args:
        try:
            M = len(workspace["time"])
        except Exception:
            return [], f"No 'time' attribute in workspace, unable to produce plots"

        if(M > 5):
            return [0, int(M//5), int((2*M)//5), int((3*M)//5),
                    int((4*M)//5), M - 1], ""
        else:
            return list(range(M)), ""

    # time indices provided
    elif "slices" in args and not "times" in args:
        try:
            return [int(x) for x in args["slices"].split(",")], ""
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            return [], exc_traceback
        
    # time values provided
    elif "times" in args and not "slices" in args:
        try:
            time_slices = []
            for time in args["times"].split(","):
                t, idx = find_nearest(workspace["time"], float(time))
                time_slices.append(idx)
            return time_slices, ""
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            return [], exc_traceback

    # time values and indices provided 
    return [], "Cannot assign both --slices and --times"

# load hto_new_model/55_70_30_T25_HP30_R8E1_2E4_Q1E-13_5E-4_QTx100

def command_qsteady_summary(inp, workspace):
    parse_input = clean_input(inp.split(' '))

    # Get args
    args = {}
    for field in parse_input[1:]:
        if field.startswith("-"):
            try: key, value = field.split("=")
            except Exception: key = field; value = None
            args[key[1:]] = value
            continue 

    time = 1.0
    t, idx = find_nearest(workspace["time"], float(time))
    time_slices = [idx]

    co2_boundary_lines = [0.0, 0.2, 0.28, 0.48, 1.48, 1.68, 1.76, 1.96]
    co2_domain_names = [
        'inflow', 'inflow_pipe[0]', 'compressor_inflow', 'compressor_outflow',
        'compressor_hx_pipe[0]', 'hx[1]', 'hx_turbine_pipe[0]',
        'turbine_inflow', 'turbine_outflow', 'outflow_pipe[0]'#, 'outflow'
        ]
    co2_domains = []
    for dom in co2_domain_names: co2_domains.append(workspace[dom])

    hto_x_start = 1.48
    hto_rev = True
    hto_domain_names = ['hx[0]']
    hto_domains = []
    for dom in hto_domain_names: hto_domains.append(workspace[dom])

    s = 0.6 # plot scaling
    fig, (ax0, ax1) = plt.subplots(2, figsize=(12*s, 8.5*s), sharex=True)

    try:
        # Temp plots
        ax0.grid(which='major', axis='y', linestyle='-', alpha=0.35)
        plot_series_stylish(ax0, co2_domains, 'T', time_slices, workspace["time"], args,
                '#142680ff', 1.0, r'CO$_2$')
        for xi in co2_boundary_lines:
            ax0.axvline(xi, color ="black", linestyle="--", alpha=0.5, linewidth=1.0)
        plot_series_stylish(ax0, hto_domains, 'T', time_slices, workspace["time"], args,
                '#e3120bff', 1.0, 'Heat transfer oil', hto_x_start, hto_rev)
        ax0.set_ylabel('Temperature [K]')
        ax0.set_xlim((0, 1.96))
        ax0.set_ylim((310, 640))

        # Pressure plots
        ax1.grid(which='major', axis='y', linestyle='-', alpha=0.35)
        plot_series_stylish(ax1, co2_domains, 'p', time_slices, workspace["time"], args,
                '#142680ff', 1E-6, r'CO$_2$', 0.0, False, False)
        for xi in co2_boundary_lines:
            ax1.axvline(xi, color ="black", linestyle="--", alpha=0.5, linewidth=1.0)
        plot_series_stylish(ax1, hto_domains, 'p', time_slices, workspace["time"], args,
                '#e3120bff', 1E-6, 'Heat transfer oil', hto_x_start, hto_rev,
                False)
        ax1.set_xlabel('Position [m]')
        ax1.set_ylabel('Pressure [MPa]')
        ax1.set_xlim((0, 1.96))
        # ax1.set_ylim((3.8, 13.2))

        # plt.show()

        plt.tight_layout()
        plt.savefig('/home/maxgains/articles/mpc-for-sco2-cycles/figures/T_p_init.pdf',
                pad_inches=0.05)

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

def command_qseries(inp, workspace):
    "parse input and produce the desired plot"

    parse_input = clean_input(inp.split(' '))

    if len(parse_input) < 3:
        print("Must provide attribute to plot as 'qsnaps <attribute> <domain1>...'")
        return

    # pull out a listing of the domains and arguments, and get the attribute to plot
    attr = parse_input[1]
    domains, args, err_string = _pull_contents(workspace, parse_input[2:], attr)
    if err_string:
        print(err_string)
        return

    # get the temporal indices to plot each distribution along
    time_slices, err_string_2 = _get_time_slices(workspace, args)
    if err_string_2:
        print(err_string_2)
        return

    # Produce the plot object
    try:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid(which='major', axis='y', linestyle='-', alpha=0.35)
        plot_series(ax, domains, attr, time_slices, workspace["time"], args)
        ax.legend()
        ax.set_xlabel(args.get("xlabel", 'Position [m]'))
        ax.set_ylabel(args.get("ylabel", attr))
        plt.show()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

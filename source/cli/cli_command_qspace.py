"""
qspace command for command line interface REPL
"""

import sys, traceback
import matplotlib.pyplot as plt
from cli_utils import clean_input
from plot_tools import property_space_plot

def _pull_contents(workspace, contents):
    """
    produces a list of domains and a dict of arguments by processing a list
    of input fields
    """
    
    domains = []
    args = {}

    delete_indicies = []
    for field in contents:
        # is actually an argument
        if field.startswith("-"):
            splt = field.split("=")
            args[splt[0].replace("-","")] = splt[1]
            continue

        if field not in workspace:
            return [], {}, f"domain {field} not found in workspace"

        target = workspace[field]
        
        # is a stream
        if hasattr(target, "domains"):
            for dom in target.domains:
                domains.append(workspace[dom])

        # is a domain
        else:
            domains.append(workspace[field])

    # append default arguments
    args, err_string = _populate_default_arguments(args)

    # if the cycle is closed, then end upon starting location
    if(args["closed"]):
        domains.append(domains[0])

    return domains, args, err_string

def _populate_default_arguments(args):
    "If an optional argument is not defined, then provide a default"

    if("closed" not in args):
        args["closed"] = True

    elif args["closed"] == "True":
        args["closed"] = True
    elif args["closed"] == "False":
        args["closed"] = False
    else:
        return {}, f"argument 'closed' must be True/False, but found {1}".format(args["closed"])

    return args, ""
        

def command_qspace(inp, workspace):
    "parses input and produces a property space plot"

    parse_input = clean_input(inp.split(" "))

    if len(parse_input) < 4:
        print("Must specify attribute1, attribute2 and at least one domain")
        return

    input1 = parse_input[1]
    input2 = parse_input[2]

    # extract a list of domains and arguments
    domains, args, err_string = _pull_contents(workspace, parse_input[3:])
    if err_string:
        print(err_string)
        return

    try:
        if "slices" in args:
            time_slices = args["slices"].split(",")
            for i, t_slice in enumerate(time_slices):
                time_slices[i] = int(t_slice) 
        else:
            time_slices = [0, -1]
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

    try:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid(True)
        ax.set_xlabel(args.get("xlabel", input1))
        ax.set_ylabel(args.get("ylabel", input2))

        property_space_plot(ax, domains, input1, input2, time_slices, 
            workspace["time"], args)
        plt.legend()
        plt.show()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()

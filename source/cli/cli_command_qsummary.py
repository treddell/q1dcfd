import sys, traceback

import matplotlib.pyplot as plt
import numpy as np

from cli_command_qtime import _get_time_array, _retrieve_domains_multi_attrs
from cli_utils import clean_input, split_fields_arguments
from plot_tools import gauges, dimensionless_gauge
from matplotlib_mappings import DEFAULT_COLOURS

def command_qsummary(inp, workspace):
    time, err_string = _get_time_array(workspace)
    if err_string: print(err_string) ; return

    # process the input
    parse_input = clean_input(inp.split(" "))
    if err_string: print(err_string) ; return

    default_args = {"xlabel": "time (s)"}
    fields, args, err_string = split_fields_arguments(parse_input[1:], default_args)
    if err_string: print(err_string) ; return
    if len(fields) > 0:
        print("Fields inputs not permitted for command 'qsummary'.")
        return

    _produce_qsummary_plots(workspace, time, args)

def _produce_qsummary_plots(workspace, time, args):
    # Unpack the workspace
    data = {
        'compressor' : workspace["compressor_node"],
        'turbine'    : workspace["turbine_node"],
        'hx_hot'     : workspace["hx[0]"],
        'hx_co2'     : workspace["hx[1]"],
        'pump'       : workspace["incomp_inflow_boundary"],
        'mpc'        : workspace["controller"]
    }

    # Sampling time
    if 'dt' in args: mpc_dt = float(args['dt'])
    else: raise Exception("Must supply MPC sampling time with '-dt=<dt>'.")

    # qsummary -dt=0.2 -N_margin_min=0.05 -N_margin_max=0.075 -Q_target=3100,3600,2450

    # Outputs
    w_nom = 75 # kW
    if 'Q_target' in args:
        Q_target = args['Q_target'].split(',')
        for i, q in enumerate(Q_target): Q_target[i] = float(q)

    if 'T_target' in args:
        T_target = float(args['T_target'])

    # Constraints: HT oil
    dT_m_max = 15
    dmdot_hot_max = 2 * mpc_dt
    mdot_hot_min = 2 # not used yet
    T_in_turb_max = 570
    T_in_turb_max_margin = 568
    N_margin_min = 0.0
    N_margin_max = 0.0
    if 'N_margin' in args:
        N_margin_min = float(args['N_margin'])
        N_margin_max = float(args['N_margin'])
    if 'N_margin_min' in args: N_margin_min = float(args['N_margin_min'])
    if 'N_margin_max' in args: N_margin_max = float(args['N_margin_max'])

    # Constraints: C02-C02
    # dT_m_max = 40
    # dmdot_hot_max = 4
    # T_in_turb_max = 570

    # Main settings
    n_plots = 8
    s = 1.0 # plot scaling
    f, axs = plt.subplots(n_plots, figsize=(5*n_plots*s, 7*s), sharex=True)
    linewidth=1.5
    nice_red = '#e3120bff'
    nice_blue = '#142680ff'

    # Trim initialization time
    if 't_start' in args:
        t_start = args['t_start']
    else:
        t_start = time[np.where(np.isnan(getattr(data['mpc'], 'W_net_ref')))[0][-1]+1]
    k_start = np.where(time >= float(t_start))[0][0]
    time = time[k_start:]
    for k, domain in data.items():
        for key in domain.data_contents:
            setattr(domain, key, getattr(domain, key)[k_start:])
    t0 = time[0]
    time = time - t0

    # # Setpoints
    # w_net_0 = 55
    # w_net_1 = 30
    # t_w_net = 20
    # assert(t_w_net >= float(t_start))
    # t_w_net = t_w_net - t0 # adjust now we have done the check
    # w_net_t_ref = np.array([time[0], t_w_net, t_w_net, time[-1]])
    # w_net_ref   = np.array([w_net_0, w_net_0, w_net_1, w_net_1])

    # --------- Net power --------
    w_net_t_ref = time
    w_net_ref = -1 * getattr(data['mpc'], 'W_net_ref') / 1000
    w_net = -1 * (getattr(data['compressor'], 'w_sh') + getattr(data['turbine'], 'w_sh'))
    w_net = w_net / 1000 # kW
    axs[0].plot(time, w_net, color='k', linewidth=linewidth,
            label=r'$\dot{W}_{net}$')
    axs[0].step(w_net_t_ref, w_net_ref, color='b', linestyle='--', linewidth=linewidth,
            label=r'$\dot{W}_{net,ref}$')
    axs[0].set_ylabel(r'$z_0 = \dot{W}_{net}$ [kW]')
    axs[0].legend()

    w_net_nom = w_net / w_nom * 100
    ax0_twin = axs[0].twinx()
    ax0_twin.plot(time, w_net_nom, alpha=0)
    ax0_twin.set_ylabel('Nominal output [%]')
    axs[0].set_ylim(0.95*min(w_net), 1.05*max(w_net))
    ax0_twin.set_ylim(0.95*min(w_net_nom), 1.05*max(w_net_nom))

    # -------- Q_in --------
    q_dot =  (getattr(data['hx_hot'], 'h')[:, 0]  * getattr(data['hx_hot'], 'mdot')[:, 0]) \
            - (getattr(data['hx_hot'], 'h')[:, -1] * getattr(data['hx_hot'], 'mdot')[:, -1])
    q_dot = q_dot / 1000 # kW
    axs[1].plot(time, q_dot, color='k', linewidth=linewidth, label=r'$\dot{Q}_{in}$')
    axs[1].set_ylabel(r'$z_1 = \dot{Q}_{in}$ [kW]')

    if 'Q_target' in args:
        # Hack to find step times
        i_steps = np.where(abs(w_net_ref[1:] - w_net_ref[:-1]) > 0)[0]
        q_dot_ref = np.ones(q_dot.shape)
        q_dot_ref[0:i_steps[0]]          = Q_target[0]
        q_dot_ref[i_steps[0]:i_steps[1]] = Q_target[1]
        q_dot_ref[i_steps[1]:]           = Q_target[2]
        axs[1].plot(time, q_dot_ref, color='b', linestyle='--', linewidth=linewidth, label=r'$\dot{Q}_{in,ref}$')
        axs[1].legend()

    # -------- T_motor --------
    T_m = getattr(data['compressor'], 'T_motor')
    dtime = time[1:] - time[:-1]
    dT_m = T_m[1:] - T_m[:-1]
    dT_m = dT_m[:,0] / dtime
    dT_m = np.hstack((dT_m, 0))

    axs[2].plot(time, T_m, color='k', linewidth=linewidth, label=r'$T_{motor}$')
    axs[2].legend(loc='upper right')
    axs[2].set_ylabel(r'$u_0 = T_{motor}$ [Nm]')

    # Highlight rate constraints
    axs[2].fill_between(time, 0.75*min(T_m), 1.25*max(T_m),
            where=dT_m > 0.98*dT_m_max,
            facecolor='red', alpha=0.3)
    axs[2].fill_between(time, 0.75*min(T_m), 1.25*max(T_m),
            where=dT_m < -0.98*dT_m_max,
            facecolor='red', alpha=0.3)
    axs[2].set_ylim(0.95*min(T_m), 1.05*max(T_m))

    # ax2_twin = axs[2].twinx()
    # ax2_twin.plot(time, dT_m, color='k', linestyle='--', linewidth=linewidth,
            # label=r'$\dot{T}_{motor}$')
    # ax2_twin.plot(time, -dT_m_max_arr, color='r', linestyle='--', linewidth=linewidth,
            # label=r'$|\dot{T}_{motor}|_{max}$')
    # ax2_twin.plot(time, dT_m_max_arr, color='r', linestyle='--', linewidth=linewidth)
    # ax2_twin.legend(loc='lower right')
    # ax2_twin.set_ylabel(r'$\dot{T}_{motor}$ [Nm/s]')

    # -------- mdot_hot --------
    mdot_hot  = getattr(data['hx_hot'], 'mdot')[:,0]
    dmdot_hot = (mdot_hot[1:] - mdot_hot[:-1]) / dtime
    dmdot_hot = np.hstack((dmdot_hot, 0))

    sample_int = int(round(mpc_dt / (time[1] - time[0])))
    try:
        sample_int = int(round(mpc_dt / (time[1] - time[0])))
        mdot_hot_ref  = getattr(data['pump'], 'mdot_ref')[:,0]
        mdot_hot_ref  = mdot_hot_ref[::sample_int]
        resampled_time = time[::sample_int]
        dresampled_time = resampled_time[1:] - resampled_time[:-1]
        dmdot_hot_ref = (mdot_hot_ref[1:] - mdot_hot_ref[:-1]) / dresampled_time
        dmdot_hot_ref = np.hstack((dmdot_hot_ref, 0))
        old_data = False
    # Hack for old data
    except Exception as e:
        # raise e
        print("\nSome error resampling. Is this old data?\n")
        mdot_hot_ref  = getattr(data['hx_hot'], 'mdot')[:,0]
        dmdot_hot_ref = (mdot_hot[1:] - mdot_hot[:-1]) / dtime
        dmdot_hot_ref = np.hstack((dmdot_hot_ref, 0))
        resampled_time = time
        old_data = True

    axs[3].plot(time, mdot_hot, color='k', linewidth=linewidth,
            label=r'$\dot{m}_{hot}$')
    if not old_data:
        axs[3].plot(resampled_time, mdot_hot_ref, color='k', linewidth=linewidth,
                linestyle=':', label=r'$\dot{m}_{hot,ref}$')
    axs[3].set_ylabel(r'$u_1 = \dot{m}_{hot}$ [kg/s]')
    axs[3].legend(loc='upper right')

    # Highlight rate constraints
    axs[3].fill_between(resampled_time, 0.75*min(mdot_hot), 1.25*max(mdot_hot),
            where=dmdot_hot_ref > 0.95*dmdot_hot_max,
            facecolor='red', alpha=0.3)
    axs[3].fill_between(resampled_time, 0.75*min(mdot_hot), 1.25*max(mdot_hot),
            where=dmdot_hot_ref < -0.95*dmdot_hot_max,
            facecolor='red', alpha=0.3)
    axs[3].set_ylim(0.95*min(mdot_hot), 1.05*max(mdot_hot))

    # ax3_twin = axs[3].twinx()
    # ax3_twin.plot(time, dmdot_hot, color='k', linestyle='--', linewidth=linewidth,
            # label=r'$\ddot{m}_{hot}$')
    # ax3_twin.plot(time, -dmdot_hot_max_arr, color='r', linestyle='--', linewidth=linewidth,
            # label=r'$|\ddot{m}_{hot}|_{max}$')
    # ax3_twin.plot(time, dmdot_hot_max_arr, color='r', linestyle='--', linewidth=linewidth)
    # ax3_twin.legend(loc='lower right')
    # ax3_twin.set_ylabel(r'$\ddot{m}_{hot}$ [kg/s^2]')

    # -------- T_in_turb --------
    T_in_t_arr = np.ones(time.shape) * T_in_turb_max
    axs[4].plot(time, getattr(data['turbine'], 'T_in'), color='k', linewidth=linewidth,
            label=r'$T_{in,turbine}$')
    axs[4].plot(time, T_in_t_arr, color='r', linestyle='--', linewidth=linewidth,
            label=r'$T_{in,turbine,max}$')
    if 'T_target' in args:
        T_target_arr = np.ones(time.shape) * T_target
        axs[4].plot(time, T_target_arr, color='b', linestyle='--', linewidth=linewidth,
                label=r'$T_{in,turbine,ref}$')
    axs[4].set_ylabel(r'$T_{in,turbine}$ [K]')
    axs[4].legend(loc='upper right')

    # Colour the 'zone' constraint for N_min
    T_in_t_arr_margin = np.ones(time.shape) * T_in_turb_max_margin
    axs[4].fill_between(time, T_in_t_arr, T_in_t_arr_margin, color='r', alpha=0.35)

    # -------- N_comp --------
    N_nom = 2094.395
    axs[5].plot(time, N_nom * getattr(data['compressor'], 'speed'), color='k', linewidth=linewidth,
            label=r'$N_{compressor}$')
    # axs[5].plot(time, (1 + N_margin_min) * N_nom * getattr(data['mpc'], 'N_min'), color='r',
            # linestyle='--', linewidth=linewidth, label=r'$N_{compressor,min}$')
    axs[5].plot(time, N_nom * getattr(data['mpc'], 'N_min'), color='r',
            linestyle='--', linewidth=linewidth, label=r'$N_{compressor,min}$')
    axs[5].plot(time, (1 - N_margin_max) * N_nom * getattr(data['mpc'], 'N_max'), color='r',
            linestyle='--', linewidth=linewidth, label=r'$N_{compressor,max}$')
    axs[5].set_ylabel(r'$N_{compressor}$ [RPM]')
    axs[5].legend(loc='upper right')

    # Colour the 'zone' constraint for N_min
    axs[5].fill_between(time, N_nom * getattr(data['mpc'], 'N_min')[:,0],
            (1 + N_margin_min) * N_nom * getattr(data['mpc'], 'N_min')[:,0],
            color='r', alpha=0.35)

    # -------- mdot_co2 --------
    axs[6].plot(time, getattr(data['compressor'], 'mdot'), color='k', linewidth=linewidth,
            label=r'$\dot{m}_{compressor}$')
    axs[6].plot(time, getattr(data['turbine'], 'mdot'), color='k', linewidth=linewidth,
            linestyle='--',
            label=r'$\dot{m}_{turbine}$')
    axs[6].set_ylabel(r'$\dot{m}_{co2}$ [kg/s]')
    axs[6].legend(loc='upper right')

    # -------- mdot_co2 --------
    axs[7].plot(time, getattr(data['compressor'], 'mdot') - getattr(data['turbine'], 'mdot'),
            color='k', linewidth=linewidth,
            label=r'$\Delta \dot{m}$')
    axs[7].set_ylabel(r'$\Delta \dot{m}$')
    # axs[7].legend(loc='upper right')

    # -------- Final formatting --------
    for ax in axs:
        ax.autoscale(enable=True, axis='x', tight=True)
        ax.grid(which='major', axis='y', linestyle='--', alpha=0.5)
    axs[-1].set_xlabel("Time [sec]", fontsize=11)
    # plt.tight_layout()
    plt.show()

    # plt.draw()
    # plt.pause(1)
    # print('\n\n')
    # input('<Hit Enter To Close Figures>')
    # plt.close()


import matplotlib.pyplot as plt

# Marker types recognized by matplotlib
MARKERS = [
	".", # 	point
	"d", # 	thin_diamond
	"o", # 	circle
	"+", # 	plus
	"x", # 	x
	"*", # 	star
	"v", # 	triangle_down
	"^", # 	triangle_up
	"<", # 	triangle_left
	">", # 	triangle_right
	"1", # 	tri_down
	"2", # 	tri_up
	"3", # 	tri_left
	"4", # 	tri_right
	"8", # 	octagon
	"s", # 	square
	"p", # 	pentagon
	"P", # 	plus (filled)
	"h", # 	hexagon1
	"H", # 	hexagon2
	"X", # 	x (filled)
	"D", # 	diamond
]

# The default matplotlib colour cycle
DEFAULT_COLOURS = plt.rcParams['axes.prop_cycle'].by_key()['color']
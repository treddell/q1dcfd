"""
assignment operator for command line interface
"""

import sys, traceback

from cli_utils import clean_input

def command_assign(inp, workspace):
    """parse input to determine if assignment is valid
    """

    parse_input = clean_input(inp.split(':='))
    if len(parse_input) != 2:
        print(
            (f"Invalid syntax, could not assign key '{parse_input[0]}' to "
            f"expression {parse_input[1:]}")
        )
        return None, None, True

    return parse_input[0].rstrip(), parse_input[1], False

"""
tablegen command for command line interface REPL
"""

import os, sys, traceback
import CoolProp.CoolProp as CP
import matplotlib.pyplot as plt
from cli_utils import clean_input, progress_print
from make_table import MakeTable
from q1dcfd_auto_versioner import tables_directory

def command_tablegen(inp):
    """ Process an input string of the form:

    tablegen <name> <fluid> p1:p2 T1:T2 --args

    And produces a tabular data set in q1dcfd/tabular_data/name
    """

    parsed_input =  clean_input(inp.split(' '))

    if len(parsed_input) < 5:
        print("minimum inputs required: <name> <fluid> p1:p2 T1:T2")
        return

    name = parsed_input[1]
    fluid = parsed_input[2]

    # read pressure
    try:
        p1,p2 = parsed_input[3].split(":")
        p1 = float(p1)
        p2 = float(p2)
    except Exception:
        print((f"Failed to parse pressure range {parsed_input[3]}"
        f"This must be of the form 'pressure_min:pressure_max'"))
        return

    if not p2>p1:
        print(f"p2 must be greater than p1, but inputs are p1:p2 = {p1}:{p2}")
        return

    # Read temperature
    try:
        T1,T2 = parsed_input[4].split(":")
        T1 = float(T1)
        T2 = float(T2)
    except Exception:
        print((f"Failed to parse temperature range {parsed_input[4]}"
        f"This must be of the form 'temperature_min:temperature_max'"))
        return

    if not T2>T1:
        print(f"T2 must be greater than T1, but inputs are T1:T2 = {T1}:{T2}")
        return

    # Read the optional arguments
    optional_args = parsed_input[5:]
    arg_dict = {"delay_parameter_plot":True}
    bad_input_message = []

    for arg in optional_args:
        
        try:
            key,value = arg.split("=")
        except Exception:
            bad_input_message.append(f"input {arg} could not be understood \n")

            continue

        if key =="--exclude_phase":
            arg_dict['exclude_phase'] = value
        
        elif key == "--warn_consistency":
            try:
                arg_dict['warn_consistency'] = float(value)
            except Exception:
                bad_input_message.append(f"Failed to cast {value} to float\n")

        elif key == "--tol_consistency":
            try:
                arg_dict['tol_consistency'] = float(value)
            except Exception:
                bad_input_message.append(f"failed to cast {value} to float\n")

        elif key == "--ignore_failure":
            try:
                arg_dict['ignore_failure'] = bool(value)
            except Exception:
                bad_input_message.append("failed to cast {value} to boolean\n")

        elif key == "--EOS":
            arg_dict["EOS"] = value

        elif key =="--N1":
            try:
                arg_dict['N1'] = int(value)
            except Exception:
                bad_input_message.append(f"failed to cast {value} to integer\n")

        elif key == "--N2":
            try:
                arg_dict['N2'] = int(value)
            except Exception:
                bad_input_message.append(f"failed to cast {value} to integer\n")

        else:
            bad_input_message.append(f"argument {key} not understood\n")

    if not "exclude_phase" in arg_dict:
        print(
            ('Excluding two phase flow by default, to enable, use argument '
            '--exclude_phase=""')
        )
        arg_dict["exclude_phase"] = "twophase"

    if bad_input_message:
        print("Bad inputs, will not proceed")
        print("".join(bad_input_message))
        return

    # Calculate the input range for each parameter space
    
    try:
        state = CP.AbstractState('HEOS', fluid)
    except Exception:
        print(f"Could not initialize a CoolProp fluid state with fluid: {fluid}")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return
    
    try:
        state.update(CP.PT_INPUTS, p1, T1)
        e11 = state.umass()
        rho11 = state.rhomass()
        s11 = state.smass()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print((f"COOLPROP FAILURE: Could not calculate fluid state at "
            f"p={p1}, T = {T1}"))
        traceback.print_exc()
        return

    try:
        state.update(CP.PT_INPUTS, p2, T2)
        e22 = state.umass()
        rho22 = state.rhomass()
        s22 = state.smass()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print((f"COOLPROP FAILURE: Could not calculate fluid state at "
            f"p={p2}, T = {T2}"))
        traceback.print_exc()
        return

    try:
        state.update(CP.PT_INPUTS, p1, T2)
        e12 = state.umass()
        rho12 = state.rhomass()
        s12 = state.smass()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print((f"COOLPROP FAILURE: Could not calculate fluid state at "
            f"p={p1}, T = {T2}"))
        traceback.print_exc()
        return

    try:
        state.update(CP.PT_INPUTS, p2, T1)
        e21 = state.umass()
        rho21 = state.rhomass()
        s21 = state.smass()
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print((f"COOLPROP FAILURE: Could not calculate fluid state at "
            f"p={p2}, T = {T1}"))
        traceback.print_exc()
        return

    e1 = min(e11, e12, e21, e22)
    e2 = max(e11, e12, e21, e22)

    rho1 = min(rho11, rho12, rho21, rho22)
    rho2 = max(rho11, rho12, rho21, rho22)

    s1 = min(s11, s12, s21, s22)
    s2 = max(s11, s12, s21, s22)

    # get the location of the table to write to file
    directory_tables = os.sep.join([tables_directory, name])

    # make the tabular data folder, if it exists already then overwrite
    if not os.path.exists(directory_tables):
        os.makedirs(directory_tables)

    # density energy
    print("Generating density energy table...")
    arg_dict['include'] = 'h, T, Cp, mu, k, a, Q, s, Cv'
    try:
        density_energy_table = MakeTable(
            os.sep.join([directory_tables,"density_energy"]),
            fluid, "rho", "e", rho1, rho2, e1, e2, **arg_dict)
        density_energy_table.write_out_table()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

    # pressure, entropy
    print("Generating pressure entropy table...")
    arg_dict['include'] = 'h, rho'
    try:
        pressure_entropy_table = MakeTable(
            os.sep.join([directory_tables,"pressure_entropy"]),
            fluid, "p", "s", p1, p2, s1, s2, **arg_dict)
        pressure_entropy_table.write_out_table()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

    # pressure, temperature
    print("Generating pressure temperature table...")
    arg_dict['include'] = 'rho, e'
    try:
        pressure_temperature_table = MakeTable(
            os.sep.join([directory_tables,"pressure_temperature"]),
            fluid, "p", "T", p1, p2, T1, T2, **arg_dict)
        pressure_temperature_table.write_out_table()

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return

    plt.close("all")

import CoolProp.CoolProp as CP

#  maps the fastflow parameters name strings to the CoolProp low level interface
COOLPROP_PARAMETERS_MAP = {
	'rho': CP.iDmass, 
	'e':   CP.iUmass, 
	'h':   CP.iHmass, 
	'p':   CP.iP, 
	'T':   CP.iT, 
	'Q':   CP.iQ, 
	'a':   CP.ispeed_sound, 
	's':   CP.iSmass, 
	'Cp':  CP.iCpmass, 
	'Cv':  CP.iCvmass, 
	'k':   CP.iconductivity, 
	'mu':  CP.iviscosity, 
}

# ------------------ a set of the valid output parameters ----------------------
VALID_OUTPUTS = set(COOLPROP_PARAMETERS_MAP.keys())

# ------------------------ a tuple of the allowable input pairs ----------------
# correct as of Coolprop 6.1.0
VALID_PAIRS  = (
	{'rho','e'},
	{'rho','h'},
	{'rho','p'},
	{'rho','T'},
	{'rho','s'},
	{'rho','Q'},
	{'e','p'},
	{'h','p'},
	{'h','s'},
	{'T','Q'},
	{'p','s'},
	{'p','Q'},
	{'p','T'},
	{'s','T'}
)

# These properties do not have analytical derivatives supported by CoolProp
# 	or fasflow, a first order approximation will be used
NUMERICAL_DERIVATIVES = {'Q', 'k', 'mu'}

COOLPROP_PHASE_MAP = {
	'critical_point'      :CP.get_phase_index('phase_critical_point'),
	'gas'                 :CP.get_phase_index('phase_gas'),
	'liquid'              :CP.get_phase_index('phase_liquid'),
	'supercritical'       :CP.get_phase_index('phase_supercritical'),
	'supercritical_gas'   :CP.get_phase_index('phase_supercritical_gas'),
	'supercritical_liquid':CP.get_phase_index('phase_supercritical_liquid'),
	'twophase'            :CP.get_phase_index('phase_twophase'),
	'unknown'             :CP.get_phase_index('phase_unknown')
}

VALID_PHASES = set(COOLPROP_PHASE_MAP.keys())

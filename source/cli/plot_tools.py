"""
Plotting function presets
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib_mappings import MARKERS

def snapshots(ax, data, attribute, time_slices, colour = 'b'):
    """
    Plots a series of state snapshots at incriments in time and appends them to
    a plot object
    Input:
    ax          : matplotlib axis object
    data        : domain Result object
    attribute       : attribute of attribute to plot
    time_slices : list of time indices to plot
    colour       : colour of plot objects
    """
    markers      = iter(MARKERS)
    attribute_data   = getattr(data, attribute)
    X            = data.node_coords
    M,N          = attribute_data.shape
    N2           = int(N/5)
    reduced_X    = X[::N2]
    reduced_data = attribute_data[:,::N2]

    for j in time_slices:
        t = j/M
        mark = next(markers)
        ax.plot(X, attribute_data[j, :], color = colour)
        ax.plot(reduced_X, reduced_data[j, :],
            color     = colour,
            linestyle = ' ',
            marker    = mark,
            label     = f" t = {t:.2f}"
        )

def dimensionless_gauge(ax, data, attribute, time, colour):
    """
    Plots a property which does not depend upon space, with respect to time
    Input:
    ax           : matplotlib axis object
    data         : domain Result object
    attribute    : attribute of attribute to plot
    args         : additional plot arguments
    """
    attribute_data = getattr(data, attribute)
    M = attribute_data.shape[0]
    M2 = int(M/5)
    reduced_t = time[::M2]
    reduced_data = attribute_data[::M2]

    ax.plot(time, attribute_data[:], color = colour)
    ax.plot(reduced_t, reduced_data[:],
            color     = colour,
            linestyle = ' ')

def gauges(ax, data, attribute, time, space_slices, colour):
    """
    Plots a series of state snapshots at incriments in space and appends them to
    a plot object
    Input:
    ax           : matplotlib axis object
    data         : domain Result object
    attribute    : attribute of attribute to plot
    space_slices : list of space indices to plot
    args         : additional plot arguments
    """
    markers = iter(MARKERS)
    attribute_data = getattr(data, attribute)
    M,N = attribute_data.shape
    M2 = int(M/5)
    reduced_t = time[::M2]
    reduced_data = attribute_data[::M2,:]

    if len(attribute_data[0]) < len(space_slices):
        space_slices = list(range(len(attribute_data[0])))

    node_coords = getattr(data, "node_coords")
        
    for i in space_slices:
        x = node_coords[i]
        mark = next(markers)
        ax.plot(time, attribute_data[:, i], color = colour)
        ax.plot(reduced_t, reduced_data[:, i],
            color     = colour,
            marker    = mark,
            linestyle = ' ',
            label     = f" x = {x:.2f}"
        )

def plot_series(ax, domains, attribute, time_slices, time, args):
    """
    Joins together a series of domains in sequence. Appends to a plot object
    """
    attribute_data = []
    X          = []
    x_start    = 0.0

    for domain in domains:
        attribute_data.append(getattr(domain, attribute))
        X_domain = np.array(domain.node_coords) + x_start
        X.extend(list((X_domain)))
        x_start = X_domain[-1]
        ax.axvline(x_start, color ="black", linestyle="--", alpha=0.5)

    attribute_data = np.concatenate(attribute_data, axis = 1)

    markers      = iter(MARKERS)
    M,N          = attribute_data.shape
    N2           = int(N/5)
    reduced_X    = X[::N2]
    reduced_data = attribute_data[:,::N2]

    colour = args.get("colour", "b")

    for j in time_slices:
        t = time[j]
        mark = next(markers)
        ax.plot(X, attribute_data[j, :], color = colour)
        ax.plot(reduced_X, reduced_data[j, :],
            color     = colour,
            linestyle = ' ',
            marker    = mark,
            label     = f" t = {t:.2f}"
        )

def property_space_plot(ax, domains, input1, input2, time_slices, time, args):
    """
    Produces a property space plot on axis "ax" consisting of domains "domains" 
    based upon attributes (input1, input2) at time indices time_slices
    """

    for domain in domains:
        if not hasattr(domain, input1):
            print(f"domain {domain} has no attribute {input1}")
            return
        if not hasattr(domain, input2):
            print(f"domain {domain} has no attribute {input2}")
            return

    markers = iter(MARKERS)

    colour = args.get("colour", "b")

    for j in time_slices:
        t = time[j]
        mark = next(markers)
        X = [] 
        Y = []
        X_ends = []
        Y_ends = []

        for domain in domains:
            
            x_data = list((getattr(domain, input1)[j]))
            y_data = list((getattr(domain, input2)[j]))

            X.extend(x_data)
            Y.extend(y_data)
            X_ends.append(x_data[0])
            X_ends.append(x_data[-1])
            Y_ends.append(y_data[0])
            Y_ends.append(y_data[-1])

        ax.plot(X_ends, Y_ends, color = colour, linestyle = " ", marker = mark,
            label = f"t={t:.2f}")
        ax.plot(X, Y, color = colour, linestyle = "-")

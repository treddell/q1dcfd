"""
command line interface q1dcfd REPL, for enhanced workflow optimization
"""

from prompt_toolkit import prompt, PromptSession
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
import subprocess, sys, traceback, os
import CoolProp.CoolProp as CP 
import numpy as np

from q1dcfd_auto_versioner import initialize
from cli_utils import clean_input, progress_print, print_centred
from cli_initialize_autocompletion import initialize_autocompletion
from cli_command_help import command_help
from cli_command_simulate import command_simulate
from cli_command_load import command_load
from cli_command_dir import command_dir
from cli_command_exit import command_exit
from cli_command_assign import command_assign
from cli_command_del import command_del
from cli_command_build import command_build
from cli_command_qplot import command_qplot
from cli_command_qsnaps import command_qseries, command_qsteady_summary
from cli_command_qtime import command_qtime, command_qtsum, command_qtqin
from cli_command_qsummary import command_qsummary
from cli_command_bash import command_bash
from cli_command_tablegen import command_tablegen
from cli_command_tests import command_tests
from cli_command_export import command_export
from cli_command_qspace import command_qspace
from cli_command_design import command_design

START_TEXT = (    "\nq1dcfd workspace manager \n"
            "q1dcfd > help     : for help on command line arguments \n"
            "q1dcfd > build    : recompile q1dcfd with specified flags\n"
            "q1dcfd > run      : simulate a .qdef definition file \n"
            "q1dcfd > load     : load a results folder into the workspace \n"
            "q1dcfd > tablegen : generate a tabular fluids data set \n"
            "q1dcfd > exit     : to exit \n")

def REPL():
    """
    Read Execute Print Loop, workspace manager for q1dcfd
    """

    # get the current version and set up directory structure, 
    # also check if installed
    version, lconfig_d = initialize()
    flag_installed = os.path.isfile(lconfig_d)

    # initialize the history keeper and autocompleter
    history = InMemoryHistory()
    autocompletion = initialize_autocompletion()
    session = PromptSession()

    # initialize the workspace and  command count
    counter = 0
    workspace = {}

    # Startup text
    progress_print(f'',"\n")
    print_centred('q1dcfd: Quasi 1D Compressible Flow Dynamics')
    print_centred(f'Version: {version}')
    print_centred('Author: Tom Reddell')

    if flag_installed:
        print(START_TEXT)
    else:
        print(("\nq1dcfd not correctly installed. No lconfig.d found\n"))
        return

    # start the REPL proper
    while True:
        inp = session.prompt(u'q1dcfd[{0}] > '.format(counter),
            auto_suggest = AutoSuggestFromHistory(),
            completer = autocompletion
        )

        inp = ''.join([inp,' '])
        counter +=1 # incriment the command count

        # Exit REPL and q1dcfd
        if inp.startswith('exit '):
            if command_exit(inp): break

        # Display help
        elif inp.startswith('help '):
            command_help(inp)

        # run simulation
        elif inp.startswith('run '):
            command_simulate(inp, autocompletion)

        elif inp.startswith('build '):
            if flag_installed:
                command_build(inp, autocompletion)
            else:
                print("q1dcfd not installed, will not build")

        # load results
        elif inp.startswith('load '):
            keys = command_load(inp, workspace, autocompletion)
            for key in keys:
                # is a compount domain. Carefully deal with indicies
                if("[" in key):
                    parent, index = key.split("[")
                    index = int(index.replace("]",""))
                    try:
                        eval(f'{parent}')
                    except Exception:
                        exec(f"{parent} = []")
                    
                    length_parent = eval(f"len({parent})")
                    if index >= length_parent:
                        n = index - length_parent + 1
                        exec(f'{parent} += [0]*{n}')

                exec(f'{key} = workspace["{key}"]')

        # List workspace contents
        elif inp.startswith('dir '):
            command_dir(inp, workspace)

        # Execute input using python
        elif inp.startswith('python '):
            try:
                exec(inp[7:])
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print("command not understood, failed with traceback:")
                traceback.print_exc()

        # Execute input using bash
        elif inp.startswith('bash '):
            command_bash(inp)

        # delete a field from the workspace
        elif inp.startswith('del '):
            field_deletes = command_del(inp, workspace, autocompletion)
            for field in field_deletes:
                if("[" in field):
                    parent, index = field.split("[")
                    index = int(index.replace("]", ""))
                    exec(f'{field} = None')
                    exec(f"if all(val is None for val in {parent}): del {parent}")
                else:
                    exec(f'del {field}')

        # do a quick plot
        elif inp.startswith('qplot '):
            command_qplot(inp, workspace)

        elif inp.startswith('qseries '):
            command_qseries(inp, workspace)

        elif inp.startswith('qssum '):
            command_qsteady_summary(inp, workspace)

        elif inp.startswith('qtime '):
            command_qtime(inp, workspace)

        elif inp.startswith('qtsum '):
            command_qtsum(inp, workspace)

        elif inp.startswith('qtqin '):
            command_qtqin(inp, workspace)

        elif inp.startswith('qsummary '):
            command_qsummary(inp, workspace)

        elif inp.startswith('qspace '):
            command_qspace(inp, workspace)

        elif inp.startswith('tablegen '):
            command_tablegen(inp)

        elif inp.startswith('export '):
            command_export(inp, workspace)

        # run the test suite
        elif inp.startswith('tests '):
            command_tests(inp, workspace)

        elif inp.startswith('design '):
            command_design(inp)

        # assign a new value to the workspace
        elif ':=' in inp:
            key, expr, error = command_assign(inp, workspace)
            if not error:
                try:
                    exec(f'{key} = {expr}')
                except Exception:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    print("Could not perform assignment")
                    traceback.print_exc()
                else:
                    if len(key.split('.')) == 1:
                        try:
                            workspace[key] = eval(f'{expr}')
                        except Exception:
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            traceback.print_exc()
                    if not key in autocompletion.words:
                        autocompletion.words.append(key)

        # no input
        elif inp == ' ':
            pass

        # other input, try to evaluate
        else:
            try:
                result = eval(inp)
                print(result)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print("command not understood, failed with traceback:")
                traceback.print_exc()

"""
export command for command line interface REPL

Saves a space-property distribution of the system at a snapshot in time
"""
import sys, traceback, os, shutil, numpy as np
from cli_utils import clean_input, progress_print

def command_export(inp, workspace):

    parse_input = clean_input(inp.split(" "))

    if len(parse_input) < 3:
        print(("Must specify time slice and save location as: "
            ">> export n /path/to/directory"))
        return

    try:
        time_slice = int(parse_input[1])

        # make sure the slices is in range
        max_time_slice = len(workspace["time"])
        if(time_slice < 0): time_slice = max_time_slice + time_slice

        if(time_slice < 0):
            print((f"time_slice {time_slice} out of range, maximum index is "
                f"{max_time_slice}"))

        save_location = os.path.abspath(parse_input[2])

    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return 

    flags = parse_input[3:]
    args = {"override":False}
    fields = []

    for flag in flags:
        if flag.startswith("-fields="):
            fields = flag.replace("-fields=","").split(",")
            continue

        args[flag[1:]] = True

    try:
        if os.path.exists(save_location):
            if args["override"]:
                shutil.rmtree(save_location)
            else:
                print((f"{save_location} already exists. Use flag -override to "
                    f"continue. This will delete the entire contents of "
                    f" {save_location}"))
                return
        os.makedirs(save_location, exist_ok=True)
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        return 

    # go through each field in the workspace and export the distribution at that
    # slice in time
    for key, value in workspace.items():

        if key == "time":
            continue

        if not hasattr(value,"node_coords"):
            continue

        if not fields:
            if not hasattr(value, "data_contents"):
                continue
            else:
                export_fields = value.data_contents
        
        else:
            export_fields = fields
        
        os.mkdir(os.sep.join([save_location, key]))
        for field in export_fields:
            if not hasattr(value, field):
                print(f"Result {key} does not have field {field}. Skipping export")
                continue

            data = np.empty((len(value.node_coords)*2), dtype = np.float64)
            data[:len(value.node_coords)] = value.node_coords
            data[len(value.node_coords):] = getattr(value, field)[time_slice,:]

            data.tofile(os.sep.join([save_location, key, field]))

        

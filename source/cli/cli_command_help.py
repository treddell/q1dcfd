"""
help command for command line interface REPL
"""

from cli_utils import clean_input
from coolprop_mappings import VALID_PHASES

HELP = {
    'main':('\ntype "help <topic>" for more help on a specific topic. The list '
        'of valid topics is: \n\n'
    '"run"     : simulate a .qdef definition file\n'
    '"load"    : load a results folder into the workspace\n'
    '"python"  : execute a snippet of python code \n'
    '"bash"    : execute a snippet of bash script \n'
    '"build"   : recompile q1dcfd with specified flags\n'
    '"dir"     : list the contents of the workspace, or the subcontents'
                ' of an object in the workspace \n'
    '"export"  : export the contents of the workspace at a snapshot in time as '
        'a set of (x, results) data\n' 
    '"del"     : delete a field from the workspace \n'
    '"assign"  : assign new variables within the workspace \n'
    '"qplot"   : quick plot some generic attributes \n'
    '"qtime"   : quick plot some fields vs time at select locations in space \n'
    '"qseries" : quick plot a spatial distribution at select points in time \n'
    '"qspace"  : quick plot a property space distribution at select points in '
        'time\n'
    '"tablegen": generate a set of tabular fluids data for a given fluid and '
        'parameter space \n'
    '"tests"   : run the test suite \n' 
    ),

    'invalid': ('\nInvalid help command, enter "help <topic>" for help on a'
        'specific topic, or just "help" for a list of topics \n'),

    'run': ('\nq1dcfd > run simulation.qdef \n \n'
        'will execute the simulation as defined by simulation.qdef. Results will'
        'be saved in q1dcfd/results and may be loaded into the workspace using '
        'the load command \n'),

    'build': ('\nq1dcfd > --compiler=<compiler> --build=<buildType> '
        '--config=<configuration> --extra-compile-flags \n \n'
        'will recompile q1dcfd with the specified flags. It will not change the'
        'default install options. Uses dub build behind the scenes\n \n'
        'Optional Arguments:\n'
        '--compiler            : specify which compiler to use\n'
        '--build               : specify a buildType, default is '
        'release-nobounds. Will recognise any valid dub input. Use debug for a '
        'debug build\n'
        '--config              : specify additional build configurations such as '
        '"quiet", "serial", "high_prec" etc. Configurations are specified from'
        'dub.json\n'
        '--extra_compile_flags : additional flags to pass to dub'),

    'load': ('\nq1dcfd > load </path/to/results/folder> \n \n'
        'will load the results data from the specified path into the workspace \n\n'
        'Optional Arguments:\n'
        '-keep_raw: If enabled, and if first time loading will not delete the '
        'raw binary data after structuring raw results into ordered fields'),

    'qplot': ('\nq1dcfd > qplot <xdata> <ydata1> <ydata2> ... \n\n'
        'plots on the same set of axis, every ydata vs xdata\n\n'
        'OptionalArguments\n'
        '-xlabel, -ylabel : plot labels\n'),

    'qtime': ('\nq1dcfd > qtime <attribute> <domain1> <domain2>... -args\n\n'
        'plots every domain.attribute vs time at select locations \n \n'
        'Optional Arguments\n'
        '-slices=n1,n2,... : a set of spatial indices for each domain. Will be '
        '0, -1 by default\n'
        '-xlabel, -ylabel    : plot labels\n'),

    'qseries': ('\nq1dcfd > qseries <attribute> <domain1> <domain2> ... -args \n \n'
        'plots a spatial distribution of <attribute> at points in time across each '
        'domain listed in series, ...\n \n'
    'Optional Arguments: \n'
    '-slices=n1,n2,n3... : a list of time indices to take distributions along\n'
    '-times=t1,t2,...    : a list of times to take distributions along, will '
    'select closest exact value\n'
    '-colour=c           : colour of produced plot\n'
    '-xlabel, -ylabel    : plot labels\n'),

    'qspace': ('\nq1dcfd > qpsace <input1> <input2> <domain1> <domain2> ... -args \n\n'
        'plots a property space distribution (input1, input) at select points in'
        'time for each domain \n\n'
        'Required Arguments\n'
        'input1, input2       : the property space parameters, eg. T s\n'
        'domain1, domain2 ... : a listing of domains to include in the property'
        ' space plot\n'
        'OptionalArguments\n'
        '-slices=n1,n2        : discrete points in time to plot property space '
        'distribution along\n'
        '-xlabel, -ylabel    : plot labels\n'),

    'exit': '\nq1dcfd > exit, will exit and return to the terminal \n',

    'python': ('\nq1dcfd > python <executable python code> \n\n'
        'will execute <executable python code> using the default python version. \n'),

    'bash': ('\nq1dcfd > bash <executable bash script> \n\n'
        'will execute <executable shell script> using the os shell and '
        'display the output. Will accept any valid shell command \n'),

    'dir': ('\nq1dcfd > dir \n\n'
        'Lists the contents of the workspace\n\n'
        'q1dcfd> dir <object>\n\n'
        'Lists the  subcontents of <object> in the workspace \n\n'),

    'del': ('\nq1dcfd > del <item1> <item2> ... \n\n'
            'Delete all listed items from the workspace. Can delete both fields'
            'and attributes of fields (denoted as del <field.attribute>).\n\n'
            '> del WORKSPACE : deletes the entire contents of the workspace\n\n'
            'EXERCISE CAUTION: Once deleted, the item is gone'
            ' until it is re-loaded or recreated. Does not delete data from the'
            ' results file. \n'),

    'assign': ('\nq1dcfd > <var> := <value> \n\n'
        'Assign a new variable <var> with the value of <value> and adds it to '
        'the workspace. Examples: \n\n'
        'x := 2 \ny := 3 \nz := x*y \n \n'
        'Can assign using any data structure with defined operators. For '
        'example, if we have a results class "flow" with fields "density", '
        '"pressure", "temperature", "gas_constant". Then we can define a new '
        'variable "flow.Z" as: \n\n'
        'flow.Z := flow.pressure/(flow.density*flow.temperature*'
        'flow.gas_constant) \n \n'
        'New variables can be used in all other valid q1dcfd commands '
        '\n'),

    'tablegen': ('\nq1dcfd > tablegen <name> <fluid> p1:p2 T1:T2 --args\n\n'
        'Generates a database of density-energy tabular data. '
        'Table will be saved in ./q1dcfd/tables/name \n\n'
        'Required Arguments:\n'
        '<name>                           : unique identifier for the table '
            'dataset \n'
        '<fluid>                          : fluid string, must be a valid '
            'CoolProp fluid \n'
        'p1:p2                            : min-pressure, max-pressure \n'
        'T1:T2                            : min-temperature, max-temperature\n\n'
        'Optional arguments\n'
        '--exclude_phase=phase1,phase2... : list of fluid phases to disregard '
            'twophase by default, use --exclude_phase= to include two phase flow'
            f' valid phase inputs are {VALID_PHASES} \n' 
        '--tol_consistency=1.0E-2         : maximum value of consistency error '
            'to tolerate. Values above this threshold will be marked as a failed'
            ' calculation\n'
        '--warn_consistency=1.0E-5        : minimum value of consistency error to '
            'trigger a warning to the user, calculation will be accepted\n'
        '--ignore_failure=False           : If True, will not attempt to patch '
            'convergence failures\n'
        '--N1                             : number of steps along axis 1\n'
        '--N2                             : number of steps along axis 2\n'),

    'tests': ('\nq1dcfd > tests -compiler=<compiler> -args\n\n'
        'Runs the test suite, recommended to do so after making any changes to '
        'The q1dcfd application.\n\n'
        'Optional Arguments:\n'
        '-compiler=dmd, ldc, gdc : specify which compiler to compile with\n'
        '-args                   : any valid flags to pass to the compiler\n'
    ),
    'export': ('\nq1dcfd > export n /path/to/save/directory -args\n\n'
        'export the state of the workspace at a particular point in time to '
        'a sub-directory of binary files of (x, property) distributions. Will '
        'export every property which belongs to an object with a node_coords '
        'field and which is within the data_contents field. Or, every property '
        '(if it exists) given by the -fields flag\n\n'
        'Required Arguments\n'
        '<n>                     : time index to export, accepts backward '
            'indexing\n'
        '/path/to/save/directory : Directory results will be saved in. Must not '
            'already exist (unless -override flag is set)\n'
        'Optional Arguments\n'
        '-fields=y1,y2,...       : A set of properties to export from every '
            'object in the workspace. If the object does not have a node_coords '
            ' attribute, or the given field then it will be ignored. If not '
            'specified will export every property in the data_contents field.\n'
        '-override               : By default, will not proceed if '
            '/path/to/save/directory exists, if this flag is specified, it will '
            'override the entire contents of the given directory, use with '
            'caution\n')
}

def command_help(inp):
    "parse the input string and provide the corresponding help info"
    parse_input = clean_input(inp.split(' '))
    n_entries = len(parse_input)

    if n_entries == 1:
        print(HELP['main'])
    elif(
            n_entries == 2
        and parse_input[1] in HELP
    ):
        print(HELP[parse_input[1]])
    else:
        print(HELP['invalid'])

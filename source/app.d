module q1dcfd.app ;

import q1dcfd.initialisation.parser: parse, process_input_args, squash ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.quick_cycle.app_interface: run_quick_cycle_design ;

import std.stdio ;
import core.stdc.stdlib: exit ;
version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

@fastmath void run_application(string[] args)
{
    if(args.length < 2)
    {
        writeln("No qdef file specified") ;
        return ;
    }

    // start initialisation timer
    version(unittest) {}
    else
    {
        import std.datetime.stopwatch: StopWatch, AutoStart;
        import core.time ;
        auto stopwatch = StopWatch(AutoStart.yes);
    }

    // get the optional arguments
    auto optional_args = process_input_args(args[2..args.length]) ;
    
    // Read and initialise the definition file
    Simulation.initialise(args[1].squash.parse) ;
    
    // stop the initialisation timer
    version(unittest) {}
    else
    {
        stopwatch.stop ;
        auto time_elapsed = stopwatch.peek.total!"msecs" ;
        double seconds = time_elapsed*1.0E-3;
        writefln("Simulation initialised in %.3f seconds", seconds) ;
    }

    if(optional_args.mode == "inspect-only")
    // if --mode=inspect-only flag passed in then will not simulate
    {
        
    }
    else if(optional_args.mode == "steady")
    // if --mode=steady flag passed in then will only perform psuedo 
    // time marching
    // to steady state 
    {
        Simulation.steady ;
        auto data_file = File(Simulation.results_folder ~ "data", "w") ;
        Simulation.write_to_disk(data_file, 0.0) ;
    }
    else if(optional_args.mode == "direct")
    // will perform transient simulation from provided initial state instead of 
    // iterating until steady state in psuedotime
    {
        Simulation.simulate ;
    }
    else if(optional_args.mode == "transient")
    // Perform pseduo time marching to steady state, then dynamic time marching
    // until termination
    {
        Simulation.steady ;
        Simulation.simulate ;
    }
}

@fastmath void main(string[] args)
{
    // if running tests
    version(unittest)
    {
        import tests_main: run_tests ;
        run_tests ;
        return ;
    }
    else
    {
        // otherwise running an application
        if(args.length < 2)
        {
            writeln("Application not specified") ;
            return ;
        }

        auto app = args[1] ;

        switch(app)
        {
            // the main application, transient simulation code
            case "q1dcfd":
            {
                run_application(args[1..$]) ;
                break ;
            }
            // minor application, quick cycle design code
            case "cycle-design":
            {
                run_quick_cycle_design(args[2..$]) ;
                break ;
            }
            default:
            {
                writefln("Mode %s not a valid application", app) ;
                return ;
            }
        }
    }
}

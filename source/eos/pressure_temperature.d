module q1dcfd.eos.pressure_temperature ;

import q1dcfd.eos.abstract_eos ;
import q1dcfd.tables.extract_table_data: TableData ;

import std.format ;

/**
 * Equation of state backend using pressure and temperature as the independent variables
 *
 * Used only as an intermediate step to constructing a density-energy state. As a result
 * currently contains no fields except pressure and temperature. This may change if a
 * pressure temperature based model is used
 */
final class PressureTemperatureEOS(backend back): EOS!(back)
{
    this(immutable(TableData) table, immutable string id = "")
    {
        import std.exception: enforce ;
        import q1dcfd.utils.errors: MismatchedTable ;

        enforce!MismatchedTable(table.i1_str == "p" || table.i2_str == "T",
                                "expected inputs p, T but found %s, %s"
                                .format(table.i1_str, table.i2_str)) ;

        enforce!MismatchedTable(table.output_vars == ["rho", "e"],
                                "expected output variables rho,e but found %(%s, %)"
                                .format(table.output_vars)) ;

        super(table, id) ;
    }

    override @property
    {
        //static pressure (Pa)
        double p() {return i1 ;}

        // Temperature (K)
        double T() {return i2 ;}

        // mass density (kg/m3)
        double rho()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(0) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.rhomass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // specific internal energy (J/kg)
        double e()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(1) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.umass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }
    }

    static if(back.using_direct_eos)
    {
        override void update_from_coolprop(const double i1, const double i2)
        {
            import coolprop: PT_INPUTS ;
            coolprop_eos.update(PT_INPUTS, i1, i2) ;
        }
    }
}

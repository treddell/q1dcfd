module q1dcfd.eos.generate_eos ;

import q1dcfd.eos ;
import q1dcfd.utils.errors: MismatchedTable ;
import q1dcfd.tables.extract_table_data: TableData ;

import std.format ;

/**
 * Generates an equation of state based upon a backend type, input pair basis
 * and the tabular data used
 */
EOSBase generate_eos(
    immutable(TableData) table,
    backend back,
    immutable string id)
{
    auto basis = table.i1_str ~ "-" ~ table.i2_str ;
    switch(basis)
    {
        case "rho-e":
        {
            final switch(back)
            {
                case backend.coolprop:
                {
                    return new DensityEnergyEOS!(backend.coolprop)(table, id) ;       
                }
                case backend.TTSE:
                {
                    return new DensityEnergyEOS!(backend.TTSE)(table, id) ;       
                }
                case backend.TTSE_recovery:
                {
                    return new DensityEnergyEOS!(backend.TTSE_recovery)(table, id) ;       
                }
                case backend.bicubic:
                {
                    return new DensityEnergyEOS!(backend.bicubic)(table, id) ;       
                }
                case backend.bicubic_recovery:
                {
                    return new DensityEnergyEOS!(backend.bicubic_recovery)(table, id) ;       
                }
            }
        }
        case "p-T":
        {
            final switch(back)
            {
                case backend.coolprop:
                {
                    return new PressureTemperatureEOS!(backend.coolprop)(table, id) ;
                }
                case backend.TTSE:
                {
                    return new PressureTemperatureEOS!(backend.TTSE)(table, id) ;
                }
                case backend.TTSE_recovery:
                {
                    return new PressureTemperatureEOS!(backend.TTSE_recovery)(table, id) ;
                }
                case backend.bicubic:
                {
                    return new PressureTemperatureEOS!(backend.bicubic)(table, id) ;
                }
                case backend.bicubic_recovery:
                {
                    return new PressureTemperatureEOS!(backend.bicubic_recovery)(table, id) ;
                }
            }
        }
        case "p-s":
        {
            final switch(back)
            {
                case backend.coolprop:
                {
                    return new PressureEntropyEOS!(backend.coolprop)(table, id) ;
                }
                case backend.TTSE:
                {
                    return new PressureEntropyEOS!(backend.TTSE)(table, id) ;
                }
                case backend.TTSE_recovery:
                {
                    return new PressureEntropyEOS!(backend.TTSE_recovery)(table, id) ;
                }
                case backend.bicubic:
                {
                    return new PressureEntropyEOS!(backend.bicubic)(table, id) ;
                }
                case backend.bicubic_recovery:
                {
                    return new PressureEntropyEOS!(backend.bicubic_recovery)(table, id) ;
                }
            }
        }
        default:
        {
            throw new MismatchedTable(
                "Provided table does not match expected input pairs in %s, found %s"
                .format(id, basis)) ;
        }
    }
}


module q1dcfd.eos.abstract_eos ;

import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.utils.math: square ;
import coolprop ;

import std.format ;
import std.conv: to ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}
@fastmath:

// A collection of permissable backend types
enum backend
{
    coolprop = 0,
    TTSE,
    TTSE_recovery,
    bicubic,
    bicubic_recovery
}

// Some attributes of the backend to assist with classification
bool using_table(backend back)
{
    return (back == backend.coolprop) ? false : true ;
}

bool using_ttse(backend back)
{
    return (   back == backend.TTSE
            || back == backend.TTSE_recovery) ? true : false ;
}

bool using_direct_eos(backend back)
{
    return (   back == backend.TTSE_recovery
            || back == backend.bicubic_recovery
            || back == backend.coolprop) ? true : false ;
}

bool using_bicubic(backend back)
{
    return (   back == backend.bicubic
            || back == backend.bicubic_recovery) ? true : false ;
}

// possible exit states of the update method
enum exit_code
{
    success = 0,
    out_of_bounds,
    invalid_point,
    coolprop_exception,
    other,
}

/**
 * Base class for a fluid equation of state update method
 *
 * Method "update" will lazily evaluate the equation of state at a specified
 * point. Properties may then be extracted depending upon the EOS type
 *
 * Acts as a callable interface for a generic equation of state
 */
interface EOSBase
{
    @property
    {
        double rho() ; // density (kg/m3)
        double e() ; // specific internal energy (J/kg)
        double p() ; // static pressure (Pa)
        double h() ; // static enthalpy (J/kg)
        double T() ; // total temperature (K)
        double cp() ; // specific heat capacity, const pressure (J/kg.K)
        double cv() ; // specific heat capacity, const volume (J/kg.K)
        double dv() ; // dynamic viscosity (kg/s.m)
        double k() ; // thermal conductivity (W/m.K)
        double a() ; // speed of sound (m/s)
        double s() ; // entropy (J/kg.K)
        double x() ;// vapour fraction (-)
        double dTdD() ; // partial derivative of temperature w.r.t density, constant energy
        double dTde() ; // partial derivative of temperature w.r.t energy, constant density
        double dhdp() ; // partial derivative d(h)/d(p)|s
        double dhdD() ; // partial derivative of enthalpy w.r.t density, constant energy
        double dhde() ; // partial derivative of enthalpy w.r.t energy, constant density
    }

    /**
     * Main equation of state update method
     */
    void update(const double i1, const double i2) ;

    /**
     * return the error state of the equation of state
     */
    exit_code get_error_state() ;

    /**
     * Return the backend enum type
     */
    backend get_backend() ;

    /**
     * return a reference to the tabular data
     */
    immutable(TableData) get_tabular_data() ;
}

/**
 * Implements the Equation of state with a specified backend
 */
abstract class EOS(backend back): EOSBase
{
    protected
    {
        // input points
        double i1 ;
        double i2 ;

        exit_code err_state ;
        immutable string id ;

        // The tabular data set is linked even if not used
        immutable TableData table ;
    }

    // Getters for equation of state data and attributes
    exit_code get_error_state() {return err_state ;}
    backend get_backend() {return back ;}
    immutable(TableData) get_tabular_data() {return table ;}

    // Assign all properties to default invalid values
    @property
    {
        import q1dcfd.utils.errors: PropertyNotImplemented ;
        double rho() {throw new PropertyNotImplemented("rho",typeid(this).toString) ;}
        double e() {throw new PropertyNotImplemented("e", typeid(this).toString) ;}
        double p() {throw new PropertyNotImplemented("p", typeid(this).toString) ;}
        double h() {throw new PropertyNotImplemented("h", typeid(this).toString) ;}
        double T() {throw new PropertyNotImplemented("T", typeid(this).toString) ;}
        double cp() {throw new PropertyNotImplemented("cp", typeid(this).toString) ;}
        double cv() {throw new PropertyNotImplemented("cv", typeid(this).toString) ;}
        double dv() {throw new PropertyNotImplemented("dv", typeid(this).toString) ;}
        double k() {throw new PropertyNotImplemented("k", typeid(this).toString) ;}
        double a() {throw new PropertyNotImplemented("a", typeid(this).toString) ;}
        double s() {throw new PropertyNotImplemented("s", typeid(this).toString) ;}
        double x() {throw new PropertyNotImplemented("x", typeid(this).toString) ;}
        double dTdD() {throw new PropertyNotImplemented("dTdD", typeid(this).toString) ;}
        double dTde() {throw new PropertyNotImplemented("dTde", typeid(this).toString) ;}
        double dhdp() {throw new PropertyNotImplemented("dhdp", typeid(this).toString) ;}
        double dhdD() {throw new PropertyNotImplemented("dhdD", typeid(this).toString) ;}
        double dhde() {throw new PropertyNotImplemented("dhde", typeid(this).toString) ;}
    }

    // direct equation of state calculator
    static if(back.using_direct_eos)
    {
        protected
        {
            /*
             * Updates the state directly using CoolProp. Usually used
             * as a backup option following a calculation failure
             */
            AbstractState coolprop_eos ;

            void update_from_coolprop(const double i1, const double i2) ;
        }
    }

    static if(back.using_ttse)
    {
        protected
        {
            int n1 ; // input 1 interval
            int n2 ; // input 2 interval
            int m ; //flattened table row index
            double i1_0 ; // nearest data point (input 1)
            double i2_0 ; // nearest data point (input 2)
            double delta_i1 ; // i1 - i1_0
            double delta_i2 ; // i2 - i2_0
            double c1, c2, c3 ; // second order coefficient

            /**
             * Generate a tabular taylor series extrapolating polynomial
             *
             *  Approximates a set of fluid properties as defined by a TableData class
             *  using the approximation
             *
             *  X(I1, I2) ~= X(I1_0, I2_0)
             *      + dI1 * d(X(I1_0, I2_0))/(d (I1))
             *      + dI2 * d(X(I1_0, I2_0))/(d (I2))
             *      + 0.5*dI1^2 * d^2(X(I1_0, I2_0))/(d (I1)^2)
             *      + 0.5*dI2^2 * d^2(X(I1_0, I2_0))/(d (I2)^2)
             *      + dI1*dI2 * d^2(X(I1_0, I2_0))/(d(I1) d(I2))
             *
             *  where I1_0, I2_0 are the closest table values to I1, I2
             *
             *  Can also retrieve a first partial derivative with a first order estimate
             *
             *  d(X(I1,I2))/d(I1) ~= d(X(I1_0,I2_0))/d(I1)
             *      + dI1 * d^2(X(I1_0, I2_0))/d(I1)^2
             *      + dI2 * d^2(X(I1_0, I2_0))/(d(I1) * d(I2))
             *
             *  d(X(I1,I2))/d(I2) ~= d(X(I1_0,I2_0))/d(I2)
             *      + dI2 * d^2(X(I1_0, I2_0))/d(I2)^2
             *      + dI1 * d^2(X(I1_0, I2_0))/(d(I1) * d(I2))
             *
             */
            exit_code update_from_table(const double i1, const double i2) pure nothrow
            {
                // determine the nearest grid point, and the 1D index of the property block
                n1 = cast(int) ((i1 - table.i1_lowest)*table.i1_step_rec + 0.50000001) ;
                n2 = cast(int) ((i2 - table.i2_lowest)*table.i2_step_rec + 0.50000001) ;
                m = (n1*table.n2 + n2)*table.n_columns ;

                // make sure the point is not out of bounds
                if(n1 < 0 || n2 < 0 || n1 >= table.n1 || n2 >= table.n2)
                {
                    return exit_code.out_of_bounds ;
                }

                // make sure the selected grid point is consistent
                if(table.data[m] < 0) return exit_code.invalid_point ;

                // calculate the coefficients of the taylor series
                i1_0 = table.i1_lowest + n1*table.i1_step ;
                i2_0 = table.i2_lowest + n2*table.i2_step ;
                delta_i1 = i1 - i1_0 ;
                delta_i2 = i2 - i2_0 ;
                c1 = 0.5*delta_i1.square ;
                c2 = 0.5*delta_i2.square ;
                c3 = delta_i2*delta_i1 ;

                return exit_code.success ;
            }

            /**
             * retrieves the requested output property from the table, using the
             * current input properties
             */
            double prop(const ulong prop_index) pure nothrow
            {
                ulong j6 = m + 6*prop_index + 1 ;

                double x = table.data[j6] ;
                double dx_i1 = table.data[j6 + 1] ;
                double dx_i2 = table.data[j6 + 2] ;
                double ddx_i1 = table.data[j6 + 3] ;
                double ddx_i2 = table.data[j6 + 4] ;
                double ddx_i1i2 = table.data[j6 + 5] ;

                return x + delta_i1*dx_i1 + delta_i2*dx_i2 + c1*ddx_i1
                    + c2*ddx_i2 + c3*ddx_i1i2 ;
            }

            /**
             * Retrieve a partial derivative of one of the output variables to a first
             * order estimate. Input 'partial' must be either 1 (for derivative with
             * respect to input variable 1), or 2 (derivative with respect to input
             * variable 2)
             */
            double der(const ulong prop_index, const ulong partial) pure nothrow
            {
                ulong j6 = m + 6*prop_index + 1 ;

                double x = table.data[j6 + partial] ;
                double dx_i1 = table.data[j6 + 2*partial + 1] ;
                double dx_i2 = table.data[j6 + 6 - partial] ;

                return x + delta_i1*dx_i1 + delta_i2*dx_i2 ;
            }
        }
    }
    else static if(back.using_bicubic)
    {
        protected
        {
            int n1;  // input 1 interval (closest point below i1)
            int n2;  // input 2 interval (closest point below i2)
            int m00; // flattened table row indices
            int m01;
            int m10;
            int m11;
            double i1_0; // nearest point below (input 1)
            double i2_0; // nearest point below (input 2)
            double i1_1; // nearest point above (input 1)
            double i2_1; // nearest point above (input 2)
            double delta_i1; // i1 - i1_0
            double delta_i2; // i2 - i2_0
            immutable double[][] Ainv = [
                [ 1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
                [ 0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
                [-3,  3,  0,  0, -2, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
                [ 2, -2,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
                [ 0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0],
                [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0],
                [ 0,  0,  0,  0,  0,  0,  0,  0, -3,  3,  0,  0, -2, -1,  0,  0],
                [ 0,  0,  0,  0,  0,  0,  0,  0,  2, -2,  0,  0,  1,  1,  0,  0],
                [-3,  0,  3,  0,  0,  0,  0,  0, -2,  0, -1,  0,  0,  0,  0,  0],
                [ 0,  0,  0,  0, -3,  0,  3,  0,  0,  0,  0,  0, -2,  0, -1,  0],
                [ 9, -9, -9,  9,  6,  3, -6, -3,  6, -6,  3, -3,  4,  2,  2,  1],
                [-6,  6,  6, -6, -3, -3,  3,  3, -4,  4, -2,  2, -2, -2, -1, -1],
                [ 2,  0, -2,  0,  0,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0,  0],
                [ 0,  0,  0,  0,  2,  0, -2,  0,  0,  0,  0,  0,  1,  0,  1,  0],
                [-6,  6,  6, -6, -4, -2,  4,  2, -3,  3, -3,  3, -2, -1, -2, -1],
                [ 4, -4, -4,  4,  2,  2, -2, -2,  2, -2,  2, -2,  1,  1,  1,  1]
            ];

            /*
             * Bicubic interpolation involves finding all the points that neighbour the
             * target point x. The indices of these points m00, m01, m10, m11 are as
             * shown in the diagram below.
             *
             *  ^   m01        m11
             *  |
             *  |
             *  |       x
             *  |
             * i2   m00        m10
             *
             *     i1 ------>
             */
            exit_code update_from_table(const double i1, const double i2) pure nothrow
            {
                // Determine the point in the bottom-left corner of the
                // rectangle that contains (i1, i2)
                n1 = cast(int) ((i1 - table.i1_lowest)*table.i1_step_rec);
                n2 = cast(int) ((i2 - table.i2_lowest)*table.i2_step_rec);

                // This gets the location in the tables of the point (i1, i2)
                m00 = (n1*table.n2 + n2)*table.n_columns;
                m01 = (n1*table.n2 + n2+1)*table.n_columns;
                m10 = ((n1+1)*table.n2 + n2)*table.n_columns;
                m11 = ((n1+1)*table.n2 + n2+1)*table.n_columns;

                // Make sure the points are not out of bounds
                if(n1 < 0 || n2 < 0 || n1 >= table.n1-1 || n2 >= table.n2-1)
                {
                    return exit_code.out_of_bounds;
                }

                // make sure the selected points are consistent
                if(   table.data[m00] < 0 || table.data[m01] < 0
                   || table.data[m10] < 0 || table.data[m11] < 0)
                {
                    return exit_code.invalid_point;
                }

                i1_0 = table.i1_lowest + n1*table.i1_step;
                i2_0 = table.i2_lowest + n2*table.i2_step;
                i1_1 = table.i1_lowest + (n1+1)*table.i1_step;
                i2_1 = table.i2_lowest + (n2+1)*table.i2_step;
                delta_i1 = (i1 - i1_0) / (i1_1 - i1_0);
                delta_i2 = (i2 - i2_0) / (i2_1 - i2_0);

                return exit_code.success;
            }

            /**
             * Calculation procedure for bicubic is as follows:
             *  - Find the vector
             *      x = [f, fx, fy, fxy]
             *    where
             *      f = [f00, f10, f01, f11],
             *    and similar for derivative vectors fx, fy, fxy.
             *
             *  - Find the coefficients of the bicubic interpolant by computing
             *      alpha = A^-1 x
             *    where A^-1 is a known constant matrix, and alpha takes the
             *    form
             *      alpha = [a00, a10, a20, a30, a01, a11, a21, a31, ...
             *               a02, a12, a22, a32, a03, a13, a23, a33].
             *
             *  - Compute the value of the desired property as
             *      p(i1,i2) = sum a_jk * delta_i1^j * delta_i2^k
             *    for all j,k (values 0 to 3 inclusive).
             */
            double prop(const ulong prop_index) pure nothrow
            {
                import std.math: pow;

                ulong j600 = m00 + 6*prop_index + 1;
                ulong j610 = m10 + 6*prop_index + 1;
                ulong j601 = m01 + 6*prop_index + 1;
                ulong j611 = m11 + 6*prop_index + 1;

                // Assemble x vector
                double[16] x;
                x[0]  = table.data[j600];
                x[1]  = table.data[j610];
                x[2]  = table.data[j601];
                x[3]  = table.data[j611];
                x[4]  = table.data[j600 + 1] * table.i1_step;
                x[5]  = table.data[j610 + 1] * table.i1_step;
                x[6]  = table.data[j601 + 1] * table.i1_step;
                x[7]  = table.data[j611 + 1] * table.i1_step;
                x[8]  = table.data[j600 + 2] * table.i2_step;
                x[9]  = table.data[j610 + 2] * table.i2_step;
                x[10] = table.data[j601 + 2] * table.i2_step;
                x[11] = table.data[j611 + 2] * table.i2_step;
                x[12] = table.data[j600 + 5] * table.i1_step * table.i2_step;
                x[13] = table.data[j610 + 5] * table.i1_step * table.i2_step;
                x[14] = table.data[j601 + 5] * table.i1_step * table.i2_step;
                x[15] = table.data[j611 + 5] * table.i1_step * table.i2_step;

                // Find coefficients by finding Ainv*x
                double[16] alpha;
                foreach(k; 0..16)
                {
                    double sum = 0;
                    foreach(l; 0..16) { sum += Ainv[k][l] * x[l]; }
                    alpha[k] = sum;
                }

                // To get alpha_ij we use alpha[4*j + i]
                // Compute the property estimate by summing
                double out_prop = 0;
                foreach(i; 0..4) {
                    foreach(j; 0..4) {
                        out_prop += alpha[4*j + i] * delta_i1.pow(i) * delta_i2.pow(j);
                    }
                }

                return out_prop;
            }

            /**
             * Retrieve a partial derivative of one of the output variables to a first
             * order estimate. Input 'partial' must be either 1 (for derivative with
             * respect to input variable 1), or 2 (derivative with respect to input
             * variable 2)
             */
            double der(const ulong prop_index, const ulong partial) // pure nothrow
            {
                throw new Exception("der not implemented for bicubic backend.");
                // ulong j6 = m + 6*prop_index + 1 ;

                // double x = table.data[j6 + partial] ;
                // double dx_i1 = table.data[j6 + 2*partial + 1] ;
                // double dx_i2 = table.data[j6 + 6 - partial] ;

                // return x + delta_i1*dx_i1 + delta_i2*dx_i2 ;
            }
        }
    }
    // stub for future implementation
    else static if(back.using_table)
    {
        double prop(const ulong prop_index) pure nothrow {return -1 ;}
        double der(const ulong prop_index, const ulong partial) pure nothrow {return -1 ;}
        exit_code update_from_table(const double i1, const double i2) pure nothrow
        {
            return exit_code.other ;
        }
    }

    void update(const double i1, const double i2)
    {
        string err_msg = "" ;
        this.i1 = i1 ;
        this.i2 = i2 ;

        // first run will attempt to calculate using a table if enabled, else skip
        static if(back.using_table)
        {
            // attempt a tabular update
            err_state = update_from_table(i1, i2) ;

            final switch(err_state)
            {
                case exit_code.success: return ;

                    // calculation failure
                case exit_code.out_of_bounds:
                {
                    err_msg = "(%s) Inputs (%f,%f) outside of table bounds [%f, %f), [%f,%f)"
                        .format(id, i1, i2, table.i1_min, table.i1_max,
                                table.i2_min, table.i2_max) ~ err_msg ;
                    break ;
                }
                case exit_code.invalid_point:
                {
                    err_msg = ("(%s) Inputs (%f, %f) correspond to "
                        ~"undefined table entry, point is an invalid phase or "
                        ~"inconsistent").format(id, i1, i2) ;
                    break ;
                }
                case exit_code.coolprop_exception:
                {
                    assert(false, "Not possible to throw from this point") ;
                }
                case exit_code.other:
                {
                    err_msg = ("An unknown error occured with inputs %f, %f"
                               .format(i1, i2)) ;
                    break ;
                }
            }
        }

        // This code segment compile in if using the direct equation of state
        // calculator, or if a tabular update fails and recovery is enabled
        static if(back.using_direct_eos)
        {
            try
            {
                update_from_coolprop(i1, i2) ;
                return ;
            }
            catch(CoolPropException e)
            {
                err_msg ~= "\nAttempt to recover using CoolProp failed with "
                ~ " traceback %s".format(e.msg) ;
                err_state = exit_code.coolprop_exception ;
            }
        }

        // if we get here then all attempts to update the state have failed
        throw new Error(err_msg) ;
    }

    /*
     * Generate the constructor
     */
    this(immutable(TableData) table, immutable string id = "")
    {
        this.table = table ;
        this.id = id ;
        this.err_state = exit_code.success;

        static if(back.using_direct_eos)
        {
            this.coolprop_eos = new AbstractState("HEOS", table.fluid_str) ;
        }
    }
}

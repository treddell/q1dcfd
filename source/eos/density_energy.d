module q1dcfd.eos.density_energy ;

import q1dcfd.eos.abstract_eos ;
import q1dcfd.tables.extract_table_data: TableData ;
import coolprop ;

import std.format ;

/**
 * Equation of state backend using density and internal energy as the independent
 * variables
 *
 * Calculates the pressure, enthalpy, temperature, specific heat, dynamic viscosity,
 * entropy and vapour fraction using the specified backend model
 *
 * All properties are lazily evaluated and not cached, hence they should be cast to
 * intermediate variables if they are used multiple times at the same state
 */
final class DensityEnergyEOS(backend back): EOS!(back)
{
    this(immutable(TableData) table, immutable string id = "")
    {
        import std.exception: enforce ;
        import q1dcfd.utils.errors: MismatchedTable ;

        enforce!MismatchedTable(table.i1_str == "rho" || table.i2_str == "e",
                                "expected inputs rho, e but found %s, %s"
                                .format(table.i1_str, table.i2_str)) ;

        enforce!MismatchedTable(table.output_vars == ["h", "T", "Cp", "mu", "k", "a", "Q", "s", "Cv"],
                                "expected output variables h, T, Cp, mu, k, a, Q, s, Cv, but "
                                ~"found %(%s, %)"
                                .format(table.output_vars)) ;

        super(table, id) ;
    }
    
    override @property
    {
        // density (kg/m3)
        double rho() { return i1 ;}

        // specific internal energy (J/kg)
        double e() { return i2 ;}

        // static pressure (Pa)
        double p() { return rho*(h - e) ;}

        // static enthalpy (J/kg)
        double h()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(0) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.hmass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // total temperature (K)
        double T()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(1) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.T ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // constant-pressure specific heat capacity (J/kg.K)
        double cp()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(2) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.cpmass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // constant-volume specific heat capacity (J/kg.K)
        double cv()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(8) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.cvmass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // dynamic viscosity (kg/s.m)
        double dv()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(3) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.viscosity ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // thermal conductivity (W/m.K)
        double k()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(4) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.conductivity ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // speed of sound (m/s)
        double a()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(5) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.speed_sound ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // entropy (J/kg.K)
        double s()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(7) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.smass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // vapour fraction (-)
        double x()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(6) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.Q ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

         // partial derivative of temperature w.r.t density, constant energy
        double dTdD()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return der(1, 1) ;
            }
            static if(back.using_direct_eos)
            {
                return coolprop_eos.first_partial_deriv(iT, iDmass, iUmass) ;
            }
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // partial derivative of temperature w.r.t internal energy, constant density
        double dTde()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return der(1, 2) ;
            }
            static if(back.using_direct_eos)
            {
                return coolprop_eos.first_partial_deriv(iT, iUmass, iDmass) ;
            }
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // partial derivative of enthalpy w.r.t density, constant energy
        double dhdD()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return der(0, 1) ;
            }
            static if(back.using_direct_eos)
            {
                return coolprop_eos.first_partial_deriv(iHmass, iDmass, iUmass) ;
            }
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // partial derivative of enthalpy w.r.t internal energy, constant density
        double dhde()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return der(0, 2) ;
            }
            static if(back.using_direct_eos)
            {
                return coolprop_eos.first_partial_deriv(iHmass, iUmass, iDmass) ;
            }
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }
    }

    static if(back.using_direct_eos)
    {
        override void update_from_coolprop(const double i1, const double i2)
        {
            import coolprop: DmassUmass_INPUTS ;
            coolprop_eos.update(DmassUmass_INPUTS, i1, i2) ;
        }
    }
}


module q1dcfd.eos.pressure_entropy ;

import q1dcfd.eos.abstract_eos ;
import q1dcfd.tables.extract_table_data: TableData ;
import coolprop ;

import std.format ;

/**
 * Equation of state backend using pressure and entropy as the independent variables
 *
 * Used to calculate the isentropic state across a momentum energy discontinuity
 * Only required variables are enthalpy and dh/dp
 */
final class PressureEntropyEOS(backend back): EOS!(back)
{
    this(immutable(TableData) table, immutable string id = "")
    {
        import std.exception: enforce ;
        import q1dcfd.utils.errors: MismatchedTable ;

        enforce!MismatchedTable(table.i1_str == "p" || table.i2_str == "s",
                                "expected inputs p, s but found %s, %s"
                                .format(table.i1_str, table.i2_str)) ;

        enforce!MismatchedTable(table.output_vars == ["h", "rho"],
                                "expected output variables h, rho but found %(%s, %)"
                                .format(table.output_vars)) ;

        super(table, id) ;
    }

    override @property
    {
        //static pressure (Pa)
        double p() {return i1 ;}

        // Entropy (J/kg.K)
        double s() {return i2 ;}

        // enthalpy (J/kg)
        double h()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(0) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.hmass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        double rho()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return prop(1) ;
            }
            static if(back.using_direct_eos) return coolprop_eos.rhomass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // internal energy (J/kg)
        double e()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) {
                    return h - (p / rho) ;
                }
            }
            static if(back.using_direct_eos) return coolprop_eos.hmass ;
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }

        // partial derivative d(h)/d(p)|s
        double dhdp()
        {
            static if(back.using_table)
            {
                if(err_state == exit_code.success) return der(0, 1) ;
            }
            static if(back.using_direct_eos)
            {
                return coolprop_eos.first_partial_deriv(iHmass, iP, iSmass) ;
            }
            else throw new Error("%s is in invalid state with error %u".format(id, err_state)) ;
        }
    }

    static if(back.using_direct_eos)
    {
        override void update_from_coolprop(const double i1, const double i2)
        {
            import coolprop: PSmass_INPUTS ;
            coolprop_eos.update(PSmass_INPUTS, i1, i2) ;
        }
    }
}

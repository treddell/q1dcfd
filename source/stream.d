module q1dcfd.stream;

/*
 * Stream structure, tracks the mass and energy content of the stream
 * throughout all the domains it passes through. Assumes a continuous flow
 * Can write system properties to disk
 */
final class Stream 
{
    import q1dcfd.domains: Domain;
    import q1dcfd.types: Real, SystemProperty;
    import q1dcfd.tables: TableData;
    import q1dcfd.control_volumes: FluidNode;
    import q1dcfd.simulation: Simulation;
    import q1dcfd.utils.metaprogramming: from;

    import std.algorithm.iteration: each;

    private 
    {
        // Internal variable state
        Real _mass;
        Real _volume = 0.0;
        Real _energy;

        // flags to check if variables are cached
        bool _energy_is_updated = false;
        bool _mass_is_updated = false;

        // address of stored properties
        long mass_index = -1;
        long energy_index = -1;
        long p_avg_index = -1;
        long T_avg_index = -1;
        long traversal_time_index = -1;

        // store a list of cells within the stream
        FluidNode[] nodes; 
    }

    const
    {
        // human readable name of the stream, will match with the name of the
        // saved results
        string name;
        
        // a listing of all the domains which the stream passes through
        string[] domains;
    }
    
    immutable
    {
        TableData conservative_data;
        TableData primitive_data;
        TableData isentropic_data;
    }

    /// fluid mass within the stream (kg)
    @property Real mass()
    {
        if(!_mass_is_updated)
        {
            _mass = 0.0;
            nodes.each!(node => _mass += node.mass);
            _mass_is_updated = true;
        }
        return _mass;
    }

    // Total energy embodied within the stream (J)
    @property Real energy()
    {
        if(!_energy_is_updated)
        {
            _energy = 0.0;
            nodes.each!(node => _energy += node.E*node.geometry.volume);
            _energy_is_updated = true;
        }
        return _energy;
    }

    /// Average temperature of the stream
    @property Real average_T()
    {
        Real result = 0.0;
        nodes.each!(node => result += node.geometry.volume*node.T);
        return result/volume;
    }

    /// Average pressure of the stream
    @property Real average_p()
    {
        Real result = 0.0;
        nodes.each!(node => result += node.geometry.volume*node.p);
        return result/volume;
    }

    /// Total volume occupied by the stream (m3)
    @property Real volume() {return _volume;}

    /// Time taken for a particle to completely traverse the stream, based upon
    /// the state at a given instant in time
    @property Real traversal_time()
    {
        Real result = 0.0;
        nodes.each!(node => result += node.geometry.length/node.v);
        return result;
    }

    this(
        const string name,
        const string[] domains,
        const TableData conservative_data, 
        const TableData primitive_data, 
        const TableData isentropic_data,
        string[] saved_fields)
    {
        this.name = name;
        this.domains = domains;
        this.conservative_data = cast(immutable) conservative_data;
        this.primitive_data = cast(immutable) primitive_data;
        this.isentropic_data = cast(immutable) isentropic_data;

        foreach(field; saved_fields)
        {
            switch(field)
            {
                case "mass":
                {
                    mass_index = Simulation.n_saved_fields;
                    Simulation.add_system_saved_property(
                        SystemProperty(
                            delegate(ref Real[] save_state)
                            {
                                save_state[mass_index] = mass;
                            }));
                    break;
                } 
                case "energy":
                {
                    energy_index = Simulation.n_saved_fields;
                    Simulation.add_system_saved_property(
                        SystemProperty(
                            delegate(ref Real[] save_state)
                            {
                                save_state[energy_index] = energy;
                            }));
                    break;
                }
                case "p_avg":
                {
                    p_avg_index = Simulation.n_saved_fields;
                    Simulation.add_system_saved_property(
                        SystemProperty(
                            delegate(ref Real[] save_state)
                            {
                                save_state[p_avg_index] = average_p;
                            }));
                    break;
                }
                case "T_avg":
                {
                    T_avg_index = Simulation.n_saved_fields;
                    Simulation.add_system_saved_property(
                        SystemProperty(
                            delegate(ref Real[] save_state)
                            {
                                save_state[T_avg_index] = average_T;
                            }));
                    break;
                }
                case "traversal_time":
                {
                    traversal_time_index = Simulation.n_saved_fields;
                    Simulation.add_system_saved_property(
                        SystemProperty(
                            delegate(ref Real[] save_state)
                            {
                                save_state[traversal_time_index] = traversal_time;
                            }));
                    break;
                }
                default:
                {
                    import std.format;
                    import std.conv: to;
                    import q1dcfd.utils.errors: InvalidDefinition;

                    throw new InvalidDefinition(
                        "unrecognised fields %s in Stream definition %s"
                        .format(saved_fields.to!string, name));
                }
            }
        }
    }

    /// Add the constructed nodes from a domain to the stream contents
    void add_node(FluidNode node)
    {
        nodes ~= node;
        _volume += node.geometry.volume;
    }

    /// reset the mass and energy count
    void reset()
    {
        _mass_is_updated = false;
        _energy_is_updated = false;
    }

    /*
     * Writes the meta data of the stream to meta.txt. Used by the post 
     * processor to reconstruct the system
     */
    void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln;

        file.writefln("\n[STREAM]: %s", name);
        file.writefln("domains: %s: %s", domains, "str[]");
        file.writefln("volume: %f: %s", volume, "float");
        if(mass_index > 0) file.writefln("mass_index: %u: %s", mass_index, "int");
        if(energy_index > 0) file.writefln("energy_index: %u: %s", energy_index, "int");
        if(p_avg_index > 0) file.writefln("p_avg_index: %u: %s", p_avg_index, "int");
        if(T_avg_index > 0) file.writefln("T_avg_index: %u: %s", T_avg_index, "int");
        if(traversal_time_index> 0) file.writefln("traversal_time_index: %u: %s",
                                                  traversal_time_index, "int");
    }
}

module q1dcfd.flux_calculators.ausmpmup ;

import q1dcfd.config: Real ;
import q1dcfd.interfaces.transport_interface: TransportInterface, FluxCalculator ;
import q1dcfd.utils.math: square ;

import std.math: fabs, sqrt ;
import std.algorithm.comparison: max, min ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

@fastmath nothrow pure Real m_split(const Real mach, const int m, const int sgn)
{
    enum Real beta = 0.125 ;

    final switch(m)
    {
        case 1:
            return 0.5*(mach + sgn*fabs(mach)) ;
        case 2:
            return sgn*0.25*(mach + sgn).square ;
        case 4:
            if(fabs(mach) >= 1.0)
            {
                return m_split(mach, 1, sgn) ;
            }
            else
            {
                return m_split(mach, 2, sgn)
                    *(1.0 - sgn*16.0*beta*m_split(mach, 2, -sgn)) ;
            }
        //default:
            //assert(false, "Invalid input for m") ;
    }
}

pure nothrow Real p_split(const Real mach, const int sgn, const Real alpha)
{
    if(fabs(mach) >= 1.0)
    {
        return m_split(mach, 1, sgn)/mach ;
    }
    else
    {
        return m_split(mach, 2, sgn)
            *((sgn*2.0- mach) - sgn*16.0*alpha*mach*m_split(mach, 2, -sgn)) ;
    }
}

FluxCalculator ausmpmup = (TransportInterface face)
/** An AUSM+-up solver for 1d flow, based upon:

    Meng-Sing Liou, A sequel to AUSM, Part II: AUSM-up for all speeds,
    Journal of Computational Physics, Volume 214, Issue 1, 2006, Pages 137-170
**/
{
    Real v_left = face.interface_l.v ;
    Real a_left = face.interface_l.a ;
    Real p_left = face.interface_l.p ;
    Real rho_left = face.interface_l.rho ;
    Real H_left = face.interface_l.H ;

    Real a_right = face.interface_r.a ;
    Real p_right = face.interface_r.p ;
    Real v_right = face.interface_r.v ;
    Real rho_right = face.interface_r.rho ;
    Real H_right = face.interface_r.H ;

    enum Real M_inf = 1.0E-4 ; //Small mach number limit

    Real a_half = 0.5*(a_right + a_left) ;
    Real M_left = v_left/a_half ;
    Real M_right = v_right/a_half ;
    Real M_bar2 = 0.5*(v_left.square + v_right.square)/a_half.square ;
    Real M0 = sqrt(min(1.0, max(M_bar2, M_inf.square))) ;
    Real fa = M0*(2.0 - M0) ;

    // Fudge factor coefficients
    Real alpha = 3.0/16.0*(-4.0 + 5.0*fa.square) ;
    enum Real Ku = 0.75 ;
    enum Real Kp = 0.25 ;
    enum Real sigma = 1.0 ;

    Real M_half = m_split(M_left, 4, 1) + m_split(M_right, 4, -1) ;
    Real pu = -Ku*p_split(M_left, 1, alpha)*p_split(M_right, -1, alpha)
        *(rho_left + rho_right)*fa*a_half*(v_right - v_left) ;
    Real p_half = p_split(M_left, 1, alpha)*p_left 
        + p_split(M_right, -1, alpha)*p_right + pu ; // interface pressure

    Real rho_half = (M_half > 0) ? rho_left : rho_right ;

    Real dm_half = a_half * M_half * rho_half ;

    face.flux[0] = dm_half ;

    Real[3] phi ;

    if(dm_half > 0)
    {
        face.flux[1] = dm_half*v_left + p_half ;
        face.flux[2] = dm_half*H_left + p_half ;
    }
    else
    {
        face.flux[1] = dm_half*v_right + p_half ;
        face.flux[2] = dm_half*H_right + p_half ;
    }
} ;
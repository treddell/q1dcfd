module q1dcfd.flux_calculators.ausmdv ;

import q1dcfd.config: Real ;
import q1dcfd.interfaces.transport_interface: TransportInterface, FluxCalculator ;
import q1dcfd.utils.math: square ;

version(high_prec) {import std.math: fabs ; }
else {import core.stdc.math: fabs ;}
import std.algorithm.comparison: max, min ;

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

FluxCalculator ausmdv = (TransportInterface face)
/++
+ AUSMDV method of Wada And Liou (1994), used to calculate interface
+ flux based on reconstructed left/right states
+/
{
    enum Real K_SWITCH = 10.0 ;
    enum Real C_EFIX = 0.125 ;

    Real v_left = face.interface_l.v ;
    Real a_left = face.interface_l.a ;
    Real p_left = face.interface_l.p ;
    Real rho_left = face.interface_l.rho ;
    Real H_left = face.interface_l.H ;

    Real a_right = face.interface_r.a ;
    Real p_right = face.interface_r.p ;
    Real v_right = face.interface_r.v ;
    Real rho_right = face.interface_r.rho ;
    Real H_right = face.interface_r.H ;

    //Calculate interface weightings
    Real ratio_r = p_right / rho_right ;
    Real ratio_l = p_left / rho_left ;

    Real alpha_l = 2.0*ratio_l/(ratio_l + ratio_r) ;
    Real alpha_r = 2.0 - alpha_l ;

    Real a  = max(a_left, a_right) ;
    Real a_rec = 1.0/a ;
    Real ML = v_left*a_rec ;
    Real MR = v_right*a_rec ;

    Real duL, duR, ML_rel2, MR_rel2, pLplus, pRminus, uLplus, uRminus ;
    Real ru_half, p_half, dp, s, ru2_AUSMV, ru2_AUSMD, ru2_half ;
    Real mass_flux, momentum_flux, energy_flux, d_ua ;

    bool caseA, caseB ;

    // Begin main calculation
    duL = 0.5*(v_left + fabs(v_left)) ;

    if(fabs(ML) <= 1.0)
    {
        ML_rel2 = (ML + 1.0).square ;
        pLplus = p_left * ML_rel2 * (2.0 - ML) * 0.25 ;
        uLplus = alpha_l * (0.25*ML_rel2 * a - duL) + duL ;
    }
    else
    {
        pLplus = p_left * duL / v_left ;
        uLplus = duL ;
    }

    duR = 0.5*(v_right - fabs(v_right)) ;
    if(fabs(MR) <= 1.0)
    {
        MR_rel2 = (MR - 1.0).square ;
        pRminus = p_right * MR_rel2 * (2.0 + MR) * 0.25 ;
        uRminus = alpha_r * (-0.25*MR_rel2 * a - duR) + duR ;
    }
    else
    {
        pRminus = p_right * duR / v_right ;
        uRminus = duR ;
    }

    ru_half  = uLplus * rho_left + uRminus * rho_right ;
    p_half   = pLplus + pRminus ;
    dp       = K_SWITCH * fabs(p_left - p_right) / min(p_left, p_right) ;
    s        = 0.5 * min(1.0, dp) ;

    ru2_AUSMV = uLplus * rho_left * v_left + uRminus * rho_right * v_right ;
    ru2_AUSMD = 0.5*(ru_half * (v_left + v_right) 
        - fabs(ru_half)*(v_right - v_left)) ;
    ru2_half = (0.5 + s) * ru2_AUSMV + (0.5 - s) * ru2_AUSMD ;

    mass_flux     = ru_half ;
    momentum_flux = ru2_half + p_half ;

    energy_flux = (ru_half >=0) ? ru_half * H_left : ru_half * H_right ;

    // Entropy fix
    caseA = ((v_left - a_left < 0.0) && (v_right - a_right > 0.0)) ;
    caseB = ((v_left + a_left < 0.0) && (v_right + a_right > 0.0)) ;

    d_ua = 0.0 ;
    if(caseA && !caseB)
    {
        d_ua = C_EFIX * ((v_right - a_right) - (v_left - a_left) ) ;
    }
    else if(caseB && !caseA)
    {
        d_ua = C_EFIX * ((v_right + a_right) - (v_left + a_left) ) ;
    }

    if(!d_ua == 0.0)
    {
        momentum_flux -= d_ua * (rho_right * v_right - rho_left * v_left) ;
        energy_flux -= d_ua*(H_right * rho_right - H_left * rho_left) ;
    }

    face.flux[0] = mass_flux ;
    face.flux[1] = momentum_flux ;
    face.flux[2] = energy_flux ;
} ;

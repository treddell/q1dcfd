module q1dcfd.flux_calculators.reconstructions ;

import q1dcfd.config: Real ;
import q1dcfd.control_volumes: FluidNode ;

version(high_prec) {import std.math: fabs ;}
else {import core.stdc.math: fabs ;}

version(LDC) { import ldc.attributes: fastmath ;} else {enum fastmath ;}

alias ReconstructionScheme 
    = pure nothrow Real[3] delegate(FluidNode, FluidNode, FluidNode) ;

alias FluxLimiter
    = pure nothrow Real[3] function(FluidNode, FluidNode, FluidNode) ;

/**
 * Generates a MUSCL scheme based upon a limiter function, and a factor k, 
 * returns the callable left side muscl calculator
 */
ReconstructionScheme generate_muscl_scheme_left(FluxLimiter limiter, const Real k)
{
    @fastmath
    typeof(return) muscl = (FluidNode lleft, FluidNode left, FluidNode right)
    {
        Real[3] phi = limiter(lleft, left, right) ;
        Real[3] result = left.cons_state[] 
            + 0.25*phi[]*((1.0 - k)*(left.cons_state[] - lleft.cons_state[])
            + (1.0 + k)*(right.cons_state[] - left.cons_state[])) ;

        return result ;
    } ;

    return muscl ;
}

/**
 * Generates a MUSCL scheme based upon a limiter function, and a factor k, 
 * returns the callable left side muscl calculator
 */
ReconstructionScheme generate_muscl_scheme_right(FluxLimiter limiter, const Real k)
{
    @fastmath
    typeof(return) muscl = (FluidNode left, FluidNode right, FluidNode rright)
    {
        Real[3] phi = limiter(left, right, rright) ;
        Real[3] result = right.cons_state[] 
            - 0.25*phi[]*((1.0 - k)*(rright.cons_state[] - right.cons_state[])
            + (1.0 + k)*(right.cons_state[] - left.cons_state[])) ;

        return result ;
    } ;

    return muscl ;
}

/**
 * Reconstruction scheme for a left sided boundary, simple averaging
 */
ReconstructionScheme generate_left_sided_scheme(FluxLimiter limiter)
{
    @fastmath
    typeof(return) left_scheme = (FluidNode lleft, FluidNode left, FluidNode right)
    {
        Real[3] result = 0.5*(left.cons_state[] + right.cons_state[]) ;
        return result ;
    } ;
    return left_scheme ;
}

/**
 * Reconstruction scheme for a right sided boundary, simply averaging
 */
ReconstructionScheme generate_right_sided_scheme(FluxLimiter limiter)
{
    @fastmath
    typeof(return) right_scheme = (FluidNode left, FluidNode right, FluidNode rright)
    {
        Real[3] result = 0.5*(left.cons_state[] + right.cons_state[]) ;
        return result ;
    } ;

    return right_scheme ;
}

/**
 * Dummy flux limiter, does not attempt to limit scheme 
 */
FluxLimiter unlimited = (FluidNode left, FluidNode centre, FluidNode right)
{
    Real[3] result = [1.0, 1.0, 1.0] ;
    return result ;
} ;

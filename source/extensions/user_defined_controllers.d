module q1dcfd.extensions.user_defined_controllers ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * This function is intended to be a user extension of the default controller initialisation,
 * allowing for customised control models to be used
 *
 * Add lines below which parse a list of parameter definitions (defs) and populate a
 * custom controller class instance (controller)
 *
 * The following commented out lines serve as a template to initialise the custom model
 * uncomment the lines starting with // and replace the line comments by code which does
 * whatever you need it to
 *
 * Parameters:
 *
 * - model_name: string, the name of the controller model to use, this must be a case in the
 *     'switch' statement below
 * - domains: The structure holding the list of all domain types, the created model must be
 *     added to this using the domains.add(name, controller) method
 * - defs: string[string], a dictionary of keyword values pairs ripped from the definition file
 *     , these must be parsed to extract controller parameters into keyword value pairs of the
 *     correct type, so that these may be passed to the Controller constructor
 * - name: string, name of the controller, what it will be referred to in the results
 *
 * Returns:
 *     bool: true if controller successfully created and added to the domain list, else false
 */
bool build_user_defined_controller(
    const string model_name,
    ref from!"q1dcfd.domains".DomainContainer domains, 
    ref string[string] defs,
    const string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required, defaults, greater, geq,
        subset, new_dict;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;

    switch(model_name)
    {
        /*
         * Add here case statements for all custom controller models you wish to use then add them
         * to the domain container
         * 
         * Each case should look something like the following:
         *
         ***************************************************************
         * case "MyCustomControlModel":
         * {
         *     // import any custom controller models
         *
         *     auto parameters = new_dict!double ;
         *     <extract parameters from "defs">
         *
         *     auto flags = new_dict!bool ;
         *     <extract flags from "defs">
         *
         *     auto target_outputs = [<extract boundary functions from "defs">] ;
         *
         *     < do any other parsing as necessary, for example, extracting the n_controls
         *     , n_inputs from "defs"
         *
         *     // get the initial controller values from "defs"     
         *     double[] initial_state = [defs.retrieve("initial_state").required!double] ;
         *     
         *     auto controller = new CustomControlClass(args...) ;
         *     domains.add(name, controller) ;
         *
         *     return true ; // returns true to let the parent function know that a user
         *     defined control model is used
         * }
         ******************************************************************
         *
         * For some examples of what this may look like, refer to
         * q1dcfd.domains.controller.generate_controller and
         * q1dcfd.domains.controller.build_controller
         * 
         */

        case "StateSpaceController":
        {
            import std.conv: to;
            import std.format;
            import q1dcfd.domains: Controller, StateSpaceControllerDomain;
            import q1dcfd.controllers: StateSpaceController;
            import q1dcfd.config: Real, Transient;

            string controller_jobfile = defs.retrieve("controller_jobfile").required!string;
            uint n_controls = defs.retrieve("n_controls").required!uint;
            uint n_measurements = defs.retrieve("n_measurements").required!uint;

            double[string] parameters = [
                "controller_dt" : defs.retrieve("controller_dt").required!Real];

            // Pick out sequentially numbered 'ref_traj_n' values
            Transient[string] target_outputs;
            foreach(i; 0..defs.length)
            {
                auto key = "ref_traj_" ~ i.to!string;
                if(key in defs)
                    target_outputs[key] =
                        &defs.retrieve(key).read_boundary_function.evaluate;
                else break;
            }
            // Pick out sequentially numbered u_n values
            Real[] initial_u; // control inputs @t=0
            foreach(i; 0..defs.length)
            {
                auto key = "u_" ~ i.to!string;
                if(key in defs) initial_u ~= defs.retrieve(key).required!Real;
                else break;
            }
            if(initial_u.length != n_controls)
                throw new Exception(format!(
                        "ERROR: number of initial control inputs %d does not match "
                        ~ "number of control variables %d.")(
                        initial_u.length, n_controls));

            auto controller =
                new StateSpaceControllerDomain!(
                        Controller!(StateSpaceController))(
                        name,
                        ["N_min", "N_max", "W_net_ref"], // saved fields, TODO
                        parameters,      // floating point parameters
                        new_dict!(bool), // flags
                        target_outputs,  // reference trajectory
                        initial_u,       // initial control inputs
                        n_controls,
                        n_measurements,
                        geometries,
                        controller_jobfile);

            domains.add(name, controller);

            return true;
        }

        // If the provided model name is not found then returns false
        // This line required!
        default: return false;
    }
}


module q1dcfd.config ;

import q1dcfd.utils.metaprogramming: from ;

// TEMP: Workaround until refactoring complete
public import q1dcfd.types.basic_types ;
public import q1dcfd.types.address ;

/*
 * Static Configuration options for q1dcfd
 * Stores global configuration options, constants and defines custom types
 */

// Steady state default time out
enum STEADY_SOLVER_TIMEOUT = 20.0 ; // seconds, simulation time

// Numerical break limits, simulation will terminate in debug mode
enum NUMERICAL_TEMPERATURE_LIMIT = 2000.0 ; // K

// Minimum Time Step
enum GLOBAL_MINIMUM_DT = 1.0E-8 ;

// Number of log messages stored in the temporary buffer
enum N_LOG_INSTRUCTIONS_RECORDED = 100 ;

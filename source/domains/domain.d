module q1dcfd.domains.domain ;

import std.conv: to ;

/**
 * Base type for all simulation domains, responsible for creation and 
 * population of nodes, and linking them to the simulation manager.
 */
abstract class Domain 
{
    import std.format ;
    
    import q1dcfd.types.geometry: CrossSection, NodeGeometry ;
    import q1dcfd.config: Real ;
    import q1dcfd.control_volumes.node: Node ;
    import q1dcfd.utils.metaprogramming: from ;

    const
    {
        // who am I ?
        string name ;

        // set of saved attributes
        string[] saved_fields ;

        // number of nodes in domain
        uint n_nodes ;

        // number of saved fields from the domain
        uint  w_order ;
    }

    // Domain geometry
    CrossSection geometry ; 

    // left index of local nodes in system node list
    uint node_index_l ;

    // right index of local nodes in system node list
    uint node_index_r ;

    // left address of domain in configuration space
    uint ode_index_l ; 

    // (local) co-ordinates of each node
    Real[] node_coords ;

    // number of independent ordinary differential equations
    uint order ; 

     // left address of save state in global write out state
    uint write_index_l ;

    // unique identifier for the domain type
    abstract @property string type() ;
    abstract @property string node_type() ;

    this(const string name,
         CrossSection geometry,
         const string[] saved_fields,
         const uint n_nodes)
    {
        this.name = name ;
        this.geometry = geometry ;
        this.saved_fields = saved_fields ;
        this.w_order = saved_fields.length.to!uint*n_nodes ;
        this.n_nodes = n_nodes ;
        this.node_coords.length = n_nodes ;
    }
    
    /**
     * Store the positions of the domain within the system node list, and link
     * to the simulation manager, actual work is performed by the derived classes,
     * which, generate each control volume and its internal interfaces
     */
    void discretise(ref Node[] nodes,
                    ref from!"q1dcfd.interfaces.root_interface".Interface[] interfaces, 
                    const uint node_index_l,
                    const uint index_l,
                    const uint write_index_l) ;

    /**
     * Initialises the state of every node described by the domain
     */
    void initialise_state(ref Real[] state, ref Node[] nodes) ;

    /** 
     * Get the global nodal address from the local nodal address
     */
    uint node_global_id(const int n)
    {
        import std.math: abs ;
        import std.exception: enforce ;
        import q1dcfd.utils.errors: InvalidDefinition ;
        
        auto m = n < 0 ? n_nodes.to!int + n : n ; // actual index

        // make sure indexing is not out of bounds
        enforce!InvalidDefinition(
            abs(m) < n_nodes,
            "Attempt to reference out of bounds node %u, domain %s contains only %u nodes"
            .format(n, name, n_nodes)) ;

        return node_index_l + m ;
    }

    /**
     * Generates the geometry of each control volume based off provided geometry
     * for the entire channel. Returns as a NodeGeometry object
     */
    static NodeGeometry generate_nodal_geometry(
        CrossSection domain_geometry, Real xa, Real xb)
    {
        NodeGeometry result ;
        result.length = xb - xa ;
        result.lx = xa ;
        result.rx = xb ;
        result.rA = domain_geometry.cross_area(xb) ;
        result.lA = domain_geometry.cross_area(xa) ;
        result.volume = domain_geometry.volume(xa, xb) ;
        return result ;
    }

    /**
     * Writes the snippet of plain text which defines the metadata of the domain
     * . Writes it to the meta.txt file. This information is read by the post 
     * processor when reconstructing the system. Final argument in writeln function
     * tells the post processor the type of the data
     */
    void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln ;
        file.writefln("\n[DOM]: %s", name) ;
        file.writefln("type: %s: %s", typeid(this), "str") ;
        file.writefln("index_l: %u: %s", ode_index_l, "int") ;
        file.writefln("index_r: %u: %s", ode_index_l + order, "int") ;
        file.writefln("w_index_l: %u: %s", write_index_l, "int") ;
        file.writefln("w_index_r: %u: %s", write_index_l + w_order, "int") ;
        file.writefln("n_nodes: %u: %s", n_nodes, "int") ;
        file.writefln("node_coords: %(%s, %) : %s", node_coords, "float[]") ;
        file.writefln("data_contents: %s : %s", saved_fields, "str[]") ;
    }

    /// link boundary functions to cells
    void link_boundaries(ref Node[] nodes) {}

    /// Have all linkages been made so that the domain is simulation ready?
    string check_simulation_ready() {return "" ;}

    /// set boundary functions
    void designate_boundary_linked(const string port) {}
}

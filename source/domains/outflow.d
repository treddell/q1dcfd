module q1dcfd.domains.outflow ;

import q1dcfd.control_volumes: TargetMassNode, FarFieldNode,
       OutflowToStagnation, IncompressibleOutflow;
import q1dcfd.utils.metaprogramming: from ;

/**
 * Does most of the actual work of build_outflow
 */
private void generate_outflow(NodeModel)(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    const string name)
{
    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, required, defaults,
        subset, greater, geq ;
    import q1dcfd.eos: backend ;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;

    scope(exit) defs.require_empty(name) ;

    static if(is(NodeModel == TargetMassNode))
    {
        auto outflow = new Outflow!(TargetMassNode)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("target_function").defaults("Unassigned").read_boundary_function,
            defs.retrieve("target_coeff").defaults(1.0).geq(0.0),
            defs.retrieve("max_dpdt").defaults(1.0E6).greater(0.0),
            defs.retrieve("mass_coeff").defaults(1.0E6).greater(0.0)) ;
    }
    else static if(is(NodeModel == FarFieldNode))
    {
        auto outflow = new Outflow!(FarFieldNode)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("target_function").defaults("Unassigned").read_boundary_function,
            defs.retrieve("target_coeff").defaults(1.0).geq(0.0)) ;
    }
    else static if(is(NodeModel == IncompressibleOutflow))
    {
        // This is a copy of the code for 'FarFieldNode'.
        // We don't need 'target_function' or 'target_coeff' for this NodeModel,
        // so defaults are just be passed in and not used.
        auto outflow = new Outflow!(IncompressibleOutflow)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("target_function").defaults("Unassigned").read_boundary_function,
            defs.retrieve("target_coeff").defaults(1.0).geq(0.0)) ;
    }
    else static if(is(NodeModel == OutflowToStagnation))
    {
        auto outflow = new Outflow!(OutflowToStagnation)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("initial_stagnation_pressure").defaults("Unassigned").read_boundary_function,
            defs.retrieve("initial_stagnation_temperature").defaults("Unassigned").read_boundary_function,
            defs.retrieve("coolprop_backend").defaults("HEOS"),
            defs.retrieve("coolprop_fluid").defaults("none"),
            defs.retrieve("target_function").defaults("Unassigned").read_boundary_function,
            defs.retrieve("target_coeff").defaults(1.0).geq(0.0));
    }
    else assert(false) ;

    domains.add(name, outflow) ;
}

/**
 * Constructs an outflow object from a set of domain definitions 
 *
 * Required Arguments:
 *     type                   : Outflow 
 *     initial_temperature    : Initial temperature distribution as a 
 *                                 MathFunction(x), evaluates at 0
 *     initial_pressure       : Initial pressure distribution as a MathFunction(x),
 *                                 evaluates at 0
 *     initial_velocity       : Initial velocity distribution as a MathFunction(x),
 *                                 evaluates at 0
 *
 * Optional Arguments    
 *     node_model             : A valid outflow node model
 *     save                   : Ordered list of attributes to save to file, 
 *                                 default (p, T, v)
 *     target_function        : the target property used to guide the formation
 *                                 of the upstream travelling accoustic wave as a
 *                                 MathFunction(t). Using the default node model,
 *                                 this will define a target pressure
 *     priority_queue         : true/false, if true will force the node to updated
 *                                 in the zero'th node queue. default is false
 *     target_coeff           : scale factor for the target function
 *     mass_coeff             : scale factor for the mass flow rate, TargetMassNode 
 *                                 model only
 *     max_dpdt               : maximum rate of change of pressure
 */
void build_outflow(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries) 
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults ;

    // get the node model
    auto model = defs.retrieve("node_model").defaults("FarFieldNode") ;

    // delegate actual work to template constructor
    switch(model)
    {
        case "FarFieldNode":
        {
            generate_outflow!(FarFieldNode)(domains, defs, name) ;
            break ;
        }
        case "TargetMassNode":
        {
            generate_outflow!(TargetMassNode)(domains, defs, name) ;
            break ;
        }
        case "OutflowToStagnation":
        {
            generate_outflow!(OutflowToStagnation)(domains, defs, name) ;
            break ;

        }
        case "IncompressibleOutflow":
        {
            generate_outflow!(IncompressibleOutflow)(domains, defs, name) ;
            break ;
        }
        // Can add additional models here
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition ;
            import std.format ;

            throw new InvalidDefinition(
                "Unrecognised node_model %s for Outflow &s".format(model, name)) ;
        }
    }
}
/**
 * Base class for an outflow domain
 */

abstract class AbstractOutflow: from!"q1dcfd.domains".FluidDomain
{
    import q1dcfd.utils.math: MathFunction ;
    import q1dcfd.domains: Domain ;
    import q1dcfd.config: Transient ;

    // far field transient target
    MathFunction target_function ;

    // flag whether the inflow needs to be in the priority queue
    // this is required for casually implicit components such as discontinuity
    // connections
    bool priority_queue ;

    // pointer to the interfacing interior
    Domain interior ;

    // a connection has been made to the boundary functions
    bool target_function_is_linked = false ;

    // target function coefficient
    double target_coeff ;

    // What am I ?
    override @property string type() { return "Outflow"; }

    this(const string name,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         const bool priority_queue,
         MathFunction target_function,
         double target_coeff)
    {
        import q1dcfd.types.geometry: Dimensionless ;
        super(name,
              new Dimensionless,
              saved_fields,
              1,
              "NoFriction",
              "NoHeatTransfer",
              1,
              false,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model) ;

        this.target_function = target_function ;

        if(target_function.not_null) designate_boundary_linked("target") ;

        this.priority_queue = priority_queue ;
        this.node_coords[0] = 0.0 ;
        this.target_coeff = target_coeff ;
    }
}

/**
 * Implements Outflow with a given NodeModel
 */
final class Outflow(NodeModel): AbstractOutflow 
{
    import q1dcfd.interfaces.root_interface: Interface ;
    import q1dcfd.control_volumes: Node ;
    import q1dcfd.config: Real ;

    static if(is(NodeModel == from!"q1dcfd.control_volumes".TargetMassNode))
    {
        double max_dpdt ;
        double mass_coeff ;

        // TargetMassNode constructor
        this(const string name,
             const string[] saved_fields,
             Transient initial_temperature_distribution,
             Transient initial_pressure_distribution,
             Transient initial_velocity_distribution,
             from!"q1dcfd.eos".backend backend_model,
             const bool priority_queue,
             MathFunction target_function,
             double target_coeff,
             double max_dpdt,
             double mass_coeff)
        {
            import q1dcfd.control_volumes: TargetMassNode ;

            this.max_dpdt = max_dpdt ;
            this.mass_coeff = mass_coeff ;

            // Shares rest of functionality with FarField constructor
            this(name,
                 saved_fields,
                 initial_temperature_distribution,
                 initial_pressure_distribution,
                 initial_velocity_distribution,
                 backend_model,
                 priority_queue,
                 target_function,
                 target_coeff) ;
        }
    }

    // OutflowToStagnation extra variables and constructor
    static if(is(NodeModel == OutflowToStagnation))
    {
        Real initial_p_stag;
        Real initial_t_stag;
        string coolprop_backend;
        string coolprop_fluid;

        this(const string name,
             const string[] saved_fields,
             Transient initial_temperature_distribution,
             Transient initial_pressure_distribution,
             Transient initial_velocity_distribution,
             from!"q1dcfd.eos".backend backend_model,
             const bool priority_queue,
             MathFunction initial_p_stag,
             MathFunction initial_t_stag,
             const string coolprop_backend,
             const string coolprop_fluid,
             MathFunction target_function,
             double target_coeff)
        {
            import std.conv: to;
            import q1dcfd.utils.math: Unassigned;
            // If stagnation values are not assigned, use inflow conditions
            if(cast(Unassigned) initial_p_stag !is null)
                this.initial_p_stag = initial_pressure_distribution(0);
            else
                this.initial_p_stag = initial_p_stag.evaluate(0);
            if(cast(Unassigned) initial_t_stag !is null)
                this.initial_t_stag = initial_temperature_distribution(0);
            else
                this.initial_t_stag = initial_t_stag.evaluate(0);

            this.coolprop_backend = coolprop_backend;
            this.coolprop_fluid = coolprop_fluid;

            // Shares rest of functionality with FarField constructor
            this(name,
                 saved_fields,
                 initial_temperature_distribution,
                 initial_pressure_distribution,
                 initial_velocity_distribution,
                 backend_model,
                 priority_queue,
                 target_function,
                 target_coeff) ;
        }
    }

    // FarField Constructor (also used for other NodeModels)
    this(const string name,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         const bool priority_queue,
         MathFunction target_function,
         double target_coeff)
    {
        super(name,
              saved_fields,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model,
              priority_queue,
              target_function,
              target_coeff) ;

        this.order = NodeModel.order ;
    }

    override @property string node_type() {return typeid(NodeModel).toString ;}

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import q1dcfd.control_volumes: TargetMassNode, FarFieldNode ;
        import q1dcfd.config: Address ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto address = Address(index_l, index_l + NodeModel.order,
            write_index_l, node_index_l, name) ;

        static if(is(NodeModel == FarFieldNode))
        {
            nodes[node_index_l] = new NodeModel(address,
                                                stream,
                                                target_coeff,
                                                backend_model);
        }
        else static if(is(NodeModel == TargetMassNode))
        {
            nodes[node_index_l] = new NodeModel(address,
                                                stream,
                                                target_coeff,
                                                max_dpdt,
                                                mass_coeff,
                                                backend_model);
        }
        else static if(is(NodeModel == IncompressibleOutflow))
        {
            nodes[node_index_l] = new NodeModel(address);
        }
        else static if (is(NodeModel == OutflowToStagnation))
        {
            nodes[node_index_l] = new NodeModel(address, stream, backend_model,
                    initial_p_stag, initial_t_stag, initial_velocity_distribution,
                    coolprop_backend, coolprop_fluid);
        }
        else
        {
            assert(false, "Unrecognised Node model %s".format(NodeModel.type)) ;
        }

        // mark this node as ordered/unordered
        nodes[node_index_l].priority_queue = priority_queue ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format ;
        import q1dcfd.eos: backend, generate_eos ;
        import q1dcfd.utils.math: square ;
        import q1dcfd.control_volumes: TargetMassNode, IncompressibleOutflow;

        // populate with initial data
        auto p = initial_pressure_distribution(0.0) ;
        auto T = initial_temperature_distribution(0.0) ;
        auto v = initial_velocity_distribution(0.0) ;

        // For 'IncompressibleOutflow's, we just write in the state values since
        // there is no EoS object
        static if(is(NodeModel == IncompressibleOutflow))
        {
            import std.conv: to;
            nodes[node_global_id(0)].to!NodeModel.p = p;
            nodes[node_global_id(0)].to!NodeModel.T = T;
            nodes[node_global_id(0)].to!NodeModel.v = v;
        }
        // Otherwise, we initialise in the regular manner
        else
        {
            // generate a temporary table to populate initial properties
            auto eos = generate_eos(stream.primitive_data,
                                    backend.TTSE_recovery,
                                    "Initialisation eos of %s".format(name)) ;
            eos.update(p, T) ;

            // Stagnation boundaries do have any ODEs to solve
            static if(!is(NodeModel == OutflowToStagnation))
            {
                // first three components are conserved variables
                state[ode_index_l..ode_index_l + 3] = [
                    eos.rho,
                    eos.rho*v,
                    eos.rho*(eos.e + 0.5*v.square)] ;
            }

            // mass flow setting nodes initialised at far field pressure
            // = outflow pressure
            static if(is(NodeModel == TargetMassNode))
            {
                state[ode_index_l + 3] = p ;
            }

            nodes[node_global_id(0)].update(state, 0.0) ;
        }
    }

    override void link_boundaries(ref Node[] nodes) 
    {
        import std.conv ;

        // No boundary transients for OutflowToStagnation
        static if (is(NodeModel == OutflowToStagnation))
        {
                target_function_is_linked = true ;
        }
        else
        {
            auto node = nodes[node_global_id(0)].to!NodeModel ;
            if(target_function.not_null)
            {
                target_function_is_linked = true ;
                node.target_function = &target_function.evaluate ;
            }
        }
    }

    override string check_simulation_ready()
    {
        import std.format ;
        import std.string: endsWith ;

        auto msg = "" ;

        // If its a reflecting boundary condition then a far field
        // pressure is needed
        if(   typeid(NodeModel).toString.endsWith(".FarFieldNode")
           && !target_function.not_null)
        {
           msg ~= format!("Target function not assigned for domain %s")(name) ;
        }
        
        return msg ;
    }
    
    override void designate_boundary_linked(const string port)
    {
        import std.format ;
        import q1dcfd.utils.errors: InvalidDefinition, MultipleConnection ;

        switch(port)
        {
            case "target":
            {
                if(!target_function_is_linked)
                {
                    target_function_is_linked = true ;
                    break ;
                }
                throw new MultipleConnection(name, port) ;
            }
            default:
            {
                throw new InvalidDefinition("boundary function %s not understood"
                                            .format(port)) ;
            }
        }
    }
}

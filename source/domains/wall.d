module q1dcfd.domains.wall ;

import q1dcfd.utils.metaprogramming: from ;

// does most of the actual work of build_axial_wall
private void generate_wall_model(NodeModel)(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    const string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to ;

    import q1dcfd.utils.dictionary_tools: retrieve, defaults, required, require_empty,
        subset, map_from, greater ;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;
    import q1dcfd.types.geometry: WallCrossSection ;
    import q1dcfd.control_volumes: TemperatureNode ;

    // Check for unrecognised arguments
    scope(exit) defs.require_empty(name) ;

    // Boundary Wall
    static if(is(NodeModel == TemperatureNode))
    {
        auto wall = new BoundaryWall!(NodeModel)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["T"]),
            defs.retrieve("temperature_transient").defaults("Unassigned").read_boundary_function) ;
    }
    // Interior Wall
    else
    {
        auto wall = new AxialWall!(NodeModel)(
            name,
            defs.retrieve("cross_section").required!string.map_from(geometries)
                .to!WallCrossSection,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["T"]),
            defs.retrieve("nodes").required!uint.greater(0),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            defs.retrieve("length").required!double.greater(0.0)) ;
    }
    
    domains.add(name, wall) ;
}

/**
 * Constructs an Axial Wall from a set of domain definitions 
 * 
 * Required Arguments:
 *     type                   : AxialWall 
 *     initial_temperature    : Initial temperature distribution as a 
 *                                 MathFunction(x)
 *     cross_section          : A reference to the WallCrossSection definition
 *     nodes                  : positive integer, number of control volumes to 
 *                                 subdivide domain into
 *     length                 : length of wall (m)
 * 
 * Optional Arguments    
 *     node_model             : A valid axial wall node model
 *     save                   : Ordered list of attributes for each control volume
 *                                  to save to file, default (T)
 */
void build_axial_wall(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults ;

    // extract the wall model
    auto model = defs.retrieve("node_model").defaults("FourierNode") ;

    // delegate most of the actual work to the template constructor
    switch(model)
    {
        case "FourierNode":
        {
            import q1dcfd.control_volumes: FourierNode ;
            generate_wall_model!(FourierNode)(domains, defs, name, geometries) ;
            break ;
        }
        // can add additional model here
        default:
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;

            throw new InvalidDefinition(
                "Unrecognized node_model %s for AxialWall %s".format(model, name)) ;    
        }
    }
}

/**
 * Constructs a Boundary Wall from a set of domain definitions 
 * 
 * Currently only defines a defined temperature wall, may add additional capability 
 * in future
 * 
 * Required Arguments:
 *     type                   : BoundaryWall 
 * 
 * Optional Arguments    
 *     node_model             : A valid BoundaryWall node model
 *     save                   : Ordered list of attributes to save to file, 
 *                                 default (T)
 *     temperature_transient  : wall lumped temperature as a MathFunction(t)
 */
void build_boundary_wall(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries) 
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults ;

    // extract the node model
    auto model = defs.retrieve("node_model").defaults("TemperatureNode") ;

    // delegate to the template constructor to do most of the actual work
    switch(model)
    {
        case "TemperatureNode":
        {
            import q1dcfd.control_volumes: TemperatureNode ;
            generate_wall_model!(TemperatureNode)(domains, defs, name, geometries) ;
            break ;
        }
        default:
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;

            throw new InvalidDefinition(
                "Unrecognized node_model %s for BoundaryWall %s".format(model, name)) ;
        }
    }
}

/**
 * A solid homogenoues element. Mostly acts as an intermediate typedef for
 * the other types of wall domains, and gives a common initialisation virtual
 * method
 */
abstract class Wall: from!"q1dcfd.domains".Domain
{
    import q1dcfd.config: Transient ;
    
    // Initial distribution of temperature within the wall
    Transient initial_temperature_distribution ;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const uint n_nodes,
         Transient initial_temperature_distribution)
    {
        super(name,
              geometry,
              saved_fields,
              n_nodes) ;

        this.initial_temperature_distribution = initial_temperature_distribution ;
    }
}

/**
 * A solid homogenous element with internal conductive heat transfer
 * Runs parallel to a flow channel
 * 
 *   Initialization of the class will generate the control volumes within the
 *   wall and append them to the system, along with (n-1) interior interfaces.
 *   The boundary interfaces are assumed to be insulated unless explicitly 
 *   overriden
 * 
 *   leftside                                        rightside
 *   domain                                          domain
 *   -----------------   ->  ---------  ->  -----------------
 *   .       |       .   ->  .       .  ->  .       |       .
 *   .       |  n0   .   ->  .  ni   .  ->  .  n-1  |       .
 *   .       |       .   ->  .       .  ->  .       |       .
 *   -----------------   ->  ---------  ->  -----------------
 *   L0      I0           I(i-1)    I(i)  I(n-2)   R0
 */
final class AxialWall(NodeModel): Wall
{
    import q1dcfd.config: Transient, Real ;
    import q1dcfd.interfaces.root_interface: Interface ;
    import q1dcfd.control_volumes: Node ;

    override @property string type() { return "AxialWall"; }
    override @property string node_type() {return typeid(NodeModel).toString ;}

    // length of the wall cell
    const Real length ;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const uint n_nodes,
         Transient initial_temperature_distribution,
         const Real length)
    {
        super(name,
              geometry,
              saved_fields,
              n_nodes,
              initial_temperature_distribution) ;

        this.order = NodeModel.order*n_nodes ;
        this.length = length ;
    }

    override void discretise(
        ref Node[] nodes, 
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import std.conv: to ;
        import q1dcfd.types.geometry: WallCrossSection ;
        import q1dcfd.interfaces: ConductiveInterface ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        // create each node
        auto dx = length/n_nodes.to!Real ;
        uint w_left = write_index_l ;
        uint left = index_l ;
        auto wall_geometry = geometry.to!WallCrossSection ;
        
        foreach(i ; 0..n_nodes)
        {
            import std.conv: to ;
            import q1dcfd.config: Address ;

            auto xa = dx*i ; // co-ordinate of nodal left face
            auto xb = dx*(i + 1) ; // co-ordinate of nodal right face
            node_coords[i] = 0.5*(xa + xb) ;
            auto node_geometry = this.generate_nodal_geometry(wall_geometry, xa, xb);
            auto id = node_index_l + i ;

            auto address = Address(left, left + NodeModel.order, w_left, id, name) ;
            
            nodes[id] = new NodeModel(address, node_geometry, wall_geometry.conductivity, 
                wall_geometry.specific_heat, wall_geometry.density) ;

            w_left += saved_fields.length ;
            left += NodeModel.order ;
        }
        
        uint m = node_index_l ; // simulation nodal index 
        
        // construct all interior interfaces
        foreach(i ; 0..n_nodes-1)
        {
            interfaces ~= new ConductiveInterface(
                nodes[m].to!NodeModel,
                nodes[m+1].to!NodeModel) ;
            m++ ;
        }
    }

    static from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".WallCrossSection domain_geometry, 
        Real xa, Real xb)
    {
        auto result = from!"q1dcfd.domains".Domain.generate_nodal_geometry(
            domain_geometry, xa, xb) ;
        result.tA = domain_geometry.transverse_area(xa, xb) ;
        result.bA = result.tA ;
        result.transverse_length = domain_geometry.thickness ;

        return result ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.conv: to ;
        uint index_l = ode_index_l ;
        auto dx = length/n_nodes.to!Real ; 

        foreach(i ; 0..n_nodes)
        {
            auto x = 0.5*dx*(2*i + 1) ;
            auto T = initial_temperature_distribution(x) ;
            state[index_l] = T ;
            nodes[node_global_id(i)].update(state, 0.0) ;

            index_l += NodeModel.order ;
        }
    }
}

/**
 * A solid homogenous element held at a user defined transient temperature
 * Used as a boundary condition
 */
final class BoundaryWall(NodeModel): Wall
{
    import q1dcfd.utils.math: MathFunction ;
    import q1dcfd.config: Real, Transient ;
    import q1dcfd.control_volumes: Node ;
    import q1dcfd.interfaces.root_interface: Interface ;

    override @property string type() { return "BoundaryWall"; }
    override @property string node_type() {return typeid(NodeModel).toString ;}

    // Boundary Transients
    MathFunction temperature_transient ;
    bool temperature_transient_is_linked = false ;

    this(const string name,
         const string[] saved_fields,
         MathFunction temperature_transient)
    {
        import q1dcfd.types.geometry: Dimensionless ;
        import q1dcfd.utils.math: Unassigned ;

        this.temperature_transient = temperature_transient ;
        if(temperature_transient.not_null) designate_boundary_linked("temperature") ;

        this.order = NodeModel.order ;
        this.node_coords = [0.0] ;

        super(name,
              new Dimensionless,
              saved_fields,
              1,
              &(new Unassigned).evaluate) ;
    }
    
    override void discretise(ref Node[] nodes, ref Interface[] interfaces, 
        const uint node_index_l, const uint index_l, const uint write_index_l)
    {
        import q1dcfd.config: Address ;
        import q1dcfd.types.geometry: NodeGeometry ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto address = Address(index_l, index_l + NodeModel.order, 
            write_index_l, node_index_l, name) ;
            
        nodes[node_index_l] = new NodeModel(address, NodeGeometry()) ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        nodes[node_global_id(0)].update(state, 0.0) ;
    }

    override void link_boundaries(ref Node[] nodes) 
    {
        if(temperature_transient.not_null)
        {
            import std.conv: to ;

            auto node = nodes[node_global_id(0)].to!NodeModel ;
            node.temperature_transient = &temperature_transient.evaluate ;
        }
    }

    override void designate_boundary_linked(const string port)
    {
        import std.format ;
        import q1dcfd.utils.errors: InvalidDefinition, MultipleConnection ;
        
        if(port == "temperature")
        {
            if(!temperature_transient_is_linked)
            {
                temperature_transient_is_linked = true ;
                return ;
            }
            throw new MultipleConnection(name, port) ;
        }
        throw new InvalidDefinition(
            "boundary function %s not understood".format(port)) ;
    }
}

unittest
{
    import std.conv: to ;
    import std.algorithm.searching: canFind ;
    import std.exception: assertNotThrown ;

    import q1dcfd.domains: DomainContainer ;
    import q1dcfd.types.geometry: CrossSection, WallCrossSection ;
    import q1dcfd.connectors.connection: Connection ;
    import q1dcfd.control_volumes: FourierNode ;

    auto container = DomainContainer() ;

    auto geometry_defs = ["thickness": "0.1", "width": "0.2",
                          "density":"1000.0", "specific_heat": "5000.0",
                          "conductivity": "20.0"] ;
    CrossSection[string ]geometries =
        ["test_geometry": new WallCrossSection(geometry_defs, "test_geometry")] ;

    auto defs = [
        "cross_section": "test_geometry",
        "nodes": "400",
        "initial_temperature": "400",
        "length": "2.0"
    ] ;

    build_axial_wall(container, defs, "unittest_wall", geometries) ;

    assertNotThrown(container.get("unittest_wall").to!(AxialWall!(FourierNode))) ;
}


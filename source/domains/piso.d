module q1dcfd.domains.piso;

import q1dcfd.utils.metaprogramming: from;

/*
 * Constructs an piso_solver object from a set of domain definitions
 */
void build_piso(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to;

    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.types.geometry: ChannelCrossSection;

    scope(exit) defs.require_empty(name);

    // Make CoolProp fluid code from backend and fluid name
    const string fluid_code = defs.retrieve("coolprop_backend") ~ ":" ~ defs.retrieve("coolprop_name");

    // Defaults to being in priority queue since the PisoSolver should be
    // updated before the Piso use it's new values
    auto piso = new Piso(
        name,
        defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
        defs.retrieve("nodes").required!uint.greater(0),
        defs.retrieve("n_channels").required!uint.greater(0),
        defs.retrieve("length").required!double.greater(0.0),
        defs.retrieve("piso_period").required!double.greater(0.0),
        fluid_code,
        defs.retrieve("flux_method").defaults("upwind"),
        defs.retrieve("nu_correlation").defaults("laminar-semicircle-constant-q"),
        defs.retrieve("fr_correlation").defaults("bellos"),
        defs.retrieve("viscous_flag").defaults("false"),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        &defs.retrieve("wall_initial_temperature").required!string.read_boundary_function.evaluate,
        defs.retrieve("priority_queue").defaults(true));

    domains.add(name, piso);
}

/*
 * Domain class which owns the single PisoSolver node
 */
final class Piso: from!"q1dcfd.domains.domain".Domain
{
    import q1dcfd.config: Real, Transient;
    import q1dcfd.control_volumes: PisoSolver;
    import q1dcfd.control_volumes: Node;
    import q1dcfd.interfaces.root_interface: Interface;

    // Property distributions at time zero
    // Of type Transient, but are actually functions of axial position
    Transient initial_pressure_distribution;
    Transient initial_temperature_distribution;
    Transient initial_velocity_distribution;

    // TODO: Should I force this true here?
    bool priority_queue;

    // Geometric parameters to pass to the PISO solver on initialisation
    string[string] piso_defs;
    immutable uint n_nodes_piso;
    Real piso_period;

    // State variable arrays, these are accessed by PisoStream
    Real[] p_array;
    Real[] T_array;
    Real[] v_array;

    this(const string name,
         from!"q1dcfd.types.geometry".ChannelCrossSection geometry,
         const uint n_nodes_piso,
         const uint n_channels,
         const Real length,
         const Real piso_period,
         const string fluid_code,
         const string flux_method,
         const string nu_correlation,
         const string fr_correlation,
         const string viscous_flag,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         Transient wall_initial_temperature_distribution,
         const bool priority_queue)
    {
        import std.conv: to;
        import q1dcfd.types.geometry: Dimensionless;

        this.priority_queue = priority_queue;

        const string[] saved_fields = []; // Empty, store results using PisoStream not Piso
        immutable n_nodes_solver = 1; // Only have one 'node' that owns the PisoSolver
        super(name, new Dimensionless, saved_fields, n_nodes_solver);

        this.initial_temperature_distribution = initial_temperature_distribution;
        this.initial_pressure_distribution    = initial_pressure_distribution;
        this.initial_velocity_distribution    = initial_velocity_distribution;

        // TODO: Here I assume that parameters in 'geometry' do not vary with
        // length, should enforce this somehow
        this.n_nodes_piso = n_nodes_piso;
        this.piso_period  = piso_period;
        piso_defs["FLUID_CODE"]         = fluid_code;
        piso_defs["FLUX_METHOD"]        = flux_method;
        piso_defs["NU_CORRELATION"]     = nu_correlation;
        piso_defs["FR_CORRELATION"]     = fr_correlation;
        piso_defs["VISCOUS_FX_FLAG"]    = viscous_flag;
        piso_defs["nCHANNELS"]          = to!string(n_channels);
        piso_defs["LENGTH"]             = to!string(length);
        piso_defs["EPSILON"]            = to!string(geometry.roughness(0));
        piso_defs["CROSS_AREA"]         = to!string(geometry.cross_area(0));
        piso_defs["HYDRAULIC_DIAMETER"] = to!string(geometry.hydraulic_diameter(0));
        piso_defs["HEAT_CIRCUMFERENCE"] = to!string(geometry.heat_circumference(0));
        piso_defs["T_wall_start"] = to!string(wall_initial_temperature_distribution(0));
        piso_defs["T_wall_end"]   = to!string(wall_initial_temperature_distribution(length));
    }

    override @property string type() { return "Piso"; }

    override @property string node_type() { return typeid(PisoSolver).toString; }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import q1dcfd.config: Address;

        this.node_index_l = node_index_l;
        this.node_index_r = node_index_l + n_nodes; // n_nodes = 1 for PisoSolver
        this.ode_index_l = index_l;
        this.write_index_l = write_index_l;

        auto address = Address(ode_index_l, ode_index_l + PisoSolver.order,
            write_index_l, node_index_l, name);

        const bool q1d_geo_flag = true; // TODO: Add in support for regular geos
        nodes[node_index_l] = new PisoSolver(address, n_nodes_piso, piso_period,
                                             initial_temperature_distribution,
                                             initial_pressure_distribution,
                                             initial_velocity_distribution,
                                             piso_defs,
                                             q1d_geo_flag);

        nodes[node_index_l].priority_queue = priority_queue;
    }

    /*
     * The state variables live in the PisoSolver class, so there is nothing to
     * initialise here.
     */
    override void initialise_state(ref Real[] state, ref Node[] nodes) { }

    // TODO: I think this will need to handle linking with boundaries in PisoSolver
    override void link_boundaries(ref Node[] nodes) { }
}


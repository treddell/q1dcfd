module q1dcfd.domains.controller ;

import q1dcfd.utils.metaprogramming: from ;

private void generate_controller(ControlModel)(
    ref from!"q1dcfd.domains".DomainContainer domains, 
    ref string[string] defs,
    const string name)
{
    import q1dcfd.controllers: PIDController, PBurstController, SetRatio ;
    import q1dcfd.utils.dictionary_tools: retrieve, required, defaults, greater, geq,
        subset, new_dict;
    import q1dcfd.initialisation.build_boundary_function ;
    import q1dcfd.config: Transient ;

    static if(is(ControlModel == PIDController))
    {
        auto min = defs.retrieve("min").defaults(-double.infinity) ;

        double[string] parameters = [
            "k_p": defs.retrieve("k_p").defaults(0.0),
            "k_i": defs.retrieve("k_i").defaults(0.0),
            "k_d": defs.retrieve("k_d").defaults(0.0),
            "min": min,
            "max": defs.retrieve("max").defaults(double.infinity).greater(min),
            "period": defs.retrieve("period").required!double.greater(0.0)];

        bool[string] flags = ["delta_bias": defs.retrieve("delta_bias").defaults(false)];
        Transient[string] target_outputs = [
            "output": &defs.retrieve("output").required!string.read_boundary_function.evaluate,
            "bias": &defs.retrieve("bias").defaults("0.0").read_boundary_function.evaluate
        ];

        // PIDController has a single output
        uint n_controls = 1 ;

        // PIDController has a single input
        uint n_inputs = 1 ;

        double[] initial_state = [defs.retrieve("initial_state").required!double] ;
    }
    else static if(is(ControlModel == PBurstController))
    {
        auto min = defs.retrieve("min").defaults(-double.infinity) ;

        double[string] parameters = [
            "k_p": defs.retrieve("k_p").defaults(0.0),
            "min": min,
            "max": defs.retrieve("max").defaults(double.infinity).greater(min),
            "burst_on_duration": defs.retrieve("burst_on_duration").required!double.geq(0.0),
            "burst_off_duration": defs.retrieve("burst_off_duration").required!double.geq(0.0)];

        bool[string] flags = ["delta_bias": defs.retrieve("delta_bias").defaults(false)];

        Transient[string] target_outputs = [
            "output": &defs.retrieve("output").required!string.read_boundary_function.evaluate,
            "bias": &defs.retrieve("bias").defaults("0.0").read_boundary_function.evaluate
        ];

        // PIDController has a single output
        uint n_controls = 1 ;

        // PIDController has a single input
        uint n_inputs = 1 ;

        double[] initial_state = [defs.retrieve("initial_state").required!double] ;
    }
    else static if(is(ControlModel == SetRatio))
    {
        // no parameters or flags used by SetRatio
        auto parameters = new_dict!double ;
        auto flags = new_dict!bool ;

        Transient[string] target_outputs = [
            "ratio": &defs.retrieve("ratio").required!string.read_boundary_function.evaluate
        ] ;

        // PIDController has a single output
        uint n_controls = 1 ;

        // PIDController has a single input
        uint n_inputs = 1 ;

        double[] initial_state = [defs.retrieve("initial_state").required!double] ;
    }
    else static assert(false) ;

    auto controller = new Controller!(ControlModel)(
        name,
        defs.retrieve("save").subset(ControlModel.STATE_FIELDS).defaults(new string[](0)),
        parameters,
        flags,
        target_outputs,
        initial_state,
        n_controls,
        n_inputs) ;

    domains.add(name, controller) ;
}

/**
 * Builds a controller device 
 *
 * Required Arguments:
 *      control_model : A valid control node model
 *      initial_state : initial state for all controlled variables as a 
 *                          vector. Must be ordered.
 *
 * Other required arguments depend upon controller type
 *
 */
void build_controller(
    ref from!"q1dcfd.domains".DomainContainer domains, 
    string[string] defs,
    string name, 
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required, require_empty ;

    // Check for unrecognised arguments
    scope(exit) defs.require_empty(name) ;

    // get the control model type
    auto model = defs.retrieve("control_model").required!string ;

    switch(model)
    {
        case "PIDController":
        {
            import q1dcfd.controllers: PIDController ;
            generate_controller!(PIDController)(domains, defs, name) ;
            return ;
        }
        case "PBurstController":
        {
            import q1dcfd.controllers: PBurstController ;
            generate_controller!(PBurstController)(domains, defs, name) ;
            return ;
        }
        case "SetRatio":
        {
            import q1dcfd.controllers: SetRatio ;
            generate_controller!(SetRatio)(domains, defs, name) ;
            return ;
        }
        default:
        {
            import q1dcfd.extensions.user_defined_controllers ;

            // will check for user defined control models if the model name does not match
            // the base version options
            // If a user defined controller cannot be found, then throws an error
            if(!build_user_defined_controller(model, domains, defs, name,
                        geometries))
            {
                import std.format ;
                import q1dcfd.utils.errors: InvalidDefinition ;
            
                throw new InvalidDefinition(
                    "Unrecognized control_model %s for Controller %s"
                    .format(model, name)) ;
            }
        }
    }
}

/**
 * Base class for a controller domain
 */
abstract class AbstractController: from!"q1dcfd.domains".Domain
{
    import q1dcfd.config: Transient, Real ;
    import q1dcfd.control_volumes: Node;

    const 
    {
        // list of controller parameters
        Real[string] parameters ;

        // list of controller flags
        bool[string] flags ;

        // list of controller functions
        Transient[string] target_outputs ;

        // Controller initial state
        Real[] initial_state ;

        // number of controller inputs and outputs
        uint n_controls ;
        uint n_inputs ;
    }
    
    this(const string name,
         const string[] saved_fields,
         const Real[string] parameters,
         const bool[string] flags,
         Transient[string] target_outputs,
         const Real[] initial_state,
         const uint n_controls,
         const uint n_inputs)
    {
        import q1dcfd.types.geometry: Dimensionless ;

        super(name,
              new Dimensionless,
              saved_fields,
              1) ;

        this.node_coords[0] = 0.0 ;

        this.parameters = parameters ;
        this.flags = flags ;
        this.target_outputs = target_outputs ;
        this.initial_state = initial_state ;
        this.n_controls = n_controls ;
        this.n_inputs = n_inputs ;
    }

    // linkages are constructed explicitly by connection
    override void link_boundaries(ref from!"q1dcfd.control_volumes".Node[] nodes) {}

    void initialise_control_system(ref Node[] nodes) {}
}

class Controller(ControlModel): AbstractController
{
    import q1dcfd.config: Transient, Real ;
    import q1dcfd.control_volumes: Node ;
    import q1dcfd.interfaces.root_interface: Interface ;

    this(const string name,
         const string[] saved_fields,
         const Real[string] parameters,
         const bool[string] flags,
         Transient[string] target_outputs,
         const Real[] initial_state,
         const uint n_controls,
         const uint n_inputs)
    {
        super(name,
              saved_fields,
              parameters,
              flags,
              target_outputs,
              initial_state,
              n_controls,
              n_inputs) ;
    }

    // debugging and type matching information
    override @property string type() {return "Controller" ;}
    override @property string node_type() {return typeid(ControlModel).toString ;}

    override void discretise(
        ref Node[] nodes, 
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import q1dcfd.config: Address ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto address = Address(index_l, index_l + Controller!(ControlModel).order, 
            write_index_l, node_index_l, name) ;

        nodes[node_index_l] = new ControlModel(
            address, n_controls, n_inputs, parameters, flags, target_outputs) ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.conv: to ;

        debug 
        {
            import q1dcfd.utils.logging: log_message ;
            import std.format ;

            log_message("Setting the initial output of %s to %(%f, %)"
                        .format(name, initial_state)) ;
        }
        nodes[node_global_id(0)].to!ControlModel.set_initial_state(initial_state) ;
    }
}

/*
 * Used to add an array of geometries to a domain, so that any of these geometries
 * may be used in the NodeModel implemented by the domain.
 */
class StateSpaceControllerDomain(T) : T
{
    import q1dcfd.control_volumes: Node;
    import q1dcfd.interfaces.root_interface: Interface;
    import q1dcfd.types.geometry: CrossSection;
    import q1dcfd.controllers: StateSpaceController;

    CrossSection[string] geometries;
    string jobfile_name;

    this(const string name,
         const string[] saved_fields,
         const Real[string] parameters,
         const bool[string] flags,
         Transient[string] target_outputs,
         const Real[] initial_state,
         const uint n_controls,
         const uint n_inputs,
         CrossSection[string] geometries,
         const string jobfile_name)
    {
        this.geometries = geometries;
        this.jobfile_name = jobfile_name;
        super(name,
              saved_fields,
              parameters,
              flags,
              target_outputs,
              initial_state,
              n_controls,
              n_inputs);
    }

    override void discretise(
        ref Node[] nodes, 
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import std.conv: to;
        // Build the StateSpaceController node
        T.discretise(nodes, interfaces, node_index_l, index_l, write_index_l);
        // Perform initialization tasks unique to state-space controllers
        nodes[node_global_id(0)].to!StateSpaceController
            .initialise_ss_controller(geometries, jobfile_name);
    }

    override void initialise_control_system(ref Node[] nodes)
    {
        import std.conv: to;
        nodes[node_global_id(0)].to!StateSpaceController.initialise_control_system();
    }
}

unittest
// Test building of a controller
{
    import std.conv: to ;
    import std.algorithm.searching: canFind ;

    import q1dcfd.domains: DomainContainer ;
    import q1dcfd.types.geometry: CrossSection ;
    import q1dcfd.connectors.connection: Connection ;
    import q1dcfd.domains.controller: AbstractController ;

    auto container = DomainContainer() ;
    CrossSection[string] geometries ;
    auto domain_defs = [
        "control_model": "PIDController",
        "initial_state": "1.0",
        "k_p": "0.71",
        "k_d": "0.43",
        "k_i": "0.11",
        "period": "1.0E-4",
        "output": "2.0"
    ] ;

    build_controller(container, domain_defs, "unittest_controller", geometries) ;

    auto controller = container.get("unittest_controller").to!AbstractController ;

    assert(controller.type == "Controller") ;
    assert(controller.name == "unittest_controller") ;
    assert(controller.node_type 
        == "q1dcfd.controllers.pid_controller.PIDController") ;
    assert(controller.n_inputs == 1) ;
    assert(controller.n_controls == 1) ;
    assert(controller.parameters.keys.canFind("k_d")) ;
    assert(controller.parameters.keys.canFind("k_i")) ;
    assert(controller.parameters.keys.canFind("k_p")) ;
    assert(controller.parameters.keys.canFind("period")) ;
    assert(controller.target_outputs.keys.canFind("output")) ;
    assert(controller.parameters["k_d"] == 0.43) ;
    assert(controller.parameters["k_i"] == 0.11) ;
    assert(controller.parameters["k_p"] == 0.71) ;
    assert(controller.parameters["period"] == 1.0E-4) ;
    assert(controller.saved_fields == []) ;
}

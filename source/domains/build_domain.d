module q1dcfd.domains.build_domain ;

import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.domains.domain: Domain ;
import q1dcfd.domains.channel: build_channel ;
import q1dcfd.domains.inflow: build_inflow ;
import q1dcfd.domains.outflow: build_outflow ;
import q1dcfd.domains.pipe: build_pipe ;
import q1dcfd.domains.turbine: build_turbine ;
import q1dcfd.domains.compressor: build_compressor ;
import q1dcfd.domains.controller: build_controller ;
import q1dcfd.domains.wall: build_axial_wall, build_boundary_wall ;
import q1dcfd.domains.momentum_energy_jump: build_momentum_energy_jump ; 
import q1dcfd.domains.heat_exchanger :build_parallel_flow_heat_exchanger ;
import q1dcfd.domains.external_valve: build_external_valve ;
import q1dcfd.domains.interface_turbomachine: build_interface_turbomachine;
import q1dcfd.domains.map_turbomachine_system: build_map_turbomachine_system;
import q1dcfd.domains.piso: build_piso ;
import q1dcfd.domains.incompressible_channel: build_incompressible_channel;
import q1dcfd.domains.heat_exchanger_mixed : build_parallel_flow_heat_exchanger_mixed ;
import q1dcfd.types.geometry: CrossSection ;
import q1dcfd.connectors.connection: Connection ;

import std.algorithm.searching: canFind ;
import std.conv: to ;
import std.format ;
import std.string ;
import std.exception: enforce ;

struct DomainContainer
{
    /** Ordered domain 'dictionary' **/
    string[] domain_names ;
    Domain[] domains ;

    Domain get(string name)
    {
        import std.algorithm.searching: countUntil ;

        auto index = domain_names.countUntil(name) ;
        if(index < 0)
        {
            throw new Exception(format!("Domain %s not found")(name)) ;
        }

        return domains[index] ;
    }

    void add(string name, Domain dom)
    {
        domain_names ~= name ;
        domains ~= dom ;
    }

    Domain[string] as_associative_array()
    // return as an (unordered) associative array
    {
        Domain[string] result ;
        foreach(i ; 0..domains.length)
        {
            result[domain_names[i]] = domains[i] ;
        }
        return result ;
    }
}

string[] get_saved_fields(ref string[string] defs, const string[] defaults, 
    const string[] possibilities, string domain)
/** 
Reads a save field of a domain definition, or else returns the default save
fields 
**/
{
    import std.algorithm.searching: canFind ;
    string[] saved_fields ;
    if("save" in defs)
    {
        saved_fields = defs["save"].split(",") ;
        defs.remove("save") ;
        foreach(elem ; saved_fields)
        {
            enforce!InvalidDefinition(possibilities.canFind(elem),
                format!("%s is not a valid %s property")(elem, domain));
        }
    }
    else
    {
        saved_fields = cast(string[]) defaults ;
    }
    return saved_fields ;
}

/// Constructs a domain object from a set of domain definitions
void build_domain(
    ref DomainContainer domains,
    string[string] domain_def, 
    string domain_name,
    CrossSection[string] geometries, 
    ref Connection[] connection_table)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required ;

    string type = domain_def.retrieve("type").required!string ;

    switch(type)
    {
        case "Channel":
            domains.build_channel(domain_def, domain_name, geometries) ;
            break ;
        
        case "Inflow":
            domains.build_inflow(domain_def, domain_name, geometries) ;
            break ;
        
        case "Outflow":
            domains.build_outflow(domain_def, domain_name, geometries) ;
            break ;
        
        case "AxialWall":
            domains.build_axial_wall(domain_def, domain_name, geometries) ;
            break ;
        
        case "Pipe":
            domains.build_pipe(domain_def, domain_name, geometries, 
                connection_table) ;
            break ;
        
        case "BoundaryWall":
            domains.build_boundary_wall(domain_def, domain_name, geometries) ;
            break ;
        
        case "MomentumEnergyJump":
            domains.build_momentum_energy_jump(domain_def, domain_name, 
                geometries) ;
            break ;
        
        case "Turbine":
            domains.build_turbine(domain_def, domain_name, geometries, 
                connection_table) ;
            break ;

        case "Compressor":
            domains.build_compressor(domain_def, domain_name, geometries,
                connection_table) ;
            break ;
        
        case "ParallelHE":
            domains.build_parallel_flow_heat_exchanger(domain_def, domain_name, 
                geometries, connection_table) ;
            break ;

        case "Controller":
            domains.build_controller(domain_def, domain_name, geometries) ;
            break ;

        case "ExternalValve":
            domains.build_external_valve(domain_def, domain_name, geometries) ;
            break ;

        case "InterfaceTurbine":
            domains.build_interface_turbomachine(domain_def, domain_name,
                    geometries, connection_table, "turbine") ;
            break ;

        case "InterfaceCompressor":
            domains.build_interface_turbomachine(domain_def, domain_name,
                    geometries, connection_table, "compressor") ;
            break ;

        case "MapTurbine":
            domains.build_map_turbomachine_system(domain_def, domain_name,
                    geometries, connection_table, "turbine") ;
            break ;

        case "MapCompressor":
            domains.build_map_turbomachine_system(domain_def, domain_name,
                    geometries, connection_table, "compressor") ;
            break ;

        case "PisoSolver":
            domains.build_piso(domain_def, domain_name, geometries);
            break;

        case "IncompressibleChannel":
            domains.build_incompressible_channel(domain_def, domain_name, geometries) ;
            break ;

        case "MixedHE":
            domains.build_parallel_flow_heat_exchanger_mixed(domain_def, domain_name,
                geometries, connection_table) ;
            break ;

        default:
            throw new InvalidDefinition(format!"Domain type %s not recognized"
                (type)) ;
    }
}

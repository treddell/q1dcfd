module q1dcfd.domains.map_turbomachine_system;

import q1dcfd.utils.metaprogramming: from;

/*
 * Build a map-based turbomachine system, which is composed of an inlet plenum,
 * a turbomachine, and an outlet plenum.
 * The plenums are essentially channels with no heat transfer and are two cells
 * long each.
 * They must be two cells so that the stencils used to calculate fluxes at
 * the component boundaries (the I1's in the diagram) do not 'see' the
 * turbomachine cell (since this cell has special treatment).
 * The turbomachine cell behaves like an interface rather than a compresible
 * finite volume (though it can have time dynamics governed by machine
 * rotordynamics).
 * The thermodynamic response of the turbomachine is assumed to be infinitely
 * fast compared to the rest of the components.
 * The turbomachine cell takes flux values of mass, momentum, and energy at it's
 * left interface (tIfaceL), calculates a momentum and energy discontinuity that
 * represents the turbomachine, then applies these discontinuities to calculate
 * the flux values at tIfaceR.
 *
 * The turbomachine system is illustrated in the diagram below:
 *
 *
 *          |-----> x                                          | x = L
 *
 * leftside                 tIfaceL      tIfaceR                rightside
 * domain                       |          |                    domain
 * ---------------------------- |----------| ----------------------------
 * .        |        .        . | turbo    | .        .        |        .
 * .        | lleft  . left   . | machine  | .  right . rright |        .
 * .        |        .        . | cell     | .        .        |        .
 * ---------------------------- |----------| ----------------------------
 * I0       I1       I2         |          |          I0       I1       I2
 *
 * Momentum and energy discontinuities over the turbomachine are modelled as
 * follows:
 *   - Set T_in = T_left and P_in = P_left
 *   - Set P_out = P_right
 *   - Calculate mass_flux = f(P_in/P_out)
 *   - Calculate W_sh = g(T_in, P_in/P_out)
 *   - Set e_flux_in = mass_flux * e_in, set e_flux_out = e_flux_in - W_sh
 *   - Calculate v_in = mass_flux/rho_in, v_out = mass_flux/rho_out
 *   - Calculate momentum fluxes as v_flux_in = mass_flux * v_in + P_in,
 *       v_flux_out = mass_flux * v_out + P_out
 *   - Assign relevant flux values to FluidNodes left and right.
 * Cells lleft and rright are not used when modelling the turbomachine.
 *
 * Regular TransportInterface type interfaces are used between pairs lleft-left
 * and rright-right.
 * By default, these would look one cell past the discontinuity, leading to
 * unstable results.
 * We account for this by using a constant gradient approach: when forming I2 on
 * the upstream side, we assume conditions in cell right are the same as those
 * in cell left. Vice-versa for the downstream side.
 *
 * Wish list:
 * - Eventually it would be nice to have the map turbomachine node inherit from
 *   FluidDomain so that I can put a stream through it.
 *   This will give me access to functionalities like tracking stream mass etc
 *   (though I guess I can probably just track reservoir mass to obtain this
 *   result).
 * - Would be nice to be able to define streams through compound components in
 *   qdef file, though this seems like it's going a bit far for my PhD.
 */
void build_map_turbomachine_system(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table,
    const string turbomachine_type)
{
    import q1dcfd.utils.dictionary_tools: new_dict, request, demand, require_empty,
        retrieve, required, request_copy, demand_copy;
    import q1dcfd.domains.plenum: build_plenum;
    import q1dcfd.domains.map_turbomachine: build_map_turbomachine;

    scope(exit) defs.require_empty(name); // Unrecognized fields, exit

    // Plenum definitions, only the initial conditions change for inflow/outflow
    auto inflow_plenum_defs = new_dict!string
        .demand_copy(defs, "cross_section")
        .demand_copy(defs, "plenum_length", "length")
        .demand_copy(defs, "initial_upstream_temperature", "initial_temperature")
        .demand_copy(defs, "initial_upstream_pressure", "initial_pressure")
        .demand(defs, "initial_upstream_velocity", "initial_velocity")
        .request_copy(defs, "eos")
        .request_copy(defs, "save_plenum", "save")
        .request_copy(defs, "f_corr_name");

    auto outflow_plenum_defs = new_dict!string
        .demand_copy(defs, "cross_section")
        .demand_copy(defs, "plenum_length", "length")
        .demand_copy(defs, "initial_downstream_pressure", "initial_pressure")
        .demand(defs, "initial_downstream_temperature", "initial_temperature")
        .demand(defs, "initial_downstream_velocity", "initial_velocity")
        .request(defs, "eos")
        .request(defs, "save_plenum", "save")
        .request(defs, "f_corr_name");

    auto turbomachine_defs = new_dict!string
        .demand(defs, "cross_section")
        .demand(defs, "map")
        .demand(defs, "plenum_length")
        .demand(defs, "initial_upstream_temperature")
        .demand(defs, "initial_upstream_pressure")
        .demand(defs, "initial_downstream_pressure")
        .demand(defs, "stream")
        .request(defs, "initial_motor_torque")
        .request(defs, "initial_mass_flow_rate")
        .request(defs, "save_turbomachine", "save")
        .request(defs, "coolprop_fluid")
        .request(defs, "initial_speed")
        .request(defs, "motor_torque")
        .request(defs, "rotor_inertia")
        .request(defs, "fixed_speed")
        // Map scaling parameters
        .request(defs, "k_dh")
        .request(defs, "k_eta")
        .request(defs, "er_design")
        .request(defs, "mfp_design")
        .request(defs, "T0_design")
        .request(defs, "P0_design");

    build_plenum(domains, inflow_plenum_defs, name ~ "_inflow", geometries);
    build_plenum(domains, outflow_plenum_defs, name ~ "_outflow", geometries);
    build_map_turbomachine(domains, turbomachine_defs, name ~ "_node", geometries,
            turbomachine_type);

    // Flag a connection through the turbomachine
    // This is not a stream connection (no shared EOS), it just builds the
    // map turbomachine interfaces that connect the inflow and outflow plenums
    // to the turbomachine node
    connection_table ~= from!"q1dcfd.connectors.connection".Connection(
        ["type":"MapTurbomachine"],
        [[name ~ "_inflow", name ~"_node"], 
         [name ~ "_node", name ~"_outflow"]],
        name ~ "_connection");
}


module q1dcfd.domains.heat_exchanger_mixed;

import q1dcfd.utils.metaprogramming: from;

/**
 * Builds a heat exchanger where channel[0] is an incompressible stream and
 * channel[1] is a compressible stream.
 */
void build_parallel_flow_heat_exchanger_mixed(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] domain_defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table)
{
    import q1dcfd.utils.dictionary_tools: request, demand, new_dict, require_empty,
        retrieve, defaults;
    import q1dcfd.domains: build_channel, build_incompressible_channel, build_axial_wall;
    import q1dcfd.connectors.connection: Connection;

    scope(exit) domain_defs.require_empty(name); // check for unrecognised fields

    // Incompressible channel, don't need the heat transfer settings since these
    // are incorporated into the incompressible solver rather than the node model
    auto channel0_defs = new_dict!string
        .demand(domain_defs, "nodes")
        .demand(domain_defs, "length")
        .demand(domain_defs, "n_channels")
        .demand(domain_defs, "channel[0]_cross_section", "cross_section")        
        .demand(domain_defs, "channel[0]_initial_temperature", "initial_temperature")
        .demand(domain_defs, "channel[0]_initial_pressure", "initial_pressure")
        .demand(domain_defs, "channel[0]_initial_velocity", "initial_velocity")
        .request(domain_defs, "save_channel[0]", "save")
        .request(domain_defs, "channel[0]_node_model", "node_model");

    // The compressible channel and wall are treated the same as for a regular HX
    auto channel1_defs = ["nodes": channel0_defs["nodes"],
                          "length": channel0_defs["length"],
                          "n_channels": channel0_defs["n_channels"]]
        .demand(domain_defs, "channel[1]_cross_section", "cross_section")
        .demand(domain_defs, "channel[1]_initial_temperature", "initial_temperature")
        .demand(domain_defs, "channel[1]_initial_pressure", "initial_pressure")
        .demand(domain_defs, "channel[1]_initial_velocity", "initial_velocity")
        .request(domain_defs, "f_corr[1]", "f_corr_name")
        .request(domain_defs, "ht_corr[1]", "ht_corr_name")
        .request(domain_defs, "thin_film_heat_transfer[1]", "thin_film_heat_transfer")
        .request(domain_defs, "save_channel[1]", "save")
        .request(domain_defs, "channel[1]_node_model", "node_model");

    auto wall_defs = ["nodes": channel0_defs["nodes"],
                      "length": channel0_defs["length"]]
        .demand(domain_defs, "wall_cross_section", "cross_section")
        .demand(domain_defs, "wall_initial_temperature", "initial_temperature")
        .request(domain_defs, "save_wall", "save")
        .request(domain_defs, "wall_node_model", "node_model");

    // Build the streams and wall
    build_incompressible_channel(domains, channel0_defs, name ~ "[0]", geometries);
    build_channel(domains, channel1_defs, name ~ "[1]", geometries);
    build_axial_wall(domains, wall_defs, name ~ "[2]", geometries);

    // Connect in coflow or counterflow configuration
    auto orientation = domain_defs.retrieve("orientation").defaults("counterflow");
    auto name_convection0 = domain_defs
        .retrieve("name_convection[0]")
        .defaults(name ~ "_convection[0]");
    auto args = ["type":"convection"];
    // Make connection interface between the compressible stream and the wall.
    // The order (wall first or channel first) sets whether the stream will be
    // connected in counterflow or coflow config (see the 'connect' function).
    if(orientation == "counterflow")
    {
        connection_table ~= Connection(args.dup, [[name ~ "[0]", name ~ "[2]"]], name_convection0);
    }
    else if(orientation == "parallelflow")
    {
        connection_table ~= Connection(args.dup, [[name ~ "[2]", name ~ "[0]"]], name_convection0);
    }
    else
    {
        import q1dcfd.utils.errors: InvalidDefinition;
        import std.format;
        throw new InvalidDefinition(
            "Orientation %s not understood in %s".format(orientation, name));
    }

    // Convective connection between the incompressible stream and the wall
    connection_table ~= Connection(
        args,
        [[name ~ "[2]", name ~ "[1]"]],
        domain_defs.retrieve("name_convection[1]").defaults(name ~ "_convection[1]"));
}


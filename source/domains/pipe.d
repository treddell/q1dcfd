module q1dcfd.domains.pipe ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Constructs a pipe as a compound domain consisting of a channel and an axial
 * wall
 * 
 *     x
 *     --->
 * 
 *     ---------------------------------------------------------
 * 
 *     wall
 * 
 *     ---------------------------------------------------------
 *      
 *     --> channel[0]
 * 
 *     ---------------------------------------------------------
 * 
 * Required Arguments:
 *     type                   : Pipe 
 *     channel_cross_section  : A reference to the ChannelCrossSection defining the pipe
 *                                 flow section
 *     wall_cross_section     : A reference to the WallCrossSection defining the pipe 
 *                                 wall
 *     channel_initial_temperature
 *                            : initial temperature distribution of the channel 
 *                                 flow section as a MathFunction(x)
 *     channel_initial_pressure
 *                            : Initial pressure distribution of the channel flow 
 *                                 section as a  MathFunction(x)
 *     channel_initial_velocity       
 *                            : Initial velocity distribution of the channel flow 
 *                                 section as a MathFunction(x)
 *     wall_initial_temperature
 *                            : Initial temperature distribution of the wall as a
 *                                 MathFunction(x)
 *     density                : constant homogenous density of the wall (kg/m3)
 *     conductivity           : constant homogenous conductivity of the wall (W/m.K)
 *     specific_heat          : constant homogenous specific heat of the wall (K/kg.K)
 *     
 *     nodes                  : positive integer, number of control volumes to 
 *                                 subdivide domain into
 *     length                 : length of pipe (m)
 *
 * Optional Arguments    
 *     channel_node_model     : A valid channel node model
 *     wall_node_model        : A valid axial wall node model
 *     f_corr                 : name of the friction factor correlation
 *     ht_corr                : name of the heat transfer correlation
 *     n_channels             : number of identical channels to branch flow into
 *     save_channel           : Ordered list of attributes per channel control 
 *                                 volume to save to file, default (p, T, v)
 *     save_wall              : Ordered list of attributes for each control volume
 *                                 to save to file, default (T)
 *     thin_film_heat_transfer: true/false, if true will calculate heat transfer 
 *                                 correlations at the thin film conditions 
 *     convection_name        : name to give to the convective heat transfer
 *     
 */
void build_pipe(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] domain_defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table)
{
    import q1dcfd.utils.dictionary_tools: new_dict, request, demand,
        retrieve, defaults, require_empty ;
    import q1dcfd.domains: build_channel, build_axial_wall ;

    // make sure there are no remaining arguments upon exit
    scope(exit) domain_defs.require_empty(name) ;

    auto channel_defs = new_dict!string
        .demand(domain_defs, "nodes")
        .demand(domain_defs, "length")
        .demand(domain_defs, "channel_cross_section", "cross_section")
        .demand(domain_defs, "channel_initial_temperature", "initial_temperature")
        .demand(domain_defs, "channel_initial_pressure", "initial_pressure")
        .demand(domain_defs, "channel_initial_velocity", "initial_velocity")
        .request(domain_defs, "f_corr_name")
        .request(domain_defs, "ht_corr_name")
        .request(domain_defs, "thin_film_heat_transfer")
        .request(domain_defs, "save_channel", "save")
        .request(domain_defs, "channel_node_model", "node_model") ;

    auto wall_defs = ["nodes": channel_defs["nodes"],
                      "length": channel_defs["length"]]
        .demand(domain_defs, "wall_cross_section", "cross_section")
        .request(domain_defs, "save_wall", "save")
        .request(domain_defs, "wall_initial_temperature", "initial_temperature")
        .request(domain_defs, "wall_node_model", "node_model") ;

    // build the channel component of the pipe
    build_channel(domains, channel_defs, name ~"[0]", geometries) ;

    // build the pipe wall component of the pipe
    build_axial_wall(domains, wall_defs, name ~"[1]", geometries) ;

    // flag a connection between the channel and wall
    connection_table ~= from!"q1dcfd.connectors.connection".Connection(
        ["type":"convection"],
        [[name ~"[1]", name ~"[0]"]],
        domain_defs.retrieve("convection_name").defaults(name ~ "_convection")) ;
}


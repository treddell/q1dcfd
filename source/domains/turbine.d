module q1dcfd.domains.turbine ;

import q1dcfd.utils.metaprogramming: from ;

/** 
 * Construct a turbine as a compound domain consisting of an outflow boundary ->
 * momentum energy jump discontinuity -> inflow boundary
 * 
 * A momemtum and energy discontinuity linking two boundary nodes
 * 
 *              ---> g(t)
 *     ---------               --------- rho(e + 1/2 v^2)
 *     |       | ---> F(X)---> |       | <----
 *     |       | --->     ---> |       |
 *     ---------               ---------
 *     upstream  
 * 
 * Required Arguments:
 *     type                   : Turbine 
 *     initial_downstream_temperature
 *                            : initial temperature downstream of the discontinuity
 *     initial_downstream_pressure
 *                            : Initial pressure downstream of the discontinuity
 *     initial_downstream_velocity       
 *                            : Initial velocity downstream of the discontinuity
 *     initial_upstream_temperature
 *                           : initial temperature upstream of the discontinuity
 *    initial_upstream_pressure
 *                           : Initial pressure upstream of the discontinuity
 *    initial_upstream_velocity       
 *                           : Initial velocity upstream of the discontinuity
 *    dataset                : The tabular fluids dataset to use
 *    efficiency_map         : Turbine isentropic efficiency, must be parsable
 *                                by the read_map function
 *    shaft_speed            : function describing the shaft speed boundary condition
 *                                 as a MathFuction(t)
 *
 * Optional Arguments    
 *    downstream_node_model  : A valid inflow node model
 *    upstream_node_model    : A valid outflow node model
 *    save_downstream        : Ordered list of attributes of the downstream control
 *                                volume to save to file, default (p, T, v)
 *    save_upstream          : Ordered list of attributes of the upstream control
 *                                volume to save to file, default (p, T, v)
 *    save_discontinuity     : Ordered list of attributes of the discontinuity to
 *                                save to file, default ()
 *    target_function        : the target property used to guide the formation
 *                                of the upstream travelling accoustic wave as a
 *                                MathFunction(t). Using the default node model,
 *                                this will define a target pressure. If not set
 *                                then must be defined by implicit connections or
 *                                controllers
 *    target_coeff           : scale factor for the target function
 *
 *    mass_coeff             : scale factor for the mass flow rate, TargetMassNode 
 *                             model only
 *    max_dpdt               : maximum rate of change of pressure
 *    eos_upstream           : The upstream equation of state
 *    eos_downstream         : The downstream equation of state
 *    eos_discontinuity      : The discontinuity equation of state
 */
void build_turbine(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table)
{
    import q1dcfd.utils.dictionary_tools: new_dict, request, demand, require_empty,
        retrieve, required;
    import q1dcfd.domains: build_inflow, build_outflow, build_momentum_energy_jump ;

    // check for unrecognised fields
    scope(exit) defs.require_empty(name) ;

    auto jump_defs = new_dict!string
        .demand(defs, "initial_downstream_temperature", "initial_temperature")
        .demand(defs, "initial_downstream_pressure", "initial_pressure")
        .demand(defs, "initial_downstream_velocity", "initial_velocity")
        .demand(defs, "efficiency_map", "efficiency_map")
        .request(defs, "save_discontinuity", "save ")
        .request(defs, "shaft_speed", "shaft_speed")
        .request(defs, "eos_discontinuity", "eos")
        .request(defs, "invert_efficiency", "invert_efficiency") ;

    auto outflow_defs = new_dict!string
        .demand(defs, "initial_upstream_temperature", "initial_temperature")
        .demand(defs, "initial_upstream_pressure", "initial_pressure")
        .demand(defs, "initial_upstream_velocity", "initial_velocity")
        .request(defs, "priority_queue", "priority_queue")
        .request(defs, "target_coeff", "target_coeff")
        .request(defs, "target_function", "target_function")
        .request(defs, "save_upstream", "save")
        .request(defs, "upstream_node_model", "node_model")
        .request(defs, "max_dpdt", "max_dpdt")
        .request(defs, "mass_coeff", "mass_coeff")
        .request(defs, "eos_upstream", "eos") ;

    auto inflow_defs = ["initial_temperature": jump_defs["initial_temperature"],
                        "initial_pressure": jump_defs["initial_pressure"],
                        "initial_velocity": jump_defs["initial_velocity"],
                        "priority_queue": "true"]
        .request(defs, "save_downstream", "save")
        .request(defs, "downstream_node_model", "node_model")
        .request(defs, "eos_downstream", "eos") ;

    // build the upstream state of the turbine
    build_outflow(domains, outflow_defs, name ~"[0]", geometries) ;

    // build the discontinuity of the turbine
    build_momentum_energy_jump(domains, jump_defs, name ~"[2]", geometries) ;

    // build the downstream state of the turbine
    build_inflow(domains, inflow_defs, name ~"[1]", geometries) ;

    // flag a connection through the turbine
    connection_table ~= from!"q1dcfd.connectors.connection".Connection(
        ["type":"stream", "data": defs.retrieve("dataset").required!string],
        [[name ~"[0]", name ~"[2]"], 
         [name ~ "[2]", name ~"[1]"]] ,
        name ~"_flowpath") ;
}

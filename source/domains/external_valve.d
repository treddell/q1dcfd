module q1dcfd.domains.external_valve ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Does most of the actual work of build_external_valve
 */
private void generate_external_valve(NodeModel)(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs,
    const string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to ;
    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults, required,
        subset, map_from, greater;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;
    import q1dcfd.eos: backend ;
    import q1dcfd.types.geometry: ChannelCrossSection ;

    scope(exit) defs.require_empty(name) ;
    
    auto valve = new ExternalValve!(NodeModel)(
        name,
        defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
        defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p","T","v"]),
        defs.retrieve("f_corr_name").defaults("SwitchFrictionFactor"),
        defs.retrieve("ht_corr_name").defaults("SwitchHeatTransfer"),
        defs.retrieve("thin_film_heat_transfer").defaults(true),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
        defs.retrieve("mass_flow_rate").defaults("Unassigned").read_boundary_function,
        defs.retrieve("length").required!double.greater(0.0)) ;

    domains.add(name, valve) ;
}

/**
 * Constructs a Valve object from a set of domain definitions 
 *
 * Required Arguments:
 *     type                   : ExternalValve
 *     cross_section          : A reference to the CrossSection definition
 *     initial_temperature    : Initial temperature distribution as a 
 *                                 MathFunction(x)
 *     initial_pressure       : Initial pressure distribution as a MathFunction(x)
 *     initial_velocity       : Initial velocity distribution as a MathFunction(x)
 *     length                 : length of channel (m)
 * Optional Arguments    
 *     mass_flow_rate         : A MathFunction(t) describing the external mass flow
 *                                 rate (kg/s)
 *     node_model             : A valid external valve node model
 *     f_corr                 : name of the friction factor correlation
 *     n_channels             : number of identical channels to branch flow into
 *     save                   : Ordered list of attributes per control volumes to 
 *                                 save to file, default (p, T, v, mdot_external)
 */
void build_external_valve(ref from!"q1dcfd.domains".DomainContainer domains,
                          string[string] defs,
                          string name,
                          from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults ;

    // get the node model
    auto model = defs.retrieve("node_model").defaults("ExternalValveNode") ;

    // delegate the actual work to the template constructor
    switch(model)
    {
        case "ExternalValveNode":
        {
            import q1dcfd.control_volumes: ExternalValveNode ;
            generate_external_valve!(ExternalValveNode)(domains, defs, name, geometries) ;
            break ;
        }
        default:
        {
            import std.format ;
            import q1dcfd.utils.errors: InvalidDefinition ;

            throw new InvalidDefinition(
                "Unrecognized node_model %s for ExternalValve %s".format(model, name)) ;
        }
    }
}

abstract class AbstractExternalValve: from!"q1dcfd.domains".FluidDomain
{
    import q1dcfd.config: Transient, Real ;
    import q1dcfd.domains: Domain ;
    import q1dcfd.utils.math: MathFunction ;

    override @property string type() { return "ExternalValve"; }

    // valve length
    immutable Real length ;

    // References to the left and right channels
    Domain left_channel ;
    Domain right_channel ;
    
    private
    {
        // external mass flow rate transient
        bool mass_flow_rate_is_linked = false ;
        MathFunction mass_flow_rate ;
    }
    
    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const string f_corr_name,
         const string ht_corr_name,
         const bool thin_film_heat_transfer,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         MathFunction mass_flow_rate,
         const double length)
    {
        super(name,
              geometry,
              saved_fields,
              1,
              f_corr_name,
              ht_corr_name,
              1,
              thin_film_heat_transfer,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model) ;

        this.length = length ;

        this.mass_flow_rate = mass_flow_rate ;
        if(mass_flow_rate.not_null) designate_boundary_linked("mass_flow") ;
    }

    override void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln ;
        super.write_metadata_to_disk(file) ;
        file.writefln("f_corr: %s: %s", f_corr_name, "str") ;
        file.writefln("h_corr: %s: %s", ht_corr_name, "str") ;
    }

    /**
     * Generates the geometry of each control volume, based off provided 
     * geometry for the entire channel. Returns as a NodeGeometry
     * object
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb) ;
        result.htA = domain_geometry.heat_area(xa, xb) ;
        result.specific_density = 1.0/result.volume ;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb)) ;
        result.delta = domain_geometry.roughness(0.5*(xa + xb)) ;

        return result ;
    }

    override void designate_boundary_linked(const string port)
    {
        import std.stdio ;
        import std.format ;

        import q1dcfd.utils.errors: InvalidDefinition, MultipleConnection ;

        if(port == "mass_flow")
        {
            if(!mass_flow_rate_is_linked)
            {
                mass_flow_rate_is_linked = true ;
                return ;
            }

            throw new MultipleConnection(name, port) ;
        }
        throw new InvalidDefinition(
            "boundary function %s not understood".format(port)) ;
    }
}

/**
 * A valve consisting of a single node, which receives an external mass
 * flow rate.
 */
final class ExternalValve(NodeModel): AbstractExternalValve
{
    import q1dcfd.config: Real ;
    import q1dcfd.control_volumes: Node ;
    import q1dcfd.interfaces.root_interface: Interface ;

    override @property string node_type() {return typeid(NodeModel).toString ;}
    
    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const string f_corr_name,
         const string ht_corr_name,
         const bool thin_film_heat_transfer,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         MathFunction mass_flow_rate,
         const double length)
    {
        super(name,
              geometry,
              saved_fields,
              f_corr_name,
              ht_corr_name,
              thin_film_heat_transfer,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model,
              mass_flow_rate,
              length) ;

        this.order = NodeModel.order ;
    }

    // Connect the external valve transient (if assigned)
    override void link_boundaries(ref Node[] nodes)
    {
        import std.conv: to ;

        if(mass_flow_rate.not_null)
        {
            nodes[node_index_l].to!NodeModel.mass_flow_rate = &mass_flow_rate.evaluate ;
        }
    }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.stdio ;
        import std.conv: to ;

        import q1dcfd.config: Address ;
        import q1dcfd.types.geometry: ChannelCrossSection ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto node_geometry = this.generate_nodal_geometry(
            geometry.to!ChannelCrossSection, 0.0, length) ;

        this.node_coords[0] = 0.5*length ;
        
        auto address = Address(index_l, index_l + NodeModel.order,
                               write_index_l, node_index_l, name) ;
        
        // Only a single node to create
        nodes[node_index_l] = new NodeModel(
            address, stream, node_geometry, f_corr_name, ht_corr_name,
            thin_film_heat_transfer, n_channels, backend_model) ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format ;
        import q1dcfd.eos: generate_eos ;
        import q1dcfd.utils.math: square ;

        // populate with initial data
        auto p = initial_pressure_distribution(0.0) ;
        auto T = initial_temperature_distribution(0.0) ;
        auto v = initial_velocity_distribution(0.0) ;

        auto eos = generate_eos(stream.primitive_data,
                                backend_model,
                                "Initialisation eos of %s".format(name)) ;
        eos.update(p,T) ;

        state[ode_index_l..ode_index_l + NodeModel.order] = [
            eos.rho, 
            eos.rho*v, 
            eos.rho*(eos.e + 0.5*v.square)
        ] ;

        nodes[node_index_l].update(state, 0.0) ;
    }
}

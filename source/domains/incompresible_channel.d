module q1dcfd.domains.incompressible_channel;

import q1dcfd.utils.metaprogramming: from;

private void generate_incompressible_channel(T)(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to;

    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, subset, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.eos: backend;
    import q1dcfd.types.geometry: ChannelCrossSection;

    scope(exit) defs.require_empty(name);

    // Since the incompressible channel must be connected to an external
    // incompressible solver, only an initial temperature profile is required to
    // initialise cross-wall heat transfer rates, however we set that all
    // initial props are required for completeness and extensibility
    auto channel = new IncompressibleChannel!(T)(
        name,
        defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
        defs.retrieve("save").subset(T.STATE_FIELDS).defaults(["p", "T", "v"]),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        defs.retrieve("nodes").required!uint.greater(3),
        defs.retrieve("n_channels").required!uint.greater(0),
        defs.retrieve("length").required!double.greater(0.0));

    domains.add(name, channel);
}

void build_incompressible_channel(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults;

    auto model = defs.retrieve("node_model").defaults("IncompressibleNode");

    // Delegate the actual work to the template constructor
    switch(model)
    {
        case "IncompressibleNode":
        {
            import q1dcfd.control_volumes: IncompressibleNode;
            generate_incompressible_channel!(IncompressibleNode)(domains, defs, name,
                                                                 geometries);
            return;
        }
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition;
            import std.format;
            throw new InvalidDefinition(
                "Unrecognized node_model %s for Channel %s".format(model, name));
        }
    }
}

/*
 * A channel that uses incompressible nodes which are updated using a separate
 * 'PisoSolver' class.
 */
final class IncompressibleChannel(NodeModel): from!"q1dcfd.domains.domain".Domain
{
    import q1dcfd.control_volumes: Node;
    import q1dcfd.config: Real, Address, Transient;

    // Debugging and connection matching information
    override @property string type() { return "IncompressibleChannel"; }
    override @property string node_type() { return typeid(NodeModel).toString; }

    immutable uint n_channels;
    immutable Real length;

    Transient initial_temperature_distribution;
    Transient initial_pressure_distribution;
    Transient initial_velocity_distribution;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         const uint n_nodes,
         const uint n_channels,
         const Real length)
    {
        super(name, geometry, saved_fields, n_nodes);
        this.n_channels = n_channels;
        this.length     = length;
        this.order      = NodeModel.order*n_nodes;

        this.initial_temperature_distribution = initial_temperature_distribution;
        this.initial_pressure_distribution    = initial_pressure_distribution;
        this.initial_velocity_distribution    = initial_velocity_distribution;
    }

    override void discretise(
        ref Node[] nodes,
        ref from!"q1dcfd.interfaces.root_interface".Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.conv: to;
        import q1dcfd.types.geometry: ChannelCrossSection;

        // Control volume axial length, required for plotting
        auto dx = length / n_nodes.to!double;

        this.node_index_l  = node_index_l;
        this.node_index_r  = node_index_l + n_nodes;
        this.ode_index_l   = index_l;
        this.write_index_l = write_index_l;

        uint left = index_l;
        uint w_left = write_index_l;

        // Construct each control volume and add to the simulation list
        foreach(immutable i; 0..n_nodes)
        {
            auto xa = i * dx;
            auto xb = (i+1) * dx;
            node_coords[i] = 0.5 * (xa + xb);
            auto node_geometry = this.generate_nodal_geometry(
                geometry.to!ChannelCrossSection, xa, xb); // Not always working

            auto id = node_index_l + i;
            auto address = Address(left, left + NodeModel.order, w_left, id, name);
            nodes[id] = new NodeModel(address, node_geometry, n_channels);

            // Increment indicies to next node
            w_left += saved_fields.length;
            left += NodeModel.order;
        }
    }

    // No EOS, so only initialises p, T, v.
    // Technically, only temperature is required (to initialise cross-wall heat
    // transfer), though we include all properties for completeness (see
    // 'generate_incompressible_channel')
    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format;
        import std.conv: to;

        Real dx = length/to!double(n_nodes);

        foreach(immutable i; 0..n_nodes)
        {
            Real x = 0.5*(2*i + 1)*dx;
            Real p = initial_pressure_distribution(x);
            Real T = initial_temperature_distribution(x);
            Real v = initial_velocity_distribution(x);

            nodes[node_global_id(i)].to!NodeModel.p = p;
            nodes[node_global_id(i)].to!NodeModel.T = T;
            nodes[node_global_id(i)].to!NodeModel.v = v;
        }
    }

    override void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln;
        super.write_metadata_to_disk(file);
    }

    /*
     * Generates the geometry of each control volume, based off provided 
     * for the entire channel. Returns as a NodeGeometry
     * object
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        // TODO: Should import this instead of copying
        import q1dcfd.domains.domain: Domain ;
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb) ;
        result.htA = domain_geometry.heat_area(xa, xb) ;
        result.specific_density = 1.0/result.volume ;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb)) ;
        result.delta = domain_geometry.roughness(0.5*(xa + xb)) ;
        return result ;
    }
}

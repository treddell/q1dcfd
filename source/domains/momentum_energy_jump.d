module q1dcfd.domains.momentum_energy_jump ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Does most of the actual work of build_mej
 *
 * Currently only has a single model type, may change in future
 */
private void generate_mej(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    const string name)
{
    import q1dcfd.utils.dictionary_tools: retrieve, required, map_from, subset, defaults,
        require_empty;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;
    import q1dcfd.control_volumes: MomentumEnergyJumpNode ;
    import q1dcfd.correlations.maps: read_map, invert ;
    import q1dcfd.eos: backend ;

    // check for unrecognised arguments
    scope(exit) defs.require_empty(name) ;

    auto mej = new MomentumEnergyJump(
        name,
        defs.retrieve("save").subset(MomentumEnergyJumpNode.STATE_FIELDS)
            .defaults(["delta_h","power"]),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
        defs.retrieve("efficiency_map").required!string.read_map.invert(
            defs.retrieve("invert_efficiency").defaults(false)),
        &defs.retrieve("shaft_speed").defaults("0.0").read_boundary_function.evaluate) ;

    domains.add(name, mej) ;
}

/**
 * A discontinuity in momentum and energy, occurs between linked boundary 
 * conditions
 *
 * Required Arguments:
 *     type                   : MomentumEnergyJump 
 *     initial_temperature    : Initial temperature of the discontinuity
 *     initial_pressure       : Initial pressure of the discontinuity
 *     initial_velocity       : Initial velocity of the discontinuity
 *     efficiency_map         : A definition for the 'discontinuity efficiency'
 *                                relating properties across the jump. For a 
 *                                turbine model this will be isentropic efficiency,
 *                                while for a compressor it is 
 *                                1/isentropic efficiency. Must be an input parsable
 *                                by the read_map method
 *
 * Optional Arguments    
 *     save                   : Ordered list of attributes for each control volume
 *                                 to save to file, default (eta)
 */
void build_momentum_energy_jump(
    ref from!"q1dcfd.domains".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    // for consistency with other domain constructors
    generate_mej(domains, defs, name) ;
}

/**
 *    A momemtum and energy discontinuity linking two boundary nodes
 *
 *             ---> g(t)
 *    ---------               --------- rho(e + 1/2 v^2)
 *    |       | ---> F(X)---> |       | <----
 *    |       | --->     ---> |       |
 *    ---------               ---------
 *    upstream                downstream
 *
 * Only defined for a single node model currently
 */
final class MomentumEnergyJump: from!"q1dcfd.domains".FluidDomain
{
    import q1dcfd.tables.extract_table_data: TableData ;
    import q1dcfd.config: Transient, Real ;
    import q1dcfd.domains: Domain ;
    import q1dcfd.control_volumes: Node, MomentumEnergyJumpNode ;
    import q1dcfd.correlations.maps: EfficiencyMap ;
    import q1dcfd.interfaces.root_interface: Interface ;

    // tabular data sets
    TableData isentropic_data ;

    // discontinuity transfer efficiency
    EfficiencyMap efficiency_map ;

    // Shaft Speed boundary condition
    Transient shaft_speed ;

    // upstream and downstream domains, required for initialisation
    Domain upstream ;
    Domain downstream ; 

    override @property string type() {return "MomentumEnergyJump" ;}
    override @property string node_type()
    {
        return typeid(MomentumEnergyJumpNode).toString ;
    }

    this(const string name,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         EfficiencyMap efficiency_map,
         Transient shaft_speed)
    {
        import q1dcfd.types.geometry: Dimensionless ;

        super(name,
              new Dimensionless,
              saved_fields,
              1,
              "NoFriction",
              "NoHeatTransfer",
              1,
              false,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model) ;

        this.efficiency_map = efficiency_map ;
        this.shaft_speed = shaft_speed ;
        this.order = MomentumEnergyJump.order ;
        this.node_coords[0] = 0.0 ;

    }

    override void discretise(
        ref Node[] nodes, 
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import q1dcfd.config: Address ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto address = Address(index_l, index_l + MomentumEnergyJump.order,
            write_index_l, node_index_l, name) ;

        nodes[node_index_l] = new MomentumEnergyJumpNode(
            address,
            stream, 
            efficiency_map,
            shaft_speed,
            backend_model) ;
        
        nodes[node_index_l].priority_queue = true ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.conv: to ;
        import std.format ;
        import q1dcfd.utils.math: square ;
        import q1dcfd.eos: generate_eos ;

        auto node = nodes[node_global_id(0)].to!MomentumEnergyJumpNode ;

        auto p_d = initial_pressure_distribution(0.0) ;
        auto T_d = initial_temperature_distribution(0.0) ;
        auto v_d = initial_velocity_distribution(0.0) ;

        auto eos = generate_eos(stream.primitive_data,
                                backend_model,
                                "Initialisation eos of %s".format(name)) ;
        eos.update(p_d,T_d) ;

        // give a pre-emptive update to the downstream state
        state[ode_index_l] = eos.rho*(eos.e + 0.5*v_d.square) ;

        node.cons_vars = [eos.rho, eos.e] ;
        node.update(state, 0.0) ;
    }
}

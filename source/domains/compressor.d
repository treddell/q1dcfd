module q1dcfd.domains.compressor ;

import q1dcfd.utils.metaprogramming: from ;

/** 
 * Construct a compressor as a compound domain consisting of an outflow boundary ->
 * momentum energy jump discontinuity -> inflow boundary
 * 
 * A momemtum and energy discontinuity linking two boundary nodes
 * 
 *              ---> g(t)
 *     ---------               --------- rho(e + 1/2 v^2)
 *     |       | ---> F(X)---> |       | <----
 *     |       | --->     ---> |       |
 *     ---------               ---------
 *     upstream  
 * 
 * Required Arguments:
 *     type                   : Compressor
 *     initial_downstream_temperature
 *                            : initial temperature downstream of the discontinuity
 *     initial_downstream_pressure
 *                            : Initial pressure downstream of the discontinuity
 *     initial_downstream_velocity       
 *                            : Initial velocity downstream of the discontinuity
 *     initial_upstream_temperature
 *                            : initial temperature upstream of the discontinuity
 *     initial_upstream_pressure
 *                            : Initial pressure upstream of the discontinuity
 *     initial_upstream_velocity       
 *                            : Initial velocity upstream of the discontinuity
 *     dataset                : The tabular fluids dataset to use
 *     efficiency_map         : Compressor isentropic efficiency, must be parsable
 *                                 by the read_map function
 *     shaft_speed            : function describing the shaft speed boundary condition
 *                                 as a MathFuction(t)
 * 
 * Optional Arguments    
 *    downstream_node_model  : A valid inflow node model
 *    upstream_node_model    : A valid outflow node model
 *    save_downstream        : Ordered list of attributes of the downstream control
 *                                volume to save to file, default (p, T, v)
 *    save_upstream          : Ordered list of attributes of the upstream control
 *                                volume to save to file, default (p, T, v)
 *    save_discontinuity     : Ordered list of attributes of the discontinuity to
 *                                save to file, default ()
 *    target_function        : the target property used to guide the formation
 *                                of the upstream travelling accoustic wave as a
 *                                MathFunction(t). Using the default node model,
 *                                this will define a target pressure. If not set
 *                                then must be defined by implicit connections or
 *                                controllers
 *    target_coeff           : scale factor for the target function
 *    mass_coeff             : scale factor for the mass flow rate, TargetMassNode 
 *                                model only
 *    max_dpdt               : maximum rate of change of pressure
 *
 */
void build_compressor(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table)
{
    import q1dcfd.domains: build_turbine ;

    defs["invert_efficiency"] = "true" ;
    build_turbine(domains, defs, name, geometries, connection_table) ;
}

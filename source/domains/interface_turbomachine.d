module q1dcfd.domains.interface_turbomachine;

import q1dcfd.utils.metaprogramming: from;

private void generate_interface_turbomachine(T)(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    string component_type)
{
    import std.conv: to;

    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, subset, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.eos: backend;
    import q1dcfd.types.geometry: ChannelCrossSection;

    scope(exit) defs.require_empty(name); // Unrecognized fields, exit

    // Build the turbomachine
    if(component_type == "turbine")
    {
        auto turbine = new InterfaceTurbine!(T)(
            name,
            defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
            defs.retrieve("save").subset(T.STATE_FIELDS).defaults(["p","T","v"]),
            defs.retrieve("map").defaults("turbine-data"),
            defs.retrieve("f_corr_name").defaults("SwitchFrictionFactor"),
            defs.retrieve("initial_downstream_temperature").required!double.greater(0.0),
            defs.retrieve("initial_downstream_pressure").required!double.greater(0.0),
            defs.retrieve("initial_upstream_temperature").required!double.greater(0.0),
            defs.retrieve("initial_upstream_pressure").required!double.greater(0.0),
            defs.retrieve("initial_mass_flow_rate").required!double.greater(0.0),
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("length").required!double.greater(0.0),
            defs.retrieve("coolprop_fluid").defaults("none"),
            defs.retrieve("speed").defaults("1.0").read_boundary_function);

        domains.add(name, turbine);
    }
    else if(component_type == "compressor")
    {
        auto compressor = new InterfaceCompressor!(T)(
            name,
            defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
            defs.retrieve("save").subset(T.STATE_FIELDS).defaults(["p","T","v"]),
            defs.retrieve("map").defaults("sandia-compressor-data"),
            defs.retrieve("f_corr_name").defaults("SwitchFrictionFactor"),
            defs.retrieve("initial_downstream_temperature").required!double.greater(0.0),
            defs.retrieve("initial_downstream_pressure").required!double.greater(0.0),
            defs.retrieve("initial_upstream_temperature").required!double.greater(0.0),
            defs.retrieve("initial_upstream_pressure").required!double.greater(0.0),
            defs.retrieve("initial_mass_flow_rate").required!double.greater(0.0),
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("length").required!double.greater(0.0),
            defs.retrieve("speed").required!string.read_boundary_function);

        domains.add(name, compressor);
    }
    else
    {
        import q1dcfd.utils.errors: InvalidDefinition;
        import std.format;
        throw new InvalidDefinition(
            "Component type %s not recognized.".format(component_type));
    }
}

/*
 * Build the interface turbomachine for the selected NodeModel
 */
void build_interface_turbomachine(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table,
    string component_type)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults;

    auto model = defs.retrieve("node_model").defaults("NavierStokesNode");
    switch(model)
    {
        case "NavierStokesNode":
        {
            import q1dcfd.control_volumes: NavierStokesNode;
            generate_interface_turbomachine!(NavierStokesNode)(domains, defs, name,
                    geometries, component_type);
            return;
        }
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition;
            import std.format;
            throw new InvalidDefinition(
                "Unrecognized node_model %s for Channel %s".format(model, name));
        }
    }
}

/*
 * Interface-based turbomachine model that uses performance maps to calculate
 * the momentum and energy discontinuities that occur over the turbomachine.
 * Under this modelling paradigm, the thermodynamic response of the turbomachine
 * is assumed to be infinitely fast compared to the rest of the components.
 * The turbomachine is composed of four adjacent internal (by default,
 * Navier-Stokes) nodes separated by a special turbomachine interface.
 * Four nodes (rather than two) are used since this makes all existing linking
 * code still work correctly.
 * Details of the interface that is used to model turbomachines are given after
 * the diagram.
 *
 * Node and interface indexing scheme:
 *
 *          |-----> x                               | x = L
 *
 * leftside           TurbomachineInterface          rightside
 * domain                       |                    domain
 * ---------------------------- | ----------------------------
 * .        |        .        . | .        .        |        .
 * .        | lleft  . left   . | .  right . rright |        .
 * .        |        .        . | .        .        |        .
 * ---------------------------- | ----------------------------
 * I0       I1       I2         |          I0       I1       I2
 *
 * Momentum and energy discontinuities over the interface are modelled as
 * follows:
 *   - Set T_in = T_left and P_in = P_left
 *   - Set P_out = P_right
 *   - Calculate mass_flux = f(P_in/P_out)
 *   - Calculate W_sh = g(T_in, P_in/P_out)
 *   - Set e_flux_in = mass_flux * e_in, set e_flux_out = e_flux_in - W_sh
 *   - Calculate v_in = mass_flux/rho_in, v_out = mass_flux/rho_out
 *   - Calculate momentum fluxes as v_flux_in = mass_flux * v_in + P_in,
 *       v_flux_out = mass_flux * v_out + P_out
 *   - Assign relevant values to FluidNodes left and right.
 * Note that cells lleft and rright are not used when modelling the turbomachine
 * interface.
 *
 * Regular TransportInterface type interfaces are used for pairs lleft-left and
 * rright-right. By default, these would look one cell past the discontinuity,
 * leading to unstable results. We account for this by using a constant
 * gradient approach: when forming I2 on the upstream side, we assume
 * conditions in cell right are the same as those in cell left. Vice-versa for
 * downstream side.
 */
abstract class InterfaceTurbomachine(NodeModel) : from!"q1dcfd.domains.fluid_domain".FluidDomain
{
    import q1dcfd.control_volumes: Node;
    import q1dcfd.interfaces.root_interface: Interface;
    import q1dcfd.config: Real, Address, Transient;
    import q1dcfd.utils.math: MathFunction;
    debug import q1dcfd.utils.logging: add_message_to_log_file;

    // Debugging and connection matching information
    override @property string type() { return "InterfaceTurbomachine"; }
    override @property string node_type() { return typeid(NodeModel).toString; }

    // Turbine axial length, split equally over the inlet and outlet volume
    immutable Real length;

    Real initial_downstream_temperature;
    Real initial_downstream_pressure;
    Real initial_upstream_temperature;
    Real initial_upstream_pressure;
    Real initial_mass_flow_rate;

    MathFunction speed;

    // What maps to use, stored in $Q1D/source/maps/<map_name>
    const string map_name;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string map_name,
         const string[] saved_fields,
         const string f_corr_name,
         Real initial_downstream_temperature,
         Real initial_downstream_pressure,
         Real initial_upstream_temperature,
         Real initial_upstream_pressure,
         Real initial_mass_flow_rate,
         from!"q1dcfd.eos".backend backend_model,
         const Real length,
         MathFunction speed)
    {
        import q1dcfd.utils.math: Polynomial;
        auto dummy_data = new Polynomial([0.0]);

        super(name,
              geometry,
              saved_fields,
              // Need to use 4 nodes (2 inlet, 2 outlet). Otherwise, the cells
              // at either end of the connecting channels would 'look over' the
              // discontinuity when using the MUSCL interpolants
              4,
              f_corr_name,
              "NoHeatTransfer", // don't have heat transfer in inlet or outlet
              1, // n_channels n/a, so just set as 1
              false, // thin-film heat transfer n/a
              // We initialise the interface turbomachine using separate upstream and
              // downstream states, so we just pass in dummy values for the
              // initial_temperature_distribution etc variables
              &dummy_data.evaluate,
              &dummy_data.evaluate,
              &dummy_data.evaluate,
              backend_model);

        this.length = length;
        this.order = NodeModel.order*n_nodes;

        this.map_name = map_name;

        this.speed = speed;

        this.initial_downstream_temperature = initial_downstream_temperature;
        this.initial_downstream_pressure    = initial_downstream_pressure;
        this.initial_upstream_temperature   = initial_upstream_temperature;
        this.initial_upstream_pressure      = initial_upstream_pressure;
        this.initial_mass_flow_rate         = initial_mass_flow_rate;
    }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.conv: to;
        import std.format;
        import q1dcfd.types.geometry: ChannelCrossSection;

        // Control volume axial length
        auto dx = length/n_nodes.to!double;

        this.node_index_l  = node_index_l;
        this.node_index_r  = node_index_l + n_nodes;
        this.ode_index_l   = index_l;
        this.write_index_l = write_index_l;

        uint left = index_l;
        uint w_left = write_index_l;

        // Construct each control volume, add to the simulation list
        foreach(immutable i; 0..n_nodes)
        {
            auto xa = i*dx;
            auto xb = (i+1)*dx;
            node_coords[i] = 0.5*(xa + xb);
            auto node_geometry = this.generate_nodal_geometry(
                geometry.to!ChannelCrossSection, xa, xb);
            auto id = node_index_l + i;
            auto address = Address(left, left + NodeModel.order, w_left, id,
                                   name);
            nodes[id] = new NodeModel(address, stream, node_geometry,
                                      f_corr_name, ht_corr_name,
                                      thin_film_heat_transfer, n_channels,
                                      backend_model);

            // Increment indicies to next node
            w_left += saved_fields.length;
            left += NodeModel.order;

            debug "Constructed %s".format(nodes[id].describe).add_message_to_log_file;
        }

        // As discussed in constructor, regular TransportInterface objects use
        // MUSCL interpolants based on a 4 cell stencil; thus, here we
        // only construct the special interface between cells 2 and 3 here.
        // The remaining interfaces are constructed during component linking.
        construct_turbo_interface(nodes, interfaces);
    }

    void construct_turbo_interface(ref Node[] nodes, ref Interface[] interfaces);

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format;
        import q1dcfd.eos: generate_eos, backend;
        import q1dcfd.utils.math: square;

        uint index_l = ode_index_l;

        auto eos = generate_eos(
            stream.primitive_data,
            backend.TTSE_recovery,
            "initialisation eos of %s".format(name));

        // Initialise the two upstream nodes
        eos.update(initial_upstream_pressure, initial_upstream_temperature);
        foreach(immutable i; 0..2)
        {
            auto node = nodes[node_global_id(i)];
            double flow_area = (node.geometry.lA + node.geometry.rA) / 2;
            Real initial_upstream_velocity =
                initial_mass_flow_rate / (eos.rho * flow_area);
            state[index_l..index_l + NodeModel.order] = [
                eos.rho, 
                eos.rho * initial_upstream_velocity, 
                eos.rho * (eos.e + 0.5 * initial_upstream_velocity.square)
            ];
            node.update(state, 0.0); // Update node for t=0.0
            index_l += NodeModel.order;
        }

        // Initialise the two downstream nodes
        eos.update(initial_downstream_pressure, initial_downstream_temperature);
        foreach(immutable i; 2..4)
        {
            auto node = nodes[node_global_id(i)];
            double flow_area = (node.geometry.lA + node.geometry.rA) / 2;
            Real initial_downstream_velocity =
                initial_mass_flow_rate / (eos.rho * flow_area);
            state[index_l..index_l + NodeModel.order] = [
                eos.rho, 
                eos.rho * initial_downstream_velocity, 
                eos.rho * (eos.e + 0.5 * initial_downstream_velocity.square)
            ];
            node.update(state, 0.0); // Update node for t=0.0
            index_l += NodeModel.order;
        }
    }

    /*
     * Generates the geometry of each control volume, based off provided 
     * for the entire turbomachine. Returns as a NodeGeometry object.
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        // TODO: This function should definitely be copied somewhere as a
        // utility rather than being copied inside several classes
        import q1dcfd.domains.domain: Domain;
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb);
        result.htA = domain_geometry.heat_area(xa, xb);
        result.specific_density = 1.0/result.volume;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb));
        result.delta = domain_geometry.roughness(0.5*(xa + xb));

        return result;
    }
}

template InterfaceTurbine(NodeModel)
{
    final class InterfaceTurbine : InterfaceTurbomachine!(NodeModel)
    {
        import q1dcfd.control_volumes: Node, FluidNode;
        import q1dcfd.interfaces.root_interface: Interface;
        import q1dcfd.config: Real, Address, Transient;
        import q1dcfd.interfaces: TurbineInterface;
        import q1dcfd.utils.math: MathFunction;

        // Turbine maps require a coolprop backend due to the input pairs used
        const string coolprop_fluid;

        this(const string name,
             from!"q1dcfd.types.geometry".CrossSection geometry,
             const string[] saved_fields,
             const string map_name,
             const string f_corr_name,
             Real initial_downstream_temperature,
             Real initial_downstream_pressure,
             Real initial_upstream_temperature,
             Real initial_upstream_pressure,
             Real initial_mass_flow_rate,
             from!"q1dcfd.eos".backend backend_model,
             const Real length,
             string coolprop_fluid,
             MathFunction speed)
        {
            this.coolprop_fluid = coolprop_fluid;

            super(name, geometry, map_name, saved_fields, f_corr_name,
                    initial_downstream_temperature, initial_downstream_pressure,
                    initial_upstream_temperature, initial_upstream_pressure,
                    initial_mass_flow_rate, backend_model, length, speed);
        }

        override void construct_turbo_interface(
            ref Node[] nodes,
            ref Interface[] interfaces)
        {
            import std.conv: to;

            interfaces ~= new TurbineInterface(
                nodes[node_index_l+1].to!FluidNode,
                nodes[node_index_l+2].to!FluidNode,
                coolprop_fluid,
                map_name);
        }
    }
}

template InterfaceCompressor(NodeModel)
{
    final class InterfaceCompressor : InterfaceTurbomachine!(NodeModel)
    {
        import q1dcfd.control_volumes: Node, FluidNode;
        import q1dcfd.interfaces.root_interface: Interface;
        import q1dcfd.config: Real, Address, Transient;
        import q1dcfd.interfaces: CompressorInterface;
        import q1dcfd.utils.math: MathFunction;

        this(const string name,
             from!"q1dcfd.types.geometry".CrossSection geometry,
             const string[] saved_fields,
             const string map_name,
             const string f_corr_name,
             Real initial_downstream_temperature,
             Real initial_downstream_pressure,
             Real initial_upstream_temperature,
             Real initial_upstream_pressure,
             Real initial_mass_flow_rate,
             from!"q1dcfd.eos".backend backend_model,
             const Real length,
             MathFunction speed)
        {
            super(name, geometry, map_name, saved_fields, f_corr_name,
                    initial_downstream_temperature, initial_downstream_pressure,
                    initial_upstream_temperature, initial_upstream_pressure,
                    initial_mass_flow_rate, backend_model, length, speed);
        }

        override void construct_turbo_interface(
            ref Node[] nodes,
            ref Interface[] interfaces)
        {
            import std.conv: to;

            interfaces ~= new CompressorInterface(
                nodes[node_index_l+1].to!FluidNode,
                nodes[node_index_l+2].to!FluidNode,
                stream,
                initial_mass_flow_rate,
                initial_downstream_pressure,
                map_name);
        }
    }
}


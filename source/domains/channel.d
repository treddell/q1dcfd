module q1dcfd.domains.channel;

import q1dcfd.utils.metaprogramming: from;

/**
 * Does most of the actual work of build_channel
 */
private void generate_channel(T)(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to;

    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, subset, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.eos: backend;
    import q1dcfd.types.geometry: ChannelCrossSection;

    scope(exit) defs.require_empty(name);

    // build the channel
    auto channel = new Channel!(T)(
        name,
        defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
        defs.retrieve("save").subset(T.STATE_FIELDS).defaults(["p","T","v"]),
        defs.retrieve("nodes").required!uint.greater(3),
        defs.retrieve("f_corr_name").defaults("SwitchFrictionFactor"),
        defs.retrieve("ht_corr_name").defaults("SwitchHeatTransfer"),
        defs.retrieve("n_channels").defaults(1).greater(0),
        // defs.retrieve("thin_film_heat_transfer").defaults(true),
        defs.retrieve("thin_film_heat_transfer").defaults(false),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
        defs.retrieve("length").required!double.greater(0.0));

    domains.add(name, channel);
}

/**
 * Constructs a Channel object from a set of domain definitions 
 *
 * Required Arguments:
 *     type                   : Channel 
 *     cross_section          : A reference to the CrossSection definition
 *     initial_temperature    : Initial temperature distribution as a 
 *                                 MathFunction(x)
 *     initial_pressure       : Initial pressure distribution as a MathFunction(x)
 *     initial_velocity       : Initial velocity distribution as a MathFunction(x)
 *     nodes                  : positive integer, number of control volumes to 
 *                                 subdivide domain into
 *     length                 : length of channel (m)
 * Optional Arguments    
 *     node_model             : A valid channel node model
 *     f_corr                 : name of the friction factor correlation
 *     ht_corr                : name of the heat transfer correlation
 *     n_channels             : number of identical channels to branch flow into
 *     save                   : Ordered list of attributes per control volumes to 
 *                                 save to file, default (p, T, v)
 *     thin_film_heat_transfer: true/false, if true will calculate heat transfer 
 *                                 correlations at the thin film conditions 
 */
void build_channel(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults;

    // define the channel type based upon the node model
    auto model = defs.retrieve("node_model").defaults("NavierStokesNode");

    // delegate the actual work to the template constructor
    switch(model)
    {
        case "NavierStokesNode":
        {
            import q1dcfd.control_volumes: NavierStokesNode;
            generate_channel!(NavierStokesNode)(domains, defs, name, geometries);
            return;
        }
        // Can add additional node models here
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition;
            import std.format;
            throw new InvalidDefinition(
                "Unrecognized node_model %s for Channel %s".format(model, name));
        }
    }
    
}

/**
 * A flow section with dominant axial flow, two and three dimensional effects
 * are accounted for via friction factor and heat transfer correlations.
 *
 * Initialization of the class will generate the control volumes within the
 * channel and append them to the system, along with all (n-4) interior
 * interfaces
 *
 * Node and interface indexing scheme:
 *
 *          |-----> x                                                | x = L
 *
 * leftside                                                           rightside
 * domain                                                             domain
 * ---------------------------- ->  ---------- ->  ----------------------------
 * .        |        .        . ->  .        . ->  .        .        |        .
 * .        |   n0   .   n1   . ->  .   ni   . ->  .  n-2   .   n-1  |        .
 * .        |        .        . ->  .        . ->  .        .        |        .
 * ---------------------------- ->  ---------- ->  ----------------------------
 * L0       L1       I0          I(i-2)   I(i-1)  I(n-4)   R0       R1
 *
 * Each fluid interface requires knowledge of the states of two nodes either
 * side of it, hence, during domain initialization, there is no way to
 * construct interfaces L0, L1, R0, R1 as the leftside and rightside domains
 * have not yet been connected. These interfaces will instead be constructed
 * when the domain is connected and added directly to the system interface
 * list.
 */
final class Channel(NodeModel): from!"q1dcfd.domains.fluid_domain".FluidDomain 
{
    import q1dcfd.control_volumes: Node;
    import q1dcfd.config: Real, Address, Transient;
    debug import q1dcfd.utils.logging: add_message_to_log_file;

    // debugging and connection matching information
    override @property string type() { return "Channel"; }
    override @property string node_type() {return typeid(NodeModel).toString;}

    // channel axial length
    immutable Real length;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const uint n_nodes,
         const string f_corr_name,
         const string ht_corr_name,
         const uint n_channels,
         const bool thin_film_heat_transfer,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         const Real length)
    {
        super(name,
              geometry,
              saved_fields,
              n_nodes,
              f_corr_name,
              ht_corr_name,
              n_channels,
              thin_film_heat_transfer,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model);

        this.length = length;
        this.order = NodeModel.order*n_nodes;
    }

    override void discretise(
        ref Node[] nodes, 
        ref from!"q1dcfd.interfaces.root_interface".Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import std.conv: to;
        import std.format;
        import q1dcfd.types.geometry: ChannelCrossSection;
        import q1dcfd.interfaces: TransportInterface;
        import q1dcfd.control_volumes: FluidNode;

        // Check that channel has been assigned to a stream.
        // This check should eventually be applied to everything that inherits
        // from FluidDomain rather than just Channel.
        if(!stream_connected)
        {
            import q1dcfd.utils.errors: InvalidDefinition ;
            throw new InvalidDefinition(
                "ERROR: Attempting to initialize FluidNode that is not part of "
                ~ "any stream. "
                ~ "Check that all fluid components are part of streams.");
        }

        // control volume axial length
        auto dx = length/n_nodes.to!double;
        
        this.node_index_l = node_index_l;
        this.node_index_r = node_index_l + n_nodes;
        this.ode_index_l = index_l;
        this.write_index_l = write_index_l;

        uint left = index_l;
        uint w_left = write_index_l;

        // Construct each control volume, add to the simulation list
        foreach(immutable i; 0..n_nodes)
        {
            auto xa = i*dx;
            auto xb = (i+1)*dx;
            node_coords[i] = 0.5*(xa + xb);
            auto node_geometry = this.generate_nodal_geometry(
                geometry.to!ChannelCrossSection, xa, xb);
            
            auto id = node_index_l + i;

            auto address = Address(left, left + NodeModel.order, w_left,
                                   id, name);
            nodes[id] = new NodeModel(address, stream, node_geometry, 
                                      f_corr_name, ht_corr_name,
                                      thin_film_heat_transfer, n_channels,
                                      backend_model);
            
            // incriment indicies to next node
            w_left += saved_fields.length;
            left += NodeModel.order;

            debug "Constructed %s".format(nodes[id].describe).add_message_to_log_file;
        }

        // Construct interfaces. Can only assign from interface 2 to n-2, as the
        // left and rightside domains have not yet been assigned       
        uint m; // system node index
        
        foreach(immutable i; 0..n_nodes-3)
        {
            m = node_index_l + i;
            interfaces ~= new TransportInterface(
                nodes[m].to!FluidNode, 
                nodes[m+1].to!FluidNode,
                nodes[m+2].to!FluidNode, 
                nodes[m+3].to!FluidNode);
        }
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format;

        import q1dcfd.eos: generate_eos, backend;
        import q1dcfd.utils.math: square;

        Real dx = length/cast(double) n_nodes;

        uint index_l = ode_index_l;

        foreach(immutable i; 0..n_nodes)
        {
            auto id = node_index_l + i;
            Real x = 0.5*(2*i + 1)*dx;

            // populate with initial data
            Real p = initial_pressure_distribution(x);
            Real T = initial_temperature_distribution(x);
            Real v = initial_velocity_distribution(x);

            auto eos = generate_eos(
                stream.primitive_data,
                backend.TTSE_recovery,
                "initialisation eos of %s".format(name));
            eos.update(p,T);

            state[index_l..index_l + NodeModel.order] = [
                eos.rho, 
                eos.rho*v, 
                eos.rho*(eos.e + 0.5*v.square)
            ];

            nodes[node_global_id(i)].update(state, 0.0);

            index_l += NodeModel.order;
        }
    }

    /*
     * Generates the geometry of each control volume, based off provided 
     * for the entire channel. Returns as a NodeGeometry
     * object
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        import q1dcfd.domains.domain: Domain;
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb);
        result.htA = domain_geometry.heat_area(xa, xb);
        result.specific_density = 1.0/result.volume;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb));
        result.delta = domain_geometry.roughness(0.5*(xa + xb));

        return result;
    }

    override void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln;
        super.write_metadata_to_disk(file);
        file.writefln("f_corr: %s: %s", f_corr_name, "str");
        file.writefln("h_corr: %s: %s", ht_corr_name, "str");
    }
}

module q1dcfd.domains.plenum;

import q1dcfd.utils.metaprogramming: from;

private void generate_plenum(T)(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import std.conv: to;

    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, subset, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.eos: backend;
    import q1dcfd.types.geometry: ChannelCrossSection;

    scope(exit) defs.require_empty(name);

    auto plenum = new Plenum!(T)(
        name,
        defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
        defs.retrieve("save").subset(T.STATE_FIELDS).defaults(["p","T","v","mdot"]),
        defs.retrieve("f_corr_name").defaults("SwitchFrictionFactor"),
        &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
        &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
        defs.retrieve("eos").defaults(backend.TTSE).required!backend,
        defs.retrieve("length").required!double.greater(0.0));

    domains.add(name, plenum);
}

/**
 * Constructs a plenum object from a set of domain definitions
 *
 * Required Arguments:
 *     cross_section          : A reference to the CrossSection definition
 *     initial_temperature    : Initial temperature distribution as a
 *                                 MathFunction(x)
 *     initial_pressure       : Initial pressure distribution as a MathFunction(x)
 *     initial_velocity       : Initial velocity distribution as a MathFunction(x)
 *     nodes                  : positive integer, number of control volumes to
 *                                 subdivide domain into
 *     length                 : length of channel (m)
 * Optional Arguments
 *     node_model             : A valid node model (must be compressible)
 *     f_corr                 : name of the friction factor correlation
 *     save                   : Ordered list of attributes per control volumes to
 *                                 save to file, default (p, T, v)
 */
void build_plenum(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults;

    auto model = defs.retrieve("node_model").defaults("NavierStokesNode");

    switch(model)
    {
        case "NavierStokesNode":
        {
            import q1dcfd.control_volumes: NavierStokesNode;
            generate_plenum!(NavierStokesNode)(domains, defs, name, geometries);
            return;
        }
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition;
            import std.format;
            throw new InvalidDefinition(
                "Unrecognized node_model %s for Channel %s".format(model, name));
        }
    }
}

/**
 * This is similar to the 'Channel' domain, but with some minor changes to make
 * it suitable for use as the inlet or outlet to a turbomachine node.
 * These changes are as follows:
 *   - n_nodes is fixed to 2
 *   - No heat transfer is permitted
 *   - No interfaces are constructed until the Plenum is connected to another
 *     component; this is because there are only two cells and because the flux
 *     calculator stencil at the interfaces cannot cross the MapTurbomachine
 *     cell.
 *
 * This domain is only intended to be used as part of a MapTurbomachine super
 * domain.
 */
final class Plenum(NodeModel): from!"q1dcfd.domains.fluid_domain".FluidDomain
{
    import q1dcfd.control_volumes: Node;
    import q1dcfd.interfaces.root_interface: Interface;
    import q1dcfd.config: Real, Address, Transient;
    debug import q1dcfd.utils.logging: add_message_to_log_file;

    // debugging and connection matching information
    override @property string type() { return "Plenum"; }
    override @property string node_type() { return typeid(NodeModel).toString; }

    // channel axial length
    immutable Real length;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const string f_corr_name,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         const Real length)
    {
        super(name,
              geometry,
              saved_fields,
              // Need to use 2 nodes; otherwise, the MUSCL interpolant at the
              // end of the connecting channel would would 'look over' the
              // turbomachine discontinuity
              2,
              f_corr_name,
              "NoHeatTransfer", // don't have heat transfer in plenums
              1, // n_channels n/a, so just set as 1
              false, // thin-film heat transfer n/a
              // We initialise the interface turbomachine using separate upstream and
              // downstream states, so we just pass in dummy values for the
              // initial_temperature_distribution etc variables
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model);

        this.length = length;
        this.order = NodeModel.order*n_nodes;
    }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.conv: to;
        import std.format;
        import q1dcfd.types.geometry: ChannelCrossSection;

        // Control volume axial length
        auto dx = length/n_nodes.to!double;

        this.node_index_l  = node_index_l;
        this.node_index_r  = node_index_l + n_nodes;
        this.ode_index_l   = index_l;
        this.write_index_l = write_index_l;

        uint left = index_l;
        uint w_left = write_index_l;

        // Construct each control volume, add to the simulation list
        foreach(immutable i; 0..n_nodes)
        {
            auto xa = i*dx;
            auto xb = (i+1)*dx;
            node_coords[i] = 0.5*(xa + xb);
            auto node_geometry = this.generate_nodal_geometry(
                geometry.to!ChannelCrossSection, xa, xb);
            auto id = node_index_l + i;
            auto address = Address(left, left + NodeModel.order, w_left, id,
                                   name);
            nodes[id] = new NodeModel(address, stream, node_geometry,
                                      f_corr_name, ht_corr_name,
                                      thin_film_heat_transfer, n_channels,
                                      backend_model);

            // Increment indicies to next node
            w_left += saved_fields.length;
            left += NodeModel.order;

            debug "Constructed %s".format(nodes[id].describe).add_message_to_log_file;
        }

        // As discussed in class documentation, MUSCL interpolants are based on
        // a 4 cell stencil, so we can only construct these once this plenum is
        // linked to another component. Thus, no interfaces are constructed here.
    }

    /* This is the same as for Channel */
    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import std.format;
        import std.conv: to;
        import q1dcfd.eos: generate_eos, backend;
        import q1dcfd.utils.math: square;

        Real dx = length / to!double(n_nodes);
        uint index_l = ode_index_l;

        foreach(immutable i; 0..n_nodes)
        {
            auto id = node_index_l + i;
            Real x = 0.5*(2*i + 1)*dx;
            Real p = initial_pressure_distribution(x);
            Real T = initial_temperature_distribution(x);
            Real v = initial_velocity_distribution(x);

            auto eos = generate_eos(
                stream.primitive_data,
                backend.TTSE_recovery,
                "initialisation eos of %s".format(name));
            eos.update(p,T);

            state[index_l..index_l + NodeModel.order] = [
                eos.rho,
                eos.rho*v,
                eos.rho*(eos.e + 0.5*v.square)
            ];

            nodes[node_global_id(i)].update(state, 0.0);

            index_l += NodeModel.order;
        }
    }

    /*
     * Generates the geometry of each control volume, based off provided
     * for the entire channel. Returns as a NodeGeometry
     * object
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        import q1dcfd.domains.domain: Domain;
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb);
        result.htA = domain_geometry.heat_area(xa, xb);
        result.specific_density = 1.0/result.volume;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb));
        result.delta = domain_geometry.roughness(0.5*(xa + xb));

        return result;
    }

    override void write_metadata_to_disk(from!"std.stdio".File file)
    {
        import std.stdio: writefln;
        super.write_metadata_to_disk(file);
        file.writefln("f_corr: %s: %s", f_corr_name, "str");
    }
}


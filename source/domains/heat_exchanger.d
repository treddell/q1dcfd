module q1dcfd.domains.heat_exchanger ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Template for a parallel flow heat exchanger compound domain
 *
 * x
 * --->
 *
 *                             length
 * |-------------------------------------------------------|
 *
 * ---------------------------------------------------------
 *
 * -->/<-- channel[1]
 *
 * ---------------------------------------------------------
 *
 * wall
 *
 * ---------------------------------------------------------
 *     
 * --> channel[0]
 *
 * ---------------------------------------------------------
 *
 * Required Arguments:
 *    type                   : ParallelHE 
 *    channel[0]_geometry    : A reference to the ChannelGeometry defining the 
 *                                channel[0] flow section
 *    channel[1]_geometry    : A reference to the ChannelGeometry defining the 
 *                                channel[1] flow section
 *    wall_geometry          : A reference to the WallGeometry defining the pipe 
 *                                wall
 *    channel[0]_initial_temperature
 *                           : initial temperature distribution of the channel[0] 
 *                                flow section as a MathFunction(x)
 *    channel[0]_initial_pressure
 *                           : Initial pressure distribution of the channel[0] flow 
 *                                section as a  MathFunction(x)
 *    channel[0]_initial_velocity       
 *                           : Initial velocity distribution of the channel[0] flow 
 *                                section as a MathFunction(x)
 *    channel[1]_initial_temperature
 *                           : initial temperature distribution of the channel[1] 
 *                                flow section as a MathFunction(x)
 *    channel[1]_initial_pressure
 *                           : Initial pressure distribution of the channel[1] flow 
 *                                section as a  MathFunction(x)
 *    channel[1]_initial_velocity       
 *                           : Initial velocity distribution of the channel[1] flow 
 *                                section as a MathFunction(x)
 *    wall_initial_temperature
 *                           : Initial temperature distribution of the wall as a
 *                                MathFunction(x)
 *    density                : constant homogenous density of the wall (kg/m3)
 *    conductivity           : constant homogenous conductivity of the wall (W/m.K)
 *    specific_heat          : constant homogenous specific heat of the wall (J/kg.K)
 *    
 *    nodes                  : positive integer, number of control volumes to 
 *                                subdivide domain into
 *    length                 : length of heat exchanger (m)
 * Optional Arguments    
 *    orientation            : counterflow/parallelflow
 *    channel[0]_node_model  : A valid channel node model
 *    channel[1]_node_model  : A valid channel node model
 *    wall_node_model        : A valid axial wall node model
 *    f_corr[0]              : name of the friction factor correlation within 
 *                                channel[0]
 *    ht_corr[0]             : name of the heat transfer correlation between
 *                                channel[0] and the wall
 *    f_corr[1]              : name of the friction factor correlation within
 *                                channel[1]
 *    ht_corr[1]             : name of the heat transfer correlation between
 *                                channel[1] and the wall
 *    save_channel[0]        : Ordered list of attributes per channel[0] control 
 *                                volume to save to file, default (p, T, v)
 *    save_channel[1]        : Ordered list of attributes per channel[1] control 
 *                                volume to save to file, default (p, T, v)
 *    save_wall              : Ordered list of attributes for each control volume
 *                                to save to file, default (T)
 *    thin_film_heat_transfer[0]
 *                           : true/false, if true will calculate heat transfer 
 *                                correlations at the thin film conditions for 
 *                                channel[0]
 *    thin_film_heat_transfer[1]
 *                           : true/false, if true will calculate heat transfer 
 *                                correlations at the thin film conditions for 
 *                                channel[1]
 *    n_channels          : number of parallel channels in each stream
 *    name_convection[0]  : unique name of the heat convection between channel[0]
 *                             and the wall
 *    name_convection[1]  : unique name of the heat convection between channel[1]
 *                             and the wall
 *
 */
void build_parallel_flow_heat_exchanger(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains, 
    string[string] domain_defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries, 
    ref from!"q1dcfd.connectors.connection".Connection[] connection_table)
{
    import q1dcfd.utils.dictionary_tools: request, demand, new_dict, require_empty,
        retrieve, defaults;
    import q1dcfd.domains: build_channel, build_axial_wall ;
    import q1dcfd.connectors.connection: Connection ;

    // check for unrecognised fields
    scope(exit) domain_defs.require_empty(name) ;

    auto channel0_defs = new_dict!string
        .demand(domain_defs, "nodes")
        .demand(domain_defs, "length")
        .demand(domain_defs, "channel[0]_cross_section", "cross_section")        
        .demand(domain_defs, "channel[0]_initial_temperature", "initial_temperature")
        .demand(domain_defs, "channel[0]_initial_pressure", "initial_pressure")
        .demand(domain_defs, "channel[0]_initial_velocity", "initial_velocity")
        .request(domain_defs, "f_corr[0]", "f_corr_name")
        .request(domain_defs, "ht_corr[0]", "ht_corr_name")
        .request(domain_defs, "thin_film_heat_transfer[0]", "thin_film_heat_transfer")
        .request(domain_defs, "n_channels")
        .request(domain_defs, "save_channel[0]", "save")
        .request(domain_defs, "channel[0]_node_model", "node_model") ;

    auto channel1_defs = ["nodes": channel0_defs["nodes"],
                          "length": channel0_defs["length"],
                          "n_channels": channel0_defs["n_channels"]]
        .demand(domain_defs, "channel[1]_cross_section", "cross_section")
        .demand(domain_defs, "channel[1]_initial_temperature", "initial_temperature")
        .demand(domain_defs, "channel[1]_initial_pressure", "initial_pressure")
        .demand(domain_defs, "channel[1]_initial_velocity", "initial_velocity")
        .request(domain_defs, "f_corr[1]", "f_corr_name")
        .request(domain_defs, "ht_corr[1]", "ht_corr_name")
        .request(domain_defs, "thin_film_heat_transfer[1]", "thin_film_heat_transfer")
        .request(domain_defs, "save_channel[1]", "save")
        .request(domain_defs, "channel[1]_node_model", "node_model") ;

    auto wall_defs = ["nodes": channel0_defs["nodes"],
                      "length": channel0_defs["length"]]
        .demand(domain_defs, "wall_cross_section", "cross_section")
        .demand(domain_defs, "wall_initial_temperature", "initial_temperature")
        .request(domain_defs, "save_wall", "save")
        .request(domain_defs, "wall_node_model", "node_model") ;

    // build the first channel component of the heat exchanger
    build_channel(domains, channel0_defs, name ~"[0]", geometries) ;

    // build the second channel component of the heat exchanger
    build_channel(domains, channel1_defs, name ~"[1]", geometries) ;

    // build the seperating wall component of the heat exchanger
    build_axial_wall(domains, wall_defs, name ~"[2]", geometries) ;

    // connect parallel or counterflow
    auto orientation = domain_defs.retrieve("orientation").defaults("counterflow") ;
    auto name_convection0 = domain_defs
        .retrieve("name_convection[0]")
        .defaults(name ~ "_convection[0]") ;
    auto args = ["type":"convection"] ;
    
    if(orientation == "counterflow")
    {
        connection_table ~= Connection(args.dup, [[name ~"[0]", name ~"[2]"]], name_convection0) ;
    }
    else if(orientation == "parallelflow")
    {
        connection_table ~= Connection(args.dup, [[name ~"[2]", name ~"[0]"]], name_convection0) ;
    }
    else
    {
        import q1dcfd.utils.errors: InvalidDefinition ;
        import std.format ;
        throw new InvalidDefinition(
            "orientation %s not understood in %s".format(orientation, name)) ;
    }

    // convective connection between the second stream and the wall
    connection_table ~= Connection(
        args,
        [[name ~"[2]", name ~"[1]"]],
        domain_defs.retrieve("name_convection[1]").defaults(name ~ "_convection[1]")) ;
}

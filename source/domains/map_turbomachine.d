module q1dcfd.domains.map_turbomachine;

import q1dcfd.utils.metaprogramming: from;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.control_volumes.node: Node;

void build_map_turbomachine(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs,
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries,
    string component_type)
{
    import std.conv: to;

    import q1dcfd.config: Real;
    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults,
        required, greater, subset, map_from;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function;
    import q1dcfd.eos: backend;
    import q1dcfd.types.geometry: ChannelCrossSection;
    import q1dcfd.control_volumes: MapTurbomachineNode;

    scope(exit) defs.require_empty(name); // Unrecognized fields, exit

    // Build the turbomachine
    if(component_type == "turbine")
    {
        // er_design, mfp_design etc are design-point conditions, used to scale
        // performance; if not supplied, then default values from
        // source/maps/<map>/constants.csv will be used.
        auto turbine = new MapTurbine(
            name,
            defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
            defs.retrieve("save").subset(MapTurbomachineNode.STATE_FIELDS).defaults(["speed", "mdot", "delta_h", "PR", "w_sh", "eta"]),
            defs.retrieve("map").defaults("turbine-data"),
            defs.retrieve("plenum_length").required!double,
            &defs.retrieve("initial_speed").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_upstream_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_upstream_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_downstream_pressure").required!string.read_boundary_function.evaluate,
            defs.retrieve("stream").required!string,
            defs.retrieve("coolprop_fluid").required!string,
            defs.retrieve("er_design").defaults!double(0.0),
            defs.retrieve("mfp_design").defaults!double(0.0),
            defs.retrieve("T0_design").defaults!double(0.0),
            defs.retrieve("P0_design").defaults!double(0.0)
            );

        domains.add(name, turbine);
    }
    else if(component_type == "compressor")
    {
        // Demand speed and rotational inertia input for compressor only, since
        // compressor is inherently variable speed (while turbine can be assumed
        // synchronous).
        // Compressor boundary conditions are supplied as motor torque
        // transients (and these may be set by a controller).
        // Compressor is solved using root finder which requires initial
        // conditions as guess.
        // k_dh and k_eta are (optional) scaling factors for the maps (they
        // scale delta_h and efficiency respectively).
        auto compressor = new MapCompressor(
            name,
            defs.retrieve("cross_section").required!string.map_from(geometries).to!ChannelCrossSection,
            defs.retrieve("save").subset(MapTurbomachineNode.STATE_FIELDS).defaults(["speed", "mdot", "delta_h", "PR", "w_sh", "eta"]),
            defs.retrieve("map").defaults("sandia-compressor-data"),
            defs.retrieve("fixed_speed").defaults(false),
            defs.retrieve("plenum_length").required!double,
            defs.retrieve("motor_torque").defaults("Unassigned").read_boundary_function,
            defs.retrieve("rotor_inertia").required!double,
            &defs.retrieve("initial_motor_torque").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_speed").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_mass_flow_rate").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_upstream_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_upstream_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_downstream_pressure").required!string.read_boundary_function.evaluate,
            defs.retrieve("stream").required!string,
            defs.retrieve("k_dh").defaults!Real(1.0),
            defs.retrieve("k_eta").defaults!Real(1.0)
            );

        domains.add(name, compressor);
    }
    else
    {
        import q1dcfd.utils.errors: InvalidDefinition;
        import std.format;
        throw new InvalidDefinition(
            "Component type %s not recognized.".format(component_type));
    }
}

abstract class MapTurbomachine : from!"q1dcfd.domains.domain".Domain
{
    import q1dcfd.config: Real, Address, Transient;
    import q1dcfd.utils.math: MathFunction;
    import q1dcfd.eos: backend;
    import q1dcfd.control_volumes.map_turbomachine_node: MapTurbomachineNode;

    // Debugging and connection matching information
    override @property string type() { return "MapTurbomachine"; }

    immutable string map_name;
    immutable string stream_name;

    Real initial_mass_flow_rate = 0.0; // Overwritten if needed
    Real initial_speed;
    Real initial_upstream_temperature;
    Real initial_upstream_pressure;
    Real initial_downstream_pressure;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         Transient initial_speed,
         Transient initial_upstream_temperature,
         Transient initial_upstream_pressure,
         Transient initial_downstream_pressure,
         const string map_name,
         const string stream_name,
         const double plenum_length)
    {
        super(name,
              geometry,
              saved_fields,
              1); // just have one map-turomachine node

        this.map_name = map_name;
        this.stream_name = stream_name;

        this.initial_speed = initial_speed(0);
        this.initial_upstream_temperature = initial_upstream_temperature(plenum_length);
        this.initial_upstream_pressure = initial_upstream_pressure(plenum_length);
        this.initial_downstream_pressure = initial_downstream_pressure(0);
    }

    /*
     * Generates the geometry of each control volume, based off provided
     * for the entire turbomachine. Returns as a NodeGeometry object.
     */
    from!"q1dcfd.types.geometry".NodeGeometry generate_nodal_geometry(
        from!"q1dcfd.types.geometry".ChannelCrossSection domain_geometry, Real xa, Real xb)
    {
        // TODO: This function should definitely be copied somewhere as a
        // utility rather than being copied inside several classes
        import q1dcfd.domains.domain: Domain;
        auto result = Domain.generate_nodal_geometry(domain_geometry, xa, xb);
        result.htA = domain_geometry.heat_area(xa, xb);
        result.specific_density = 1.0/result.volume;
        result.hydr_d = domain_geometry.hydraulic_diameter(0.5*(xa + xb));
        result.delta = domain_geometry.roughness(0.5*(xa + xb));

        return result;
    }

    /*
     * Stores initial conditions then uses the turbomachine model to calculate
     * the flux values.
     */
    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import q1dcfd.eos: generate_eos, backend;
        import q1dcfd.simulation: Simulation;
        import q1dcfd.stream: Stream;
        import std.format;
        import std.conv: to;

        auto node = nodes[node_global_id(0)].to!MapTurbomachineNode;
        node.mdot  = initial_mass_flow_rate;
        node.T_in  = initial_upstream_temperature;
        node.p_in  = initial_upstream_pressure;
        node.p_out = initial_downstream_pressure;
        node.speed = initial_speed;

        // For turbine, we don't want speed to be touched again
        // For compressor, we want speed to be written into the state vector
        // then using the system integrator.

        // Usually rho_in and h_in are taken from inlet plenum during
        // update_flux, but need to initialize for first timestep
        Stream stream = cast(Stream) Simulation.get_stream(stream_name);
        auto eos = generate_eos(
            stream.primitive_data,
            backend.TTSE_recovery,
            "initialisation eos of %s".format("MapTurbomachineNode"));
        eos.update(node.p_in, node.T_in);

        node.rho_in = eos.rho;
        node.h_in = eos.e + node.p_in/node.rho_in;

        // Solve the turbomachine model so that flux values are cached for
        // interfaces and update_balance
        node.compute_turbomachine_fluxes();
    }
}

final class MapTurbine : MapTurbomachine
{
    import q1dcfd.config: Real;
    import q1dcfd.utils.math: MathFunction;
    import q1dcfd.eos: backend;
    import q1dcfd.control_volumes.map_turbomachine_node: MapTurbineNode;

    override @property string type() { return "MapTurbine"; }
    override @property string node_type()
    { return typeid(MapTurbineNode).toString; }

    const string coolprop_fluid; // Turbine model uses CoolProp EoS for some parts
    immutable Real er_design, mfp_design, T0_design, P0_design; // Scaling parameters

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const string map_name,
         const double plenum_length,
         Transient initial_speed,
         Transient initial_upstream_temperature,
         Transient initial_upstream_pressure,
         Transient initial_downstream_pressure,
         const string stream_name,
         const string coolprop_fluid,
         immutable Real er_design,
         immutable Real mfp_design,
         immutable Real T0_design,
         immutable Real P0_design)
    {
        super(name, geometry, saved_fields, initial_speed,
                initial_upstream_temperature, initial_upstream_pressure,
                initial_downstream_pressure, map_name, stream_name,
                plenum_length);

        this.coolprop_fluid = coolprop_fluid;
        this.order = MapTurbineNode.order*n_nodes;
        this.er_design  = er_design;
        this.mfp_design = mfp_design;
        this.T0_design  = T0_design;
        this.P0_design  = P0_design;
    }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.conv: to;
        import std.format;
        import q1dcfd.types.geometry: ChannelCrossSection;
        debug import q1dcfd.utils.logging: add_message_to_log_file;

        this.node_index_l  = node_index_l;
        this.node_index_r  = node_index_l + n_nodes;
        this.ode_index_l   = index_l;
        this.write_index_l = write_index_l;

        auto address = Address(ode_index_l, ode_index_l + MapTurbineNode.order,
            write_index_l, node_index_l, name);

        // Currently, assume that the turbomachine has no volume, so use xa and
        // xb = 0.
        auto xa = 0.0;
        auto xb = 0.0;
        auto node_geometry = this.generate_nodal_geometry(
            geometry.to!ChannelCrossSection, xa, xb);

        nodes[node_index_l] = new MapTurbineNode(
                address, node_geometry, coolprop_fluid, map_name, er_design,
                mfp_design, T0_design, P0_design);

        debug "Constructed %s".format(nodes[node_index_l].describe)
            .add_message_to_log_file;
    }
}

final class MapCompressor : MapTurbomachine
{
    import q1dcfd.config: Real;
    import q1dcfd.utils.math: MathFunction;
    import q1dcfd.eos: backend;
    import q1dcfd.control_volumes.map_turbomachine_node: MapCompressorNode;

    override @property string type() { return "MapCompressor"; }
    override @property string node_type()
    { return typeid(MapCompressorNode).toString; }

    Real initial_motor_torque;
    MathFunction motor_torque_transient;
    Real rotor_inertia;

    immutable Real k_dh, k_eta; // Scaling parameters

    bool motor_torque_transient_is_linked = false;
    bool motor_torque_transient_is_specified = false;
    immutable bool fixed_speed;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const string map_name,
         const bool fixed_speed,
         const double plenum_length,
         MathFunction motor_torque_transient,
         const double rotor_inertia,
         Transient initial_motor_torque,
         Transient initial_speed,
         Transient initial_mass_flow_rate,
         Transient initial_upstream_temperature,
         Transient initial_upstream_pressure,
         Transient initial_downstream_pressure,
         const string stream_name,
         immutable Real k_dh,
         immutable Real k_eta)
    {
        super(name, geometry, saved_fields, initial_speed,
                initial_upstream_temperature, initial_upstream_pressure,
                initial_downstream_pressure, map_name, stream_name,
                plenum_length);

        this.fixed_speed = fixed_speed;
        this.initial_motor_torque = initial_motor_torque(0);
        this.motor_torque_transient = motor_torque_transient;
        this.rotor_inertia = rotor_inertia;
        this.order = MapCompressorNode.order*n_nodes;
        this.initial_mass_flow_rate = initial_mass_flow_rate(0);
        this.k_dh  = k_dh;
        this.k_eta = k_eta;

        if(motor_torque_transient.not_null)
        {
            designate_boundary_linked("motor_torque");
            motor_torque_transient_is_specified = true;
        }
    }

    override void discretise(
        ref Node[] nodes,
        ref Interface[] interfaces,
        const uint node_index_l,
        const uint index_l,
        const uint write_index_l)
    {
        import std.conv: to;
        import std.format;
        import q1dcfd.types.geometry: ChannelCrossSection;
        debug import q1dcfd.utils.logging: add_message_to_log_file;

        this.node_index_l  = node_index_l;
        this.node_index_r  = node_index_l + n_nodes;
        this.ode_index_l   = index_l;
        this.write_index_l = write_index_l;

        auto address = Address(ode_index_l, ode_index_l + MapCompressorNode.order,
            write_index_l, node_index_l, name);

        // Currently, assume that the turbomachine has no volume, so use xa and
        // xb = 0.
        auto xa = 0.0;
        auto xb = 0.0;
        auto node_geometry = this.generate_nodal_geometry(
            geometry.to!ChannelCrossSection, xa, xb);

        nodes[node_index_l] = new MapCompressorNode(
                address, node_geometry, stream_name, fixed_speed,
                initial_downstream_pressure, map_name, k_dh, k_eta,
                rotor_inertia);

        debug "Constructed %s".format(nodes[node_index_l].describe)
            .add_message_to_log_file;
    }

    /*
     * Compressor is asynchronous, so ODEs are used to model time evolution of
     * compressor speed.
     * State variables are T_motor (may be set by controller) and compressor
     * speed.
     */
    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        super.initialise_state(state, nodes);

        import std.conv: to;
        auto node = nodes[node_global_id(0)].to!MapCompressorNode;
        node.T_motor = initial_motor_torque;

        state[ode_index_l]     = initial_motor_torque;
        state[ode_index_l + 1] = initial_speed;
    }

    /*
     * Link the motor torque boundary condition to the turbomachine.
     * Only applicable for compressor since turbine is assumed synchronous.
     */
    override void link_boundaries(ref Node[] nodes)
    {
        import std.conv: to;

        if(motor_torque_transient.not_null)
        {
            auto node = nodes[node_global_id(0)].to!MapCompressorNode;
            node.motor_torque_transient = &motor_torque_transient.evaluate;
            // motor_torque_transient_is_specified == true for this case
            node.configure_motor_torque_transients(motor_torque_transient_is_specified);
        }
    }

    override void designate_boundary_linked(const string port)
    {
        import q1dcfd.utils.errors: InvalidDefinition, MultipleConnection;
        import std.format;

        switch(port)
        {
            case "motor_torque":
            {
                if(!motor_torque_transient_is_linked)
                {
                    motor_torque_transient_is_linked = true;
                    break;
                }
                throw new MultipleConnection(name, port);
            }
            default:
            {
                throw new InvalidDefinition("boundary function %s not understood".format(port));
            }
        }
    }
}


module q1dcfd.domains.inflow ;

import q1dcfd.utils.metaprogramming: from ;
import q1dcfd.control_volumes: InflowDV, InflowFromStagnation,
        PumpFromStagnation, IncompressibleInflow, IncompressiblePump;

/**
 * Does most of the actual work of build_inflow
 */
private void generate_inflow(NodeModel)(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs, 
    const string name)
{
    import q1dcfd.utils.dictionary_tools: retrieve, require_empty, defaults, required,
        subset ;
    import q1dcfd.eos: backend ;
    import q1dcfd.initialisation.build_boundary_function: read_boundary_function ;

    scope(exit) defs.require_empty(name) ;

    static if(is(NodeModel == PumpFromStagnation))
    {
        auto inflow = new Inflow!(NodeModel)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("state_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("velocity_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("massflow_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("initial_stagnation_pressure").defaults("Unassigned").read_boundary_function,
            defs.retrieve("initial_stagnation_temperature").defaults("Unassigned").read_boundary_function,
            defs.retrieve("coolprop_backend").defaults("HEOS"),
            defs.retrieve("coolprop_fluid").defaults("none"),
            defs.retrieve("piso_period").defaults("0.0"),
            defs.retrieve("relax_factor").defaults("0.005"),
            defs.retrieve("a_in").defaults("0.0"),
            defs.retrieve("mdot_max").required!double,
            defs.retrieve("mdot_dot_max").required!double);

        domains.add(name, inflow) ;
    }
    else
    {
        auto inflow = new Inflow!(NodeModel)(
            name,
            defs.retrieve("save").subset(NodeModel.STATE_FIELDS).defaults(["p", "T", "v"]),
            &defs.retrieve("initial_temperature").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_pressure").required!string.read_boundary_function.evaluate,
            &defs.retrieve("initial_velocity").required!string.read_boundary_function.evaluate,
            defs.retrieve("eos").defaults(backend.TTSE_recovery).required!backend,
            defs.retrieve("state_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("velocity_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("massflow_transient").defaults("Unassigned").read_boundary_function,
            defs.retrieve("priority_queue").defaults(false),
            defs.retrieve("initial_stagnation_pressure").defaults("Unassigned").read_boundary_function,
            defs.retrieve("initial_stagnation_temperature").defaults("Unassigned").read_boundary_function,
            defs.retrieve("coolprop_backend").defaults("HEOS"),
            defs.retrieve("coolprop_fluid").defaults("none"),
            defs.retrieve("piso_period").defaults("0.0"),
            defs.retrieve("relax_factor").defaults("0.005"),
            defs.retrieve("a_in").defaults("0.0"),
            defs.retrieve("mdot_max").defaults(0.0),
            defs.retrieve("mdot_dot_max").defaults(0.0));

        domains.add(name, inflow) ;
    }
}

/**
 * Constructs an Inflow object from a set of domain definitions 
 *
 * Required Arguments:
 *     type                   : Inflow 
 *     initial_temperature    : Initial temperature distribution as a 
 *                                 MathFunction(x)
 *     initial_pressure       : Initial pressure distribution as a MathFunction(x)
 *     initial_velocity       : Initial velocity distribution as a MathFunction(x)
 *
 * Optional Arguments    
 *     node_model             : A valid inflow node model
 *     save                   : Ordered list of attributes to save to file, 
 *                                 default (p, T, v)
 *     state_transient        : thermodynamic inflow variable as a MathFunction(t)
 *                                 using the default InflowDV node model this will
 *                                 define density. If not assigned, must be related
 *                                 by an implicit connection or control variable 
 *                                 connection
 *     velocity_transient     : inflow velocity as a MathFunction(t). If not 
 *                                 assigned, must be related by an implicit 
 *                                 connection or control variable connection
 *     priority_queue         : true/false, if true will force the node to updated
 *                                 in the zero'th node queue. default is false
 */
void build_inflow(
    ref from!"q1dcfd.domains.build_domain".DomainContainer domains,
    string[string] defs, 
    string name,
    from!"q1dcfd.types.geometry".CrossSection[string] geometries)
{
    import q1dcfd.utils.dictionary_tools: retrieve, defaults ;

    // get the node model
    auto model = defs.retrieve("node_model").defaults("InflowDV") ;

    // delegate actual work to template constructor
    switch(model)
    {
        case "InflowDV":
        {
            generate_inflow!(InflowDV)(domains, defs, name) ;
            break ;
        }
        case "InflowFromStagnation":
        {
            generate_inflow!(InflowFromStagnation)(domains, defs, name);
            break;
        }
        case "PumpFromStagnation":
        {
            generate_inflow!(PumpFromStagnation)(domains, defs, name);
            break;
        }
        case "IncompressibleInflow":
        {
            generate_inflow!(IncompressibleInflow)(domains, defs, name);
            break;
        }
        case "IncompressiblePump":
        {
            generate_inflow!(IncompressiblePump)(domains, defs, name);
            break;
        }
        // Can add additional node models here
        default:
        {
            import q1dcfd.utils.errors: InvalidDefinition ;
            import std.format ;

            throw new InvalidDefinition(
                "Unrecognized node_model %s for Inflow %s".format(model, name)) ;
        }
    }
}

/**
 * Inflow boundary with one assigned state transient, and assigned velocity
 *
 * Necessary to have an intermediate class so that inflow_velocity can be found
 * at initialisation for arbitrary NodeModel
 */
abstract class AbstractInflow: from!"q1dcfd.domains".FluidDomain
{
    import q1dcfd.utils.math: MathFunction ;
    import q1dcfd.domains: Domain ;
    import q1dcfd.config: Transient ;
    
    // Boundary transients
    MathFunction inflow_state_var ;
    MathFunction inflow_velocity ;
    MathFunction inflow_massflow ;

    // pointer to the interfacing interior
    Domain interior ;

    // flag whether the inflow needs to be in the priority queue
    // this is required for casually implicit components such as discontinuity
    // connections
    bool priority_queue ;

    // a connection has been made to the boundary functions
    bool state_transient_is_linked = false ;
    bool velocity_transient_is_linked = false ;
    bool massflow_transient_is_linked = false ;

    override @property string type() { return "Inflow"; }

    this(const string name,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         MathFunction inflow_state_var,
         MathFunction inflow_velocity,
         MathFunction inflow_massflow,
         const bool priority_queue)
    {
        import q1dcfd.types.geometry: Dimensionless ;

        super(name,
              new Dimensionless,
              saved_fields,
              1,
              "NoFriction",
              "NoHeatTransfer",
              1,
              false,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model) ;

        this.inflow_velocity = inflow_velocity ;
        this.inflow_state_var = inflow_state_var ;
        this.inflow_massflow = inflow_massflow ;

        if(inflow_velocity.not_null) designate_boundary_linked("velocity") ;
        if(inflow_state_var.not_null) designate_boundary_linked("state") ;

        this.priority_queue = priority_queue ;
        this.node_coords[0] = 0.0 ;
    }

    override string check_simulation_ready()
    {
        import std.format ;
        auto msg = "" ;
        
        if(!inflow_velocity.not_null) msg ~= "inflow velocity not set for %s".format(name) ;
        if(!inflow_state_var.not_null) msg ~= "inflow density not set for %s".format(name) ;

        return msg ;
    }
    
    override void designate_boundary_linked(const string port)
    {
        import q1dcfd.utils.errors: InvalidDefinition, MultipleConnection ;
        import std.format ;

        switch(port)
        {
            case "massflow":
            {
                if(!massflow_transient_is_linked)
                {
                    massflow_transient_is_linked = true ;
                    break ;
                }
                throw new MultipleConnection(name, port) ;
            }
            case "velocity":
            {
                if(!velocity_transient_is_linked)
                {
                    velocity_transient_is_linked = true ;
                    break ;
                }
                throw new MultipleConnection(name, port) ;
            }
            case "state":
            {
                if(!state_transient_is_linked)
                {
                    state_transient_is_linked = true ;
                    break ;
                }
                throw new MultipleConnection(name, port) ;
            }
            default:
            {
                throw new InvalidDefinition("boundary function %s not understood".format(port)) ;
            }
        }
    }
}

/**
 * Implements AbstractInflow with a NodeModel 
 */
final class Inflow(NodeModel): AbstractInflow
{
    import q1dcfd.utils.math: MathFunction ;
    import q1dcfd.control_volumes: Node ;
    import q1dcfd.config: Real, Transient ;
    import q1dcfd.interfaces.root_interface: Interface ;

    // Extra variables for InflowFromStagnation and derivatives
    static if ( 
            is(NodeModel == InflowFromStagnation)
        ||  is(NodeModel == PumpFromStagnation))
    {
        Real initial_p_stag;
        Real initial_t_stag;
        Real relax_factor;
        Real a_in; // Inflow area
        const string coolprop_backend;
        const string coolprop_fluid;
        Real mdot_max;
        Real mdot_dot_max;
    }
    // Extra variables for IncompressibleInflow
    else static if (
            is(NodeModel == IncompressibleInflow)
        ||  is(NodeModel == IncompressiblePump))
    {
        const string coolprop_backend;
        const string coolprop_fluid;
        const Real piso_period;
        Real a_in;
        Real mdot_max;
        Real mdot_dot_max;
    }

    // TODO: Would be much cleaner to have different constructors for different
    // NodeModels 

    this(const string name,
         const string[] saved_fields,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         from!"q1dcfd.eos".backend backend_model,
         MathFunction inflow_state_var,
         MathFunction inflow_velocity,
         MathFunction inflow_massflow,
         const bool priority_queue,
         MathFunction initial_p_stag,
         MathFunction initial_t_stag,
         const string coolprop_backend,
         const string coolprop_fluid,
         const string piso_period,
         const string relax_factor,
         const string a_in,
         Real mdot_max=0.0,
         Real mdot_dot_max=0.0)
    {
        import std.conv: to;

        super(name,
              saved_fields,
              initial_temperature_distribution,
              initial_pressure_distribution,
              initial_velocity_distribution,
              backend_model,
              inflow_state_var,
              inflow_velocity,
              inflow_massflow,
              priority_queue) ;

        this.order = NodeModel.order ;

        static if (   is(NodeModel == InflowFromStagnation)
                   || is(NodeModel == PumpFromStagnation))
        {
            import q1dcfd.utils.math: Unassigned;
            // If stagnation values are not assigned, use inflow conditions
            if(cast(Unassigned) initial_p_stag !is null)
                this.initial_p_stag = initial_pressure_distribution(0);
            else
                this.initial_p_stag = initial_p_stag.evaluate(0);
            if(cast(Unassigned) initial_t_stag !is null)
                this.initial_t_stag = initial_temperature_distribution(0);
            else
                this.initial_t_stag = initial_t_stag.evaluate(0);

            // For InflowFromStagnation, it is invalid not to have a positive
            // a_in since we use massflow as the boundary variable.
            import q1dcfd.utils.errors: InvalidDefinition;
            this.a_in = to!Real(a_in);
            if (this.a_in == 0.0) {
                throw new InvalidDefinition(
                        "Must define flow area for InflowFromStagnation boundary.");
            }

            this.relax_factor = to!Real(relax_factor);
            this.coolprop_backend = coolprop_backend;
            this.coolprop_fluid = coolprop_fluid;
            this.mdot_max = mdot_max;
            this.mdot_dot_max = mdot_dot_max;
        }
        else static if (
                is(NodeModel == IncompressibleInflow)
            ||  is(NodeModel == IncompressiblePump))
        {
            this.coolprop_backend = coolprop_backend;
            this.coolprop_fluid = coolprop_fluid;
            this.piso_period = to!Real(piso_period);
            this.mdot_max = mdot_max;
            this.mdot_dot_max = mdot_dot_max;

            // For IncompressibleInflow 
            import q1dcfd.utils.errors: InvalidDefinition;
            this.a_in = to!Real(a_in);
            if (this.a_in == 0.0) {
                throw new InvalidDefinition(
                        "Must define flow area for IncompressibleInflow boundary.");
            }
        }
    }

    override @property string node_type() { return typeid(NodeModel).toString; }

    override void discretise(
        ref Node[] nodes, 
        ref Interface[] interfaces, 
        const uint node_index_l, 
        const uint index_l, 
        const uint write_index_l)
    {
        import q1dcfd.config: Address ;

        this.node_index_l = node_index_l ;
        this.node_index_r = node_index_l + n_nodes ;
        this.ode_index_l = index_l ;
        this.write_index_l = write_index_l ;

        auto address = Address(ode_index_l, ode_index_l + NodeModel.order,
            write_index_l, node_index_l, name) ;

        static if (is(NodeModel == InflowFromStagnation))
        {
            // InflowFromStagnation requires extra ininitialization data
            nodes[node_index_l] = new NodeModel(address, stream, backend_model,
                    initial_p_stag, initial_t_stag, initial_velocity_distribution,
                    coolprop_backend, coolprop_fluid, a_in, relax_factor);
        }
        else static if (is(NodeModel == PumpFromStagnation))
        {
            // InflowFromStagnation requires extra ininitialization data
            nodes[node_index_l] = new NodeModel(mdot_max, mdot_dot_max, address,
                    stream, backend_model, initial_p_stag, initial_t_stag,
                    initial_velocity_distribution, coolprop_backend,
                    coolprop_fluid, a_in, relax_factor);
        }
        else static if (is(NodeModel == IncompressibleInflow))
        {
            // Incompressible inflows are based on a Node not a FluidNode, so
            // different initialisation arguments are required
            nodes[node_index_l] = new NodeModel(address, coolprop_backend,
                    coolprop_fluid, piso_period, a_in);
        }
        else static if (is(NodeModel == IncompressiblePump))
        {
            // Incompressible inflows are based on a Node not a FluidNode, so
            // different initialisation arguments are required
            nodes[node_index_l] = new NodeModel(address, coolprop_backend,
                    coolprop_fluid, piso_period, a_in,
                    initial_pressure_distribution,
                    initial_temperature_distribution,
                    initial_velocity_distribution, mdot_max, mdot_dot_max);
        }
        else
        {
            nodes[node_index_l] = new NodeModel(address, stream, backend_model) ;
        }
        nodes[node_index_l].priority_queue = priority_queue ;
    }

    override void initialise_state(ref Real[] state, ref Node[] nodes)
    {
        import q1dcfd.eos:generate_eos, backend ;
        import q1dcfd.utils.math: square ;
        import std.format ;

        // populate with initial data
        auto p = initial_pressure_distribution(0.0) ;
        auto T = initial_temperature_distribution(0.0) ;
        auto v = initial_velocity_distribution(0.0) ;

        static if (
                is(NodeModel == IncompressibleInflow)
            ||  is(NodeModel == IncompressiblePump))
        {
            // Incompressible inflows have an internal EoS (Coolprop) that is used
            // to initialize the derived properties
            import std.conv: to;
            nodes[node_global_id(0)].to!NodeModel.p = p;
            nodes[node_global_id(0)].to!NodeModel.T = T;
            nodes[node_global_id(0)].to!NodeModel.v = v;
            nodes[node_global_id(0)].to!NodeModel.update_derived_properties();
            nodes[node_global_id(0)].to!NodeModel.mdot = v
                * nodes[node_global_id(0)].to!NodeModel.a_in
                * nodes[node_global_id(0)].to!NodeModel.rho;
        }
        else
        {
            // All other inflows update state from a standard EoS
            auto eos = generate_eos(stream.primitive_data,
                                    backend.TTSE_recovery,
                                    "Initialisation eos of %s".format(name));
            eos.update(p,T);

            // Stagnation boundaries do have any ODEs to solve
            static if(   !is(NodeModel == InflowFromStagnation)
                      && !is(NodeModel == PumpFromStagnation))
            {
                state[ode_index_l] = eos.rho*(eos.e + 0.5*v.square);
            }

            nodes[node_global_id(0)].update(state, 0.0);
        }
    }

    override void link_boundaries(ref Node[] nodes)
    {
        import std.conv: to ;

        auto node = nodes[node_global_id(0)].to!NodeModel ;
        if(inflow_state_var.not_null)
        {
            // InflowFromStagnation and IncompressibleInflow use temperature transients
            static if (   is(NodeModel == InflowFromStagnation)
                       || is(NodeModel == PumpFromStagnation)
                       || is(NodeModel == IncompressibleInflow)
                       || is(NodeModel == IncompressiblePump))
            {
                node.temperature_transient = &inflow_state_var.evaluate;
            }
            // Other node models use density transients
            else
            {
                node.density_transient = &inflow_state_var.evaluate;
                node.density_rate_transient = &inflow_state_var.derivative.evaluate;
            }
            
            // flag connection complete
            state_transient_is_linked = true ;
        }

        if(inflow_velocity.not_null)
        {
            static if (   !is(NodeModel == InflowFromStagnation)
                       && !is(NodeModel == PumpFromStagnation)
                       && !is(NodeModel == IncompressibleInflow)
                       && !is(NodeModel == IncompressiblePump))
            {
                node.velocity_transient = &inflow_velocity.evaluate ;
                node.velocity_rate_transient = &inflow_velocity.derivative.evaluate;

                // flag connection complete
                velocity_transient_is_linked = true ;
            }
            else
            {
                import q1dcfd.utils.errors: InvalidDefinition ;
                throw new InvalidDefinition(
                        "InflowFromStagnation and IncompressibleInflow boundaries should not have"
                        ~ " velocity transients defined.");
            }
        }

        if(inflow_massflow.not_null)
        {
            static if (   is(NodeModel == InflowFromStagnation)
                       || is(NodeModel == PumpFromStagnation)
                       || is(NodeModel == IncompressibleInflow)
                       || is(NodeModel == IncompressiblePump))
            {
                node.massflow_transient = &inflow_massflow.evaluate;

                // flag connection complete
                massflow_transient_is_linked = true ;
            }
            else
            {
                import q1dcfd.utils.errors: InvalidDefinition ;
                throw new InvalidDefinition(
                        "Only InflowFromStagnation boundaries should have"
                        ~ " mass flow transients defined.");
            }
        }
    }
}

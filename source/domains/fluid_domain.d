module q1dcfd.domains.fluid_domain ;

import q1dcfd.utils.metaprogramming: from ;

/**
 * Base class for a fluid transporting domain 
 */
abstract class FluidDomain: from!"q1dcfd.domains.domain".Domain
{
    import q1dcfd.config: Real, Transient ;
    import q1dcfd.stream: Stream ;
    import q1dcfd.eos: backend ;

    // Correlation identifiers
    string f_corr_name ;
    string ht_corr_name ;

    // Property distributions at time 0, while of type Transient, they
    // are actually functions of axial position
    Transient initial_pressure_distribution ;
    Transient initial_temperature_distribution ;
    Transient initial_velocity_distribution ;

    // Stream structure which the domain belongs to
    Stream stream ;
    bool stream_connected = false;

    // number of identical similaritudal channels
    uint n_channels ;

    // Use thin film over bulk heat transfer correlations
    bool thin_film_heat_transfer ;

    // backend model which the domain belongs to
    backend backend_model  ;

    this(const string name,
         from!"q1dcfd.types.geometry".CrossSection geometry,
         const string[] saved_fields,
         const uint n_nodes,
         const string f_corr_name,
         const string ht_corr_name,
         const uint n_channels,
         const bool thin_film_heat_transfer,
         Transient initial_temperature_distribution,
         Transient initial_pressure_distribution,
         Transient initial_velocity_distribution,
         backend backend_model)
    {
        super(name, geometry, saved_fields, n_nodes) ;

        this.f_corr_name = f_corr_name ;
        this.ht_corr_name = ht_corr_name ;
        this.n_channels = n_channels ;
        this.thin_film_heat_transfer = thin_film_heat_transfer ;
        this.initial_temperature_distribution = initial_temperature_distribution ;
        this.initial_pressure_distribution = initial_pressure_distribution ;
        this.initial_velocity_distribution = initial_velocity_distribution ;
        this.backend_model = backend_model ;
    }

    /**
     * Assign a stream to the domain
     */
    void set_stream(Stream stream)
    {
        stream_connected = true;
        this.stream = stream ;
    }
}

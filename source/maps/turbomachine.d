module q1dcfd.maps.turbomachine;

/*
 * Simple wrapper so that Turbine and Compressor final classes can be used
 * polymorphically by state space controller.
 */
abstract class Turbomachine
{
    double[string] calc_downstream(double Massflow, double Speed, double P_in,
            double T_in, uint verbose=0);

    double[string] calc_from_er(double Speed, double T_in, double P_in,
            double P_out, double mdot_guess, double tol=2E-3, uint max_iter=30,
            double relax_factor=1.0);

    double get_speed_nominal();
}


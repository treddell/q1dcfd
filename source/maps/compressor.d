module q1dcfd.maps.compressor;

import std.typecons;
import mir.ndslice;
import lubeck;

import q1dcfd.maps.turbomachine: Turbomachine;
import q1dcfd.eos: EOSBase;

alias odarr = Slice!(double*, 1);
alias tdarr = Slice!(double*, 2);
alias iodarr = Slice!(immutable(double)*, 1);

/*
 * This class is a Dlang clone of the Comp_funct.py module in SSCAR.
 * It models the steady-state behaviour of a compressor using
 * experimentally-generated performance maps.
 * This code accounts for changes in inlet conditions and mass flow rate (i.e.
 * off-design operation) using real-gas relationships.
 *
 * Currently, this code only supports delta_h-eta performance maps, though the
 * Comp_funct SSCAR module also supports MFP-PR and mdot-PR maps.
 * 
 * Comp_funct reads in unstructured experimental data then generates structured
 * 2D interpolation maps. I don't know of an unstructured interpolation library
 * for D, so we just generate the maps (DH, ETA, SPEED) in python and read them
 * in here.
 *
 * This code also supports scaling of the performance maps so that compressors
 * with different operating points can be approximately modelled using the the
 * maps from some other compressor (see documentation in scale_maps method for
 * details).
 */
final class Compressor : Turbomachine
{
    import q1dcfd.maps.turbo_utils;
    import mir.interpolate.linear;
    import q1dcfd.stream: Stream;

    string type;
    string method = "linear";
    string fluid;

    enum ErrCode { surge_line_overrun=0, mass_flow_overrun=1, non_convergence=2 }
    immutable string[uint] error_map; // must be initialized at run-time

    // Only the (rho,e) EOS has the full selection of thermodynamic parameters,
    // however we sometimes need to do updates based on (p,T) inputs or (p,s)
    // inputs, so we need the other EOS objects to update the (rho,e) EOS.
    EOSBase EosPT;
    EOSBase EosRhoE;
    EOSBase EosPS;

    // As discussed in doc section, input data arrays are only used to generate
    // the interpolating vectors for the maps (mass_grid, DH_grid, etc), rather
    // than to generate the maps themselves (no unstructured interpolation is
    // available for D).
    //
    // Input data from the compressor maps
    // Array naming: mdot_eta is the vector of mdot values corresponding to the
    // vector eta, and similar for for mdot_speed etc.
    // mdot_surge and speed_surge are used to construct the surge line, and
    // dh_surge is currently unused.
    // TODO: Replace these initializers with mixins
    double[] mdot_eta, DH_eta, DH_isen_eta, eta, mdot_speed, DH_speed, speed,
             mdot_surge, dh_surge, speed_surge;
    double mdot_design, DH_design, eta_design, speed_design, Tcorr, Pcorr,
           Volume, speed_nominal, speed_max;
    immutable string[] ARRAYS = ["mdot_eta", "DH_eta", "DH_isen_eta", "eta",
              "mdot_speed", "DH_speed", "speed", "mdot_surge", "dh_surge",
              "speed_surge"];
    immutable string[] CONSTANTS = ["mdot_design", "DH_design", "eta_design",
              "speed_design", "Tcorr", "Pcorr", "Volume", "speed_nominal"];

    odarr mass_grid, DH_grid, DH_isen_grid, speed_grid;
    tdarr DH, ETA, SPEED;

    double I_mass_min, I_speed_min, I_dh_min, I_mass_max, I_speed_max, I_dh_max;

    Linear!(double, 2, immutable(double)*, immutable(double)*) dh_interpolator,
        eta_interpolator, speed_interpolator;
    Linear!(double, 1, immutable(double)*) surge_interpolator;

    // Need to store these two props for root-finder guesses
    double P_out;
    double delta_s;

    // For scaling the maps
    double K_DH, K_ETA;

    // MFP_speed, PR_speed, MFP_design, PR_design only needed for MFP-PR based
    // compressors (rather than speed-based)

    // Reads in values from the data files and performs the functions in
    // create_griddata in Ingo's code
    this(string data_path, Stream stream, double initial_P_out,
            double k_dh=1.0, double k_eta=1.0, double initial_delta_s=0.0)
    {
        import std.algorithm: minIndex, minElement;
        import std.algorithm.comparison: min, max;
        import std.algorithm.searching: minElement, maxElement;
        import q1dcfd.eos: generate_eos, backend;

        error_map = [
            0 : "surge line overrun",
            1 : "mass flow overrun",
            2 : "non-convergence error"];

        EosPT = generate_eos(
            stream.primitive_data,
            backend.bicubic_recovery,
            "Primitive (p,T) EOS for compressor.");
        EosRhoE = generate_eos(
            stream.conservative_data,
            backend.bicubic_recovery,
            "Conservative (rho,e) EOS for compressor.");
        EosPS = generate_eos(
            stream.isentropic_data,
            backend.bicubic_recovery,
            "Isentropic (p,s) EOS for compressor.");

        // Read performance data from CSV files
        auto arr_data = read_data_with_header_from_csv(data_path ~ "/arrays.csv");
        // Dummy data is indicated by -1s, the following loop removes this
        foreach(key; arr_data.byKey()) {
            if (minElement(arr_data[key]) < 0.0) {
                arr_data[key] = arr_data[key][0..minIndex(arr_data[key])];
            }
        }
        mixin(ReadArraysFromAA!(ARRAYS, "arr_data"));
        auto const_data = read_constants_with_header_from_csv(data_path ~ "/constants.csv");
        mixin(ReadArraysFromAA!(CONSTANTS, "const_data"));

        DH    = read_tables_from_csv(data_path ~ "/dh_map.csv");
        ETA   = read_tables_from_csv(data_path ~ "/eta_map.csv");
        // Currently SPEED is unused
        // SPEED = read_tables_from_csv(data_path ~ "/speed_map.csv");

        // Currently enforce the that the maps are square, and use x-length as
        // resolution for interpolating variables (could relax this by making
        // resolutions for each interpolating variable)
        auto map_res_err = "Compressor maps currently must be square. Check map x and y dims.";
        assert(DH.length!0 == DH.length!1, map_res_err);
        assert(ETA.length!0 == ETA.length!1, map_res_err);
        assert(DH.length!0 == ETA.length!0, map_res_err);
        // assert(SPEED.length!0 == SPEED.length!1, map_res_err);
        // assert(DH.length!0 == SPEED.length!0, map_res_err);
        import std.conv: to;
        uint res = to!uint(DH.length!0);

        // Scale maps, this scales the limits used to form the grids below
        this.K_DH = k_dh;
        this.K_ETA = k_eta;
        scale_maps();

        // Establish limits of input data (Note: I switched these to be max-min
        // or min-max, Josh/Ingo had it the min-min, max-max, which is wrong)
        auto min_mdot = max(minElement(mdot_eta), minElement(mdot_speed));
        auto max_mdot = min(maxElement(mdot_eta), maxElement(mdot_speed));
        auto min_DH   = max(minElement(DH_eta), minElement(DH_speed));
        auto max_DH   = min(maxElement(DH_eta), maxElement(DH_speed));
        auto min_DH_isen = minElement(DH_isen_eta);
        auto max_DH_isen = maxElement(DH_isen_eta);
        auto min_speed = minElement(speed);
        auto max_speed = maxElement(speed);

        // Create grids for interpolation
        mass_grid    = linspace(min_mdot, max_mdot, res);
        DH_grid      = linspace(min_DH, max_DH, res);
        DH_isen_grid = linspace(min_DH_isen, max_DH_isen, res);
        speed_grid   = linspace(min_speed, max_speed, res);
        // Scale by design point parameters
        mass_grid[]    = mass_grid[] * mdot_design;
        DH_grid[]      = DH_grid[] * DH_design;
        DH_isen_grid[] = DH_isen_grid[] * DH_design;
        speed_grid[]   = speed_grid[] * speed_design;

        // We read in interpolated grid data from python output during
        // initialisation, so don't need to generate this data here

        // Limits for interpolation
        I_mass_min  = min_mdot * mdot_design;   I_mass_max = max_mdot * mdot_design;
        I_speed_min = min_speed * speed_design; I_speed_max = max_speed * speed_design;
        I_dh_min    = min_DH * DH_design;       I_dh_max = max_DH * DH_design;

        // The maps get a bit weird near the max speed limit, so use a 3% margin
        speed_max = 0.97 * I_speed_max;

        // Make interpolators
        dh_interpolator = linear!(double, 2)(cast(iodarr) mass_grid,
                cast(iodarr) speed_grid, DH.transposed);
        eta_interpolator = linear!(double, 2)(cast(iodarr) mass_grid,
                cast(iodarr) DH_grid, ETA.transposed);

        // Make the surge interpolator
        auto speed_surge_odarr = slice!double(speed_surge.length);
        auto mdot_surge_odarr = slice!double(mdot_surge.length);
        speed_surge_odarr[] = speed_surge[]; // TODO: * speed_design, then use N_eq as input
        mdot_surge_odarr[] = mdot_surge[];
        surge_interpolator = linear!double(cast(iodarr) speed_surge_odarr, mdot_surge_odarr);

        P_out = initial_P_out;
        delta_s = initial_delta_s;
    }

    override double get_speed_nominal() { return speed_nominal; }

    /* Compute max actual speed based on inlet conditions. */
    double compute_max_speed(double P_in, double T_in)
    {
        return get_N(speed_max, Tcorr, Pcorr, T_in, P_in);
    }

    /*
     * Scale the performance maps so that they can be used to model a compressor
     * with different operating characteristics.
     * Steps:
     *  - K_DH is the ratio delta_h_new / delta_h_original
     *  - Scale DH and DH_design by this factor, DH_eta, DH_isen_eta, and
     *    DH_speed are already normalized so they don't need to be scaled
     *  - K_ETA is the ratio eta_new / eta_original
     *  - Scale all eta values by this factor; ETA, eta_design, eta
     */
    void scale_maps()
    {
        DH[] = DH[] * K_DH;
        DH_design *= K_DH;
        ETA[] = ETA[] * K_ETA;
        eta[] *= K_ETA;      // Not currently used
        eta_design *= K_ETA; // Not currently used
    }

    /* Updates the (rho,e) EOS from (P,T) inputs */
    void update_conservative_eos_from_pt(double P, double T)
    {
        EosPT.update(P, T);
        EosRhoE.update(EosPT.rho, EosPT.e);
    }

    /* -------------------------------------------------------------------------
     * A collection of short functions to correct turbomachinery maps as per
     * Glassman 1972.
     */

    /* Evaluates specific heat ratio V_cr for varying inlet conditions */
    double eval_heat_ratio(double T0_std, double P0_std, double Tin, double Pin)
    {
        update_conservative_eos_from_pt(P0_std, T0_std);
        double gm_std = EosRhoE.cp / EosRhoE.cv;
        update_conservative_eos_from_pt(Pin, Tin);
        double gm = EosRhoE.cp / EosRhoE.cv;
        double V_cr_ratio = (gm_std*(gm+1.0)*T0_std)/(gm*(gm_std+1.0)*Tin);
        return V_cr_ratio;
    }

    /* Evaluates scaling factor epsilon */
    double eval_epsilon(double T0_std, double P0_std, double Tin, double Pin)
    {
        // TODO: Combine this with eval_heat_ratio
        import std.math: pow;
        update_conservative_eos_from_pt(P0_std, T0_std);
        double gm_std = EosRhoE.cp / EosRhoE.cv;
        update_conservative_eos_from_pt(Pin, Tin);
        double gm = EosRhoE.cp / EosRhoE.cv;
        auto frac_pow = 1.0 * pow(2.0, 1.9);
        double epsilon = pow(gm_std*(2.0/(gm_std+1.0)), (gm_std/(gm_std-1.0)))
                         / pow(gm*(2.0/(gm+1.0)), (gm/(gm-1.0)));

        return epsilon;
    }

    /* Evaluates specific enthalpy given equivalent h */
    double get_h(double h_eq, double T0_std, double P0_std, double Tin, double Pin)
    {
        double V_cr = eval_heat_ratio(T0_std,  P0_std,  Tin,  Pin);
        double h    = h_eq/(V_cr);
        return h;
    }

    /* Evaluates equivalent enthalpy given h */
    // double get_h_eq(double h, double T0_std, double P0_std, double Tin, double Pin)
    // {
        // V_cr = eval_heat_ratio(T0_std,  P0_std,  Tin,  Pin);
        // h_eq = h * V_cr;
        // return h_eq;
    // }

    /* Evaluates equivalent mass flow rate given mdot */
    double get_mdot_eq(double mdot, double T0_std, double P0_std, double Tin, double Pin)
    {
        import std.math: sqrt;
        double V_cr    = eval_heat_ratio(T0_std, P0_std, Tin, Pin);
        double epsilon = eval_epsilon(T0_std, P0_std, Tin, Pin);
        double mdot_eq = mdot*sqrt(1.0/V_cr)*(P0_std/Pin)*epsilon;
        return mdot_eq;
    }

    /* Evaluates mass flow rate given equivalent mdot */
    double get_mdot(double mdot_eq, double T0_std, double P0_std, double Tin, double Pin)
    {
        import std.math: sqrt;
        double V_cr    = eval_heat_ratio(T0_std, P0_std, Tin, Pin);
        double epsilon = eval_epsilon(T0_std, P0_std, Tin, Pin);
        double mdot    = mdot_eq*sqrt(V_cr)*(Pin/P0_std)/epsilon;
        return mdot;
    }

    /* Evaluates equivalent speed given speed */
    double get_N_eq(double N, double T0_std, double P0_std, double Tin, double Pin)
    {
        import std.math: sqrt;
        double V_cr = eval_heat_ratio(T0_std, P0_std, Tin, Pin);
        double N_eq = N * sqrt(V_cr);
        return N_eq;
    }

    /* Evaluates speed given equivalent speed */
    double get_N(double N_eq, double T0_std, double P0_std, double Tin, double Pin)
    {
        import std.math: sqrt;
        double V_cr = eval_heat_ratio(T0_std, P0_std, Tin, Pin);
        double N = N_eq / sqrt(V_cr);
        return N;
    }

    // -------------------------------------------------------------------------

    void check_data()
    {
        import std.stdio: writeln;

        // Check that efficiency maps (mdot_eta, DH_eta, eta) are the same length
        if (mdot_eta.length != DH_eta.length || mdot_eta.length != eta.length)
        {
            writeln("mdot_eta (length): ", mdot_eta.length);
            writeln("DH_eta   (length): ", DH_eta.length);
            writeln("eta      (length): ", eta.length);
            throw new Exception("Lists defining mdot_eta, PR_eta, and eta are
                    different lengths.");
        }

        // Check that speed maps (mdot_speed, DH_speed, speed) are the same length
        if (mdot_speed.length != DH_speed.length || mdot_speed.length != speed.length)
        {
            writeln("mdot_eta (length): ", mdot_speed.length);
            writeln("PR_eta   (length): ", DH_speed.length);
            writeln("eta      (length): ", speed.length);
            throw new Exception("Lists defining mdot_speed, PR_speed, and speed
                    are different lengths.");
        }

        if (Volume == 0.0) { writeln("Warning: Volume set to 0 m^3"); }
    }

    /*
     * Calculates compressor downstream conditions based on inlet condtions and speed.
     * Inputs:
     *     massflow (kg/s), speed (RPM), P_in (Pa), T_in (K)
     * Outputs:
     *     P_out (Pa), T_out (K), delta_H (kJ), eta
     *
     * Calculation procedure:
     *  - To account for inlet conditions away from the reference condition,
     *    convert mass flow rate, speed, and delta_h to corrected conditions
     *    (mdot_corr, N_cor, h_corr) using the relationships from Glassman
     *    (1972).
     *  - Calculate equivalent enthalpy rise (delta_h_eq) from 2D map using
     *    mdot_eq & N_eq,
     *  - Calculate outlet conditions from isentropic relations using eta
     */
    override double[string]
        calc_downstream(double Massflow, double Speed, double P_in, double T_in,
                        uint verbose=1)
    {
        import std.stdio: writeln, writefln;

        // Convert mass flow and speed to equivalent conditions
        double N_eq = get_N_eq(Speed, Tcorr, Pcorr, T_in, P_in);
        double mdot_eq = get_mdot_eq(Massflow, Tcorr, Pcorr, T_in, P_in);

        if(N_eq > speed_max)
        {
            import std.format;
            throw new SpeedOverrun(format!(
                    "ERROR: Equivalent speed %.3f is greater than maximum "
                    ~ " equivalent speed %2.f.")(N_eq, speed_max));
                    
        }

        // Check if within map
        // TODO: do speed_surge * speed_design and mdot_surge * mdot_design in
        // constructor, then I can just pull mdot_eq straight out of surge
        // interpolator with N_eq as input.
        auto N_eq_nd = get_N_eq(Speed / speed_design, Tcorr, Pcorr, T_in, P_in);
        auto mdot_eq_min = mdot_design * surge_interpolator(N_eq_nd);
        double mdot_surge = get_mdot(mdot_eq_min, Tcorr, Pcorr, T_in, P_in);
        auto delta_h_eq = dh_interpolator(mdot_eq, N_eq);
        auto delta_h = get_h(delta_h_eq, Tcorr, Pcorr, T_in, P_in);

        // Calculate efficiency from second map
        // If efficiency or delta_h is negative, we have run off the map
        auto eta = eta_interpolator(mdot_eq, delta_h_eq);
        if(eta < 0 || delta_h_eq < 0)
        {
            if(mdot_eq < mdot_eq_min) // overran the surge line (LHS)
            {
                import std.conv: to;
                throw new SurgeLineOverrun(
                        mdot_surge,
                        "\nWarning: Compressor trying to operate outside of surge line. "
                        ~ "Minimum mass flow for this operating point is "
                        ~ to!string(mdot_surge)
                        );
            }
            else // overran maximum mdot (RHS)
            {
                // Replace this with eta = nan so we don't accidentally let any of
                // these points through the edge case handling in calc_from_er
                eta = float.nan;
                throw new MassflowOverrun(Massflow,
                    "Warning: Mass flow rate value is greater than the maximum mass "
                    ~ "flow rate for this operating point.");
            }
        }

        // Calculate conditions at other side of compressor using isentropic relations and eta
        update_conservative_eos_from_pt(P_in, T_in);
        auto h_in = EosRhoE.h;
        auto s_in = EosRhoE.s;
        auto rho_in = EosRhoE.rho;

        // Perform thermodynamic energy balance calculation
        auto delta_h_isen = eta / 100.0 * delta_h;
        auto h_out_isen = h_in + delta_h_isen;
        auto h_out = h_in + delta_h;

        // Use isentropic relations to calculate outlet pressure (isentropic
        // efficiency specifies p_out_a = p_out_s)
        update_ps_eos_from_hs(EosPS, h_out_isen, s_in, P_out);
        EosRhoE.update(EosPS.rho, EosPS.e);
        P_out = EosRhoE.p; // P_out is stored
        // Update EOS to actual outlet conditions (h_out, P_out), then calculate
        // other outlet properties
        update_ps_eos_from_hp(EosPS, h_out, P_out, s_in + delta_s);
        EosRhoE.update(EosPS.rho, EosPS.e);
        auto T_out = EosRhoE.T;
        auto e_out = EosRhoE.e;
        auto rho_out = EosRhoE.rho;
        auto delta_H = delta_h * Massflow;

        // Store delta_s for the root-finder guess at next timestep
        delta_s = EosRhoE.s - s_in;

        // Assemble outputs
        double[string] outs;
        outs["P_out"]      = P_out;
        outs["T_out"]      = T_out;
        outs["e_out"]      = e_out;
        outs["rho_in"]     = rho_in; // TODO: w_sh calculator in here
        outs["rho_out"]    = rho_out;
        outs["h_out"]      = h_out;
        outs["delta_h"]    = delta_h;
        outs["eta"]        = eta;
        outs["mdot"]       = Massflow;
        outs["N_eq"]       = N_eq;
        outs["delta_h_eq"] = delta_h_eq;
        // For turbomachines, define PR as > 1.0 (P_out/P_in for compressor)
        outs["PR"]         = P_out / P_in;
        // Need mdot_surge for constraint linearization
        outs["mdot_surge"] = mdot_surge;
        return outs;
    }

    /*
     * Calculates compressor downstream conditions based on inlet condtions and speed.
     * Inputs:
     *     speed (RPM), T_in (K), P_in (Pa), P_out (Pa)
     * Outputs:
     *     massflow (kg/s), T_out (K), delta_H (kJ), eta
     *
     * Using the current maps, we cannot analytically evaluate the massflow and
     * isentropic efficiency from pressure ratio and inlet conditions (as is
     * done for the turbine. Instead, we have to use a Newton-Raphson
     * root finder to calculate mass flow and efficiency for the prescribed
     * inlet conditions.
     *
     * Root finding problem is
     *   g(mdot) = P_out_i  (outlet pressure evaluated using calc_downstream)
     *   0 = P_out - g(mdot)
     *
     * Newton iteration is
     *   mdot_n+1 = mdot_n - (P_out - g(mdot_n)) / - g_dash(mdot_n)
     *
     * Stop when mdot_n+1 - mdot_n <= tol.
     * g_dash cannot be calculated analytically so we find it using finite
     * differences. Current value of mdot is used as initial guess.
     *
     * TODO: Sometimes there are two solutions to this root-finding problem.
     * If we have a sufficiently accurate initial guess, then this is not a
     * problem, but if we do not have a good guess, then this can be a problem.
     * I'm not sure if this will cause problems in practice in the Q1D main
     * code, but could be worth looking in to if I get abrupt mdot changes
     * sometimes.
     */
    override double[string] calc_from_er(
            double Speed,
            double T_in,
            double P_in,
            double P_out,
            double mdot_guess,
            double tol=2E-3,
            uint max_iter=30,
            double relax_factor=1.0)
    {
        import std.stdio;
        import std.conv: to;
        import q1dcfd.utils.errors: NonConvergenceError;
        import std.math: fabs, isNaN;

        // TODO: Best value here?
        enum g_dash_tol = 1E-8; // Normalized value
        // enum H = 1E-8; // H is step for forward diff used to find g_dash
        enum H = 1E-3; // Map gradients more reliable for this step size
        double g_dash, delta, P_out_plus, P_out_i;
        double[string] results, results_plus; // To hold results of calc_downstream
        double mdot_guess_0 = mdot_guess; // Cache initial guess

        // Here we iterate mass flow rate to make P_out (model) = P_out (target).
        // This root finding operation can fail for three expected reasons:
        //
        // 1) The compressor surges, i.e. the target P_out is above the maximum
        //    P_out for any mass flow rate at the current operating point.
        //    For this type of failure, the root finder may either overrun the
        //    surge line (mdot_guess < minimum valid mdot) or overrun the
        //    maximum mass flow rate.
        //
        // 2) The target pressure ratio is very low (i.e. P_out_target is just
        //    above P_in) and we are operating on the right side of the map.
        //    In this case, the root finding problem is ill-conditioned, since
        //    if mdot_guess overruns the desired value by only a small amount,
        //    then we will run off the map.
        //
        // 3) (Rare) The target operating poing is on the left side of the map
        //    and our initial guess is not sufficiently close to the solution.
        //    Here, if the initial guess is near the top of the P_out peak, but
        //    P_out_target is reasonably low, we can easily overrun the surge
        //    line unless a high level of relaxation is used.
        //
        // For all of these problems, we initially try increasing the relaxation.
        // If relaxation reaches some defined limit and we haven't obtained
        // convergence, then we either throw an error or use problem-specific
        // edge case handling.

        double P_out_max = 0.0;         // Max value of P_out during this run
        double mdot_star;               // Value of mdot that maximizes P_out
        double P_out_min = float.nan;   // Min value of P_out during this run
        double mdot_star_star;          // Value of mdot that minimizes P_out
        double mdot_surge;              // Value of mdot at surge

        try
        {
            // Try to find the mdot that gives the required P_out
            foreach(i; 0..max_iter)
            {
                // Calculate outlet state for current guess of mdot, may throw
                // an error
                results = calc_downstream(mdot_guess, Speed, P_in, T_in);
                // Iterate on mdot only, so mdot_surge is always the same
                if(i == 0) mdot_surge = results["mdot_surge"];

                // Get P_out for current mdot guess and store if max or min
                P_out_i = results["P_out"];
                if(P_out_i > P_out_max)
                {
                    P_out_max = P_out_i;
                    mdot_star = mdot_guess;
                }
                // Can't use 'else if' here, since on the first iteration
                // P_out_max and P_out_min should both be overwritten
                if(P_out_i < P_out_min || isNaN(P_out_min))
                {
                    P_out_min = P_out_i;
                    mdot_star_star = mdot_guess;
                }

                // Find g_dash using one-sided difference
                results_plus = calc_downstream(mdot_guess*(1+H), Speed, P_in, T_in);
                P_out_plus = results_plus["P_out"];
                g_dash = (P_out_plus - P_out_i) / (mdot_guess * H);

                // If we are on a very flat part of the curve, the root finder
                // will usually get thrown off the map.
                // Currently, we adjust mdot guess to the right and see if we
                // fall on a better part [what about cases on LHS?].
                if (fabs(g_dash / P_out) <= g_dash_tol)
                {
                    // Op point info
                    writeln("P_in  = ", P_in);
                    writeln("P_out = ", P_out);
                    writeln("T_in  = ", T_in);
                    writeln("Speed = ", Speed);

                    delta = (P_out - P_out_i) / g_dash;
                    auto mdot_next = mdot_guess + relax_factor * delta;
                    writeln("g_dash          = ", g_dash);
                    writeln("delta           = ", delta);
                    writeln("Next mdot_guess = ", mdot_next);

                    // Currently skeptical about this edge case so let's throw
                    // the error at the moment
                    throw new Exception("ERROR: Root finder in compressor "
                            ~ "calc_from_er encountered very low gradient.");

                    // return calc_from_er(Speed, T_in, P_in, P_out, mdot_guess*1.1,
                            // tol, max_iter, relax_factor);
                }

                delta = (P_out - P_out_i) / g_dash;
                mdot_guess += relax_factor * delta;
                if (fabs(delta) < tol) { return results; }
            }
            // For loop fall through: root finding operation did not converge
            throw new NonConvergenceError(
                    "Root finder in compressor calc_from_er did not converge."
                    ~ "\nMaximum number of iterations, " ~ to!string(max_iter)
                    ~ ", exceeded.\n");
        }

        // Mass flow rate dropped below the minimum admissible value for this
        // operating point, this can result from either surge, an
        // ill-conditioned root finding problem, or an invalid initial guess.
        catch(SurgeLineOverrun e)
        {
            // Invalid initial guess, revise and try again
            import std.math: isNaN;
            if(isNaN(mdot_star))
            {
                return calc_from_er(Speed, T_in, P_in, P_out, 1.1 * mdot_guess_0);
            }
            // Test if this is potentially a surge case
            if(P_out_max < P_out)
            {
                return surge_case("surge line overrun", Speed, T_in, P_in,
                        P_out, mdot_guess_0, tol, max_iter, relax_factor,
                        mdot_star, P_out_max);
            }
            // Not surge, overran the surge line for this op point due to either 1)
            // the low pressure ratio or 2) 'sharpness' of pressure vs mdot curve.
            // First, try to converge this case with lots of relaxation.
            else
            {
                return ill_conditioned_case(ErrCode.surge_line_overrun, Speed,
                        T_in, P_in, P_out, mdot_guess_0, tol, max_iter,
                        relax_factor, P_out_max, mdot_star, P_out_min,
                        mdot_star_star, mdot_surge);
           }
        }

        // Mass flow rate above maximum admissible value for this operating
        // point, either from surge, an ill-conditioned root-finding problem, or
        // a bad initial guess.
        catch(MassflowOverrun e)
        {
            // Invalid initial guess, revise and try again
            import std.math: isNaN;
            if(isNaN(mdot_star))
            {
                return calc_from_er(Speed, T_in, P_in, P_out, 0.9 * mdot_guess_0);
            }
            // Check if this is potentially a surge case (case 1).
            if(P_out_max < P_out)
            {
                return surge_case("mass flow overrun", Speed, T_in, P_in, P_out,
                        mdot_guess_0, tol, max_iter, relax_factor, mdot_star,
                        P_out_max);
            }
            // Ill-conditioned root finding problem, handle the same way as for
            // a surge line overrun.
            else
            {
                return ill_conditioned_case(ErrCode.mass_flow_overrun, Speed,
                        T_in, P_in, P_out, mdot_guess_0, tol, max_iter,
                        relax_factor, P_out_max, mdot_star, P_out_min,
                        mdot_star_star, mdot_surge);
            }
        }

        // Surge can also result in the root finder getting stuck at the top of
        // the pressure vs mdot peak and timing out
        catch(NonConvergenceError e)
        {
            // Check if this is potentially a surge case.
            if(P_out_max < P_out)
            {
                return surge_case("non-convergence error", Speed, T_in, P_in,
                        P_out, mdot_guess_0, tol, max_iter, relax_factor,
                        mdot_star, P_out_max);
            }

            // Otherwise, root-finding operation timed out for an unknown reason
            // Try one last time with heaps of relaxation.
            // [TODO: Should probably go straight to bisection instead.]
            else
            {
                // Here go straight to a really high relaxation case
                immutable double low_rf = 0.05;
                return ill_conditioned_case(ErrCode.non_convergence, Speed,
                        T_in, P_in, P_out, mdot_guess_0, tol, max_iter, low_rf,
                        P_out_max, mdot_star, P_out_min, mdot_star_star,
                        mdot_surge);
            }
        }

        // Previously had fallback case for arbitrary exception here.
        // Current error handling approach should catch any convergence error so
        // I think it's best to let an error be thrown if it's from an
        // unexpected source.
    }

    /*
     * Handle a surge case for the compressor.
     * This is where the compressor is unable to provide the desired outlet
     * pressure for any mass flow rate (see calc_from_er for more details).
     */
    double[string] surge_case(
            string error_case,
            double Speed,
            double T_in,
            double P_in,
            double P_out,
            double mdot_guess,
            double tol,
            uint max_iter,
            double relax_factor,
            double mdot_star,
            double P_out_max)
    {
        // This might be a surge case; to be sure, first try to converge it
        // using more relaxation
        immutable double surge_min_rf = 0.4;
        if(relax_factor > surge_min_rf)
        {
            return calc_from_er(Speed, T_in, P_in, P_out, mdot_guess, tol,
                    max_iter, surge_min_rf);
        }
        // If this operating point still doesn't converge, then we have surge.
        // Use the operating point that obtains max P_out as the solution.
        else
        {
            import std.stdio;
            writefln("\nWARNING: Surge occured (" ~ error_case ~ ").");
            writefln("P_out (target) = %.4g", P_out);
            writefln("P_out (max)    = %.4g", P_out_max);
            writefln("mdot_star      = %.4g", mdot_star);
            writefln("T_in           = %.4g", T_in);
            writefln("P_in           = %.4g", P_in);
            writefln("Speed          = %.4g", Speed);

            return calc_downstream(mdot_star, Speed, P_in, T_in);
        }
    }

    /*
     * Handle a case where the root-finding problem in calc_from_er is
     * ill-conditioned.
     * This happens when the pressure ratio is extremely low or the pressure vs
     * mdot curve is very 'sharp'.
     */
    double[string] ill_conditioned_case(
            ErrCode err_code,
            double Speed,
            double T_in,
            double P_in,
            double P_out,
            double mdot_guess_0,
            double tol,
            uint max_iter,
            double relax_factor,
            double P_out_max,
            double mdot_star,
            double P_out_min,
            double mdot_star_star,
            double mdot_surge)
    {
        immutable double min_rf = 0.05;
        if(relax_factor > min_rf)
        {
            return calc_from_er(Speed, T_in, P_in, P_out, mdot_guess_0, tol,
                    max_iter, relax_factor/2);
        }
        // Failed, even with high relaxation
        else
        {
            // If we bracket the root, use bisection as a fallback (guaranteed
            // convergence)
            if(P_out_min < P_out && P_out < P_out_max)
                return calc_from_er_bisection(Speed, T_in, P_in, P_out,
                        mdot_star_star, mdot_star);

            // If we overrun the surge line, use mdot_surge to bracket the root
            // (unless P_out < P_in).
            else if(err_code == ErrCode.surge_line_overrun 
                    && P_in <= P_out && P_out < P_out_max)
                return calc_from_er_bisection(Speed, T_in, P_in, P_out,
                        mdot_surge+1E-6, mdot_star);

            // Otherwise, we might have a 'lower PR' case on the RHS of the
            // mdot vs P_out curve
            // else if(err_code == ErrCode.mass_flow_overrun
            // TODO: Why do we sometimes get this error on surgeline overrun?
            else if(P_out_min > P_out)
            {
                // There is some chance we can end up here due to a flat P vs
                // mdot curve which shoots the root finder off to the right; try
                // to check for this by seeing if PR is sufficiently low
                immutable double PR_threshold = 1.1; // FIX0729
                if(P_out/P_in > PR_threshold)
                {
                    import std.stdio;
                    string msg = "ERROR: Compressor root finder failed with"
                        ~ " P_out_min > P_out. Pressure ratio is not"
                        ~ " sufficiently low for this to be a 'low PR' case.\n";
                    writefln("\nFailed low PR case, PR = %.3f", P_out/P_in);
                    writeln("\n\n\nIF THIS ERROR OCCURS, I COULD WRITE A "
                            ~ "GUARANTEED ROBUST BISECTION METHOD BY PUTTING "
                            ~ "A FLAG TO SUPRESS SURGE AND MDOT OVERRUN ERRORS "
                            ~ "IN calc_downstream.\n\n");
                    throw new CalcFromErNonConvergence(error_map[err_code],
                            Speed, T_in, P_in, P_out, P_out_max, mdot_star,
                            P_out_min, mdot_star_star, mdot_guess_0, mdot_surge,
                            msg);
                }

                // Low PR, just use P_out = P_out_min (will be close)
                double[string] results;
                results = calc_downstream(mdot_star_star, Speed, P_in, T_in);
                results["P_out"] = P_out; // Override P_out (should be very close)

                import std.stdio;
                writefln("\nWARNING: Low PR case occured.");
                writefln("P_out (target) = %.4g", P_out);
                writefln("P_out (min)    = %.4g", P_out_min);
                writefln("mdot_star_star = %.4g", mdot_star_star);
                writefln("Efficiency     = %.4g", results["eta"]); // EDIT

                return results;
            }

            // Something bad happened
            else
            {
                import std.stdio;
                writeln("\n\n\nIF THIS ERROR OCCURS, I COULD WRITE A "
                        ~ "GUARANTEED ROBUST BISECTION METHOD BY PUTTING "
                        ~ "A FLAG TO SUPRESS SURGE AND MDOT OVERRUN ERRORS "
                        ~ "IN calc_downstream.\n\n");
                throw new CalcFromErNonConvergence(error_map[err_code], Speed,
                        T_in, P_in, P_out, P_out_max, mdot_star, P_out_min,
                        mdot_star_star, mdot_guess_0, mdot_surge);
            }
        }
    }

    /*
     * Calculates compressor downstream conditions based on inlet condtions and
     * speed using the bisection method.
     * Compressor outlet pressure function is continuous so this necessarily
     * converges.
     *
     * Root finding problem is
     *   g(mdot) = P_out_i  (outlet pressure evaluated using calc_downstream)
     *   0 = P_out - g(mdot)
     *
     * See 'calc_from_er' for more detailed docs.
     */
    double[string] calc_from_er_bisection(
            double Speed,
            double T_in,
            double P_in,
            double P_out,      // target P_out
            double mdot_low,   // mdot where P_out_i < P_out (target)
            double mdot_high,  // mdot where P_out_i > P_out (target)
            double tol=5E-3,   // bisection has slow convergence so use higher tol
            uint max_iter=100) // Not currently used, should incorporate
    {
        import std.conv: to;
        import std.math: fabs;
        import q1dcfd.utils.errors: NonConvergenceError;

        double[string] results_low, results_mid, results_high;
        double mdot_mid = (mdot_high + mdot_low) / 2.0;
        results_low  = calc_downstream(mdot_low, Speed, P_in, T_in);
        results_mid  = calc_downstream(mdot_mid, Speed, P_in, T_in);
        results_high = calc_downstream(mdot_high, Speed, P_in, T_in);

        if(fabs(results_high["P_out"] - results_low["P_out"]) < tol)
        {
            return results_mid;
        }
        else if(results_mid["P_out"] < P_out)
        {
            return calc_from_er_bisection(Speed, T_in, P_in, P_out,
                    mdot_mid, mdot_high);
        }
        else
        {
            return calc_from_er_bisection(Speed, T_in, P_in, P_out,
                    mdot_low, mdot_mid);
        }
    }

    /*
     * Used specially for the compressor class, used to restart the root finding
     * problem when the compressor overruns the surge line
     */
    class SurgeLineOverrun : Exception
    {
        // Mass flow rate for which surge occurs for the current operating condition
        double mdot_surge;

        this(double mdot, string msg, string file = __FILE__, size_t line = __LINE__) 
        {
            this.mdot_surge = mdot;
            super(msg, file, line);
        }
    }

    /*
     * Used specially for the compressor class; restarts the root-finding
     * problem with more relaxation if overrun the max mass flow rate.
     */
    class MassflowOverrun : Exception
    {
        double mdot_max;

        this(double mdot, string msg, string file=__FILE__, size_t line =__LINE__) 
        {
            this.mdot_max = mdot;
            super(msg, file, line);
        }
    }

    /* Similar to the above cases. */
    class SpeedOverrun : Exception
    {
        this(string msg, string file=__FILE__, size_t line =__LINE__) 
        {
            super(msg, file, line);
        }
    }

    /*
     * Root finder in function 'calc_from_er' did not converge, print some
     * diagnostics and throw error.
     */
    class CalcFromErNonConvergence : Exception
    {
        import std.stdio;

        this(
            string error_type,
            double Speed,
            double T_in,
            double P_in,
            double P_out,
            double P_out_max,
            double mdot_star,
            double P_out_min,
            double mdot_star_star,
            double mdot_guess_0,
            double mdot_surge,
            string custom_msg="",
            string file=__FILE__,
            size_t line =__LINE__) 
        {
            writeln();
            if(custom_msg == "")
                writeln("\nUnexpected error in compressor root finder (function "
                  ~ "calc_from_er).\n");
            else writeln(custom_msg);
            writefln("Speed          = %.4g", Speed);
            writefln("T_in           = %.4g", T_in);
            writefln("P_in           = %.4g", P_in);
            writefln("P_out (target) = %.4g", P_out);
            writefln("P_out (max)    = %.4g", P_out_max);
            writefln("mdot_star      = %.4g", mdot_star);
            writefln("P_out (min)    = %.4g", P_out_min);
            writefln("mdot_star_star = %.4g", mdot_star_star);
            writefln("mdot_guess_0   = %.4g", mdot_guess_0);
            writefln("mdot_surge     = %.4g", mdot_surge);
            writeln();

            string msg = "\nRoot-finding operation in calc_from_er failed due to a "
                    ~ error_type ~ " that could not be handled appropriately.\n";
            super(msg, file, line);
        }
    }
}

/*
 * Updates q1dcfd tabular EoS using (h,s) inputs.
 * Iterates on pressure to obtain a (p,s) input.
 * Inputs:
 *     h0 - known enthalpy
 *     s0 - known entropy
 *     p  - guess of pressure
 *
 * Currently for testing here, just using a CoolProp HEOS backend. Will use a
 * q1d backend when ported to q1dcfd.
 */
void update_ps_eos_from_hs(
        EOSBase EosPS,
        double h0,
        double s0,
        double p,
        double tol=1.00E-2,
        uint max_iter=1_000)
{
    import std.math: fabs;
    double H = 1E-8;
    double h, h_plus, h_minus, f_dash, delta;
    foreach(i; 0..max_iter)
    {
        // Value of h for current estimate p
        EosPS.update(p, s0);
        h = EosPS.h;

        // Compute dh/dp|s
        EosPS.update((1+H) * p, s0);
        h_plus = EosPS.h;
        EosPS.update((1-H) * p, s0);
        h_minus = EosPS.h;
        f_dash = (h_plus - h_minus) / (2*H*p);
        delta = (h0 - h) / f_dash;
        p += delta;
        if (fabs(delta) < tol) {
            EosPS.update(p, s0);
            return;
        }
    }

    // If loop falls through, root finder has failed to converge
    throw new Exception(
        "Root-finding problem to calculate p from (h, s) did not converge.");
}

/*
 * Updates q1dcfd tabular EoS using (h,s) inputs.
 * Iterates on pressure to obtain a (p,s) input.
 * Inputs:
 *     h0 - known enthalpy
 *     s0 - known entropy
 *     p  - guess of pressure
 *
 * Currently for testing here, just using a CoolProp HEOS backend. Will use a
 * q1d backend when ported to q1dcfd.
 */
void update_ps_eos_from_hp(
        EOSBase EosPS,
        double h0,
        double p0,
        double s,
        double tol=1.00E-2,
        uint max_iter=1_000)
{
    import std.math: fabs;
    double H = 1E-8;
    double h, h_plus, h_minus, f_dash, delta;
    foreach(i; 0..max_iter)
    {
        // Value of h for current estimate s
        EosPS.update(p0, s);
        h = EosPS.h;

        // Compute dh/ds|p
        EosPS.update(p0, (1+H)*s);
        h_plus = EosPS.h;
        EosPS.update(p0, (1-H)*s);
        h_minus = EosPS.h;
        f_dash = (h_plus - h_minus) / (2*H*s);
        delta = (h0 - h) / f_dash;
        s += delta;
        if (fabs(delta) < tol) {
            EosPS.update(p0, s);
            return;
        }
    }

    // If loop falls through, root finder has failed to converge
    throw new Exception(
        "Root-finding problem to calculate s from (p, h) did not converge.");
}

// TODO: Fix the unittests so that they work for the q1d EOS backend
/* Take the (h,s) update for q1d (nominally) backends for a spin */
// unittest
// {
    // import coolprop;

    // // Want to do a (h,s) update using tabular backend
    // // Operating state is:
    // double P = 12E6; // Pa
    // double T = 550;  // K
    // double H = 716716.7278036827; // J/kg
    // double S = 2368.950425023637; // J/kg.K

    // auto Eos = new AbstractState("HEOS", "CO2");
    // double P_guess = 13E6; // Pa
    // update_q1d_eos_from_h_s(Eos, H, S, P_guess);

    // // Check accuracy, p calculated from EoS should be the same as that known
    // // from the inital state (within some tolerance)
    // double err = 1 - (Eos.p/P);
    // assert(err < 1E-8);
// }



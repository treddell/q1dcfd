module q1dcfd.maps.turbo_utils;

import std.typecons;

import mir.ndslice;
import lubeck;

alias odarr = Slice!(double*, 1);
alias tdarr = Slice!(double*, 2);

template ReadArraysFromAA(string[] arr_names, string aa_name)
{
    import std.format;
    string ReadArraysFromAA()
    {
        string str_out = "";
        foreach(name; arr_names) {
            str_out ~= format!("%s = %s[\"%s\"]; ")(name, aa_name, name);
        }
        return str_out;
    }
}

/*
 * Meshgrid with n01 rows and n23 columns analgous to those created by numpy and
 * MATLAB with endpoints inclusive:
 *
 * | i0, i0, i0, ... |       | i2, ..., i3  |
 * | .               |  and  | i2, ..., i3  |
 * | .               |       | i2, ..., i3  |
 * | i1, i1, i1 ...  |       | ...      ... |
 */
class meshgrid
{
    tdarr grid0, grid1;

    /* Constructor creates the meshgrids, see class description for parameters */
    this(double i0, double i1, uint n01,
         double i2, double i3, uint n23)
    {
        grid0 = slice!double(n01, n23);
        grid1 = slice!double(n01, n23);
        auto d01 = (i1 - i0) / (n01 - 1);
        auto d23 = (i3 - i2) / (n23 - 1);
        foreach(j; 0..n01) { grid0[j, 0..$] = i0 + j * d01; }
        foreach(j; 0..n23) { grid1[0..$, j] = i2 + j * d23; }
    }

    void display()
    {
        import std.stdio: writeln;
        writeln("\ngrid0 = ");
        foreach(j; 0..grid0.length) { writeln(grid0[j, 0..$]); }
        writeln("\ngrid1 = ");
        foreach(j; 0..grid1.length) { writeln(grid1[j, 0..$]); }
    }
}

/* linearly spaced vector, inclusive of x0 and x1 */
odarr linspace(double x0, double x1, uint n)
{
    auto arr = slice!double(n);
    auto d = (x1 - x0) / (n - 1);
    foreach(i; 0..n) { arr[i] = x0 + (i * d); }
    return arr;
}

/* Counts the number of lines of a text file */
ulong count_lines(string path)
{
    import std.algorithm: count;
    import std.stdio: File;
    import std.conv: to;

    // Read header to initialise arrays
    auto f = File(path, "r");
    string contents;
    f.readf!"%s"(contents);
    auto n_lines = count(contents, "\n");
    f.close();
    return n_lines;
}

/* Counts the of rows and columns fo the maps */
Tuple!(size_t, size_t) count_rows_cols(string path)
{
    import std.typecons;
    import std.string;
    import std.algorithm: count;
    import std.stdio: File;
    import std.conv: to;

    // Read header to initialise arrays
    auto f = File(path, "r");
    string contents;
    f.readf!"%s"(contents);
    auto lines = contents.split("\n");

    auto rows = count(contents, "\n") - 1; // -1 to account for header
    auto cols = lines[1].split(",").length;
    f.close();

    return tuple(rows, cols);
}

/* Assumes that the first row is a header containing the array names */
double[][string] read_data_with_header_from_csv(string file_path)
{
    import std.string;
    import std.conv: to;
    import std.stdio: File;

    // Read header and initialise arrays to correct lengths
    double[][string] arrays;
    auto arr_length = count_lines(file_path) - 1; // account for header
    auto f = File(file_path, "r");
    auto header = f.readln().strip();
    auto array_names = header.split(",");
    foreach(name; array_names) arrays[name].length = arr_length;

    // Loop through the lines and store the values
    uint i = 0;
    while(!f.eof())
    {
        auto line = f.readln().strip();
        auto values = line.split(",");
        foreach(j, val; values) { arrays[array_names[j]][i] = to!double(val); }
        i++;
    }
    f.close();

    return arrays;
}

double[string] read_constants_with_header_from_csv(string file_path)
{
    import std.string;
    import std.conv: to;
    import std.stdio: File;

    // Read header and initialise arrays to correct lengths
    double[string] constants;
    auto f = File(file_path, "r");
    auto names = f.readln().strip().split(",");
    auto values = f.readln().strip().split(",");
    foreach(j, val; values) { constants[names[j]] = to!double(val); }
    f.close();
    return constants;
}

tdarr read_tables_from_csv(string file_path)
{
    import std.string;
    import std.conv: to;
    import std.stdio: File;

    auto f = File(file_path, "r");
    auto row = f.readln().strip(); // First row is a header
    auto table_dims = count_rows_cols(file_path);
    tdarr table_data;
    table_data = slice!double(table_dims[0], table_dims[1]);
    string[] values;
    values.length = table_dims[1];

    uint i = 0;
    while(!f.eof())
    {
        row = f.readln().strip();
        values = row.split(",");
        foreach(j, val; values) { table_data[i, j] = to!double(val); }
        i++;
    }
    f.close();

    return table_data;
}

// For the meshgrid code
unittest
{
    auto test_grid = new meshgrid(0.0, 2.0, 3, 1.0, 2.0, 2);
    assert(test_grid.grid0 == 
        [[0.0, 0.0],
         [1.0, 1.0],
         [2.0, 2.0]]);
    assert(test_grid.grid1 == 
        [[1.0, 2.0],
         [1.0, 2.0],
         [1.0, 2.0]]);
}

// For the linspace code
unittest
{
    auto test_arr_1 = linspace(0.0, 2.0, 5);
    assert(test_arr_1 == [0.0, 0.5, 1.0, 1.5, 2.0]);

    // Sadly, constant multipliers change the type so they need to be
    // implemented on a separate line
    auto test_arr_2 = linspace(0.0, 2.0, 5);
    test_arr_2[] = test_arr_2[] * 2.0;
    assert(test_arr_2 == [0.0, 1.0, 2.0, 3.0, 4.0]);

    // Works, but causes type problems
    // auto test_arr_2 = linspace(0.0, 2.0, 5) * 2.0;
    // assert(test_arr_2 == [0.0, 1.0, 2.0, 3.0, 4.0]);
}


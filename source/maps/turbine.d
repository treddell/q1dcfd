module q1dcfd.maps.turbine;

import std.typecons;
import coolprop;
import mir.ndslice;
import lubeck;

import q1dcfd.maps.turbomachine: Turbomachine;

alias odarr = Slice!(double*, 1);
alias tdarr = Slice!(double*, 2);
alias iodarr = Slice!(immutable(double)*, 1);

/*
 * Currently only models the synchronous (fixed speed) case.
 */
class Turbine : Turbomachine
{
    import std.math: pow, sqrt;
    import q1dcfd.maps.turbo_utils;
    import mir.interpolate.linear;

    string method = "linear";
    string fluid;

    AbstractState Eos;

    double[] uc, eta, er, mfp;
    double T0_design, P0_design, er_design, U_C_design, mfp_design,
           speed_design, eta_design, Volume, speed_nominal;
    immutable string[] ARRAYS = ["uc", "eta", "er", "mfp"];
    immutable string[] CONSTANTS = ["T0_design", "P0_design", "er_design",
              "U_C_design", "mfp_design", "speed_design", "eta_design",
              "Volume", "speed_nominal"];

    double ER_mfp_min, ER_mfp_max, ER_speed_min, ER_speed_max;

    Linear!(double, 1, immutable(double)*) ER, ETA, MFP;

    /*
     * Reads performance data from CSV files.
     * Create maps of ETA, and ER at equivalent conditions.
     * Maps are dimensionalised based on design point.
     */
    this(string data_path, string fluid, double er_d=0.0, double mfp_d=0.0,
            double T0_d=0.0, double P0_d=0.0)
    {
        import std.algorithm: minIndex, minElement;
        import std.algorithm.searching: minElement, maxElement;

        this.fluid = fluid;
        Eos = new AbstractState("BICUBIC&HEOS", fluid);

        // Read performance data from CSV files
        auto arr_data = read_data_with_header_from_csv(data_path ~ "/arrays.csv");
        // Dummy data is indicated by -1s, the following loop removes this
        foreach(key; arr_data.byKey()) {
            if (minElement(arr_data[key]) < 0.0) {
                arr_data[key] = arr_data[key][0..minIndex(arr_data[key])];
            }
        }
        mixin(ReadArraysFromAA!(ARRAYS, "arr_data"));
        auto const_data = read_constants_with_header_from_csv(data_path ~ "/constants.csv");
        mixin(ReadArraysFromAA!(CONSTANTS, "const_data"));

        // Establish limits of input data (Note: I switched these to be max-min
        // or min-max, Josh/Ingo had it the other way around)
        auto min_er = minElement(er); // None of these are needed for fixed speed
        auto max_er = maxElement(er);
        auto min_uc = minElement(uc);
        auto max_uc = maxElement(uc);

        // Manually change design conditions if user has supplied them
        if(er_d != 0.0)  er_design  = er_d;
        if(mfp_d != 0.0) mfp_design = mfp_d;
        if(T0_d != 0.0)  T0_design  = T0_d;
        if(P0_d != 0.0)  P0_design  = P0_d;

        // Really hideous way of setting up mfp and er interpolation vectors
        auto mfp_scaled = slice!double(mfp.length);
        mfp_scaled[] = mfp[];
        mfp_scaled[] *= mfp_design;
        // This evaluates er_scaled = er * (er_design - 1) + 1
        auto er_scaled = slice!double(er.length);
        er_scaled[] = er[];
        er_scaled[] *= (er_design - 1.0);
        er_scaled[] += 1.0;
        auto eta_scaled = eta.sliced * eta_design;

        // Reverse vectors for interpolation (unfortunately can't use
        // slice.reversed due to type bug)
        auto mfp_scaled_reversed = reverse_1dslice(mfp_scaled);
        auto er_scaled_reversed  = reverse_1dslice(cast(odarr) er_scaled);
        // auto eta_scaled_reversed = eta_scaled;
        // auto uc_reversed  = uc.sliced;

        // Find limits
        ER_mfp_min = minElement(mfp_scaled);
        ER_mfp_max = maxElement(mfp_scaled);
        ER_speed_min = 1.0;
        ER_speed_max = 1.0;

        // Fixed speed, only have single mfp vs er line
        ER = linear!double(cast(iodarr) mfp_scaled_reversed, er_scaled_reversed);
        MFP = linear!double(cast(iodarr) er_scaled_reversed, mfp_scaled_reversed);
        // Efficiency interpolant (doesn't need reversing)
        ETA = linear!double(cast(iodarr) uc.sliced, eta_scaled);
    }

    override double get_speed_nominal() { return speed_nominal; }

    odarr reverse_1dslice(odarr fwd_slice)
    {
        auto rev_slice = slice!double(fwd_slice.length!0);
        foreach(i; 0..fwd_slice.length!0) rev_slice[i] = fwd_slice[$-1-i];
        return rev_slice;
    }

    /* -------------------------------------------------------------------------
     * A collection of short functions to correct turbomachinery maps as per
     * Glassman 1972.
     */

    /* Calculate turbine spouting velocity from inlet conds. */
    double C_0(double T_in, double P_in, double P_out)
    {
        Eos.update(PT_INPUTS, P_in, T_in);
        double gm = Eos.cpmass / Eos.cvmass;
        double C_P = Eos.cpmass;
        double a = 2.0 * C_P * T_in * (1.0 - pow(P_out/P_in, (gm-1.0)/gm));
        double C0 = sqrt(a);
        return C0;
    }

    /* Calculates MFP from mdot and performs required unit conversion (pressure in bar) */
    double get_MFP(double mdot, double T_in, double P_in) {
        return mdot * sqrt(T_in) / (P_in / pow(10, 5));
    }

    /* Calculates mdot from MFP and performs required unit conversions (pressure in bar) */
    double get_mdot(double MFP, double T_in, double P_in) {
        return MFP * (P_in / pow(10, 5)) / sqrt(T_in);
    }

    /* Converts corrected speed to speed */
    double get_N(double N_corr, double T0_in, double T0_d) {
        return N_corr * sqrt(T0_in) / sqrt(T0_d);
    }

    /* Converts speed to corrected speed */
    double get_N_corr(double N, double T0_in, double T0_d) {
        return N * sqrt(T0_d) / sqrt(T0_in);
    }
    // -------------------------------------------------------------------------

    void check_data()
    {
        import std.stdio: writeln;

        // writeln("Turbine input data check not yet implemented.");
        if (uc.length != eta.length || uc.length <= 1 || eta.length <= 1)
        {
            writeln("uc :\n", uc);
            writeln("eta:\n", eta);
            throw new Exception("Error in lists defining UC and eta.");
        }
        if (er.length != mfp.length || er.length <= 1 || mfp.length <= 1)
        {
            writeln("er :\n", er);
            writeln("mfp:\n", mfp);
            throw new Exception("Error in lists defining ER and MFP.");
        }
        if(Volume == double.nan)
        {
            writeln("Volume not defined. Volume set to 0.0 m^3");
            Volume = 0.0;
        }
    }

    /* Helper function for finding speed constraints. */
    double calc_pr(double mdot, double P_in, double T_in)
    {
        double mfp = get_MFP(mdot, T_in, P_in);
        double er = ER(mfp);
        return er;
    }

    /*
     * Calculates turbine downstream conditions based on inlet condtions and speed.
     * Inputs:
     *   massflow (kg/s), speed (RPM) P_in (Pa), T_in
     * Outputs:
     *   P_out (Pa), T_out (K), eta
     *
     * Calculation procedure is as follows:
     * - Find massflow parameter (mfp_op) and equivalent speed (N_corr)
     * - Calculate expansion ratio (er_op) using from map using mfp_op
     * - Use isentropic relationships to calculate the spouting velocity and
     *   velocity ratio (uc_op)
     * - Calculate efficiency (eta_op) from map using uc_op
     * - Calculate delta_h and other outlet conditions from isentropic relations
     */
    override double[string]
        calc_downstream(double Massflow, double Speed, double P_in, double T_in,
                        uint verbose=0)
    {
        import std.stdio: writeln, writefln;

        // Calculate mfp for given mass flow rate and inlet conditions
        auto mfp_op = get_MFP(Massflow, T_in, P_in);
        // Calculate corrected speed based on deviation of inlet conditions from design
        auto N_corr = get_N_corr(Speed, T_in, T0_design);

        // Limit bounds for interpolation
        if(mfp_op < ER_mfp_min)
        {
            mfp_op = ER_mfp_min;
            writeln("WARNING: Turbine interpolation trying to exceed min mass "
                    ~"flow. Normalised mass flow parameter (mfp_op) limited to ",
                    mfp_op);
        }
        else if(mfp_op > ER_mfp_max)
        {
            mfp_op = ER_mfp_max;
            writeln("WARNING: Turbine interpolation trying to exceed max mass "
                    ~"flow. Normalised mass flow parameter (mfp_op) limited to ",
                    mfp_op);
        }

        // Interpolate to find expansion ratio
        auto er_op = ER(mfp_op);
        auto P_out = P_in / er_op;

        // Calculate spouting velocity and design point spouting velocity
        auto C0_op = C_0(T_in, P_in, P_out);
        auto C0_d  = C_0(T0_design, P_in, P_in/er_design);

        // Calculate U_C based on C0_op and fraction of speed from nominal
        // Avoids need to calc tip_speed/geom, assumes turbine design point U_C=0.7
        auto uc_op = U_C_design * (Speed / speed_design) / (C0_op / C0_d);

        // Interpolate eta from U_C map
        auto eta_op = ETA(uc_op);

        // Calculate remaining outlet conditions from efficiency
        Eos.update(PT_INPUTS, P_in, T_in);
        auto h_in = Eos.hmass;
        auto s_in = Eos.smass;
        auto rho_in = Eos.rhomass;
        Eos.update(PSmass_INPUTS, P_out, s_in);
        auto h_out_isen = Eos.hmass;

        auto delta_h_isen = h_out_isen - h_in;
        auto delta_h = delta_h_isen * eta_op / 100.0;
        auto h_out = h_in + delta_h;
        Eos.update(HmassP_INPUTS, h_out, P_out);
        auto T_out = Eos.T;
        auto e_out = Eos.umass;
        auto rho_out = Eos.rhomass;

        // Assemble outputs
        double[string] outs;
        outs["P_out"]   = P_out;
        outs["T_out"]   = T_out;
        outs["e_out"]   = e_out;
        outs["rho_in"]  = rho_in; // TODO: calculate w_sh in here instead.
        outs["rho_out"] = rho_out;
        outs["eta"]     = eta_op;
        outs["U_C"]     = uc_op;
        outs["ER"]      = er_op;
        outs["MFP"]     = mfp_op;
        outs["mdot"]    = Massflow;
        outs["delta_h"] = delta_h;
        // For turbomachines, define PR as > 1.0 (PR = P_in/P_out (= ER) for turb)
        outs["PR"]      = er_op;
        // Required for compatibility with MPC
        outs["mdot_surge"] = double.nan;
        return outs;
    }

    /*
     * Calculates turbine mass flow rate and outlet temperature based on inlet
     * state and outlet pressure.
     * Inputs:
     *   speed (RPM), T_in (K), P_in (Pa), P_out (Pa)
     * Outputs:
     *   mdot (kg/s), T_out (K), rho_out (kg/m3), ETA, U_C, ER, MFP, delta_h (kJ/kg)
     *
     * Calculation procedure is as follows:
     * - Using expansion ratio (er_op), calculate current massflow parameter
     *   (mfp_op) from map
     * - Find mass flow rate from mfp_op
     * - Calculate corrected speed (N_corr) from inlet conditions and speed
     * - Use isentropic relationships to calculate the spouting velocity and
     *   velocity ratio (uc_op)
     * - Calculate efficiency (eta_op) from map using uc_op
     * - Calculate delta_h and other outlet conditions from isentropic relations
     */
    override double[string]
        calc_from_er(double Speed, double T_in, double P_in, double P_out,
            // redundant inputs, reqd to match function prototype
            double mdot_guess=float.nan, double tol=float.nan, uint max_iter=0,
            double relax_factor=float.nan)

    {
        // Calculate MFP for the current expansion ratio
        auto er_op = P_in / P_out;
        auto mfp_op = MFP(er_op);
        // Find mass flow rate from MFP
        auto mdot = get_mdot(mfp_op, T_in, P_in);

        // Calculate corrected speed based on deviation of inlet conditions from design
        auto N_corr = get_N_corr(Speed, T_in, T0_design);

        // // Limit bounds for interpolation
        // if(mfp_op < ER_mfp_min)
        // {
            // import std.stdio: writeln, writefln;
            // writefln("WARNING: Turbine interpolation trying to exceed min mass "
                    // ~ "flow. Normalised mass flow parameter (mfp = %.3f) limited "
                    // ~ "to %.3f.", mfp_op, ER_mfp_min);
            // mfp_op = ER_mfp_min;
        // }
        // else if(mfp_op > ER_mfp_max)
        // {
            // import std.stdio: writeln, writefln;
            // writefln("WARNING: Turbine interpolation trying to exceed min mass "
                    // ~ "flow. Normalised mass flow parameter (mfp = %.3f) limited "
                    // ~ "to %.3f.", mfp_op, ER_mfp_max);
            // mfp_op = ER_mfp_max;
        // }

        // Calculate spouting velocity and design point spouting velocity
        auto C0_op = C_0(T_in, P_in, P_out);
        auto C0_d  = C_0(T0_design, P_in, P_in/er_design);

        // Calculate U_C based on C0_op and fraction of speed from nominal
        // Avoids need to calc tip_speed/geom, assumes turbine design point U_C=0.7
        auto uc_op = U_C_design * (Speed / speed_design) / (C0_op / C0_d);

        // Interpolate eta from U_C map
        auto eta_op = ETA(uc_op);

        // Calculate remaining outlet conditions from efficiency
        Eos.update(PT_INPUTS, P_in, T_in);
        auto h_in = Eos.hmass;
        auto s_in = Eos.smass;
        Eos.update(PSmass_INPUTS, P_out, s_in);
        auto h_out_isen = Eos.hmass;

        auto delta_h_isen = h_out_isen - h_in;
        auto delta_h = delta_h_isen * eta_op / 100.0;
        auto h_out = h_in + delta_h;
        Eos.update(HmassP_INPUTS, h_out, P_out);
        auto T_out = Eos.T;
        auto rho_out = Eos.rhomass;

        // Assemble outputs
        double[string] outs;
        outs["mdot"]    = mdot;
        outs["T_out"]   = T_out;
        outs["rho_out"] = rho_out;
        outs["h_out"]   = h_out;
        outs["eta"]     = eta_op;
        outs["U_C"]     = uc_op;
        outs["ER"]      = er_op;
        outs["MFP"]     = mfp_op;
        outs["delta_h"] = delta_h;
        return outs;
    }
}


module q1dcfd.connectors.connect_channels ;

import q1dcfd.domains.domain: Domain ;
import q1dcfd.control_volumes: Node, FluidNode ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces: TransportInterface ;
import q1dcfd.config: Real ;
import q1dcfd.tables.extract_table_data: TableData ;

import std.format ;
import std.array: join ;
import std.exception: enforce ;
import std.conv: to ;

void connect_channel(Domain channel_l, Domain channel_r, 
    ref Interface[] interfaces, ref Node[] nodes)
/*
Defines a connection between two channels and appends I0, I1, I2
interfaces to the system

channel_l             channel_r

        I0         I1        I2
------------------- -------------------
|        |        | |        |        |
|        |        | |        |        |
|        |        | |        |        |
------------------- -------------------

Inputs:
    Channel channel_l: left channel, will connect to the rightmost nodes
    Channel channel_r: right channel, will connect to the leftmost nodes
*/
{
    // prepare to append 3 more interfaces
    interfaces.reserve(3) ;

    // construct I0 interface
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-3)].to!FluidNode,
        nodes[channel_l.node_global_id(-2)].to!FluidNode,
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode) ;

    // construct I1 interface
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-2)].to!FluidNode,
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode,
        nodes[channel_r.node_global_id(1)].to!FluidNode) ;

    // construct I2 interface
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode,
        nodes[channel_r.node_global_id(1)].to!FluidNode,
        nodes[channel_r.node_global_id(2)].to!FluidNode) ;
}

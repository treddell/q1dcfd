module q1dcfd.connectors.connect_incompressible_solver;

import q1dcfd.domains: Domain;
import q1dcfd.control_volumes: Node, ThermalNode, IncompressibleNode, PisoSolver;
import q1dcfd.config: Real;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.interfaces: SolverToChannel;
import q1dcfd.utils.errors: InvalidDefinition;

import std.conv: to;
import std.array;

/*
 * Connects an incompressible solver node to an incompressible channel that can
 * be used as part of other components, such as heat exchangers.
 * Assigns fluid property values from the solver to nodes in the incompressible
 * channel.
 */
void connect_incompressible_solver_to_channel(
        Domain solver,
        Domain channel,
        ref Interface[] interfaces,
        ref Node[] nodes)
{
    interfaces.reserve(1);

    interfaces ~= new SolverToChannel(
        nodes[solver.node_global_id(0)].to!PisoSolver,
        nodes[channel.node_index_l..channel.node_index_r].to!(IncompressibleNode[])
        );

    // Eventually could use a switch here (like in connect_inflow.d) to handle
    // different types of incompressible solvers and domains.
}


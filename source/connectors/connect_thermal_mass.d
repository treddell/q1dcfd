module q1dcfd.connectors.connect_thermal_mass ;

import q1dcfd.domains: Domain, Wall ;
import q1dcfd.control_volumes: Node, ThermalNode ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces: ConductiveInterface ;
import q1dcfd.config: Real ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.utils.errors: InvalidDefinition ;

import std.conv: to ;
import std.format ;

void connect_thermal_mass(Domain wall1, Domain wall2, string type, 
    ref Interface[] interfaces, ref Node[] nodes)
/** Connects lumped thermal masses together

if type = 'axial-conduction': Thermal masses are connected axially left to right
if type='transverse-conduction': Thermal masses are stacked transversely bottom 
    to top
**/
{
    // axial conduction
    if(type == "axial-conduction")
    {
        interfaces ~= new ConductiveInterface(
            nodes[wall1.node_global_id(-1)].to!ThermalNode,
            nodes[wall2.node_global_id(0)].to!ThermalNode) ;
    }
    else if(type == "transverse-conduction")
    {
        import std.exception: enforce ;

        enforce!InvalidDefinition(wall1.n_nodes == wall2.n_nodes, format!
            ("Inconsistent number of nodes between wall1 (%u) and wall2(%u)")
            (wall1.n_nodes, wall2.n_nodes)) ;

        foreach(i ; 0..wall1.n_nodes)
        {
            interfaces ~= new ConductiveInterface(
                nodes[wall1.node_global_id(i)].to!ThermalNode,
                nodes[wall2.node_global_id(i)].to!ThermalNode,
                true) ;
        }
    }
    else
    {
        throw new InvalidDefinition(format!("Wall-Wall connection type %s not "
            ~"understood")(type)) ;
    }
}
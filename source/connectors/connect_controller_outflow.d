module q1dcfd.connectors.connect_controller_outflow ;

import q1dcfd.domains: Domain ;
import q1dcfd.control_volumes: Node, OutflowNode ;
import q1dcfd.controllers: AbstractControlNode ;
import q1dcfd.config: Real ;
import q1dcfd.utils.errors: InvalidDefinition ;

import std.exception: enforce ;
import std.format ;
import std.conv: to ;

void connect_controller_outflow(
    Domain controller, 
    Domain outflow, 
    const uint port, 
    ref Node[] nodes)
/**
Connects a control output to the target function of an outflow boundary condition
**/
{
    // get a reference to the control node
    auto control_node = nodes[controller.node_global_id(0)].to!AbstractControlNode ;
    auto outflow_node = nodes[outflow.node_global_id(0)].to!OutflowNode ;

    enforce!InvalidDefinition(control_node.output_ports.length > port, 
        format!("Attempt to connect Outflow to controller port %u, maximum number"
        ~"of controller ports is %u")(port, control_node.output_ports.length)) ;

    // link the boundary conditions to the controller variable ports
    Real control_var_link(const Real t){return control_node.output_ports[port] ;}

    outflow_node.target_function =&control_var_link ;
    outflow.designate_boundary_linked("target") ;
}

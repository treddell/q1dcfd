module q1dcfd.connectors.connect_controller_inflow ;

import q1dcfd.domains: Domain, AbstractInflow ;
import q1dcfd.control_volumes: Node, FluidNode ;
import q1dcfd.controllers: AbstractControlNode ;
import q1dcfd.config: Real ;
import q1dcfd.utils.errors: InvalidDefinition ;

import std.exception: enforce ;
import std.format ;
import std.conv: to ;

void connect_controller_inflow(Domain controller, Domain inflow, uint port, 
    string type, ref Node[] nodes)
/** Connects a control output to an inflow boundary condition

**/
{
    // get a reference to the control node and boundary node
    auto control_node = nodes[controller.node_global_id(0)].to!AbstractControlNode ;
    auto inflow_node = nodes[inflow.node_global_id(0)].to!Node ;

    enforce!InvalidDefinition(control_node.output_ports.length > port, 
        format!("Attempt to connect Inflow to controller port %u, maximum number"
        ~"of controller ports is %u")(port, control_node.output_ports.length)) ;

    // link the boundary conditions to the controller variable ports
    Real control_var_link(const Real t){return control_node.output_ports[port] ;}
    Real control_der_link(const Real t){return 0.0 ;}

    if(type == "control-flow-in")
    {
        if(   inflow_node.type == "InflowFromStagnation"
           || inflow_node.type == "PumpFromStagnation")
        {
            import q1dcfd.control_volumes: InflowFromStagnation;
            auto in_fs_node = cast(InflowFromStagnation) inflow_node;
            in_fs_node.massflow_transient = &control_var_link;
            inflow.designate_boundary_linked("massflow");
        }
        else if(
                inflow_node.type == "IncompressibleInflow"
            ||  inflow_node.type == "IncompressiblePump")
        {
            import q1dcfd.control_volumes: IncompressibleInflow;
            auto incomp_in_node = cast(IncompressibleInflow) inflow_node;
            incomp_in_node.massflow_transient = &control_var_link;
            inflow.designate_boundary_linked("massflow");
        }
        else
        {
            import q1dcfd.control_volumes: InflowDV;
            auto in_dv_node = cast(InflowDV) inflow_node;
            in_dv_node.velocity_transient = &control_var_link ;
            in_dv_node.velocity_rate_transient = &control_der_link ;
            inflow.designate_boundary_linked("velocity") ;
        }
    }
    else if(type == "control-state-in")
    {
        if(   inflow_node.type == "InflowFromStagnation"
           || inflow_node.type == "PumpFromStagnation")
        {
            import q1dcfd.control_volumes: InflowFromStagnation;
            auto in_fs_node = cast(InflowFromStagnation) inflow_node;
            in_fs_node.temperature_transient = &control_var_link;
            inflow.designate_boundary_linked("state");
        }
        else if(
                inflow_node.type == "IncompressibleInflow"
            ||  inflow_node.type == "IncompressiblePump")
        {
            import q1dcfd.control_volumes: IncompressibleInflow;
            auto incompressible_node = cast(IncompressibleInflow) inflow_node;
            incompressible_node.temperature_transient = &control_var_link;
            inflow.designate_boundary_linked("state");
        }
        else
        {
            import q1dcfd.control_volumes: InflowDV;
            auto in_dv_node = cast(InflowDV) inflow_node;
            in_dv_node.density_transient = &control_var_link ;
            in_dv_node.density_rate_transient = &control_der_link ;
            inflow.designate_boundary_linked("state") ;
        }
    }
    else
    {
        throw new InvalidDefinition(
            format!("Controller connection type %s not understood")(type)) ;
    }
}

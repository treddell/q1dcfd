module q1dcfd.connectors.connect_controller_motor;

import q1dcfd.domains: Domain, MapCompressor;
import q1dcfd.control_volumes: Node, MapCompressorNode;
import q1dcfd.controllers: AbstractControlNode;
import q1dcfd.config: Real;
import q1dcfd.utils.errors: InvalidDefinition;

import std.exception: enforce;
import std.string;
import std.format;
import std.conv: to;

/* Connects a control output to a motor torque transient */
void connect_controller_motor(
    Domain controller,
    Domain compressor,
    const uint port,
    ref Node[] nodes)
{
    // Get a reference to the control node
    auto control_node = nodes[controller.node_global_id(0)].to!AbstractControlNode;
    auto compressor_node = nodes[compressor.node_global_id(0)].to!MapCompressorNode;

    enforce!InvalidDefinition(control_node.output_ports.length > port,
        format!("Attempt to connect Valve to controller port %u, maximum number "
        ~ "of controller ports is %u")(port, control_node.output_ports.length));

    // Link the boundary conditions to the controller variable ports
    Real control_var_link(const Real t){ return control_node.output_ports[port]; }

    compressor_node.motor_torque_derivative = &control_var_link;
    immutable bool specify_torque_directly = false; // controller computes torque derivative
    compressor_node.configure_motor_torque_transients(specify_torque_directly);
    compressor.designate_boundary_linked("motor_torque");
}


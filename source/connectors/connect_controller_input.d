module q1dcfd.connectors.connect_controller_input ;

import q1dcfd.domains: Domain ;
import q1dcfd.controllers: AbstractControlNode ;
import q1dcfd.control_volumes: Node, NavierStokesNode, InflowDV,
       InflowFromStagnation, IncompressibleInflow, OutflowNode, 
    OutflowToStagnation, IncompressibleOutflow, ThermalNode,
    MapTurbomachineNode;
import q1dcfd.utils.errors: InvalidConnection ;
import q1dcfd.config: Real, InputPort ;

import std.conv: to ;
import std.string ;
import std.format ;

void connect_controller_input(
    Domain input_domain, 
    Domain controller, 
    const uint connection_port, 
    const int local_node_id_inp,
    const string variable,
    ref Node[] nodes)
{
    auto control_node = nodes[controller.node_global_id(0)].to!AbstractControlNode ;

    // Look at the domain node type to deduce the input connection
    string signature = input_domain.node_type.split(".")[$ - 1] ;

    // If negative node index is specified, indexing from end of domain
    int local_node_id;
    if (local_node_id < 0) local_node_id = input_domain.n_nodes - local_node_id_inp;
    else                   local_node_id = local_node_id_inp;

    switch(signature)
    {
        case "NavierStokesNode":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!NavierStokesNode ;
            
            // Construct controller input linkage
            InputPort control_input_ns = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_ns ;
            break ;
        }

        case "InflowDV":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!InflowDV ;
            
            // Construct controller input linkage
            InputPort control_input_inflow_dv = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_inflow_dv ;
            break ;
        }

        case "PumpFromStagnation": goto case "InflowFromStagnation";
        case "InflowFromStagnation":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!InflowFromStagnation ;
            
            // Construct controller input linkage
            InputPort control_input_inflow_fs = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_inflow_fs ;
            break ;
        }

        case "IncompressiblePump": goto case "IncompressibleInflow";
        case "IncompressibleInflow":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!IncompressibleInflow ;
            
            // Construct controller input linkage
            InputPort control_input_inflow_incomp = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_inflow_incomp ;
            break ;
        }

        case "OutflowNode":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!OutflowNode ;
            
            // Construct controller input linkage
            InputPort control_input_outflow = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_outflow ;
            break ;
        }

        case "OutflowToStagnation":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!OutflowToStagnation ;
            
            // Construct controller input linkage
            InputPort control_input_outflow = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_outflow ;
            break ;
        }

        case "IncompressibleOutflow":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!IncompressibleOutflow ;
            
            // Construct controller input linkage
            InputPort control_input_outflow = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_outflow ;
            break ;
        }

        case "FarFieldNode": goto case "OutflowNode" ;

        case "TargetMassNode": goto case "OutflowNode" ;

        case "ThermalNode":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!ThermalNode ;
            
            // Construct controller input linkage
            InputPort control_input_thermal = () {return input_node.map(variable) ;};
            control_node.input_ports[connection_port] = control_input_thermal;
            break ;
        }

        case "MapCompressorNode": goto case "MapTurbomachineNode";
        case "MapTurbineNode":    goto case "MapTurbomachineNode";
        case "MapTurbomachineNode":
        {
            auto input_node = nodes[input_domain.node_global_id(local_node_id)]
                .to!MapTurbomachineNode;
            InputPort control_input_mt = () { return input_node.map(variable); };
            control_node.input_ports[connection_port] = control_input_mt;
            break;
        }

        default:
        {
            throw new InvalidConnection(
                format!("Cannot connect %s.node[%u] of type %s to controller %s")
                (input_domain.name, local_node_id_inp, signature, controller.name)) ;
        }
    }
}

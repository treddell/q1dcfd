module q1dcfd.connectors.connect_inflow ;

import q1dcfd.domains: Domain, AbstractInflow ;
import q1dcfd.control_volumes: Node, FluidNode ;
import q1dcfd.config: Real ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces: TransportInterface, InflowInterface, 
    InflowExtrapolation, InteriorFlowConditions;
import q1dcfd.simulation: Simulation ;
import q1dcfd.utils.errors: InvalidDefinition ;

import std.conv: to ;
import std.exception: enforce ;
import std.format ;
import std.array ;

/*
 * Defines a connection between an inflow boundary condition and a channel
 * domain. Assigns pointers from the first few interior nodes to the boundary
 * domain so that extrapolations may be performed. Constructs the I0 and I1
 * interfaces of the interior domain and assigns to the system
 * 
 * inflow   interior interior interior
 * state    1        2       3
 * -----------------------------------
 * |        .        .       .
 * |        .        .       .
 * |        .        .       .
 * -----------------------------------
 * I0      I1
 */
void connect_inflow(Domain boundary, Domain interior, ref Interface[] interfaces,
        ref Node[] nodes)
{
    enforce!InvalidDefinition(boundary.to!AbstractInflow.interior is null,
        format!("Inflow %s connects to multiple interiors %s and %s")
        (boundary.name, boundary.to!AbstractInflow.interior.name, interior.name));

    // prepare to append 3 more interfaces
    interfaces.reserve(3) ;

    // construct I0 interface
    interfaces ~= new TransportInterface(
        nodes[boundary.node_global_id(0)].to!FluidNode,
        nodes[interior.node_global_id(0)].to!FluidNode,
        nodes[interior.node_global_id(1)].to!FluidNode,
        nodes[interior.node_global_id(2)].to!FluidNode) ;


    // construct I1 interface
    interfaces ~= new InflowInterface(
        nodes[boundary.node_global_id(0)].to!FluidNode,
        nodes[interior.node_global_id(0)].to!FluidNode,
        nodes[interior.node_global_id(1)].to!FluidNode) ;

    // Construct interfaces specific to boundary model 
    auto boundary_node_type = boundary.node_type().split(".")[$-1];
    switch(boundary_node_type)
    {
        case "InflowDV":
        {
            // define the outgoing characteristics extrapolation
            interfaces ~= new InflowExtrapolation(
                nodes[boundary.node_global_id(0)].to!FluidNode,
                nodes[interior.node_index_l..interior.node_index_r]
                    .to!(FluidNode[]));
            break;
        }
        case "PumpFromStagnation": goto case "InflowFromStagnation";
        case "InflowFromStagnation":
        {
            // Reference to the values in the first cell
            interfaces ~= new InteriorFlowConditions(
                nodes[boundary.node_global_id(0)].to!FluidNode,
                nodes[interior.node_index_l..interior.node_index_r]
                    .to!(FluidNode[]));
            break;
        }
        default:
        {
            import std.format;
            throw new Exception(
                "Unrecognized node model %s".format(boundary_node_type));
        }
    }

    // give the boundary a pointer to the interior domain
    boundary.to!AbstractInflow.interior = interior ;
}


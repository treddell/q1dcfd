module q1dcfd.connectors.connect_incompressible_boundaries;

import q1dcfd.domains: Domain, AbstractInflow;
import q1dcfd.control_volumes: Node, IncompressibleBoundary, PisoSolver;
import q1dcfd.config: Real;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.utils.errors: InvalidDefinition;

import std.conv: to;
import std.format;
import std.array;

void connect_incompressible_inflow(
        Domain boundary,
        Domain solver,
        ref Interface[] interfaces,
        ref Node[] nodes)
{
    interfaces.reserve(1);

    auto boundary_node_type = boundary.node_type().split(".")[$-1];
    switch(boundary_node_type)
    {
        case "IncompressiblePump":
            goto case "IncompressibleInflow";
        case "IncompressibleInflow":
        {
            import q1dcfd.interfaces: IncompressibleInflowInterface;

            // Reference to the values in the first cell
            interfaces ~= new IncompressibleInflowInterface(
                nodes[boundary.node_global_id(0)].to!IncompressibleBoundary,
                nodes[solver.node_global_id(0)].to!PisoSolver);
            break;
        }
        default:
        {
            import std.format;
            throw new Exception(
                "Unrecognized node model %s".format(boundary_node_type));
        }
    }
}

void connect_incompressible_outflow(
        Domain solver,
        Domain boundary,
        ref Interface[] interfaces,
        ref Node[] nodes)
{
    interfaces.reserve(1);

    auto boundary_node_type = boundary.node_type().split(".")[$-1];
    switch(boundary_node_type)
    {
        case "IncompressibleOutflow":
        {
            import q1dcfd.interfaces: IncompressibleOutflowInterface;

            // Reference to the values in the first cell
            interfaces ~= new IncompressibleOutflowInterface(
                nodes[boundary.node_global_id(0)].to!IncompressibleBoundary,
                nodes[solver.node_global_id(0)].to!PisoSolver);
            break;
        }
        default:
        {
            import std.format;
            throw new Exception(
                "Unrecognized node model %s".format(boundary_node_type));
        }
    }
}


module q1dcfd.connectors.connect_valve_channel ;

import q1dcfd.domains: Domain, AbstractExternalValve ;
import q1dcfd.control_volumes: Node, FluidNode ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces: TransportInterface,
    InflowInterface, OutflowInterface ;
import q1dcfd.config: Real ;

import std.conv: to ;

void connect_valve_channel_right(
    Domain valve,
    Domain channel,
    ref Interface[] interfaces,
    ref Node[] nodes)
{
    auto vlv = valve.to!AbstractExternalValve ;
    vlv.right_channel = channel ;

    if(vlv.right_channel !is null && vlv.left_channel !is null)
    {
        vlv.construct_fluxes(interfaces, nodes) ;
    }
}

void connect_valve_channel_left(
    Domain channel,
    Domain valve,    
    ref Interface[] interfaces,
    ref Node[] nodes)
{
    auto vlv = valve.to!AbstractExternalValve ;
    vlv.left_channel = channel ;

    if(vlv.right_channel !is null && vlv.left_channel !is null)
    {
        vlv.construct_fluxes(interfaces, nodes) ;
    }
}

private void construct_fluxes(
    AbstractExternalValve valve,
    ref Interface[] interfaces,
    ref Node[] nodes)
/**
   Construct the fluxes across the valve

   left_channel            valve      right_channel
   
           I0         I1       I2          I3
   ------------------- --------- -------------------
   |        |        | |       | |        |        |
   |        |        | |       | |        |        |
   |        |        | |       | |        |        |
   ------------------- --------- -------------------

*/
{
    interfaces.reserve(4) ;

    // construct I0 interface
    interfaces ~= new TransportInterface(
        nodes[valve.left_channel.node_global_id(-3)].to!FluidNode,
        nodes[valve.left_channel.node_global_id(-2)].to!FluidNode,
        nodes[valve.left_channel.node_global_id(-1)].to!FluidNode,
        nodes[valve.node_global_id(0)].to!FluidNode) ;

    // construct I1 interface
    interfaces ~= new TransportInterface(
        nodes[valve.left_channel.node_global_id(-2)].to!FluidNode,
        nodes[valve.left_channel.node_global_id(-1)].to!FluidNode,
        nodes[valve.node_global_id(0)].to!FluidNode,
        nodes[valve.right_channel.node_global_id(0)].to!FluidNode) ;

    // construct I2 interface
    interfaces ~= new TransportInterface(
         nodes[valve.left_channel.node_global_id(-1)].to!FluidNode,
         nodes[valve.node_global_id(0)].to!FluidNode,
         nodes[valve.right_channel.node_global_id(0)].to!FluidNode,
         nodes[valve.right_channel.node_global_id(1)].to!FluidNode) ;
 
    // construct I3 interface
    interfaces ~= new TransportInterface(
        nodes[valve.node_global_id(0)].to!FluidNode,
        nodes[valve.right_channel.node_global_id(0)].to!FluidNode,
        nodes[valve.right_channel.node_global_id(1)].to!FluidNode,
        nodes[valve.right_channel.node_global_id(2)].to!FluidNode) ;
}

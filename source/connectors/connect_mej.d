module q1dcfd.connectors.connect_mej ;

import q1dcfd.domains: Domain, MomentumEnergyJump, AbstractInflow, 
    AbstractOutflow ;
import q1dcfd.control_volumes: Node, FluidNode, MomentumEnergyJumpNode, 
    InflowDV, OutflowNode ;
import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces.jump_correction: JumpCorrection ;
import q1dcfd.simulation: Simulation ;
import q1dcfd.config: Real ;

import std.conv: to ;
import std.exception: enforce ;
import std.format ;

void connect_mej_left(Domain domain1, Domain domain2, ref Interface[] interfaces,
    ref Node[] nodes)
/** Performs an inflow connection to a momentum energy jump      

**/
{
    auto jump_domain = domain2.to!MomentumEnergyJump ;
    
    enforce!InvalidDefinition(jump_domain.upstream is null, 
        format!("Multiple inflow connections to %s, %s and %s")
        (jump_domain.name, jump_domain.upstream, domain1.name)) ; 

    // give the jump domain a reference to the downstream domain
    jump_domain.upstream = domain1 ;

    enforce!InvalidDefinition(
        jump_domain.upstream.to!AbstractOutflow.interior !is null, 
        format!("Must define connection between %s and interior before defining"
            ~"connection between %s and %s")
            (domain1.name, domain1.name, domain2.name)) ; 

    if(jump_domain.downstream !is null)
    {
        Simulation.add_correction(new JumpCorrection(
            nodes[jump_domain.upstream.to!AbstractOutflow.interior.node_global_id(-1)]
                .to!FluidNode,
            nodes[jump_domain.upstream.node_global_id(0)].to!FluidNode,
            nodes[jump_domain.downstream.node_global_id(0)].to!FluidNode,
            nodes[jump_domain.downstream.to!AbstractInflow.interior.node_global_id(0)]
                .to!FluidNode));
    }

    // give the discontinuity node access to the upstream node
    auto discontinuity = nodes[jump_domain.node_global_id(0)].to!MomentumEnergyJumpNode ;
    auto upstream = nodes[domain1.node_global_id(0)].to!OutflowNode ;
    discontinuity.upstream = upstream ;
}

void connect_mej_right(Domain domain1, Domain domain2, ref Interface[] interfaces,
        ref Node[] nodes)
/**
Performs an outflow connection from a momentum energy jump
**/
{
    auto jump_domain = domain1.to!MomentumEnergyJump ;

    enforce!InvalidDefinition(jump_domain.downstream is null, 
        format!("Multiple outflow connections to %s, %s and %s")
        (jump_domain.name, jump_domain.downstream, domain2.name)) ; 

    // give the jump domain a reference to the downstream domain and construct
    // the mass flow rate correction
    jump_domain.downstream = domain2 ;
    if(jump_domain.upstream !is null)
    {
        Simulation.add_correction(new JumpCorrection(
            nodes[jump_domain.upstream.to!AbstractOutflow.interior.node_global_id(-1)]
                .to!FluidNode,
            nodes[jump_domain.upstream.node_global_id(0)].to!FluidNode,
            nodes[jump_domain.downstream.node_global_id(0)].to!FluidNode,
            nodes[jump_domain.downstream.to!AbstractInflow.interior.node_global_id(0)]
                .to!FluidNode));
    }

    enforce!InvalidDefinition(
        jump_domain.downstream.to!AbstractInflow.interior !is null, 
        format!("Must define connection between %s and interior before defining"
            ~"connection between %s and %s")
            (domain2.name, domain2.name, domain1.name)) ; 

    // give the discontinuity node access to the downstream node
    auto discontinuity = nodes[domain1.node_global_id(0)].to!MomentumEnergyJumpNode ;
    auto downstream = nodes[domain2.node_global_id(0)].to!InflowDV ;
    discontinuity.downstream = downstream ;

    // construct boundary functions for the downstream state
    Real density_transient(const Real t) { return discontinuity.rho_extrapolated ;}
    Real velocity_transient(const Real t) { return discontinuity.v_extrapolated ;}
    Real density_rate_transient(const Real t) { return discontinuity.drhodt_extrapolated ;}
    Real velocity_rate_transient(const Real t) { return discontinuity.dvdt_extrapolated ;}

    downstream.density_transient = &density_transient ;
    downstream.density_rate_transient = &density_rate_transient ;
    downstream.velocity_transient = &velocity_transient ;
    downstream.velocity_rate_transient = &velocity_rate_transient ;

    // designate initial parameters to avoid undefined initial derivatives
    discontinuity.drhodt_extrapolated = 0.0 ;
    discontinuity.dvdt_extrapolated = 0.0 ;
    downstream.rho = 1.0 ;
    downstream.v = 1.0 ;

    // flag connection complete
    auto downstream_domain = domain2.to!AbstractInflow ;
    downstream_domain.state_transient_is_linked = true ;
    downstream_domain.velocity_transient_is_linked = true ;
}

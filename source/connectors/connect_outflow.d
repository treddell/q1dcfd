module q1dcfd.connectors.connect_outflow ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.domains: Domain, AbstractOutflow ;
import q1dcfd.control_volumes: Node, FluidNode, OutflowNode ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces: OutflowInterface, OutflowExtrapolation, 
    TransportInterface, OutflowInteriorFlowConditions;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.config: Real ;

import std.format ;
import std.array: join, split;
import std.exception: enforce ;
import std.conv: to ;

//void connect_outflow(Channel interior, Outflow boundary)
void connect_outflow(Domain interior, Domain boundary, ref Interface[] interfaces,
        ref Node[] nodes)
/*
Defines a connection between an outflow boundary condition and a channel
domain. Assigns pointers from the last few interior nodes to the boundary
domain so that extrapolations may be performed. Constructs the I0 and I1
interfaces of the interior domain and assigns to the system

 interior interior interior outflow
 -3       -2       -1       state
------------------------------------
.        .        .       |
.        .        .       |
.        .        .       |
------------------------------------
 I1      I0

Domain boundary : the outflow boundary domain
Domain interior : the interior domain
*/
{
    enforce!InvalidDefinition(boundary.to!AbstractOutflow.interior is null,
        format!("Outflow %s connects to multiple interiors %s and %s")
        (boundary.name, boundary.to!AbstractOutflow.interior.name, interior.name));
    
    // prepare to append 3 more interfaces
    interfaces.reserve(3) ;

    // construct I0 interface
    interfaces ~= new TransportInterface(
        nodes[interior.node_global_id(-3)].to!FluidNode,
        nodes[interior.node_global_id(-2)].to!FluidNode,
        nodes[interior.node_global_id(-1)].to!FluidNode,
        nodes[boundary.node_global_id(0)].to!FluidNode) ;

    // construct I1 interface
    interfaces ~= new OutflowInterface(
        nodes[interior.node_global_id(-2)].to!FluidNode,
        nodes[interior.node_global_id(-1)].to!FluidNode,
        nodes[boundary.node_global_id(0)].to!FluidNode) ;

    // construct interfaces specific to boundary model
    auto boundary_node_type = boundary.node_type().split(".")[$-1];
    switch(boundary_node_type)
    {
        case "OutflowToStagnation":
        {
            // reference to the values in the first cell
            interfaces ~= new OutflowInteriorFlowConditions(
                nodes[boundary.node_global_id(0)].to!FluidNode,
                nodes[interior.node_index_l..interior.node_index_r]
                    .to!(FluidNode[]));
            break;
        }
        default:
        {
            // For all other node types, use outgoing characteristics extrapolation
            interfaces ~= new OutflowExtrapolation(
                nodes[interior.node_index_l..interior.node_index_r].to!(FluidNode[]),
                nodes[boundary.node_global_id(0)].to!OutflowNode) ;
        }
    }
    // give the boundary a pointer to the interior domain
    boundary.to!AbstractOutflow.interior = interior ;
}

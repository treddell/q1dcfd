module q1dcfd.connectors.connect_wall_channel ;

import q1dcfd.simulation: Simulation ;
import q1dcfd.domains: Domain, AxialWall, BoundaryWall ;
import q1dcfd.config: Real ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.interfaces.convective_interface: ConvectiveInterface ;
import q1dcfd.control_volumes: 
    Node, ThermalNode, FluidNode ;
import q1dcfd.thermal_face: ThermalFace ;

import std.format ;
import std.exception: enforce ;
import std.conv: to ;

void connect_wall(Domain wall,
                  Domain channel,
                  bool reversed, 
                  ref Interface[] interfaces,
                  ref Node[] nodes,
                  const string name)
/*
 * Defines a connection between a channel domain and its interfacing axial wall
 * domain. Constructs the heat transfer interfaces and assigns them to the
 * system
*/
{
    // reserve space for the new interfaces
    interfaces.reserve(channel.n_nodes) ;

    // Check the wall type and work out what kind of interface to construct
    // make sure the wall and channel have equal nodes
    if(wall.type == "AxialWall")
    {
        enforce(wall.n_nodes == channel.n_nodes, 
            format!("Cannot currently support mismatched node "
            ~" numbers for wall %s and channel %s")(wall.name, channel.name)) ;
    }

    // generate the multinode heat transfer interface tracker
    auto face = new ThermalFace(name, wall.name, channel.name, true) ;
    Simulation.add_thermal_face(face) ;
    
    // Build all the interfaces and add to the system
    int m ; // thermal node true index
    foreach(immutable i ; 0..channel.n_nodes)
    {
        if(wall.type == "BoundaryWall") m = 0 ;
        else if(reversed) m = channel.n_nodes.to!int - i - 1 ;
        else m = i ;

        Simulation.add_convective_interface(new ConvectiveInterface(
            nodes[wall.node_global_id(m)].to!ThermalNode,
            nodes[channel.node_global_id(i)].to!FluidNode,
            face, name)) ;
    }
}

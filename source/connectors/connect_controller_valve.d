module q1dcfd.connectors.connect_controller_valve ;

import q1dcfd.domains: Domain, ExternalValve ;
import q1dcfd.control_volumes: Node, ExternalValveNode ;
import q1dcfd.controllers: AbstractControlNode ;
import q1dcfd.config: Real ;
import q1dcfd.utils.errors: InvalidDefinition ;

import std.exception: enforce ;
import std.string ;
import std.format ;
import std.conv: to ;

void connect_controller_valve(
    Domain controller, 
    Domain valve, 
    const uint port, 
    ref Node[] nodes)
/**
Connects a control output to an external valve mass flow rate transient
**/
{
    // get a reference to the control node
    auto control_node = nodes[controller.node_global_id(0)]
        .to!AbstractControlNode ;
    auto valve_node = nodes[valve.node_global_id(0)]
        .to!ExternalValveNode ;

    enforce!InvalidDefinition(control_node.output_ports.length > port, 
        format!("Attempt to connect Valve to controller port %u, maximum number"
        ~"of controller ports is %u")(port, control_node.output_ports.length)) ;

    // link the boundary conditions to the controller variable ports
    Real control_var_link(const Real t){return control_node.output_ports[port] ;}

    valve_node.mass_flow_rate =&control_var_link ;
    valve.designate_boundary_linked("mass_flow") ;
}

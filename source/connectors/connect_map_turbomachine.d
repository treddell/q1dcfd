module q1dcfd.connectors.connect_map_turbomachine;

import q1dcfd.domains.domain: Domain;
import q1dcfd.control_volumes: Node, FluidNode, MapTurbomachineNode;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.interfaces: MapTurbomachineInletInterface, MapTurbomachineOutletInterface;
import q1dcfd.config: Real;

import std.conv: to;

/*
 * Connects the inlet side of a MapTurbomachineNode to a Plenum object.
 * All of the remaining interfaces for the plenum are constructed when the
 * plenum is connected to a chanel, so only one interface is constructed here.
 * Unlike regular compressible interfaces that use 4-cell stencils, this
 * interface just needs the end node from the plenum and the turbomachine node
 * (because we don't want to look over the turbomachine discontinuity).
 * See map_turbomachine_interfaces.d for details on the interfaces.
 */
void connect_map_turbomachine_inlet(
    Domain plenum, Domain map_turbomachine_node, 
    ref Interface[] interfaces, ref Node[] nodes)
{
    interfaces.reserve(1);

    interfaces ~= new MapTurbomachineInletInterface(
        nodes[plenum.node_global_id(-1)].to!FluidNode,
        nodes[map_turbomachine_node.node_global_id(0)].to!MapTurbomachineNode);
}

/*
 * As above but for outlet.
 */
void connect_map_turbomachine_outlet(
    Domain map_turbomachine_node, Domain plenum,
    ref Interface[] interfaces, ref Node[] nodes)
{
    interfaces.reserve(1);

    // map_turbomachine_node has just one cell so index -1 equivalent to index 0
    interfaces ~= new MapTurbomachineOutletInterface(
        nodes[map_turbomachine_node.node_global_id(-1)].to!MapTurbomachineNode,
        nodes[plenum.node_global_id(0)].to!FluidNode);
}


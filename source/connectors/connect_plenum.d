module q1dcfd.connectors.connect_plenum;

import q1dcfd.domains.domain: Domain;
import q1dcfd.control_volumes: Node, FluidNode;
import q1dcfd.interfaces.root_interface: Interface;
import q1dcfd.interfaces: TransportInterface;
import q1dcfd.config: Real;
import q1dcfd.tables.extract_table_data: TableData;

import std.format;
import std.array: join;
import std.exception: enforce;
import std.conv: to;

/*
 * Basically a copy of connect_channel with a hack to prevent the stencil from
 * crossing over the turbine discontinuity.
 */
void connect_plenum_inlet(
    Domain channel_l, Domain plenum, 
    ref Interface[] interfaces, ref Node[] nodes)
{
    // prepare to append 3 more interfaces
    interfaces.reserve(3);

    // construct I0 interface
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-3)].to!FluidNode,
        nodes[channel_l.node_global_id(-2)].to!FluidNode,
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[plenum.node_global_id(0)].to!FluidNode);

    // construct I1 interface
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-2)].to!FluidNode,
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[plenum.node_global_id(0)].to!FluidNode,
        nodes[plenum.node_global_id(1)].to!FluidNode);

    // construct I2 interface
    // Cell plenum.node_global_id(2) would be past turbomachine
    // discontinuity, so we just assume conditions are the same as in cell
    // plenum.node_global_id(1)
    interfaces ~= new TransportInterface(
        nodes[channel_l.node_global_id(-1)].to!FluidNode,
        nodes[plenum.node_global_id(0)].to!FluidNode,
        nodes[plenum.node_global_id(1)].to!FluidNode,
        nodes[plenum.node_global_id(1)].to!FluidNode); // extrapolated cell
}

/*
 * As above.
 */
void connect_plenum_outlet(
    Domain plenum, Domain channel_r, 
    ref Interface[] interfaces, ref Node[] nodes)
{
    // prepare to append 3 more interfaces
    interfaces.reserve(3);

    // construct I0 interface
    // Cell channel_r.node_global_id(-3) would be past turbomachine
    // discontinuity, so we just assume conditions are the same as in cell
    // channel_r.node_global_id(-2)
    interfaces ~= new TransportInterface(
        nodes[plenum.node_global_id(-2)].to!FluidNode, // extrapolated cell
        nodes[plenum.node_global_id(-2)].to!FluidNode,
        nodes[plenum.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode);

    // construct I1 interface
    interfaces ~= new TransportInterface(
        nodes[plenum.node_global_id(-2)].to!FluidNode,
        nodes[plenum.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode,
        nodes[channel_r.node_global_id(1)].to!FluidNode);

    // construct I2 interface
    interfaces ~= new TransportInterface(
        nodes[plenum.node_global_id(-1)].to!FluidNode,
        nodes[channel_r.node_global_id(0)].to!FluidNode,
        nodes[channel_r.node_global_id(1)].to!FluidNode,
        nodes[channel_r.node_global_id(2)].to!FluidNode);
}




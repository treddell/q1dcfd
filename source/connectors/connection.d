module q1dcfd.connectors.connection ;

import q1dcfd.domains: Domain, FluidDomain ;
import q1dcfd.utils.errors: InvalidDefinition ;
import q1dcfd.tables.extract_table_data: TableData ;
import q1dcfd.stream: Stream ;

import std.conv: to ;
import std.algorithm.searching: canFind ;
import std.algorithm.iteration ;
import std.exception: enforce ;
import std.format ;
import std.array: join, array ;
import std.string ;

class DuplicateConnection: Exception 
{
    // Custom Exception Handle for a multiply defined connection
    this(string msg, string file = __FILE__, size_t line = __LINE__) 
    {
        super(msg, file, line);
    }
    
    override string toString()
    {
        return format!("DuplicateConnection! line %u in %s: %s")
            (line, file, message) ;
    }
}

/**
 * Temporary object, used for construction
 *
 *    A connection block, split into its arguments and flowlines. Once all 
 *    connection blocks are parsed, they are given to build_connections, to 
 *    construct the global flow network
 */
struct Connection 
{
    string[2][] directed_pairs ;
    string type ;
    string[] domains ;
    bool alternate ;
    string dataset_key ; // tabular data set (for fluid connections)
    const string name ; // connection name
    string[] saved_fields ; // properties to save to file if applicable

    // controller output variable, -1 = null connection
    int controller_port ;

    // controller input variable, -1 = null connection
    string input_variable ;

    // negative indices permitted (-1 = last cell, etc)
    int node_id ;

    string[string] args ;
    string[][] flowlines ;

    this(string[string] args, string[][] flowlines, const string name)
    {
        this.args = args ;
        this.flowlines = flowlines ;
        this.name = name ;

        // get the domain set used by the connection block
        domains = flowlines.join.uniq.array ;

        // listing of left -> right connection pairs. Ensures there are no
        // duplicates or else the definition is ill posed 
        foreach(string[] stream ; flowlines)
        {
            foreach(i, string node ; stream[0..stream.length - 1])
            {
                string[2] this_pair = [node, stream[i + 1]] ;

                enforce!DuplicateConnection(!directed_pairs.canFind(this_pair), 
                    format!("Connection %s->%s redefined in decleration %s")
                    (this_pair[0], this_pair[1], stream.join("->"))) ;

                directed_pairs ~= this_pair ;
            }
        }

        // extract and build arguments
        import q1dcfd.utils.dictionary_tools: retrieve, required, defaults ;
        type = args.retrieve("type").required!string ;
        controller_port = args.retrieve("port").defaults(-1) ;
        node_id = args.retrieve("node_id").defaults(0) ;
        input_variable = args.retrieve("input_variable").defaults("") ;
        saved_fields = args.retrieve("save").defaults("").value.split(",") ;

        if(
                type == "control-state-in" 
            ||  type == "control-flow-in" 
            ||  type == "control-target-variable"
            ||  type == "control-inventory"
            ||  type == "control-motor-torque"
            ||  type == "controller-input")
        {
            enforce!InvalidDefinition(controller_port > -1, 
                format!("Parameter 'port' not defined in %s connection %s")
                (type, flowlines.to!string)) ;

            if(type == "controller-input" && input_variable == "")
            {
                throw new InvalidDefinition(
                    format!("Parameter 'input_variable' not defined in %s "
                    ~"connection %s")(type, flowlines.to!string)) ;
            }
        }
        else
        {
            enforce!InvalidDefinition(controller_port < 0,
                format!("Parameter 'port=%u' in connection "
                ~"%s invalid. Only control connections use this argument")
                (controller_port, flowlines.to!string)) ;
        }

        if(type == "stream")
        {
            dataset_key = args.retrieve("data").required!string ;
        }
    }

    void set_tabular_data(
        ref TableData[string] fluid_conservative_data,
        ref TableData[string] fluid_isentropic_data, 
        ref TableData[string] fluid_primitive_data,
        ref Stream[string] streams,
        ref Domain[string] domains_array)
    {
        // only valid for stream type connections
        if(type != "stream") {return ;}

        // check if the data set has already been loaded into memory, if not, do so
        if(dataset_key !in fluid_conservative_data)
        {
            import std.path ;
            import std.file: exists ;

            import q1dcfd.lconfig: ROOT_DIRECTORY ;
            import q1dcfd.utils.errors: TableNotFound ;

            string conservative_dir = [ROOT_DIRECTORY, "tabular_data",
                dataset_key, "density_energy", ""].join(dirSeparator) ;

            string isentropic_dir = [ROOT_DIRECTORY, "tabular_data",
                dataset_key, "pressure_entropy", ""].join(dirSeparator);

            string primitive_dir = [ROOT_DIRECTORY, "tabular_data",
                dataset_key, "pressure_temperature", ""].join(dirSeparator);

            enforce!TableNotFound(conservative_dir,
                                  "Did not find a density-energy table in "
                                  ~"tabular_data with key %f, have you "
                                  ~"generated this table with the tablegen "
                                  ~"command?".format(dataset_key)) ;

            enforce!TableNotFound(isentropic_dir,
                                  "Did not find a pressure-entropy table in "
                                  ~"tabular_data with key %f, have you "
                                  ~"generated this table with the tablegen "
                                  ~"command?".format(dataset_key)) ;

            enforce!TableNotFound(primitive_dir,
                                  "Did not find a pressure-temperature table in "
                                  ~"tabular_data with key %f, have you "
                                  ~"generated this table with the tablegen "
                                  ~"command?".format(dataset_key)) ;
                    
            fluid_conservative_data[dataset_key] = new TableData(conservative_dir);
            fluid_isentropic_data[dataset_key] = new TableData(isentropic_dir);
            fluid_primitive_data[dataset_key] = new TableData(primitive_dir);
        }

        streams[name] = new Stream(
            name,
            domains,
            fluid_conservative_data[dataset_key],
            fluid_primitive_data[dataset_key],
            fluid_isentropic_data[dataset_key],
            saved_fields) ;

        // give the dataset to each domain
        domains.each!(
            domain => domains_array[domain]
            .to!FluidDomain
            .set_stream(streams[name])) ;
    }

    void check_domains_exist(ref Domain[string] global_domains)
    {
        foreach(domain ; domains)
        {
            enforce(domain in global_domains, format!("Domain %s is used in a "
                ~ "connection but never defined")(domain)) ;
        }
    }
}

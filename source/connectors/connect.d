module q1dcfd.connectors.connect ;

import q1dcfd.domains.domain: Domain ;
import q1dcfd.control_volumes.node: Node ;
import q1dcfd.interfaces.root_interface: Interface ;
import q1dcfd.connectors.connection: Connection ;
import q1dcfd.connectors.connect_channels: connect_channel ;
import q1dcfd.connectors.connect_inflow: connect_inflow ;
import q1dcfd.connectors.connect_outflow: connect_outflow ;
import q1dcfd.connectors.connect_wall_channel: connect_wall ;
import q1dcfd.connectors.connect_thermal_mass: connect_thermal_mass ;
import q1dcfd.connectors.connect_mej: connect_mej_right, connect_mej_left ;
import q1dcfd.connectors.connect_controller_inflow: connect_controller_inflow ;
import q1dcfd.connectors.connect_controller_outflow: connect_controller_outflow;
import q1dcfd.connectors.connect_controller_input: connect_controller_input ;
import q1dcfd.connectors.connect_controller_valve: connect_controller_valve ;
import q1dcfd.connectors.connect_controller_motor: connect_controller_motor ;
import q1dcfd.connectors.connect_valve_channel: connect_valve_channel_left,
        connect_valve_channel_right ;
import q1dcfd.connectors.connect_interface_turbomachine:
        connect_interface_turbomachine_inlet, connect_interface_turbomachine_outlet;
import q1dcfd.connectors.connect_plenum:
        connect_plenum_inlet, connect_plenum_outlet;
import q1dcfd.connectors.connect_map_turbomachine:
        connect_map_turbomachine_inlet, connect_map_turbomachine_outlet;
import q1dcfd.connectors.connect_wall_incompressible_channel:
        connect_wall_incompressible_channel;
import q1dcfd.connectors.connect_incompressible_solver:
        connect_incompressible_solver_to_channel;
import q1dcfd.connectors.connect_incompressible_boundaries:
        connect_incompressible_inflow, connect_incompressible_outflow;
import q1dcfd.utils.errors: InvalidConnection ;

import std.format ;

void connect(Domain domain1, Domain domain2, ref Connection connection,
    ref Interface[] interfaces, ref Node[] nodes)
/** General connection mapper

Connects domain1 -> domain2 using the respective connection model

Inputs:
    Domain domain1, domains2 : types and references of the two domains to connect
    FlowNetwork network: reference to the FlowNetwork object

'network.alternate' will flag the alternative connection model (if one exists)
E.g connect walls axially or transversly
'port' used for control systems connection
**/
{
    string signature ;
    if(domain2.type == "Controller")
    {
        connect_controller_input(domain1, domain2, connection.controller_port,
                connection.node_id, connection.input_variable, nodes) ;
        return ;
    }
    else
    {
        signature = domain1.type ~ ":" ~ domain2.type ;
    }

    switch(signature)
    {
        case "Inflow:Channel":
            connect_inflow(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Channel:Channel":
            connect_channel(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Channel:Outflow":
            connect_outflow(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Channel:AxialWall":
            connect_wall(domain2, domain1, false, interfaces, nodes,
                connection.name) ;
            break ;

        case "Channel:BoundaryWall":
            goto case "Channel:AxialWall" ;

        case "AxialWall:Channel":
            connect_wall(domain1, domain2, true, interfaces, nodes,
                connection.name) ;
            break ;

        case "BoundaryWall:Channel":
            goto case "AxialWall:Channel" ;

        case "AxialWall:AxialWall":
            connect_thermal_mass(domain1, domain2, connection.type, interfaces,
                nodes);
            break ;

        case "AxialWall:BoundaryWall":
            goto case "AxialWall:AxialWall" ;

        case "BoundaryWall:AxialWall":
            goto case "AxialWall:AxialWall" ;

        case "Outflow:MomentumEnergyJump":
            connect_mej_left(domain1, domain2, interfaces, nodes) ;
            break ;

        case "MomentumEnergyJump:Inflow":
            connect_mej_right(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Controller:Inflow":
            connect_controller_inflow(domain1, domain2, connection.controller_port,
                connection.type, nodes) ;
            break ;

        case "Controller:Outflow":
            connect_controller_outflow(domain1, domain2, connection.controller_port,
                nodes) ;
            break ;

        case "Controller:MapCompressor":
            connect_controller_motor(domain1, domain2, connection.controller_port,
                nodes) ;
            break ;

        case "Channel:ExternalValve":
            connect_valve_channel_left(domain1, domain2, interfaces, nodes) ;
            break ;

        case "ExternalValve:Channel":
            connect_valve_channel_right(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Controller:ExternalValve":
            connect_controller_valve(domain1, domain2, connection.controller_port,
                nodes) ;
            break ;

        case "Channel:InterfaceTurbomachine":
            connect_interface_turbomachine_inlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "InterfaceTurbomachine:Channel":
            connect_interface_turbomachine_outlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Channel:Plenum":
            connect_plenum_inlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Plenum:Channel":
            connect_plenum_outlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Plenum:MapCompressor": goto case "Plenum:MapTurbomachine";
        case "Plenum:MapTurbine":    goto case "Plenum:MapTurbomachine";
        case "Plenum:MapTurbomachine":
            connect_map_turbomachine_inlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "MapCompressor:Plenum": goto case "MapTurbomachine:Plenum";
        case "MapTurbine:Plenum":    goto case "MapTurbomachine:Plenum";
        case "MapTurbomachine:Plenum":
            connect_map_turbomachine_outlet(domain1, domain2, interfaces, nodes) ;
            break ;

        case "Piso:IncompressibleChannel":
            connect_incompressible_solver_to_channel(domain1, domain2, interfaces, nodes);
             break;

        case "IncompressibleChannel:BoundaryWall":
            goto case "IncompressibleChannel:AxialWall" ;

        case "BoundaryWall:IncompressibleChannel":
            goto case "AxialWall:IncompressibleChannel" ;

        case "IncompressibleChannel:AxialWall":
            connect_wall_incompressible_channel(domain2, domain1, false,
                    interfaces, nodes, connection.name);
            break;

        case "AxialWall:IncompressibleChannel":
            connect_wall_incompressible_channel(domain1, domain2, true,
                    interfaces, nodes, connection.name);
            break;

        case "Inflow:Piso":
            connect_incompressible_inflow(domain1, domain2, interfaces, nodes);
            break;

        case "Piso:Outflow":
            connect_incompressible_outflow(domain1, domain2, interfaces, nodes);
            break;

        default:
            throw new InvalidConnection(format!("Cannot connect domain1 '%s' of"
                ~ " type %s to domain2 '%s' of type %s")
                (domain1.name, domain1.type, domain2.name, domain2.type)) ;
    }
}

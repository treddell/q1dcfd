#!/bin/bash

COOLPROP_INSTALL_DIR=$1

DIRECTORY=$(cd `dirname $0` && pwd)
COOLPROPD_DIR="$DIRECTORY/CoolPropD"
COOLPROPD_INSTALL="$COOLPROPD_DIR/install.sh"
LCONFIG_D="$DIRECTORY/source/lconfig.d"

# install CoolProp D wrapper
sh $COOLPROPD_INSTALL $COOLPROP_INSTALL_DIR

# register CoolPropD as a local package
dub add-local $COOLPROPD_DIR

# create the lconfig.d file
cat > $LCONFIG_D << EOF
// Local install configuration options. This file automatically generated

module q1dcfd.lconfig ;

// q1dcfd Install directory
enum string ROOT_DIRECTORY = "$DIRECTORY" ;

// CoolProp Repository directory
enum string COOLPROP_DIRECTORY = "$COOLPROP_INSTALL_DIR" ;
EOF

# Compile in release mode
dub build --build=release-nobounds --config=standard
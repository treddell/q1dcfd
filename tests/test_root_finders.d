
void test_secant(string[string] args)
{
    import q1dcfd.numerics. root ;
    import q1dcfd.utils.errors: TestFailure, NonConvergenceError ;
    import q1dcfd.utils.math: square ;

    import std.exception: enforce ;
    import std.math: sqrt, fabs ;

    // Function type test
    C1Function f = (double x) { return x.square - 2.0 ;} ;

    auto result1 = secant(f, 1.0) ;
    auto result2 = secant(f, -1.0) ;

    enforce!TestFailure(result1.exit_success == 0) ;
    enforce!TestFailure(result2.exit_success == 0) ;
    enforce!TestFailure(fabs(result1.x-sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(result2.x+sqrt(2.0)) < 1.48E-8) ;

    // Delegate test type
    double y = 2.0 ;
    C1Delegate g = (double x) { return x.square - y ;} ;

    auto result3 = secant(g, 1.0) ;
    auto result4 = secant(g, -1.0) ;

    enforce!TestFailure(result3.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(result4.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(fabs(result3.x-sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(result4.x+sqrt(2.0)) < 1.48E-8) ;

    // No solution, secant should fail
    C1Function h = (double x) { return x.square + 1.0 ;} ;
    enforce!TestFailure(secant(h, 1.0).exit_success == exit_code.MAX_ITERATIONS_EXCEEDED) ;
}

void test_ridder(string[string] args)
{
    import q1dcfd.numerics. root ;
    import q1dcfd.utils.errors: TestFailure, NonConvergenceError ;
    import q1dcfd.utils.math: square ;

    import std.exception: enforce ;
    import std.math: sqrt, fabs ;

    // Function type test
    C1Function f = (double x) { return x.square - 2.0 ;} ;

    auto result1 = ridder(f, 1.0, 3.0) ;
    auto result2 = ridder(f, -3.0, -1.0) ;

    enforce!TestFailure(result1.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(result2.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(fabs(result1.x-sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(result2.x+sqrt(2.0)) < 1.48E-8) ;

    // Delegate type test
    double y = 2.0 ;
    C1Delegate g = (double x) { return x.square - y ;} ;

    auto result3 = ridder(g, 1.0, 3.0) ;
    auto result4 = ridder(g, -3.0, -1.0) ;

    enforce!TestFailure(result3.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(result4.exit_success == exit_code.SUCCESS) ;
    enforce!TestFailure(fabs(result3.x-sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(result4.x+sqrt(2.0)) < 1.48E-8) ;

    C1Function h = (double x) { return x.square + 1.0 ;} ;
    enforce!TestFailure(ridder(h, -5.0, 5.0).exit_success == exit_code.MAX_ITERATIONS_EXCEEDED) ;
}

void test_root1d(string[string] args)
{
    import q1dcfd.numerics. root ;
    import q1dcfd.utils.errors: TestFailure, NonConvergenceError ;
    import q1dcfd.utils.math: square ;

    import std.exception: enforce ;
    import std.math: sqrt, fabs ;
    import core.exception: AssertError ;
    import std.format ;
    import std.conv: to ;

    // Function does not bracket a root, should throw
    C1Function f = (double x) { return x.square - 2.0 ;} ;

    try
    {
        auto result = root1d(f, 2.0, 3.0, 2.5) ;

        throw new TestFailure("Test should fail but passed") ;
    }
    catch(AssertError e)
    {
        // expected behaviour
    }
    catch(Error e)
    {
        throw new TestFailure("Unexpected Error %s thrown, with message %s"
                              .format(typeid(e).to!string, e.msg)) ;
    }

    // Function brackets a distinct root, should pass
    C1Function g = (double x) { return x.square - 2.0 ;} ;

    enforce!TestFailure(fabs(root1d(g, 1.0, 3.0, 2.0) - sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(root1d(g, -3.0, -1.0, -2.0) + sqrt(2.0)) < 1.48E-8) ;

    // Delegate brackets a distinct root
    double y = 2.0 ;
    C1Delegate h = (double x) { return x.square - y ;} ;

    enforce!TestFailure(fabs(root1d(h, 1.0, 3.0, 2.0) - sqrt(2.0)) < 1.48E-8) ;
    enforce!TestFailure(fabs(root1d(h, -3.0, -1.0, -2.0) + sqrt(2.0)) < 1.48E-8) ;
}

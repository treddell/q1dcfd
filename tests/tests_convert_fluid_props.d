
import q1dcfd ;
import q1dcfd.utils.convert_fluid_props: cons_from_pressure_step ;
import q1dcfd.utils.math: square ;
import q1dcfd.utils.errors: TestFailure ;

import std.math: approxEqual, fabs ;
import std.exception: enforce ;

void test_cons_from_pressure_step(string[string] args)
{
    auto conservative_data = cast(immutable) args["table_directory"] ;
    auto isentropic_data = cast(immutable) args["isentropic_table"] ;

    auto conservative_table = new TableData(conservative_data) ;
    auto conservative_eos = generate_eos(
        cast(immutable) conservative_table,
        backend.TTSE,
        "Test eos, conservative data, cons_from_pressure_step") ;

    auto isentropic_table = new TableData(isentropic_data) ;
    auto isentropic_eos = generate_eos(
        cast(immutable) isentropic_table,
        backend.TTSE,
        "Test eos, isentropic data, cons_from_pressure_step") ;

    // upstream conditions @ p = 20E6, T = 800, v = 10
    Real s1 = 2705.7132 ;
    Real h1 = 1006451.4285 ;
    Real dm = 1283.3569 ;
    Real eta = 0.8 ;

    // downstream conditions
    Real rho_actual = 60.11937;
    Real e_actual = 769774.955804799 ;
    Real v_actual = 21.3468121 ;
    Real rhoE = rho_actual*(e_actual + 0.5*v_actual.square) ;

    // guess values
    Real[2] cons_vars = [70, 800_000.0] ;

    Real tol = 1.0E-6 ;

    cons_from_pressure_step(conservative_eos, isentropic_eos, cons_vars,
        dm, h1, s1, rhoE, eta, tol) ;

    enforce!TestFailure(fabs(1.0 - cons_vars[0]/rho_actual)<tol) ;
    enforce!TestFailure(fabs(1.0 - cons_vars[1]/e_actual)<tol) ;
}


void test_dictionary_tools(string[string] args)
{
    import std.exception: assertThrown, assertNotThrown, enforce ;
    import core.exception: AssertError ;

    import q1dcfd.utils.errors: InvalidDefinition, TestFailure ;
    import q1dcfd.utils.dictionary_tools ;

    string[string] raw_defs = [
        "x": "1.1",
        "y": "-2.3",
        "z": "5", 
        "w": "jhjf",
        "u": "-11",
        "s": "1,2,3,4"
    ] ;

    auto defs = new_dict!string
        .request(raw_defs,"x", "a")
        .demand(raw_defs, "y", "b")
        .demand(raw_defs, "z", "c")
        .demand(raw_defs, "w", "d") 
        .demand(raw_defs, "u", "f")
        .demand(raw_defs, "s");

    // Check request/demands of non existent keys
    enforce!TestFailure(new_dict!string.request(raw_defs, "v", "g") == new_dict!string) ;

    try
    {
        assertThrown!InvalidDefinition(new_dict!string.demand(raw_defs, "t", "h")) ;
    }
    catch(AssertError e)
    {
        throw new TestFailure(e.msg) ;
    }
    
    // Check raw definitions are emptied
    try
    {
        assertNotThrown(raw_defs.require_empty("testcase")) ;
    }
    catch(AssertError e)
    {
        throw new TestFailure(e.msg) ;
    }

    // check new definitions have correctly copied
    enforce!TestFailure(defs["a"] == "1.1") ;
    enforce!TestFailure(defs["b"] == "-2.3") ;
    enforce!TestFailure(defs["c"] == "5") ;
    enforce!TestFailure(defs["d"] == "jhjf") ;
    enforce!TestFailure(defs["f"] == "-11") ;

    // check various unpacking and bounds checking operations
    auto temp = defs.retrieve("a").defaults(1.0).greater(0.5).less(2.0) ;
    enforce!TestFailure(temp == 1.1) ;
    enforce!TestFailure(defs.retrieve("b").required!double.geq(-3.0).leq(0.0) == -2.3) ;
    enforce!TestFailure(defs.retrieve("c").required!int == 5) ;
    enforce!TestFailure(defs.retrieve("e").defaults(4.1) == 4.1) ;
    
    auto superset = ["1","2","3","4","5"] ; 
    enforce!TestFailure(defs.retrieve("s")
           .subset(superset)
           .defaults(superset) == ["1", "2", "3", "4"]) ;

    try
    {
        assertThrown!InvalidDefinition(defs.retrieve("d").required!int) ;
        assertThrown!InvalidDefinition(defs.retrieve("f").required!int.greater(-5)) ;
    }
    catch(AssertError e)
    {
        throw new TestFailure(e.msg) ;
    }

    // check that GenericFields are treated as basic type inputs
    auto func = (double x) {return 2.0*x ;} ;
    enforce!TestFailure(func(temp) == 2.2) ;
}

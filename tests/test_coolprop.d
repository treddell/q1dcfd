import q1dcfd.utils.errors: TestFailure ;
import coolprop ;
import std.exception: enforce ;
import std.math: approxEqual ;

/// Extensive testing covered in CoolProp wrapper subpackage, only need to confirm linking
void test_load_coolprop(string[string] args)
{
    auto fluid = new AbstractState("HEOS", "CO2") ;
    fluid.update(PT_INPUTS, 8.0E6, 500.0) ;
    enforce!TestFailure(fluid.rhomass.approxEqual(89.410106806)) ;
}

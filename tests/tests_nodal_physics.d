import q1dcfd.config: Real, Address ;
import q1dcfd.tables: TableData ;
import q1dcfd.control_volumes: NavierStokesNode ;
import q1dcfd.utils.math: square ;
import q1dcfd.types.geometry: NodeGeometry ;
import q1dcfd.stream: Stream ;
import q1dcfd.eos: generate_eos, backend ;
import q1dcfd.utils.errors: TestFailure ;

import std.math: PI, approxEqual, fabs ; 
import std.exception: enforce ;

void test_navier_stokes_node(string[string] args)
{
    auto stream = new Stream(
        "test", new string[](0),
        new TableData(args["table_directory"]),
        new TableData(args["primitive_table"]),
        new TableData(args["isentropic_table"]),
        new string[](0)) ;
    
    auto check_eos = generate_eos(
        stream.conservative_data,
        backend.TTSE,
        "Test eos: test_navier_stokes_node") ;

    // generate the test geometry
    auto geometry = NodeGeometry() ;
    geometry.hydr_d = 0.1 ;
    geometry.volume = 0.1*0.1*0.5*PI*0.01 ;
    geometry.specific_density = 1.0/geometry.volume ;
    geometry.delta = 0.0 ;
    geometry.length = 0.01 ;

    auto address = Address(0, 3, 0, 0, "domain") ;

    // generate the test node
    auto ns_node = new NavierStokesNode(
        address, stream, geometry, "SwitchFrictionFactor", "SwitchHeatTransfer",
        false, 1, backend.TTSE) ;

    // generate the test state
    Real rho_in = 100.0 ;
    Real e_in = 400_000.0 ;
    Real v_in = 10.0 ;
    Real[] state = [rho_in, rho_in*v_in, rho_in*(e_in+0.5*v_in.square)] ;
    check_eos.update(rho_in, e_in) ;

    // positive velocity coefficients
    Real M_p, re_p, ke_p, E_p, Pf_p, Ef_p, dt_p, dm_p ;

    with(ns_node)
    {
        // test update
        update(state, 0.0) ;

        enforce!TestFailure(flux.left == [0.0,0.0,0.0],"Flux not correctly zeroed") ;
        enforce!TestFailure(flux.right == [0.0,0.0,0.0],"Flux not correctly zeroed") ;
        enforce!TestFailure(flux.extern_heat == 0.0, "Flux not correctly zeroed") ;
        enforce!TestFailure(rho == rho_in) ;
        enforce!TestFailure(v.approxEqual(v_in, 10.0*Real.epsilon)) ;
        enforce!TestFailure(e.approxEqual(e_in, 10.0*Real.epsilon)) ;
        enforce!TestFailure(ke.approxEqual(0.5*v_in.square, 10.0*Real.epsilon)) ;
        enforce!TestFailure(dm == state[1]) ;
        enforce!TestFailure(internal_source[0] == 0.0) ;

        // get positive velocity coefficients
        dm_p = dm ;
        M_p = M ;
        re_p = re ;
        ke_p = ke ;
        E_p = E ;
        Pf_p = internal_source[1] ;
        Ef_p = internal_source[2] ;
        dt_p = critical_dt ;

        // check at negative velocities
        state = [rho_in, -rho_in*v_in, rho_in*(e_in+0.5*v_in.square)] ;
        update(state, 0.0) ;

        enforce!TestFailure(dm == -dm_p) ;
        enforce!TestFailure(M == -M_p) ;
        enforce!TestFailure(re == re_p) ;
        enforce!TestFailure(ke == ke_p) ;
        enforce!TestFailure(E = E_p) ;
        enforce!TestFailure(internal_source[1] == -Pf_p) ;
        enforce!TestFailure(internal_source[2] == -Ef_p) ;
        enforce!TestFailure(critical_dt == dt_p) ;

        // check balance update equations
        Real[] balance = [0.0, 0.0, 0.0] ;
        update_balance(balance) ;
        enforce!TestFailure(balance == [0.0, internal_source[1], internal_source[2]]) ;
    }  
}

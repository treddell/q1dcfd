import q1dcfd.config: Real ;
import q1dcfd.numerics: RK4, DOPRI, RKCK45 ;
import q1dcfd.utils.errors: TestFailure ;

import std.math: exp, sin, cos, approxEqual, sqrt ;
import std.exception: enforce ;
import std.algorithm.comparison: min ;
import std.stdio ;

/** 
 * Simple ODE test case to test validity of numerical integrator 
 *
 * Has similar interface to actual simulation inputs
 */
class SpringMassDamper
{
    static
    {
        Real alpha ; // k/m
        Real beta ; // c/m
        Real omega_d ; // damped natural frequency
        Real[] state ;
        Real[] balance ;
        Real c1, c2 ;
    }

    static void initialise(Real alpha, Real beta, Real[2] initial)
    {
        SpringMassDamper.state.length = 2 ;
        SpringMassDamper.balance.length = 2; 
        SpringMassDamper.alpha = alpha ;
        SpringMassDamper.beta = alpha ;
        SpringMassDamper.omega_d = sqrt(-0.25*beta*beta + alpha) ;

        SpringMassDamper.state[] = initial[] ;
        SpringMassDamper.march(SpringMassDamper.balance, SpringMassDamper.state,  0.0) ;
        SpringMassDamper.c1 = initial[0] ;
        SpringMassDamper.c2 = (initial[1] + 0.5*beta * initial[0])/omega_d ;

        assert(0.25*beta*beta < alpha, "Test case ODE only implemented for "
            ~"underdamped motion") ;
    }

    static void march(ref Real[] balance, const ref Real[] state, const Real t)
    {
        balance[0] = state[1] ;
        balance[1] = -alpha * state[0] - beta * state[1] ;
    }

    private static Real[2] analytical(Real t)
    {
        auto x = exp(-0.5*beta*t) * (c1*cos(omega_d*t) + c2*sin(omega_d*t)) ;
        auto v = -0.5*beta*x + omega_d * exp(-0.5*beta*t) *(-c1*sin(omega_d*t)
            + c2*cos(omega_d*t)) ;
        Real[2] result = [x, v] ;
        return result ;
    }
}

// Test case parameters
enum Real BETA = 1.0 ;
enum Real ALPHA = 1.0 ;
enum Real END_TIME = 5.0 ;
enum Real MAX_DELTA_T = 1.0E-3 ;
enum Real MIN_DELTA_T = 1.0E-5 ;
enum Real ADAP_SF = 0.98 ;
enum Real ADAP_TOL = 1.0E-3 ;
enum Real[2] INITIAL = [1.0, 1.0] ;

void test_rk4(string[string] args)
// Testing Classic Runge Kutta 4th Order Method
{
    SpringMassDamper.initialise(ALPHA, BETA, INITIAL) ;
    auto integrator = new RK4 ;
    integrator.reserve_workspace(2) ;

    Real t = 0.0 ;
    Real dt = MAX_DELTA_T ;
    while(t < END_TIME)
    {
        dt = integrator.update(&SpringMassDamper.march, SpringMassDamper.state, 
            SpringMassDamper.balance, t, dt);
        dt = min(dt, END_TIME - dt) ;
        t += dt ;
    }

    Real[2] exact = SpringMassDamper.analytical(END_TIME) ;

    enforce!TestFailure(exact[0].approxEqual(SpringMassDamper.state[0])) ;
    enforce!TestFailure(exact[1].approxEqual(SpringMassDamper.state[1])) ;
}

void test_rkck45(string[string] args)
// Testing Cash Karp 45 Method
{
    SpringMassDamper.initialise(ALPHA, BETA, INITIAL) ;
    auto integrator = new RKCK45(MIN_DELTA_T, MAX_DELTA_T, ADAP_SF, ADAP_TOL) ;
    integrator.reserve_workspace(2) ;

    Real t = 0.0 ;
    Real dt = MAX_DELTA_T ;
    while(t < END_TIME)
    {
        dt = integrator.update(&SpringMassDamper.march, SpringMassDamper.state, 
            SpringMassDamper.balance, t, dt);
        dt = min(dt, END_TIME - dt) ;
        t += dt ;
    }

    Real[2] exact = SpringMassDamper.analytical(END_TIME) ;

    enforce!TestFailure(exact[0].approxEqual(SpringMassDamper.state[0])) ;
    enforce!TestFailure(exact[1].approxEqual(SpringMassDamper.state[1])) ;
}

void test_dopri(string[string] args)
// Testing Dormand Prince 45 Method
{
    SpringMassDamper.initialise(ALPHA, BETA, INITIAL) ;
    auto integrator = new DOPRI(MIN_DELTA_T, MAX_DELTA_T, ADAP_SF, ADAP_TOL) ;
    integrator.reserve_workspace(2) ;

    Real t = 0.0 ;
    Real dt = MAX_DELTA_T ;
    while(t < END_TIME)
    {
        dt = integrator.update(&SpringMassDamper.march, SpringMassDamper.state, 
            SpringMassDamper.balance, t, dt);
        dt = min(dt, END_TIME - dt) ;
        t += dt ;
    }

    Real[2] exact = SpringMassDamper.analytical(END_TIME) ;

    enforce!TestFailure(exact[0].approxEqual(SpringMassDamper.state[0])) ;
    enforce!TestFailure(exact[1].approxEqual(SpringMassDamper.state[1])) ;
}

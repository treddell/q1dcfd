import std.exception: enforce ;

import q1dcfd.utils.math ;
import q1dcfd.utils.errors: TestFailure ;

void test_linear_solver(string[string] args)
{
    import std.math: approxEqual ;

    import q1dcfd.numerics: gauss ;

    double[][] A = [
        [1.00, 0.00, 0.00,  0.00,  0.00,   0.00],
        [1.00, 0.63, 0.39,  0.25,  0.16,   0.10],
        [1.00, 1.26, 1.58,  1.98,  2.49,   3.13],
        [1.00, 1.88, 3.55,  6.70, 12.62,  23.80],
        [1.00, 2.51, 6.32, 15.88, 39.90, 100.28],
        [1.00, 3.14, 9.87, 31.01, 97.41, 306.02]
    ];
    
    double[] b = [-0.01, 0.61, 0.91, 0.99, 0.60, 0.02];

    auto result = gauss(A, b) ;

    double[] exact = [-0.01, 1.602790394502114,-1.6132030599055613, 
        1.2454941213714368, -0.4909897195846576, 0.065760696175232];

    enforce!TestFailure(result.approxEqual(exact, 1.0E-13)) ;
}

// Test Polynomial object
void test_polynomial(string[string] args)
{
    import std.math: approxEqual ;

    auto quad = new Polynomial([-4, 0, 1]) ;
    enforce!TestFailure(quad.evaluate(2) == 0) ;
    enforce!TestFailure(quad.evaluate(-2) == 0) ;
    enforce!TestFailure(quad.evaluate(17.1) == 288.41) ;
    
    auto line = quad.derivative ;
    enforce!TestFailure(line.evaluate(2) == 4.0) ;
    enforce!TestFailure(line.evaluate(0) == 0.0) ;
    enforce!TestFailure(line.evaluate(-13.1) == -26.2) ;

    auto cubic = quad.indefinite_integral ;
    enforce!TestFailure(cubic.coeffs == [0.0, -4.0, 0.0, 1.0/3.0]) ;
    enforce!TestFailure(quad.integrate(-2.0, 2.0).approxEqual(-10.667)) ;
}

// Test Exponential Polynomial Object
void test_exponential_polynomial(string[string] args)
{
    import std.math: approxEqual ;
    
    auto func = new ExponentialPolynomial(
        [-1.0, 2.0, 3.0],
        [0.0, -0.5, 1.5]) ;

    enforce!TestFailure(func.coeffs == [-1.0, 2.0, 3.0]) ;
    enforce!TestFailure(func.indices == [0.0, -0.5, 1.5]) ;
    enforce!TestFailure(func.evaluate(1.0).approxEqual(4.0)) ;
    enforce!TestFailure(func.evaluate(4.0).approxEqual(24.0)) ;

    auto der = func.derivative ;
    enforce!TestFailure(der.coeffs == [-1.0, 4.5]) ;
    enforce!TestFailure(der.indices == [-1.5, 0.5]) ;

    enforce!TestFailure(func.integrate(1.0, 2.0).approxEqual(6.24508)) ;
}

// Test PiecewiseFunction object on a piecewise continuous polynomial
void test_piecewise_func(string[string] args)
{
    import std.math: approxEqual ;
    import std.conv: to ;
    
    auto piecewise = new PiecewiseFunction(
        [new Polynomial([0.0, 1.0]),
         new Polynomial([1.0,-2.0,1.0])], 
        [1.0, 2.0]) ;
    
    enforce!TestFailure(piecewise.pieces.length == 2) ;
    enforce!TestFailure(piecewise.pieces[0].to!Polynomial.coeffs == [0.0, 1.0]) ;
    enforce!TestFailure(piecewise.pieces[1].to!Polynomial.coeffs == [1.0, -2.0, 1.0]) ;
    enforce!TestFailure(piecewise.evaluate(0.5) == 0.5) ;
    enforce!TestFailure(piecewise.evaluate(1.5) == 0.25) ;
    
    auto der = piecewise.derivative ;
    enforce!TestFailure(der.pieces[0].to!Polynomial.coeffs == [1.0]) ;
    enforce!TestFailure(der.pieces[1].to!Polynomial.coeffs == [-2.0, 2.0]) ;

    enforce!TestFailure(piecewise.integrate(0.5, 1.5).approxEqual(0.416667)) ;
}

// Test Step object
void test_step(string[string] args)
{
    auto step = new Step(1.0, 2.0, 1.5) ;
    enforce!TestFailure(step.evaluate(1.4999) == 1.0) ;
    enforce!TestFailure(step.evaluate(1.5001) == 2.0) ;
    enforce!TestFailure(step.evaluate(-double.infinity) == 1.0) ;
    enforce!TestFailure(step.evaluate(double.infinity) == 2.0) ;   
}

// Test linear ramp object
void test_linear_ramp(string[string] args)
{
    import std.math: approxEqual ;
    import std.conv: to ;

    auto ramp = new LinearRamp(1.0,1.0,2.0,4.0) ;
    enforce!TestFailure(ramp.pieces[1].to!Polynomial.coeffs.approxEqual([-2.0, 3.0])) ;
    enforce!TestFailure(ramp.evaluate(0.5) == 1.0) ;
    enforce!TestFailure(ramp.evaluate(2.5) == 4.0) ;
    enforce!TestFailure(ramp.evaluate(1.5).approxEqual(2.5)) ;
    enforce!TestFailure(ramp.derivative.evaluate(1.5).approxEqual(3.0)) ;

    enforce!TestFailure(ramp.integrate(1.25, 1.75).approxEqual(1.25)) ;
}

// Test cubic ramp object
void test_cubic_ramp(string[string] args)
{
    import std.math: approxEqual ;

    auto ramp = new CubicRamp(1.0, 1.0, 2.0, 3.0) ;
    enforce!TestFailure(ramp.evaluate(1.0 - 10.0*double.epsilon) == 1.0) ;
    enforce!TestFailure(ramp.evaluate(1.0 + 10.0*double.epsilon).approxEqual(1.0)) ;
    enforce!TestFailure(ramp.evaluate(2.0 + 10.0*double.epsilon) == 3.0) ;
    enforce!TestFailure(ramp.evaluate(2.0 - 10.0*double.epsilon).approxEqual(3.0)) ;

    auto ramp_slope = ramp.derivative ;
    enforce!TestFailure(ramp_slope.evaluate(1.0 - 10.0*double.epsilon) == 0.0) ;
    enforce!TestFailure(ramp_slope.evaluate(1.0 + 10.0*double.epsilon).approxEqual(0.0)) ;
    enforce!TestFailure(ramp_slope.evaluate(2.0 + 10.0*double.epsilon) == 0.0) ;
    enforce!TestFailure(ramp_slope.evaluate(2.0 - 10.0*double.epsilon).approxEqual(0.0)) ;
}

// Test square root ramp object
void test_square_root_ramp(string[string] args)
{
    import std.math: approxEqual ;
    import std.conv: to ;
    
    auto func = new SqrtRamp(1.0, 4.0, 9.0, -1.0) ;
    enforce!TestFailure(func.pieces[1].to!ExponentialPolynomial.coeffs.approxEqual([-2.5, 6.5])) ;
    enforce!TestFailure(func.pieces[1].to!ExponentialPolynomial.indices == [0.5, 0.0]) ;
    enforce!TestFailure(func.evaluate(0.5) == 4.0) ;
    enforce!TestFailure(func.evaluate(9.5) == -1.0) ;
    enforce!TestFailure(func.evaluate(4.0).approxEqual(1.5)) ;

    auto der  = func.derivative ;
    enforce!TestFailure(der.evaluate(0.999) == 0.0) ;
    enforce!TestFailure(der.evaluate(9.0001) == 0.0) ;
}

// Test cubic spline interpolation
void test_cubic_spline(string[string] args)
{
    import std.math: approxEqual ;
    
    auto x = [0.9, 1.3, 1.9, 2.1] ;
    auto y = [1.3, 1.5, 1.85, 2.1] ;

    auto spline = new CubicSpline(x, y) ;
    enforce!TestFailure(spline.evaluate(0.9).approxEqual(1.3)) ;
    enforce!TestFailure(spline.evaluate(1.1).approxEqual(1.4054054)) ;
    enforce!TestFailure(spline.evaluate(1.5).approxEqual(1.58183183)) ;
    enforce!TestFailure(spline.evaluate(2.0).approxEqual(1.96841216)) ;
    enforce!TestFailure(spline.evaluate(2.1).approxEqual(2.1)) ;
}

import coolprop ;
import q1dcfd.quick_cycle ;
import q1dcfd.utils.errors: TestFailure ;

import std.exception: enforce ;
import std.math: approxEqual ;
import std.format ;

private
{
    // Uses Helium as an ideal gas approximation
    // perfectly efficient turbomachinery, all conditions ideal
    double p_turbine_in = 4.0E6 ;
    double p_turbine_out = 1.0E6 ;
    double t_turbine_in = 800 + 273 ;
    double t_compressor_in = 30 + 273 ;
    double w_out ;
    double w_in ;

    AbstractState turbine_inlet_state ;
    AbstractState turbine_outlet_state ;
    AbstractState compressor_inlet_state ;
    AbstractState compressor_outlet_state ;

    bool initialised = false ;
}

void initialise()
{
    if(!initialised)
    {
        turbine_inlet_state = new AbstractState("HEOS", "Helium") ;
        turbine_outlet_state = new AbstractState("HEOS", "Helium") ;
        compressor_inlet_state = new AbstractState("HEOS", "Helium") ;
        compressor_outlet_state = new AbstractState("HEOS", "Helium") ;
    
        turbine_inlet_state.update(PT_INPUTS, p_turbine_in, t_turbine_in) ;
        turbine_outlet_state.update(PSmass_INPUTS, p_turbine_out, turbine_inlet_state.smass) ;
        compressor_inlet_state.update(PT_INPUTS, p_turbine_out, t_compressor_in) ;
        compressor_outlet_state.update(PSmass_INPUTS, p_turbine_in, compressor_inlet_state.smass) ;

        w_out = turbine_inlet_state.hmass - turbine_outlet_state.hmass ;
        w_in = compressor_outlet_state.hmass - compressor_inlet_state.hmass ;

        initialised = true ;
    }
}

void test_simple_brayton_design(string[string] args)
{
    initialise ;
    
    auto q_in = turbine_inlet_state.hmass - compressor_outlet_state.hmass ;
    auto efficiency = (w_out - w_in)/q_in ;

    auto simple_brayton = design_simple_brayton(
        "Helium", p_turbine_in, t_turbine_in, p_turbine_out, t_compressor_in,
        1.0, 1.0) ;

    enforce!TestFailure(efficiency.approxEqual(simple_brayton.efficiency, 1.0E-12, 1.0E-12),
            format!("Expected efficiency %f but got efficiency %s")
            (efficiency, simple_brayton.efficiency)) ;
}

void test_recuperated_brayton_cycle_design(string[string] args)
{
    initialise ;
    
    auto hot_side_exit = new AbstractState("HEOS", "Helium") ;
    hot_side_exit.update(PT_INPUTS, p_turbine_out, compressor_outlet_state.T + 10) ;

    auto heat_recup = turbine_outlet_state.hmass - hot_side_exit.hmass ;
    auto q_in = turbine_inlet_state.hmass - compressor_outlet_state.hmass - heat_recup ;
    auto efficiency = (w_out - w_in)/q_in ;

    auto recup_brayton = design_recuperated_brayton(
        "Helium", p_turbine_in, t_turbine_in, p_turbine_out, t_compressor_in,
        1.0, 10.0, 1.0) ;

    enforce!TestFailure(efficiency.approxEqual(recup_brayton.efficiency, 1.0E-12, 1.0E-12),
            format!("Expected efficiency %f but got efficiency %s")
            (efficiency, recup_brayton.efficiency)) ;
}

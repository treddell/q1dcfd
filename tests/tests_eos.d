import std.math: approxEqual ;
import std.conv: to ;
import std.exception: enforce ;

import q1dcfd.utils.errors: TestFailure ;

import q1dcfd ;

/*
 * Tests importing the tabular data, assigning to a TableData object,
 * assigning TableData object to a EOS object, correct contents of TableData,
 * and approximately correct fluid properties 
 */
void eos_test(backend back, string[string] args)
{
    // create data class and check its inputs
    string conservative_data = args["table_directory"] ;
    string isentropic_data = args["isentropic_table"] ;
    string primitive_data = args["primitive_table"] ;
    auto conservative_table = cast(immutable) new TableData(conservative_data) ;
    auto isentropic_table = cast(immutable) new TableData(isentropic_data) ;
    auto primitive_table = cast(immutable) new TableData(primitive_data) ;
    
    with(conservative_table)
    {
        enforce!TestFailure(output_vars == ["h", "T", "Cp", "mu", "k", "a", "Q", "s"]) ;
        enforce!TestFailure(i1_str == "rho") ;
        enforce!TestFailure(i2_str == "e") ;
        enforce!TestFailure(n_variables == 8) ;
        enforce!TestFailure(n_columns ==8*6 + 1) ;
        enforce!TestFailure(i1_min.approxEqual(46.3892, 1.0E-2, 1.0E-4)) ;
        enforce!TestFailure(i1_max.approxEqual(1039.03, 1.0E-2, 1.0E-2)) ;
        enforce!TestFailure(i2_min.approxEqual(169229.0, 1.0E-2, 1.0)) ;
        enforce!TestFailure(i2_max.approxEqual(965618.0, 1.0E-2, 1.0)) ;
        enforce!TestFailure(i1_step.approxEqual(19.8527, 1.0E-2, 1.0E-4)) ;
        enforce!TestFailure(i2_step.approxEqual(15927.8, 1.0E-2, 1.0E-1)) ;
        enforce!TestFailure(i1_lowest.approxEqual(i1_min + 0.5*i1_step)) ;
        enforce!TestFailure(i1_largest.approxEqual(i1_max - 0.5*i1_step)) ;
        enforce!TestFailure(i2_lowest.approxEqual(i2_min + 0.5*i2_step)) ;
        enforce!TestFailure(i2_largest.approxEqual(i2_max - 0.5*i2_step)) ;
    }

    // create interpolation classes
    auto eos = generate_eos(
        conservative_table,
        back,
        "EOS Test Conservative") ;

    auto isentropic_eos = generate_eos(
        isentropic_table,
        back,
        "EOS Test Isentropic") ;

    auto primitive_eos = generate_eos(
        primitive_table,
        back,
        "EOS Test Primitive") ;

    // give a state
    eos.update(400.0, 500_000.0) ;

    // check state
    with(eos)
    {
        enforce!TestFailure(rho == 400.0) ; // density
        enforce!TestFailure(e == 500_000.0) ; // internal energy
        enforce!TestFailure(h.approxEqual(580566.0, 1.0E-2, 1)) ; // enthalpy
        enforce!TestFailure(T.approxEqual(483.157, 1.0E-2, 1.0E-3)) ; // temperature
        enforce!TestFailure(cp.approxEqual(1508.0, 1.0E-2, 1.0)) ; // specific heat cap
        enforce!TestFailure(dv.approxEqual(3.6482E-5, 1.0E-2, 1.0E-3)) ; // viscosity 
        enforce!TestFailure(k.approxEqual(0.054026, 1.0E-2, 1.0E-6)) ; // conductivity
        enforce!TestFailure(a.approxEqual(388.1, 1.0E-2, 1.0E-1)) ; // speed of sound
        enforce!TestFailure(s.approxEqual(1932.3, 1.0E-2, 1.0)) ; // entropy

        // first partial derivatives
        // Less accurate, since the taylor series are calculated at first not
        // second order
        enforce!TestFailure(dTdD.approxEqual(0.221887, 3.0E-2, 1.0E-2)) ; // "alpha_d"
        enforce!TestFailure(dTde.approxEqual(0.0011126, 1.0E-2, 1.0E-3)) ; // "alpha_e"
    }

    // give an isentropic state
    isentropic_eos.update(10.0E6, 2000) ;

    with(isentropic_eos)
    {
        enforce!TestFailure(h.approxEqual(530816.33, 1.0E-2, 1)) ; // enthalpy
        enforce!TestFailure(rho.approxEqual(168.64893, 1.0E-2, 1)) ; // density
    }

    // give a primitive state
    primitive_eos.update(10.0E6, 600.0) ;

    with(primitive_eos)
    {
        enforce!TestFailure(rho.approxEqual(89.9411525, 1.0E-2, 1)) ; // density
        enforce!TestFailure(e.approxEqual(668090.872, 1.0E-2, 1)) ; // internal energy
    }

    // Test out of bounds calculation
    if(back == backend.TTSE_recovery)
    {
        primitive_eos.update(40.0E6, 1000.0) ;
        enforce!TestFailure(primitive_eos.rho.approxEqual(192.372273)) ;
        enforce!TestFailure(primitive_eos.e.approxEqual(1045027.0835587)) ;
    }
}

void ttse_direct_test(string[string] args) {eos_test(backend.TTSE, args) ;}
void ttse_recovery_test(string[string] args) {eos_test(backend.TTSE_recovery, args) ;}
void coolprop_eos_test(string[string] args) {eos_test(backend.coolprop, args) ;}

module tests_main ;

import tests_eos: ttse_direct_test, ttse_recovery_test, coolprop_eos_test ;
import tests_convert_fluid_props: test_cons_from_pressure_step ;
import tests_integrators: test_rk4, test_rkck45, test_dopri ;
import tests_nodal_physics: test_navier_stokes_node ;
import test_coolprop: test_load_coolprop ;
import test_cycle_design: test_simple_brayton_design, test_recuperated_brayton_cycle_design ;
import test_maths: test_polynomial, test_linear_ramp, test_cubic_ramp, test_exponential_polynomial,
    test_step, test_square_root_ramp, test_cubic_spline, test_piecewise_func, test_linear_solver ;
import test_utils: test_dictionary_tools ;
import test_code_generation: test_unique_and_required, test_unique_not_required,
    test_required_not_unique, test_generate_update_invariants, test_generate_string_mappings,
    test_init_conditions;
import test_root_finders: test_secant, test_ridder, test_root1d;

/**
 * Test Case Container
 */
private struct Test 
{
    string name ; // Test identifier, human readable
    void function(string[string]) run ; // Test case 
}

Test[] tests = [
    Test("CoolProp Test", &test_load_coolprop),
    Test("TTSE data import and interpolation validation", &ttse_direct_test),
    Test("TTSE data import and interpolation with recovery", &ttse_recovery_test),
    Test("CoolProp Equation of State Tests", &coolprop_eos_test), 
    Test("CycleDesign: Simple Brayton", &test_simple_brayton_design),
    Test("CycleDesign: Recuperated Brayton", &test_recuperated_brayton_cycle_design),
    Test("conservative variables from pressure step", &test_cons_from_pressure_step),
    Test("ODE Solvers: RK4", &test_rk4),
    Test("ODE Solvers: RKCK45", &test_rkck45),
    Test("ODE Solvers: DOPRI", &test_dopri),
    Test("Root Finders: secant", &test_secant),
    Test("Root Finders: ridder", &test_ridder),
    Test("Root Finders: root1d", &test_root1d),
    Test("Linear Solver: Gauss", &test_linear_solver),
    Test("Nodal Physics: NavierStokes", &test_navier_stokes_node),
    Test("Math: Polynomial", &test_polynomial),
    Test("Math: Linear Ramp", &test_linear_ramp),
    Test("Math: Cubic Ramp", &test_cubic_spline),
    Test("Math: Exponential Polynomial", &test_exponential_polynomial),
    Test("Math: Step", &test_step),
    Test("Math: Square Root Ramp", &test_square_root_ramp),
    Test("Math: Cubic Spline", &test_cubic_spline),
    Test("Math: Piecewise Function", &test_piecewise_func),
    Test("Utils: Dictionary Tools", &test_dictionary_tools),
    Test("UDA's: @required", &test_required_not_unique),
    Test("UDA's: @unique", &test_unique_not_required),
    Test("UDA's: @required and @unique", &test_unique_and_required),
    Test("UDA's: @updateInvariant", &test_generate_update_invariants),
    Test("UDA's: @recordableProperty", &test_generate_string_mappings),
    Test("UDA's: @initCondition", &test_init_conditions)
] ;

//run all tests
void run_tests() 
{
    import std.path: dirSeparator ;
    import std.format ;
    import std.stdio ;
    import std.algorithm: count ;
    import std.array: join, split ;

    import q1dcfd ;
    import q1dcfd.utils.pprint: completion_print ;
    import q1dcfd.utils.errors: TestFailure ;
    import q1dcfd.lconfig: ROOT_DIRECTORY ;

    // Condition Failures: An assertion fails
    enum CONDITION_FAILURE = "Test Assertion Failure!" ;

    // Ungraceful failure: A test step throws an unexpected exception
    enum UNGRACEFUL_FAILURE = "Test failed to run!" ;

    // get tabular data directory
    string table_directory = [ROOT_DIRECTORY,"tabular_data","TEST",
        "density_energy"].join(dirSeparator) ;
    string isentropic_table = [ROOT_DIRECTORY, "tabular_data","TEST",
        "pressure_entropy"].join(dirSeparator) ;
    string primitive_table = [ROOT_DIRECTORY, "tabular_data","TEST",
        "pressure_temperature"].join(dirSeparator) ;

    // arguments
    string[string] args ;
    args["table_directory"] = table_directory ;
    args["isentropic_table"] = isentropic_table ;
    args["primitive_table"] = primitive_table ;

    // test summary
    int n_failed = 0 ;
    string log = "" ;

    // Run every test, if fails, then record exception and move on
    foreach(i, test ; tests)
    {
        auto n_spaces = (i > 8) ? "" : " " ;
        auto begin_msg = "(%u/%u) %s: %s".format(i+1, tests.length, n_spaces, test.name) ;
        writef("%s", begin_msg) ;

        // clears all cached simulation data returning it to default state
        scope(exit)
        {
            Simulation.reset ;
        }

        try 
        {
            test.run(args) ;
            scope(success) {completion_print(begin_msg, "[PASSED]") ;}
        }
        catch(TestFailure e)
        // unsuccessful test
        {
            n_failed++ ;
            log ~= "\n.............. Test %s ..............\n %s \n %s"
                .format(test.name, CONDITION_FAILURE, e) ;

            completion_print(begin_msg, "[FAILED]") ;
        }
        catch(Error e)
        {
            n_failed++ ;
            log ~= "\n.............. Test %s ..............\n %s \n %s"
                .format(test.name, UNGRACEFUL_FAILURE, e) ;

            completion_print(begin_msg, "[FAILED]") ;
        }
    }

    // print completion report
    if(n_failed == 0)
    {
        writeln("\nAll tests successful\n") ;
    }
    else
    {
        writefln("\n%u/%u tests failed! With Stack traces: \n%s\n", n_failed, tests.length, log);
    } 
}

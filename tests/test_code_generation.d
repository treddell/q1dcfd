/**
 * Tests related to UDA's and automatic code generation
 */

private struct TestStruct {}
 
class TestObject(bool is_required, bool is_unique)
{
    import q1dcfd.types.udas;
    import q1dcfd.simulation: Simulation;
    import q1dcfd.code_generation.generate_node_body;

    mixin GenerateGetterSetters!(typeof(this));
    mixin(GenerateInitialisationConditions!(typeof(this)));

    // code generation expects this method
    string identify() {return "TestObject";}

    static if(is_required && is_unique)
    {
        @required @unique protected TestStruct _thing;
    }
    else static if(is_required)
    {
        @required protected TestStruct _thing;
    }
    else static if(is_unique)
    {
        @unique protected TestStruct _thing;
    }
    else static assert(false);
}

private class WellPosedObject
{
    import q1dcfd.types.udas;
    import q1dcfd.types: AssertionResult;

    import q1dcfd.code_generation.generate_node_body;

    @updateInvariant
    {
        void should_work()
        {
            assert(2 > 1, "Oh no");
        }
    }

    @initCondition
    AssertionResult should_be_true()
    {
        return AssertionResult(2 > 1, "I should be true");
    }

    mixin(GenerateInvariantConditions!(typeof(this)));
    mixin(GenerateInitialisationConditions!(typeof(this)));
}

private class IllPosedObject
{
    import q1dcfd.types.udas;
    import q1dcfd.types: AssertionResult;

    import q1dcfd.code_generation.generate_node_body;

    @updateInvariant
    {
        void shouldnt_work()
        {
            assert(1 > 2, "false");
        }
    }

    @initCondition
    AssertionResult should_be_false()
    {
        return AssertionResult(2 < 1, "I should be false");
    }

    mixin(GenerateInvariantConditions!(typeof(this)));
    mixin(GenerateInitialisationConditions!(typeof(this)));
}

private class StringMapObject
{
    import q1dcfd.types.udas;
    import q1dcfd.types: Real;
    import q1dcfd.code_generation.generate_node_body;

    @recordableProperty
    {
        Real x() {return 5.0;}

        Real y() {return 3.0;}
    }

    mixin(GenerateStringMappings!(typeof(this)));
}

// Compile Time tests
static assert(__traits(hasMember, TestObject!(true, true), "set_thing"));
static assert(__traits(hasMember, TestObject!(true, true), "thing"));
static assert(__traits(hasMember, TestObject!(true, false), "set_thing"));
static assert(__traits(hasMember, TestObject!(true, false), "thing"));
static assert(__traits(hasMember, TestObject!(false, true), "set_thing"));
static assert(__traits(hasMember, TestObject!(false, true), "thing"));

/**
 * Testing the functionality of the GenerateGetterSetters code generation
 * routine
 */
private void test_getter_setters(bool is_required, bool is_unique)()
{
    import std.exception: enforce;
    import std.format;

    import q1dcfd.utils.errors: InvalidDefinition, TestFailure;
    import q1dcfd.simulation: Simulation;

    auto inst = new TestObject!(is_required, is_unique);
    auto thing = TestStruct();

    // testing required initialisation
    static if(is_required)
    {
        // required argument not set, should fail checks
        auto result = inst.check_initialisation_conditions;

        enforce!TestFailure(!result.success,
                            "Should have marked initialisation as a failure, but did not");
    }

    // testing setter
    inst.set_thing(thing);

    // testing required initialisation
    static if(is_required)
    {
        // required argument set, should pass checks
        result = inst.check_initialisation_conditions;

        enforce!TestFailure(result.success, "Initialisation checks should have succeeded");
    }

    // testing getter
    enforce!TestFailure(inst.thing is thing);

    // testing @unique
    // if @required but not @unique, then calling the setter again will work
    // else should throw an Invalid Definition
    try
    {
        inst.set_thing(thing);

        static if(is_unique)
        {
            throw new TestFailure("@uniqueness test failed");
        }
    }
    catch(InvalidDefinition e)
    {
        static if(is_unique)
        {
            // Intended behaviour
        }
        else
        {
            throw new TestFailure("Should not have thrown an InvalidDefinition, but did "
                                  ~ "with message: %s".format(e.msg));
        }
    }
    catch(Error e)
    {
        throw new TestFailure("Resetting unique field did not throw expected InvalidDefinition "
                              ~ "instead threw %s with message %s"
                              .format(typeid(e).toString, e.msg));
    }
}

void test_unique_and_required(string[string] args)
{
    test_getter_setters!(true, true);
}

void test_unique_not_required(string[string] args)
{
    test_getter_setters!(false, true);
}

void test_required_not_unique(string[string] args)
{
    test_getter_setters!(true, false);
}

/**
 * testing the correct functionality of the @updateInvariant UDA
 */
void test_generate_update_invariants(string[string] args)
{
    import std.format;
    import core.exception: AssertError;

    import q1dcfd.utils.errors: TestFailure;

    auto inst1 = new WellPosedObject;
    auto inst2 = new IllPosedObject;

    try
    {
        inst1.check_well_posedness;
    }
    catch(Error e)
    {
        throw new TestFailure("Assertion should have passed, but failed");
    }

    try
    {
        inst2.check_well_posedness;

        throw new TestFailure("Assert should have been thrown but wasn't");
    }
    catch(AssertError e)
    {
        // expected behaviour
    }
    catch(Error e)
    {
        throw new TestFailure(("Expected an AssertError, but instead a %s was thrown with"
                               ~ " message: %s").format(typeid(e), e.msg));
    }
}

void test_generate_string_mappings(string[string] args)
{
    import std.exception: enforce;
    import core.exception: AssertError;
    import std.math: approxEqual;

    import q1dcfd.utils.errors: TestFailure, MapToNonExistentField;

    auto inst = new StringMapObject;

    enforce!TestFailure(inst.map("x").approxEqual(5.0), "failed to map expected property x");
    enforce!TestFailure(inst.map("y").approxEqual(3.0), "failed to map expected property y");

    // try getting non existent property
    try
    {
        auto temp = inst.map("z");

        throw new TestFailure("Should have failed to get key");
    }
    catch(MapToNonExistentField e)
    {
        // expected behaviour
    }
}

void test_init_conditions(string[string] args)
{
    import std.format;
    import std.exception: enforce;

    import q1dcfd.utils.errors: TestFailure;

    auto inst1 = new WellPosedObject;
    auto inst2 = new IllPosedObject;

    auto result1 = inst1.check_initialisation_conditions;
    auto result2 = inst2.check_initialisation_conditions;

    enforce!TestFailure(result1.success, "Should have passed");
    enforce!TestFailure(!result2.success, "Should have failed");
}
